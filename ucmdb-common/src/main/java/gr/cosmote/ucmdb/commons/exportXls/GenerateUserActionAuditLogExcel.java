package gr.cosmote.ucmdb.commons.exportXls;

import gr.cosmote.ucmdb.models.database.NodeDifferences;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Data
@Slf4j
public class GenerateUserActionAuditLogExcel {
    private List<NodeDifferences> userDbActions;

    public byte[] exportActionExcel() {
        log.debug("GenerateUserActionAuditLogExcel.exportActionExcel :: Method Entered");
        Workbook auditWb = initWorkbook();
        createAuditWorkbookHeader(auditWb);
        fillAuditWorkbookWithData(auditWb);

        setColumnsWidth(auditWb);
        return writeAuditWorkbookBytes(auditWb);
    }

    private Workbook initWorkbook() {
        Workbook auditWb = new XSSFWorkbook();
        auditWb.createSheet("Action Audit Log");

        return auditWb;
    }

    private void styleAuditHeaderRow(Workbook auditWb, Row row) {
        CellStyle auditHeaderCellStyle = auditWb.createCellStyle();

        Font font = auditWb.createFont();
        font.setBold(true);

        auditHeaderCellStyle.setFont(font);
        auditHeaderCellStyle.setWrapText(true);


        Iterator<Cell> cells = row.cellIterator();
        while (cells.hasNext()) {
            Cell cell = cells.next();

            cell.setCellStyle(auditHeaderCellStyle);
        }
    }

    private void createAuditWorkbookHeader(Workbook auditWb) {
        Sheet userActionAuditLogSheet = auditWb.getSheet("Action Audit Log");

        Row headerAuditRow = userActionAuditLogSheet.createRow(0);


        headerAuditRow.createCell(0).setCellValue("PROCESS ID");
        headerAuditRow.createCell(1).setCellValue("OWNER TENANT");
        headerAuditRow.createCell(2).setCellValue("DISPLAY LABEL");
        headerAuditRow.createCell(3).setCellValue("USER ACTION");
        headerAuditRow.createCell(4).setCellValue("UPDATED BY");
        headerAuditRow.createCell(5).setCellValue("USER GROUP");
        headerAuditRow.createCell(6).setCellValue("MODIFY TIME");

        styleAuditHeaderRow(auditWb, headerAuditRow);
    }

    private void fillAuditWorkbookWithData(Workbook auditWb) {
        log.debug("GenerateUserActionAuditLogExcel.fillAuditWorkbookWithData :: Method Entered");
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Sheet userActionAuditLogSheet = auditWb.getSheet("Action Audit Log");

        // Cell Style for PROCESS_ID Column
        CellStyle alignStyle = auditWb.createCellStyle();
        alignStyle.setAlignment(HorizontalAlignment.LEFT);

        AtomicInteger rowNumber = new AtomicInteger(1);
        userDbActions.forEach(comm -> {
            Row currentAuditRow = userActionAuditLogSheet.createRow(rowNumber.getAndIncrement());

            if (comm.getNodeId() == 0) {
                currentAuditRow.createCell(0).setCellValue("-");
            } else {
                currentAuditRow.createCell(0).setCellValue(comm.getNodeId());
            }

            currentAuditRow.getCell(0).setCellStyle(alignStyle);
            currentAuditRow.createCell(1).setCellValue(comm.getOwnerTenant());
            currentAuditRow.createCell(2).setCellValue(comm.getDisplayLabel());
            currentAuditRow.createCell(3).setCellValue(comm.getUserAction());
            currentAuditRow.createCell(4).setCellValue(comm.getUpdatedBy());
            currentAuditRow.createCell(5).setCellValue(comm.getUserGroup());
            currentAuditRow.createCell(6).setCellValue(sdf.format(comm.getLastModifiedTime()));
        });

        log.debug("GenerateUserActionAuditLogExcel.fillAuditWorkbookWithData :: Method Returned");
    }

    private void setColumnsWidth(Workbook auditWb) {
        log.debug("GenerateUserActionAuditLogExcel.setColumnsWidth :: Method Entered");
        Sheet userActionAuditLogSheet = auditWb.getSheet("Action Audit Log");

        int numOfSheetColumns = userActionAuditLogSheet.getRow(0).getPhysicalNumberOfCells();
        for (int col = 0; col < numOfSheetColumns; col++) {
            userActionAuditLogSheet.setColumnWidth(col, 5800);
        }
        log.debug("GenerateUserActionAuditLogExcel.setColumnsWidth :: Method Returned");
    }

    private byte[] writeAuditWorkbookBytes(Workbook auditWb) {
        log.debug("GenerateUserActionAuditLogExcel.writeAuditWorkbookBytes :: Method Entered");

        byte[] auditWbBytes;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            auditWb.write(baos);
            auditWbBytes = baos.toByteArray();
        } catch (IOException ex) {
            log.error("GenerateUserActionAuditLogExcel.writeAuditWorkbookBytes :: Error", ex);

            return null;
        }

        return auditWbBytes;
    }
}

