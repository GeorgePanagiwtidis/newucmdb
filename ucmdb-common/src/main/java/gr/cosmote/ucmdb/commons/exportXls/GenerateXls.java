package gr.cosmote.ucmdb.commons.exportXls;

import gr.cosmote.ucmdb.models.base.BusinessResult;
import gr.cosmote.ucmdb.models.base.Ci;
import gr.cosmote.ucmdb.models.base.Ci_Simple;
import gr.cosmote.ucmdb.models.base.Computer;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import oracle.j2ee.ws.saaj.util.ByteOutputStream;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.poi.hpsf.SummaryInformation;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;

@Data
@Slf4j
public class GenerateXls {

    // Excel Workbook
    private Workbook workbook;
    // Object that contains Result like Computers, Cpus etc. Coming From Business Ejb
    private BusinessResult bs;
    // Excel Template aas Stream -- Stored in ucmdb-web\src\main\webapp\WEB-INF
    private InputStream input;

    // Init Point for Excel file Completion
    public byte[] generateXlsFile() {
        log.debug("GenerateXls.java :: generateXlsFile() entered ");
        setupWorkbookConfiguration();

        // Init stage -- Load Sheets, Method Names, Sheet Columns
        List<Sheet> sheetList = initExcelSheets();
        Map<Integer, String> methodNames = initMappedMethodNames();
        List<Map<Integer, String>> sheetColumns = generateSheetColumns();

        // Fill Sheets with Data
        fillSheets(sheetList, methodNames, sheetColumns);

        return writeDataToFile();
    }

    private void setupWorkbookConfiguration () {
        log.debug("GenerateXls :: setupWorkbookConfiguration :: Method Entered");

        if (workbook == null) {
            try {
                workbook = new HSSFWorkbook(input);

                // Set 1st Sheet as Active
                workbook.setActiveSheet(0);
                workbook.setFirstVisibleTab(0);
                workbook.setSelectedTab(0);
            } catch (IOException ioe) {
                log.error("GenerateXls.java :: generateXlsFile() :: Can't open workbook from input template {}", ioe);
            }
        }

        log.debug("GenerateXls :: setupWorkbookConfiguration :: Configurtion of workbook completed");
    }

    private List initExcelSheets() {
        log.debug("GenerateXIs :: initExcelSheets :: Method Entered");
        List<Sheet> sheets = new ArrayList<>();

        int numberOfDefinedSheets = workbook.getNumberOfSheets();
        // We start index at 3 in order to avoid Computers, Bus. App. & Net Devices Sheets
        for (int sheetIndex = 3; sheetIndex < numberOfDefinedSheets; sheetIndex++) {
            sheets.add(workbook.getSheetAt(sheetIndex));
        }

        log.debug("GenerateXIs :: initExcelSheets :: Method Returned Sheets numner of {}", sheets.size());
        return sheets;
    }

    private Map initMappedMethodNames() {
        log.debug("GenerateXIs :: initMappedMethodNames :: Method Entered");
        Map<Integer, String> methodNames = new HashMap<>();

        methodNames.put(0, "getCpus");
        methodNames.put(1, "getDiskDevice");
        methodNames.put(2, "getFibreChannelHba");
        methodNames.put(3, "getFileSystem");
        methodNames.put(4, "getHardwareBoard");
        methodNames.put(5, "getIbmLParProfiles");
        methodNames.put(6, "getIbmProcessorPool");
        methodNames.put(7, "getUcmdbInterface");
        methodNames.put(8, "getIpAddress");
        methodNames.put(9, "getIpServiceEndpoint");
        methodNames.put(10, "getIpSubnet");
        methodNames.put(11, "getLogicalVolume");
        methodNames.put(12, "getNetworkAdapter");
        methodNames.put(13, "getOracle");
        methodNames.put(14, "getOracleTnsListener");
        methodNames.put(15, "getOsUSer");
        methodNames.put(16, "getPhysicalVolume");
        methodNames.put(17, "getVirtualizationLayerSoftware");
        methodNames.put(18, "getVolumeGroup");
        methodNames.put(19, "getWeblogicAS");

        log.debug("GenerateXIs :: initMappedMethodNames :: Method Returned method names size of {}", methodNames.keySet().size());
        return methodNames;
    }

    private void fillSheets(List<Sheet> sheets, Map<Integer, String> methodNames, List<Map<Integer, String>> sheetColumns) {
        log.debug("GenerateXIs :: fillSheets :: Method Entered to put data in Excel Workbook");

        // Helper Index in order to get Method Names & Sheet Columns
        int index = 0;
        for (Sheet sheet : sheets) {
            addRowsAndMergeCells(sheet, methodNames.get(index));
            setSheetRows(sheet, methodNames.get(index), sheetColumns.get(index));
            index++;
        }

        // First we fill Sheets for Computers, Business Applications & Net Devices
        fillCustomSheets(sheetColumns ,index);

        // Auto Width Columns
        autoWidthWorkbookColumns();

        log.debug("GenerateXIs :: fillSheets :: Method Added Data to Excel Workbook");
    }

    private void fillCustomSheets(List<Map<Integer, String>> sheetColumns, int index) {
        Sheet sheetBusinessApplications = workbook.getSheetAt(0);
        Sheet sheetNetDevices = workbook.getSheetAt(1);
        Sheet sheetComputer = workbook.getSheetAt(2);

        fillSheetComputer(sheetComputer, sheetColumns.get(index));
        fillSheetBusinessApplicationIfBaExists(sheetBusinessApplications, sheetColumns.get(++index));
        fillSheetNetDevicesIfExists(sheetNetDevices, sheetColumns.get(++index));
    }

    private void autoWidthWorkbookColumns() {
        log.debug("GenerateXls :: autoWidthWorkbookColumns :: Method Entered");

        for (int sheetIndex = 0; sheetIndex < workbook.getNumberOfSheets(); sheetIndex++) {
            Sheet currentSheet = workbook.getSheetAt(sheetIndex);

            if (currentSheet.getPhysicalNumberOfRows() > 0) {
                Row row = currentSheet.getRow(0);
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    int columnIndex = cell.getColumnIndex();
                    currentSheet.autoSizeColumn(columnIndex);
                }
            }
        }

        log.debug("GenerateXls :: autoWidthWorkbookColumns :: Method Returned");
    }

    private byte[] writeDataToFile() {
        log.debug("GenerateXIs :: writeDataToFile :: Method Entered");

        ByteOutputStream byteStream = null;
        byte[] byteArray = new byte[0];
        try {
            byteStream = new ByteOutputStream();
            workbook.write(byteStream);
            byteArray = byteStream.getBytes();
        } catch (IOException e) {
            log.error("GenerateXls.java ::: Error in generateXlsFile()");
        }
        finally {
            try {
                byteStream.close();
                workbook.close();
            }
            catch (NullPointerException npe) {
                log.error("GenerateXls :: writeDataToFile :: Streams may have not open yet");
            }
            catch (IOException ioe) {
                log.error("GenerateXls :: writeDataToFile :: Error while trying to close streams");
            }
        }

        log.debug("GenerateXIs :: writeDataToFile :: Method Returned byte array of Excel");
        return byteArray;
    }

    private CellStyle setCellStyle() {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);

        return cellStyle;
    }

    private List generateSheetColumns() {

        Map<Integer, String> columnsSheetNetDevices = new HashMap<>();
        columnsSheetNetDevices.put(2, "display_label");
        columnsSheetNetDevices.put(3, "primary_dns_name");
        columnsSheetNetDevices.put(4, "host_isvirtual");
        columnsSheetNetDevices.put(5, "root_class");
        columnsSheetNetDevices.put(6, "snmp_sys_name");
        columnsSheetNetDevices.put(7, "category");
        columnsSheetNetDevices.put(8, "data_note");
        columnsSheetNetDevices.put(9, "domain");
        columnsSheetNetDevices.put(10, "Node_Environment");
        columnsSheetNetDevices.put(11, "role");
        columnsSheetNetDevices.put(12, "TenantOwner");
        columnsSheetNetDevices.put(13, "TenantsUses");
        columnsSheetNetDevices.put(14, "create_time");
        columnsSheetNetDevices.put(15, "os_description");
        columnsSheetNetDevices.put(16, "name");
        columnsSheetNetDevices.put(17, "discovered_os_name");
        columnsSheetNetDevices.put(18, "discovered_description");
        columnsSheetNetDevices.put(19, "description");
        columnsSheetNetDevices.put(20, "data_adminstate");
        columnsSheetNetDevices.put(21, "data_allow_auto_discovery");
        columnsSheetNetDevices.put(22, "data_changecorrstate");
        columnsSheetNetDevices.put(23, "data_changeisnew");
        columnsSheetNetDevices.put(24, "data_changestate");
        columnsSheetNetDevices.put(25, "data_operationcorrstate");
        columnsSheetNetDevices.put(26, "data_operationisnew");
        columnsSheetNetDevices.put(27, "data_operationstate");
        columnsSheetNetDevices.put(28, "data_source");
        columnsSheetNetDevices.put(29, "data_testcorrstate");
        columnsSheetNetDevices.put(30, "data_testisnew");
        columnsSheetNetDevices.put(31, "data_teststate");
        columnsSheetNetDevices.put(32, "data_updated_by");
        columnsSheetNetDevices.put(33, "default_gateway_ip_address_type");
        columnsSheetNetDevices.put(34, "discovered_os_vendor");
        columnsSheetNetDevices.put(35, "extended_os_family");
        columnsSheetNetDevices.put(36, "global_id");
        columnsSheetNetDevices.put(37, "host_iscomplete");
        columnsSheetNetDevices.put(38, "host_isroute");
        columnsSheetNetDevices.put(39, "host_key");
        columnsSheetNetDevices.put(40, "host_osaccuracy");
        columnsSheetNetDevices.put(41, "is_save_persistency");
        columnsSheetNetDevices.put(42, "last_modified_time");
        columnsSheetNetDevices.put(43, "lic_type_udf");
        columnsSheetNetDevices.put(44, "lic_type_udi");
        columnsSheetNetDevices.put(45, "node_model");
        columnsSheetNetDevices.put(46, "node_role");
        columnsSheetNetDevices.put(47, "os_vendor");
        columnsSheetNetDevices.put(48, "root_actualdeletetime");
        columnsSheetNetDevices.put(49, "root_candidatefordeletetime");
        columnsSheetNetDevices.put(50, "root_enableageing");
        columnsSheetNetDevices.put(51, "root_iscandidatefordeletion");
        columnsSheetNetDevices.put(52, "root_lastaccesstime");
        columnsSheetNetDevices.put(53, "serial_number");
        columnsSheetNetDevices.put(54, "sys_object_id");
        columnsSheetNetDevices.put(55, "track_changes");
        columnsSheetNetDevices.put(56, "vendor");
        columnsSheetNetDevices.put(57, "default_gateway_ip_address");
        columnsSheetNetDevices.put(58, "discovered_model");
        columnsSheetNetDevices.put(59, "discovered_vendor");
        columnsSheetNetDevices.put(60, "host_nnm_uid");
        columnsSheetNetDevices.put(61, "node_family");
        columnsSheetNetDevices.put(62, "contextmenu");
        columnsSheetNetDevices.put(63, "discovered_os_version");

        Map<Integer, String> columnsSheetBusinessApplication = new HashMap<>();
        columnsSheetBusinessApplication.put(2, "display_label");
        columnsSheetBusinessApplication.put(3, "app_id");
        columnsSheetBusinessApplication.put(4, "business_criticality");
        columnsSheetBusinessApplication.put(5, "calculated_id");
        columnsSheetBusinessApplication.put(6, "city");
        columnsSheetBusinessApplication.put(7, "contextmenu");
        columnsSheetBusinessApplication.put(8, "country");
        columnsSheetBusinessApplication.put(9, "create_time");
        columnsSheetBusinessApplication.put(10, "data_adminstate");
        columnsSheetBusinessApplication.put(11, "data_allow_auto_discovery");
        columnsSheetBusinessApplication.put(12, "data_changecorrstate");
        columnsSheetBusinessApplication.put(13, "data_changeisnew");
        columnsSheetBusinessApplication.put(14, "data_changestate");
        columnsSheetBusinessApplication.put(15, "data_externalid");
        columnsSheetBusinessApplication.put(16, "data_note");
        columnsSheetBusinessApplication.put(17, "data_operationcorrstate");
        columnsSheetBusinessApplication.put(18, "data_operationisnew");
        columnsSheetBusinessApplication.put(19, "data_operationstate");
        columnsSheetBusinessApplication.put(20, "data_origin");
        columnsSheetBusinessApplication.put(21, "data_source");
        columnsSheetBusinessApplication.put(22, "data_testcorrstate");
        columnsSheetBusinessApplication.put(23, "data_testisnew");
        columnsSheetBusinessApplication.put(24, "data_teststate");
        columnsSheetBusinessApplication.put(25, "data_updated_by");
        columnsSheetBusinessApplication.put(26, "deploy_type");
        columnsSheetBusinessApplication.put(27, "description");
        columnsSheetBusinessApplication.put(28, "digest");
        columnsSheetBusinessApplication.put(29, "document_list");
        columnsSheetBusinessApplication.put(30, "domain");
        columnsSheetBusinessApplication.put(31, "entrance_urls");
        columnsSheetBusinessApplication.put(32, "global_id");
        columnsSheetBusinessApplication.put(33, "is_save_persistency");
        columnsSheetBusinessApplication.put(34, "isBulkModeled");
        columnsSheetBusinessApplication.put(35, "last_modified_time");
        columnsSheetBusinessApplication.put(36, "name");
        columnsSheetBusinessApplication.put(37, "root_actualdeletetime");
        columnsSheetBusinessApplication.put(38, "root_actualdeletionperiod");
        columnsSheetBusinessApplication.put(39, "root_candidatefordeletetime");
        columnsSheetBusinessApplication.put(40, "root_class");
        columnsSheetBusinessApplication.put(41, "root_container");
        columnsSheetBusinessApplication.put(42, "root_deletioncandidateperiod");
        columnsSheetBusinessApplication.put(43, "root_enableageing");
        columnsSheetBusinessApplication.put(44, "root_iscandidatefordeletion");
        columnsSheetBusinessApplication.put(45, "root_lastaccesstime");
        columnsSheetBusinessApplication.put(46, "root_system");
        columnsSheetBusinessApplication.put(47, "ServiceClass");
        columnsSheetBusinessApplication.put(48, "state");
        columnsSheetBusinessApplication.put(49, "tags");
        columnsSheetBusinessApplication.put(50, "TenantOwner");
        columnsSheetBusinessApplication.put(51, "TenantsUses");
        columnsSheetBusinessApplication.put(52, "track_changes");
        columnsSheetBusinessApplication.put(53, "user_label");

        Map<Integer, String> columnsSheetComputer = new HashMap<>();
        columnsSheetComputer.put(2, "display_label");
        columnsSheetComputer.put(3, "description");
        columnsSheetComputer.put(4, "root_class");
        columnsSheetComputer.put(5, "bios_asset_tag");
        columnsSheetComputer.put(6, "application_name");
        columnsSheetComputer.put(7, "category");
        columnsSheetComputer.put(8, "discovered_location");
        columnsSheetComputer.put(9, "domain");
        columnsSheetComputer.put(10, "functionality");
        columnsSheetComputer.put(11, "host_isvirtual");
        columnsSheetComputer.put(12, "Node_Environment");
        columnsSheetComputer.put(13, "role");
        columnsSheetComputer.put(14, "ServiceClass");
        columnsSheetComputer.put(15, "TenantOwner");
        columnsSheetComputer.put(16, "TenantsUses");
        columnsSheetComputer.put(17, "primary_dns_name");
        columnsSheetComputer.put(18, "os_description");
        columnsSheetComputer.put(19, "net_bios_name");
        columnsSheetComputer.put(20, "name");
        columnsSheetComputer.put(21, "domain_name");
        columnsSheetComputer.put(22, "discovered_os_name");
        columnsSheetComputer.put(23, "bios_date");
        columnsSheetComputer.put(24, "bios_source");
        columnsSheetComputer.put(25, "bios_uuid");
        columnsSheetComputer.put(26, "bios_version");
        columnsSheetComputer.put(27, "chassis_type");
        columnsSheetComputer.put(28, "contextmenu");
        columnsSheetComputer.put(29, "create_time");
        columnsSheetComputer.put(30, "data_adminstate");
        columnsSheetComputer.put(31, "data_allow_auto_discovery");
        columnsSheetComputer.put(32, "data_changecorrstate");
        columnsSheetComputer.put(33, "data_changeisnew");
        columnsSheetComputer.put(34, "data_changestate");
        columnsSheetComputer.put(35, "data_operationcorrstate");
        columnsSheetComputer.put(36, "data_operationisnew");
        columnsSheetComputer.put(37, "data_operationstate");
        columnsSheetComputer.put(38, "data_source");
        columnsSheetComputer.put(39, "data_testcorrstate");
        columnsSheetComputer.put(40, "data_testisnew");
        columnsSheetComputer.put(41, "data_teststate");
        columnsSheetComputer.put(42, "data_updated_by");
        columnsSheetComputer.put(43, "default_gateway_ip_address");
        columnsSheetComputer.put(44, "default_gateway_ip_address_type");
        columnsSheetComputer.put(45, "discovered_model");
        columnsSheetComputer.put(46, "discovered_os_vendor");
        columnsSheetComputer.put(47, "discovered_os_version");
        columnsSheetComputer.put(48, "discovered_vendor");
        columnsSheetComputer.put(49, "dns_servers");
        columnsSheetComputer.put(50, "global_id");
        columnsSheetComputer.put(51, "host_iscomplete");
        columnsSheetComputer.put(52, "host_key");
        columnsSheetComputer.put(53, "host_last_boot_time");
        columnsSheetComputer.put(54, "host_osinstalltype");
        columnsSheetComputer.put(55, "host_osrelease");
        columnsSheetComputer.put(56, "is_save_persistency");
        columnsSheetComputer.put(57, "last_modified_time");
        columnsSheetComputer.put(58, "lic_type_udf");
        columnsSheetComputer.put(59, "lic_type_udi");
        columnsSheetComputer.put(60, "memory_size");
        columnsSheetComputer.put(61, "node_role");
        columnsSheetComputer.put(62, "os_family");
        columnsSheetComputer.put(63, "os_vendor");
        columnsSheetComputer.put(64, "primary_ip_address");
        columnsSheetComputer.put(65, "primary_mac_address");
        columnsSheetComputer.put(66, "processor_family");
        columnsSheetComputer.put(67, "root_actualdeletetime");
        columnsSheetComputer.put(68, "root_candidatefordeletetime");
        columnsSheetComputer.put(69, "root_enableageing");
        columnsSheetComputer.put(70, "root_iscandidatefordeletion");
        columnsSheetComputer.put(71, "root_lastaccesstime");
        columnsSheetComputer.put(72, "serial_number");
        columnsSheetComputer.put(73, "swap_memory_size");
        columnsSheetComputer.put(74, "track_changes");
        columnsSheetComputer.put(75, "user_label");
        columnsSheetComputer.put(76, "extended_os_family");
        columnsSheetComputer.put(77, "node_model");
        columnsSheetComputer.put(78, "vendor");

        Map<Integer, String> columnsSheetCpu = new HashMap<>();
        columnsSheetCpu.put(3, "display_label");
        columnsSheetCpu.put(4, "root_class");
        columnsSheetCpu.put(5, "cpu_vendor");
        columnsSheetCpu.put(6, "core_number");
        columnsSheetCpu.put(7, "cpu_specifier");
        columnsSheetCpu.put(8, "cpu_clock_speed");
        columnsSheetCpu.put(9, "cpu_id");
        columnsSheetCpu.put(10, "isvirtual");
        columnsSheetCpu.put(11, "root_lastaccesstime");
        columnsSheetCpu.put(12, "last_modified_time");
        columnsSheetCpu.put(13, "logical_cpu_count");
        columnsSheetCpu.put(14, "name");
        columnsSheetCpu.put(15, "TenantOwner");

        Map<Integer, String> columnsSheetDiskDevice = new HashMap<>();
        columnsSheetDiskDevice.put(3, "display_label");
        columnsSheetDiskDevice.put(4, "root_class");
        columnsSheetDiskDevice.put(5, "disk_type");
        columnsSheetDiskDevice.put(6, "isvirtual");
        columnsSheetDiskDevice.put(7, "root_lastaccesstime");
        columnsSheetDiskDevice.put(8, "last_modified_time");
        columnsSheetDiskDevice.put(9, "name");
        columnsSheetDiskDevice.put(10, "TenantOwner");

        Map<Integer, String> columnsSheetFibreChannel = new HashMap<>();
        columnsSheetFibreChannel.put(3, "display_label");
        columnsSheetFibreChannel.put(4, "root_class");
        columnsSheetFibreChannel.put(5, "description");
        columnsSheetFibreChannel.put(6, "fchba_model");
        columnsSheetFibreChannel.put(7, "fchba_vendor");
        columnsSheetFibreChannel.put(8, "isvirtual");
        columnsSheetFibreChannel.put(9, "root_lastaccesstime");
        columnsSheetFibreChannel.put(10, "last_modified_time");
        columnsSheetFibreChannel.put(11, "name");
        columnsSheetFibreChannel.put(12, "fchba_wwn");
        columnsSheetFibreChannel.put(13, "TenantOwner");

        Map<Integer, String> columnsSheetFileSystem = new HashMap<>();
        columnsSheetFileSystem.put(3, "display_label");
        columnsSheetFileSystem.put(4, "root_class");
        columnsSheetFileSystem.put(5, "disk_size");
        columnsSheetFileSystem.put(6, "filesystem_type");
        columnsSheetFileSystem.put(7, "free_space");
        columnsSheetFileSystem.put(8, "root_lastaccesstime");
        columnsSheetFileSystem.put(9, "last_modified_time");
        columnsSheetFileSystem.put(10, "mount_device");
        columnsSheetFileSystem.put(11, "mount_point");
        columnsSheetFileSystem.put(12, "name");
        columnsSheetFileSystem.put(13, "TenantOwner");
        columnsSheetFileSystem.put(14, ""); //---> Storage Type???

        Map<Integer, String> columnsSheetHardwareBoard = new HashMap<>();
        columnsSheetHardwareBoard.put(3, "display_label");
        columnsSheetHardwareBoard.put(4, "hardware_board_index");
        columnsSheetHardwareBoard.put(5, "bus");
        columnsSheetHardwareBoard.put(6, "root_class");
        columnsSheetHardwareBoard.put(7, "isvirtual");
        columnsSheetHardwareBoard.put(8, "root_lastaccesstime");
        columnsSheetHardwareBoard.put(9, "last_modified_time");
        columnsSheetHardwareBoard.put(10, "name");
        columnsSheetHardwareBoard.put(11, "TenantOwner");

        Map<Integer, String> columnsSheetIbmLPar = new HashMap<>();
        columnsSheetIbmLPar.put(3, "display_label");
        columnsSheetIbmLPar.put(4, "active_cpu_in_pool");
        columnsSheetIbmLPar.put(5, "active_physical_cpu");
        columnsSheetIbmLPar.put(6, "root_class");
        columnsSheetIbmLPar.put(7, " "); // ---> CPU mode???
        columnsSheetIbmLPar.put(8, "conn_monitoring");
        columnsSheetIbmLPar.put(9, "desired_proc_units");
        columnsSheetIbmLPar.put(10, "desired_num_huge_pages");
        columnsSheetIbmLPar.put(11, " "); // ---> desired physical cpus ???
        columnsSheetIbmLPar.put(12, "isvirtual");
        columnsSheetIbmLPar.put(13, " "); // ---> lpar profile name ???
        columnsSheetIbmLPar.put(14, "boot_mode"); // ---> lpar boot mode ???
        columnsSheetIbmLPar.put(15, "lpar_id");
        columnsSheetIbmLPar.put(16, "lpar_name");
        columnsSheetIbmLPar.put(17, "auto_start"); // ---> lpar profile autostart???
        columnsSheetIbmLPar.put(18, "lpar_io_pool_ids");
        columnsSheetIbmLPar.put(19, "serial_number");
        columnsSheetIbmLPar.put(20, "default_profile_name");
        columnsSheetIbmLPar.put(21, "lpar_state");
        columnsSheetIbmLPar.put(22, "lpar_type");
        columnsSheetIbmLPar.put(23, "root_lastaccesstime");
        columnsSheetIbmLPar.put(24, "last_modified_time");
        columnsSheetIbmLPar.put(25, "lpar_mode");
        columnsSheetIbmLPar.put(26, "max_proc_units");
        columnsSheetIbmLPar.put(27, "max_num_huge_pages");
        columnsSheetIbmLPar.put(28, "max_procs");
        columnsSheetIbmLPar.put(29, "max_virtual_slots");
        columnsSheetIbmLPar.put(30, " "); // ---> maximum physical cpus ???
        columnsSheetIbmLPar.put(31, "max_mem");
        columnsSheetIbmLPar.put(32, " "); // ---> memory requested ???
        columnsSheetIbmLPar.put(33, "min_mem");
        columnsSheetIbmLPar.put(34, "min_proc_units");
        columnsSheetIbmLPar.put(35, "min_num_huge_pages");
        columnsSheetIbmLPar.put(36, "name");
        columnsSheetIbmLPar.put(37, "online_virtual_cpu");
        columnsSheetIbmLPar.put(38, "TenantOwner");
        columnsSheetIbmLPar.put(39, "power_ctrl_lpar_ids");
        columnsSheetIbmLPar.put(40, "redundant_err_path_reporting");
        columnsSheetIbmLPar.put(41, "shared_pool_id");
        columnsSheetIbmLPar.put(42, "sharing_mode");
        columnsSheetIbmLPar.put(43, "uncap_weight");
        columnsSheetIbmLPar.put(44, "virtual_serial_adapters");
        columnsSheetIbmLPar.put(45, "work_group_id");

        Map<Integer, String> columnsSheetIbmProcPool = new HashMap<>();
        columnsSheetIbmProcPool.put(3, "display_label");
        columnsSheetIbmProcPool.put(4, "root_class");
        columnsSheetIbmProcPool.put(5, " "); // ---> cpu pool available physical cpus
        columnsSheetIbmProcPool.put(6, " "); // ---> cpu pool configurable physical cpus
        columnsSheetIbmProcPool.put(7, " "); // ---> cpu pool pending available physical cpus
        columnsSheetIbmProcPool.put(8, "root_lastaccesstime");
        columnsSheetIbmProcPool.put(9, "last_modified_time");
        columnsSheetIbmProcPool.put(10, "name");
        columnsSheetIbmProcPool.put(11, "TenantOwner");
        columnsSheetIbmProcPool.put(12, " "); // ---> pool id

        Map<Integer, String> columnsSheetInterface = new HashMap<>();
        columnsSheetInterface.put(3, "display_label");
        columnsSheetInterface.put(4, "root_class");
        columnsSheetInterface.put(5, "interface_alias");
        columnsSheetInterface.put(6, "interface_description");
        columnsSheetInterface.put(7, " "); // ---> interface gateways
        columnsSheetInterface.put(8, "interface_index");
        columnsSheetInterface.put(9, "interface_name");
        columnsSheetInterface.put(10, "interface_role");
        columnsSheetInterface.put(11, "interface_speed");
        columnsSheetInterface.put(12, "interface_type");
        columnsSheetInterface.put(13, "isPseudo");
        columnsSheetInterface.put(14, "isvirtual");
        columnsSheetInterface.put(15, "root_lastaccesstime");
        columnsSheetInterface.put(16, "last_modified_time");
        columnsSheetInterface.put(17, "mac_address");

        Map<Integer, String> columnsSheetIpAddress = new HashMap<>();
        columnsSheetIpAddress.put(3, "display_label");
        columnsSheetIpAddress.put(4, "authoritative_dns_name");
        columnsSheetIpAddress.put(5, "root_class");
        columnsSheetIpAddress.put(6, "ip_lease_time");
        columnsSheetIpAddress.put(7, "ip_netaddr");
        columnsSheetIpAddress.put(8, "ip_netclass");
        columnsSheetIpAddress.put(9, "ip_netmask");
        columnsSheetIpAddress.put(10, "ip_nettype");
        columnsSheetIpAddress.put(11, "ip_address_value");
        columnsSheetIpAddress.put(12, "root_lastaccesstime");
        columnsSheetIpAddress.put(13, "last_modified_time");
        columnsSheetIpAddress.put(14, "name");
        columnsSheetIpAddress.put(15, "TenantOwner");
        columnsSheetIpAddress.put(16, "routing_domain");

        Map<Integer, String> columnsSheetIpServiceEndpoint = new HashMap<>();
        columnsSheetIpServiceEndpoint.put(3, "display_label");
        columnsSheetIpServiceEndpoint.put(4, "bound_to_ip_address");
        columnsSheetIpServiceEndpoint.put(5, "root_class");
        columnsSheetIpServiceEndpoint.put(6, "ip_service_name");
        columnsSheetIpServiceEndpoint.put(7, "root_lastaccesstime");
        columnsSheetIpServiceEndpoint.put(8, "last_modified_time");
        columnsSheetIpServiceEndpoint.put(9, "network_port_number");
        columnsSheetIpServiceEndpoint.put(10, "port_type");
        columnsSheetIpServiceEndpoint.put(11, ""); // ---> service address???
        columnsSheetIpServiceEndpoint.put(12, "service_names");

        Map<Integer, String> columnsSheetIpSubnet = new HashMap<>();
        columnsSheetIpSubnet.put(3, "display_label");
        columnsSheetIpSubnet.put(4, "root_class");
        columnsSheetIpSubnet.put(5, "ip_address_type");
        columnsSheetIpSubnet.put(6, "ip_address_value");
        columnsSheetIpSubnet.put(7, "ip_prefix_length");
        columnsSheetIpSubnet.put(8, "ip_ismanaged");
        columnsSheetIpSubnet.put(9, "root_lastaccesstime");
        columnsSheetIpSubnet.put(10, "last_modified_time");
        columnsSheetIpSubnet.put(11, "name");
        columnsSheetIpSubnet.put(12, "network_broadcastaddress");
        columnsSheetIpSubnet.put(13, "network_netclass");
        columnsSheetIpSubnet.put(14, "network_netmask");
        columnsSheetIpSubnet.put(15, "TenantOwner");
        columnsSheetIpSubnet.put(16, "routing_domain");

        Map<Integer, String> columnsSheetLogicalVolume = new HashMap<>();
        columnsSheetLogicalVolume.put(3, "display_label");
        columnsSheetLogicalVolume.put(4, "root_class");
        columnsSheetLogicalVolume.put(5, "free_space");
        columnsSheetLogicalVolume.put(6, "root_lastaccesstime");
        columnsSheetLogicalVolume.put(7, "last_modified_time");
        columnsSheetLogicalVolume.put(8, " "); // ---> logical volume file system type
        columnsSheetLogicalVolume.put(9, "logicalvolume_size");
        columnsSheetLogicalVolume.put(10, "logicalvolume_status");
        columnsSheetLogicalVolume.put(11, "name");

        Map<Integer, String> columnsSheetNetworkAdapter = new HashMap<>();
        columnsSheetNetworkAdapter.put(3, "display_label");
        columnsSheetNetworkAdapter.put(4, "board_index");
        columnsSheetNetworkAdapter.put(5, "root_class");
        columnsSheetNetworkAdapter.put(6, "root_lastaccesstime");
        columnsSheetNetworkAdapter.put(7, "last_modified_time");
        columnsSheetNetworkAdapter.put(8, "name");
        columnsSheetNetworkAdapter.put(9, "network_adapter_type");

        Map<Integer, String> columnsSheetOracle = new HashMap<>();
        columnsSheetOracle.put(3, "display_label");
        columnsSheetOracle.put(4, "application_category");
        columnsSheetOracle.put(5, "application_ip");
        columnsSheetOracle.put(6, "application_ip_type");
        columnsSheetOracle.put(7, "application_username");
        columnsSheetOracle.put(8, "application_version");
        columnsSheetOracle.put(9, "root_class");
        columnsSheetOracle.put(10, "discovered_product_name");
        columnsSheetOracle.put(11, "oracle_filetype");
        columnsSheetOracle.put(12, "root_lastaccesstime");
        columnsSheetOracle.put(13, "last_modified_time");
        columnsSheetOracle.put(14, "name");
        columnsSheetOracle.put(15, "TenantOwner");
        columnsSheetOracle.put(16, "product_name");
        columnsSheetOracle.put(17, " "); // ---> type
        columnsSheetOracle.put(18, "vendor"); // ---> oracle vendor
        columnsSheetOracle.put(19, "version");

        Map<Integer, String> columnsSheetOracleTnsListener = new HashMap<>();
        columnsSheetOracleTnsListener.put(3, "display_label");
        columnsSheetOracleTnsListener.put(4, "application_category");
        columnsSheetOracleTnsListener.put(5, "application_ip");
        columnsSheetOracleTnsListener.put(6, "application_ip_type");
        columnsSheetOracleTnsListener.put(7, "root_class");
        columnsSheetOracleTnsListener.put(8, "discovered_product_name");
        columnsSheetOracleTnsListener.put(9, "root_lastaccesstime");
        columnsSheetOracleTnsListener.put(10, "last_modified_time");
        columnsSheetOracleTnsListener.put(11, "name");
        columnsSheetOracleTnsListener.put(12, "TenantOwner");
        columnsSheetOracleTnsListener.put(13, "vendor");
        columnsSheetOracleTnsListener.put(14, "version");

        Map<Integer, String> columnsSheetOsUser = new HashMap<>();
        columnsSheetOsUser.put(3, "display_label");
        columnsSheetOsUser.put(4, "root_class");
        columnsSheetOsUser.put(5, "homedir");
        columnsSheetOsUser.put(6, "root_lastaccesstime");
        columnsSheetOsUser.put(7, "last_modified_time");
        columnsSheetOsUser.put(8, "name");
        columnsSheetOsUser.put(9, " "); // ---> note
        columnsSheetOsUser.put(10, "group_id");
        columnsSheetOsUser.put(11, "user_id");

        Map<Integer, String> columnsSheetPhysicalVolume = new HashMap<>();
        columnsSheetPhysicalVolume.put(3, "display_label");
        columnsSheetPhysicalVolume.put(4, "root_class");
        columnsSheetPhysicalVolume.put(5, "root_lastaccesstime");
        columnsSheetPhysicalVolume.put(6, "last_modified_time");
        columnsSheetPhysicalVolume.put(7, "name");
        columnsSheetPhysicalVolume.put(8, "volume_id");
        columnsSheetPhysicalVolume.put(9, "volume_size");

        Map<Integer, String> columnsSheetVirtualizationLayer = new HashMap<>();
        columnsSheetVirtualizationLayer.put(3, "display_label");
        columnsSheetVirtualizationLayer.put(4, "application_ip_type");
        columnsSheetVirtualizationLayer.put(5, "root_class");
        columnsSheetVirtualizationLayer.put(6, "discovered_product_name");
        columnsSheetVirtualizationLayer.put(7, "maintenance_mode");
        columnsSheetVirtualizationLayer.put(8, "root_lastaccesstime");
        columnsSheetVirtualizationLayer.put(9, "last_modified_time");
        columnsSheetVirtualizationLayer.put(10, "vmotion_enabled");
        columnsSheetVirtualizationLayer.put(11, "vendor");

        Map<Integer, String> columnsSheetVolumeGroup = new HashMap<>();
        columnsSheetVolumeGroup.put(3, "display_label");
        columnsSheetVolumeGroup.put(4, "root_class");
        columnsSheetVolumeGroup.put(5, "root_lastaccesstime");
        columnsSheetVolumeGroup.put(6, "last_modified_time");
        columnsSheetVolumeGroup.put(7, "name");
        columnsSheetVolumeGroup.put(8, "volume_group_id");
        columnsSheetVolumeGroup.put(9, " "); // ---> group size???

        Map<Integer, String> columnsSheetWeblogicAs = new HashMap<>();
        columnsSheetWeblogicAs.put(3, "display_label");
        columnsSheetWeblogicAs.put(4, "application_category");
        columnsSheetWeblogicAs.put(5, "application_ip");
        columnsSheetWeblogicAs.put(6, "application_ip_type");
        columnsSheetWeblogicAs.put(7, "application_server_type");
        columnsSheetWeblogicAs.put(8, "root_class");
        columnsSheetWeblogicAs.put(9, "root_container_name");
        columnsSheetWeblogicAs.put(10, "discovered_product_name");
        columnsSheetWeblogicAs.put(11, "root_lastaccesstime");
        columnsSheetWeblogicAs.put(12, "last_modified_time");
        columnsSheetWeblogicAs.put(13, "name");
        columnsSheetWeblogicAs.put(14, "product_name");
        columnsSheetWeblogicAs.put(15, "j2eeserver_protocol"); // ---> protocol???
        columnsSheetWeblogicAs.put(16, "j2eeserver_vendor");

        List<Map<Integer, String>> listToReturn = new ArrayList<>();
        listToReturn.addAll(Arrays.asList(columnsSheetCpu, columnsSheetDiskDevice, columnsSheetFibreChannel,
                columnsSheetFileSystem, columnsSheetHardwareBoard, columnsSheetIbmLPar, columnsSheetIbmProcPool,
                columnsSheetInterface, columnsSheetIpAddress, columnsSheetIpServiceEndpoint,
                columnsSheetIpSubnet, columnsSheetLogicalVolume, columnsSheetNetworkAdapter, columnsSheetOracle,
                columnsSheetOracleTnsListener, columnsSheetOsUser, columnsSheetPhysicalVolume,
                columnsSheetVirtualizationLayer, columnsSheetVolumeGroup, columnsSheetWeblogicAs,
                columnsSheetComputer, columnsSheetBusinessApplication, columnsSheetNetDevices));

        return listToReturn;
    }

    // Helper Methods for Custom Sheets
    private void fillSheetComputer(Sheet sheet, Map<Integer, String> map) {
        log.debug("GenerateXls.java :: fillSheetComputer :: method entered");
        HashMap<String, String> propertiesMap = new HashMap<>();

        List<Ci> filteredCiCollectionsForComputers =
                bs.getCiCollection().stream()
                        // Exclude Ci Collections that contain Business Applications or Net Devices
                        .filter(ciCollection -> ciCollection.getBusinessApplications() == null)
                        .filter(ciCollection -> ciCollection.getNetdevices() == null)
                        .collect(Collectors.toList());

        try {
            for (Ci ci : filteredCiCollectionsForComputers) {
                if (!ci.getComputers().isEmpty()) {
                    for (Computer comp : ci.getComputers()) {

                        for (Map.Entry<String, String> entry : comp.getProperties().entrySet()) {
                            propertiesMap.put(entry.getKey(), entry.getValue());
                        }

                    }
                }
            }
        } catch (Exception e) {
            log.error("GenerateXls.java ::: error in fillSheetComputer()");
        }


        int linesToAdd = 0;

        for (Ci ci : filteredCiCollectionsForComputers) {
            linesToAdd += ci.getComputers().size();
        }

        CellStyle hlink_style = workbook.createCellStyle();
        hlink_style.setAlignment(HorizontalAlignment.CENTER);
        createRowsAndCells(linesToAdd, map.size()+2, sheet, hlink_style);
        mergeCells(sheet, linesToAdd);
        log.debug("GenerateXls.java :: fillSheetComputer :: After mergeCells");
        int cnt, start2 = 1;
        Row r;
        for (Ci ci : filteredCiCollectionsForComputers) {
            cnt = 0;
            if (ci.getComputers().size() == 0) cnt += 1;
            else cnt += ci.getComputers().size();
            log.debug("GenerateXls.java :: fillSheetComputer :: trying to merge area of size: {}", start2 - start2 + cnt - 1);
            if (cnt > 1) {
                sheet.addMergedRegion(new CellRangeAddress(start2, start2 +
                        cnt - 1, 1, 1));
            }
            r = sheet.getRow(start2);
            r.getCell(1).setCellValue(ci.getProperties().get("display_label"));
            r.getCell(1).getCellStyle().setVerticalAlignment(VerticalAlignment.TOP);
            start2 = start2 + cnt;
        }

        cnt = 1;
        for (Ci ci : filteredCiCollectionsForComputers) {
            for (Computer comp : ci.getComputers()) {
                Row row = sheet.getRow(cnt);
                Set<String> keys = comp.getProperties().keySet();
                int i = 3;
                for (Map.Entry<String, String> entry : propertiesMap.entrySet()) {
                    if (comp.getProperties().containsKey(entry.getKey())) {
                        for (String key : keys) {
                            for (Map.Entry<Integer, String> entry2 : map.entrySet()) {
                                if (entry2.getValue().equals(key)) {
                                    row.getCell(entry2.getKey()).setCellValue(comp.getProperties().get(key));
                                }
                            }
                        }
                    } else {
                        row.getCell(i).setCellValue(" ");
                    }

                }
                cnt++;
            }
        }
    }

    private void fillSheetBusinessApplicationIfBaExists(Sheet sheetBusinessApplication, Map<Integer, String> businessApplicationProperties) {
        long businessApplicationsCount =
                bs.getCiCollection().stream()
                        .filter((business) -> business.getBusinessApplications() != null)
                        .count();

        if (businessApplicationsCount == 0) {
            // Remove Sheet from Template
            workbook.removeSheetAt(workbook.getSheetIndex("Business Applications"));
        }
        else {
            // Fill Sheet with Data
            fillSheetBusinessApplication(sheetBusinessApplication, businessApplicationProperties);
        }

    }

    private void fillSheetBusinessApplication(Sheet sheet, Map<Integer, String> map) {
        log.debug("GenerateXls.java :: fillSheetBusinessApplication :: method entered");
        HashMap<String, String> propertiesMap = new HashMap<>();

        List<Ci> filterCiCollectionsWithBusinessServices =
                bs.getCiCollection().stream()
                        .filter((ciCollection) -> ciCollection.getBusinessApplications() != null)
                        .filter((ciCollection) -> !ciCollection.getBusinessApplications().isEmpty())
                        .collect(Collectors.toList());

        int linesToAdd = 0;
        try {
            for (Ci ci : filterCiCollectionsWithBusinessServices) {
                linesToAdd += ci.getBusinessApplications().size();
                for (Ci_Simple business_app : ci.getBusinessApplications()) {
                    for (Map.Entry<String, String> entry : business_app.getProperties().entrySet()) {
                        propertiesMap.put(entry.getKey(), entry.getValue());
                    }
                }
            }
        } catch (Exception e) {
            log.error("GenerateXls.java ::: error in fillSheetBusinessApplication()");
        }

        CellStyle hlink_style = workbook.createCellStyle();
        hlink_style.setAlignment(HorizontalAlignment.CENTER);
        createRowsAndCells(linesToAdd, map.size()+2, sheet, hlink_style);
        mergeCells(sheet, linesToAdd);
        log.debug("GenerateXls.java :: fillSheetBusinessApplication :: After mergeCells");
        int cnt, start2 = 1;
        Row r;
        for (Ci ci : filterCiCollectionsWithBusinessServices) {
            cnt = 0;
            if (ci.getBusinessApplications().size() == 0) cnt += 1;
            else cnt += ci.getBusinessApplications().size();
            log.debug("GenerateXls.java :: fillSheetBusinessApplication :: trying to merge area of size: {}", start2 - start2 + cnt - 1);
            if (cnt > 1) {
                sheet.addMergedRegion(new CellRangeAddress(start2, start2 +
                        cnt - 1, 1, 1));
            }
            r = sheet.getRow(start2);
            r.getCell(1).setCellValue(ci.getProperties().get("display_label"));
            r.getCell(1).getCellStyle().setVerticalAlignment(VerticalAlignment.TOP);
            start2 = start2 + cnt;
        }

        cnt = 1;
        for (Ci ci : filterCiCollectionsWithBusinessServices) {
            for (Ci_Simple business_app : ci.getBusinessApplications()) {
                Row row = sheet.getRow(cnt);
                Set<String> keys = business_app.getProperties().keySet();
                int i = 3;
                for (Map.Entry<String, String> entry : propertiesMap.entrySet()) {
                    if (business_app.getProperties().containsKey(entry.getKey())) {
                        for (String key : keys) {
                            for (Map.Entry<Integer, String> entry2 : map.entrySet()) {
                                if (entry2.getValue().equals(key)) {
                                    row.getCell(entry2.getKey()).setCellValue(business_app.getProperties().get(key));
                                }
                            }
                        }
                    } else {
                        row.getCell(i).setCellValue(" ");
                    }

                }
                cnt++;
            }
        }
    }

    private void fillSheetNetDevicesIfExists(Sheet sheetNetDevices, Map<Integer, String> netDevicesProperties) {
        long netDevicesCount =
                bs.getCiCollection().stream()
                        .filter((ci) -> ci.getNetdevices() != null)
                        .count();

        if (netDevicesCount == 0) {
            // Remove Sheet from Template
            workbook.removeSheetAt(workbook.getSheetIndex("Network Devices"));
        }
        else {
            // Fill Sheet with Data
            fillSheetNetDevices(sheetNetDevices, netDevicesProperties);
        }

    }

    private void fillSheetNetDevices(Sheet sheet, Map<Integer, String> map) {
        log.debug("GenerateXls.java :: fillSheetNetDevices :: method entered");
        HashMap<String, String> propertiesMap = new HashMap<>();

        List<Ci> filterCiCollectionsWithNetDevices =
                bs.getCiCollection().stream()
                        .filter((ciCollection) -> ciCollection.getNetdevices() != null)
                        .filter((ciCollection) -> !ciCollection.getNetdevices().isEmpty())
                        .collect(Collectors.toList());

        int linesToAdd = 0;
        try {
            for (Ci ci : filterCiCollectionsWithNetDevices) {
                linesToAdd += ci.getNetdevices().size();
                for (Ci_Simple netdevice : ci.getNetdevices()) {
                    for (Map.Entry<String, String> entry : netdevice.getProperties().entrySet()) {
                        propertiesMap.put(entry.getKey(), entry.getValue());
                    }
                }
            }
        } catch (Exception e) {
            log.error("GenerateXls.java ::: error in fillSheetNetDevices()");
        }

        CellStyle hlink_style = workbook.createCellStyle();
        hlink_style.setAlignment(HorizontalAlignment.CENTER);
        createRowsAndCells(linesToAdd, map.size()+2, sheet, hlink_style);
        mergeCells(sheet, linesToAdd);
        log.debug("GenerateXls.java :: fillSheetNetDevices :: After mergeCells");
        int cnt, start2 = 1;
        Row r;
        for (Ci ci : filterCiCollectionsWithNetDevices) {
            cnt = 0;
            if (ci.getNetdevices().size() == 0) cnt += 1;
            else cnt += ci.getNetdevices().size();
            log.debug("GenerateXls.java :: fillSheetNetDevices :: trying to merge area of size: {}", start2 - start2 + cnt - 1);
            if (cnt > 1) {
                sheet.addMergedRegion(new CellRangeAddress(start2, start2 +
                        cnt - 1, 1, 1));
            }
            r = sheet.getRow(start2);
            r.getCell(1).setCellValue(ci.getProperties().get("display_label"));
            r.getCell(1).getCellStyle().setVerticalAlignment(VerticalAlignment.TOP);
            start2 = start2 + cnt;
        }

        cnt = 1;
        for (Ci ci : filterCiCollectionsWithNetDevices) {
            for (Ci_Simple netdevice : ci.getNetdevices()) {
                Row row = sheet.getRow(cnt);
                Set<String> keys = netdevice.getProperties().keySet();
                int i = 3;
                for (Map.Entry<String, String> entry : propertiesMap.entrySet()) {
                    if (netdevice.getProperties().containsKey(entry.getKey())) {
                        for (String key : keys) {
                            for (Map.Entry<Integer, String> entry2 : map.entrySet()) {
                                if (entry2.getValue().equals(key)) {
                                    row.getCell(entry2.getKey()).setCellValue(netdevice.getProperties().get(key));
                                }
                            }
                        }
                    } else {
                        row.getCell(i).setCellValue(" ");
                    }

                }
                cnt++;
            }
        }
    }

    // Workbook Methods for Putting and Formatting Data
    private void addRowsAndMergeCells(Sheet sheet, String methodName) {
        CellStyle cellStyle = setCellStyle();

        log.debug("GenerateXls.java :: addRowsAndMergeCells :: method entered");
        int linesToAdd = 0;
        int numOfCells = 0;

        // TODO -- Check this
        List<Ci> allExceptBusinessApplicationsAndNetDevices =
                bs.getCiCollection()
                .stream()
                .filter(ciCollection -> !ciCollection.getComputers().isEmpty())
                .collect(Collectors.toList());

        try {
            for (Ci ci : allExceptBusinessApplicationsAndNetDevices) {
                linesToAdd += ci.getComputers().size();
                log.debug("GenerateXls.java :: addRowsAndMergeCells :: Computers size: {}", ci.getComputers().size());
                for (Computer comp : ci.getComputers()) {
                    log.debug("GenerateXls.java :: addRowsAndMergeCells :: methodName: {}, comp: {}", methodName, comp.getId());
                    List<Ci_Simple> ci_simples = (List<Ci_Simple>) MethodUtils.
                            invokeExactMethod(comp, methodName);
                    if (ci_simples.size() > 1) {
                        linesToAdd += ci_simples.size() - 1;
                    }
                    numOfCells = 50;
                }
            }

            log.debug("GenerateXls.java :: addRowsAndMergeCells :: after for loop");

            createRowsAndCells(linesToAdd, numOfCells + 3, sheet, cellStyle);
            mergeCells(sheet, linesToAdd);
            log.debug("GenerateXls.java :: addRowsAndMergeCells :: after mergeCells");

            log.debug("GenerateXls.java :: addRowsAndMergeCells :: getCiCollection: {}", bs.getCiCollection().size());


            int cnt, start = 1, start2 = 1;
            Row row;
            for (Ci ci : allExceptBusinessApplicationsAndNetDevices) {
                cnt = 0;
                log.debug("GenerateXls.java :: addRowsAndMergeCells :: inside first for, computers: {}", ci.getComputers().size());

                for (Computer computer : ci.getComputers()) {
                    log.debug("GenerateXls.java :: addRowsAndMergeCells :: inside second for");

                    List<Ci_Simple> ci_simples = (List<Ci_Simple>) MethodUtils.
                            invokeExactMethod(computer, methodName);
                    if (ci_simples.size() == 0) cnt += 1;
                    else cnt += ci_simples.size();
                    if (ci_simples.size() > 1) {
                        sheet.addMergedRegion(new CellRangeAddress(start, start +
                                ci_simples.size() - 1, 2, 2));
                    }


                    row = sheet.getRow(start);
                    row.getCell(2).setCellValue(computer.getProperties().get("display_label"));
                    row.getCell(2).getCellStyle().setVerticalAlignment(VerticalAlignment.TOP);
                    if (ci_simples.size() == 0) start = start + 1;
                    else start += ci_simples.size();
                }
                log.debug("GenerateXls.java :: addRowsAndMergeCells :: after second for");
                log.debug("GenerateXLS.java :: start2: {}, cnt: {}", start2, cnt);
                if (cnt > 1) {
                    sheet.addMergedRegion(new CellRangeAddress(start2, start2 +
                            cnt - 1, 1, 1));
                }
                log.debug("GenerateXls.java :: addRowsAndMergeCells :: after addMergeRegion");

                row = sheet.getRow(start2);
                log.debug("GenerateXls.java :: addRowsAndMergeCells :: after getRow with start2: {}", start2);

                row.getCell(1).setCellValue(ci.getProperties().get("display_label"));
                row.getCell(1).getCellStyle().setVerticalAlignment(VerticalAlignment.TOP);
                start2 = start2 + cnt;
            }
            log.debug("GenerateXls.java :: addRowsAndMergeCells :: after first for");
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            log.error("GenerateXls.java ::: Error in addRowsAndMergeCells()");
        }
        log.debug("GenerateXls.java :: addRowsAndMergeCells :: method finished");

    }

    private void createRowsAndCells(int numOfRows, int numOfCells, Sheet sheet, CellStyle hlink_style) {
        log.debug("GenerateXls.java :: createRowsAndCells :: Method entered");

        int rowsStartingPoint = 1;
        for (int j = 0; j < numOfRows; j++) {
            sheet.createRow(rowsStartingPoint);
            rowsStartingPoint++;
        }

        for (int u = 1; u < rowsStartingPoint; u++) {
            int cellsStartingPoint = 0;
            for (int i = 0; i < numOfCells; i++) {
                hlink_style.setAlignment(HorizontalAlignment.CENTER);
                sheet.getRow(u).createCell(cellsStartingPoint);
                sheet.getRow(u).getCell(cellsStartingPoint).setCellStyle(hlink_style);
                cellsStartingPoint++;
            }
        }
    }

    private void mergeCells(Sheet sheet, int linesToAdd) {
        log.debug("GenerateXls.java :: mergeCells :: method entered with linesToAdd: {} and Sheet: {}", linesToAdd, sheet);
        if (linesToAdd > 1) {
            sheet.addMergedRegion(new CellRangeAddress(1, linesToAdd, 0, 0));
        }
        log.debug("GenerateXls.java :: mergeCells :: after merge region");

        log.debug("GenerateXls.java :: mergeCells :: sheet.firstRowNum: {}, sheet.lastRowNum: {}", sheet.getFirstRowNum(), sheet.getLastRowNum());
        Row r = sheet.getRow(1);
        log.debug("GenerateXls.java :: mergeCells :: get display_label: {}", bs.getProperties().get("display_label"));
        r.getCell(0).setCellValue(bs.getProperties().get("display_label"));
        log.debug("GenerateXls.java :: mergeCells :: get display_label: {}", bs.getProperties().get("display_label"));
        r.getCell(0).getCellStyle().setVerticalAlignment(VerticalAlignment.TOP);
        log.debug("GenerateXls.java :: mergeCells :: method finished");

    }

    private void setSheetRows(Sheet sheet, String methodName, Map<Integer, String> tmp) {
        log.debug("GenerateXls.java :: setSheetRows :: method entered");
        int cnt = 1;

        HashMap<String, String> propertiesMap = new HashMap<>();

        try {

            for (Ci ci : bs.getCiCollection()) {
                for (Computer comp : ci.getComputers()) {
                    List<Ci_Simple> ci_simples = (List<Ci_Simple>) MethodUtils.
                            invokeExactMethod(comp, methodName);
                    for (Ci_Simple ci_simple : ci_simples) {
                        for (Map.Entry<String, String> entry : ci_simple.getProperties().entrySet()) {
                            propertiesMap.put(entry.getKey(), entry.getValue());
                        }
                    }
                }
            }

            for (Ci ci : bs.getCiCollection()) {
                for (Computer comp : ci.getComputers()) {
                    List<Ci_Simple> ci_simples = (List<Ci_Simple>) MethodUtils.
                            invokeExactMethod(comp, methodName);
                    if (ci_simples.size() == 0) cnt++;
                    for (Ci_Simple ci_simple : ci_simples) {
                        Row tmpRow = sheet.getRow(cnt);
                        Set<String> keys = ci_simple.getProperties().keySet();
                        int i = 3;
                        for (Map.Entry<String, String> entry : propertiesMap.entrySet()) {
                            if (ci_simple.getProperties().containsKey(entry.getKey())) {
                                for (String key : keys) {
                                    for (Map.Entry<Integer, String> entry2 : tmp.entrySet()) {
                                        if (entry2.getValue().equals(key)) {
                                            tmpRow.getCell(entry2.getKey()).
                                                    setCellValue(ci_simple.getProperties().get(key));
                                        }
                                    }
                                }
                            } else {
                                tmpRow.getCell(i).setCellValue(" ");
                            }
                            i++;
                        }
                        cnt++;
                    }
                }
            }
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            log.error("GenerateXls.java ::: Error in setSheetRows() ");
        }
    }
}
