package gr.cosmote.ucmdb.commons.exceptions;

public class SystemException extends RuntimeException {
    public SystemException(String message) {
        super(message);
    }

    public SystemException(Throwable t) {
        super(t);
    }

    public SystemException(String message, Throwable t) {
        super(message, t);
    }
}
