package gr.cosmote.ucmdb.commons.exportXls;

import gr.cosmote.ucmdb.models.database.UcmdbMissingNode;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Data
@Slf4j
public class GenerateMissingCisExcel {
    private List<UcmdbMissingNode> MissingCisDb;

    public byte[] exportMissingCisExcel(){
        log.debug("GenerateMissingCisExcel.exportMissingCisExcel :: Method Entered");
        Workbook missingCisWb = initWorkbook();
        createMissingCisWbHeader(missingCisWb);
        fillMissingCisWbData(missingCisWb);

        setColumnsWidth(missingCisWb);
        return writeMissingCisWbBytes(missingCisWb);
    }

    private Workbook initWorkbook(){
        Workbook missingCisWb = new XSSFWorkbook();
        missingCisWb.createSheet("Missing CIs");

        return missingCisWb;
    }

    private void styleMissingCisHeaderRow(Workbook missingCisWb, Row row){
        CellStyle missingCisHeaderCellStyle = missingCisWb.createCellStyle();

        Font font = missingCisWb.createFont();
        font.setBold(true);

        missingCisHeaderCellStyle.setFont(font);
        missingCisHeaderCellStyle.setWrapText(true);

        Iterator<Cell> cells = row.cellIterator();
        while (cells.hasNext()) {
            Cell cell = cells.next();

            cell.setCellStyle(missingCisHeaderCellStyle);
        }
    }

    private void createMissingCisWbHeader( Workbook missingCisWb){
        Sheet missingCisSheet = missingCisWb.getSheet("Missing CIs");

        Row headerMissingCisRow = missingCisSheet.createRow(0);

        headerMissingCisRow.createCell(0).setCellValue("TENANT OWNER");
        headerMissingCisRow.createCell(1).setCellValue("DISPLAY LABEL");
        headerMissingCisRow.createCell(2).setCellValue("IP ADDRESS");
        headerMissingCisRow.createCell(3).setCellValue("ENVIRONMENT");
        headerMissingCisRow.createCell(4).setCellValue("NOTE");
        headerMissingCisRow.createCell(5).setCellValue("INSERTED DATE");
        headerMissingCisRow.createCell(6).setCellValue("INSERTED BY");

        styleMissingCisHeaderRow(missingCisWb, headerMissingCisRow);
    }

    private void fillMissingCisWbData(Workbook missingCisWb){
        log.debug("GenerateMissingCisExcel.fillMissingCisWbData :: Method Entered");
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Sheet missingCisSheet = missingCisWb.getSheet("Missing CIs");

        CellStyle alignStyle = missingCisWb.createCellStyle();
        alignStyle.setAlignment(HorizontalAlignment.LEFT);

        AtomicInteger rowNumb = new AtomicInteger(1);

        MissingCisDb.forEach(comm -> {
            Row currentMissingCiRow = missingCisSheet.createRow(rowNumb.getAndIncrement());

            currentMissingCiRow.createCell(0).setCellValue(comm.getTenantOwner());
            currentMissingCiRow.createCell(1).setCellValue(comm.getDisplayLabel());
            currentMissingCiRow.createCell(2).setCellValue(comm.getIpAddress());
            currentMissingCiRow.createCell(3).setCellValue(comm.getEnvironment());
            currentMissingCiRow.createCell(4).setCellValue(comm.getNote());
            currentMissingCiRow.createCell(5).setCellValue(sdf.format(comm.getInsertedDate()));
            currentMissingCiRow.createCell(6).setCellValue(comm.getInsertedBy());

        });

        log.debug("GenerateMissingCisExcel.fillMissingCisWbData :: Method Returned");
    }

    private void setColumnsWidth (Workbook missingCisWb) {
        log.debug("GenerateMissingCisExcel.setColumnsWidth :: Method Entered");
        Sheet missingCisSheet = missingCisWb.getSheet("Missing CIs");

        int numOfSheetCols = missingCisSheet.getRow(0).getPhysicalNumberOfCells();
        for (int col = 0; col < numOfSheetCols; col++) {
            missingCisSheet.setColumnWidth(col, 5400);
        }

        log.debug("GenerateMissingCisExcel.setColumnsWidth :: Method Returned");
    }

    private byte[] writeMissingCisWbBytes(Workbook missingCisWb){
        log.debug("GenerateMissingCisExcel.writeMissingCisBytes :: Method Entered");

        byte[] missingCisByte;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()){
            missingCisWb.write(baos);
            missingCisByte = baos.toByteArray();
        } catch (IOException ex){
            log.error("GenerateMissingCisExcel.writeMissingCisBytes :: Error", ex);

            return null;
        }
        return missingCisByte;
    }

}
