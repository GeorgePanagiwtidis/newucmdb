package gr.cosmote.ucmdb.commons.exportXls;

import gr.cosmote.ucmdb.models.view.UserCommentDTO;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Data
@Slf4j
public class GenerateUserCommentsExcel {
    private List<UserCommentDTO> userDbComments;

    public byte[] exportExcel() {
        log.debug("GenerateUserCommentsExcel.exportExcel :: Method Entered");

        Workbook wb = initWorkbook();
        createWorkbookHeader(wb);
        fillWorkbookWithData(wb);

        setColumnsWidth(wb);
        return writeWorkbookBytes(wb);
    }

    private Workbook initWorkbook() {
        Workbook wb = new XSSFWorkbook();
        wb.createSheet("User Comments");

        return wb;
    }

    private void styleHeaderRow(Workbook wb, Row row) {
        CellStyle headerCellStyle = wb.createCellStyle();

        Font font = wb.createFont();
        font.setBold(true);

        headerCellStyle.setFont(font);
        headerCellStyle.setFillForegroundColor(IndexedColors.AQUA.getIndex());
        headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        headerCellStyle.setWrapText(true);

        Iterator<Cell> cells = row.cellIterator();
        while(cells.hasNext()) {
            Cell cell = cells.next();

            cell.setCellStyle(headerCellStyle);
        }
    }

    private void createWorkbookHeader(Workbook wb) {
        Sheet userCommentsSheet = wb.getSheet("User Comments");

        Row headerRow = userCommentsSheet.createRow(0);

        headerRow.createCell(0).setCellValue("Display Label");
        headerRow.createCell(1).setCellValue("Global ID");
        headerRow.createCell(2).setCellValue("Ci Type");
        headerRow.createCell(3).setCellValue("Tenant Owner");
        headerRow.createCell(4).setCellValue("Category");
        headerRow.createCell(5).setCellValue("Environment");
        headerRow.createCell(6).setCellValue("Role");
        headerRow.createCell(7).setCellValue("Application Domain");
        headerRow.createCell(8).setCellValue("Author");
        headerRow.createCell(9).setCellValue("Comment Date");
        headerRow.createCell(10).setCellValue("Comment Description");

        styleHeaderRow(wb, headerRow);
    }

    private void fillWorkbookWithData(Workbook wb) {
        log.debug("GenerateUserCommentsExcel.fillWorkbookWithData :: Method Entered");
        // Used for Date Formatting
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        Sheet userCommentsSheet = wb.getSheet("User Comments");
        AtomicInteger rowNumber = new AtomicInteger(1);

        userDbComments.forEach(comm -> {
            Row currentRow = userCommentsSheet.createRow(rowNumber.getAndIncrement());

            currentRow.createCell(0).setCellValue(comm.getDisplayLabel());
            currentRow.createCell(1).setCellValue(comm.getGlobalId());
            currentRow.createCell(2).setCellValue(comm.getCiType());
            currentRow.createCell(3).setCellValue(comm.getTenantOwner());
            currentRow.createCell(4).setCellValue(comm.getCategory());
            currentRow.createCell(5).setCellValue(comm.getEnvironment());
            currentRow.createCell(6).setCellValue(comm.getRole());
            currentRow.createCell(7).setCellValue(comm.getDomain());
            currentRow.createCell(8).setCellValue(comm.getAuthor());
            currentRow.createCell(9).setCellValue(sdf.format(comm.getDate()));
            currentRow.createCell(10).setCellValue(comm.getComment());

        });

        log.debug("GenerateUserCommentsExcel.fillWorkbookWithData :: Method Returned");
    }

    private void setColumnsWidth(Workbook wb) {
        log.debug("GenerateUserCommentsExcel.setColumnsWidth :: Method Entered");
        Sheet userCommentsSheet = wb.getSheet("User Comments");

        int numberOfHeaderCells = userCommentsSheet.getRow(0).getPhysicalNumberOfCells();
        for (int headerCell = 0; headerCell < numberOfHeaderCells; headerCell++) {
            userCommentsSheet.setColumnWidth(headerCell, 5800);
        }

        log.debug("GenerateUserCommentsExcel.setColumnsWidth :: Method Returned");
    }

    private byte[] writeWorkbookBytes(Workbook wb) {
        log.debug("GenerateUserCommentsExcel.writeWorkbookBytes :: Method Entered");

        byte[] wbBytes;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()){
            wb.write(baos);
            wbBytes = baos.toByteArray();
        }
        catch (IOException ex) {
            log.error("GenerateUserCommentsExcel.writeWorkbookBytes :: Error", ex);

            return null;
        }

        return wbBytes;
    }
}
