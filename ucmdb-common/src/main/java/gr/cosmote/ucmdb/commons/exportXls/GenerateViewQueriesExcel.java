package gr.cosmote.ucmdb.commons.exportXls;

import gr.cosmote.ucmdb.models.database.ViewQueriesProperties;
import gr.cosmote.ucmdb.models.view.viewquery.ViewQueryRelatedCI;
import gr.cosmote.ucmdb.models.view.viewquery.ViewQueryRequestDTO;
import gr.cosmote.ucmdb.models.view.viewquery.ViewQueryResponseDTO;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Data
@Slf4j
public class GenerateViewQueriesExcel {

    private List<ViewQueriesProperties> queryProperties;
    private List<ViewQueryResponseDTO> viewQueryResponse;

    public byte[] generateExcel(ViewQueryRequestDTO requestDTO) {
        log.debug("GenerateViewQueriesExcel.generateExcel :: Method Entered for Query {}", requestDTO.getQueryName());

        sortResponseByDisplayLabel(this.viewQueryResponse);

        Workbook wb = initWorkbookAndFillData(requestDTO.getQueryName(), this.viewQueryResponse);
        autoWidthColumns(wb);

        return writeWorkbookDataToByteArray(wb);
    }

    private void sortResponseByDisplayLabel(List<ViewQueryResponseDTO> viewQueryResponse) {
        viewQueryResponse.sort((ViewQueryResponseDTO r1, ViewQueryResponseDTO r2) -> {
            String hostName1 = r1.getCi().getProperties().get("display_label");
            String hostName2 = r2.getCi().getProperties().get("display_label");

            return hostName1.toLowerCase().compareTo(hostName2.toLowerCase());
        });
    }

    private Workbook initWorkbookAndFillData(String queryName, List<ViewQueryResponseDTO> viewQueryResponse) {
        log.debug("GenerateViewQueriesExcel.initWorkbookAndFillData :: Method Entered to fill Data size of {}", viewQueryResponse.size());

        Workbook wb = new HSSFWorkbook();

        // Create Sheet
        Sheet queryResultsSheet = wb.createSheet(String.format("%s Results", queryName));

        // Init Row Number
        int sheetRowNum = 0;
        CellStyle[] cellStyles = {getParentCellStyle(wb), getTableHeaderStyle(wb), getChildrenCellStyle(wb)};
        for (ViewQueryResponseDTO r : viewQueryResponse) {
            String parentDisplayLabel = r.getCi().getProperties().get("display_label");
            String parentCellText = String.format("Host Node: %s", parentDisplayLabel);

            Row row = queryResultsSheet.createRow(sheetRowNum);

            Cell parentCell = row.createCell(0);
            fillParentCellDataAndStyle(queryResultsSheet, sheetRowNum++, parentCell, cellStyles[0], parentCellText);
            sheetRowNum = fillChildrenData(queryResultsSheet, cellStyles[1], cellStyles[2], sheetRowNum, parentDisplayLabel, r.getRelationCIs());
        }

        log.debug("GenerateViewQueriesExcel.initWorkbookAndFillData :: Method Returned filled Workbook");
        return wb;
    }

    private CellStyle getParentCellStyle(Workbook wb) {
        CellStyle cellStyle = wb.createCellStyle();
//        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        cellStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        return cellStyle;
    }

    private CellStyle getTableHeaderStyle(Workbook wb) {
        CellStyle headerCellStyle = wb.createCellStyle();

        Font font = wb.createFont();
        font.setBold(true);
        font.setItalic(true);

        headerCellStyle.setFont(font);
        headerCellStyle.setBorderTop(BorderStyle.THIN);
        headerCellStyle.setBorderBottom(BorderStyle.THIN);
        headerCellStyle.setBorderLeft(BorderStyle.THIN);
        headerCellStyle.setBorderRight(BorderStyle.THIN);

        return headerCellStyle;
    }

    private CellStyle getChildrenCellStyle(Workbook wb) {
        CellStyle childCellStyle = wb.createCellStyle();

        childCellStyle.setBorderTop(BorderStyle.THIN);
        childCellStyle.setBorderBottom(BorderStyle.THIN);
        childCellStyle.setBorderLeft(BorderStyle.THIN);
        childCellStyle.setBorderRight(BorderStyle.THIN);

        return childCellStyle;
    }

    private void fillParentCellDataAndStyle(Sheet sheet, int sheetRow, Cell cell, CellStyle cellStyle, String data) {
        CellRangeAddress cellRangeAddress = new CellRangeAddress(sheetRow, sheetRow, 0, queryProperties.size()-1);

        sheet.addMergedRegion(cellRangeAddress);
        cell.setCellValue(data);
        cell.setCellStyle(cellStyle);

        RegionUtil.setBorderTop(BorderStyle.THIN.getCode(), cellRangeAddress, sheet);
        RegionUtil.setBorderBottom(BorderStyle.THIN.getCode(), cellRangeAddress, sheet);
        RegionUtil.setBorderLeft(BorderStyle.THIN.getCode(), cellRangeAddress, sheet);
        RegionUtil.setBorderRight(BorderStyle.THIN.getCode(), cellRangeAddress, sheet);
    }

    private void createTableHeaderForChildren(int sheetRow, Sheet queryResultsSheet, CellStyle tableHeaderStyle) {
        Row tableHeader = queryResultsSheet.createRow(sheetRow);

        this.queryProperties.forEach(prop -> {
            Cell headerCell = tableHeader.createCell(prop.getPropertyPriority()-1);

            headerCell.setCellStyle(tableHeaderStyle);
            headerCell.setCellValue(prop.getPropertyLabel());
        });
    }

    private int fillChildrenData(Sheet queryResultsSheet, CellStyle tableHeaderStyle, CellStyle childCellStyle, int sheetRowNum, String parentNode, List<ViewQueryRelatedCI> viewQueryResponse) {
        int currentRowIndex = sheetRowNum;

        if (viewQueryResponse.isEmpty()) {
            Cell noChildenCell = queryResultsSheet.createRow(currentRowIndex).createCell(0);
            noChildenCell.setCellValue(String.format("No Guest Nodes found for Host: %s", parentNode));

            queryResultsSheet.addMergedRegion(new CellRangeAddress(sheetRowNum, sheetRowNum, 0, queryProperties.size()-1));

            currentRowIndex++;
        }
        else {
            createTableHeaderForChildren(currentRowIndex++, queryResultsSheet, tableHeaderStyle);

            for (ViewQueryRelatedCI relatedCI : viewQueryResponse) {
                Map<String, String> relatedCiProps = relatedCI.getRelatedCiProperties().getProperties();

                Row currentRow = queryResultsSheet.createRow(currentRowIndex++);

                queryProperties.forEach(prop -> {
                    Cell childCell = currentRow.createCell(prop.getPropertyPriority()-1);

                    childCell.setCellStyle(childCellStyle);
                    childCell.setCellValue(relatedCiProps.get(prop.getPropertyName()));
                });
            }
        }

        return currentRowIndex + 1;
    }

    private void autoWidthColumns(Workbook wb) {
        log.debug("GenerateViewQueriesExcel.autoWidthColumns :: Method Entered");

        int numberOfSheets = wb.getNumberOfSheets();

        for (int sheetIndex = 0; sheetIndex < numberOfSheets; sheetIndex++) {
            Sheet currentSheet = wb.getSheetAt(sheetIndex);

            Row sheetRow = currentSheet.getRow(getFirstRowWithData());
            Iterator<Cell> cellsIterator = sheetRow.cellIterator();

            while (cellsIterator.hasNext()) {
                Cell currentCell = cellsIterator.next();
                currentSheet.autoSizeColumn(currentCell.getColumnIndex());
            }
        }

        log.debug("GenerateViewQueriesExcel.autoWidthColumns :: Method Returned");
    }

    private byte[] writeWorkbookDataToByteArray(Workbook wb) {
        byte[] workbookBytes = null;
        try (ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream()) {
            wb.write(byteArrayOS);
            workbookBytes = byteArrayOS.toByteArray();
        }
        catch (IOException ex) {
            log.error("GenerateViewQueriesExcel.writeWorkbookDataToByteArray :: ERROR while writing to Byte[]");
        }

        return workbookBytes;
    }

    private int getFirstRowWithData() {
        int rowWithData = 0;
        for (ViewQueryResponseDTO r : this.viewQueryResponse) {
            if (!r.getRelationCIs().isEmpty()) {
                rowWithData +=2;
                break;
            }
            else {
                rowWithData+=3;
            }
        }

        return rowWithData;
    }
}
