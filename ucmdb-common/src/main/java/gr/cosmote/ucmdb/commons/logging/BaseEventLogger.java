package gr.cosmote.ucmdb.commons.logging;
import lombok.extern.slf4j.Slf4j;
import org.kie.api.event.kiebase.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Slf4j
public class BaseEventLogger implements KieBaseEventListener {

    @Override
    public void beforeKiePackageAdded(BeforeKiePackageAddedEvent beforeKiePackageAddedEvent) {
        log.debug("KBase:beforeKiePackageAdded():{}", beforeKiePackageAddedEvent);
    }

    @Override
    public void afterKiePackageAdded(AfterKiePackageAddedEvent afterKiePackageAddedEvent) {
        log.debug("KBase:afterKiePackageAdded():{}", afterKiePackageAddedEvent);
    }

    @Override
    public void beforeKiePackageRemoved(BeforeKiePackageRemovedEvent beforeKiePackageRemovedEvent) {
        log.debug("KBase:beforeKiePackageRemoved():{}", beforeKiePackageRemovedEvent);
    }

    @Override
    public void afterKiePackageRemoved(AfterKiePackageRemovedEvent afterKiePackageRemovedEvent) {
        log.debug("KBase:afterKiePackageRemoved():{}", afterKiePackageRemovedEvent);
    }

    @Override
    public void beforeKieBaseLocked(BeforeKieBaseLockedEvent beforeKieBaseLockedEvent) {
        log.debug("KBase:beforeKieBaseLocked():{}", beforeKieBaseLockedEvent);
    }

    @Override
    public void afterKieBaseLocked(AfterKieBaseLockedEvent afterKieBaseLockedEvent) {
        log.debug("KBase:afterKieBaseLocked():{}", afterKieBaseLockedEvent);
    }

    @Override
    public void beforeKieBaseUnlocked(BeforeKieBaseUnlockedEvent beforeKieBaseUnlockedEvent) {
        log.debug("KBase:beforeKieBaseUnlocked():{}", beforeKieBaseUnlockedEvent);
    }

    @Override
    public void afterKieBaseUnlocked(AfterKieBaseUnlockedEvent afterKieBaseUnlockedEvent) {
        log.debug("KBase:afterKieBaseUnlocked():{}", afterKieBaseUnlockedEvent);
    }

    @Override
    public void beforeRuleAdded(BeforeRuleAddedEvent beforeRuleAddedEvent) {
        log.debug("KBase:beforeRuleAdded():{}", beforeRuleAddedEvent);
    }

    @Override
    public void afterRuleAdded(AfterRuleAddedEvent afterRuleAddedEvent) {
        log.debug("KBase:afterRuleAdded():{}", afterRuleAddedEvent);
    }

    @Override
    public void beforeRuleRemoved(BeforeRuleRemovedEvent beforeRuleRemovedEvent) {
        log.debug("KBase:beforeRuleRemoved():{}", beforeRuleRemovedEvent);
    }

    @Override
    public void afterRuleRemoved(AfterRuleRemovedEvent afterRuleRemovedEvent) {
        log.debug("KBase:afterRuleRemoved():{}", afterRuleRemovedEvent);
    }

    @Override
    public void beforeFunctionRemoved(BeforeFunctionRemovedEvent beforeFunctionRemovedEvent) {
        log.debug("KBase:beforeFunctionRemoved():{}", beforeFunctionRemovedEvent);
    }

    @Override
    public void afterFunctionRemoved(AfterFunctionRemovedEvent afterFunctionRemovedEvent) {
        log.debug("KBase:afterFunctionRemoved():{}", afterFunctionRemovedEvent);
    }

    @Override
    public void beforeProcessAdded(BeforeProcessAddedEvent beforeProcessAddedEvent) {
        log.debug("KBase:beforeProcessAdded():{}", beforeProcessAddedEvent);
    }

    @Override
    public void afterProcessAdded(AfterProcessAddedEvent afterProcessAddedEvent) {
        log.debug("KBase:afterProcessAdded():{}", afterProcessAddedEvent);
    }

    @Override
    public void beforeProcessRemoved(BeforeProcessRemovedEvent beforeProcessRemovedEvent) {
        log.debug("KBase:beforeProcessRemoved():{}", beforeProcessRemovedEvent);
    }

    @Override
    public void afterProcessRemoved(AfterProcessRemovedEvent afterProcessRemovedEvent) {
        log.debug("KBase:afterProcessRemoved():{}", afterProcessRemovedEvent);
    }
}
