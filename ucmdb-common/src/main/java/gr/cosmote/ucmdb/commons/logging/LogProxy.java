package gr.cosmote.ucmdb.commons.logging;

import org.apache.commons.lang.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

public class LogProxy implements java.lang.reflect.InvocationHandler {
    private Object obj;
    private Logger logger = null;

    private LogProxy(Object obj, Logger logger) {
        this.obj = obj;
        this.logger = logger;
    }

    public static Object newInstance(Object obj, Logger logger) {
        return Proxy.newProxyInstance(
                obj.getClass().getClassLoader(), obj.getClass().getInterfaces(), new LogProxy(obj, logger)
        );
    }

    public static Object newInstance(Object obj) {
        return newInstance(obj, LOG_PROXY_LOGGER);
    }

    public Object invoke(Object proxy, Method m, Object[] args) throws Throwable {

        Object result;

        StopWatch w = new StopWatch(); w.start();

        final String fullClassName = obj.getClass().getName() + "." + m.getName() + "()";
        final String arguments     = args == null?"null": Arrays.toString(args);

        try {

            logger.debug(fullClassName + " ::: before calling method with arguments " + arguments);

            result = m.invoke(obj, args);

        } catch (InvocationTargetException e) {
            throw e.getTargetException();
        } catch (Exception e) {
            throw new Exception(fullClassName + " ::: unexpected invocation exception ", e);
        } finally {
            w.stop();

            logger.info(fullClassName + " ::: after calling method with arguments " + arguments + " took " + w.getTime());
        }

        return result;

    }

    public String toString() {
        final StringBuffer sb = new StringBuffer("LogProxy{");
        sb.append("obj=").append(obj);
        sb.append('}');
        return sb.toString();
    }

    private static final Logger LOG_PROXY_LOGGER = LoggerFactory.getLogger(LogProxy.class);
}
