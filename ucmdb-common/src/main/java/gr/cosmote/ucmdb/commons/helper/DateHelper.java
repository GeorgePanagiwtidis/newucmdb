package gr.cosmote.ucmdb.commons.helper;


import lombok.extern.slf4j.Slf4j;
import org.joda.time.format.DateTimeFormat;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Created by Intrasoft Intl - dasimakopoulos on 27/8/2017.
 */
@Slf4j
public class DateHelper {
    public static DateTimeFormatter defaultDateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

    public static String dateTimeToString(Date date) {
        if (date == null)
            return null;
        LocalDateTime ldt = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        return ldt.format(defaultDateTimeFormatter);
    }

    public static Date stringToDateTimeTo(String stringdate) {
        if (stringdate == null || stringdate.isEmpty())
            return null;

        LocalDateTime ldt = LocalDateTime.parse(stringdate, defaultDateTimeFormatter);
        Date out = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
        return out;
    }

    public static Date parseUcmdbDate(String ucmdbDate) {
        Date parsedDate;
        try {
            Instant parsedInstant = Instant.parse(ucmdbDate);

            parsedDate = new Date(parsedInstant.toEpochMilli());
        }
        catch (Exception ex) {
            log.error("DateHelper.parseUcmdbDate :: ERROR", ex);

            parsedDate = null;
        }

        return parsedDate;
    }
}
