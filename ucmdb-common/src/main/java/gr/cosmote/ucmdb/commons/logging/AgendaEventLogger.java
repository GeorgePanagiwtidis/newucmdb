package gr.cosmote.ucmdb.commons.logging;

import lombok.extern.slf4j.Slf4j;
import org.kie.api.event.rule.*;

@Slf4j
public class AgendaEventLogger implements AgendaEventListener {
    @Override
    public void matchCreated(MatchCreatedEvent matchCreatedEvent) {
        log.info("AgendaEventLogger:agenda:matchCreated():{}", matchCreatedEvent);
    }

    @Override
    public void matchCancelled(MatchCancelledEvent matchCancelledEvent) {
        log.info("AgendaEventLogger:agenda:matchCancelled():{}", matchCancelledEvent);
    }

    @Override
    public void beforeMatchFired(BeforeMatchFiredEvent beforeMatchFiredEvent) {
        log.info("AgendaEventLogger:agenda:beforeMatchFired():{}", beforeMatchFiredEvent);
    }

    @Override
    public void afterMatchFired(AfterMatchFiredEvent afterMatchFiredEvent) {
        log.info("AgendaEventLogger:agenda:afterMatchFired():{}", afterMatchFiredEvent);
    }

    @Override
    public void agendaGroupPopped(AgendaGroupPoppedEvent agendaGroupPoppedEvent) {
        log.info("AgendaEventLogger:agenda:agendaGroupPopped():{}", agendaGroupPoppedEvent);
    }

    @Override
    public void agendaGroupPushed(AgendaGroupPushedEvent agendaGroupPushedEvent) {
        log.info("AgendaEventLogger:agenda:agendaGroupPushed():{}", agendaGroupPushedEvent);
    }

    @Override
    public void beforeRuleFlowGroupActivated(RuleFlowGroupActivatedEvent ruleFlowGroupActivatedEvent) {
        log.info("AgendaEventLogger:agenda:beforeRuleFlowGroupActivated():{}", ruleFlowGroupActivatedEvent);
    }

    @Override
    public void afterRuleFlowGroupActivated(RuleFlowGroupActivatedEvent ruleFlowGroupActivatedEvent) {
        log.info("AgendaEventLogger:agenda:afterRuleFlowGroupActivated():{}", ruleFlowGroupActivatedEvent);
    }

    @Override
    public void beforeRuleFlowGroupDeactivated(RuleFlowGroupDeactivatedEvent ruleFlowGroupDeactivatedEvent) {
        log.info("AgendaEventLogger:agenda:beforeRuleFlowGroupDeactivated():{}", ruleFlowGroupDeactivatedEvent);
    }

    @Override
    public void afterRuleFlowGroupDeactivated(RuleFlowGroupDeactivatedEvent ruleFlowGroupDeactivatedEvent) {
        log.info("AgendaEventLogger:agenda:afterRuleFlowGroupDeactivated():{}", ruleFlowGroupDeactivatedEvent);
    }
}
