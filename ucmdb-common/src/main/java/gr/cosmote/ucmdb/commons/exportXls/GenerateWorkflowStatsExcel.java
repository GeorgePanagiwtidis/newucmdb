package gr.cosmote.ucmdb.commons.exportXls;

import gr.cosmote.ucmdb.models.view.WorkflowStatsDTO;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Data
@Slf4j
public class GenerateWorkflowStatsExcel {
    private List<WorkflowStatsDTO> WorkflowStatsDb;

    public byte[] exportWorkflowStatsExcel(){
        log.debug("GenerateWorkflowStatsExcel.exportWorkflowStatsExcel :: Method Entered");

        Workbook workflowStatsWb = initWb();
        createWorkflowStatsHeader(workflowStatsWb);
        fillWorkflowStatsWb(workflowStatsWb);
        setWorkflowStatsColumnsWidth(workflowStatsWb);

        return writeWorkflowStatsWbBytes(workflowStatsWb);
    }

    private Workbook initWb(){
        Workbook workflowStatsWb = new XSSFWorkbook();
        workflowStatsWb.createSheet("Workflow Stats");

        return workflowStatsWb;
    }

    private void workflowStatsStyleHeaderRow (Workbook workflowStatsWb , Row row) {
        CellStyle workflowStatsStyle = workflowStatsWb.createCellStyle();

        Font workflowFont = workflowStatsWb.createFont();
        workflowFont.setBold(true);

        workflowStatsStyle.setFont(workflowFont);
        workflowStatsStyle.setWrapText(true);

        Iterator<Cell> workflowCells = row.cellIterator();
        while (workflowCells.hasNext()){
            Cell workflowCell = workflowCells.next();

            workflowCell.setCellStyle(workflowStatsStyle);
        }
    }

    private void createWorkflowStatsHeader (Workbook workflowStatsWb){
        Sheet workflowStatsSheet = workflowStatsWb.getSheet("Workflow Stats");

        Row headerWorkflowStatsRow = workflowStatsSheet.createRow(0);

        headerWorkflowStatsRow.createCell(0).setCellValue("WORKFLOW MONTH");
        headerWorkflowStatsRow.createCell(1).setCellValue("TENANT OWNER");
        headerWorkflowStatsRow.createCell(2).setCellValue("TOTAL NODES");
        headerWorkflowStatsRow.createCell(3).setCellValue("COMPLETED NODES");
        headerWorkflowStatsRow.createCell(4).setCellValue("OPEN NODES");

        workflowStatsStyleHeaderRow(workflowStatsWb, headerWorkflowStatsRow);
    }

    private void fillWorkflowStatsWb(Workbook workflowStatsWb){
        log.debug("GenerateWorkflowStatsExcel.fillWorkflowStatsWb :: Method Entered");
        Sheet workflowStatsSheet = workflowStatsWb.getSheet("Workflow Stats");

        CellStyle alignStyle = workflowStatsWb.createCellStyle();
        alignStyle.setAlignment(HorizontalAlignment.LEFT);

        AtomicInteger rowNumber = new AtomicInteger(1);

        WorkflowStatsDb.forEach(stat -> {
            Row currentWorkflowRow = workflowStatsSheet.createRow(rowNumber.getAndIncrement());
            fillCellValuesBasedOnIndex(workflowStatsWb, currentWorkflowRow, stat);
        });

        log.debug("GenerateWorkflowStatsExcel.fillWorkflowStatsWb :: Method Returned");
    }

    private void fillCellValuesBasedOnIndex(Workbook wb, Row row, WorkflowStatsDTO stat) {
        // Date Formatting
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM yyyy");

        // Cell Styles
        CellStyle cellStyle  = wb.createCellStyle();
        cellStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        // Create Cells
        row.createCell(0);
        row.createCell(1);
        row.createCell(2);
        row.createCell(3);
        row.createCell(4);

        if (WorkflowStatsDb.indexOf(stat) == WorkflowStatsDb.size() - 1) {
            row.getCell(0).setCellValue(stat.getTotalMonthsMessage());

            row.getCell(0).setCellStyle(cellStyle);
            row.getCell(1).setCellStyle(cellStyle);
            row.getCell(2).setCellStyle(cellStyle);
            row.getCell(3).setCellStyle(cellStyle);
            row.getCell(4).setCellStyle(cellStyle);
        } else {
            row.getCell(0).setCellValue(sdf.format(stat.getWorkflowMonth()));
        }

        row.getCell(1).setCellValue(stat.getTenantOwner());
        row.getCell(2).setCellValue(stat.getTotalTasks());
        row.getCell(3).setCellValue(stat.getCompletedTasks());
        row.getCell(4).setCellValue(stat.getOpenTasks());
    }

    private void setWorkflowStatsColumnsWidth(Workbook workflowStatsWb){
        log.debug("GenerateWorkflowStatsExcel.setWorkflowStatsColumnsWidth :: Method Entered");
        Sheet workflowStatsSheet = workflowStatsWb.getSheet("Workflow Stats");

        int numberOfSheetCols = workflowStatsSheet.getRow(0).getPhysicalNumberOfCells();
        for (int columns = 0; columns < numberOfSheetCols; columns++){
            workflowStatsSheet.setColumnWidth(columns, 8500);
        }

        log.debug("GenerateWorkflowStatsExcel.setWorkflowStatsColumnsWidth :: Method Returned");
    }

    private byte[] writeWorkflowStatsWbBytes(Workbook workflowStatsWb){
        log.debug("GenerateWorkflowStatsExcel.writeWorkflowStatsWbBytes :: Method Entered");

        byte[] workflowStatsByte;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            workflowStatsWb.write(baos);
            workflowStatsByte = baos.toByteArray();
        }
        catch (IOException ex){
            log.error("GenerateWorkflowStatsExcel.writeWorkflowStatsWbBytes:: Error", ex);
            return null;
        }

        return workflowStatsByte;
    }

}