package gr.cosmote.ucmdb.commons.logging;

import org.kie.api.event.rule.ObjectDeletedEvent;
import org.kie.api.event.rule.ObjectInsertedEvent;
import org.kie.api.event.rule.ObjectUpdatedEvent;
import org.kie.api.event.rule.RuleRuntimeEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RuleEventLogger implements RuleRuntimeEventListener {
    private static final Logger log = LoggerFactory.getLogger(RuleEventLogger.class);

    @Override
    public void objectInserted(ObjectInsertedEvent objectInsertedEvent) {
        log.info("rule:objectInserted():{}", objectInsertedEvent);
    }

    @Override
    public void objectUpdated(ObjectUpdatedEvent objectUpdatedEvent) {
        log.info("rule:objectUpdated():{}", objectUpdatedEvent);
    }

    @Override
    public void objectDeleted(ObjectDeletedEvent objectDeletedEvent) {
        log.info("rule:objectDeleted():{}", objectDeletedEvent);
    }
}
