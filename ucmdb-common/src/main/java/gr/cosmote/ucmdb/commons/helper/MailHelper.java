package gr.cosmote.ucmdb.commons.helper;

import gr.cosmote.ucmdb.commons.exceptions.ApplicationException;
import gr.cosmote.ucmdb.commons.properties.ApplicationProperties;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;

import javax.activation.DataSource;
import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import java.util.Properties;

@Slf4j
public class MailHelper {
    private static final String RELAY_SMTP = ApplicationProperties.getInstance().getProperty("MAIL_HOST");
    private static final String FROM = "ucmdb@cosmote.gr";
    private static final String SUBJECT = "uCMDB Notification";


    final static InputStream HTML_TEMPLATE = MailHelper.class.getClassLoader().getResourceAsStream("resources/mail-html-template.html");
    final static InputStream LOGO_IMAGE = MailHelper.class.getClassLoader().getResourceAsStream("resources/assets/logo.png");

    private static String htmlTemplate;
    private static DataSource byteArrayDataSource;

    static {
        try {
            htmlTemplate = IOUtils.toString(HTML_TEMPLATE, StandardCharsets.UTF_8.name());
            byteArrayDataSource = new ByteArrayDataSource(LOGO_IMAGE, "image/png");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void sendPlainEmail(String username, String recipients) throws MessagingException {
        log.debug(" MailHelper.sendPlainEmail :: Entered Method with input {} ", recipients);

        if ("NULL".equals(RELAY_SMTP)) {
            return;
        }

        Session ucmdbEmailSession = getSession("smtp.gmail.com");

        String[] recipientsArray = recipients.split(";");

        Message message = new MimeMessage(ucmdbEmailSession);
        message.setSubject(SUBJECT);
        message.setFrom(new InternetAddress("uCMBD"));
        message.setSentDate(new Date());
        message.setText("");

        for (String recipient : recipientsArray) {
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
        }

        Transport.send(message);

        log.debug(" MailHelper.sendPlainEmail :: Successfully sent email {}", message);
    }

    public static void sendHTMLEmail(String title, String messageBody, String comments, String recipients, List<String> nodesWithOsType) {
        log.debug(" MailHelper.sendHTMLEmail :: Entered Method with input {} ", recipients);

        if ("NULL".equals(RELAY_SMTP)) {
            return;
        }

        try {
            Transport.send(createEmailMessage(title, messageBody, comments, recipients, nodesWithOsType));
        } catch (Exception ex) {
            throw new ApplicationException("MailHelper :: sentHTMLEmail :: ERROR: " + ex);
        }

        log.debug(" MailHelper.sendHTMLEmail :: Successfully sent email {}");
    }

    private static Session getSession(String host) {
        log.debug(" MailHelper.getSession :: Entered Method with input {} ", host);

        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.debug", "true");

        log.debug(" MailHelper.getSession :: Method return session {} ");

        return Session.getInstance(properties);
    }

    // For Testing Purposes!!
    private static Session getGmailSession() {
        log.debug(" MailHelper.getGmailSession :: Entered Method");

        final String username = "vasilios.bouzas@gmail.com";
        final String password = "1m9ugpj66BILL";

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.debug", "true");
        props.put("mail.smtp.ssl.enable", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");

        log.debug(" MailHelper.getGmailSession :: Method return session {} ");

        return Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
    }

    private static Message createEmailMessage(String title, String messageBody, String comments, String recipients, List<String> nodesWithOsType) throws Exception {
        // Create Mail Session
        Session ucmdbEmailSession = getSession(RELAY_SMTP);
        ucmdbEmailSession.setDebug(true);

        // Create Email Message
        Message message = new MimeMessage(ucmdbEmailSession);

        /*E-mail message's set-up*/
        message.setFrom(new InternetAddress(FROM));
        message.setSubject(SUBJECT);
        message.setSentDate(new Date());

        Multipart multiPart = new MimeMultipart();
        MimeBodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent(createHTMLTemplate(title, messageBody, comments, nodesWithOsType), "text/html");
        multiPart.addBodyPart(htmlPart);

        /*Add the logo image to the e-mail sent.*/
        MimeBodyPart imagePart = new MimeBodyPart();

        /*Create a DataSource from an InputStream*/
        imagePart.setDataHandler(new DataHandler(byteArrayDataSource));
        imagePart.setHeader("Content-ID", "<image>");
        multiPart.addBodyPart(imagePart);
        message.setContent(multiPart);

        /*Add the recipient list*/
        String[] recipientsArray = recipients.split(";");
        for (String recipient : recipientsArray) {
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
        }

        return message;
    }

    private static String createHTMLTemplate(String title, String messageBody, String comments, List<String> nodesDisplayLabels) {
        Document document = Jsoup.parse(htmlTemplate);

        Element titleElement = document.select("#notification-title").first();
        titleElement.text(title);

        Element mainMessage = document.select("#main-message").first();
        mainMessage.text(messageBody);

        editReassignmentCommentsDiv(document, comments);
        editNodesDisplayLabelsDiv(document, nodesDisplayLabels);

        return document.outerHtml();
    }

    private static void editReassignmentCommentsDiv(Document document, String comments) {
        Element reAssignmentCommentsDiv = document.select("#reassignment-comments").first();
        if (comments != null) {
            Element reAssignmentCommentsPre = reAssignmentCommentsDiv.select("pre").first();
            reAssignmentCommentsPre.text(comments);
        }
        else {
            reAssignmentCommentsDiv.remove();
        }
    }

    private static void editNodesDisplayLabelsDiv(Document document, List<String> nodesDisplayLabels) {
        Element nodesDisplayLabelsDiv = document.select("#nodes-display-labels").first();

        if (nodesDisplayLabels == null) {
            nodesDisplayLabelsDiv.remove();
        }
        else {
            // Manipulate Unorder List Element
            Element nodesList = nodesDisplayLabelsDiv.select("#nodes-ul").first();

            nodesDisplayLabels
                    .stream()
                    .forEach(node -> {
                        Element li = new Element(Tag.valueOf("li"), "");
                        li.text(node);

                        nodesList.append(li.outerHtml());
                    });
        }
    }
}