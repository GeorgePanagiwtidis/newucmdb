package gr.cosmote.ucmdb.commons.exceptions;

public class ApplicationException extends RuntimeException {
    public ApplicationException(String message) {
        super(message);
    }
    public ApplicationException(Throwable e) {
        super(e);
    }

    public ApplicationException(String message, Throwable t) {
        super(message, t);
    }
}
