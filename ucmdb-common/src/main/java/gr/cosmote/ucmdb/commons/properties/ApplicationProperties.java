package gr.cosmote.ucmdb.commons.properties;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.vfs2.FileChangeEvent;
import org.apache.commons.vfs2.FileListener;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.apache.commons.vfs2.impl.DefaultFileMonitor;

import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

public final class ApplicationProperties {

    private static       Properties            PROPERTIES   = new Properties();
    private static final ApplicationProperties INSTANCE     = new ApplicationProperties();
    private static final String                PRO_FILENAME = "application.properties";

    private ApplicationProperties() {
        loadProperties();

        loadWatcher();
    }

    private void loadWatcher() {
        System.out.println("ApplicationProperties.ApplicationProperties() ::: Loading Watcher"); //Using system.out because log4j is not available yet!!!
        try {
            System.out.println("ApplicationProperties.ApplicationProperties() ::: Getting Manager");
            FileSystemManager manager = VFS.getManager();

            Thread      currentThread      = Thread.currentThread();
            ClassLoader contextClassLoader = currentThread.getContextClassLoader();

            URL url = contextClassLoader.getResource(PRO_FILENAME);

            System.out.println("ApplicationProperties.ApplicationProperties() ::: Trying to resolve file " + PRO_FILENAME);
            org.apache.commons.vfs2.FileObject file = manager.resolveFile( url.getFile() );

            System.out.println("ApplicationProperties.ApplicationProperties() ::: file " + PRO_FILENAME + " resolved");
            System.out.println("ApplicationProperties.ApplicationProperties() ::: file " + PRO_FILENAME + " resolved");

            System.out.println("ApplicationProperties.ApplicationProperties() ::: Registering Default File Monitor");
            DefaultFileMonitor fm = new DefaultFileMonitor(new FileListener() {
                public void fileCreated(FileChangeEvent fileChangeEvent) throws Exception {}

                public void fileDeleted(FileChangeEvent fileChangeEvent) throws Exception {}

                public void fileChanged(FileChangeEvent fileChangeEvent) throws Exception {
                    System.out.println("ApplicationProperties.ApplicationProperties() ::: Reloading Properties Starting");
                    loadProperties();
                    System.out.println("ApplicationProperties.ApplicationProperties() ::: Reloading Properties Finished");
                }
            });
            fm.setDelay( 5000 );
            fm.addFile ( file );

            fm.start();
        } catch (Exception e) {
            throw new RuntimeException("ApplicationProperties.ApplicationProperties() ::: Unable to initialize Watcher", e);
        }
    }

    private void loadProperties() {
        Thread      currentThread      = Thread.currentThread();
        ClassLoader contextClassLoader = currentThread.getContextClassLoader();

        InputStream in = contextClassLoader.getResourceAsStream(PRO_FILENAME);

        try {
            PROPERTIES.load(in);
        } catch (Exception e) {
            throw new RuntimeException("ApplicationProperties.ApplicationProperties() ::: Unable to initialize property file");
        } finally {
            try {
                in.close();
            }catch (Exception e) {}
        }
    }

    public static ApplicationProperties getInstance() {
        return INSTANCE;
    }

    public String getProperty( String key) {
        String prop = PROPERTIES.getProperty(key);

        if ( StringUtils.isEmpty(prop) ) {
            return null;
        } else {
            return PROPERTIES.getProperty(key).trim();
        }
    }

    public String getProperty( String key, String defaultValue) {

        String prop = getProperty(key);

        if ( StringUtils.isEmpty(prop) ) {
            return defaultValue;
        } else {
            return prop;
        }
    }
}
