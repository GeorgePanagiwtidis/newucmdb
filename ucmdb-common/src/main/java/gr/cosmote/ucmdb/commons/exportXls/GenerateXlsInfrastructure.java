package gr.cosmote.ucmdb.commons.exportXls;

import gr.cosmote.ucmdb.models.base.Infrastructure;
import gr.cosmote.ucmdb.models.database.CiAttributeConfiguration;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import oracle.j2ee.ws.saaj.util.ByteOutputStream;
import org.apache.poi.hpsf.SummaryInformation;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Data
public class GenerateXlsInfrastructure {

    private List<Infrastructure> infraList;
    private List<CiAttributeConfiguration> ciAttributes;

    public byte[] generateXlsFile() {
        List<CiAttributeConfiguration> sortedAttributes = transformCiAttributesToSortedAttributes();
        log.debug("GenerateXlsInfrastructure.java :: generateXlsFile() entered ");

        HSSFWorkbook workbook = createWorkbook();
        createHeader(workbook, sortedAttributes);
        fillWorkbookWithData(workbook.getSheetAt(0), sortedAttributes);

        log.debug("GenerateXlsInfrastructure.java ::: generateXlsFile() returning {} ");
        return writeWorkbookToFile(workbook);
    }

    private List<CiAttributeConfiguration> transformCiAttributesToSortedAttributes() {
        return ciAttributes
                .stream()
                .filter(attr -> infraList.stream().anyMatch(infra -> infra.getProperties().get(attr.getAttributeName()) != null))
                .filter(attr -> attr.getAttributeLabel() != null)
                .sorted((attr1, attr2) -> {
                    if (attr1.getAttributePriority() == 100 && attr2.getAttributePriority() == 100) {
                        return attr1.getAttributeLabel().compareTo(attr2.getAttributeLabel());
                    }
                    else {
                        return attr1.getAttributePriority() - attr2.getAttributePriority();
                    }
                })
                .collect(Collectors.toList());
    }

    private HSSFWorkbook createWorkbook() {
        HSSFWorkbook workbook = new HSSFWorkbook();
        workbook.createSheet("Infrastructure Results");
        workbook.createInformationProperties();
        SummaryInformation summaryInfo = workbook.getSummaryInformation();
        summaryInfo.setAuthor("COSMOTE uCMDB");

        log.debug("GenerateXlsInfrastructure.createWorkbook :: Method Created workbook");
        return workbook;
    }

    private void createHeader(HSSFWorkbook workbook, List<CiAttributeConfiguration> attributes) {
        Sheet resultsSheet = workbook.getSheetAt(0);
        Row header = resultsSheet.createRow(0);

        int colNum = 0;
        for (CiAttributeConfiguration ciAttribute : attributes) {
            Cell headerCell = header.createCell(colNum++);

            headerCell.setCellStyle(createHeaderCellStyle(workbook));
            headerCell.setCellValue(ciAttribute.getAttributeLabel());
        }
    }

    private CellStyle createHeaderCellStyle(HSSFWorkbook workbook) {
        // Style the Cell
        CellStyle style = workbook.createCellStyle();

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);

        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFont(headerFont);
        style.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        return style;
    }

    private void fillWorkbookWithData(Sheet sheet, List<CiAttributeConfiguration> attributes) {
        int rowNum = 1;
        for (Infrastructure infra : infraList) {
            Row row = sheet.createRow(rowNum++);

            int cellNum = 0;
            for (CiAttributeConfiguration ciAttribute : attributes) {
                row.createCell(cellNum++).setCellValue(infra.getProperties().get(ciAttribute.getAttributeName()));
            }
        }

        autoWidthColumns(sheet);
    }

    private byte[] writeWorkbookToFile(HSSFWorkbook workbook) {
        byte[] byteArray;

        try (ByteOutputStream byteOutputStream = new ByteOutputStream()) {
            workbook.write(byteOutputStream);

            byteArray = byteOutputStream.getBytes();
        } catch (IOException ioe) {
            log.error("GenerateXlsInfrastructure.writeWorkbookToFile :: An ERROR occured, returning empty array {}", ioe);
            byteArray = new byte[0];
        } finally {
            try {
                workbook.close();
            } catch (IOException ex) {
                workbook = null;
            }
        }

        return byteArray;
    }

    private void autoWidthColumns(Sheet resultSheet) {
        short numOfCells = resultSheet.getRow(0).getLastCellNum();

        for (int col = 0; col < numOfCells; col++) {
            resultSheet.autoSizeColumn(col);
        }
    }
}
