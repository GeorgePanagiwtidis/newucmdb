
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Topology complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Topology"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CIs" type="{http://schemas.hp.com/ucmdb/1/types}CIs"/&gt;
 *         &lt;element name="relations" type="{http://schemas.hp.com/ucmdb/1/types}Relations" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Topology", propOrder = {
    "cIs",
    "relations"
})
public class Topology {

    @XmlElement(name = "CIs", required = true)
    protected CIs cIs;
    protected Relations relations;

    /**
     * Gets the value of the cIs property.
     * 
     * @return
     *     possible object is
     *     {@link CIs }
     *     
     */
    public CIs getCIs() {
        return cIs;
    }

    /**
     * Sets the value of the cIs property.
     * 
     * @param value
     *     allowed object is
     *     {@link CIs }
     *     
     */
    public void setCIs(CIs value) {
        this.cIs = value;
    }

    /**
     * Gets the value of the relations property.
     * 
     * @return
     *     possible object is
     *     {@link Relations }
     *     
     */
    public Relations getRelations() {
        return relations;
    }

    /**
     * Sets the value of the relations property.
     * 
     * @param value
     *     allowed object is
     *     {@link Relations }
     *     
     */
    public void setRelations(Relations value) {
        this.relations = value;
    }

}
