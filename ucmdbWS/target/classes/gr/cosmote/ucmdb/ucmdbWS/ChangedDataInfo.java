
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChangedDataInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChangedDataInfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="changedIDs" type="{http://schemas.hp.com/ucmdb/1/types}IDs"/&gt;
 *         &lt;element name="rootID" type="{http://schemas.hp.com/ucmdb/1/types}ID"/&gt;
 *         &lt;element name="changeType"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="NO_CHANGE"/&gt;
 *               &lt;enumeration value="ROOT_CHANGED"/&gt;
 *               &lt;enumeration value="RELATED_OBJECT_CHANGED"/&gt;
 *               &lt;enumeration value="ROOT_AND_OBJECT_CHANGED"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangedDataInfo", namespace = "http://schemas.hp.com/ucmdb/1/types/history", propOrder = {
    "changedIDs",
    "rootID",
    "changeType"
})
public class ChangedDataInfo {

    @XmlElement(required = true)
    protected IDs changedIDs;
    @XmlElement(required = true)
    protected ID rootID;
    @XmlElement(required = true)
    protected String changeType;

    /**
     * Gets the value of the changedIDs property.
     * 
     * @return
     *     possible object is
     *     {@link IDs }
     *     
     */
    public IDs getChangedIDs() {
        return changedIDs;
    }

    /**
     * Sets the value of the changedIDs property.
     * 
     * @param value
     *     allowed object is
     *     {@link IDs }
     *     
     */
    public void setChangedIDs(IDs value) {
        this.changedIDs = value;
    }

    /**
     * Gets the value of the rootID property.
     * 
     * @return
     *     possible object is
     *     {@link ID }
     *     
     */
    public ID getRootID() {
        return rootID;
    }

    /**
     * Sets the value of the rootID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID }
     *     
     */
    public void setRootID(ID value) {
        this.rootID = value;
    }

    /**
     * Gets the value of the changeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeType() {
        return changeType;
    }

    /**
     * Sets the value of the changeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeType(String value) {
        this.changeType = value;
    }

}
