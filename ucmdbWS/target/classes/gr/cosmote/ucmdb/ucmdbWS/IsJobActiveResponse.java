
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for isJobActiveResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="isJobActiveResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="JobState" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "isJobActiveResponse", namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", propOrder = {
    "jobState"
})
public class IsJobActiveResponse {

    @XmlElement(name = "JobState")
    protected boolean jobState;

    /**
     * Gets the value of the jobState property.
     * 
     */
    public boolean isJobState() {
        return jobState;
    }

    /**
     * Sets the value of the jobState property.
     * 
     */
    public void setJobState(boolean value) {
        this.jobState = value;
    }

}
