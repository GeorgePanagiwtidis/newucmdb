
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IntCondition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntCondition"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://schemas.hp.com/ucmdb/1/types}Condition"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="condition" type="{http://schemas.hp.com/ucmdb/1/types}IntProp"/&gt;
 *         &lt;element name="intOperator"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="Equal"/&gt;
 *               &lt;enumeration value="NotEqual"/&gt;
 *               &lt;enumeration value="Greater"/&gt;
 *               &lt;enumeration value="GreaterEqual"/&gt;
 *               &lt;enumeration value="Less"/&gt;
 *               &lt;enumeration value="LessEqual"/&gt;
 *               &lt;enumeration value="In"/&gt;
 *               &lt;enumeration value="IsNull"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntCondition", propOrder = {
    "condition",
    "intOperator"
})
public class IntCondition
    extends Condition
{

    @XmlElement(required = true)
    protected IntProp condition;
    @XmlElement(required = true)
    protected String intOperator;

    /**
     * Gets the value of the condition property.
     * 
     * @return
     *     possible object is
     *     {@link IntProp }
     *     
     */
    public IntProp getCondition() {
        return condition;
    }

    /**
     * Sets the value of the condition property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntProp }
     *     
     */
    public void setCondition(IntProp value) {
        this.condition = value;
    }

    /**
     * Gets the value of the intOperator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntOperator() {
        return intOperator;
    }

    /**
     * Sets the value of the intOperator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntOperator(String value) {
        this.intOperator = value;
    }

}
