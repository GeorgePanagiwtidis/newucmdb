
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ImpactRelation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ImpactRelation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ID" type="{http://schemas.hp.com/ucmdb/1/types}ID"/&gt;
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="end1ID" type="{http://schemas.hp.com/ucmdb/1/types}ID"/&gt;
 *         &lt;element name="end2ID" type="{http://schemas.hp.com/ucmdb/1/types}ID"/&gt;
 *         &lt;element name="rule" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="action" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ImpactRelation", namespace = "http://schemas.hp.com/ucmdb/1/types/impact", propOrder = {
    "id",
    "type",
    "end1ID",
    "end2ID",
    "rule",
    "action"
})
public class ImpactRelation {

    @XmlElement(name = "ID", required = true)
    protected ID id;
    @XmlElement(required = true)
    protected String type;
    @XmlElement(required = true)
    protected ID end1ID;
    @XmlElement(required = true)
    protected ID end2ID;
    @XmlElement(required = true)
    protected String rule;
    @XmlElement(required = true)
    protected String action;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link ID }
     *     
     */
    public ID getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID }
     *     
     */
    public void setID(ID value) {
        this.id = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the end1ID property.
     * 
     * @return
     *     possible object is
     *     {@link ID }
     *     
     */
    public ID getEnd1ID() {
        return end1ID;
    }

    /**
     * Sets the value of the end1ID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID }
     *     
     */
    public void setEnd1ID(ID value) {
        this.end1ID = value;
    }

    /**
     * Gets the value of the end2ID property.
     * 
     * @return
     *     possible object is
     *     {@link ID }
     *     
     */
    public ID getEnd2ID() {
        return end2ID;
    }

    /**
     * Sets the value of the end2ID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID }
     *     
     */
    public void setEnd2ID(ID value) {
        this.end2ID = value;
    }

    /**
     * Gets the value of the rule property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRule() {
        return rule;
    }

    /**
     * Sets the value of the rule property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRule(String value) {
        this.rule = value;
    }

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAction(String value) {
        this.action = value;
    }

}
