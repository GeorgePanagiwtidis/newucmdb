
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getAllClassesHierarchyResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getAllClassesHierarchyResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="classesHierarchy" type="{http://schemas.hp.com/ucmdb/1/types/classmodel}UcmdbClassModelHierarchy"/&gt;
 *         &lt;element name="comments" type="{http://schemas.hp.com/ucmdb/1/types}ResComments"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAllClassesHierarchyResponse", namespace = "http://schemas.hp.com/ucmdb/1/params/classmodel", propOrder = {
    "classesHierarchy",
    "comments"
})
public class GetAllClassesHierarchyResponse {

    @XmlElement(required = true)
    protected UcmdbClassModelHierarchy classesHierarchy;
    @XmlElement(required = true)
    protected ResComments comments;

    /**
     * Gets the value of the classesHierarchy property.
     * 
     * @return
     *     possible object is
     *     {@link UcmdbClassModelHierarchy }
     *     
     */
    public UcmdbClassModelHierarchy getClassesHierarchy() {
        return classesHierarchy;
    }

    /**
     * Sets the value of the classesHierarchy property.
     * 
     * @param value
     *     allowed object is
     *     {@link UcmdbClassModelHierarchy }
     *     
     */
    public void setClassesHierarchy(UcmdbClassModelHierarchy value) {
        this.classesHierarchy = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link ResComments }
     *     
     */
    public ResComments getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResComments }
     *     
     */
    public void setComments(ResComments value) {
        this.comments = value;
    }

}
