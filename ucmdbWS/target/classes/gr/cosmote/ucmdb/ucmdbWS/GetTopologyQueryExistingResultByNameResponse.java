
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getTopologyQueryExistingResultByNameResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getTopologyQueryExistingResultByNameResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="topologyMap" type="{http://schemas.hp.com/ucmdb/1/types}TopologyMap"/&gt;
 *         &lt;element name="chunkInfo" type="{http://schemas.hp.com/ucmdb/1/types}ChunkInfo"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getTopologyQueryExistingResultByNameResponse", namespace = "http://schemas.hp.com/ucmdb/1/params/query", propOrder = {
    "topologyMap",
    "chunkInfo"
})
public class GetTopologyQueryExistingResultByNameResponse {

    @XmlElement(required = true)
    protected TopologyMap topologyMap;
    @XmlElement(required = true)
    protected ChunkInfo chunkInfo;

    /**
     * Gets the value of the topologyMap property.
     * 
     * @return
     *     possible object is
     *     {@link TopologyMap }
     *     
     */
    public TopologyMap getTopologyMap() {
        return topologyMap;
    }

    /**
     * Sets the value of the topologyMap property.
     * 
     * @param value
     *     allowed object is
     *     {@link TopologyMap }
     *     
     */
    public void setTopologyMap(TopologyMap value) {
        this.topologyMap = value;
    }

    /**
     * Gets the value of the chunkInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ChunkInfo }
     *     
     */
    public ChunkInfo getChunkInfo() {
        return chunkInfo;
    }

    /**
     * Sets the value of the chunkInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChunkInfo }
     *     
     */
    public void setChunkInfo(ChunkInfo value) {
        this.chunkInfo = value;
    }

}
