
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ClassModelQueryGetClassModelIconPathsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClassModelQueryGetClassModelIconPathsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="relativePaths" type="{http://schemas.hp.com/ucmdb/ui/1/types}ClassNameAndIcons"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClassModelQueryGetClassModelIconPathsResponse", namespace = "http://schemas.hp.com/ucmdb/ui/1/params", propOrder = {
    "relativePaths"
})
public class ClassModelQueryGetClassModelIconPathsResponse {

    @XmlElement(required = true)
    protected ClassNameAndIcons relativePaths;

    /**
     * Gets the value of the relativePaths property.
     * 
     * @return
     *     possible object is
     *     {@link ClassNameAndIcons }
     *     
     */
    public ClassNameAndIcons getRelativePaths() {
        return relativePaths;
    }

    /**
     * Sets the value of the relativePaths property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClassNameAndIcons }
     *     
     */
    public void setRelativePaths(ClassNameAndIcons value) {
        this.relativePaths = value;
    }

}
