
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addCIsAndRelations complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addCIsAndRelations"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="cmdbContext" type="{http://schemas.hp.com/ucmdb/1/types}CmdbContext"/&gt;
 *         &lt;element name="dataStore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="updateExisting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="CIsAndRelationsUpdates" type="{http://schemas.hp.com/ucmdb/1/types/update}CIsAndRelationsUpdates"/&gt;
 *         &lt;element name="ignoreValidation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addCIsAndRelations", namespace = "http://schemas.hp.com/ucmdb/1/params/update", propOrder = {
    "cmdbContext",
    "dataStore",
    "updateExisting",
    "cIsAndRelationsUpdates",
    "ignoreValidation"
})
public class AddCIsAndRelations {

    @XmlElement(required = true)
    protected CmdbContext cmdbContext;
    protected String dataStore;
    @XmlElementRef(name = "updateExisting", namespace = "http://schemas.hp.com/ucmdb/1/params/update", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> updateExisting;
    @XmlElement(name = "CIsAndRelationsUpdates", required = true)
    protected CIsAndRelationsUpdates cIsAndRelationsUpdates;
    @XmlElementRef(name = "ignoreValidation", namespace = "http://schemas.hp.com/ucmdb/1/params/update", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> ignoreValidation;

    /**
     * Gets the value of the cmdbContext property.
     * 
     * @return
     *     possible object is
     *     {@link CmdbContext }
     *     
     */
    public CmdbContext getCmdbContext() {
        return cmdbContext;
    }

    /**
     * Sets the value of the cmdbContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link CmdbContext }
     *     
     */
    public void setCmdbContext(CmdbContext value) {
        this.cmdbContext = value;
    }

    /**
     * Gets the value of the dataStore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataStore() {
        return dataStore;
    }

    /**
     * Sets the value of the dataStore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataStore(String value) {
        this.dataStore = value;
    }

    /**
     * Gets the value of the updateExisting property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getUpdateExisting() {
        return updateExisting;
    }

    /**
     * Sets the value of the updateExisting property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setUpdateExisting(JAXBElement<Boolean> value) {
        this.updateExisting = value;
    }

    /**
     * Gets the value of the cIsAndRelationsUpdates property.
     * 
     * @return
     *     possible object is
     *     {@link CIsAndRelationsUpdates }
     *     
     */
    public CIsAndRelationsUpdates getCIsAndRelationsUpdates() {
        return cIsAndRelationsUpdates;
    }

    /**
     * Sets the value of the cIsAndRelationsUpdates property.
     * 
     * @param value
     *     allowed object is
     *     {@link CIsAndRelationsUpdates }
     *     
     */
    public void setCIsAndRelationsUpdates(CIsAndRelationsUpdates value) {
        this.cIsAndRelationsUpdates = value;
    }

    /**
     * Gets the value of the ignoreValidation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIgnoreValidation() {
        return ignoreValidation;
    }

    /**
     * Sets the value of the ignoreValidation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIgnoreValidation(JAXBElement<Boolean> value) {
        this.ignoreValidation = value;
    }

}
