
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomProperties complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomProperties"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="propertiesList" type="{http://schemas.hp.com/ucmdb/1/types/props}PropertiesList" minOccurs="0"/&gt;
 *         &lt;element name="predefinedProperties" type="{http://schemas.hp.com/ucmdb/1/types/props}PredefinedProperties" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomProperties", namespace = "http://schemas.hp.com/ucmdb/1/types/props", propOrder = {
    "propertiesList",
    "predefinedProperties"
})
public class CustomProperties {

    protected PropertiesList propertiesList;
    protected PredefinedProperties predefinedProperties;

    /**
     * Gets the value of the propertiesList property.
     * 
     * @return
     *     possible object is
     *     {@link PropertiesList }
     *     
     */
    public PropertiesList getPropertiesList() {
        return propertiesList;
    }

    /**
     * Sets the value of the propertiesList property.
     * 
     * @param value
     *     allowed object is
     *     {@link PropertiesList }
     *     
     */
    public void setPropertiesList(PropertiesList value) {
        this.propertiesList = value;
    }

    /**
     * Gets the value of the predefinedProperties property.
     * 
     * @return
     *     possible object is
     *     {@link PredefinedProperties }
     *     
     */
    public PredefinedProperties getPredefinedProperties() {
        return predefinedProperties;
    }

    /**
     * Sets the value of the predefinedProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link PredefinedProperties }
     *     
     */
    public void setPredefinedProperties(PredefinedProperties value) {
        this.predefinedProperties = value;
    }

}
