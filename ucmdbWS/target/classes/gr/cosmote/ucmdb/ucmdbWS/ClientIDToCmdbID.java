
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ClientIDToCmdbID complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClientIDToCmdbID"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="clientID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="cmdbID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClientIDToCmdbID", namespace = "http://schemas.hp.com/ucmdb/1/types/update", propOrder = {
    "clientID",
    "cmdbID"
})
public class ClientIDToCmdbID {

    @XmlElement(required = true)
    protected String clientID;
    @XmlElement(required = true)
    protected String cmdbID;

    /**
     * Gets the value of the clientID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientID() {
        return clientID;
    }

    /**
     * Sets the value of the clientID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientID(String value) {
        this.clientID = value;
    }

    /**
     * Gets the value of the cmdbID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCmdbID() {
        return cmdbID;
    }

    /**
     * Sets the value of the cmdbID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCmdbID(String value) {
        this.cmdbID = value;
    }

}
