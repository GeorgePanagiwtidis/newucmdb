
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for setTriggerTQLProbesLimitRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="setTriggerTQLProbesLimitRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="jobName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="tqlName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="probesLimit" type="{http://schemas.hp.com/ucmdb/1/types}strList"/&gt;
 *         &lt;element name="cmdbContext" type="{http://schemas.hp.com/ucmdb/1/types}CmdbContext"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "setTriggerTQLProbesLimitRequest", namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", propOrder = {
    "jobName",
    "tqlName",
    "probesLimit",
    "cmdbContext"
})
public class SetTriggerTQLProbesLimitRequest {

    @XmlElement(required = true)
    protected String jobName;
    @XmlElement(required = true)
    protected String tqlName;
    @XmlElement(required = true)
    protected StrList probesLimit;
    @XmlElement(required = true)
    protected CmdbContext cmdbContext;

    /**
     * Gets the value of the jobName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobName() {
        return jobName;
    }

    /**
     * Sets the value of the jobName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobName(String value) {
        this.jobName = value;
    }

    /**
     * Gets the value of the tqlName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTqlName() {
        return tqlName;
    }

    /**
     * Sets the value of the tqlName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTqlName(String value) {
        this.tqlName = value;
    }

    /**
     * Gets the value of the probesLimit property.
     * 
     * @return
     *     possible object is
     *     {@link StrList }
     *     
     */
    public StrList getProbesLimit() {
        return probesLimit;
    }

    /**
     * Sets the value of the probesLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrList }
     *     
     */
    public void setProbesLimit(StrList value) {
        this.probesLimit = value;
    }

    /**
     * Gets the value of the cmdbContext property.
     * 
     * @return
     *     possible object is
     *     {@link CmdbContext }
     *     
     */
    public CmdbContext getCmdbContext() {
        return cmdbContext;
    }

    /**
     * Sets the value of the cmdbContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link CmdbContext }
     *     
     */
    public void setCmdbContext(CmdbContext value) {
        this.cmdbContext = value;
    }

}
