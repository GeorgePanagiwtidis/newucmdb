
package gr.cosmote.ucmdb.ucmdbWS;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gr.cosmote.ucmdb.ucmdbWS package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetTopologyQueryResultCountByNameResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "getTopologyQueryResultCountByNameResponse");
    private final static QName _GetTopologyQueryResultCountByName_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "getTopologyQueryResultCountByName");
    private final static QName _GetTopologyQueryExistingResultByName_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "getTopologyQueryExistingResultByName");
    private final static QName _GetTopologyQueryExistingResultByNameResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "getTopologyQueryExistingResultByNameResponse");
    private final static QName _ExecuteTopologyQueryByName_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "executeTopologyQueryByName");
    private final static QName _ExecuteTopologyQueryByNameResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "executeTopologyQueryByNameResponse");
    private final static QName _GetCIsByType_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "getCIsByType");
    private final static QName _GetCIsByTypeResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "getCIsByTypeResponse");
    private final static QName _GetFilteredCIsByType_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "getFilteredCIsByType");
    private final static QName _GetFilteredCIsByTypeResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "getFilteredCIsByTypeResponse");
    private final static QName _GetCIsById_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "getCIsById");
    private final static QName _GetCIsByIdResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "getCIsByIdResponse");
    private final static QName _GetCIsByIdWithEnumValues_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "getCIsByIdWithEnumValues");
    private final static QName _GetCIsByIdWithEnumValuesResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "getCIsByIdWithEnumValuesResponse");
    private final static QName _PullTopologyMapChunks_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "pullTopologyMapChunks");
    private final static QName _PullTopologyMapChunksResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "pullTopologyMapChunksResponse");
    private final static QName _GetQueryNameOfView_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "getQueryNameOfView");
    private final static QName _GetQueryNameOfViewResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "getQueryNameOfViewResponse");
    private final static QName _ExecuteTopologyQueryByNameWithParameters_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "executeTopologyQueryByNameWithParameters");
    private final static QName _ExecuteTopologyQueryByNameWithParametersResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "executeTopologyQueryByNameWithParametersResponse");
    private final static QName _ExecuteTopologyQueryWithParameters_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "executeTopologyQueryWithParameters");
    private final static QName _ExecuteTopologyQueryWithParametersResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "executeTopologyQueryWithParametersResponse");
    private final static QName _GetCINeighbours_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "getCINeighbours");
    private final static QName _GetCINeighboursResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "getCINeighboursResponse");
    private final static QName _ReleaseChunks_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "releaseChunks");
    private final static QName _ReleaseChunksResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "releaseChunksResponse");
    private final static QName _GetRelationsById_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "getRelationsById");
    private final static QName _GetRelationsByIdResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "getRelationsByIdResponse");
    private final static QName _ChunkRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/1/types", "ChunkRequest");
    private final static QName _CreatedIDsMap_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/update", "CreatedIDsMap");
    private final static QName _AddCIsAndRelations_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/update", "addCIsAndRelations");
    private final static QName _AddCIsAndRelationsResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/update", "addCIsAndRelationsResponse");
    private final static QName _UpdateCIsAndRelations_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/update", "updateCIsAndRelations");
    private final static QName _UpdateCIsAndRelationsResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/update", "updateCIsAndRelationsResponse");
    private final static QName _DeleteCIsAndRelations_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/update", "deleteCIsAndRelations");
    private final static QName _DeleteCIsAndRelationsResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/update", "deleteCIsAndRelationsResponse");
    private final static QName _CalculateImpact_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/impact", "calculateImpact");
    private final static QName _CalculateImpactResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/impact", "calculateImpactResponse");
    private final static QName _GetImpactRulesByNamePrefix_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/impact", "getImpactRulesByNamePrefix");
    private final static QName _GetImpactRulesByNamePrefixResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/impact", "getImpactRulesByNamePrefixResponse");
    private final static QName _GetImpactPath_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/impact", "getImpactPath");
    private final static QName _GetImpactPathResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/impact", "getImpactPathResponse");
    private final static QName _GetImpactRulesByGroupName_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/impact", "getImpactRulesByGroupName");
    private final static QName _GetImpactRulesByGroupNameResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/impact", "getImpactRulesByGroupNameResponse");
    private final static QName _ImpactPathTopology_QNAME = new QName("http://schemas.hp.com/ucmdb/1/types/impact", "ImpactPathTopology");
    private final static QName _GetCmdbClassDefinition_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/classmodel", "getCmdbClassDefinition");
    private final static QName _GetCmdbClassDefinitionResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/classmodel", "getCmdbClassDefinitionResponse");
    private final static QName _GetAllClassesHierarchy_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/classmodel", "getAllClassesHierarchy");
    private final static QName _GetAllClassesHierarchyResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/classmodel", "getAllClassesHierarchyResponse");
    private final static QName _GetClassAncestors_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/classmodel", "getClassAncestors");
    private final static QName _GetClassAncestorsResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/classmodel", "getClassAncestorsResponse");
    private final static QName _UcmdbClass_QNAME = new QName("http://schemas.hp.com/ucmdb/1/types/classmodel", "UcmdbClass");
    private final static QName _GetChangedCIs_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/history", "getChangedCIs");
    private final static QName _GetChangedCIsResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/history", "getChangedCIsResponse");
    private final static QName _IsJobActiveRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "isJobActiveRequest");
    private final static QName _IsJobActiveResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "isJobActiveResponse");
    private final static QName _GetDiscoveryJobsNamesRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getDiscoveryJobsNamesRequest");
    private final static QName _GetDiscoveryJobsNamesResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getDiscoveryJobsNamesResponse");
    private final static QName _ActivateJobRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "activateJobRequest");
    private final static QName _DeactivateJobRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "deactivateJobRequest");
    private final static QName _AddTriggerTQLRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "addTriggerTQLRequest");
    private final static QName _RemoveTriggerTQLRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "removeTriggerTQLRequest");
    private final static QName _DisableTriggerTQLRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "disableTriggerTQLRequest");
    private final static QName _SetTriggerTQLProbesLimitRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "setTriggerTQLProbesLimitRequest");
    private final static QName _GetDomainsNamesRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getDomainsNamesRequest");
    private final static QName _GetDomainsNamesResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getDomainsNamesResponse");
    private final static QName _GetDomainTypeRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getDomainTypeRequest");
    private final static QName _GetDomainTypeResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getDomainTypeResponse");
    private final static QName _GetProbesNamesRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getProbesNamesRequest");
    private final static QName _GetProbesNamesResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getProbesNamesResponse");
    private final static QName _GetProbeIPsRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getProbeIPsRequest");
    private final static QName _GetProbeIPsResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getProbeIPsResponse");
    private final static QName _GetProbeScopeRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getProbeScopeRequest");
    private final static QName _GetProbeScopeResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getProbeScopeResponse");
    private final static QName _UpdateProbeScopeRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "updateProbeScopeRequest");
    private final static QName _GetJobTriggerTqlsRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getJobTriggerTqlsRequest");
    private final static QName _GetJobTriggerTqlsResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getJobTriggerTqlsResponse");
    private final static QName _GetCredentialsEntriesIDsRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getCredentialsEntriesIDsRequest");
    private final static QName _GetCredentialsEntriesIDsResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getCredentialsEntriesIDsResponse");
    private final static QName _GetCredentialsEntryRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getCredentialsEntryRequest");
    private final static QName _GetCredentialsEntryResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "getCredentialsEntryResponse");
    private final static QName _AddCredentialsEntryRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "addCredentialsEntryRequest");
    private final static QName _AddCredentialsEntryResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "addCredentialsEntryResponse");
    private final static QName _UpdateCredentialsEntryRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "updateCredentialsEntryRequest");
    private final static QName _RemoveCredentialsEntryRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "removeCredentialsEntryRequest");
    private final static QName _AddTriggerCIRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "addTriggerCIRequest");
    private final static QName _RemoveTriggerCIRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "removeTriggerCIRequest");
    private final static QName _DispatchAdHocJobResopnse_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "dispatchAdHocJobResopnse");
    private final static QName _DispatchAdHocJobRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "dispatchAdHocJobRequest");
    private final static QName _IsProbeConnectedRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "isProbeConnectedRequest");
    private final static QName _IsProbeConnectedResponce_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "isProbeConnectedResponce");
    private final static QName _RediscoverCIsRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "rediscoverCIsRequest");
    private final static QName _RediscoverCIsResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "rediscoverCIsResponse");
    private final static QName _RediscoverViewCIsRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "rediscoverViewCIsRequest");
    private final static QName _RediscoverViewCIsResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "rediscoverViewCIsResponse");
    private final static QName _CheckViewDiscoveryProgressRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "checkViewDiscoveryProgressRequest");
    private final static QName _CheckViewDiscoveryProgressResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "checkViewDiscoveryProgressResponse");
    private final static QName _CheckDiscoveryProgressRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "checkDiscoveryProgressRequest");
    private final static QName _CheckDiscoveryProgressResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/discovery/1/params", "checkDiscoveryProgressResponse");
    private final static QName _ClassModelQueryGetClassModelIconPathsRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/ui/1/params", "ClassModelQueryGetClassModelIconPathsRequest");
    private final static QName _ClassModelQueryGetClassModelIconPathsResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/ui/1/params", "ClassModelQueryGetClassModelIconPathsResponse");
    private final static QName _AddCustomerRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/management/1/params", "addCustomerRequest");
    private final static QName _AddCustomerResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/management/1/params", "addCustomerResponse");
    private final static QName _RemoveCustomerRequest_QNAME = new QName("http://schemas.hp.com/ucmdb/management/1/params", "removeCustomerRequest");
    private final static QName _RemoveCustomerResponse_QNAME = new QName("http://schemas.hp.com/ucmdb/management/1/params", "removeCustomerResponse");
    private final static QName _UpdateCIsAndRelationsIgnoreValidation_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/update", "ignoreValidation");
    private final static QName _AddCIsAndRelationsUpdateExisting_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/update", "updateExisting");
    private final static QName _XmlPropValue_QNAME = new QName("http://schemas.hp.com/ucmdb/1/types", "value");
    private final static QName _StrListPropStrValues_QNAME = new QName("http://schemas.hp.com/ucmdb/1/types", "strValues");
    private final static QName _IntListPropIntValues_QNAME = new QName("http://schemas.hp.com/ucmdb/1/types", "intValues");
    private final static QName _GetTopologyQueryResultCountByNameCountInvisible_QNAME = new QName("http://schemas.hp.com/ucmdb/1/params/query", "countInvisible");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gr.cosmote.ucmdb.ucmdbWS
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Scope }
     * 
     */
    public Scope createScope() {
        return new Scope();
    }

    /**
     * Create an instance of {@link GetTopologyQueryResultCountByNameResponse }
     * 
     */
    public GetTopologyQueryResultCountByNameResponse createGetTopologyQueryResultCountByNameResponse() {
        return new GetTopologyQueryResultCountByNameResponse();
    }

    /**
     * Create an instance of {@link GetTopologyQueryResultCountByName }
     * 
     */
    public GetTopologyQueryResultCountByName createGetTopologyQueryResultCountByName() {
        return new GetTopologyQueryResultCountByName();
    }

    /**
     * Create an instance of {@link GetTopologyQueryExistingResultByName }
     * 
     */
    public GetTopologyQueryExistingResultByName createGetTopologyQueryExistingResultByName() {
        return new GetTopologyQueryExistingResultByName();
    }

    /**
     * Create an instance of {@link GetTopologyQueryExistingResultByNameResponse }
     * 
     */
    public GetTopologyQueryExistingResultByNameResponse createGetTopologyQueryExistingResultByNameResponse() {
        return new GetTopologyQueryExistingResultByNameResponse();
    }

    /**
     * Create an instance of {@link ExecuteTopologyQueryByName }
     * 
     */
    public ExecuteTopologyQueryByName createExecuteTopologyQueryByName() {
        return new ExecuteTopologyQueryByName();
    }

    /**
     * Create an instance of {@link ExecuteTopologyQueryByNameResponse }
     * 
     */
    public ExecuteTopologyQueryByNameResponse createExecuteTopologyQueryByNameResponse() {
        return new ExecuteTopologyQueryByNameResponse();
    }

    /**
     * Create an instance of {@link GetCIsByType }
     * 
     */
    public GetCIsByType createGetCIsByType() {
        return new GetCIsByType();
    }

    /**
     * Create an instance of {@link GetCIsByTypeResponse }
     * 
     */
    public GetCIsByTypeResponse createGetCIsByTypeResponse() {
        return new GetCIsByTypeResponse();
    }

    /**
     * Create an instance of {@link GetFilteredCIsByType }
     * 
     */
    public GetFilteredCIsByType createGetFilteredCIsByType() {
        return new GetFilteredCIsByType();
    }

    /**
     * Create an instance of {@link GetFilteredCIsByTypeResponse }
     * 
     */
    public GetFilteredCIsByTypeResponse createGetFilteredCIsByTypeResponse() {
        return new GetFilteredCIsByTypeResponse();
    }

    /**
     * Create an instance of {@link GetCIsById }
     * 
     */
    public GetCIsById createGetCIsById() {
        return new GetCIsById();
    }

    /**
     * Create an instance of {@link GetCIsByIdResponse }
     * 
     */
    public GetCIsByIdResponse createGetCIsByIdResponse() {
        return new GetCIsByIdResponse();
    }

    /**
     * Create an instance of {@link GetCIsByIdWithEnumValues }
     * 
     */
    public GetCIsByIdWithEnumValues createGetCIsByIdWithEnumValues() {
        return new GetCIsByIdWithEnumValues();
    }

    /**
     * Create an instance of {@link GetCIsByIdWithEnumValuesResponse }
     * 
     */
    public GetCIsByIdWithEnumValuesResponse createGetCIsByIdWithEnumValuesResponse() {
        return new GetCIsByIdWithEnumValuesResponse();
    }

    /**
     * Create an instance of {@link PullTopologyMapChunks }
     * 
     */
    public PullTopologyMapChunks createPullTopologyMapChunks() {
        return new PullTopologyMapChunks();
    }

    /**
     * Create an instance of {@link PullTopologyMapChunksResponse }
     * 
     */
    public PullTopologyMapChunksResponse createPullTopologyMapChunksResponse() {
        return new PullTopologyMapChunksResponse();
    }

    /**
     * Create an instance of {@link GetQueryNameOfView }
     * 
     */
    public GetQueryNameOfView createGetQueryNameOfView() {
        return new GetQueryNameOfView();
    }

    /**
     * Create an instance of {@link GetQueryNameOfViewResponse }
     * 
     */
    public GetQueryNameOfViewResponse createGetQueryNameOfViewResponse() {
        return new GetQueryNameOfViewResponse();
    }

    /**
     * Create an instance of {@link ExecuteTopologyQueryByNameWithParameters }
     * 
     */
    public ExecuteTopologyQueryByNameWithParameters createExecuteTopologyQueryByNameWithParameters() {
        return new ExecuteTopologyQueryByNameWithParameters();
    }

    /**
     * Create an instance of {@link ExecuteTopologyQueryByNameWithParametersResponse }
     * 
     */
    public ExecuteTopologyQueryByNameWithParametersResponse createExecuteTopologyQueryByNameWithParametersResponse() {
        return new ExecuteTopologyQueryByNameWithParametersResponse();
    }

    /**
     * Create an instance of {@link ExecuteTopologyQueryWithParameters }
     * 
     */
    public ExecuteTopologyQueryWithParameters createExecuteTopologyQueryWithParameters() {
        return new ExecuteTopologyQueryWithParameters();
    }

    /**
     * Create an instance of {@link GetCINeighbours }
     * 
     */
    public GetCINeighbours createGetCINeighbours() {
        return new GetCINeighbours();
    }

    /**
     * Create an instance of {@link GetCINeighboursResponse }
     * 
     */
    public GetCINeighboursResponse createGetCINeighboursResponse() {
        return new GetCINeighboursResponse();
    }

    /**
     * Create an instance of {@link ReleaseChunks }
     * 
     */
    public ReleaseChunks createReleaseChunks() {
        return new ReleaseChunks();
    }

    /**
     * Create an instance of {@link ReleaseChunksResponse }
     * 
     */
    public ReleaseChunksResponse createReleaseChunksResponse() {
        return new ReleaseChunksResponse();
    }

    /**
     * Create an instance of {@link GetRelationsById }
     * 
     */
    public GetRelationsById createGetRelationsById() {
        return new GetRelationsById();
    }

    /**
     * Create an instance of {@link GetRelationsByIdResponse }
     * 
     */
    public GetRelationsByIdResponse createGetRelationsByIdResponse() {
        return new GetRelationsByIdResponse();
    }

    /**
     * Create an instance of {@link ExecuteTopologyQueryWithParametersResponse }
     * 
     */
    public ExecuteTopologyQueryWithParametersResponse createExecuteTopologyQueryWithParametersResponse() {
        return new ExecuteTopologyQueryWithParametersResponse();
    }

    /**
     * Create an instance of {@link ChunkRequest }
     * 
     */
    public ChunkRequest createChunkRequest() {
        return new ChunkRequest();
    }

    /**
     * Create an instance of {@link CmdbContext }
     * 
     */
    public CmdbContext createCmdbContext() {
        return new CmdbContext();
    }

    /**
     * Create an instance of {@link CI }
     * 
     */
    public CI createCI() {
        return new CI();
    }

    /**
     * Create an instance of {@link CIs }
     * 
     */
    public CIs createCIs() {
        return new CIs();
    }

    /**
     * Create an instance of {@link ShallowCI }
     * 
     */
    public ShallowCI createShallowCI() {
        return new ShallowCI();
    }

    /**
     * Create an instance of {@link ShallowCIs }
     * 
     */
    public ShallowCIs createShallowCIs() {
        return new ShallowCIs();
    }

    /**
     * Create an instance of {@link Relation }
     * 
     */
    public Relation createRelation() {
        return new Relation();
    }

    /**
     * Create an instance of {@link Relations }
     * 
     */
    public Relations createRelations() {
        return new Relations();
    }

    /**
     * Create an instance of {@link ShallowRelation }
     * 
     */
    public ShallowRelation createShallowRelation() {
        return new ShallowRelation();
    }

    /**
     * Create an instance of {@link ShallowRelations }
     * 
     */
    public ShallowRelations createShallowRelations() {
        return new ShallowRelations();
    }

    /**
     * Create an instance of {@link IDs }
     * 
     */
    public IDs createIDs() {
        return new IDs();
    }

    /**
     * Create an instance of {@link ID }
     * 
     */
    public ID createID() {
        return new ID();
    }

    /**
     * Create an instance of {@link CIProperties }
     * 
     */
    public CIProperties createCIProperties() {
        return new CIProperties();
    }

    /**
     * Create an instance of {@link BooleanProp }
     * 
     */
    public BooleanProp createBooleanProp() {
        return new BooleanProp();
    }

    /**
     * Create an instance of {@link BytesProp }
     * 
     */
    public BytesProp createBytesProp() {
        return new BytesProp();
    }

    /**
     * Create an instance of {@link DateProp }
     * 
     */
    public DateProp createDateProp() {
        return new DateProp();
    }

    /**
     * Create an instance of {@link DoubleProp }
     * 
     */
    public DoubleProp createDoubleProp() {
        return new DoubleProp();
    }

    /**
     * Create an instance of {@link FloatProp }
     * 
     */
    public FloatProp createFloatProp() {
        return new FloatProp();
    }

    /**
     * Create an instance of {@link IntListProp }
     * 
     */
    public IntListProp createIntListProp() {
        return new IntListProp();
    }

    /**
     * Create an instance of {@link IntProp }
     * 
     */
    public IntProp createIntProp() {
        return new IntProp();
    }

    /**
     * Create an instance of {@link LongProp }
     * 
     */
    public LongProp createLongProp() {
        return new LongProp();
    }

    /**
     * Create an instance of {@link StrListProp }
     * 
     */
    public StrListProp createStrListProp() {
        return new StrListProp();
    }

    /**
     * Create an instance of {@link StrProp }
     * 
     */
    public StrProp createStrProp() {
        return new StrProp();
    }

    /**
     * Create an instance of {@link XmlProp }
     * 
     */
    public XmlProp createXmlProp() {
        return new XmlProp();
    }

    /**
     * Create an instance of {@link BooleanProps }
     * 
     */
    public BooleanProps createBooleanProps() {
        return new BooleanProps();
    }

    /**
     * Create an instance of {@link DateProps }
     * 
     */
    public DateProps createDateProps() {
        return new DateProps();
    }

    /**
     * Create an instance of {@link IntProps }
     * 
     */
    public IntProps createIntProps() {
        return new IntProps();
    }

    /**
     * Create an instance of {@link LongProps }
     * 
     */
    public LongProps createLongProps() {
        return new LongProps();
    }

    /**
     * Create an instance of {@link FloatProps }
     * 
     */
    public FloatProps createFloatProps() {
        return new FloatProps();
    }

    /**
     * Create an instance of {@link IntListProps }
     * 
     */
    public IntListProps createIntListProps() {
        return new IntListProps();
    }

    /**
     * Create an instance of {@link DoubleProps }
     * 
     */
    public DoubleProps createDoubleProps() {
        return new DoubleProps();
    }

    /**
     * Create an instance of {@link BytesProps }
     * 
     */
    public BytesProps createBytesProps() {
        return new BytesProps();
    }

    /**
     * Create an instance of {@link StrProps }
     * 
     */
    public StrProps createStrProps() {
        return new StrProps();
    }

    /**
     * Create an instance of {@link StrListProps }
     * 
     */
    public StrListProps createStrListProps() {
        return new StrListProps();
    }

    /**
     * Create an instance of {@link XmlProps }
     * 
     */
    public XmlProps createXmlProps() {
        return new XmlProps();
    }

    /**
     * Create an instance of {@link Parameters }
     * 
     */
    public Parameters createParameters() {
        return new Parameters();
    }

    /**
     * Create an instance of {@link BooleanParams }
     * 
     */
    public BooleanParams createBooleanParams() {
        return new BooleanParams();
    }

    /**
     * Create an instance of {@link DateParams }
     * 
     */
    public DateParams createDateParams() {
        return new DateParams();
    }

    /**
     * Create an instance of {@link IntParams }
     * 
     */
    public IntParams createIntParams() {
        return new IntParams();
    }

    /**
     * Create an instance of {@link LongParams }
     * 
     */
    public LongParams createLongParams() {
        return new LongParams();
    }

    /**
     * Create an instance of {@link FloatParams }
     * 
     */
    public FloatParams createFloatParams() {
        return new FloatParams();
    }

    /**
     * Create an instance of {@link IntListParams }
     * 
     */
    public IntListParams createIntListParams() {
        return new IntListParams();
    }

    /**
     * Create an instance of {@link DoubleParams }
     * 
     */
    public DoubleParams createDoubleParams() {
        return new DoubleParams();
    }

    /**
     * Create an instance of {@link BytesParams }
     * 
     */
    public BytesParams createBytesParams() {
        return new BytesParams();
    }

    /**
     * Create an instance of {@link StrParams }
     * 
     */
    public StrParams createStrParams() {
        return new StrParams();
    }

    /**
     * Create an instance of {@link StrListParams }
     * 
     */
    public StrListParams createStrListParams() {
        return new StrListParams();
    }

    /**
     * Create an instance of {@link XmlParams }
     * 
     */
    public XmlParams createXmlParams() {
        return new XmlParams();
    }

    /**
     * Create an instance of {@link Topology }
     * 
     */
    public Topology createTopology() {
        return new Topology();
    }

    /**
     * Create an instance of {@link TopologyMap }
     * 
     */
    public TopologyMap createTopologyMap() {
        return new TopologyMap();
    }

    /**
     * Create an instance of {@link CINode }
     * 
     */
    public CINode createCINode() {
        return new CINode();
    }

    /**
     * Create an instance of {@link CINodes }
     * 
     */
    public CINodes createCINodes() {
        return new CINodes();
    }

    /**
     * Create an instance of {@link RelationNode }
     * 
     */
    public RelationNode createRelationNode() {
        return new RelationNode();
    }

    /**
     * Create an instance of {@link RelationNodes }
     * 
     */
    public RelationNodes createRelationNodes() {
        return new RelationNodes();
    }

    /**
     * Create an instance of {@link ParameterizedNode }
     * 
     */
    public ParameterizedNode createParameterizedNode() {
        return new ParameterizedNode();
    }

    /**
     * Create an instance of {@link ParameterizedNodes }
     * 
     */
    public ParameterizedNodes createParameterizedNodes() {
        return new ParameterizedNodes();
    }

    /**
     * Create an instance of {@link ParameterizedCondition }
     * 
     */
    public ParameterizedCondition createParameterizedCondition() {
        return new ParameterizedCondition();
    }

    /**
     * Create an instance of {@link ParameterizedConditions }
     * 
     */
    public ParameterizedConditions createParameterizedConditions() {
        return new ParameterizedConditions();
    }

    /**
     * Create an instance of {@link Conditions }
     * 
     */
    public Conditions createConditions() {
        return new Conditions();
    }

    /**
     * Create an instance of {@link IntCondition }
     * 
     */
    public IntCondition createIntCondition() {
        return new IntCondition();
    }

    /**
     * Create an instance of {@link IntListCondition }
     * 
     */
    public IntListCondition createIntListCondition() {
        return new IntListCondition();
    }

    /**
     * Create an instance of {@link BooleanCondition }
     * 
     */
    public BooleanCondition createBooleanCondition() {
        return new BooleanCondition();
    }

    /**
     * Create an instance of {@link StrCondition }
     * 
     */
    public StrCondition createStrCondition() {
        return new StrCondition();
    }

    /**
     * Create an instance of {@link StrListCondition }
     * 
     */
    public StrListCondition createStrListCondition() {
        return new StrListCondition();
    }

    /**
     * Create an instance of {@link FloatCondition }
     * 
     */
    public FloatCondition createFloatCondition() {
        return new FloatCondition();
    }

    /**
     * Create an instance of {@link LongCondition }
     * 
     */
    public LongCondition createLongCondition() {
        return new LongCondition();
    }

    /**
     * Create an instance of {@link DoubleCondition }
     * 
     */
    public DoubleCondition createDoubleCondition() {
        return new DoubleCondition();
    }

    /**
     * Create an instance of {@link DateCondition }
     * 
     */
    public DateCondition createDateCondition() {
        return new DateCondition();
    }

    /**
     * Create an instance of {@link XmlCondition }
     * 
     */
    public XmlCondition createXmlCondition() {
        return new XmlCondition();
    }

    /**
     * Create an instance of {@link IntConditions }
     * 
     */
    public IntConditions createIntConditions() {
        return new IntConditions();
    }

    /**
     * Create an instance of {@link IntListConditions }
     * 
     */
    public IntListConditions createIntListConditions() {
        return new IntListConditions();
    }

    /**
     * Create an instance of {@link BooleanConditions }
     * 
     */
    public BooleanConditions createBooleanConditions() {
        return new BooleanConditions();
    }

    /**
     * Create an instance of {@link StrConditions }
     * 
     */
    public StrConditions createStrConditions() {
        return new StrConditions();
    }

    /**
     * Create an instance of {@link StrListConditions }
     * 
     */
    public StrListConditions createStrListConditions() {
        return new StrListConditions();
    }

    /**
     * Create an instance of {@link FloatConditions }
     * 
     */
    public FloatConditions createFloatConditions() {
        return new FloatConditions();
    }

    /**
     * Create an instance of {@link LongConditions }
     * 
     */
    public LongConditions createLongConditions() {
        return new LongConditions();
    }

    /**
     * Create an instance of {@link DoubleConditions }
     * 
     */
    public DoubleConditions createDoubleConditions() {
        return new DoubleConditions();
    }

    /**
     * Create an instance of {@link DateConditions }
     * 
     */
    public DateConditions createDateConditions() {
        return new DateConditions();
    }

    /**
     * Create an instance of {@link XmlConditions }
     * 
     */
    public XmlConditions createXmlConditions() {
        return new XmlConditions();
    }

    /**
     * Create an instance of {@link ResComments }
     * 
     */
    public ResComments createResComments() {
        return new ResComments();
    }

    /**
     * Create an instance of {@link ChunkInfo }
     * 
     */
    public ChunkInfo createChunkInfo() {
        return new ChunkInfo();
    }

    /**
     * Create an instance of {@link ChunkKey }
     * 
     */
    public ChunkKey createChunkKey() {
        return new ChunkKey();
    }

    /**
     * Create an instance of {@link ShallowCompoundRelation }
     * 
     */
    public ShallowCompoundRelation createShallowCompoundRelation() {
        return new ShallowCompoundRelation();
    }

    /**
     * Create an instance of {@link ShallowCompoundRelations }
     * 
     */
    public ShallowCompoundRelations createShallowCompoundRelations() {
        return new ShallowCompoundRelations();
    }

    /**
     * Create an instance of {@link ShallowTopology }
     * 
     */
    public ShallowTopology createShallowTopology() {
        return new ShallowTopology();
    }

    /**
     * Create an instance of {@link IntList }
     * 
     */
    public IntList createIntList() {
        return new IntList();
    }

    /**
     * Create an instance of {@link StrList }
     * 
     */
    public StrList createStrList() {
        return new StrList();
    }

    /**
     * Create an instance of {@link PropertiesList }
     * 
     */
    public PropertiesList createPropertiesList() {
        return new PropertiesList();
    }

    /**
     * Create an instance of {@link CustomProperties }
     * 
     */
    public CustomProperties createCustomProperties() {
        return new CustomProperties();
    }

    /**
     * Create an instance of {@link CustomTypedProperties }
     * 
     */
    public CustomTypedProperties createCustomTypedProperties() {
        return new CustomTypedProperties();
    }

    /**
     * Create an instance of {@link PredefinedProperties }
     * 
     */
    public PredefinedProperties createPredefinedProperties() {
        return new PredefinedProperties();
    }

    /**
     * Create an instance of {@link PredefinedTypedProperties }
     * 
     */
    public PredefinedTypedProperties createPredefinedTypedProperties() {
        return new PredefinedTypedProperties();
    }

    /**
     * Create an instance of {@link QualifierProperties }
     * 
     */
    public QualifierProperties createQualifierProperties() {
        return new QualifierProperties();
    }

    /**
     * Create an instance of {@link TypedProperties }
     * 
     */
    public TypedProperties createTypedProperties() {
        return new TypedProperties();
    }

    /**
     * Create an instance of {@link TypedPropertiesCollection }
     * 
     */
    public TypedPropertiesCollection createTypedPropertiesCollection() {
        return new TypedPropertiesCollection();
    }

    /**
     * Create an instance of {@link SimplePredefinedProperty }
     * 
     */
    public SimplePredefinedProperty createSimplePredefinedProperty() {
        return new SimplePredefinedProperty();
    }

    /**
     * Create an instance of {@link SimplePredefinedPropertyCollection }
     * 
     */
    public SimplePredefinedPropertyCollection createSimplePredefinedPropertyCollection() {
        return new SimplePredefinedPropertyCollection();
    }

    /**
     * Create an instance of {@link SimpleTypedPredefinedProperty }
     * 
     */
    public SimpleTypedPredefinedProperty createSimpleTypedPredefinedProperty() {
        return new SimpleTypedPredefinedProperty();
    }

    /**
     * Create an instance of {@link SimpleTypedPredefinedPropertyCollection }
     * 
     */
    public SimpleTypedPredefinedPropertyCollection createSimpleTypedPredefinedPropertyCollection() {
        return new SimpleTypedPredefinedPropertyCollection();
    }

    /**
     * Create an instance of {@link CICountNode }
     * 
     */
    public CICountNode createCICountNode() {
        return new CICountNode();
    }

    /**
     * Create an instance of {@link CICountNodes }
     * 
     */
    public CICountNodes createCICountNodes() {
        return new CICountNodes();
    }

    /**
     * Create an instance of {@link RelationCountNode }
     * 
     */
    public RelationCountNode createRelationCountNode() {
        return new RelationCountNode();
    }

    /**
     * Create an instance of {@link RelationCountNodes }
     * 
     */
    public RelationCountNodes createRelationCountNodes() {
        return new RelationCountNodes();
    }

    /**
     * Create an instance of {@link TopologyCountMap }
     * 
     */
    public TopologyCountMap createTopologyCountMap() {
        return new TopologyCountMap();
    }

    /**
     * Create an instance of {@link ClientIDToCmdbID }
     * 
     */
    public ClientIDToCmdbID createClientIDToCmdbID() {
        return new ClientIDToCmdbID();
    }

    /**
     * Create an instance of {@link AddCIsAndRelations }
     * 
     */
    public AddCIsAndRelations createAddCIsAndRelations() {
        return new AddCIsAndRelations();
    }

    /**
     * Create an instance of {@link AddCIsAndRelationsResponse }
     * 
     */
    public AddCIsAndRelationsResponse createAddCIsAndRelationsResponse() {
        return new AddCIsAndRelationsResponse();
    }

    /**
     * Create an instance of {@link UpdateCIsAndRelations }
     * 
     */
    public UpdateCIsAndRelations createUpdateCIsAndRelations() {
        return new UpdateCIsAndRelations();
    }

    /**
     * Create an instance of {@link UpdateCIsAndRelationsResponse }
     * 
     */
    public UpdateCIsAndRelationsResponse createUpdateCIsAndRelationsResponse() {
        return new UpdateCIsAndRelationsResponse();
    }

    /**
     * Create an instance of {@link DeleteCIsAndRelations }
     * 
     */
    public DeleteCIsAndRelations createDeleteCIsAndRelations() {
        return new DeleteCIsAndRelations();
    }

    /**
     * Create an instance of {@link DeleteCIsAndRelationsResponse }
     * 
     */
    public DeleteCIsAndRelationsResponse createDeleteCIsAndRelationsResponse() {
        return new DeleteCIsAndRelationsResponse();
    }

    /**
     * Create an instance of {@link CIsAndRelationsUpdates }
     * 
     */
    public CIsAndRelationsUpdates createCIsAndRelationsUpdates() {
        return new CIsAndRelationsUpdates();
    }

    /**
     * Create an instance of {@link CalculateImpact }
     * 
     */
    public CalculateImpact createCalculateImpact() {
        return new CalculateImpact();
    }

    /**
     * Create an instance of {@link CalculateImpactResponse }
     * 
     */
    public CalculateImpactResponse createCalculateImpactResponse() {
        return new CalculateImpactResponse();
    }

    /**
     * Create an instance of {@link GetImpactRulesByNamePrefix }
     * 
     */
    public GetImpactRulesByNamePrefix createGetImpactRulesByNamePrefix() {
        return new GetImpactRulesByNamePrefix();
    }

    /**
     * Create an instance of {@link GetImpactRulesByNamePrefixResponse }
     * 
     */
    public GetImpactRulesByNamePrefixResponse createGetImpactRulesByNamePrefixResponse() {
        return new GetImpactRulesByNamePrefixResponse();
    }

    /**
     * Create an instance of {@link GetImpactPath }
     * 
     */
    public GetImpactPath createGetImpactPath() {
        return new GetImpactPath();
    }

    /**
     * Create an instance of {@link GetImpactPathResponse }
     * 
     */
    public GetImpactPathResponse createGetImpactPathResponse() {
        return new GetImpactPathResponse();
    }

    /**
     * Create an instance of {@link GetImpactRulesByGroupName }
     * 
     */
    public GetImpactRulesByGroupName createGetImpactRulesByGroupName() {
        return new GetImpactRulesByGroupName();
    }

    /**
     * Create an instance of {@link GetImpactRulesByGroupNameResponse }
     * 
     */
    public GetImpactRulesByGroupNameResponse createGetImpactRulesByGroupNameResponse() {
        return new GetImpactRulesByGroupNameResponse();
    }

    /**
     * Create an instance of {@link ImpactRuleNames }
     * 
     */
    public ImpactRuleNames createImpactRuleNames() {
        return new ImpactRuleNames();
    }

    /**
     * Create an instance of {@link ImpactRuleName }
     * 
     */
    public ImpactRuleName createImpactRuleName() {
        return new ImpactRuleName();
    }

    /**
     * Create an instance of {@link ImpactTopology }
     * 
     */
    public ImpactTopology createImpactTopology() {
        return new ImpactTopology();
    }

    /**
     * Create an instance of {@link ImpactedCI }
     * 
     */
    public ImpactedCI createImpactedCI() {
        return new ImpactedCI();
    }

    /**
     * Create an instance of {@link ImpactedCIs }
     * 
     */
    public ImpactedCIs createImpactedCIs() {
        return new ImpactedCIs();
    }

    /**
     * Create an instance of {@link ImpactStep }
     * 
     */
    public ImpactStep createImpactStep() {
        return new ImpactStep();
    }

    /**
     * Create an instance of {@link ImpactPath }
     * 
     */
    public ImpactPath createImpactPath() {
        return new ImpactPath();
    }

    /**
     * Create an instance of {@link ImpactPaths }
     * 
     */
    public ImpactPaths createImpactPaths() {
        return new ImpactPaths();
    }

    /**
     * Create an instance of {@link ImpactRule }
     * 
     */
    public ImpactRule createImpactRule() {
        return new ImpactRule();
    }

    /**
     * Create an instance of {@link ImpactRules }
     * 
     */
    public ImpactRules createImpactRules() {
        return new ImpactRules();
    }

    /**
     * Create an instance of {@link ImpactRuleNamesComplexType }
     * 
     */
    public ImpactRuleNamesComplexType createImpactRuleNamesComplexType() {
        return new ImpactRuleNamesComplexType();
    }

    /**
     * Create an instance of {@link Identifier }
     * 
     */
    public Identifier createIdentifier() {
        return new Identifier();
    }

    /**
     * Create an instance of {@link ImpactRelation }
     * 
     */
    public ImpactRelation createImpactRelation() {
        return new ImpactRelation();
    }

    /**
     * Create an instance of {@link ImpactRelations }
     * 
     */
    public ImpactRelations createImpactRelations() {
        return new ImpactRelations();
    }

    /**
     * Create an instance of {@link Actions }
     * 
     */
    public Actions createActions() {
        return new Actions();
    }

    /**
     * Create an instance of {@link Action }
     * 
     */
    public Action createAction() {
        return new Action();
    }

    /**
     * Create an instance of {@link GetCmdbClassDefinition }
     * 
     */
    public GetCmdbClassDefinition createGetCmdbClassDefinition() {
        return new GetCmdbClassDefinition();
    }

    /**
     * Create an instance of {@link GetCmdbClassDefinitionResponse }
     * 
     */
    public GetCmdbClassDefinitionResponse createGetCmdbClassDefinitionResponse() {
        return new GetCmdbClassDefinitionResponse();
    }

    /**
     * Create an instance of {@link GetAllClassesHierarchy }
     * 
     */
    public GetAllClassesHierarchy createGetAllClassesHierarchy() {
        return new GetAllClassesHierarchy();
    }

    /**
     * Create an instance of {@link GetAllClassesHierarchyResponse }
     * 
     */
    public GetAllClassesHierarchyResponse createGetAllClassesHierarchyResponse() {
        return new GetAllClassesHierarchyResponse();
    }

    /**
     * Create an instance of {@link GetClassAncestors }
     * 
     */
    public GetClassAncestors createGetClassAncestors() {
        return new GetClassAncestors();
    }

    /**
     * Create an instance of {@link GetClassAncestorsResponse }
     * 
     */
    public GetClassAncestorsResponse createGetClassAncestorsResponse() {
        return new GetClassAncestorsResponse();
    }

    /**
     * Create an instance of {@link UcmdbClass }
     * 
     */
    public UcmdbClass createUcmdbClass() {
        return new UcmdbClass();
    }

    /**
     * Create an instance of {@link Qualifier }
     * 
     */
    public Qualifier createQualifier() {
        return new Qualifier();
    }

    /**
     * Create an instance of {@link Qualifiers }
     * 
     */
    public Qualifiers createQualifiers() {
        return new Qualifiers();
    }

    /**
     * Create an instance of {@link Attribute }
     * 
     */
    public Attribute createAttribute() {
        return new Attribute();
    }

    /**
     * Create an instance of {@link QualifiersProperties }
     * 
     */
    public QualifiersProperties createQualifiersProperties() {
        return new QualifiersProperties();
    }

    /**
     * Create an instance of {@link UcmdbClassHierarchyNode }
     * 
     */
    public UcmdbClassHierarchyNode createUcmdbClassHierarchyNode() {
        return new UcmdbClassHierarchyNode();
    }

    /**
     * Create an instance of {@link UcmdbClassModelHierarchy }
     * 
     */
    public UcmdbClassModelHierarchy createUcmdbClassModelHierarchy() {
        return new UcmdbClassModelHierarchy();
    }

    /**
     * Create an instance of {@link ClassNames }
     * 
     */
    public ClassNames createClassNames() {
        return new ClassNames();
    }

    /**
     * Create an instance of {@link Attributes }
     * 
     */
    public Attributes createAttributes() {
        return new Attributes();
    }

    /**
     * Create an instance of {@link UcmdbFault }
     * 
     */
    public UcmdbFault createUcmdbFault() {
        return new UcmdbFault();
    }

    /**
     * Create an instance of {@link GetChangedCIs }
     * 
     */
    public GetChangedCIs createGetChangedCIs() {
        return new GetChangedCIs();
    }

    /**
     * Create an instance of {@link GetChangedCIsResponse }
     * 
     */
    public GetChangedCIsResponse createGetChangedCIsResponse() {
        return new GetChangedCIsResponse();
    }

    /**
     * Create an instance of {@link ChangedDataInfo }
     * 
     */
    public ChangedDataInfo createChangedDataInfo() {
        return new ChangedDataInfo();
    }

    /**
     * Create an instance of {@link IsJobActiveRequest }
     * 
     */
    public IsJobActiveRequest createIsJobActiveRequest() {
        return new IsJobActiveRequest();
    }

    /**
     * Create an instance of {@link IsJobActiveResponse }
     * 
     */
    public IsJobActiveResponse createIsJobActiveResponse() {
        return new IsJobActiveResponse();
    }

    /**
     * Create an instance of {@link GetDiscoveryJobsNamesRequest }
     * 
     */
    public GetDiscoveryJobsNamesRequest createGetDiscoveryJobsNamesRequest() {
        return new GetDiscoveryJobsNamesRequest();
    }

    /**
     * Create an instance of {@link GetDiscoveryJobsNamesResponse }
     * 
     */
    public GetDiscoveryJobsNamesResponse createGetDiscoveryJobsNamesResponse() {
        return new GetDiscoveryJobsNamesResponse();
    }

    /**
     * Create an instance of {@link ActivateJobRequest }
     * 
     */
    public ActivateJobRequest createActivateJobRequest() {
        return new ActivateJobRequest();
    }

    /**
     * Create an instance of {@link DeactivateJobRequest }
     * 
     */
    public DeactivateJobRequest createDeactivateJobRequest() {
        return new DeactivateJobRequest();
    }

    /**
     * Create an instance of {@link AddTriggerTQLRequest }
     * 
     */
    public AddTriggerTQLRequest createAddTriggerTQLRequest() {
        return new AddTriggerTQLRequest();
    }

    /**
     * Create an instance of {@link RemoveTriggerTQLRequest }
     * 
     */
    public RemoveTriggerTQLRequest createRemoveTriggerTQLRequest() {
        return new RemoveTriggerTQLRequest();
    }

    /**
     * Create an instance of {@link DisableTriggerTQLRequest }
     * 
     */
    public DisableTriggerTQLRequest createDisableTriggerTQLRequest() {
        return new DisableTriggerTQLRequest();
    }

    /**
     * Create an instance of {@link SetTriggerTQLProbesLimitRequest }
     * 
     */
    public SetTriggerTQLProbesLimitRequest createSetTriggerTQLProbesLimitRequest() {
        return new SetTriggerTQLProbesLimitRequest();
    }

    /**
     * Create an instance of {@link GetDomainsNamesRequest }
     * 
     */
    public GetDomainsNamesRequest createGetDomainsNamesRequest() {
        return new GetDomainsNamesRequest();
    }

    /**
     * Create an instance of {@link GetDomainsNamesResponse }
     * 
     */
    public GetDomainsNamesResponse createGetDomainsNamesResponse() {
        return new GetDomainsNamesResponse();
    }

    /**
     * Create an instance of {@link GetDomainTypeRequest }
     * 
     */
    public GetDomainTypeRequest createGetDomainTypeRequest() {
        return new GetDomainTypeRequest();
    }

    /**
     * Create an instance of {@link GetDomainTypeResponse }
     * 
     */
    public GetDomainTypeResponse createGetDomainTypeResponse() {
        return new GetDomainTypeResponse();
    }

    /**
     * Create an instance of {@link GetProbesNamesRequest }
     * 
     */
    public GetProbesNamesRequest createGetProbesNamesRequest() {
        return new GetProbesNamesRequest();
    }

    /**
     * Create an instance of {@link GetProbesNamesResponse }
     * 
     */
    public GetProbesNamesResponse createGetProbesNamesResponse() {
        return new GetProbesNamesResponse();
    }

    /**
     * Create an instance of {@link GetProbeIPsRequest }
     * 
     */
    public GetProbeIPsRequest createGetProbeIPsRequest() {
        return new GetProbeIPsRequest();
    }

    /**
     * Create an instance of {@link GetProbeIPsResponse }
     * 
     */
    public GetProbeIPsResponse createGetProbeIPsResponse() {
        return new GetProbeIPsResponse();
    }

    /**
     * Create an instance of {@link GetProbeScopeRequest }
     * 
     */
    public GetProbeScopeRequest createGetProbeScopeRequest() {
        return new GetProbeScopeRequest();
    }

    /**
     * Create an instance of {@link GetProbeScopeResponse }
     * 
     */
    public GetProbeScopeResponse createGetProbeScopeResponse() {
        return new GetProbeScopeResponse();
    }

    /**
     * Create an instance of {@link UpdateProbeScopeRequest }
     * 
     */
    public UpdateProbeScopeRequest createUpdateProbeScopeRequest() {
        return new UpdateProbeScopeRequest();
    }

    /**
     * Create an instance of {@link GetJobTriggerTqlsRequest }
     * 
     */
    public GetJobTriggerTqlsRequest createGetJobTriggerTqlsRequest() {
        return new GetJobTriggerTqlsRequest();
    }

    /**
     * Create an instance of {@link GetJobTriggerTqlsResponse }
     * 
     */
    public GetJobTriggerTqlsResponse createGetJobTriggerTqlsResponse() {
        return new GetJobTriggerTqlsResponse();
    }

    /**
     * Create an instance of {@link GetCredentialsEntriesIDsRequest }
     * 
     */
    public GetCredentialsEntriesIDsRequest createGetCredentialsEntriesIDsRequest() {
        return new GetCredentialsEntriesIDsRequest();
    }

    /**
     * Create an instance of {@link GetCredentialsEntriesIDsResponse }
     * 
     */
    public GetCredentialsEntriesIDsResponse createGetCredentialsEntriesIDsResponse() {
        return new GetCredentialsEntriesIDsResponse();
    }

    /**
     * Create an instance of {@link GetCredentialsEntryRequest }
     * 
     */
    public GetCredentialsEntryRequest createGetCredentialsEntryRequest() {
        return new GetCredentialsEntryRequest();
    }

    /**
     * Create an instance of {@link GetCredentialsEntryResponse }
     * 
     */
    public GetCredentialsEntryResponse createGetCredentialsEntryResponse() {
        return new GetCredentialsEntryResponse();
    }

    /**
     * Create an instance of {@link AddCredentialsEntryRequest }
     * 
     */
    public AddCredentialsEntryRequest createAddCredentialsEntryRequest() {
        return new AddCredentialsEntryRequest();
    }

    /**
     * Create an instance of {@link AddCredentialsEntryResponse }
     * 
     */
    public AddCredentialsEntryResponse createAddCredentialsEntryResponse() {
        return new AddCredentialsEntryResponse();
    }

    /**
     * Create an instance of {@link UpdateCredentialsEntryRequest }
     * 
     */
    public UpdateCredentialsEntryRequest createUpdateCredentialsEntryRequest() {
        return new UpdateCredentialsEntryRequest();
    }

    /**
     * Create an instance of {@link RemoveCredentialsEntryRequest }
     * 
     */
    public RemoveCredentialsEntryRequest createRemoveCredentialsEntryRequest() {
        return new RemoveCredentialsEntryRequest();
    }

    /**
     * Create an instance of {@link AddTriggerCIRequest }
     * 
     */
    public AddTriggerCIRequest createAddTriggerCIRequest() {
        return new AddTriggerCIRequest();
    }

    /**
     * Create an instance of {@link DispatchAdHocJobResopnse }
     * 
     */
    public DispatchAdHocJobResopnse createDispatchAdHocJobResopnse() {
        return new DispatchAdHocJobResopnse();
    }

    /**
     * Create an instance of {@link DispatchAdHocJobRequest }
     * 
     */
    public DispatchAdHocJobRequest createDispatchAdHocJobRequest() {
        return new DispatchAdHocJobRequest();
    }

    /**
     * Create an instance of {@link IsProbeConnectedRequest }
     * 
     */
    public IsProbeConnectedRequest createIsProbeConnectedRequest() {
        return new IsProbeConnectedRequest();
    }

    /**
     * Create an instance of {@link IsProbeConnectedResponce }
     * 
     */
    public IsProbeConnectedResponce createIsProbeConnectedResponce() {
        return new IsProbeConnectedResponce();
    }

    /**
     * Create an instance of {@link RediscoverCIsRequest }
     * 
     */
    public RediscoverCIsRequest createRediscoverCIsRequest() {
        return new RediscoverCIsRequest();
    }

    /**
     * Create an instance of {@link RediscoverCIsResponse }
     * 
     */
    public RediscoverCIsResponse createRediscoverCIsResponse() {
        return new RediscoverCIsResponse();
    }

    /**
     * Create an instance of {@link RediscoverViewCIsRequest }
     * 
     */
    public RediscoverViewCIsRequest createRediscoverViewCIsRequest() {
        return new RediscoverViewCIsRequest();
    }

    /**
     * Create an instance of {@link RediscoverViewCIsResponse }
     * 
     */
    public RediscoverViewCIsResponse createRediscoverViewCIsResponse() {
        return new RediscoverViewCIsResponse();
    }

    /**
     * Create an instance of {@link CheckViewDiscoveryProgressRequest }
     * 
     */
    public CheckViewDiscoveryProgressRequest createCheckViewDiscoveryProgressRequest() {
        return new CheckViewDiscoveryProgressRequest();
    }

    /**
     * Create an instance of {@link CheckViewDiscoveryProgressResponse }
     * 
     */
    public CheckViewDiscoveryProgressResponse createCheckViewDiscoveryProgressResponse() {
        return new CheckViewDiscoveryProgressResponse();
    }

    /**
     * Create an instance of {@link CheckDiscoveryProgressRequest }
     * 
     */
    public CheckDiscoveryProgressRequest createCheckDiscoveryProgressRequest() {
        return new CheckDiscoveryProgressRequest();
    }

    /**
     * Create an instance of {@link CheckDiscoveryProgressResponse }
     * 
     */
    public CheckDiscoveryProgressResponse createCheckDiscoveryProgressResponse() {
        return new CheckDiscoveryProgressResponse();
    }

    /**
     * Create an instance of {@link RemoveTriggerCIRequest }
     * 
     */
    public RemoveTriggerCIRequest createRemoveTriggerCIRequest() {
        return new RemoveTriggerCIRequest();
    }

    /**
     * Create an instance of {@link IPList }
     * 
     */
    public IPList createIPList() {
        return new IPList();
    }

    /**
     * Create an instance of {@link IP }
     * 
     */
    public IP createIP() {
        return new IP();
    }

    /**
     * Create an instance of {@link IPRange }
     * 
     */
    public IPRange createIPRange() {
        return new IPRange();
    }

    /**
     * Create an instance of {@link ClassModelQueryGetClassModelIconPathsRequest }
     * 
     */
    public ClassModelQueryGetClassModelIconPathsRequest createClassModelQueryGetClassModelIconPathsRequest() {
        return new ClassModelQueryGetClassModelIconPathsRequest();
    }

    /**
     * Create an instance of {@link ClassModelQueryGetClassModelIconPathsResponse }
     * 
     */
    public ClassModelQueryGetClassModelIconPathsResponse createClassModelQueryGetClassModelIconPathsResponse() {
        return new ClassModelQueryGetClassModelIconPathsResponse();
    }

    /**
     * Create an instance of {@link ClassNameAndIcon }
     * 
     */
    public ClassNameAndIcon createClassNameAndIcon() {
        return new ClassNameAndIcon();
    }

    /**
     * Create an instance of {@link ClassNameAndIcons }
     * 
     */
    public ClassNameAndIcons createClassNameAndIcons() {
        return new ClassNameAndIcons();
    }

    /**
     * Create an instance of {@link AddCustomerRequest }
     * 
     */
    public AddCustomerRequest createAddCustomerRequest() {
        return new AddCustomerRequest();
    }

    /**
     * Create an instance of {@link AddCustomerResponse }
     * 
     */
    public AddCustomerResponse createAddCustomerResponse() {
        return new AddCustomerResponse();
    }

    /**
     * Create an instance of {@link RemoveCustomerRequest }
     * 
     */
    public RemoveCustomerRequest createRemoveCustomerRequest() {
        return new RemoveCustomerRequest();
    }

    /**
     * Create an instance of {@link RemoveCustomerResponse }
     * 
     */
    public RemoveCustomerResponse createRemoveCustomerResponse() {
        return new RemoveCustomerResponse();
    }

    /**
     * Create an instance of {@link Scope.Exclude }
     * 
     */
    public Scope.Exclude createScopeExclude() {
        return new Scope.Exclude();
    }

    /**
     * Create an instance of {@link Scope.Include }
     * 
     */
    public Scope.Include createScopeInclude() {
        return new Scope.Include();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTopologyQueryResultCountByNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "getTopologyQueryResultCountByNameResponse")
    public JAXBElement<GetTopologyQueryResultCountByNameResponse> createGetTopologyQueryResultCountByNameResponse(GetTopologyQueryResultCountByNameResponse value) {
        return new JAXBElement<GetTopologyQueryResultCountByNameResponse>(_GetTopologyQueryResultCountByNameResponse_QNAME, GetTopologyQueryResultCountByNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTopologyQueryResultCountByName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "getTopologyQueryResultCountByName")
    public JAXBElement<GetTopologyQueryResultCountByName> createGetTopologyQueryResultCountByName(GetTopologyQueryResultCountByName value) {
        return new JAXBElement<GetTopologyQueryResultCountByName>(_GetTopologyQueryResultCountByName_QNAME, GetTopologyQueryResultCountByName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTopologyQueryExistingResultByName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "getTopologyQueryExistingResultByName")
    public JAXBElement<GetTopologyQueryExistingResultByName> createGetTopologyQueryExistingResultByName(GetTopologyQueryExistingResultByName value) {
        return new JAXBElement<GetTopologyQueryExistingResultByName>(_GetTopologyQueryExistingResultByName_QNAME, GetTopologyQueryExistingResultByName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTopologyQueryExistingResultByNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "getTopologyQueryExistingResultByNameResponse")
    public JAXBElement<GetTopologyQueryExistingResultByNameResponse> createGetTopologyQueryExistingResultByNameResponse(GetTopologyQueryExistingResultByNameResponse value) {
        return new JAXBElement<GetTopologyQueryExistingResultByNameResponse>(_GetTopologyQueryExistingResultByNameResponse_QNAME, GetTopologyQueryExistingResultByNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteTopologyQueryByName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "executeTopologyQueryByName")
    public JAXBElement<ExecuteTopologyQueryByName> createExecuteTopologyQueryByName(ExecuteTopologyQueryByName value) {
        return new JAXBElement<ExecuteTopologyQueryByName>(_ExecuteTopologyQueryByName_QNAME, ExecuteTopologyQueryByName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteTopologyQueryByNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "executeTopologyQueryByNameResponse")
    public JAXBElement<ExecuteTopologyQueryByNameResponse> createExecuteTopologyQueryByNameResponse(ExecuteTopologyQueryByNameResponse value) {
        return new JAXBElement<ExecuteTopologyQueryByNameResponse>(_ExecuteTopologyQueryByNameResponse_QNAME, ExecuteTopologyQueryByNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCIsByType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "getCIsByType")
    public JAXBElement<GetCIsByType> createGetCIsByType(GetCIsByType value) {
        return new JAXBElement<GetCIsByType>(_GetCIsByType_QNAME, GetCIsByType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCIsByTypeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "getCIsByTypeResponse")
    public JAXBElement<GetCIsByTypeResponse> createGetCIsByTypeResponse(GetCIsByTypeResponse value) {
        return new JAXBElement<GetCIsByTypeResponse>(_GetCIsByTypeResponse_QNAME, GetCIsByTypeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFilteredCIsByType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "getFilteredCIsByType")
    public JAXBElement<GetFilteredCIsByType> createGetFilteredCIsByType(GetFilteredCIsByType value) {
        return new JAXBElement<GetFilteredCIsByType>(_GetFilteredCIsByType_QNAME, GetFilteredCIsByType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFilteredCIsByTypeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "getFilteredCIsByTypeResponse")
    public JAXBElement<GetFilteredCIsByTypeResponse> createGetFilteredCIsByTypeResponse(GetFilteredCIsByTypeResponse value) {
        return new JAXBElement<GetFilteredCIsByTypeResponse>(_GetFilteredCIsByTypeResponse_QNAME, GetFilteredCIsByTypeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCIsById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "getCIsById")
    public JAXBElement<GetCIsById> createGetCIsById(GetCIsById value) {
        return new JAXBElement<GetCIsById>(_GetCIsById_QNAME, GetCIsById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCIsByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "getCIsByIdResponse")
    public JAXBElement<GetCIsByIdResponse> createGetCIsByIdResponse(GetCIsByIdResponse value) {
        return new JAXBElement<GetCIsByIdResponse>(_GetCIsByIdResponse_QNAME, GetCIsByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCIsByIdWithEnumValues }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "getCIsByIdWithEnumValues")
    public JAXBElement<GetCIsByIdWithEnumValues> createGetCIsByIdWithEnumValues(GetCIsByIdWithEnumValues value) {
        return new JAXBElement<GetCIsByIdWithEnumValues>(_GetCIsByIdWithEnumValues_QNAME, GetCIsByIdWithEnumValues.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCIsByIdWithEnumValuesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "getCIsByIdWithEnumValuesResponse")
    public JAXBElement<GetCIsByIdWithEnumValuesResponse> createGetCIsByIdWithEnumValuesResponse(GetCIsByIdWithEnumValuesResponse value) {
        return new JAXBElement<GetCIsByIdWithEnumValuesResponse>(_GetCIsByIdWithEnumValuesResponse_QNAME, GetCIsByIdWithEnumValuesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PullTopologyMapChunks }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "pullTopologyMapChunks")
    public JAXBElement<PullTopologyMapChunks> createPullTopologyMapChunks(PullTopologyMapChunks value) {
        return new JAXBElement<PullTopologyMapChunks>(_PullTopologyMapChunks_QNAME, PullTopologyMapChunks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PullTopologyMapChunksResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "pullTopologyMapChunksResponse")
    public JAXBElement<PullTopologyMapChunksResponse> createPullTopologyMapChunksResponse(PullTopologyMapChunksResponse value) {
        return new JAXBElement<PullTopologyMapChunksResponse>(_PullTopologyMapChunksResponse_QNAME, PullTopologyMapChunksResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetQueryNameOfView }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "getQueryNameOfView")
    public JAXBElement<GetQueryNameOfView> createGetQueryNameOfView(GetQueryNameOfView value) {
        return new JAXBElement<GetQueryNameOfView>(_GetQueryNameOfView_QNAME, GetQueryNameOfView.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetQueryNameOfViewResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "getQueryNameOfViewResponse")
    public JAXBElement<GetQueryNameOfViewResponse> createGetQueryNameOfViewResponse(GetQueryNameOfViewResponse value) {
        return new JAXBElement<GetQueryNameOfViewResponse>(_GetQueryNameOfViewResponse_QNAME, GetQueryNameOfViewResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteTopologyQueryByNameWithParameters }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "executeTopologyQueryByNameWithParameters")
    public JAXBElement<ExecuteTopologyQueryByNameWithParameters> createExecuteTopologyQueryByNameWithParameters(ExecuteTopologyQueryByNameWithParameters value) {
        return new JAXBElement<ExecuteTopologyQueryByNameWithParameters>(_ExecuteTopologyQueryByNameWithParameters_QNAME, ExecuteTopologyQueryByNameWithParameters.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteTopologyQueryByNameWithParametersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "executeTopologyQueryByNameWithParametersResponse")
    public JAXBElement<ExecuteTopologyQueryByNameWithParametersResponse> createExecuteTopologyQueryByNameWithParametersResponse(ExecuteTopologyQueryByNameWithParametersResponse value) {
        return new JAXBElement<ExecuteTopologyQueryByNameWithParametersResponse>(_ExecuteTopologyQueryByNameWithParametersResponse_QNAME, ExecuteTopologyQueryByNameWithParametersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteTopologyQueryWithParameters }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "executeTopologyQueryWithParameters")
    public JAXBElement<ExecuteTopologyQueryWithParameters> createExecuteTopologyQueryWithParameters(ExecuteTopologyQueryWithParameters value) {
        return new JAXBElement<ExecuteTopologyQueryWithParameters>(_ExecuteTopologyQueryWithParameters_QNAME, ExecuteTopologyQueryWithParameters.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteTopologyQueryByNameWithParametersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "executeTopologyQueryWithParametersResponse")
    public JAXBElement<ExecuteTopologyQueryByNameWithParametersResponse> createExecuteTopologyQueryWithParametersResponse(ExecuteTopologyQueryByNameWithParametersResponse value) {
        return new JAXBElement<ExecuteTopologyQueryByNameWithParametersResponse>(_ExecuteTopologyQueryWithParametersResponse_QNAME, ExecuteTopologyQueryByNameWithParametersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCINeighbours }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "getCINeighbours")
    public JAXBElement<GetCINeighbours> createGetCINeighbours(GetCINeighbours value) {
        return new JAXBElement<GetCINeighbours>(_GetCINeighbours_QNAME, GetCINeighbours.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCINeighboursResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "getCINeighboursResponse")
    public JAXBElement<GetCINeighboursResponse> createGetCINeighboursResponse(GetCINeighboursResponse value) {
        return new JAXBElement<GetCINeighboursResponse>(_GetCINeighboursResponse_QNAME, GetCINeighboursResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReleaseChunks }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "releaseChunks")
    public JAXBElement<ReleaseChunks> createReleaseChunks(ReleaseChunks value) {
        return new JAXBElement<ReleaseChunks>(_ReleaseChunks_QNAME, ReleaseChunks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReleaseChunksResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "releaseChunksResponse")
    public JAXBElement<ReleaseChunksResponse> createReleaseChunksResponse(ReleaseChunksResponse value) {
        return new JAXBElement<ReleaseChunksResponse>(_ReleaseChunksResponse_QNAME, ReleaseChunksResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRelationsById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "getRelationsById")
    public JAXBElement<GetRelationsById> createGetRelationsById(GetRelationsById value) {
        return new JAXBElement<GetRelationsById>(_GetRelationsById_QNAME, GetRelationsById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRelationsByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "getRelationsByIdResponse")
    public JAXBElement<GetRelationsByIdResponse> createGetRelationsByIdResponse(GetRelationsByIdResponse value) {
        return new JAXBElement<GetRelationsByIdResponse>(_GetRelationsByIdResponse_QNAME, GetRelationsByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChunkRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/types", name = "ChunkRequest")
    public JAXBElement<ChunkRequest> createChunkRequest(ChunkRequest value) {
        return new JAXBElement<ChunkRequest>(_ChunkRequest_QNAME, ChunkRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientIDToCmdbID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/update", name = "CreatedIDsMap")
    public JAXBElement<ClientIDToCmdbID> createCreatedIDsMap(ClientIDToCmdbID value) {
        return new JAXBElement<ClientIDToCmdbID>(_CreatedIDsMap_QNAME, ClientIDToCmdbID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddCIsAndRelations }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/update", name = "addCIsAndRelations")
    public JAXBElement<AddCIsAndRelations> createAddCIsAndRelations(AddCIsAndRelations value) {
        return new JAXBElement<AddCIsAndRelations>(_AddCIsAndRelations_QNAME, AddCIsAndRelations.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddCIsAndRelationsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/update", name = "addCIsAndRelationsResponse")
    public JAXBElement<AddCIsAndRelationsResponse> createAddCIsAndRelationsResponse(AddCIsAndRelationsResponse value) {
        return new JAXBElement<AddCIsAndRelationsResponse>(_AddCIsAndRelationsResponse_QNAME, AddCIsAndRelationsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCIsAndRelations }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/update", name = "updateCIsAndRelations")
    public JAXBElement<UpdateCIsAndRelations> createUpdateCIsAndRelations(UpdateCIsAndRelations value) {
        return new JAXBElement<UpdateCIsAndRelations>(_UpdateCIsAndRelations_QNAME, UpdateCIsAndRelations.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCIsAndRelationsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/update", name = "updateCIsAndRelationsResponse")
    public JAXBElement<UpdateCIsAndRelationsResponse> createUpdateCIsAndRelationsResponse(UpdateCIsAndRelationsResponse value) {
        return new JAXBElement<UpdateCIsAndRelationsResponse>(_UpdateCIsAndRelationsResponse_QNAME, UpdateCIsAndRelationsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteCIsAndRelations }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/update", name = "deleteCIsAndRelations")
    public JAXBElement<DeleteCIsAndRelations> createDeleteCIsAndRelations(DeleteCIsAndRelations value) {
        return new JAXBElement<DeleteCIsAndRelations>(_DeleteCIsAndRelations_QNAME, DeleteCIsAndRelations.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteCIsAndRelationsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/update", name = "deleteCIsAndRelationsResponse")
    public JAXBElement<DeleteCIsAndRelationsResponse> createDeleteCIsAndRelationsResponse(DeleteCIsAndRelationsResponse value) {
        return new JAXBElement<DeleteCIsAndRelationsResponse>(_DeleteCIsAndRelationsResponse_QNAME, DeleteCIsAndRelationsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CalculateImpact }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/impact", name = "calculateImpact")
    public JAXBElement<CalculateImpact> createCalculateImpact(CalculateImpact value) {
        return new JAXBElement<CalculateImpact>(_CalculateImpact_QNAME, CalculateImpact.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CalculateImpactResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/impact", name = "calculateImpactResponse")
    public JAXBElement<CalculateImpactResponse> createCalculateImpactResponse(CalculateImpactResponse value) {
        return new JAXBElement<CalculateImpactResponse>(_CalculateImpactResponse_QNAME, CalculateImpactResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetImpactRulesByNamePrefix }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/impact", name = "getImpactRulesByNamePrefix")
    public JAXBElement<GetImpactRulesByNamePrefix> createGetImpactRulesByNamePrefix(GetImpactRulesByNamePrefix value) {
        return new JAXBElement<GetImpactRulesByNamePrefix>(_GetImpactRulesByNamePrefix_QNAME, GetImpactRulesByNamePrefix.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetImpactRulesByNamePrefixResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/impact", name = "getImpactRulesByNamePrefixResponse")
    public JAXBElement<GetImpactRulesByNamePrefixResponse> createGetImpactRulesByNamePrefixResponse(GetImpactRulesByNamePrefixResponse value) {
        return new JAXBElement<GetImpactRulesByNamePrefixResponse>(_GetImpactRulesByNamePrefixResponse_QNAME, GetImpactRulesByNamePrefixResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetImpactPath }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/impact", name = "getImpactPath")
    public JAXBElement<GetImpactPath> createGetImpactPath(GetImpactPath value) {
        return new JAXBElement<GetImpactPath>(_GetImpactPath_QNAME, GetImpactPath.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetImpactPathResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/impact", name = "getImpactPathResponse")
    public JAXBElement<GetImpactPathResponse> createGetImpactPathResponse(GetImpactPathResponse value) {
        return new JAXBElement<GetImpactPathResponse>(_GetImpactPathResponse_QNAME, GetImpactPathResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetImpactRulesByGroupName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/impact", name = "getImpactRulesByGroupName")
    public JAXBElement<GetImpactRulesByGroupName> createGetImpactRulesByGroupName(GetImpactRulesByGroupName value) {
        return new JAXBElement<GetImpactRulesByGroupName>(_GetImpactRulesByGroupName_QNAME, GetImpactRulesByGroupName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetImpactRulesByGroupNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/impact", name = "getImpactRulesByGroupNameResponse")
    public JAXBElement<GetImpactRulesByGroupNameResponse> createGetImpactRulesByGroupNameResponse(GetImpactRulesByGroupNameResponse value) {
        return new JAXBElement<GetImpactRulesByGroupNameResponse>(_GetImpactRulesByGroupNameResponse_QNAME, GetImpactRulesByGroupNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImpactTopology }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/types/impact", name = "ImpactPathTopology")
    public JAXBElement<ImpactTopology> createImpactPathTopology(ImpactTopology value) {
        return new JAXBElement<ImpactTopology>(_ImpactPathTopology_QNAME, ImpactTopology.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCmdbClassDefinition }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/classmodel", name = "getCmdbClassDefinition")
    public JAXBElement<GetCmdbClassDefinition> createGetCmdbClassDefinition(GetCmdbClassDefinition value) {
        return new JAXBElement<GetCmdbClassDefinition>(_GetCmdbClassDefinition_QNAME, GetCmdbClassDefinition.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCmdbClassDefinitionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/classmodel", name = "getCmdbClassDefinitionResponse")
    public JAXBElement<GetCmdbClassDefinitionResponse> createGetCmdbClassDefinitionResponse(GetCmdbClassDefinitionResponse value) {
        return new JAXBElement<GetCmdbClassDefinitionResponse>(_GetCmdbClassDefinitionResponse_QNAME, GetCmdbClassDefinitionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllClassesHierarchy }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/classmodel", name = "getAllClassesHierarchy")
    public JAXBElement<GetAllClassesHierarchy> createGetAllClassesHierarchy(GetAllClassesHierarchy value) {
        return new JAXBElement<GetAllClassesHierarchy>(_GetAllClassesHierarchy_QNAME, GetAllClassesHierarchy.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllClassesHierarchyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/classmodel", name = "getAllClassesHierarchyResponse")
    public JAXBElement<GetAllClassesHierarchyResponse> createGetAllClassesHierarchyResponse(GetAllClassesHierarchyResponse value) {
        return new JAXBElement<GetAllClassesHierarchyResponse>(_GetAllClassesHierarchyResponse_QNAME, GetAllClassesHierarchyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetClassAncestors }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/classmodel", name = "getClassAncestors")
    public JAXBElement<GetClassAncestors> createGetClassAncestors(GetClassAncestors value) {
        return new JAXBElement<GetClassAncestors>(_GetClassAncestors_QNAME, GetClassAncestors.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetClassAncestorsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/classmodel", name = "getClassAncestorsResponse")
    public JAXBElement<GetClassAncestorsResponse> createGetClassAncestorsResponse(GetClassAncestorsResponse value) {
        return new JAXBElement<GetClassAncestorsResponse>(_GetClassAncestorsResponse_QNAME, GetClassAncestorsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UcmdbClass }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/types/classmodel", name = "UcmdbClass")
    public JAXBElement<UcmdbClass> createUcmdbClass(UcmdbClass value) {
        return new JAXBElement<UcmdbClass>(_UcmdbClass_QNAME, UcmdbClass.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetChangedCIs }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/history", name = "getChangedCIs")
    public JAXBElement<GetChangedCIs> createGetChangedCIs(GetChangedCIs value) {
        return new JAXBElement<GetChangedCIs>(_GetChangedCIs_QNAME, GetChangedCIs.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetChangedCIsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/history", name = "getChangedCIsResponse")
    public JAXBElement<GetChangedCIsResponse> createGetChangedCIsResponse(GetChangedCIsResponse value) {
        return new JAXBElement<GetChangedCIsResponse>(_GetChangedCIsResponse_QNAME, GetChangedCIsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsJobActiveRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "isJobActiveRequest")
    public JAXBElement<IsJobActiveRequest> createIsJobActiveRequest(IsJobActiveRequest value) {
        return new JAXBElement<IsJobActiveRequest>(_IsJobActiveRequest_QNAME, IsJobActiveRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsJobActiveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "isJobActiveResponse")
    public JAXBElement<IsJobActiveResponse> createIsJobActiveResponse(IsJobActiveResponse value) {
        return new JAXBElement<IsJobActiveResponse>(_IsJobActiveResponse_QNAME, IsJobActiveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDiscoveryJobsNamesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "getDiscoveryJobsNamesRequest")
    public JAXBElement<GetDiscoveryJobsNamesRequest> createGetDiscoveryJobsNamesRequest(GetDiscoveryJobsNamesRequest value) {
        return new JAXBElement<GetDiscoveryJobsNamesRequest>(_GetDiscoveryJobsNamesRequest_QNAME, GetDiscoveryJobsNamesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDiscoveryJobsNamesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "getDiscoveryJobsNamesResponse")
    public JAXBElement<GetDiscoveryJobsNamesResponse> createGetDiscoveryJobsNamesResponse(GetDiscoveryJobsNamesResponse value) {
        return new JAXBElement<GetDiscoveryJobsNamesResponse>(_GetDiscoveryJobsNamesResponse_QNAME, GetDiscoveryJobsNamesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivateJobRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "activateJobRequest")
    public JAXBElement<ActivateJobRequest> createActivateJobRequest(ActivateJobRequest value) {
        return new JAXBElement<ActivateJobRequest>(_ActivateJobRequest_QNAME, ActivateJobRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeactivateJobRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "deactivateJobRequest")
    public JAXBElement<DeactivateJobRequest> createDeactivateJobRequest(DeactivateJobRequest value) {
        return new JAXBElement<DeactivateJobRequest>(_DeactivateJobRequest_QNAME, DeactivateJobRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddTriggerTQLRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "addTriggerTQLRequest")
    public JAXBElement<AddTriggerTQLRequest> createAddTriggerTQLRequest(AddTriggerTQLRequest value) {
        return new JAXBElement<AddTriggerTQLRequest>(_AddTriggerTQLRequest_QNAME, AddTriggerTQLRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveTriggerTQLRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "removeTriggerTQLRequest")
    public JAXBElement<RemoveTriggerTQLRequest> createRemoveTriggerTQLRequest(RemoveTriggerTQLRequest value) {
        return new JAXBElement<RemoveTriggerTQLRequest>(_RemoveTriggerTQLRequest_QNAME, RemoveTriggerTQLRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisableTriggerTQLRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "disableTriggerTQLRequest")
    public JAXBElement<DisableTriggerTQLRequest> createDisableTriggerTQLRequest(DisableTriggerTQLRequest value) {
        return new JAXBElement<DisableTriggerTQLRequest>(_DisableTriggerTQLRequest_QNAME, DisableTriggerTQLRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetTriggerTQLProbesLimitRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "setTriggerTQLProbesLimitRequest")
    public JAXBElement<SetTriggerTQLProbesLimitRequest> createSetTriggerTQLProbesLimitRequest(SetTriggerTQLProbesLimitRequest value) {
        return new JAXBElement<SetTriggerTQLProbesLimitRequest>(_SetTriggerTQLProbesLimitRequest_QNAME, SetTriggerTQLProbesLimitRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDomainsNamesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "getDomainsNamesRequest")
    public JAXBElement<GetDomainsNamesRequest> createGetDomainsNamesRequest(GetDomainsNamesRequest value) {
        return new JAXBElement<GetDomainsNamesRequest>(_GetDomainsNamesRequest_QNAME, GetDomainsNamesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDomainsNamesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "getDomainsNamesResponse")
    public JAXBElement<GetDomainsNamesResponse> createGetDomainsNamesResponse(GetDomainsNamesResponse value) {
        return new JAXBElement<GetDomainsNamesResponse>(_GetDomainsNamesResponse_QNAME, GetDomainsNamesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDomainTypeRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "getDomainTypeRequest")
    public JAXBElement<GetDomainTypeRequest> createGetDomainTypeRequest(GetDomainTypeRequest value) {
        return new JAXBElement<GetDomainTypeRequest>(_GetDomainTypeRequest_QNAME, GetDomainTypeRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDomainTypeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "getDomainTypeResponse")
    public JAXBElement<GetDomainTypeResponse> createGetDomainTypeResponse(GetDomainTypeResponse value) {
        return new JAXBElement<GetDomainTypeResponse>(_GetDomainTypeResponse_QNAME, GetDomainTypeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProbesNamesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "getProbesNamesRequest")
    public JAXBElement<GetProbesNamesRequest> createGetProbesNamesRequest(GetProbesNamesRequest value) {
        return new JAXBElement<GetProbesNamesRequest>(_GetProbesNamesRequest_QNAME, GetProbesNamesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProbesNamesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "getProbesNamesResponse")
    public JAXBElement<GetProbesNamesResponse> createGetProbesNamesResponse(GetProbesNamesResponse value) {
        return new JAXBElement<GetProbesNamesResponse>(_GetProbesNamesResponse_QNAME, GetProbesNamesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProbeIPsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "getProbeIPsRequest")
    public JAXBElement<GetProbeIPsRequest> createGetProbeIPsRequest(GetProbeIPsRequest value) {
        return new JAXBElement<GetProbeIPsRequest>(_GetProbeIPsRequest_QNAME, GetProbeIPsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProbeIPsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "getProbeIPsResponse")
    public JAXBElement<GetProbeIPsResponse> createGetProbeIPsResponse(GetProbeIPsResponse value) {
        return new JAXBElement<GetProbeIPsResponse>(_GetProbeIPsResponse_QNAME, GetProbeIPsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProbeScopeRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "getProbeScopeRequest")
    public JAXBElement<GetProbeScopeRequest> createGetProbeScopeRequest(GetProbeScopeRequest value) {
        return new JAXBElement<GetProbeScopeRequest>(_GetProbeScopeRequest_QNAME, GetProbeScopeRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProbeScopeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "getProbeScopeResponse")
    public JAXBElement<GetProbeScopeResponse> createGetProbeScopeResponse(GetProbeScopeResponse value) {
        return new JAXBElement<GetProbeScopeResponse>(_GetProbeScopeResponse_QNAME, GetProbeScopeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProbeScopeRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "updateProbeScopeRequest")
    public JAXBElement<UpdateProbeScopeRequest> createUpdateProbeScopeRequest(UpdateProbeScopeRequest value) {
        return new JAXBElement<UpdateProbeScopeRequest>(_UpdateProbeScopeRequest_QNAME, UpdateProbeScopeRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobTriggerTqlsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "getJobTriggerTqlsRequest")
    public JAXBElement<GetJobTriggerTqlsRequest> createGetJobTriggerTqlsRequest(GetJobTriggerTqlsRequest value) {
        return new JAXBElement<GetJobTriggerTqlsRequest>(_GetJobTriggerTqlsRequest_QNAME, GetJobTriggerTqlsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetJobTriggerTqlsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "getJobTriggerTqlsResponse")
    public JAXBElement<GetJobTriggerTqlsResponse> createGetJobTriggerTqlsResponse(GetJobTriggerTqlsResponse value) {
        return new JAXBElement<GetJobTriggerTqlsResponse>(_GetJobTriggerTqlsResponse_QNAME, GetJobTriggerTqlsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCredentialsEntriesIDsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "getCredentialsEntriesIDsRequest")
    public JAXBElement<GetCredentialsEntriesIDsRequest> createGetCredentialsEntriesIDsRequest(GetCredentialsEntriesIDsRequest value) {
        return new JAXBElement<GetCredentialsEntriesIDsRequest>(_GetCredentialsEntriesIDsRequest_QNAME, GetCredentialsEntriesIDsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCredentialsEntriesIDsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "getCredentialsEntriesIDsResponse")
    public JAXBElement<GetCredentialsEntriesIDsResponse> createGetCredentialsEntriesIDsResponse(GetCredentialsEntriesIDsResponse value) {
        return new JAXBElement<GetCredentialsEntriesIDsResponse>(_GetCredentialsEntriesIDsResponse_QNAME, GetCredentialsEntriesIDsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCredentialsEntryRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "getCredentialsEntryRequest")
    public JAXBElement<GetCredentialsEntryRequest> createGetCredentialsEntryRequest(GetCredentialsEntryRequest value) {
        return new JAXBElement<GetCredentialsEntryRequest>(_GetCredentialsEntryRequest_QNAME, GetCredentialsEntryRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCredentialsEntryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "getCredentialsEntryResponse")
    public JAXBElement<GetCredentialsEntryResponse> createGetCredentialsEntryResponse(GetCredentialsEntryResponse value) {
        return new JAXBElement<GetCredentialsEntryResponse>(_GetCredentialsEntryResponse_QNAME, GetCredentialsEntryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddCredentialsEntryRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "addCredentialsEntryRequest")
    public JAXBElement<AddCredentialsEntryRequest> createAddCredentialsEntryRequest(AddCredentialsEntryRequest value) {
        return new JAXBElement<AddCredentialsEntryRequest>(_AddCredentialsEntryRequest_QNAME, AddCredentialsEntryRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddCredentialsEntryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "addCredentialsEntryResponse")
    public JAXBElement<AddCredentialsEntryResponse> createAddCredentialsEntryResponse(AddCredentialsEntryResponse value) {
        return new JAXBElement<AddCredentialsEntryResponse>(_AddCredentialsEntryResponse_QNAME, AddCredentialsEntryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCredentialsEntryRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "updateCredentialsEntryRequest")
    public JAXBElement<UpdateCredentialsEntryRequest> createUpdateCredentialsEntryRequest(UpdateCredentialsEntryRequest value) {
        return new JAXBElement<UpdateCredentialsEntryRequest>(_UpdateCredentialsEntryRequest_QNAME, UpdateCredentialsEntryRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveCredentialsEntryRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "removeCredentialsEntryRequest")
    public JAXBElement<RemoveCredentialsEntryRequest> createRemoveCredentialsEntryRequest(RemoveCredentialsEntryRequest value) {
        return new JAXBElement<RemoveCredentialsEntryRequest>(_RemoveCredentialsEntryRequest_QNAME, RemoveCredentialsEntryRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddTriggerCIRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "addTriggerCIRequest")
    public JAXBElement<AddTriggerCIRequest> createAddTriggerCIRequest(AddTriggerCIRequest value) {
        return new JAXBElement<AddTriggerCIRequest>(_AddTriggerCIRequest_QNAME, AddTriggerCIRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddTriggerCIRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "removeTriggerCIRequest")
    public JAXBElement<AddTriggerCIRequest> createRemoveTriggerCIRequest(AddTriggerCIRequest value) {
        return new JAXBElement<AddTriggerCIRequest>(_RemoveTriggerCIRequest_QNAME, AddTriggerCIRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DispatchAdHocJobResopnse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "dispatchAdHocJobResopnse")
    public JAXBElement<DispatchAdHocJobResopnse> createDispatchAdHocJobResopnse(DispatchAdHocJobResopnse value) {
        return new JAXBElement<DispatchAdHocJobResopnse>(_DispatchAdHocJobResopnse_QNAME, DispatchAdHocJobResopnse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DispatchAdHocJobRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "dispatchAdHocJobRequest")
    public JAXBElement<DispatchAdHocJobRequest> createDispatchAdHocJobRequest(DispatchAdHocJobRequest value) {
        return new JAXBElement<DispatchAdHocJobRequest>(_DispatchAdHocJobRequest_QNAME, DispatchAdHocJobRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsProbeConnectedRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "isProbeConnectedRequest")
    public JAXBElement<IsProbeConnectedRequest> createIsProbeConnectedRequest(IsProbeConnectedRequest value) {
        return new JAXBElement<IsProbeConnectedRequest>(_IsProbeConnectedRequest_QNAME, IsProbeConnectedRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsProbeConnectedResponce }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "isProbeConnectedResponce")
    public JAXBElement<IsProbeConnectedResponce> createIsProbeConnectedResponce(IsProbeConnectedResponce value) {
        return new JAXBElement<IsProbeConnectedResponce>(_IsProbeConnectedResponce_QNAME, IsProbeConnectedResponce.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RediscoverCIsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "rediscoverCIsRequest")
    public JAXBElement<RediscoverCIsRequest> createRediscoverCIsRequest(RediscoverCIsRequest value) {
        return new JAXBElement<RediscoverCIsRequest>(_RediscoverCIsRequest_QNAME, RediscoverCIsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RediscoverCIsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "rediscoverCIsResponse")
    public JAXBElement<RediscoverCIsResponse> createRediscoverCIsResponse(RediscoverCIsResponse value) {
        return new JAXBElement<RediscoverCIsResponse>(_RediscoverCIsResponse_QNAME, RediscoverCIsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RediscoverViewCIsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "rediscoverViewCIsRequest")
    public JAXBElement<RediscoverViewCIsRequest> createRediscoverViewCIsRequest(RediscoverViewCIsRequest value) {
        return new JAXBElement<RediscoverViewCIsRequest>(_RediscoverViewCIsRequest_QNAME, RediscoverViewCIsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RediscoverViewCIsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "rediscoverViewCIsResponse")
    public JAXBElement<RediscoverViewCIsResponse> createRediscoverViewCIsResponse(RediscoverViewCIsResponse value) {
        return new JAXBElement<RediscoverViewCIsResponse>(_RediscoverViewCIsResponse_QNAME, RediscoverViewCIsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckViewDiscoveryProgressRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "checkViewDiscoveryProgressRequest")
    public JAXBElement<CheckViewDiscoveryProgressRequest> createCheckViewDiscoveryProgressRequest(CheckViewDiscoveryProgressRequest value) {
        return new JAXBElement<CheckViewDiscoveryProgressRequest>(_CheckViewDiscoveryProgressRequest_QNAME, CheckViewDiscoveryProgressRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckViewDiscoveryProgressResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "checkViewDiscoveryProgressResponse")
    public JAXBElement<CheckViewDiscoveryProgressResponse> createCheckViewDiscoveryProgressResponse(CheckViewDiscoveryProgressResponse value) {
        return new JAXBElement<CheckViewDiscoveryProgressResponse>(_CheckViewDiscoveryProgressResponse_QNAME, CheckViewDiscoveryProgressResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckDiscoveryProgressRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "checkDiscoveryProgressRequest")
    public JAXBElement<CheckDiscoveryProgressRequest> createCheckDiscoveryProgressRequest(CheckDiscoveryProgressRequest value) {
        return new JAXBElement<CheckDiscoveryProgressRequest>(_CheckDiscoveryProgressRequest_QNAME, CheckDiscoveryProgressRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckDiscoveryProgressResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", name = "checkDiscoveryProgressResponse")
    public JAXBElement<CheckDiscoveryProgressResponse> createCheckDiscoveryProgressResponse(CheckDiscoveryProgressResponse value) {
        return new JAXBElement<CheckDiscoveryProgressResponse>(_CheckDiscoveryProgressResponse_QNAME, CheckDiscoveryProgressResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClassModelQueryGetClassModelIconPathsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/ui/1/params", name = "ClassModelQueryGetClassModelIconPathsRequest")
    public JAXBElement<ClassModelQueryGetClassModelIconPathsRequest> createClassModelQueryGetClassModelIconPathsRequest(ClassModelQueryGetClassModelIconPathsRequest value) {
        return new JAXBElement<ClassModelQueryGetClassModelIconPathsRequest>(_ClassModelQueryGetClassModelIconPathsRequest_QNAME, ClassModelQueryGetClassModelIconPathsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClassModelQueryGetClassModelIconPathsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/ui/1/params", name = "ClassModelQueryGetClassModelIconPathsResponse")
    public JAXBElement<ClassModelQueryGetClassModelIconPathsResponse> createClassModelQueryGetClassModelIconPathsResponse(ClassModelQueryGetClassModelIconPathsResponse value) {
        return new JAXBElement<ClassModelQueryGetClassModelIconPathsResponse>(_ClassModelQueryGetClassModelIconPathsResponse_QNAME, ClassModelQueryGetClassModelIconPathsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddCustomerRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/management/1/params", name = "addCustomerRequest")
    public JAXBElement<AddCustomerRequest> createAddCustomerRequest(AddCustomerRequest value) {
        return new JAXBElement<AddCustomerRequest>(_AddCustomerRequest_QNAME, AddCustomerRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/management/1/params", name = "addCustomerResponse")
    public JAXBElement<AddCustomerResponse> createAddCustomerResponse(AddCustomerResponse value) {
        return new JAXBElement<AddCustomerResponse>(_AddCustomerResponse_QNAME, AddCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveCustomerRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/management/1/params", name = "removeCustomerRequest")
    public JAXBElement<RemoveCustomerRequest> createRemoveCustomerRequest(RemoveCustomerRequest value) {
        return new JAXBElement<RemoveCustomerRequest>(_RemoveCustomerRequest_QNAME, RemoveCustomerRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/management/1/params", name = "removeCustomerResponse")
    public JAXBElement<RemoveCustomerResponse> createRemoveCustomerResponse(RemoveCustomerResponse value) {
        return new JAXBElement<RemoveCustomerResponse>(_RemoveCustomerResponse_QNAME, RemoveCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/update", name = "ignoreValidation", scope = UpdateCIsAndRelations.class, defaultValue = "false")
    public JAXBElement<Boolean> createUpdateCIsAndRelationsIgnoreValidation(Boolean value) {
        return new JAXBElement<Boolean>(_UpdateCIsAndRelationsIgnoreValidation_QNAME, Boolean.class, UpdateCIsAndRelations.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/update", name = "updateExisting", scope = AddCIsAndRelations.class, defaultValue = "true")
    public JAXBElement<Boolean> createAddCIsAndRelationsUpdateExisting(Boolean value) {
        return new JAXBElement<Boolean>(_AddCIsAndRelationsUpdateExisting_QNAME, Boolean.class, AddCIsAndRelations.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/update", name = "ignoreValidation", scope = AddCIsAndRelations.class, defaultValue = "false")
    public JAXBElement<Boolean> createAddCIsAndRelationsIgnoreValidation(Boolean value) {
        return new JAXBElement<Boolean>(_UpdateCIsAndRelationsIgnoreValidation_QNAME, Boolean.class, AddCIsAndRelations.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/types", name = "value", scope = XmlProp.class)
    public JAXBElement<String> createXmlPropValue(String value) {
        return new JAXBElement<String>(_XmlPropValue_QNAME, String.class, XmlProp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/types", name = "value", scope = StrProp.class)
    public JAXBElement<String> createStrPropValue(String value) {
        return new JAXBElement<String>(_XmlPropValue_QNAME, String.class, StrProp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StrList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/types", name = "strValues", scope = StrListProp.class)
    public JAXBElement<StrList> createStrListPropStrValues(StrList value) {
        return new JAXBElement<StrList>(_StrListPropStrValues_QNAME, StrList.class, StrListProp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/types", name = "value", scope = LongProp.class)
    public JAXBElement<Long> createLongPropValue(Long value) {
        return new JAXBElement<Long>(_XmlPropValue_QNAME, Long.class, LongProp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/types", name = "value", scope = IntProp.class)
    public JAXBElement<BigInteger> createIntPropValue(BigInteger value) {
        return new JAXBElement<BigInteger>(_XmlPropValue_QNAME, BigInteger.class, IntProp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IntList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/types", name = "intValues", scope = IntListProp.class)
    public JAXBElement<IntList> createIntListPropIntValues(IntList value) {
        return new JAXBElement<IntList>(_IntListPropIntValues_QNAME, IntList.class, IntListProp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/types", name = "value", scope = FloatProp.class)
    public JAXBElement<Float> createFloatPropValue(Float value) {
        return new JAXBElement<Float>(_XmlPropValue_QNAME, Float.class, FloatProp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/types", name = "value", scope = DoubleProp.class)
    public JAXBElement<Double> createDoublePropValue(Double value) {
        return new JAXBElement<Double>(_XmlPropValue_QNAME, Double.class, DoubleProp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/types", name = "value", scope = DateProp.class)
    public JAXBElement<XMLGregorianCalendar> createDatePropValue(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_XmlPropValue_QNAME, XMLGregorianCalendar.class, DateProp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/types", name = "value", scope = BytesProp.class)
    public JAXBElement<byte[]> createBytesPropValue(byte[] value) {
        return new JAXBElement<byte[]>(_XmlPropValue_QNAME, byte[].class, BytesProp.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/types", name = "value", scope = BooleanProp.class)
    public JAXBElement<Boolean> createBooleanPropValue(Boolean value) {
        return new JAXBElement<Boolean>(_XmlPropValue_QNAME, Boolean.class, BooleanProp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.hp.com/ucmdb/1/params/query", name = "countInvisible", scope = GetTopologyQueryResultCountByName.class, defaultValue = "true")
    public JAXBElement<Boolean> createGetTopologyQueryResultCountByNameCountInvisible(Boolean value) {
        return new JAXBElement<Boolean>(_GetTopologyQueryResultCountByNameCountInvisible_QNAME, Boolean.class, GetTopologyQueryResultCountByName.class, value);
    }

}
