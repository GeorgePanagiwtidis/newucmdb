
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for impactRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="impactRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="impactCategory" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IDs" type="{http://schemas.hp.com/ucmdb/1/types}IDs"/&gt;
 *         &lt;element ref="{http://schemas.hp.com/ucmdb/1/types/impact}ImpactRuleNames"/&gt;
 *         &lt;element name="severity" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "impactRequest", namespace = "http://schemas.hp.com/ucmdb/1/params/impact", propOrder = {
    "impactCategory",
    "iDs",
    "impactRuleNames",
    "severity"
})
@XmlSeeAlso({
    CalculateImpact.class
})
public abstract class ImpactRequest {

    @XmlElement(required = true)
    protected String impactCategory;
    @XmlElement(name = "IDs", required = true)
    protected IDs iDs;
    @XmlElement(name = "ImpactRuleNames", namespace = "http://schemas.hp.com/ucmdb/1/types/impact", required = true)
    protected ImpactRuleNames impactRuleNames;
    protected int severity;

    /**
     * Gets the value of the impactCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImpactCategory() {
        return impactCategory;
    }

    /**
     * Sets the value of the impactCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImpactCategory(String value) {
        this.impactCategory = value;
    }

    /**
     * Gets the value of the iDs property.
     * 
     * @return
     *     possible object is
     *     {@link IDs }
     *     
     */
    public IDs getIDs() {
        return iDs;
    }

    /**
     * Sets the value of the iDs property.
     * 
     * @param value
     *     allowed object is
     *     {@link IDs }
     *     
     */
    public void setIDs(IDs value) {
        this.iDs = value;
    }

    /**
     * 
     *                         The impact rules for which to retrieve the result
     *                     
     * 
     * @return
     *     possible object is
     *     {@link ImpactRuleNames }
     *     
     */
    public ImpactRuleNames getImpactRuleNames() {
        return impactRuleNames;
    }

    /**
     * Sets the value of the impactRuleNames property.
     * 
     * @param value
     *     allowed object is
     *     {@link ImpactRuleNames }
     *     
     */
    public void setImpactRuleNames(ImpactRuleNames value) {
        this.impactRuleNames = value;
    }

    /**
     * Gets the value of the severity property.
     * 
     */
    public int getSeverity() {
        return severity;
    }

    /**
     * Sets the value of the severity property.
     * 
     */
    public void setSeverity(int value) {
        this.severity = value;
    }

}
