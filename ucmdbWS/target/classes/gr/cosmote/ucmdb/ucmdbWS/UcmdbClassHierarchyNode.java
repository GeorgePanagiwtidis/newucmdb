
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UcmdbClassHierarchyNode complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UcmdbClassHierarchyNode"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="classNames" type="{http://schemas.hp.com/ucmdb/1/types/classmodel}ClassNames"/&gt;
 *         &lt;element name="classParentName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UcmdbClassHierarchyNode", namespace = "http://schemas.hp.com/ucmdb/1/types/classmodel", propOrder = {
    "classNames",
    "classParentName"
})
public class UcmdbClassHierarchyNode {

    @XmlElement(required = true)
    protected ClassNames classNames;
    @XmlElement(required = true)
    protected String classParentName;

    /**
     * Gets the value of the classNames property.
     * 
     * @return
     *     possible object is
     *     {@link ClassNames }
     *     
     */
    public ClassNames getClassNames() {
        return classNames;
    }

    /**
     * Sets the value of the classNames property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClassNames }
     *     
     */
    public void setClassNames(ClassNames value) {
        this.classNames = value;
    }

    /**
     * Gets the value of the classParentName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassParentName() {
        return classParentName;
    }

    /**
     * Sets the value of the classParentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassParentName(String value) {
        this.classParentName = value;
    }

}
