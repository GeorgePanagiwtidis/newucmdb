
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getJobTriggerTqlsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getJobTriggerTqlsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="strList" type="{http://schemas.hp.com/ucmdb/1/types}strList"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getJobTriggerTqlsResponse", namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", propOrder = {
    "strList"
})
public class GetJobTriggerTqlsResponse {

    @XmlElement(required = true)
    protected StrList strList;

    /**
     * Gets the value of the strList property.
     * 
     * @return
     *     possible object is
     *     {@link StrList }
     *     
     */
    public StrList getStrList() {
        return strList;
    }

    /**
     * Sets the value of the strList property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrList }
     *     
     */
    public void setStrList(StrList value) {
        this.strList = value;
    }

}
