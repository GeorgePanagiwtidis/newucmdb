
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCmdbClassDefinitionResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCmdbClassDefinitionResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://schemas.hp.com/ucmdb/1/types/classmodel}UcmdbClass"/&gt;
 *         &lt;element name="comments" type="{http://schemas.hp.com/ucmdb/1/types}ResComments"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCmdbClassDefinitionResponse", namespace = "http://schemas.hp.com/ucmdb/1/params/classmodel", propOrder = {
    "ucmdbClass",
    "comments"
})
public class GetCmdbClassDefinitionResponse {

    @XmlElement(name = "UcmdbClass", namespace = "http://schemas.hp.com/ucmdb/1/types/classmodel", required = true)
    protected UcmdbClass ucmdbClass;
    @XmlElement(required = true)
    protected ResComments comments;

    /**
     * Gets the value of the ucmdbClass property.
     * 
     * @return
     *     possible object is
     *     {@link UcmdbClass }
     *     
     */
    public UcmdbClass getUcmdbClass() {
        return ucmdbClass;
    }

    /**
     * Sets the value of the ucmdbClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link UcmdbClass }
     *     
     */
    public void setUcmdbClass(UcmdbClass value) {
        this.ucmdbClass = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link ResComments }
     *     
     */
    public ResComments getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResComments }
     *     
     */
    public void setComments(ResComments value) {
        this.comments = value;
    }

}
