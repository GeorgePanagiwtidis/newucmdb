
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Relation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Relation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ID" type="{http://schemas.hp.com/ucmdb/1/types}ID"/&gt;
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="end1ID" type="{http://schemas.hp.com/ucmdb/1/types}ID"/&gt;
 *         &lt;element name="end2ID" type="{http://schemas.hp.com/ucmdb/1/types}ID"/&gt;
 *         &lt;element name="props" type="{http://schemas.hp.com/ucmdb/1/types}CIProperties" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Relation", propOrder = {
    "id",
    "type",
    "end1ID",
    "end2ID",
    "props"
})
public class Relation {

    @XmlElement(name = "ID", required = true)
    protected ID id;
    @XmlElement(required = true)
    protected String type;
    @XmlElement(required = true)
    protected ID end1ID;
    @XmlElement(required = true)
    protected ID end2ID;
    protected CIProperties props;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link ID }
     *     
     */
    public ID getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID }
     *     
     */
    public void setID(ID value) {
        this.id = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the end1ID property.
     * 
     * @return
     *     possible object is
     *     {@link ID }
     *     
     */
    public ID getEnd1ID() {
        return end1ID;
    }

    /**
     * Sets the value of the end1ID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID }
     *     
     */
    public void setEnd1ID(ID value) {
        this.end1ID = value;
    }

    /**
     * Gets the value of the end2ID property.
     * 
     * @return
     *     possible object is
     *     {@link ID }
     *     
     */
    public ID getEnd2ID() {
        return end2ID;
    }

    /**
     * Sets the value of the end2ID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID }
     *     
     */
    public void setEnd2ID(ID value) {
        this.end2ID = value;
    }

    /**
     * Gets the value of the props property.
     * 
     * @return
     *     possible object is
     *     {@link CIProperties }
     *     
     */
    public CIProperties getProps() {
        return props;
    }

    /**
     * Sets the value of the props property.
     * 
     * @param value
     *     allowed object is
     *     {@link CIProperties }
     *     
     */
    public void setProps(CIProperties value) {
        this.props = value;
    }

}
