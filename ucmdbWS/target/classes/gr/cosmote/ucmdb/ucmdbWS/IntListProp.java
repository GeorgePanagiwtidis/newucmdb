
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IntListProp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntListProp"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://schemas.hp.com/ucmdb/1/types}CIProp"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="intValues" type="{http://schemas.hp.com/ucmdb/1/types}intList" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntListProp", propOrder = {
    "intValues"
})
public class IntListProp
    extends CIProp
{

    @XmlElementRef(name = "intValues", namespace = "http://schemas.hp.com/ucmdb/1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<IntList> intValues;

    /**
     * Gets the value of the intValues property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link IntList }{@code >}
     *     
     */
    public JAXBElement<IntList> getIntValues() {
        return intValues;
    }

    /**
     * Sets the value of the intValues property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link IntList }{@code >}
     *     
     */
    public void setIntValues(JAXBElement<IntList> value) {
        this.intValues = value;
    }

}
