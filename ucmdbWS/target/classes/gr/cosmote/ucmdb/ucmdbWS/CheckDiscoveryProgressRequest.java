
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for checkDiscoveryProgressRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="checkDiscoveryProgressRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CmdbIDs" type="{http://schemas.hp.com/ucmdb/1/types}strList"/&gt;
 *         &lt;element name="CmdbContext" type="{http://schemas.hp.com/ucmdb/1/types}CmdbContext"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "checkDiscoveryProgressRequest", namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", propOrder = {
    "cmdbIDs",
    "cmdbContext"
})
public class CheckDiscoveryProgressRequest {

    @XmlElement(name = "CmdbIDs", required = true)
    protected StrList cmdbIDs;
    @XmlElement(name = "CmdbContext", required = true)
    protected CmdbContext cmdbContext;

    /**
     * Gets the value of the cmdbIDs property.
     * 
     * @return
     *     possible object is
     *     {@link StrList }
     *     
     */
    public StrList getCmdbIDs() {
        return cmdbIDs;
    }

    /**
     * Sets the value of the cmdbIDs property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrList }
     *     
     */
    public void setCmdbIDs(StrList value) {
        this.cmdbIDs = value;
    }

    /**
     * Gets the value of the cmdbContext property.
     * 
     * @return
     *     possible object is
     *     {@link CmdbContext }
     *     
     */
    public CmdbContext getCmdbContext() {
        return cmdbContext;
    }

    /**
     * Sets the value of the cmdbContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link CmdbContext }
     *     
     */
    public void setCmdbContext(CmdbContext value) {
        this.cmdbContext = value;
    }

}
