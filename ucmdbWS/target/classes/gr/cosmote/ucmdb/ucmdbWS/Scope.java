
package gr.cosmote.ucmdb.ucmdbWS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Scope complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Scope"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Exclude"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Ranges" type="{http://schemas.hp.com/ucmdb/discovery/1/types}IPRange" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Include"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Ranges" type="{http://schemas.hp.com/ucmdb/discovery/1/types}IPRange" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Scope", namespace = "http://schemas.hp.com/ucmdb/discovery/1/types", propOrder = {
    "exclude",
    "include"
})
public class Scope {

    @XmlElement(name = "Exclude", required = true)
    protected Scope.Exclude exclude;
    @XmlElement(name = "Include", required = true)
    protected Scope.Include include;

    /**
     * Gets the value of the exclude property.
     * 
     * @return
     *     possible object is
     *     {@link Scope.Exclude }
     *     
     */
    public Scope.Exclude getExclude() {
        return exclude;
    }

    /**
     * Sets the value of the exclude property.
     * 
     * @param value
     *     allowed object is
     *     {@link Scope.Exclude }
     *     
     */
    public void setExclude(Scope.Exclude value) {
        this.exclude = value;
    }

    /**
     * Gets the value of the include property.
     * 
     * @return
     *     possible object is
     *     {@link Scope.Include }
     *     
     */
    public Scope.Include getInclude() {
        return include;
    }

    /**
     * Sets the value of the include property.
     * 
     * @param value
     *     allowed object is
     *     {@link Scope.Include }
     *     
     */
    public void setInclude(Scope.Include value) {
        this.include = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Ranges" type="{http://schemas.hp.com/ucmdb/discovery/1/types}IPRange" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ranges"
    })
    public static class Exclude {

        @XmlElement(name = "Ranges", namespace = "http://schemas.hp.com/ucmdb/discovery/1/types")
        protected List<IPRange> ranges;

        /**
         * Gets the value of the ranges property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the ranges property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRanges().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link IPRange }
         * 
         * 
         */
        public List<IPRange> getRanges() {
            if (ranges == null) {
                ranges = new ArrayList<IPRange>();
            }
            return this.ranges;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Ranges" type="{http://schemas.hp.com/ucmdb/discovery/1/types}IPRange" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ranges"
    })
    public static class Include {

        @XmlElement(name = "Ranges", namespace = "http://schemas.hp.com/ucmdb/discovery/1/types")
        protected List<IPRange> ranges;

        /**
         * Gets the value of the ranges property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the ranges property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRanges().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link IPRange }
         * 
         * 
         */
        public List<IPRange> getRanges() {
            if (ranges == null) {
                ranges = new ArrayList<IPRange>();
            }
            return this.ranges;
        }

    }

}
