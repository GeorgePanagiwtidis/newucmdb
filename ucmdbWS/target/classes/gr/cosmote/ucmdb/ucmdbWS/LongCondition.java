
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LongCondition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LongCondition"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://schemas.hp.com/ucmdb/1/types}Condition"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="condition" type="{http://schemas.hp.com/ucmdb/1/types}LongProp"/&gt;
 *         &lt;element name="longOperator"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="Equal"/&gt;
 *               &lt;enumeration value="NotEqual"/&gt;
 *               &lt;enumeration value="Greater"/&gt;
 *               &lt;enumeration value="GreaterEqual"/&gt;
 *               &lt;enumeration value="Less"/&gt;
 *               &lt;enumeration value="LessEqual"/&gt;
 *               &lt;enumeration value="In"/&gt;
 *               &lt;enumeration value="IsNull"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LongCondition", propOrder = {
    "condition",
    "longOperator"
})
public class LongCondition
    extends Condition
{

    @XmlElement(required = true)
    protected LongProp condition;
    @XmlElement(required = true)
    protected String longOperator;

    /**
     * Gets the value of the condition property.
     * 
     * @return
     *     possible object is
     *     {@link LongProp }
     *     
     */
    public LongProp getCondition() {
        return condition;
    }

    /**
     * Sets the value of the condition property.
     * 
     * @param value
     *     allowed object is
     *     {@link LongProp }
     *     
     */
    public void setCondition(LongProp value) {
        this.condition = value;
    }

    /**
     * Gets the value of the longOperator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLongOperator() {
        return longOperator;
    }

    /**
     * Sets the value of the longOperator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLongOperator(String value) {
        this.longOperator = value;
    }

}
