
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomTypedProperties complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomTypedProperties"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="propertiesList" type="{http://schemas.hp.com/ucmdb/1/types/props}PropertiesList" minOccurs="0"/&gt;
 *         &lt;element name="predefinedTypedProperties" type="{http://schemas.hp.com/ucmdb/1/types/props}PredefinedTypedProperties" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomTypedProperties", namespace = "http://schemas.hp.com/ucmdb/1/types/props", propOrder = {
    "propertiesList",
    "predefinedTypedProperties"
})
public class CustomTypedProperties {

    protected PropertiesList propertiesList;
    protected PredefinedTypedProperties predefinedTypedProperties;

    /**
     * Gets the value of the propertiesList property.
     * 
     * @return
     *     possible object is
     *     {@link PropertiesList }
     *     
     */
    public PropertiesList getPropertiesList() {
        return propertiesList;
    }

    /**
     * Sets the value of the propertiesList property.
     * 
     * @param value
     *     allowed object is
     *     {@link PropertiesList }
     *     
     */
    public void setPropertiesList(PropertiesList value) {
        this.propertiesList = value;
    }

    /**
     * Gets the value of the predefinedTypedProperties property.
     * 
     * @return
     *     possible object is
     *     {@link PredefinedTypedProperties }
     *     
     */
    public PredefinedTypedProperties getPredefinedTypedProperties() {
        return predefinedTypedProperties;
    }

    /**
     * Sets the value of the predefinedTypedProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link PredefinedTypedProperties }
     *     
     */
    public void setPredefinedTypedProperties(PredefinedTypedProperties value) {
        this.predefinedTypedProperties = value;
    }

}
