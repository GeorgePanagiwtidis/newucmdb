
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCredentialsEntriesIDsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCredentialsEntriesIDsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="credentialsEntryIDs" type="{http://schemas.hp.com/ucmdb/1/types}strList"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCredentialsEntriesIDsResponse", namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", propOrder = {
    "credentialsEntryIDs"
})
public class GetCredentialsEntriesIDsResponse {

    @XmlElement(required = true)
    protected StrList credentialsEntryIDs;

    /**
     * Gets the value of the credentialsEntryIDs property.
     * 
     * @return
     *     possible object is
     *     {@link StrList }
     *     
     */
    public StrList getCredentialsEntryIDs() {
        return credentialsEntryIDs;
    }

    /**
     * Sets the value of the credentialsEntryIDs property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrList }
     *     
     */
    public void setCredentialsEntryIDs(StrList value) {
        this.credentialsEntryIDs = value;
    }

}
