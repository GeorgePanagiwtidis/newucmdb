
package gr.cosmote.ucmdb.ucmdbWS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ImpactRuleNames complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ImpactRuleNames"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="impactRuleName" type="{http://schemas.hp.com/ucmdb/1/types/impact}ImpactRuleName" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ImpactRuleNames", namespace = "http://schemas.hp.com/ucmdb/1/types/impact", propOrder = {
    "impactRuleName"
})
public class ImpactRuleNamesComplexType {

    @XmlElement(required = true)
    protected List<ImpactRuleName> impactRuleName;

    /**
     * Gets the value of the impactRuleName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the impactRuleName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getImpactRuleName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ImpactRuleName }
     * 
     * 
     */
    public List<ImpactRuleName> getImpactRuleName() {
        if (impactRuleName == null) {
            impactRuleName = new ArrayList<ImpactRuleName>();
        }
        return this.impactRuleName;
    }

}
