
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCIsByIdResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCIsByIdResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CIs" type="{http://schemas.hp.com/ucmdb/1/types}CIs"/&gt;
 *         &lt;element name="chunkInfo" type="{http://schemas.hp.com/ucmdb/1/types}ChunkInfo"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCIsByIdResponse", namespace = "http://schemas.hp.com/ucmdb/1/params/query", propOrder = {
    "cIs",
    "chunkInfo"
})
public class GetCIsByIdResponse {

    @XmlElement(name = "CIs", required = true)
    protected CIs cIs;
    @XmlElement(required = true)
    protected ChunkInfo chunkInfo;

    /**
     * Gets the value of the cIs property.
     * 
     * @return
     *     possible object is
     *     {@link CIs }
     *     
     */
    public CIs getCIs() {
        return cIs;
    }

    /**
     * Sets the value of the cIs property.
     * 
     * @param value
     *     allowed object is
     *     {@link CIs }
     *     
     */
    public void setCIs(CIs value) {
        this.cIs = value;
    }

    /**
     * Gets the value of the chunkInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ChunkInfo }
     *     
     */
    public ChunkInfo getChunkInfo() {
        return chunkInfo;
    }

    /**
     * Sets the value of the chunkInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChunkInfo }
     *     
     */
    public void setChunkInfo(ChunkInfo value) {
        this.chunkInfo = value;
    }

}
