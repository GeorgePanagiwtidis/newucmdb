
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CINode complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CINode"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="label" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CIs" type="{http://schemas.hp.com/ucmdb/1/types}CIs"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CINode", propOrder = {
    "label",
    "cIs"
})
public class CINode {

    @XmlElement(required = true)
    protected String label;
    @XmlElement(name = "CIs", required = true)
    protected CIs cIs;

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the cIs property.
     * 
     * @return
     *     possible object is
     *     {@link CIs }
     *     
     */
    public CIs getCIs() {
        return cIs;
    }

    /**
     * Sets the value of the cIs property.
     * 
     * @param value
     *     allowed object is
     *     {@link CIs }
     *     
     */
    public void setCIs(CIs value) {
        this.cIs = value;
    }

}
