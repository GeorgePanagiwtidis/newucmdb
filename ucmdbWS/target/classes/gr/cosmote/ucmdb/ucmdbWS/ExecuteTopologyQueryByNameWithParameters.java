
package gr.cosmote.ucmdb.ucmdbWS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for executeTopologyQueryByNameWithParameters complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="executeTopologyQueryByNameWithParameters"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="cmdbContext" type="{http://schemas.hp.com/ucmdb/1/types}CmdbContext"/&gt;
 *         &lt;element name="queryName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="parameterizedNodes" type="{http://schemas.hp.com/ucmdb/1/types}ParameterizedNode" maxOccurs="unbounded"/&gt;
 *         &lt;element name="queryTypedProperties" type="{http://schemas.hp.com/ucmdb/1/types/props}TypedPropertiesCollection" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "executeTopologyQueryByNameWithParameters", namespace = "http://schemas.hp.com/ucmdb/1/params/query", propOrder = {
    "cmdbContext",
    "queryName",
    "parameterizedNodes",
    "queryTypedProperties"
})
public class ExecuteTopologyQueryByNameWithParameters {

    @XmlElement(required = true)
    protected CmdbContext cmdbContext;
    @XmlElement(required = true)
    protected String queryName;
    @XmlElement(required = true)
    protected List<ParameterizedNode> parameterizedNodes;
    protected TypedPropertiesCollection queryTypedProperties;

    /**
     * Gets the value of the cmdbContext property.
     * 
     * @return
     *     possible object is
     *     {@link CmdbContext }
     *     
     */
    public CmdbContext getCmdbContext() {
        return cmdbContext;
    }

    /**
     * Sets the value of the cmdbContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link CmdbContext }
     *     
     */
    public void setCmdbContext(CmdbContext value) {
        this.cmdbContext = value;
    }

    /**
     * Gets the value of the queryName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueryName() {
        return queryName;
    }

    /**
     * Sets the value of the queryName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueryName(String value) {
        this.queryName = value;
    }

    /**
     * Gets the value of the parameterizedNodes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parameterizedNodes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParameterizedNodes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParameterizedNode }
     * 
     * 
     */
    public List<ParameterizedNode> getParameterizedNodes() {
        if (parameterizedNodes == null) {
            parameterizedNodes = new ArrayList<ParameterizedNode>();
        }
        return this.parameterizedNodes;
    }

    /**
     * Gets the value of the queryTypedProperties property.
     * 
     * @return
     *     possible object is
     *     {@link TypedPropertiesCollection }
     *     
     */
    public TypedPropertiesCollection getQueryTypedProperties() {
        return queryTypedProperties;
    }

    /**
     * Sets the value of the queryTypedProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link TypedPropertiesCollection }
     *     
     */
    public void setQueryTypedProperties(TypedPropertiesCollection value) {
        this.queryTypedProperties = value;
    }

}
