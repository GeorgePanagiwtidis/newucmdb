
package gr.cosmote.ucmdb.ucmdbWS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ShallowCIs complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShallowCIs"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="shallowCI" type="{http://schemas.hp.com/ucmdb/1/types}ShallowCI" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShallowCIs", propOrder = {
    "shallowCI"
})
public class ShallowCIs {

    @XmlElement(required = true)
    protected List<ShallowCI> shallowCI;

    /**
     * Gets the value of the shallowCI property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shallowCI property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShallowCI().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShallowCI }
     * 
     * 
     */
    public List<ShallowCI> getShallowCI() {
        if (shallowCI == null) {
            shallowCI = new ArrayList<ShallowCI>();
        }
        return this.shallowCI;
    }

}
