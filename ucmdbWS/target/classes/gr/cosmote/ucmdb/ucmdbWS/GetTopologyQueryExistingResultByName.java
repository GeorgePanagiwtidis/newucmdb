
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getTopologyQueryExistingResultByName complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getTopologyQueryExistingResultByName"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="cmdbContext" type="{http://schemas.hp.com/ucmdb/1/types}CmdbContext"/&gt;
 *         &lt;element name="queryName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="queryTypedProperties" type="{http://schemas.hp.com/ucmdb/1/types/props}TypedPropertiesCollection" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getTopologyQueryExistingResultByName", namespace = "http://schemas.hp.com/ucmdb/1/params/query", propOrder = {
    "cmdbContext",
    "queryName",
    "queryTypedProperties"
})
public class GetTopologyQueryExistingResultByName {

    @XmlElement(required = true)
    protected CmdbContext cmdbContext;
    @XmlElement(required = true)
    protected String queryName;
    protected TypedPropertiesCollection queryTypedProperties;

    /**
     * Gets the value of the cmdbContext property.
     * 
     * @return
     *     possible object is
     *     {@link CmdbContext }
     *     
     */
    public CmdbContext getCmdbContext() {
        return cmdbContext;
    }

    /**
     * Sets the value of the cmdbContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link CmdbContext }
     *     
     */
    public void setCmdbContext(CmdbContext value) {
        this.cmdbContext = value;
    }

    /**
     * Gets the value of the queryName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueryName() {
        return queryName;
    }

    /**
     * Sets the value of the queryName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueryName(String value) {
        this.queryName = value;
    }

    /**
     * Gets the value of the queryTypedProperties property.
     * 
     * @return
     *     possible object is
     *     {@link TypedPropertiesCollection }
     *     
     */
    public TypedPropertiesCollection getQueryTypedProperties() {
        return queryTypedProperties;
    }

    /**
     * Sets the value of the queryTypedProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link TypedPropertiesCollection }
     *     
     */
    public void setQueryTypedProperties(TypedPropertiesCollection value) {
        this.queryTypedProperties = value;
    }

}
