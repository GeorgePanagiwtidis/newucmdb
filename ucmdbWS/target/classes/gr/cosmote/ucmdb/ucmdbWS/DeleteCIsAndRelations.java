
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteCIsAndRelations complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteCIsAndRelations"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="cmdbContext" type="{http://schemas.hp.com/ucmdb/1/types}CmdbContext"/&gt;
 *         &lt;element name="dataStore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CIsAndRelationsUpdates" type="{http://schemas.hp.com/ucmdb/1/types/update}CIsAndRelationsUpdates"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteCIsAndRelations", namespace = "http://schemas.hp.com/ucmdb/1/params/update", propOrder = {
    "cmdbContext",
    "dataStore",
    "cIsAndRelationsUpdates"
})
public class DeleteCIsAndRelations {

    @XmlElement(required = true)
    protected CmdbContext cmdbContext;
    protected String dataStore;
    @XmlElement(name = "CIsAndRelationsUpdates", required = true)
    protected CIsAndRelationsUpdates cIsAndRelationsUpdates;

    /**
     * Gets the value of the cmdbContext property.
     * 
     * @return
     *     possible object is
     *     {@link CmdbContext }
     *     
     */
    public CmdbContext getCmdbContext() {
        return cmdbContext;
    }

    /**
     * Sets the value of the cmdbContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link CmdbContext }
     *     
     */
    public void setCmdbContext(CmdbContext value) {
        this.cmdbContext = value;
    }

    /**
     * Gets the value of the dataStore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataStore() {
        return dataStore;
    }

    /**
     * Sets the value of the dataStore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataStore(String value) {
        this.dataStore = value;
    }

    /**
     * Gets the value of the cIsAndRelationsUpdates property.
     * 
     * @return
     *     possible object is
     *     {@link CIsAndRelationsUpdates }
     *     
     */
    public CIsAndRelationsUpdates getCIsAndRelationsUpdates() {
        return cIsAndRelationsUpdates;
    }

    /**
     * Sets the value of the cIsAndRelationsUpdates property.
     * 
     * @param value
     *     allowed object is
     *     {@link CIsAndRelationsUpdates }
     *     
     */
    public void setCIsAndRelationsUpdates(CIsAndRelationsUpdates value) {
        this.cIsAndRelationsUpdates = value;
    }

}
