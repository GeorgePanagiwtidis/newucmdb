
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StrListCondition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StrListCondition"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://schemas.hp.com/ucmdb/1/types}Condition"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="condition" type="{http://schemas.hp.com/ucmdb/1/types}StrProp"/&gt;
 *         &lt;element name="strListOperator"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="InList"/&gt;
 *               &lt;enumeration value="IsNull"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StrListCondition", propOrder = {
    "condition",
    "strListOperator"
})
public class StrListCondition
    extends Condition
{

    @XmlElement(required = true)
    protected StrProp condition;
    @XmlElement(required = true)
    protected String strListOperator;

    /**
     * Gets the value of the condition property.
     * 
     * @return
     *     possible object is
     *     {@link StrProp }
     *     
     */
    public StrProp getCondition() {
        return condition;
    }

    /**
     * Sets the value of the condition property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrProp }
     *     
     */
    public void setCondition(StrProp value) {
        this.condition = value;
    }

    /**
     * Gets the value of the strListOperator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrListOperator() {
        return strListOperator;
    }

    /**
     * Sets the value of the strListOperator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrListOperator(String value) {
        this.strListOperator = value;
    }

}
