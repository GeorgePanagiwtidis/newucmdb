
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FloatCondition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FloatCondition"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://schemas.hp.com/ucmdb/1/types}Condition"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="condition" type="{http://schemas.hp.com/ucmdb/1/types}FloatProp"/&gt;
 *         &lt;element name="floatOperator"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="Equal"/&gt;
 *               &lt;enumeration value="NotEqual"/&gt;
 *               &lt;enumeration value="Greater"/&gt;
 *               &lt;enumeration value="GreaterEqual"/&gt;
 *               &lt;enumeration value="Less"/&gt;
 *               &lt;enumeration value="LessEqual"/&gt;
 *               &lt;enumeration value="In"/&gt;
 *               &lt;enumeration value="IsNull"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FloatCondition", propOrder = {
    "condition",
    "floatOperator"
})
public class FloatCondition
    extends Condition
{

    @XmlElement(required = true)
    protected FloatProp condition;
    @XmlElement(required = true)
    protected String floatOperator;

    /**
     * Gets the value of the condition property.
     * 
     * @return
     *     possible object is
     *     {@link FloatProp }
     *     
     */
    public FloatProp getCondition() {
        return condition;
    }

    /**
     * Sets the value of the condition property.
     * 
     * @param value
     *     allowed object is
     *     {@link FloatProp }
     *     
     */
    public void setCondition(FloatProp value) {
        this.condition = value;
    }

    /**
     * Gets the value of the floatOperator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFloatOperator() {
        return floatOperator;
    }

    /**
     * Sets the value of the floatOperator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFloatOperator(String value) {
        this.floatOperator = value;
    }

}
