
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for impactResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="impactResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="impactTopology" type="{http://schemas.hp.com/ucmdb/1/types}Topology"/&gt;
 *         &lt;element name="identifier" type="{http://schemas.hp.com/ucmdb/1/types/impact}Identifier"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "impactResponse", namespace = "http://schemas.hp.com/ucmdb/1/params/impact", propOrder = {
    "impactTopology",
    "identifier"
})
@XmlSeeAlso({
    CalculateImpactResponse.class
})
public abstract class ImpactResponse {

    @XmlElement(required = true)
    protected Topology impactTopology;
    @XmlElement(required = true)
    protected Identifier identifier;

    /**
     * Gets the value of the impactTopology property.
     * 
     * @return
     *     possible object is
     *     {@link Topology }
     *     
     */
    public Topology getImpactTopology() {
        return impactTopology;
    }

    /**
     * Sets the value of the impactTopology property.
     * 
     * @param value
     *     allowed object is
     *     {@link Topology }
     *     
     */
    public void setImpactTopology(Topology value) {
        this.impactTopology = value;
    }

    /**
     * Gets the value of the identifier property.
     * 
     * @return
     *     possible object is
     *     {@link Identifier }
     *     
     */
    public Identifier getIdentifier() {
        return identifier;
    }

    /**
     * Sets the value of the identifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link Identifier }
     *     
     */
    public void setIdentifier(Identifier value) {
        this.identifier = value;
    }

}
