
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addTriggerTQLRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addTriggerTQLRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="JobName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="TqlName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CmdbContext" type="{http://schemas.hp.com/ucmdb/1/types}CmdbContext"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addTriggerTQLRequest", namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", propOrder = {
    "jobName",
    "tqlName",
    "cmdbContext"
})
public class AddTriggerTQLRequest {

    @XmlElement(name = "JobName", required = true)
    protected String jobName;
    @XmlElement(name = "TqlName", required = true)
    protected String tqlName;
    @XmlElement(name = "CmdbContext", required = true)
    protected CmdbContext cmdbContext;

    /**
     * Gets the value of the jobName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobName() {
        return jobName;
    }

    /**
     * Sets the value of the jobName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobName(String value) {
        this.jobName = value;
    }

    /**
     * Gets the value of the tqlName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTqlName() {
        return tqlName;
    }

    /**
     * Sets the value of the tqlName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTqlName(String value) {
        this.tqlName = value;
    }

    /**
     * Gets the value of the cmdbContext property.
     * 
     * @return
     *     possible object is
     *     {@link CmdbContext }
     *     
     */
    public CmdbContext getCmdbContext() {
        return cmdbContext;
    }

    /**
     * Sets the value of the cmdbContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link CmdbContext }
     *     
     */
    public void setCmdbContext(CmdbContext value) {
        this.cmdbContext = value;
    }

}
