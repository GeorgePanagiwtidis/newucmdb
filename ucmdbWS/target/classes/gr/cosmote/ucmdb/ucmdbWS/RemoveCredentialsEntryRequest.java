
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for removeCredentialsEntryRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="removeCredentialsEntryRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="domainName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="protocolName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="credentialsEntryID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CmdbContext" type="{http://schemas.hp.com/ucmdb/1/types}CmdbContext"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "removeCredentialsEntryRequest", namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", propOrder = {
    "domainName",
    "protocolName",
    "credentialsEntryID",
    "cmdbContext"
})
public class RemoveCredentialsEntryRequest {

    @XmlElement(required = true)
    protected String domainName;
    @XmlElement(required = true)
    protected String protocolName;
    @XmlElement(required = true)
    protected String credentialsEntryID;
    @XmlElement(name = "CmdbContext", required = true)
    protected CmdbContext cmdbContext;

    /**
     * Gets the value of the domainName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDomainName() {
        return domainName;
    }

    /**
     * Sets the value of the domainName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDomainName(String value) {
        this.domainName = value;
    }

    /**
     * Gets the value of the protocolName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtocolName() {
        return protocolName;
    }

    /**
     * Sets the value of the protocolName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtocolName(String value) {
        this.protocolName = value;
    }

    /**
     * Gets the value of the credentialsEntryID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCredentialsEntryID() {
        return credentialsEntryID;
    }

    /**
     * Sets the value of the credentialsEntryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCredentialsEntryID(String value) {
        this.credentialsEntryID = value;
    }

    /**
     * Gets the value of the cmdbContext property.
     * 
     * @return
     *     possible object is
     *     {@link CmdbContext }
     *     
     */
    public CmdbContext getCmdbContext() {
        return cmdbContext;
    }

    /**
     * Sets the value of the cmdbContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link CmdbContext }
     *     
     */
    public void setCmdbContext(CmdbContext value) {
        this.cmdbContext = value;
    }

}
