
package gr.cosmote.ucmdb.ucmdbWS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ImpactPath complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ImpactPath"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ImpactStep" type="{http://schemas.hp.com/ucmdb/1/types/impact}ImpactStep" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ImpactPath", namespace = "http://schemas.hp.com/ucmdb/1/types/impact", propOrder = {
    "impactStep"
})
public class ImpactPath {

    @XmlElement(name = "ImpactStep")
    protected List<ImpactStep> impactStep;

    /**
     * Gets the value of the impactStep property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the impactStep property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getImpactStep().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ImpactStep }
     * 
     * 
     */
    public List<ImpactStep> getImpactStep() {
        if (impactStep == null) {
            impactStep = new ArrayList<ImpactStep>();
        }
        return this.impactStep;
    }

}
