
package gr.cosmote.ucmdb.ucmdbWS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ShallowRelations complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShallowRelations"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="shallowRelation" type="{http://schemas.hp.com/ucmdb/1/types}ShallowRelation" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShallowRelations", propOrder = {
    "shallowRelation"
})
public class ShallowRelations {

    @XmlElement(required = true)
    protected List<ShallowRelation> shallowRelation;

    /**
     * Gets the value of the shallowRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shallowRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShallowRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShallowRelation }
     * 
     * 
     */
    public List<ShallowRelation> getShallowRelation() {
        if (shallowRelation == null) {
            shallowRelation = new ArrayList<ShallowRelation>();
        }
        return this.shallowRelation;
    }

}
