
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StrListProp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StrListProp"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://schemas.hp.com/ucmdb/1/types}CIProp"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="strValues" type="{http://schemas.hp.com/ucmdb/1/types}strList" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StrListProp", propOrder = {
    "strValues"
})
public class StrListProp
    extends CIProp
{

    @XmlElementRef(name = "strValues", namespace = "http://schemas.hp.com/ucmdb/1/types", type = JAXBElement.class, required = false)
    protected JAXBElement<StrList> strValues;

    /**
     * Gets the value of the strValues property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link StrList }{@code >}
     *     
     */
    public JAXBElement<StrList> getStrValues() {
        return strValues;
    }

    /**
     * Sets the value of the strValues property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link StrList }{@code >}
     *     
     */
    public void setStrValues(JAXBElement<StrList> value) {
        this.strValues = value;
    }

}
