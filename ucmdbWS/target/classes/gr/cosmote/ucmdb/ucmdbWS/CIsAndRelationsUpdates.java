
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CIsAndRelationsUpdates complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CIsAndRelationsUpdates"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CIsForUpdate" type="{http://schemas.hp.com/ucmdb/1/types}CIs" minOccurs="0"/&gt;
 *         &lt;element name="relationsForUpdate" type="{http://schemas.hp.com/ucmdb/1/types}Relations" minOccurs="0"/&gt;
 *         &lt;element name="referencedCIs" type="{http://schemas.hp.com/ucmdb/1/types}CIs" minOccurs="0"/&gt;
 *         &lt;element name="referencedRelations" type="{http://schemas.hp.com/ucmdb/1/types}Relations" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CIsAndRelationsUpdates", namespace = "http://schemas.hp.com/ucmdb/1/types/update", propOrder = {
    "cIsForUpdate",
    "relationsForUpdate",
    "referencedCIs",
    "referencedRelations"
})
public class CIsAndRelationsUpdates {

    @XmlElement(name = "CIsForUpdate")
    protected CIs cIsForUpdate;
    protected Relations relationsForUpdate;
    protected CIs referencedCIs;
    protected Relations referencedRelations;

    /**
     * Gets the value of the cIsForUpdate property.
     * 
     * @return
     *     possible object is
     *     {@link CIs }
     *     
     */
    public CIs getCIsForUpdate() {
        return cIsForUpdate;
    }

    /**
     * Sets the value of the cIsForUpdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link CIs }
     *     
     */
    public void setCIsForUpdate(CIs value) {
        this.cIsForUpdate = value;
    }

    /**
     * Gets the value of the relationsForUpdate property.
     * 
     * @return
     *     possible object is
     *     {@link Relations }
     *     
     */
    public Relations getRelationsForUpdate() {
        return relationsForUpdate;
    }

    /**
     * Sets the value of the relationsForUpdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Relations }
     *     
     */
    public void setRelationsForUpdate(Relations value) {
        this.relationsForUpdate = value;
    }

    /**
     * Gets the value of the referencedCIs property.
     * 
     * @return
     *     possible object is
     *     {@link CIs }
     *     
     */
    public CIs getReferencedCIs() {
        return referencedCIs;
    }

    /**
     * Sets the value of the referencedCIs property.
     * 
     * @param value
     *     allowed object is
     *     {@link CIs }
     *     
     */
    public void setReferencedCIs(CIs value) {
        this.referencedCIs = value;
    }

    /**
     * Gets the value of the referencedRelations property.
     * 
     * @return
     *     possible object is
     *     {@link Relations }
     *     
     */
    public Relations getReferencedRelations() {
        return referencedRelations;
    }

    /**
     * Sets the value of the referencedRelations property.
     * 
     * @param value
     *     allowed object is
     *     {@link Relations }
     *     
     */
    public void setReferencedRelations(Relations value) {
        this.referencedRelations = value;
    }

}
