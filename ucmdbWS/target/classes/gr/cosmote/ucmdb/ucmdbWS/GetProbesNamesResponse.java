
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getProbesNamesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getProbesNamesResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="probesNames" type="{http://schemas.hp.com/ucmdb/1/types}strList"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getProbesNamesResponse", namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", propOrder = {
    "probesNames"
})
public class GetProbesNamesResponse {

    @XmlElement(required = true)
    protected StrList probesNames;

    /**
     * Gets the value of the probesNames property.
     * 
     * @return
     *     possible object is
     *     {@link StrList }
     *     
     */
    public StrList getProbesNames() {
        return probesNames;
    }

    /**
     * Sets the value of the probesNames property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrList }
     *     
     */
    public void setProbesNames(StrList value) {
        this.probesNames = value;
    }

}
