
package gr.cosmote.ucmdb.ucmdbWS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ParameterizedNodes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ParameterizedNodes"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="parameterizedNode" type="{http://schemas.hp.com/ucmdb/1/types}ParameterizedNode" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParameterizedNodes", propOrder = {
    "parameterizedNode"
})
public class ParameterizedNodes {

    @XmlElement(required = true)
    protected List<ParameterizedNode> parameterizedNode;

    /**
     * Gets the value of the parameterizedNode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parameterizedNode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParameterizedNode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParameterizedNode }
     * 
     * 
     */
    public List<ParameterizedNode> getParameterizedNode() {
        if (parameterizedNode == null) {
            parameterizedNode = new ArrayList<ParameterizedNode>();
        }
        return this.parameterizedNode;
    }

}
