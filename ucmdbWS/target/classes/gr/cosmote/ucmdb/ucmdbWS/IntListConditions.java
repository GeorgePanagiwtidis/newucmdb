
package gr.cosmote.ucmdb.ucmdbWS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IntListConditions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntListConditions"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://schemas.hp.com/ucmdb/1/types}Condition"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="intListCondition" type="{http://schemas.hp.com/ucmdb/1/types}IntListCondition" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntListConditions", propOrder = {
    "intListCondition"
})
public class IntListConditions
    extends Condition
{

    protected List<IntListCondition> intListCondition;

    /**
     * Gets the value of the intListCondition property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the intListCondition property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntListCondition().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntListCondition }
     * 
     * 
     */
    public List<IntListCondition> getIntListCondition() {
        if (intListCondition == null) {
            intListCondition = new ArrayList<IntListCondition>();
        }
        return this.intListCondition;
    }

}
