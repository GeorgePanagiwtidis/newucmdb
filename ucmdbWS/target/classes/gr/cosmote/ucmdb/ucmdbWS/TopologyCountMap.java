
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TopologyCountMap complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TopologyCountMap"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CICountNodes" type="{http://schemas.hp.com/ucmdb/1/types/query}CICountNodes" minOccurs="0"/&gt;
 *         &lt;element name="relationCountNodes" type="{http://schemas.hp.com/ucmdb/1/types/query}RelationCountNodes" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TopologyCountMap", namespace = "http://schemas.hp.com/ucmdb/1/types/query", propOrder = {
    "ciCountNodes",
    "relationCountNodes"
})
public class TopologyCountMap {

    @XmlElement(name = "CICountNodes")
    protected CICountNodes ciCountNodes;
    protected RelationCountNodes relationCountNodes;

    /**
     * Gets the value of the ciCountNodes property.
     * 
     * @return
     *     possible object is
     *     {@link CICountNodes }
     *     
     */
    public CICountNodes getCICountNodes() {
        return ciCountNodes;
    }

    /**
     * Sets the value of the ciCountNodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link CICountNodes }
     *     
     */
    public void setCICountNodes(CICountNodes value) {
        this.ciCountNodes = value;
    }

    /**
     * Gets the value of the relationCountNodes property.
     * 
     * @return
     *     possible object is
     *     {@link RelationCountNodes }
     *     
     */
    public RelationCountNodes getRelationCountNodes() {
        return relationCountNodes;
    }

    /**
     * Sets the value of the relationCountNodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link RelationCountNodes }
     *     
     */
    public void setRelationCountNodes(RelationCountNodes value) {
        this.relationCountNodes = value;
    }

}
