
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BooleanCondition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BooleanCondition"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://schemas.hp.com/ucmdb/1/types}Condition"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="condition" type="{http://schemas.hp.com/ucmdb/1/types}BooleanProp"/&gt;
 *         &lt;element name="booleanOperator"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="Equal"/&gt;
 *               &lt;enumeration value="NotEqual"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BooleanCondition", propOrder = {
    "condition",
    "booleanOperator"
})
public class BooleanCondition
    extends Condition
{

    @XmlElement(required = true)
    protected BooleanProp condition;
    @XmlElement(required = true)
    protected String booleanOperator;

    /**
     * Gets the value of the condition property.
     * 
     * @return
     *     possible object is
     *     {@link BooleanProp }
     *     
     */
    public BooleanProp getCondition() {
        return condition;
    }

    /**
     * Sets the value of the condition property.
     * 
     * @param value
     *     allowed object is
     *     {@link BooleanProp }
     *     
     */
    public void setCondition(BooleanProp value) {
        this.condition = value;
    }

    /**
     * Gets the value of the booleanOperator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBooleanOperator() {
        return booleanOperator;
    }

    /**
     * Sets the value of the booleanOperator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBooleanOperator(String value) {
        this.booleanOperator = value;
    }

}
