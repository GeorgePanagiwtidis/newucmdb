
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IntListCondition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntListCondition"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://schemas.hp.com/ucmdb/1/types}Condition"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="condition" type="{http://schemas.hp.com/ucmdb/1/types}IntListProp"/&gt;
 *         &lt;element name="intListOperator"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="InList"/&gt;
 *               &lt;enumeration value="IsNull"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntListCondition", propOrder = {
    "condition",
    "intListOperator"
})
public class IntListCondition
    extends Condition
{

    @XmlElement(required = true)
    protected IntListProp condition;
    @XmlElement(required = true)
    protected String intListOperator;

    /**
     * Gets the value of the condition property.
     * 
     * @return
     *     possible object is
     *     {@link IntListProp }
     *     
     */
    public IntListProp getCondition() {
        return condition;
    }

    /**
     * Sets the value of the condition property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntListProp }
     *     
     */
    public void setCondition(IntListProp value) {
        this.condition = value;
    }

    /**
     * Gets the value of the intListOperator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntListOperator() {
        return intListOperator;
    }

    /**
     * Sets the value of the intListOperator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntListOperator(String value) {
        this.intListOperator = value;
    }

}
