
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PredefinedTypedProperties complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PredefinedTypedProperties"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="qualifierProperties" type="{http://schemas.hp.com/ucmdb/1/types/props}QualifierProperties" minOccurs="0"/&gt;
 *         &lt;element name="simpleTypedPredefinedProperties" type="{http://schemas.hp.com/ucmdb/1/types/props}SimpleTypedPredefinedPropertyCollection" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PredefinedTypedProperties", namespace = "http://schemas.hp.com/ucmdb/1/types/props", propOrder = {
    "qualifierProperties",
    "simpleTypedPredefinedProperties"
})
public class PredefinedTypedProperties {

    protected QualifierProperties qualifierProperties;
    protected SimpleTypedPredefinedPropertyCollection simpleTypedPredefinedProperties;

    /**
     * Gets the value of the qualifierProperties property.
     * 
     * @return
     *     possible object is
     *     {@link QualifierProperties }
     *     
     */
    public QualifierProperties getQualifierProperties() {
        return qualifierProperties;
    }

    /**
     * Sets the value of the qualifierProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link QualifierProperties }
     *     
     */
    public void setQualifierProperties(QualifierProperties value) {
        this.qualifierProperties = value;
    }

    /**
     * Gets the value of the simpleTypedPredefinedProperties property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleTypedPredefinedPropertyCollection }
     *     
     */
    public SimpleTypedPredefinedPropertyCollection getSimpleTypedPredefinedProperties() {
        return simpleTypedPredefinedProperties;
    }

    /**
     * Sets the value of the simpleTypedPredefinedProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleTypedPredefinedPropertyCollection }
     *     
     */
    public void setSimpleTypedPredefinedProperties(SimpleTypedPredefinedPropertyCollection value) {
        this.simpleTypedPredefinedProperties = value;
    }

}
