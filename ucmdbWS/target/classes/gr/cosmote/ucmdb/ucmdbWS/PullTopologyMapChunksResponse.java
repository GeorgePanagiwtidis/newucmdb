
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for pullTopologyMapChunksResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="pullTopologyMapChunksResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="topologyMap" type="{http://schemas.hp.com/ucmdb/1/types}TopologyMap"/&gt;
 *         &lt;element name="comments" type="{http://schemas.hp.com/ucmdb/1/types}ResComments" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pullTopologyMapChunksResponse", namespace = "http://schemas.hp.com/ucmdb/1/params/query", propOrder = {
    "topologyMap",
    "comments"
})
public class PullTopologyMapChunksResponse {

    @XmlElement(required = true)
    protected TopologyMap topologyMap;
    protected ResComments comments;

    /**
     * Gets the value of the topologyMap property.
     * 
     * @return
     *     possible object is
     *     {@link TopologyMap }
     *     
     */
    public TopologyMap getTopologyMap() {
        return topologyMap;
    }

    /**
     * Sets the value of the topologyMap property.
     * 
     * @param value
     *     allowed object is
     *     {@link TopologyMap }
     *     
     */
    public void setTopologyMap(TopologyMap value) {
        this.topologyMap = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link ResComments }
     *     
     */
    public ResComments getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResComments }
     *     
     */
    public void setComments(ResComments value) {
        this.comments = value;
    }

}
