
package gr.cosmote.ucmdb.ucmdbWS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LongConditions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LongConditions"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://schemas.hp.com/ucmdb/1/types}Condition"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="longCondition" type="{http://schemas.hp.com/ucmdb/1/types}LongCondition" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LongConditions", propOrder = {
    "longCondition"
})
public class LongConditions
    extends Condition
{

    protected List<LongCondition> longCondition;

    /**
     * Gets the value of the longCondition property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the longCondition property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLongCondition().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LongCondition }
     * 
     * 
     */
    public List<LongCondition> getLongCondition() {
        if (longCondition == null) {
            longCondition = new ArrayList<LongCondition>();
        }
        return this.longCondition;
    }

}
