
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CIProperties complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CIProperties"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dateProps" type="{http://schemas.hp.com/ucmdb/1/types}DateProps" minOccurs="0"/&gt;
 *         &lt;element name="doubleProps" type="{http://schemas.hp.com/ucmdb/1/types}DoubleProps" minOccurs="0"/&gt;
 *         &lt;element name="floatProps" type="{http://schemas.hp.com/ucmdb/1/types}FloatProps" minOccurs="0"/&gt;
 *         &lt;element name="intListProps" type="{http://schemas.hp.com/ucmdb/1/types}IntListProps" minOccurs="0"/&gt;
 *         &lt;element name="intProps" type="{http://schemas.hp.com/ucmdb/1/types}IntProps" minOccurs="0"/&gt;
 *         &lt;element name="strProps" type="{http://schemas.hp.com/ucmdb/1/types}StrProps" minOccurs="0"/&gt;
 *         &lt;element name="strListProps" type="{http://schemas.hp.com/ucmdb/1/types}StrListProps" minOccurs="0"/&gt;
 *         &lt;element name="longProps" type="{http://schemas.hp.com/ucmdb/1/types}LongProps" minOccurs="0"/&gt;
 *         &lt;element name="bytesProps" type="{http://schemas.hp.com/ucmdb/1/types}BytesProps" minOccurs="0"/&gt;
 *         &lt;element name="xmlProps" type="{http://schemas.hp.com/ucmdb/1/types}XmlProps" minOccurs="0"/&gt;
 *         &lt;element name="booleanProps" type="{http://schemas.hp.com/ucmdb/1/types}BooleanProps" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CIProperties", propOrder = {
    "dateProps",
    "doubleProps",
    "floatProps",
    "intListProps",
    "intProps",
    "strProps",
    "strListProps",
    "longProps",
    "bytesProps",
    "xmlProps",
    "booleanProps"
})
public class CIProperties {

    protected DateProps dateProps;
    protected DoubleProps doubleProps;
    protected FloatProps floatProps;
    protected IntListProps intListProps;
    protected IntProps intProps;
    protected StrProps strProps;
    protected StrListProps strListProps;
    protected LongProps longProps;
    protected BytesProps bytesProps;
    protected XmlProps xmlProps;
    protected BooleanProps booleanProps;

    /**
     * Gets the value of the dateProps property.
     * 
     * @return
     *     possible object is
     *     {@link DateProps }
     *     
     */
    public DateProps getDateProps() {
        return dateProps;
    }

    /**
     * Sets the value of the dateProps property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateProps }
     *     
     */
    public void setDateProps(DateProps value) {
        this.dateProps = value;
    }

    /**
     * Gets the value of the doubleProps property.
     * 
     * @return
     *     possible object is
     *     {@link DoubleProps }
     *     
     */
    public DoubleProps getDoubleProps() {
        return doubleProps;
    }

    /**
     * Sets the value of the doubleProps property.
     * 
     * @param value
     *     allowed object is
     *     {@link DoubleProps }
     *     
     */
    public void setDoubleProps(DoubleProps value) {
        this.doubleProps = value;
    }

    /**
     * Gets the value of the floatProps property.
     * 
     * @return
     *     possible object is
     *     {@link FloatProps }
     *     
     */
    public FloatProps getFloatProps() {
        return floatProps;
    }

    /**
     * Sets the value of the floatProps property.
     * 
     * @param value
     *     allowed object is
     *     {@link FloatProps }
     *     
     */
    public void setFloatProps(FloatProps value) {
        this.floatProps = value;
    }

    /**
     * Gets the value of the intListProps property.
     * 
     * @return
     *     possible object is
     *     {@link IntListProps }
     *     
     */
    public IntListProps getIntListProps() {
        return intListProps;
    }

    /**
     * Sets the value of the intListProps property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntListProps }
     *     
     */
    public void setIntListProps(IntListProps value) {
        this.intListProps = value;
    }

    /**
     * Gets the value of the intProps property.
     * 
     * @return
     *     possible object is
     *     {@link IntProps }
     *     
     */
    public IntProps getIntProps() {
        return intProps;
    }

    /**
     * Sets the value of the intProps property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntProps }
     *     
     */
    public void setIntProps(IntProps value) {
        this.intProps = value;
    }

    /**
     * Gets the value of the strProps property.
     * 
     * @return
     *     possible object is
     *     {@link StrProps }
     *     
     */
    public StrProps getStrProps() {
        return strProps;
    }

    /**
     * Sets the value of the strProps property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrProps }
     *     
     */
    public void setStrProps(StrProps value) {
        this.strProps = value;
    }

    /**
     * Gets the value of the strListProps property.
     * 
     * @return
     *     possible object is
     *     {@link StrListProps }
     *     
     */
    public StrListProps getStrListProps() {
        return strListProps;
    }

    /**
     * Sets the value of the strListProps property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrListProps }
     *     
     */
    public void setStrListProps(StrListProps value) {
        this.strListProps = value;
    }

    /**
     * Gets the value of the longProps property.
     * 
     * @return
     *     possible object is
     *     {@link LongProps }
     *     
     */
    public LongProps getLongProps() {
        return longProps;
    }

    /**
     * Sets the value of the longProps property.
     * 
     * @param value
     *     allowed object is
     *     {@link LongProps }
     *     
     */
    public void setLongProps(LongProps value) {
        this.longProps = value;
    }

    /**
     * Gets the value of the bytesProps property.
     * 
     * @return
     *     possible object is
     *     {@link BytesProps }
     *     
     */
    public BytesProps getBytesProps() {
        return bytesProps;
    }

    /**
     * Sets the value of the bytesProps property.
     * 
     * @param value
     *     allowed object is
     *     {@link BytesProps }
     *     
     */
    public void setBytesProps(BytesProps value) {
        this.bytesProps = value;
    }

    /**
     * Gets the value of the xmlProps property.
     * 
     * @return
     *     possible object is
     *     {@link XmlProps }
     *     
     */
    public XmlProps getXmlProps() {
        return xmlProps;
    }

    /**
     * Sets the value of the xmlProps property.
     * 
     * @param value
     *     allowed object is
     *     {@link XmlProps }
     *     
     */
    public void setXmlProps(XmlProps value) {
        this.xmlProps = value;
    }

    /**
     * Gets the value of the booleanProps property.
     * 
     * @return
     *     possible object is
     *     {@link BooleanProps }
     *     
     */
    public BooleanProps getBooleanProps() {
        return booleanProps;
    }

    /**
     * Sets the value of the booleanProps property.
     * 
     * @param value
     *     allowed object is
     *     {@link BooleanProps }
     *     
     */
    public void setBooleanProps(BooleanProps value) {
        this.booleanProps = value;
    }

}
