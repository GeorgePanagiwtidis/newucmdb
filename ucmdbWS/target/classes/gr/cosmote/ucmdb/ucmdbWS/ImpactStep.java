
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ImpactStep complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ImpactStep"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="triggerCI" type="{http://schemas.hp.com/ucmdb/1/types/impact}ImpactedCIs"/&gt;
 *         &lt;element name="ruleName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="impactedCI" type="{http://schemas.hp.com/ucmdb/1/types/impact}ImpactedCIs"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ImpactStep", namespace = "http://schemas.hp.com/ucmdb/1/types/impact", propOrder = {
    "triggerCI",
    "ruleName",
    "impactedCI"
})
public class ImpactStep {

    @XmlElement(required = true)
    protected ImpactedCIs triggerCI;
    @XmlElement(required = true)
    protected String ruleName;
    @XmlElement(required = true)
    protected ImpactedCIs impactedCI;

    /**
     * Gets the value of the triggerCI property.
     * 
     * @return
     *     possible object is
     *     {@link ImpactedCIs }
     *     
     */
    public ImpactedCIs getTriggerCI() {
        return triggerCI;
    }

    /**
     * Sets the value of the triggerCI property.
     * 
     * @param value
     *     allowed object is
     *     {@link ImpactedCIs }
     *     
     */
    public void setTriggerCI(ImpactedCIs value) {
        this.triggerCI = value;
    }

    /**
     * Gets the value of the ruleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRuleName() {
        return ruleName;
    }

    /**
     * Sets the value of the ruleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRuleName(String value) {
        this.ruleName = value;
    }

    /**
     * Gets the value of the impactedCI property.
     * 
     * @return
     *     possible object is
     *     {@link ImpactedCIs }
     *     
     */
    public ImpactedCIs getImpactedCI() {
        return impactedCI;
    }

    /**
     * Sets the value of the impactedCI property.
     * 
     * @param value
     *     allowed object is
     *     {@link ImpactedCIs }
     *     
     */
    public void setImpactedCI(ImpactedCIs value) {
        this.impactedCI = value;
    }

}
