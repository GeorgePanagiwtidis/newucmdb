
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IPRange complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IPRange"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Start" type="{http://schemas.hp.com/ucmdb/discovery/1/types}IP"/&gt;
 *         &lt;element name="End" type="{http://schemas.hp.com/ucmdb/discovery/1/types}IP"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IPRange", namespace = "http://schemas.hp.com/ucmdb/discovery/1/types", propOrder = {
    "start",
    "end"
})
public class IPRange {

    @XmlElement(name = "Start", required = true)
    protected IP start;
    @XmlElement(name = "End", required = true)
    protected IP end;

    /**
     * Gets the value of the start property.
     * 
     * @return
     *     possible object is
     *     {@link IP }
     *     
     */
    public IP getStart() {
        return start;
    }

    /**
     * Sets the value of the start property.
     * 
     * @param value
     *     allowed object is
     *     {@link IP }
     *     
     */
    public void setStart(IP value) {
        this.start = value;
    }

    /**
     * Gets the value of the end property.
     * 
     * @return
     *     possible object is
     *     {@link IP }
     *     
     */
    public IP getEnd() {
        return end;
    }

    /**
     * Sets the value of the end property.
     * 
     * @param value
     *     allowed object is
     *     {@link IP }
     *     
     */
    public void setEnd(IP value) {
        this.end = value;
    }

}
