
package gr.cosmote.ucmdb.ucmdbWS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ImpactPaths complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ImpactPaths"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ImpactPath" type="{http://schemas.hp.com/ucmdb/1/types/impact}ImpactPath" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ImpactPaths", namespace = "http://schemas.hp.com/ucmdb/1/types/impact", propOrder = {
    "impactPath"
})
public class ImpactPaths {

    @XmlElement(name = "ImpactPath")
    protected List<ImpactPath> impactPath;

    /**
     * Gets the value of the impactPath property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the impactPath property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getImpactPath().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ImpactPath }
     * 
     * 
     */
    public List<ImpactPath> getImpactPath() {
        if (impactPath == null) {
            impactPath = new ArrayList<ImpactPath>();
        }
        return this.impactPath;
    }

}
