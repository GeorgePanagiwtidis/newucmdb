
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Qualifier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Qualifier"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="qualifierProps" type="{http://schemas.hp.com/ucmdb/1/types/classmodel}QualifiersProperties" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Qualifier", namespace = "http://schemas.hp.com/ucmdb/1/types/classmodel", propOrder = {
    "name",
    "qualifierProps"
})
public class Qualifier {

    @XmlElement(required = true)
    protected String name;
    protected QualifiersProperties qualifierProps;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the qualifierProps property.
     * 
     * @return
     *     possible object is
     *     {@link QualifiersProperties }
     *     
     */
    public QualifiersProperties getQualifierProps() {
        return qualifierProps;
    }

    /**
     * Sets the value of the qualifierProps property.
     * 
     * @param value
     *     allowed object is
     *     {@link QualifiersProperties }
     *     
     */
    public void setQualifierProps(QualifiersProperties value) {
        this.qualifierProps = value;
    }

}
