
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for dispatchAdHocJobResopnse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="dispatchAdHocJobResopnse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OSHVResult" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dispatchAdHocJobResopnse", namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", propOrder = {
    "oshvResult"
})
public class DispatchAdHocJobResopnse {

    @XmlElement(name = "OSHVResult", required = true)
    protected String oshvResult;

    /**
     * Gets the value of the oshvResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOSHVResult() {
        return oshvResult;
    }

    /**
     * Sets the value of the oshvResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOSHVResult(String value) {
        this.oshvResult = value;
    }

}
