
package gr.cosmote.ucmdb.ucmdbWS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for XmlConditions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XmlConditions"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://schemas.hp.com/ucmdb/1/types}Condition"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="xmlCondition" type="{http://schemas.hp.com/ucmdb/1/types}XmlCondition" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XmlConditions", propOrder = {
    "xmlCondition"
})
public class XmlConditions
    extends Condition
{

    protected List<XmlCondition> xmlCondition;

    /**
     * Gets the value of the xmlCondition property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the xmlCondition property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getXmlCondition().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XmlCondition }
     * 
     * 
     */
    public List<XmlCondition> getXmlCondition() {
        if (xmlCondition == null) {
            xmlCondition = new ArrayList<XmlCondition>();
        }
        return this.xmlCondition;
    }

}
