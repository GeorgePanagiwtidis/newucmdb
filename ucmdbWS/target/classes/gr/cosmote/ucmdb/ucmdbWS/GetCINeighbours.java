
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCINeighbours complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCINeighbours"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="cmdbContext" type="{http://schemas.hp.com/ucmdb/1/types}CmdbContext"/&gt;
 *         &lt;element name="ID" type="{http://schemas.hp.com/ucmdb/1/types}ID"/&gt;
 *         &lt;element name="neighbourType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CIProperties" type="{http://schemas.hp.com/ucmdb/1/types/props}TypedPropertiesCollection" minOccurs="0"/&gt;
 *         &lt;element name="relationProperties" type="{http://schemas.hp.com/ucmdb/1/types/props}TypedPropertiesCollection" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCINeighbours", namespace = "http://schemas.hp.com/ucmdb/1/params/query", propOrder = {
    "cmdbContext",
    "id",
    "neighbourType",
    "ciProperties",
    "relationProperties"
})
public class GetCINeighbours {

    @XmlElement(required = true)
    protected CmdbContext cmdbContext;
    @XmlElement(name = "ID", required = true)
    protected ID id;
    @XmlElement(required = true, defaultValue = "object")
    protected String neighbourType;
    @XmlElement(name = "CIProperties")
    protected TypedPropertiesCollection ciProperties;
    protected TypedPropertiesCollection relationProperties;

    /**
     * Gets the value of the cmdbContext property.
     * 
     * @return
     *     possible object is
     *     {@link CmdbContext }
     *     
     */
    public CmdbContext getCmdbContext() {
        return cmdbContext;
    }

    /**
     * Sets the value of the cmdbContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link CmdbContext }
     *     
     */
    public void setCmdbContext(CmdbContext value) {
        this.cmdbContext = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link ID }
     *     
     */
    public ID getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link ID }
     *     
     */
    public void setID(ID value) {
        this.id = value;
    }

    /**
     * Gets the value of the neighbourType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNeighbourType() {
        return neighbourType;
    }

    /**
     * Sets the value of the neighbourType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNeighbourType(String value) {
        this.neighbourType = value;
    }

    /**
     * Gets the value of the ciProperties property.
     * 
     * @return
     *     possible object is
     *     {@link TypedPropertiesCollection }
     *     
     */
    public TypedPropertiesCollection getCIProperties() {
        return ciProperties;
    }

    /**
     * Sets the value of the ciProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link TypedPropertiesCollection }
     *     
     */
    public void setCIProperties(TypedPropertiesCollection value) {
        this.ciProperties = value;
    }

    /**
     * Gets the value of the relationProperties property.
     * 
     * @return
     *     possible object is
     *     {@link TypedPropertiesCollection }
     *     
     */
    public TypedPropertiesCollection getRelationProperties() {
        return relationProperties;
    }

    /**
     * Sets the value of the relationProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link TypedPropertiesCollection }
     *     
     */
    public void setRelationProperties(TypedPropertiesCollection value) {
        this.relationProperties = value;
    }

}
