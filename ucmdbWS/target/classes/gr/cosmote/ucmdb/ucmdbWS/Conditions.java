
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Conditions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Conditions"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="booleanConditions" type="{http://schemas.hp.com/ucmdb/1/types}BooleanConditions" minOccurs="0"/&gt;
 *         &lt;element name="dateConditions" type="{http://schemas.hp.com/ucmdb/1/types}DateConditions" minOccurs="0"/&gt;
 *         &lt;element name="doubleConditions" type="{http://schemas.hp.com/ucmdb/1/types}DoubleConditions" minOccurs="0"/&gt;
 *         &lt;element name="floatConditions" type="{http://schemas.hp.com/ucmdb/1/types}FloatConditions" minOccurs="0"/&gt;
 *         &lt;element name="intConditions" type="{http://schemas.hp.com/ucmdb/1/types}IntConditions" minOccurs="0"/&gt;
 *         &lt;element name="intListConditions" type="{http://schemas.hp.com/ucmdb/1/types}IntListConditions" minOccurs="0"/&gt;
 *         &lt;element name="strConditions" type="{http://schemas.hp.com/ucmdb/1/types}StrConditions" minOccurs="0"/&gt;
 *         &lt;element name="strListConditions" type="{http://schemas.hp.com/ucmdb/1/types}StrListConditions" minOccurs="0"/&gt;
 *         &lt;element name="xmlConditions" type="{http://schemas.hp.com/ucmdb/1/types}XmlConditions" minOccurs="0"/&gt;
 *         &lt;element name="longConditions" type="{http://schemas.hp.com/ucmdb/1/types}LongConditions" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Conditions", propOrder = {
    "booleanConditions",
    "dateConditions",
    "doubleConditions",
    "floatConditions",
    "intConditions",
    "intListConditions",
    "strConditions",
    "strListConditions",
    "xmlConditions",
    "longConditions"
})
public class Conditions {

    protected BooleanConditions booleanConditions;
    protected DateConditions dateConditions;
    protected DoubleConditions doubleConditions;
    protected FloatConditions floatConditions;
    protected IntConditions intConditions;
    protected IntListConditions intListConditions;
    protected StrConditions strConditions;
    protected StrListConditions strListConditions;
    protected XmlConditions xmlConditions;
    protected LongConditions longConditions;

    /**
     * Gets the value of the booleanConditions property.
     * 
     * @return
     *     possible object is
     *     {@link BooleanConditions }
     *     
     */
    public BooleanConditions getBooleanConditions() {
        return booleanConditions;
    }

    /**
     * Sets the value of the booleanConditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link BooleanConditions }
     *     
     */
    public void setBooleanConditions(BooleanConditions value) {
        this.booleanConditions = value;
    }

    /**
     * Gets the value of the dateConditions property.
     * 
     * @return
     *     possible object is
     *     {@link DateConditions }
     *     
     */
    public DateConditions getDateConditions() {
        return dateConditions;
    }

    /**
     * Sets the value of the dateConditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateConditions }
     *     
     */
    public void setDateConditions(DateConditions value) {
        this.dateConditions = value;
    }

    /**
     * Gets the value of the doubleConditions property.
     * 
     * @return
     *     possible object is
     *     {@link DoubleConditions }
     *     
     */
    public DoubleConditions getDoubleConditions() {
        return doubleConditions;
    }

    /**
     * Sets the value of the doubleConditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link DoubleConditions }
     *     
     */
    public void setDoubleConditions(DoubleConditions value) {
        this.doubleConditions = value;
    }

    /**
     * Gets the value of the floatConditions property.
     * 
     * @return
     *     possible object is
     *     {@link FloatConditions }
     *     
     */
    public FloatConditions getFloatConditions() {
        return floatConditions;
    }

    /**
     * Sets the value of the floatConditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link FloatConditions }
     *     
     */
    public void setFloatConditions(FloatConditions value) {
        this.floatConditions = value;
    }

    /**
     * Gets the value of the intConditions property.
     * 
     * @return
     *     possible object is
     *     {@link IntConditions }
     *     
     */
    public IntConditions getIntConditions() {
        return intConditions;
    }

    /**
     * Sets the value of the intConditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntConditions }
     *     
     */
    public void setIntConditions(IntConditions value) {
        this.intConditions = value;
    }

    /**
     * Gets the value of the intListConditions property.
     * 
     * @return
     *     possible object is
     *     {@link IntListConditions }
     *     
     */
    public IntListConditions getIntListConditions() {
        return intListConditions;
    }

    /**
     * Sets the value of the intListConditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntListConditions }
     *     
     */
    public void setIntListConditions(IntListConditions value) {
        this.intListConditions = value;
    }

    /**
     * Gets the value of the strConditions property.
     * 
     * @return
     *     possible object is
     *     {@link StrConditions }
     *     
     */
    public StrConditions getStrConditions() {
        return strConditions;
    }

    /**
     * Sets the value of the strConditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrConditions }
     *     
     */
    public void setStrConditions(StrConditions value) {
        this.strConditions = value;
    }

    /**
     * Gets the value of the strListConditions property.
     * 
     * @return
     *     possible object is
     *     {@link StrListConditions }
     *     
     */
    public StrListConditions getStrListConditions() {
        return strListConditions;
    }

    /**
     * Sets the value of the strListConditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrListConditions }
     *     
     */
    public void setStrListConditions(StrListConditions value) {
        this.strListConditions = value;
    }

    /**
     * Gets the value of the xmlConditions property.
     * 
     * @return
     *     possible object is
     *     {@link XmlConditions }
     *     
     */
    public XmlConditions getXmlConditions() {
        return xmlConditions;
    }

    /**
     * Sets the value of the xmlConditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link XmlConditions }
     *     
     */
    public void setXmlConditions(XmlConditions value) {
        this.xmlConditions = value;
    }

    /**
     * Gets the value of the longConditions property.
     * 
     * @return
     *     possible object is
     *     {@link LongConditions }
     *     
     */
    public LongConditions getLongConditions() {
        return longConditions;
    }

    /**
     * Sets the value of the longConditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link LongConditions }
     *     
     */
    public void setLongConditions(LongConditions value) {
        this.longConditions = value;
    }

}
