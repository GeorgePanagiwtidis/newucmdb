
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StrCondition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StrCondition"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://schemas.hp.com/ucmdb/1/types}Condition"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="condition" type="{http://schemas.hp.com/ucmdb/1/types}StrProp"/&gt;
 *         &lt;element name="strOperator"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="Equal"/&gt;
 *               &lt;enumeration value="NotEqual"/&gt;
 *               &lt;enumeration value="Like"/&gt;
 *               &lt;enumeration value="LikeIgnoreCase"/&gt;
 *               &lt;enumeration value="In"/&gt;
 *               &lt;enumeration value="IsNull"/&gt;
 *               &lt;enumeration value="EqualIgnoreCase"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StrCondition", propOrder = {
    "condition",
    "strOperator"
})
public class StrCondition
    extends Condition
{

    @XmlElement(required = true)
    protected StrProp condition;
    @XmlElement(required = true)
    protected String strOperator;

    /**
     * Gets the value of the condition property.
     * 
     * @return
     *     possible object is
     *     {@link StrProp }
     *     
     */
    public StrProp getCondition() {
        return condition;
    }

    /**
     * Sets the value of the condition property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrProp }
     *     
     */
    public void setCondition(StrProp value) {
        this.condition = value;
    }

    /**
     * Gets the value of the strOperator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrOperator() {
        return strOperator;
    }

    /**
     * Sets the value of the strOperator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrOperator(String value) {
        this.strOperator = value;
    }

}
