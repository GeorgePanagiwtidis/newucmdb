
package gr.cosmote.ucmdb.ucmdbWS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for executeTopologyQueryWithParameters complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="executeTopologyQueryWithParameters"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="cmdbContext" type="{http://schemas.hp.com/ucmdb/1/types}CmdbContext"/&gt;
 *         &lt;element name="queryXml" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="parameterizedNodes" type="{http://schemas.hp.com/ucmdb/1/types}ParameterizedNode" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "executeTopologyQueryWithParameters", namespace = "http://schemas.hp.com/ucmdb/1/params/query", propOrder = {
    "cmdbContext",
    "queryXml",
    "parameterizedNodes"
})
public class ExecuteTopologyQueryWithParameters {

    @XmlElement(required = true)
    protected CmdbContext cmdbContext;
    @XmlElement(required = true)
    protected String queryXml;
    @XmlElement(required = true)
    protected List<ParameterizedNode> parameterizedNodes;

    /**
     * Gets the value of the cmdbContext property.
     * 
     * @return
     *     possible object is
     *     {@link CmdbContext }
     *     
     */
    public CmdbContext getCmdbContext() {
        return cmdbContext;
    }

    /**
     * Sets the value of the cmdbContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link CmdbContext }
     *     
     */
    public void setCmdbContext(CmdbContext value) {
        this.cmdbContext = value;
    }

    /**
     * Gets the value of the queryXml property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueryXml() {
        return queryXml;
    }

    /**
     * Sets the value of the queryXml property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueryXml(String value) {
        this.queryXml = value;
    }

    /**
     * Gets the value of the parameterizedNodes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parameterizedNodes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParameterizedNodes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParameterizedNode }
     * 
     * 
     */
    public List<ParameterizedNode> getParameterizedNodes() {
        if (parameterizedNodes == null) {
            parameterizedNodes = new ArrayList<ParameterizedNode>();
        }
        return this.parameterizedNodes;
    }

}
