
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addCredentialsEntryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addCredentialsEntryResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="credentialsEntryID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addCredentialsEntryResponse", namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", propOrder = {
    "credentialsEntryID"
})
public class AddCredentialsEntryResponse {

    @XmlElement(required = true)
    protected String credentialsEntryID;

    /**
     * Gets the value of the credentialsEntryID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCredentialsEntryID() {
        return credentialsEntryID;
    }

    /**
     * Sets the value of the credentialsEntryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCredentialsEntryID(String value) {
        this.credentialsEntryID = value;
    }

}
