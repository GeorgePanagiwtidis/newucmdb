
package gr.cosmote.ucmdb.ucmdbWS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addCIsAndRelationsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addCIsAndRelationsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://schemas.hp.com/ucmdb/1/params/update}CreatedIDsMap" maxOccurs="unbounded"/&gt;
 *         &lt;element name="comments" type="{http://schemas.hp.com/ucmdb/1/types}ResComments"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addCIsAndRelationsResponse", namespace = "http://schemas.hp.com/ucmdb/1/params/update", propOrder = {
    "createdIDsMap",
    "comments"
})
public class AddCIsAndRelationsResponse {

    @XmlElement(name = "CreatedIDsMap", required = true)
    protected List<ClientIDToCmdbID> createdIDsMap;
    @XmlElement(required = true)
    protected ResComments comments;

    /**
     * Gets the value of the createdIDsMap property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the createdIDsMap property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreatedIDsMap().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClientIDToCmdbID }
     * 
     * 
     */
    public List<ClientIDToCmdbID> getCreatedIDsMap() {
        if (createdIDsMap == null) {
            createdIDsMap = new ArrayList<ClientIDToCmdbID>();
        }
        return this.createdIDsMap;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link ResComments }
     *     
     */
    public ResComments getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResComments }
     *     
     */
    public void setComments(ResComments value) {
        this.comments = value;
    }

}
