
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TopologyMap complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TopologyMap"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CINodes" type="{http://schemas.hp.com/ucmdb/1/types}CINodes" minOccurs="0"/&gt;
 *         &lt;element name="relationNodes" type="{http://schemas.hp.com/ucmdb/1/types}RelationNodes" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TopologyMap", propOrder = {
    "ciNodes",
    "relationNodes"
})
public class TopologyMap {

    @XmlElement(name = "CINodes")
    protected CINodes ciNodes;
    protected RelationNodes relationNodes;

    /**
     * Gets the value of the ciNodes property.
     * 
     * @return
     *     possible object is
     *     {@link CINodes }
     *     
     */
    public CINodes getCINodes() {
        return ciNodes;
    }

    /**
     * Sets the value of the ciNodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link CINodes }
     *     
     */
    public void setCINodes(CINodes value) {
        this.ciNodes = value;
    }

    /**
     * Gets the value of the relationNodes property.
     * 
     * @return
     *     possible object is
     *     {@link RelationNodes }
     *     
     */
    public RelationNodes getRelationNodes() {
        return relationNodes;
    }

    /**
     * Sets the value of the relationNodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link RelationNodes }
     *     
     */
    public void setRelationNodes(RelationNodes value) {
        this.relationNodes = value;
    }

}
