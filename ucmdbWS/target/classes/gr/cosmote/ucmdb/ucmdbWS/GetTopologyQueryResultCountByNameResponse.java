
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getTopologyQueryResultCountByNameResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getTopologyQueryResultCountByNameResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="topologyCountMap" type="{http://schemas.hp.com/ucmdb/1/types/query}TopologyCountMap"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getTopologyQueryResultCountByNameResponse", namespace = "http://schemas.hp.com/ucmdb/1/params/query", propOrder = {
    "topologyCountMap"
})
public class GetTopologyQueryResultCountByNameResponse {

    @XmlElement(required = true)
    protected TopologyCountMap topologyCountMap;

    /**
     * Gets the value of the topologyCountMap property.
     * 
     * @return
     *     possible object is
     *     {@link TopologyCountMap }
     *     
     */
    public TopologyCountMap getTopologyCountMap() {
        return topologyCountMap;
    }

    /**
     * Sets the value of the topologyCountMap property.
     * 
     * @param value
     *     allowed object is
     *     {@link TopologyCountMap }
     *     
     */
    public void setTopologyCountMap(TopologyCountMap value) {
        this.topologyCountMap = value;
    }

}
