
package gr.cosmote.ucmdb.ucmdbWS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SimpleTypedPredefinedPropertyCollection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SimpleTypedPredefinedPropertyCollection"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SimpleTypedPredefinedProperty" type="{http://schemas.hp.com/ucmdb/1/types/props}SimpleTypedPredefinedProperty" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SimpleTypedPredefinedPropertyCollection", namespace = "http://schemas.hp.com/ucmdb/1/types/props", propOrder = {
    "simpleTypedPredefinedProperty"
})
public class SimpleTypedPredefinedPropertyCollection {

    @XmlElement(name = "SimpleTypedPredefinedProperty", required = true)
    protected List<SimpleTypedPredefinedProperty> simpleTypedPredefinedProperty;

    /**
     * Gets the value of the simpleTypedPredefinedProperty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the simpleTypedPredefinedProperty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSimpleTypedPredefinedProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleTypedPredefinedProperty }
     * 
     * 
     */
    public List<SimpleTypedPredefinedProperty> getSimpleTypedPredefinedProperty() {
        if (simpleTypedPredefinedProperty == null) {
            simpleTypedPredefinedProperty = new ArrayList<SimpleTypedPredefinedProperty>();
        }
        return this.simpleTypedPredefinedProperty;
    }

}
