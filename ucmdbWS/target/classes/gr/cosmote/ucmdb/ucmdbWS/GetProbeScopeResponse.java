
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getProbeScopeResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getProbeScopeResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="probeScope" type="{http://schemas.hp.com/ucmdb/discovery/1/types}Scope"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getProbeScopeResponse", namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", propOrder = {
    "probeScope"
})
public class GetProbeScopeResponse {

    @XmlElement(required = true)
    protected Scope probeScope;

    /**
     * Gets the value of the probeScope property.
     * 
     * @return
     *     possible object is
     *     {@link Scope }
     *     
     */
    public Scope getProbeScope() {
        return probeScope;
    }

    /**
     * Sets the value of the probeScope property.
     * 
     * @param value
     *     allowed object is
     *     {@link Scope }
     *     
     */
    public void setProbeScope(Scope value) {
        this.probeScope = value;
    }

}
