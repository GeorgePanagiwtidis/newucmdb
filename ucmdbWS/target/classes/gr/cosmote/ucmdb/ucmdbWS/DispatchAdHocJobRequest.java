
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for dispatchAdHocJobRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="dispatchAdHocJobRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="JobName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CIID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ProbeName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Timeout" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="CmdbContext" type="{http://schemas.hp.com/ucmdb/1/types}CmdbContext"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dispatchAdHocJobRequest", namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", propOrder = {
    "jobName",
    "ciid",
    "probeName",
    "timeout",
    "cmdbContext"
})
public class DispatchAdHocJobRequest {

    @XmlElement(name = "JobName", required = true)
    protected String jobName;
    @XmlElement(name = "CIID", required = true)
    protected String ciid;
    @XmlElement(name = "ProbeName", required = true)
    protected String probeName;
    @XmlElement(name = "Timeout")
    protected long timeout;
    @XmlElement(name = "CmdbContext", required = true)
    protected CmdbContext cmdbContext;

    /**
     * Gets the value of the jobName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobName() {
        return jobName;
    }

    /**
     * Sets the value of the jobName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobName(String value) {
        this.jobName = value;
    }

    /**
     * Gets the value of the ciid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCIID() {
        return ciid;
    }

    /**
     * Sets the value of the ciid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCIID(String value) {
        this.ciid = value;
    }

    /**
     * Gets the value of the probeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProbeName() {
        return probeName;
    }

    /**
     * Sets the value of the probeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProbeName(String value) {
        this.probeName = value;
    }

    /**
     * Gets the value of the timeout property.
     * 
     */
    public long getTimeout() {
        return timeout;
    }

    /**
     * Sets the value of the timeout property.
     * 
     */
    public void setTimeout(long value) {
        this.timeout = value;
    }

    /**
     * Gets the value of the cmdbContext property.
     * 
     * @return
     *     possible object is
     *     {@link CmdbContext }
     *     
     */
    public CmdbContext getCmdbContext() {
        return cmdbContext;
    }

    /**
     * Sets the value of the cmdbContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link CmdbContext }
     *     
     */
    public void setCmdbContext(CmdbContext value) {
        this.cmdbContext = value;
    }

}
