
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DateCondition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DateCondition"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://schemas.hp.com/ucmdb/1/types}Condition"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="condition" type="{http://schemas.hp.com/ucmdb/1/types}DateProp"/&gt;
 *         &lt;element name="dateOperator"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="Equal"/&gt;
 *               &lt;enumeration value="NotEqual"/&gt;
 *               &lt;enumeration value="Greater"/&gt;
 *               &lt;enumeration value="GreaterEqual"/&gt;
 *               &lt;enumeration value="Less"/&gt;
 *               &lt;enumeration value="LessEqual"/&gt;
 *               &lt;enumeration value="ChangedDuring"/&gt;
 *               &lt;enumeration value="UnchangedDuring"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DateCondition", propOrder = {
    "condition",
    "dateOperator"
})
public class DateCondition
    extends Condition
{

    @XmlElement(required = true)
    protected DateProp condition;
    @XmlElement(required = true)
    protected String dateOperator;

    /**
     * Gets the value of the condition property.
     * 
     * @return
     *     possible object is
     *     {@link DateProp }
     *     
     */
    public DateProp getCondition() {
        return condition;
    }

    /**
     * Sets the value of the condition property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateProp }
     *     
     */
    public void setCondition(DateProp value) {
        this.condition = value;
    }

    /**
     * Gets the value of the dateOperator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateOperator() {
        return dateOperator;
    }

    /**
     * Sets the value of the dateOperator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateOperator(String value) {
        this.dateOperator = value;
    }

}
