
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getCredentialsEntryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getCredentialsEntryResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="credentialsEntryParameters" type="{http://schemas.hp.com/ucmdb/1/types}CIProperties"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCredentialsEntryResponse", namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", propOrder = {
    "credentialsEntryParameters"
})
public class GetCredentialsEntryResponse {

    @XmlElement(required = true)
    protected CIProperties credentialsEntryParameters;

    /**
     * Gets the value of the credentialsEntryParameters property.
     * 
     * @return
     *     possible object is
     *     {@link CIProperties }
     *     
     */
    public CIProperties getCredentialsEntryParameters() {
        return credentialsEntryParameters;
    }

    /**
     * Sets the value of the credentialsEntryParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link CIProperties }
     *     
     */
    public void setCredentialsEntryParameters(CIProperties value) {
        this.credentialsEntryParameters = value;
    }

}
