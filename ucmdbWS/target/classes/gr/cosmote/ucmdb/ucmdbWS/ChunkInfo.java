
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChunkInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChunkInfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="numberOfChunks" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="chunksKey" type="{http://schemas.hp.com/ucmdb/1/types}ChunkKey"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChunkInfo", propOrder = {
    "numberOfChunks",
    "chunksKey"
})
public class ChunkInfo {

    protected int numberOfChunks;
    @XmlElement(required = true)
    protected ChunkKey chunksKey;

    /**
     * Gets the value of the numberOfChunks property.
     * 
     */
    public int getNumberOfChunks() {
        return numberOfChunks;
    }

    /**
     * Sets the value of the numberOfChunks property.
     * 
     */
    public void setNumberOfChunks(int value) {
        this.numberOfChunks = value;
    }

    /**
     * Gets the value of the chunksKey property.
     * 
     * @return
     *     possible object is
     *     {@link ChunkKey }
     *     
     */
    public ChunkKey getChunksKey() {
        return chunksKey;
    }

    /**
     * Sets the value of the chunksKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChunkKey }
     *     
     */
    public void setChunksKey(ChunkKey value) {
        this.chunksKey = value;
    }

}
