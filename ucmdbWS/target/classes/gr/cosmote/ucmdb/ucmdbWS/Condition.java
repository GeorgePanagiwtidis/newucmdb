
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Condition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Condition"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Condition")
@XmlSeeAlso({
    IntCondition.class,
    IntListCondition.class,
    BooleanCondition.class,
    StrCondition.class,
    StrListCondition.class,
    FloatCondition.class,
    LongCondition.class,
    DoubleCondition.class,
    DateCondition.class,
    XmlCondition.class,
    IntConditions.class,
    IntListConditions.class,
    BooleanConditions.class,
    StrConditions.class,
    StrListConditions.class,
    FloatConditions.class,
    LongConditions.class,
    DoubleConditions.class,
    DateConditions.class,
    XmlConditions.class
})
public abstract class Condition {


}
