
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Parameters complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Parameters"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dateParams" type="{http://schemas.hp.com/ucmdb/1/types}DateParams"/&gt;
 *         &lt;element name="doubleParams" type="{http://schemas.hp.com/ucmdb/1/types}DoubleParams"/&gt;
 *         &lt;element name="floatParams" type="{http://schemas.hp.com/ucmdb/1/types}FloatParams"/&gt;
 *         &lt;element name="intListParams" type="{http://schemas.hp.com/ucmdb/1/types}IntListParams"/&gt;
 *         &lt;element name="intParams" type="{http://schemas.hp.com/ucmdb/1/types}IntParams"/&gt;
 *         &lt;element name="strParams" type="{http://schemas.hp.com/ucmdb/1/types}StrParams"/&gt;
 *         &lt;element name="strListParams" type="{http://schemas.hp.com/ucmdb/1/types}StrListParams"/&gt;
 *         &lt;element name="longParams" type="{http://schemas.hp.com/ucmdb/1/types}LongParams"/&gt;
 *         &lt;element name="bytesParams" type="{http://schemas.hp.com/ucmdb/1/types}BytesParams"/&gt;
 *         &lt;element name="xmlParams" type="{http://schemas.hp.com/ucmdb/1/types}XmlParams"/&gt;
 *         &lt;element name="booleanParams" type="{http://schemas.hp.com/ucmdb/1/types}BooleanParams"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Parameters", propOrder = {
    "dateParams",
    "doubleParams",
    "floatParams",
    "intListParams",
    "intParams",
    "strParams",
    "strListParams",
    "longParams",
    "bytesParams",
    "xmlParams",
    "booleanParams"
})
public class Parameters {

    @XmlElement(required = true)
    protected DateParams dateParams;
    @XmlElement(required = true)
    protected DoubleParams doubleParams;
    @XmlElement(required = true)
    protected FloatParams floatParams;
    @XmlElement(required = true)
    protected IntListParams intListParams;
    @XmlElement(required = true)
    protected IntParams intParams;
    @XmlElement(required = true)
    protected StrParams strParams;
    @XmlElement(required = true)
    protected StrListParams strListParams;
    @XmlElement(required = true)
    protected LongParams longParams;
    @XmlElement(required = true)
    protected BytesParams bytesParams;
    @XmlElement(required = true)
    protected XmlParams xmlParams;
    @XmlElement(required = true)
    protected BooleanParams booleanParams;

    /**
     * Gets the value of the dateParams property.
     * 
     * @return
     *     possible object is
     *     {@link DateParams }
     *     
     */
    public DateParams getDateParams() {
        return dateParams;
    }

    /**
     * Sets the value of the dateParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateParams }
     *     
     */
    public void setDateParams(DateParams value) {
        this.dateParams = value;
    }

    /**
     * Gets the value of the doubleParams property.
     * 
     * @return
     *     possible object is
     *     {@link DoubleParams }
     *     
     */
    public DoubleParams getDoubleParams() {
        return doubleParams;
    }

    /**
     * Sets the value of the doubleParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link DoubleParams }
     *     
     */
    public void setDoubleParams(DoubleParams value) {
        this.doubleParams = value;
    }

    /**
     * Gets the value of the floatParams property.
     * 
     * @return
     *     possible object is
     *     {@link FloatParams }
     *     
     */
    public FloatParams getFloatParams() {
        return floatParams;
    }

    /**
     * Sets the value of the floatParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link FloatParams }
     *     
     */
    public void setFloatParams(FloatParams value) {
        this.floatParams = value;
    }

    /**
     * Gets the value of the intListParams property.
     * 
     * @return
     *     possible object is
     *     {@link IntListParams }
     *     
     */
    public IntListParams getIntListParams() {
        return intListParams;
    }

    /**
     * Sets the value of the intListParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntListParams }
     *     
     */
    public void setIntListParams(IntListParams value) {
        this.intListParams = value;
    }

    /**
     * Gets the value of the intParams property.
     * 
     * @return
     *     possible object is
     *     {@link IntParams }
     *     
     */
    public IntParams getIntParams() {
        return intParams;
    }

    /**
     * Sets the value of the intParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntParams }
     *     
     */
    public void setIntParams(IntParams value) {
        this.intParams = value;
    }

    /**
     * Gets the value of the strParams property.
     * 
     * @return
     *     possible object is
     *     {@link StrParams }
     *     
     */
    public StrParams getStrParams() {
        return strParams;
    }

    /**
     * Sets the value of the strParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrParams }
     *     
     */
    public void setStrParams(StrParams value) {
        this.strParams = value;
    }

    /**
     * Gets the value of the strListParams property.
     * 
     * @return
     *     possible object is
     *     {@link StrListParams }
     *     
     */
    public StrListParams getStrListParams() {
        return strListParams;
    }

    /**
     * Sets the value of the strListParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrListParams }
     *     
     */
    public void setStrListParams(StrListParams value) {
        this.strListParams = value;
    }

    /**
     * Gets the value of the longParams property.
     * 
     * @return
     *     possible object is
     *     {@link LongParams }
     *     
     */
    public LongParams getLongParams() {
        return longParams;
    }

    /**
     * Sets the value of the longParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link LongParams }
     *     
     */
    public void setLongParams(LongParams value) {
        this.longParams = value;
    }

    /**
     * Gets the value of the bytesParams property.
     * 
     * @return
     *     possible object is
     *     {@link BytesParams }
     *     
     */
    public BytesParams getBytesParams() {
        return bytesParams;
    }

    /**
     * Sets the value of the bytesParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link BytesParams }
     *     
     */
    public void setBytesParams(BytesParams value) {
        this.bytesParams = value;
    }

    /**
     * Gets the value of the xmlParams property.
     * 
     * @return
     *     possible object is
     *     {@link XmlParams }
     *     
     */
    public XmlParams getXmlParams() {
        return xmlParams;
    }

    /**
     * Sets the value of the xmlParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link XmlParams }
     *     
     */
    public void setXmlParams(XmlParams value) {
        this.xmlParams = value;
    }

    /**
     * Gets the value of the booleanParams property.
     * 
     * @return
     *     possible object is
     *     {@link BooleanParams }
     *     
     */
    public BooleanParams getBooleanParams() {
        return booleanParams;
    }

    /**
     * Sets the value of the booleanParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link BooleanParams }
     *     
     */
    public void setBooleanParams(BooleanParams value) {
        this.booleanParams = value;
    }

}
