
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getImpactPathResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getImpactPathResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://schemas.hp.com/ucmdb/1/types/impact}ImpactPathTopology"/&gt;
 *         &lt;element name="comments" type="{http://schemas.hp.com/ucmdb/1/types}ResComments"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getImpactPathResponse", namespace = "http://schemas.hp.com/ucmdb/1/params/impact", propOrder = {
    "impactPathTopology",
    "comments"
})
public class GetImpactPathResponse {

    @XmlElement(name = "ImpactPathTopology", namespace = "http://schemas.hp.com/ucmdb/1/types/impact", required = true)
    protected ImpactTopology impactPathTopology;
    @XmlElement(required = true)
    protected ResComments comments;

    /**
     * Gets the value of the impactPathTopology property.
     * 
     * @return
     *     possible object is
     *     {@link ImpactTopology }
     *     
     */
    public ImpactTopology getImpactPathTopology() {
        return impactPathTopology;
    }

    /**
     * Sets the value of the impactPathTopology property.
     * 
     * @param value
     *     allowed object is
     *     {@link ImpactTopology }
     *     
     */
    public void setImpactPathTopology(ImpactTopology value) {
        this.impactPathTopology = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link ResComments }
     *     
     */
    public ResComments getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResComments }
     *     
     */
    public void setComments(ResComments value) {
        this.comments = value;
    }

}
