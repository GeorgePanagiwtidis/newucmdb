
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ShallowTopology complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShallowTopology"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CIs" type="{http://schemas.hp.com/ucmdb/1/types}ShallowCIs"/&gt;
 *         &lt;element name="relations" type="{http://schemas.hp.com/ucmdb/1/types}ShallowRelations" minOccurs="0"/&gt;
 *         &lt;element name="compoundRelations" type="{http://schemas.hp.com/ucmdb/1/types}ShallowCompoundRelations" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShallowTopology", propOrder = {
    "cIs",
    "relations",
    "compoundRelations"
})
public class ShallowTopology {

    @XmlElement(name = "CIs", required = true)
    protected ShallowCIs cIs;
    protected ShallowRelations relations;
    protected ShallowCompoundRelations compoundRelations;

    /**
     * Gets the value of the cIs property.
     * 
     * @return
     *     possible object is
     *     {@link ShallowCIs }
     *     
     */
    public ShallowCIs getCIs() {
        return cIs;
    }

    /**
     * Sets the value of the cIs property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShallowCIs }
     *     
     */
    public void setCIs(ShallowCIs value) {
        this.cIs = value;
    }

    /**
     * Gets the value of the relations property.
     * 
     * @return
     *     possible object is
     *     {@link ShallowRelations }
     *     
     */
    public ShallowRelations getRelations() {
        return relations;
    }

    /**
     * Sets the value of the relations property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShallowRelations }
     *     
     */
    public void setRelations(ShallowRelations value) {
        this.relations = value;
    }

    /**
     * Gets the value of the compoundRelations property.
     * 
     * @return
     *     possible object is
     *     {@link ShallowCompoundRelations }
     *     
     */
    public ShallowCompoundRelations getCompoundRelations() {
        return compoundRelations;
    }

    /**
     * Sets the value of the compoundRelations property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShallowCompoundRelations }
     *     
     */
    public void setCompoundRelations(ShallowCompoundRelations value) {
        this.compoundRelations = value;
    }

}
