
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getProbeIPsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getProbeIPsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="probeIPs" type="{http://schemas.hp.com/ucmdb/discovery/1/types}IPList"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getProbeIPsResponse", namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", propOrder = {
    "probeIPs"
})
public class GetProbeIPsResponse {

    @XmlElement(required = true)
    protected IPList probeIPs;

    /**
     * Gets the value of the probeIPs property.
     * 
     * @return
     *     possible object is
     *     {@link IPList }
     *     
     */
    public IPList getProbeIPs() {
        return probeIPs;
    }

    /**
     * Sets the value of the probeIPs property.
     * 
     * @param value
     *     allowed object is
     *     {@link IPList }
     *     
     */
    public void setProbeIPs(IPList value) {
        this.probeIPs = value;
    }

}
