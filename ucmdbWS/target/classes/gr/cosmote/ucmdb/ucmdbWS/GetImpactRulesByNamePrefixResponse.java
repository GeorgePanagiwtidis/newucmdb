
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getImpactRulesByNamePrefixResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getImpactRulesByNamePrefixResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="impactRules" type="{http://schemas.hp.com/ucmdb/1/types/impact}ImpactRules" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getImpactRulesByNamePrefixResponse", namespace = "http://schemas.hp.com/ucmdb/1/params/impact", propOrder = {
    "impactRules"
})
public class GetImpactRulesByNamePrefixResponse {

    protected ImpactRules impactRules;

    /**
     * Gets the value of the impactRules property.
     * 
     * @return
     *     possible object is
     *     {@link ImpactRules }
     *     
     */
    public ImpactRules getImpactRules() {
        return impactRules;
    }

    /**
     * Sets the value of the impactRules property.
     * 
     * @param value
     *     allowed object is
     *     {@link ImpactRules }
     *     
     */
    public void setImpactRules(ImpactRules value) {
        this.impactRules = value;
    }

}
