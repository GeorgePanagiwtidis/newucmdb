
package gr.cosmote.ucmdb.ucmdbWS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CICountNodes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CICountNodes"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CICountNode" type="{http://schemas.hp.com/ucmdb/1/types/query}CICountNode" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CICountNodes", namespace = "http://schemas.hp.com/ucmdb/1/types/query", propOrder = {
    "ciCountNode"
})
public class CICountNodes {

    @XmlElement(name = "CICountNode")
    protected List<CICountNode> ciCountNode;

    /**
     * Gets the value of the ciCountNode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ciCountNode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCICountNode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CICountNode }
     * 
     * 
     */
    public List<CICountNode> getCICountNode() {
        if (ciCountNode == null) {
            ciCountNode = new ArrayList<CICountNode>();
        }
        return this.ciCountNode;
    }

}
