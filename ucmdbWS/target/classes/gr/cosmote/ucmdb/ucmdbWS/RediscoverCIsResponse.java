
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rediscoverCIsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rediscoverCIsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="isSucceed" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rediscoverCIsResponse", namespace = "http://schemas.hp.com/ucmdb/discovery/1/params", propOrder = {
    "isSucceed"
})
public class RediscoverCIsResponse {

    protected boolean isSucceed;

    /**
     * Gets the value of the isSucceed property.
     * 
     */
    public boolean isIsSucceed() {
        return isSucceed;
    }

    /**
     * Sets the value of the isSucceed property.
     * 
     */
    public void setIsSucceed(boolean value) {
        this.isSucceed = value;
    }

}
