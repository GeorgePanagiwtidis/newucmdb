
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getRelationsByIdResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getRelationsByIdResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Relations" type="{http://schemas.hp.com/ucmdb/1/types}Relations"/&gt;
 *         &lt;element name="chunkInfo" type="{http://schemas.hp.com/ucmdb/1/types}ChunkInfo"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getRelationsByIdResponse", namespace = "http://schemas.hp.com/ucmdb/1/params/query", propOrder = {
    "relations",
    "chunkInfo"
})
public class GetRelationsByIdResponse {

    @XmlElement(name = "Relations", required = true)
    protected Relations relations;
    @XmlElement(required = true)
    protected ChunkInfo chunkInfo;

    /**
     * Gets the value of the relations property.
     * 
     * @return
     *     possible object is
     *     {@link Relations }
     *     
     */
    public Relations getRelations() {
        return relations;
    }

    /**
     * Sets the value of the relations property.
     * 
     * @param value
     *     allowed object is
     *     {@link Relations }
     *     
     */
    public void setRelations(Relations value) {
        this.relations = value;
    }

    /**
     * Gets the value of the chunkInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ChunkInfo }
     *     
     */
    public ChunkInfo getChunkInfo() {
        return chunkInfo;
    }

    /**
     * Sets the value of the chunkInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChunkInfo }
     *     
     */
    public void setChunkInfo(ChunkInfo value) {
        this.chunkInfo = value;
    }

}
