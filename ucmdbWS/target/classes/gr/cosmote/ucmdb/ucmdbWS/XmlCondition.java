
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for XmlCondition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XmlCondition"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://schemas.hp.com/ucmdb/1/types}Condition"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="condition" type="{http://schemas.hp.com/ucmdb/1/types}XmlProp"/&gt;
 *         &lt;element name="xmlOperator"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="IsNull"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XmlCondition", propOrder = {
    "condition",
    "xmlOperator"
})
public class XmlCondition
    extends Condition
{

    @XmlElement(required = true)
    protected XmlProp condition;
    @XmlElement(required = true)
    protected String xmlOperator;

    /**
     * Gets the value of the condition property.
     * 
     * @return
     *     possible object is
     *     {@link XmlProp }
     *     
     */
    public XmlProp getCondition() {
        return condition;
    }

    /**
     * Sets the value of the condition property.
     * 
     * @param value
     *     allowed object is
     *     {@link XmlProp }
     *     
     */
    public void setCondition(XmlProp value) {
        this.condition = value;
    }

    /**
     * Gets the value of the xmlOperator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXmlOperator() {
        return xmlOperator;
    }

    /**
     * Sets the value of the xmlOperator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXmlOperator(String value) {
        this.xmlOperator = value;
    }

}
