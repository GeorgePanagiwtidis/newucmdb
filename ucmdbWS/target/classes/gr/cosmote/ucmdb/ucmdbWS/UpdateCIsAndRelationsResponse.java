
package gr.cosmote.ucmdb.ucmdbWS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateCIsAndRelationsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateCIsAndRelationsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://schemas.hp.com/ucmdb/1/params/update}CreatedIDsMap" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateCIsAndRelationsResponse", namespace = "http://schemas.hp.com/ucmdb/1/params/update", propOrder = {
    "createdIDsMap"
})
public class UpdateCIsAndRelationsResponse {

    @XmlElement(name = "CreatedIDsMap")
    protected List<ClientIDToCmdbID> createdIDsMap;

    /**
     * Gets the value of the createdIDsMap property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the createdIDsMap property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreatedIDsMap().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClientIDToCmdbID }
     * 
     * 
     */
    public List<ClientIDToCmdbID> getCreatedIDsMap() {
        if (createdIDsMap == null) {
            createdIDsMap = new ArrayList<ClientIDToCmdbID>();
        }
        return this.createdIDsMap;
    }

}
