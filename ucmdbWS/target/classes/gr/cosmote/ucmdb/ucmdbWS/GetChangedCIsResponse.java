
package gr.cosmote.ucmdb.ucmdbWS;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getChangedCIsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getChangedCIsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="changeDataInfo" type="{http://schemas.hp.com/ucmdb/1/types/history}ChangedDataInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getChangedCIsResponse", namespace = "http://schemas.hp.com/ucmdb/1/params/history", propOrder = {
    "changeDataInfo"
})
public class GetChangedCIsResponse {

    protected List<ChangedDataInfo> changeDataInfo;

    /**
     * Gets the value of the changeDataInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the changeDataInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChangeDataInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChangedDataInfo }
     * 
     * 
     */
    public List<ChangedDataInfo> getChangeDataInfo() {
        if (changeDataInfo == null) {
            changeDataInfo = new ArrayList<ChangedDataInfo>();
        }
        return this.changeDataInfo;
    }

}
