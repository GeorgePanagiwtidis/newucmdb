
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PredefinedProperties complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PredefinedProperties"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="qualifierProperties" type="{http://schemas.hp.com/ucmdb/1/types/props}QualifierProperties" minOccurs="0"/&gt;
 *         &lt;element name="simplePredefinedProperties" type="{http://schemas.hp.com/ucmdb/1/types/props}SimplePredefinedPropertyCollection" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PredefinedProperties", namespace = "http://schemas.hp.com/ucmdb/1/types/props", propOrder = {
    "qualifierProperties",
    "simplePredefinedProperties"
})
public class PredefinedProperties {

    protected QualifierProperties qualifierProperties;
    protected SimplePredefinedPropertyCollection simplePredefinedProperties;

    /**
     * Gets the value of the qualifierProperties property.
     * 
     * @return
     *     possible object is
     *     {@link QualifierProperties }
     *     
     */
    public QualifierProperties getQualifierProperties() {
        return qualifierProperties;
    }

    /**
     * Sets the value of the qualifierProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link QualifierProperties }
     *     
     */
    public void setQualifierProperties(QualifierProperties value) {
        this.qualifierProperties = value;
    }

    /**
     * Gets the value of the simplePredefinedProperties property.
     * 
     * @return
     *     possible object is
     *     {@link SimplePredefinedPropertyCollection }
     *     
     */
    public SimplePredefinedPropertyCollection getSimplePredefinedProperties() {
        return simplePredefinedProperties;
    }

    /**
     * Sets the value of the simplePredefinedProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimplePredefinedPropertyCollection }
     *     
     */
    public void setSimplePredefinedProperties(SimplePredefinedPropertyCollection value) {
        this.simplePredefinedProperties = value;
    }

}
