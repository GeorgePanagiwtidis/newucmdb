
package gr.cosmote.ucmdb.ucmdbWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ImpactTopology complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ImpactTopology"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CIs" type="{http://schemas.hp.com/ucmdb/1/types}CIs"/&gt;
 *         &lt;element name="impactRelations" type="{http://schemas.hp.com/ucmdb/1/types/impact}ImpactRelations"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ImpactTopology", namespace = "http://schemas.hp.com/ucmdb/1/types/impact", propOrder = {
    "cIs",
    "impactRelations"
})
public class ImpactTopology {

    @XmlElement(name = "CIs", required = true)
    protected CIs cIs;
    @XmlElement(required = true)
    protected ImpactRelations impactRelations;

    /**
     * Gets the value of the cIs property.
     * 
     * @return
     *     possible object is
     *     {@link CIs }
     *     
     */
    public CIs getCIs() {
        return cIs;
    }

    /**
     * Sets the value of the cIs property.
     * 
     * @param value
     *     allowed object is
     *     {@link CIs }
     *     
     */
    public void setCIs(CIs value) {
        this.cIs = value;
    }

    /**
     * Gets the value of the impactRelations property.
     * 
     * @return
     *     possible object is
     *     {@link ImpactRelations }
     *     
     */
    public ImpactRelations getImpactRelations() {
        return impactRelations;
    }

    /**
     * Sets the value of the impactRelations property.
     * 
     * @param value
     *     allowed object is
     *     {@link ImpactRelations }
     *     
     */
    public void setImpactRelations(ImpactRelations value) {
        this.impactRelations = value;
    }

}
