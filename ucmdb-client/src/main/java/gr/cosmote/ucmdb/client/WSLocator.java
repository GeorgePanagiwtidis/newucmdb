package gr.cosmote.ucmdb.client;

import gr.cosmote.ucmdb.commons.properties.ApplicationProperties;
import gr.cosmote.ucmdb.ucmdbWS.UcmdbService;
import lombok.extern.slf4j.Slf4j;

import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class WSLocator
{
    private static final String PROPERTY_WSDL_UCMDB = "WSDL_UCMDB";

    public static UcmdbService getUCMDBService() {
        URL url = null;
        try {
            url = WSLocator.class.getClassLoader().getResource(ApplicationProperties.getInstance().getProperty(PROPERTY_WSDL_UCMDB));
//            url = new URL(ApplicationProperties.getInstance().getProperty(PROPERTY_WSDL_UCMDB));
        } catch (Exception e) {
            log.error("WSLocator :: getUCMDBService :: Error trying to initialize WebService Client");
        }

        HandlerResolver resolver = new HandlerResolver(){
            @Override
            public List<Handler> getHandlerChain(PortInfo portInfo){
                List<Handler> handlers = new ArrayList<Handler>();
                handlers.add(new SOAPMessageHandler());
                return handlers;
            }
        };
        UcmdbService service = new UcmdbService(url);
        service.setHandlerResolver(resolver);
        return service ;
    }
}
