package gr.cosmote.ucmdb.client;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.*;
import java.util.stream.Collectors;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import gr.cosmote.ucmdb.commons.exceptions.ApplicationException;
import gr.cosmote.ucmdb.commons.properties.ApplicationProperties;
import gr.cosmote.ucmdb.model.TopologyMapResponse;
import gr.cosmote.ucmdb.models.base.GenericCI;
import gr.cosmote.ucmdb.ucmdbWS.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UCMDBCallHelper {

    private static final String NEW_UCMDB = ApplicationProperties.getInstance().getProperty("NEW_UCMDB");
    private static final String USERNAME = ApplicationProperties.getInstance().getProperty("UCMDB_API_USER");
    private static final String PASSWORD = ApplicationProperties.getInstance().getProperty("UCMDB_API_PASSWORD");
    private static String UCMDB_ENDPOINT = ApplicationProperties.getInstance().getProperty("GR.COSMOTE.UCMDB.UCMDBSERVICE.ENDPOINT");
    private static final String PULL_SINGLE_PAGE_CHUNK_ENABLED = ApplicationProperties.getInstance().getProperty("SCHEDULER_RETRIEVE_SINGLE_CHUNK_NODES");

    private static final CmdbContext ctx = new CmdbContext();
    private static final UcmdbService service = WSLocator.getUCMDBService();

    private static UcmdbServicePortType servicePort = service.getUcmdbServiceSOAP12PortHttp();

    private static void init() {
        boolean isNewUcmdbEnv = Boolean.valueOf(NEW_UCMDB);
        if (isNewUcmdbEnv) {
            UCMDB_ENDPOINT = ApplicationProperties.getInstance().getProperty("GR.COSMOTE.UCMDB.UCMDBSERVICE.ENDPOINT_NEW");
        }

        BindingProvider provider = (BindingProvider) servicePort;
        provider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, UCMDB_ENDPOINT);

        ctx.setCallerApplication("uCMDBClient");

        Authenticator myAuth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(USERNAME, PASSWORD.toCharArray());
            }
        };

        Authenticator.setDefault(myAuth);
    }

    public static Holder<UcmdbClassModelHierarchy> getAllClassesHierarchy() {
        log.debug("UCMDBCallHelper :: getAllClassesHierarchy :: method entered");

        init();

        Holder<UcmdbClassModelHierarchy> holder = new Holder<UcmdbClassModelHierarchy>();
        try {
            servicePort.getAllClassesHierarchy(ctx, holder, null);
        } catch (Exception e) {
            log.error("UCMDBCallHelper :: getAllClassesHierarchy :: error in method", e);
        }

        log.debug("UCMDBCallHelper :: getAllClassesHierarchy :: method returning {}", holder);
        return holder;
    }

    public static Holder<UcmdbClassModelHierarchy> getClassAncestors(String type) {
        log.debug("UCMDBCallHelper :: getClassAncestors :: method entered with " +
                "type: {} ", type);

        init();

        Holder<UcmdbClassModelHierarchy> holder = new Holder<UcmdbClassModelHierarchy>();
        Holder<ResComments> holder1 = new Holder<ResComments>();
        try {
            servicePort.getClassAncestors(ctx, type, holder, holder1);
        } catch (Exception e) {
            log.error("UCMDBCallHelper :: getClassAncestors :: error in method", e);
        }

        return holder;
    }

    public static Holder<CIs> getCIsByType(String type, CustomProperties customProperties, TypedPropertiesCollection typedPropertiesCollection) {
        log.debug("UCMDBCallHelper :: getCIsByType :: method entered with " +
                "type: {} and customProperties: {}", type, customProperties);

        init();

        Holder<CIs> holder = new Holder<CIs>();
        Holder<ChunkInfo> chunkHolder = new Holder<>();

        try {
            servicePort.getCIsByType(ctx, type, customProperties, holder, chunkHolder);

            if (holder.value.getCI().size() > 0) {
                return holder;
            } else if (chunkHolder.value.getNumberOfChunks() > 0) {
                getPropertiesFromChunks(chunkHolder, holder, typedPropertiesCollection);
                return holder;
            }

        } catch (Exception e) {
            log.error("UCMDBCallHelper :: getCIsByType :: error in method", e);
        }

        log.debug("UCMDBCallHelper :: getCIsByType :: method returning {}", holder);
        return null;
    }

    public static Holder<CIs> getCIsById(TypedPropertiesCollection typedPropertiesCollection, IDs ids) {
        log.debug("UCMDBCallHelper :: getCIsById :: method entered with " +
                "typedPropertiesCollection: {} and ids: {}", typedPropertiesCollection, ids);

        init();

        Holder<CIs> holder = new Holder<CIs>();

        try {
            servicePort.getCIsById(ctx, typedPropertiesCollection, ids, holder, null);


        } catch (Exception e) {
            log.error("UCMDBCallHelper :: getCIsById :: error in method", e);
        }

        log.debug("UCMDBCallHelper :: getCIsById :: method returning {}", holder);
        return holder;
    }

    public static Holder<Topology> getCINeighbours(ID id, String neighbourType, TypedPropertiesCollection typedPropertiesCollection) {
        //log.debug("UCMDBCallHelper :: getCINeighbours :: method entered with id: {}, neighbourType: {} and typedPropertiesCollectionof size: {}", id.getValue(), neighbourType, typedPropertiesCollection.getTypedProperties().size());

        init();

        Holder<Topology> holder = new Holder<Topology>();

        try {
            servicePort.getCINeighbours(ctx, id, neighbourType, typedPropertiesCollection, null, holder, null);
        } catch (Exception e) {
            log.error("UCMDBCallHelper :: getCINeighbours :: error in method", e);
        }

        log.debug("UCMDBCallHelper :: getCINeighbours :: method returning {}", holder);
        return holder;
    }

    public static TopologyMapResponse getTopologyMap(String queryName, TypedPropertiesCollection typedPropertiesCollection) {
        log.debug("UCMDBCallHelper.getTopologyMap() ::: Method entered with queryName {}, typedPropertiesCollection {}", queryName, typedPropertiesCollection);

        init();

        Holder<TopologyMap> holder = new Holder<>();
        Holder<ChunkInfo> chunkHolder = new Holder<>();

        try {
            servicePort.executeTopologyQueryByName(ctx, queryName, typedPropertiesCollection, holder, chunkHolder);
        } catch (Exception e) {
            log.error("UCMDBCallHelper.getTopologyMap() ::: Error in method", e);
        }
        if (holder.value == null)
            throw new gr.cosmote.ucmdb.commons.exceptions.ApplicationException(String.format("UcmdbServicePortType.executeTopologyQueryByName :: Error holder or holder value for query name %s is null. Check uCMDB connectivity", queryName));

        log.debug("UCMDBCallHelper.getTopologyMap() ::: method returning {}", holder);

        TopologyMapResponse response = new TopologyMapResponse();
        response.setChunkHolder(chunkHolder);
        response.setHolder(holder);

        return response;
    }

    public static List<GenericCI> executeTopologyQueryByName(String queryName, TypedPropertiesCollection typedPropertiesCollection) {
        log.debug("UCMDBCallHelper :: executeTopologyQueryByName :: method entered with " +
                "queryName {}, typedPropertiesCollection {}", queryName, typedPropertiesCollection);

        init();

        Holder<TopologyMap> holder = new Holder<>();
        Holder<ChunkInfo> chunkHolder = new Holder<>();

        try {
            servicePort.executeTopologyQueryByName(ctx, queryName, typedPropertiesCollection, holder, chunkHolder);
        } catch (Exception e) {
            log.error("UCMDBCallHelper :: executeTopologyQueryByName :: error in method", e);
        }
        if (holder.value == null)
            throw new gr.cosmote.ucmdb.commons.exceptions.ApplicationException(String.format("UcmdbServicePortType.executeTopologyQueryByName :: Error holder or holder value for query name %s is null. Check uCMDB connectivity", queryName));

        log.debug("UCMDBCallHelper :: executeTopologyQueryByName :: method returning {}", holder);

        TopologyMap validTopologyMap = getValidTopologyMap(holder.value, chunkHolder, typedPropertiesCollection, 1);
        List<GenericCI> genericCIList = transformCINodeToGenericCI(validTopologyMap.getCINodes());

        return genericCIList;
    }

    public static List<GenericCI> executeTopologyQueryByNameWithParams(String queryName, List<ParameterizedNode> parameterizedNodes, TypedPropertiesCollection typedPropertiesCollection) throws ApplicationException {
        log.debug("UCMDBCallHelper.executeTopologyQueryByNameWithParams :: Method Entered for Query {}", queryName);
        List<GenericCI> serviceResult = new ArrayList<>();

        Holder<TopologyMap> topologyMapHolder = new Holder<>();
        Holder<ChunkInfo> chunkInfoHolder = new Holder<>();
        try {
            servicePort.executeTopologyQueryByNameWithParameters(ctx, queryName, parameterizedNodes, typedPropertiesCollection, topologyMapHolder, chunkInfoHolder);

            TopologyMap topologyMap = topologyMapHolder.value;
            ChunkInfo chunkInfo = chunkInfoHolder.value;
            if (isResultChuncked(topologyMap, chunkInfo)) {
                int numOfChunks = chunkInfo.getNumberOfChunks();

                //Retrieve Chunks
                ChunkRequest chunkRequest = new ChunkRequest();
                chunkRequest.setChunkInfo(chunkInfo);

                for (int chunkNumber = 1; chunkNumber <= numOfChunks; chunkNumber++) {
                    chunkRequest.setChunkNumber(chunkNumber);
                    topologyMap = getTopologyMapForChunkRequest(chunkRequest, typedPropertiesCollection);
                    serviceResult.addAll(transformCINodeToGenericCI(topologyMap.getCINodes()));
                }
            } else {
                serviceResult = transformCINodeToGenericCI(topologyMap.getCINodes());
            }
        } catch (Exception ex) {
            throw new ApplicationException("UCMDBCallHelper.executeTopologyQueryByNameWithParams :: Error", ex);
        }

        log.debug("UCMDBCallHelper.executeTopologyQueryByNameWithParams :: Method Returned Result size of {}", serviceResult.size());
        return serviceResult;
    }

    public static List<GenericCI> getNodeAsGenericCIByChunk(String queryName, TypedPropertiesCollection typedPropertiesCollection, TopologyMapResponse topologyMapResponse, int chunkNumber) {
        log.debug("UCMDBCallHelper.getNodeAsGenericCIByChunk() ::: method entered with queryName {}, typedPropertiesCollection {}", queryName, typedPropertiesCollection);
        init();

        TopologyMap valiTopologyMap = getValidTopologyMap(topologyMapResponse.getHolder().value, topologyMapResponse.getChunkHolder(), typedPropertiesCollection, chunkNumber);

        log.debug("UCMDBCallHelper.getNodeAsGenericCIByChunk() ::: method returning ");
        return transformCINodeToGenericCI(valiTopologyMap.getCINodes());
    }

    private static TopologyMap getValidTopologyMap(TopologyMap topologyMap, Holder<ChunkInfo> chunkHolder, TypedPropertiesCollection typedPropertiesCollection, int chunkNumber) {
        if (isResultChuncked(topologyMap, chunkHolder.value)) {
            //Retrieve Chunks
            ChunkRequest chunkRequest = new ChunkRequest();
            ChunkInfo chunkInfo = new ChunkInfo();

            chunkInfo.setNumberOfChunks(chunkHolder.value.getNumberOfChunks());
            chunkInfo.setChunksKey(chunkHolder.value.getChunksKey());

            chunkRequest.setChunkInfo(chunkInfo);
            chunkRequest.setChunkNumber(chunkNumber);

            topologyMap = getTopologyMapForChunkRequest(chunkRequest, typedPropertiesCollection);
        }
        return topologyMap;
    }

    private static List<GenericCI> transformCINodeToGenericCI(CINodes ciNodes) {
        List<GenericCI> genericCIList = new ArrayList<>();
        List<CI> cis = new ArrayList<>();

        for (CINode node : ciNodes.getCINode()) {
            cis.addAll(node.getCIs().getCI());
        }

        for (CI item : cis) {
            Map<String, String> itemProperties = getProperties(item.getID().getValue(), cis);
            final GenericCI result = new GenericCI();
            result.setId(item.getID().getValue());
            result.setType(item.getType());
            result.setProperties(itemProperties);

            genericCIList.add(result);
        }
        return genericCIList;
    }

    public static Map<String, String> getProperties(String id, List<CI> cis) {

        for (CI item : cis) {

            if (id.equals(item.getID().getValue())) {
                Map<String, String> tempProperties = new TreeMap<>();

                if (item.getProps().getStrProps() != null) {
                    for (StrProp property : item.getProps().getStrProps().getStrProp()) {
                        tempProperties.put(property.getName(), property.getValue().getValue());
                    }
                }

                if (item.getProps().getStrListProps() != null) {
                    List<StrListProp> strListProps = item.getProps().getStrListProps().getStrListProp();

                    for (StrListProp prop : strListProps) {
                        String name = prop.getName();

                        StringBuffer stringBuffer = new StringBuffer();

                        List<String> strValues = prop.getStrValues().getValue().getStrValue();

                        for (String val : strValues) {
                            stringBuffer.append(val + ",");
                        }

                        stringBuffer.deleteCharAt(stringBuffer.length() - 1);

                        tempProperties.put(prop.getName(), stringBuffer.toString());
                    }
                }

                if (item.getProps().getDateProps() != null) {
                    for (DateProp property1 : item.getProps().getDateProps().getDateProp()) {
                        tempProperties.put(property1.getName(), property1.getValue().getValue().toString());
                    }
                }

                if (item.getProps().getLongProps() != null) {
                    for (LongProp property : item.getProps().getLongProps().getLongProp()) {
                        tempProperties.put(property.getName(), property.getValue().getValue().toString());
                    }
                }

                if (item.getProps().getBooleanProps() != null) {
                    for (BooleanProp property : item.getProps().getBooleanProps().getBooleanProp()) {
                        tempProperties.put(property.getName(), property.getValue().getValue().toString());
                    }
                }

                if (item.getProps().getIntProps() != null) {
                    for (IntProp property : item.getProps().getIntProps().getIntProp()) {
                        tempProperties.put(property.getName(), property.getValue().getValue().toString());
                    }
                }

                if (item.getProps().getDoubleProps() != null) {
                    for (DoubleProp property : item.getProps().getDoubleProps().getDoubleProp()) {
                        tempProperties.put(property.getName(), property.getValue().getValue().toString());
                    }
                }

                return tempProperties;

            }
        }
        return null;
    }

    public static boolean updateCIsAndRelations(String username, CIsAndRelationsUpdates updatedCI) {
        log.debug("UCMDBCallHelper.updateCIsAndRelations :: Entered method for Updating CI with Global ID: {}", updatedCI.getCIsForUpdate().getCI().get(0).getID());

        init();

        boolean updateSuccess;
        try {
            log.debug("UCMDBCallHelper.updateCIsAndRelations :: SOAP Service Call with input {}", updatedCI);
            servicePort.updateCIsAndRelations(ctx, username, updatedCI, false);
            updateSuccess = true;
        } catch (Exception e) {
            throw new ApplicationException("UCMDBCallHelper :: updateCIsAndRelations() ::: Error failed to update uCMDB", e);
        }

        log.debug("UCMDBCallHelper.updateCIsAndRelations :: Method Successfully updated uCMDB");

        return updateSuccess;
    }

    private static TopologyMap getTopologyMapForChunkRequest(ChunkRequest chunkRequest, TypedPropertiesCollection typedPropertiesCollection) {
        Holder<TopologyMap> holder = pullTopologyMapChunks(chunkRequest, typedPropertiesCollection);
        return transformTopology(holder.value);
    }

    private static TopologyMap transformTopology(TopologyMap map) {
        TopologyMap topologyMap = new TopologyMap();

        CINodes ciNodes = new CINodes();
        RelationNodes relationNodes = new RelationNodes();

        ciNodes.getCINode().addAll(map.getCINodes().getCINode());
        relationNodes.getRelationNode().addAll(map.getRelationNodes().getRelationNode());

        topologyMap.setCINodes(ciNodes);
        topologyMap.setRelationNodes(relationNodes);

        return topologyMap;

    }

    private static Holder<TopologyMap> pullTopologyMapChunks(ChunkRequest chunkRequest, TypedPropertiesCollection typedPropertiesCollection) {
        log.debug("UCMDBCallHelper :: pullTopologyMapChunks :: method entered with " +
                "chunkRequest {}, typedPropertiesCollection {}", chunkRequest, typedPropertiesCollection);

        init();

        Holder<TopologyMap> holder = new Holder<>();

        try {
            servicePort.pullTopologyMapChunks(ctx, chunkRequest, typedPropertiesCollection, holder, null);
        } catch (Exception e) {
            log.error("UCMDBCallHelper :: pullTopologyMapChunks :: error in method", e);
        }

        log.debug("UCMDBCallHelper :: pullTopologyMapChunks :: method returning {}", holder);
        return holder;
    }

    public static Holder<TopologyMap> retrieveResultsForAllChunks(Holder<TopologyMap> holder, Holder<ChunkInfo> chunkHolder, TypedPropertiesCollection typedPropertiesCollection) {

        if (holder.value.getCINodes().getCINode().size() > 0) {
            return holder;
        } else if (chunkHolder.value.getNumberOfChunks() > 0) {
            Holder<TopologyMap> map = new Holder<TopologyMap>();
            map.value = new TopologyMap();

            CINodes ciNodes = new CINodes();
            RelationNodes relationNodes = new RelationNodes();

            int numberOfChunksAvailable = chunkHolder.value.getNumberOfChunks();
            for (int i = 1; i <= numberOfChunksAvailable; i++) {
                ChunkRequest chunkRequest = new ChunkRequest();
                ChunkInfo chunkInfo = new ChunkInfo();

                chunkInfo.setNumberOfChunks(chunkHolder.value.getNumberOfChunks());
                chunkInfo.setChunksKey(chunkHolder.value.getChunksKey());

                chunkRequest.setChunkInfo(chunkInfo);
                chunkRequest.setChunkNumber(i);

                Holder<TopologyMap> tempMap = pullTopologyMapChunks(chunkRequest, typedPropertiesCollection);
                ciNodes.getCINode().addAll(tempMap.value.getCINodes().getCINode());
                relationNodes.getRelationNode().addAll(tempMap.value.getRelationNodes().getRelationNode());
            }
            map.value.setCINodes(ciNodes);
            map.value.setRelationNodes(relationNodes);

            return map;
        }
        return null;
    }

    private static void getPropertiesFromChunks(Holder<ChunkInfo> chunkHolder, Holder<CIs> holder, TypedPropertiesCollection typedPropertiesCollection) {

        for (int i = 1; i <= chunkHolder.value.getNumberOfChunks(); i++) {

            ChunkRequest chunkRequest = new ChunkRequest();
            ChunkInfo chunkInfo = new ChunkInfo();

            chunkInfo.setNumberOfChunks(chunkHolder.value.getNumberOfChunks());
            chunkInfo.setChunksKey(chunkHolder.value.getChunksKey());

            chunkRequest.setChunkInfo(chunkInfo);
            chunkRequest.setChunkNumber(i);

            Holder<TopologyMap> map = pullTopologyMapChunks(chunkRequest, typedPropertiesCollection);
            holder.value.getCI().addAll(map.value.getCINodes().getCINode().get(0).getCIs().getCI());

        }

    }

    public static Holder<CIs> getFilteredCIsByType(String type, String logicalOperator, CustomProperties customProperties, Conditions conditions) {
        log.debug("UCMDBCallHelper :: getFilteredCIsByType :: method entered with " +
                "type {}, Conditions {}", type, conditions);

        init();

        Holder<CIs> holder = new Holder<>();
        Holder<ChunkInfo> chunkHolder = new Holder<>();

        try {
            servicePort.getFilteredCIsByType(ctx, type, customProperties, conditions, logicalOperator, holder, chunkHolder);

            if (holder.value.getCI().size() > 0) {
                return holder;
            } else if (chunkHolder.value.getNumberOfChunks() > 0) {
                getPropertiesFromChunks(chunkHolder, holder, initTypedPropertiesCollectionObjectByCustomProperties(type, customProperties));
                return holder;
            }
        } catch (Exception e) {
            log.error("UCMDBCallHelper :: pullTopologyMapChunks :: error in method", e);
        }

        log.debug("UCMDBCallHelper :: pullTopologyMapChunks :: method returning {}", holder);
        return holder;
    }

    private static final TypedPropertiesCollection initTypedPropertiesCollectionObjectByCustomProperties(String type, CustomProperties properties) {
        TypedPropertiesCollection typedPropertiesCollection = new TypedPropertiesCollection();
        TypedProperties typedProperties = new TypedProperties();
        typedProperties.setType(type);

        CustomTypedProperties customTypedProperties = new CustomTypedProperties();
        customTypedProperties.setPropertiesList(properties.getPropertiesList());

        typedProperties.setProperties(customTypedProperties);
        typedPropertiesCollection.getTypedProperties().add(typedProperties);

        return typedPropertiesCollection;
    }

    private static boolean isResultChuncked(TopologyMap topologyMap, ChunkInfo chunkInfo) {
        return !(topologyMap.getCINodes().getCINode().size() > 0) && chunkInfo.getNumberOfChunks() > 0;
    }
}
