package gr.cosmote.ucmdb.client;

import lombok.extern.slf4j.Slf4j;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Set;

/**
 * Created by Intrasoft Intl - dasimakopoulos on 26/8/2017.
 */
@Slf4j
public class SOAPMessageHandler implements SOAPHandler<SOAPMessageContext> {
    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        try {
            if ((Boolean) context.get(SOAPMessageContext.MESSAGE_OUTBOUND_PROPERTY)) {
                SOAPMessage message = context.getMessage();
//                SOAPEnvelope envelope = message.getSOAPPart().getEnvelope();
//                SOAPHeader header = envelope.getHeader();
                if (null != message) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    message.writeTo(baos);
                    log.debug(baos.toString(java.nio.charset.StandardCharsets.UTF_8.name()));
                }
                return true;
            }
        } catch (Exception e) {

        }
        return false;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        SOAPMessageContext messageContext = (SOAPMessageContext) context;
        log.debug(messageContext.getMessage().toString());
        return true;
    }

    @Override
    public void close(MessageContext context) {
        //do nothing
    }

    @Override
    public Set<QName> getHeaders() {
        return null;
    }
}
