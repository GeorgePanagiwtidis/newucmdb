package gr.cosmote.ucmdb.model;

import gr.cosmote.ucmdb.ucmdbWS.ChunkInfo;
import gr.cosmote.ucmdb.ucmdbWS.TopologyMap;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.xml.ws.Holder;

@Slf4j
@Data
public class TopologyMapResponse {
    Holder<TopologyMap> holder;
    Holder<ChunkInfo> chunkHolder;
}
