package gr.cosmote.ucmdb.web.filter;

import org.apache.commons.lang.StringUtils;
import org.glassfish.jersey.message.internal.ReaderWriter;
import org.glassfish.jersey.server.ContainerRequest;
import org.glassfish.jersey.server.ContainerResponse;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

import gr.cosmote.ucmdb.web.entity.BaseEntity;
import lombok.extern.slf4j.Slf4j;

@Provider
@Slf4j
public class UcmdbLoggingFilter //extends LoggingFilter implements ContainerRequestFilter, ContainerResponseFilter {
{
    private static final String LOGGER_MESSAGE =
            "OssLoggingFilter.log() ::: \n" +
                    "Request  User    : {}\n" +
                    "Request  Path    : {}\n" +
                    "Request  Headers : {}\n" +
                    "Request  Entity  : {}\n" +
                    "Response Entity  : {}";

    public ContainerRequest filter(final ContainerRequest containerRequest) {
        return containerRequest;
    }

    private String getEntityBody(final ContainerRequest containerRequest) {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final InputStream in  = containerRequest.getEntityStream();

        final StringBuilder b = new StringBuilder();

        try {
            ReaderWriter.writeTo(in, out);

            byte[] requestEntity = out.toByteArray();

            if (requestEntity.length == 0) {
                b.append(StringUtils.EMPTY);
            }
            else {
                b.append(new String(requestEntity));
            }

        } catch (IOException ex) {
            log.error("OssLoggingFilter.filter error in method",ex);
        }

        return b.toString();
    }

    @Context
    SecurityContext securityContext;

    public ContainerResponse filter(final ContainerRequest containerRequest, final ContainerResponse containerResponse) {
        final Principal principal      = securityContext.getUserPrincipal();
        final String                         requestPath    = containerRequest.getRequestUri().toString();
        final MultivaluedMap<String, String> headers        = containerRequest.getRequestHeaders();
        final String                         requestEntity  = getEntityBody(containerResponse.getRequestContext());
        final Object                         responseEntity = containerResponse.getEntity();

        if (responseEntity instanceof BaseEntity) {
            final BaseEntity baseEntity = (BaseEntity) responseEntity;

            if (baseEntity.getHeader() != null && principal != null) {
                baseEntity.getHeader().setUsername(principal.getName());
            }
        }

        log.info(LOGGER_MESSAGE, principal, requestPath, headers.toString(), requestEntity, responseEntity);

        return containerResponse;
    }
}
