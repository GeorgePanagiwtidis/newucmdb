package gr.cosmote.ucmdb.web.services;

import gr.cosmote.ucmdb.ejb.ViewQueriesRemote;
import gr.cosmote.ucmdb.models.database.ViewQueries;
import gr.cosmote.ucmdb.models.database.ViewQueriesProperties;
import gr.cosmote.ucmdb.models.view.viewquery.ViewQueryRequestDTO;
import gr.cosmote.ucmdb.models.view.viewquery.ViewQueryResponseDTO;
import gr.cosmote.ucmdb.web.entity.BaseEntity;
import gr.cosmote.ucmdb.web.entity.BaseEntityException;
import lombok.extern.slf4j.Slf4j;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/viewQueries")
@Slf4j
public class ViewQueriesService {

    @EJB
    private ViewQueriesRemote viewQueriesEjb;

    @Path("/getViewQueries")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<List<ViewQueries>> getViewQueries() {
        log.debug("ViewQueriesService.getViewQueries :: Method Entered");
        BaseEntity<List<ViewQueries>> queriesResponse = new BaseEntity<>();

        try {
            queriesResponse.setPayload(viewQueriesEjb.getViewQueries());
        }
        catch (Exception ex) {
            queriesResponse.setError(BaseEntityException.NO_VIEW_QUERIES);
            log.error("ViewQueriesService.getViewQueries :: ERROR {}", ex);
        }

        log.debug("ViewQueriesService.getViewQueries :: Method Returned");
        return  queriesResponse;
    }

    @Path("/executeQuery")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<List<ViewQueryResponseDTO>> executeQueryByName(ViewQueryRequestDTO viewQueryRequest) {
        log.debug("ViewQueriesService.executeQueryByName :: Method Entered");
        BaseEntity<List<ViewQueryResponseDTO>> queryResponse = new BaseEntity<>();

        try {
            queryResponse.setPayload(viewQueriesEjb.executeTopologyQuery(viewQueryRequest));
        }
        catch (Exception ex) {
            queryResponse.setError(BaseEntityException.QUERY_EXECUTION_ERROR);
            log.error("ViewQueriesService.executeQueryByName :: ERROR {}", ex);
        }

        log.debug("ViewQueriesService.executeQueryByName :: Method Returned");
        return queryResponse;
    }

    @Path("/clearCache")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String clearQueriesCache() {
        log.debug("ViewQueriesService.clearQueriesCache :: Method Entered");

        try {
            viewQueriesEjb.clearCache();
        }
        catch (Exception ex) {
            log.error("ViewQueriesService.clearQueriesCache :: ERROR {}", ex);
            return "FAILURE";
        }

        return "SUCCESS";
    }

    @Path("/getQueryProperties/{queryName}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<List<ViewQueriesProperties>> getViewQueryProperties(@PathParam("queryName") String queryName) {
        log.debug("ViewQueryService.getViewQueryProperties :: Method Entered with Query Name {}", queryName);
        BaseEntity<List<ViewQueriesProperties>> viewQueryProperties = new BaseEntity<>();

        try {
            viewQueryProperties.setPayload(viewQueriesEjb.getViewQueryProperties(queryName));
        }
        catch (Exception ex) {
            log.error("ViewQueryService.getViewQueryProperties :: ERROR", ex);

            viewQueryProperties.setError(BaseEntityException.NO_VIEW_QUERY_PROPERTIES);
            return viewQueryProperties;
        }

        log.debug("ViewQueryService.getViewQueryProperties :: Method Returned");
        return viewQueryProperties;
    }
}
