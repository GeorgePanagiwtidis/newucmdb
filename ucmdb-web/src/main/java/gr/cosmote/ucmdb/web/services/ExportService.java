package gr.cosmote.ucmdb.web.services;

import gr.cosmote.ucmdb.commons.exportXls.*;
import gr.cosmote.ucmdb.ejb.*;
import gr.cosmote.ucmdb.ejb.BusinessRemote;
import gr.cosmote.ucmdb.ejb.NodeRemote;
import gr.cosmote.ucmdb.ejb.UserActionsRemote;
import gr.cosmote.ucmdb.ejb.ViewQueriesRemote;
import gr.cosmote.ucmdb.models.base.*;
import gr.cosmote.ucmdb.models.database.*;
import gr.cosmote.ucmdb.models.view.UserCommentDTO;
import gr.cosmote.ucmdb.models.view.WorkflowStatsDTO;
import gr.cosmote.ucmdb.models.view.infrastructureDTOs.InfraExcelExportRequest;
import gr.cosmote.ucmdb.models.view.viewquery.ViewQueryRequestDTO;
import gr.cosmote.ucmdb.models.view.viewquery.ViewQueryResponseDTO;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import gr.cosmote.ucmdb.web.helper.EjbHelper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Path("/export")
public class ExportService {

    @EJB
    private NodeRemote nodeRemote;
    @EJB
    private BusinessRemote businessRemote;
    @EJB
    UserActionsRemote userCommentsEjb;
    @EJB
    ViewQueriesRemote viewQueriesRemote;
    @EJB
    InfrastructureRemote infrastructureRemote;

    @EJB
    CiAttributeConfigurationRemote ciAttributeConfigurationRemote;

    @POST
    @Path("/xls")
    @Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    @Consumes({MediaType.APPLICATION_JSON})
    public byte[] exportXls(@Context final ServletContext servletContext, final ExcelCriteria criteria) throws NamingException {
        log.debug("ExportService.exportXls ::: Method Entered for Business Service {} which belongs to Tenant {}", criteria.getBusinessServiceName(), criteria.getTenant());

        final InputStream is = servletContext.getResourceAsStream("/WEB-INF/uCmdbExportTemplate.xls");
        byte[] byteArray = null;
        BusinessResult businessResult = businessRemote.getBusinessService(criteria);

        if (!isBusinessResultEmpty(businessResult)) {
            try {
                GenerateXls genXls = new GenerateXls();
                genXls.setInput(is);
                genXls.setBs(businessResult);
                byteArray = genXls.generateXlsFile();
            } catch (Exception ex) {
                log.error("ExportService :: exportXIs :: An error occurred while exporting Excel File {}", ex);
                return null;
            } finally {
                IOUtils.closeQuietly(is);
            }
        } else {
            byteArray = new byte[0];
        }

        log.debug("ExportService :: exportXIs :: Method Returned Exported Excel");
        return byteArray;
    }

    @POST
    @Path("/xlsInfrastructure")
    @Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    @Consumes({MediaType.APPLICATION_JSON})
    public byte[] exportXlsInfrastructure(final InfraExcelExportRequest criteria) {
        log.debug("ExportService.exportXlsInfrastructure ::: Entered with searchCriteria {}", criteria);

        byte[] byteArray;
        try {
            List<Infrastructure> results = infrastructureRemote.
                    getInfraResultsForExport(criteria);

            if (results.size() > 0) {
                GenerateXlsInfrastructure genXls = new GenerateXlsInfrastructure();
                genXls.setInfraList(results);
                genXls.setCiAttributes(getCiAttributes(criteria));
                byteArray = genXls.generateXlsFile();
                return byteArray;
            }
        } catch (Exception e) {
            log.error("ExportService.exportXlsInfrastructure :: ERROR {}", e);
        }


        return null;
    }

    @Path("/exportViewQuery")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response generateViewQueriesExcel(ViewQueryRequestDTO requestDTO) {
        log.debug("ExportService.generateViewQueriesExcel :: Method Entered with {}", requestDTO);
        Response excelResponse;

        byte[] workbookBytes;
        try {
            List<ViewQueryResponseDTO> viewQueryResponse = viewQueriesRemote.executeTopologyQuery(requestDTO);
            GenerateViewQueriesExcel viewQueriesExcel = new GenerateViewQueriesExcel();
            viewQueriesExcel.setQueryProperties(getViewQueryProperties(requestDTO.getQueryName()));
            viewQueriesExcel.setViewQueryResponse(viewQueryResponse);

            workbookBytes = viewQueriesExcel.generateExcel(requestDTO);
            excelResponse = Response.ok(workbookBytes).build();
        } catch (Exception ex) {
            log.error("ExportService.generateViewQueriesExcel :: ERROR", ex);

            return Response.serverError().build();
        }

        log.debug("ExportService.generateViewQueriesExcel :: Method Returned Exported Excel Size of {}", workbookBytes.length);
        return excelResponse;
    }

    private List<ViewQueriesProperties> getViewQueryProperties(String queryName) {
        ViewQueries viewQuery =
                viewQueriesRemote.getViewQueries()
                        .stream()
                        .filter(query -> query.getQueryName().equals(queryName))
                        .findFirst().orElse(null);

        return viewQuery != null ? viewQuery.getViewQueriesProperties() : null;
    }

    @Path("/exportUserComments")
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response generateUserCommentsExcel() {
        log.debug("ExportService.generateUserCommentsExcel :: Method Entered");

        byte[] exportedExcelBytes;
        try {
            List<UserCommentDTO> userComments = userCommentsEjb.getUserComments();

            GenerateUserCommentsExcel userCommentsExcel = new GenerateUserCommentsExcel();
            userCommentsExcel.setUserDbComments(userComments);
            exportedExcelBytes = userCommentsExcel.exportExcel();

            if (exportedExcelBytes == null) return Response.serverError().build();
        }
        catch (Exception ex) {
            log.error("ExportService.generateUserCommentsExcel :: Method Entered");

            return Response.serverError().build();
        }

        log.debug("ExportService.generateUserCommentsExcel :: Method Returned Exported Excel");

        return Response.ok(exportedExcelBytes).build();
    }

    @Path("/exportUserActionAuditLog")
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response generateUserActionAuditLogExcel() {
        log.debug("ExportService.generateUserActionAuditLogExcel :: Method Entered");

        byte[] exportedActionExcelBytes;
        try{
            List<NodeDifferences> userAuditingActions = nodeRemote.getAllNodeActions();
            userAuditingActions.sort(Comparator.comparing(NodeDifferences::getLastModifiedTime).reversed());

            GenerateUserActionAuditLogExcel userAuditingActionsExcel = new GenerateUserActionAuditLogExcel();
            userAuditingActionsExcel.setUserDbActions(userAuditingActions);
            exportedActionExcelBytes = userAuditingActionsExcel.exportActionExcel();

            if (exportedActionExcelBytes == null) return Response.serverError().build();
        }
        catch (Exception ex){
            log.error("ExportService.generateUserActionAuditLogExcel :: ERROR");

            return Response.serverError().build();
        }
        log.debug("ExportService.generateUserActionAuditLogExcel :: Method Returned Exported Action Audit Log Excel");
        return Response.ok(exportedActionExcelBytes).build();
    }

    @Path("/exportMissingCis")
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response generateMissingCisExcel() {
        log.debug("ExportService.generateMissingCisExcel :: Method Entered");

        byte[] exportedMissingCisBytes;
        try{
            List<UcmdbMissingNode> missingCis = userCommentsEjb.getUcmdbMissingNodes();
            missingCis.sort(Comparator.comparing(UcmdbMissingNode::getInsertedDate).reversed());

            GenerateMissingCisExcel missingCisExcel = new GenerateMissingCisExcel();
            missingCisExcel.setMissingCisDb(missingCis);
            exportedMissingCisBytes = missingCisExcel.exportMissingCisExcel();

            if (exportedMissingCisBytes == null) return Response.serverError().build();
        }
        catch (Exception ex){
            log.error("ExportService.generateMissingCisExcel :: ERROR");

            return Response.serverError().build();
        }
        log.debug("ExportService.generateMissingCisExcel :: Method Returned Exported Action Audit Log Excel");
        return Response.ok(exportedMissingCisBytes).build();
    }

    @Path("/exportWorkflowStats")
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response generateWorkflowStatsExcel(){
        log.debug("ExportService.generateWorkflowStatsExcel :: Method Entered");

        byte[] exportedWorkflowStatsBytes;
        try {
            List<WorkflowStatsDTO> workflowStats = nodeRemote.getWorkflowStats();
            workflowStats.sort(Comparator.comparing(WorkflowStatsDTO::getWorkflowMonth));

            GenerateWorkflowStatsExcel workflowStatsExcel = new GenerateWorkflowStatsExcel();
            workflowStatsExcel.setWorkflowStatsDb(workflowStats);
            exportedWorkflowStatsBytes = workflowStatsExcel.exportWorkflowStatsExcel();

            if   (exportedWorkflowStatsBytes == null){
                return Response.serverError().build();
            }
        }
        catch (Exception ex){
            log.error("ExportService.generateWorkflowStatsExcel :: ERROR");
            return Response.serverError().build();
        }
        log.debug("ExportService.generateWorkflowStatsExcel :: Method Returned Exported Workflow Stats Excel");

        return Response.ok(exportedWorkflowStatsBytes).build();
    }

    private List<CiAttributeConfiguration> getCiAttributes(InfraExcelExportRequest criteria) throws NamingException {
        log.debug("ExportService.getCiAttributes :: Method Entered with {}", criteria);
        List<CiAttributeConfiguration> attributes;

        String ciType;
        try {
            ciType = criteria.getBusinessCriteria().getSearchTerm();
        } catch (NullPointerException npe) {
            ciType = "node";
        }

        attributes = filterCiAttributesBasedOnUserSelectedColumns(ciAttributeConfigurationRemote.getCiAttributesConfiguration(ciType), criteria.getSelectedTableColumns());

        log.debug("ExportService.getCiAttributes :: Method Returned Attributes size of {}", attributes.size());
        return attributes;
    }

    private List<CiAttributeConfiguration> filterCiAttributesBasedOnUserSelectedColumns(List<CiAttributeConfiguration> attrs, List<String> selectedColumns) {
        return attrs
                .stream()
                .filter(ciAttribute -> selectedColumns.contains(ciAttribute.getAttributeName()))
                .collect(Collectors.toList());
    }

    private boolean isBusinessResultEmpty(BusinessResult businessResult) {
        // Check if Business Result Contains Computers, BA on NetDevices

        long resultsCount =
                businessResult.getCiCollection()
                        .stream()
                        .filter(ciCollection -> getNonEmptyCiCollections(ciCollection))
                        .count();

        return resultsCount == 0;
    }

    private boolean getNonEmptyCiCollections(Ci ciCollection) {
        boolean noComputers = ciCollection.getComputers().size() != 0;
        boolean noBusinessApplications = ciCollection.getBusinessApplications() != null;
        boolean noNetDevices = ciCollection.getNetdevices() != null;

        return noComputers || noBusinessApplications || noNetDevices;
    }
}

