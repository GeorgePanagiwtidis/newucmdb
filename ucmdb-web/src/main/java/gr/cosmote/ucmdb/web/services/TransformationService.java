package gr.cosmote.ucmdb.web.services;

import gr.cosmote.ucmdb.commons.helper.DateHelper;
import gr.cosmote.ucmdb.ejb.TasksRemote;
import gr.cosmote.ucmdb.models.base.CIAttribute;
import gr.cosmote.ucmdb.models.database.Task;
import gr.cosmote.ucmdb.models.database.TaskSummary;
import gr.cosmote.ucmdb.models.enumeration.TaskStatusEnum;
import gr.cosmote.ucmdb.models.enumeration.TaskStatusReasonEnum;
import gr.cosmote.ucmdb.models.view.TaskAttributeDTO;
import gr.cosmote.ucmdb.models.view.TaskDTO;
import gr.cosmote.ucmdb.models.view.TaskEditDTO;
import gr.cosmote.ucmdb.models.view.TaskGroupDTO;
import gr.cosmote.ucmdb.models.view.TaskReportDTO;
import gr.cosmote.ucmdb.web.helper.EjbHelper;
import lombok.extern.slf4j.Slf4j;

import javax.ejb.EJB;
import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class TransformationService {
    @EJB
    private static TasksRemote tasksRemote;

    public static List<TaskDTO> transformTaskSummaryToDTO(List<TaskSummary> taskSummaryList) {
        return taskSummaryList.stream()
                .map(ts -> {
                    Task task = ts.getTask();

                    TaskDTO taskDTO = new TaskDTO();
                    taskDTO.setId(String.valueOf(ts.getId()));
                    taskDTO.setNodeId(ts.getNode().getId());
                    taskDTO.setStatus((task.getStatus() != null) ? task.getStatus().toString() : "");
                    taskDTO.setNodeGlobalId(ts.getNode().getGlobalId());
                    taskDTO.setAssignedDate(ts.getTask().getAssignedDate());
                    taskDTO.setCiType(ts.getCiType());
                    taskDTO.setOsType(ts.getOsType());
                    taskDTO.setCreationDate(task.getCreationDate());
                    taskDTO.setOwnerTenant(ts.getOwnerTenant());
                    taskDTO.setDisplayLabel(ts.getDisplayLabel());
                    taskDTO.setTaskId("" + task.getId());
                    taskDTO.setMonitored(task.isMonitored());
                    taskDTO.setCurrentStage(task.getCurrentStage());

//                    //TODO: Check this below
//                    UcmdbUsers loggedUser = new UcmdbUsers();
//                    loggedUser.setUsername(ts.getAssignedTo());
//                    taskDTO.setAssignedTo(loggedUser);
                    taskDTO.setAssignedTo(ts.getAssignedTo());

                    taskDTO.setGroup(ts.getGroup().getDescription());

                    return taskDTO;
                })
                .collect(Collectors.toList());
    }

    public static TaskDTO transformTaskSummaryToDTO(TaskSummary taskSummary) {
        TaskDTO taskDTO = new TaskDTO();

        taskDTO.setId(String.valueOf(taskSummary.getId()));
        taskDTO.setNodeGlobalId(taskSummary.getNode().getGlobalId());
        taskDTO.setNodeId(taskSummary.getNode().getId());
        taskDTO.setDisplayLabel(taskSummary.getDisplayLabel());
        taskDTO.setOwnerTenant(taskSummary.getOwnerTenant());
        taskDTO.setCiType(taskSummary.getCiType());
        taskDTO.setOsType(taskSummary.getOsType());
        taskDTO.setAssignedDate(taskSummary.getTask().getAssignedDate());
        taskDTO.setCreationDate(taskSummary.getTask().getCreationDate());
        taskDTO.setGroup(taskSummary.getGroup().getDescription());
        taskDTO.setStatus(taskSummary.getTask().getStatus().toString());
        taskDTO.setAssignedTo(taskSummary.getAssignedTo());
        taskDTO.setTaskId(String.valueOf(taskSummary.getTaskId()));
        taskDTO.setMonitored(taskSummary.getTask().isMonitored());
        taskDTO.setCurrentStage(taskSummary.getTask().getCurrentStage());

        return taskDTO;
    }

    public static TaskReportDTO transformTaskToReportDTO(Task task) {
        TaskReportDTO taskReportDTO = new TaskReportDTO();

        taskReportDTO.setNodeName(task.findCIAttributeValue("display_label"));
        taskReportDTO.setOsType(task.getOsType());
        taskReportDTO.setOwnerTenant(task.findCIAttributeValue("TenantOwner"));
        taskReportDTO.setTaskStatus(task.getStatus().toString());
        taskReportDTO.setAssignedDate(DateHelper.dateTimeToString(task.getAssignedDate()));
        taskReportDTO.setAssignedGroup(task.getUserGroup().getDescription());
        taskReportDTO.setAssignedEngineer(task.getCurrentAssignee());
        taskReportDTO.setProcessId(String.valueOf(task.getProcessId()));
//        taskReportDTO.setGlobalId(task.getGlobalId());
//        taskReportDTO.setCiType(task.getCiType());

        return taskReportDTO;
    }

    public static Task transformTaskEditDTOToTask(TaskEditDTO taskEditDTO) {
        log.debug("TasksEjb.transformTaskEditDTOToTask :: Method entered with input {}", taskEditDTO);

        TaskSummary taskSummaryRecord = null;
        taskSummaryRecord = tasksRemote.getTaskSummaryRecord(Long.parseLong(taskEditDTO.getTaskId()));

        Task task = new Task();
        task.setTaskStatusReasonComment(taskEditDTO.getReassignTask() != null ? taskEditDTO.getReassignTask().getReassignReason() : null);
        task.setAssignedDate(DateHelper.stringToDateTimeTo(taskEditDTO.getAssignedDate()));
        task.setCiType(taskEditDTO.getCiType());
        task.setOsType(taskEditDTO.getOsType());
        task.setCompletionDate(taskEditDTO.getCompletionDate());
        task.setId(Long.parseLong(taskEditDTO.getTaskId()));
        task.setName(taskEditDTO.getHeader().getTitle());
        task.setUserGroup(taskSummaryRecord != null && taskSummaryRecord.getGroup() != null ? taskSummaryRecord.getGroup() : null);
        task.setStatus(taskEditDTO.getTaskStatus() != null ? TaskStatusEnum.valueOf(taskEditDTO.getTaskStatus()) : null);
        task.setCurrentAssignee(taskEditDTO.getCurrentAssignee());
        task.setGlobalId(taskEditDTO.getNodeGlobalId());
        task.setMonitored(taskEditDTO.isMonitored());
        task.setCurrentStage(taskEditDTO.getCurrentStage());
        log.debug("TasksEjb.transformTaskEditDTOToTask :: Transformation Started....");

        // Task Attributes
        List<CIAttribute> updatedCiAttributes = null;
        if (task.getAttributes() != null) {
            updatedCiAttributes = task.getAttributes();
        } else {
            updatedCiAttributes = new ArrayList<CIAttribute>();
        }

        // Update Groups
        List<TaskGroupDTO> updatedTaskGroups = taskEditDTO.getGroups();

        Map<Integer, String> groupIndexNameMapping = new HashMap<Integer, String>();
        int index = 0;
        for (TaskGroupDTO taskGroupDTO : updatedTaskGroups) {
            groupIndexNameMapping.put(index, taskGroupDTO.getGroupName());
            index++;
        }

        for (TaskGroupDTO taskGroupDTO : updatedTaskGroups) {

            List<TaskAttributeDTO> taskGroupAttributes = taskGroupDTO.getAttributes();

            for (TaskAttributeDTO taskAttributeDTO : taskGroupAttributes) {
                CIAttribute ciAttribute = new CIAttribute();
                ciAttribute.setGroupName(taskGroupDTO.getGroupName());
                ciAttribute.setGroupPriority(taskGroupDTO.getGroupPriority());


                ciAttribute.setFieldLabel(taskAttributeDTO.getFieldLabel());
                ciAttribute.setFieldName(taskAttributeDTO.getFieldName());
                ciAttribute.setFieldValue(taskAttributeDTO.getFieldValue());
                ciAttribute.setFieldLovs(taskAttributeDTO.getFieldLOV());
                ciAttribute.setFieldType(transformFieldType(taskAttributeDTO.getFieldType()));
                ciAttribute.setEditable(taskAttributeDTO.isEditable());
                ciAttribute.setMandatory(taskAttributeDTO.isMandatory());

                updatedCiAttributes.add(ciAttribute);
            }
        }


        task.setAttributes(updatedCiAttributes);

        log.debug("TasksEjb.transformTaskEditDTOToTask :: Method returned transformed Task {}", task);

        return task;
    }

    private static String transformFieldType(TaskAttributeDTO.FIELDTYPE fieldtype) {
        if (TaskAttributeDTO.FIELDTYPE.dropdown.equals(fieldtype)) {
            return "TEXT_LIST";
        } else if (TaskAttributeDTO.FIELDTYPE.dropdown_multiple.equals(fieldtype)) {
            return "LIST_VALUES";
        } else if (TaskAttributeDTO.FIELDTYPE.checkbox.equals(fieldtype)) {
            return "BOOLEAN";
        } else if (TaskAttributeDTO.FIELDTYPE.numeric.equals(fieldtype)) {
            return "NUMBER";
        } else if (TaskAttributeDTO.FIELDTYPE.date.equals(fieldtype)) {
            return "DATE";
        } else if (TaskAttributeDTO.FIELDTYPE.list.equals(fieldtype)) {
            return "LIST";
        } else {
            return "TEXT";
        }
    }

    public static List<TaskAttributeDTO> transformCiAttributeListToTaskAttributeDTOList(List<CIAttribute> ciAttributes) {
        return
                ciAttributes.stream()
                        .map((ciAttribute -> {
                            TaskAttributeDTO taskAttribute = new TaskAttributeDTO();
                            taskAttribute.setFieldLabel(ciAttribute.getFieldLabel());
                            taskAttribute.setFieldName(ciAttribute.getFieldName());
                            taskAttribute.setFieldValue(ciAttribute.getFieldValue());
                            taskAttribute.setFieldLOV(ciAttribute.getFieldLovs());
//                        taskAttribute.setFieldType(TaskAttributeDTO.FIELDTYPE.valueOf(ciAttribute.getFieldType()));
                            taskAttribute.setAttributePriority(ciAttribute.getGroupPriority());
                            taskAttribute.setEditable(ciAttribute.isEditable());
                            taskAttribute.setMandatory(ciAttribute.isMandatory());

                            return taskAttribute;
                        }))
                        .collect(Collectors.toList());
    }
}
