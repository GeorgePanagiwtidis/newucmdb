package gr.cosmote.ucmdb.web.services;

import com.jcabi.aspects.Loggable;

import javax.ejb.EJB;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import gr.cosmote.ucmdb.commons.exceptions.ApplicationException;
import gr.cosmote.ucmdb.ejb.UserRemote;
import gr.cosmote.ucmdb.models.base.NameCodeObject;
import gr.cosmote.ucmdb.models.database.UcmdbUsers;
import gr.cosmote.ucmdb.models.view.UserInfoDTO;
import gr.cosmote.ucmdb.web.entity.BaseEntity;
import gr.cosmote.ucmdb.web.entity.BaseEntityException;
import gr.cosmote.ucmdb.web.helper.EjbHelper;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@Path("/secure/user")
public class UserService {
    
    @EJB
    private UserRemote userRemote;

    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Loggable(value = Loggable.DEBUG, name = "gr.cosmote")
    @Path("/getUser")
    public BaseEntity<UcmdbUsers> getUser(@Context final HttpServletRequest httpServletRequest, @Context final HttpServletResponse response) {
        log.debug("UserService.getUser() ::: Entered method");

        final HttpSession session      = httpServletRequest.getSession(true);
        final String      username     = httpServletRequest.getUserPrincipal().getName();
        final Object      sessionValue = session.getAttribute(username);
        if (sessionValue != null) {
            log.debug("UserService.getUser() ::: Returning user from Session: {}", sessionValue);
            return new BaseEntity<UcmdbUsers>((UcmdbUsers)sessionValue);
        }

        try {
            final UcmdbUsers ucmdbUsers = userRemote.getUser();

            if ( ucmdbUsers == null ) {
                final  BaseEntity<UcmdbUsers> value = new BaseEntity<UcmdbUsers>();
                log.debug("UserService.getUser() ::: Return if user = null = {}",value);
                return value;
            }

            final  BaseEntity<UcmdbUsers> value = new BaseEntity<UcmdbUsers>(ucmdbUsers);
            log.debug("UserService.getUser() ::: Return = {}",value);
            return value;

        } catch (Exception e){
            log.error("UserService.getUser() ::: Error in getUser method with HttpServletRequest = {}",httpServletRequest,e);
            return new BaseEntity<UcmdbUsers>(BaseEntityException.NO_USER_FOUND);
        }
    }

    @Path("/getUserInfo")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<UserInfoDTO> getUserInfo() throws NamingException {
        log.debug("UserService :: getUserInfo :: Method Entered");
        BaseEntity<UserInfoDTO> userInfo = new BaseEntity<>();

        try {
            userInfo.setPayload(userRemote.getUserInfo());
        }
        catch (Exception ex) {
            log.error("UserService :: getUserInfo :: ERROR {}", ex);

            userInfo.setError(BaseEntityException.NO_USER_FOUND);
            return userInfo;
        }

        log.debug("UserService :: getUserInfo :: Method Returned");
        return userInfo;
    }

    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Loggable(value = Loggable.DEBUG, name = "gr.cosmote")
    @Path("/getAllUsers")
    public BaseEntity<List<UcmdbUsers>> getUserGroups() throws NamingException {
        log.debug("UserService.getUserGroups() ::: Entered method");
        BaseEntity<List<UcmdbUsers>> allUsers = new BaseEntity<>();

        try {
            allUsers.setPayload(userRemote.getAllUsers());
        }
        catch (ApplicationException ex) {
            log.error("UserService :: getAllUsers :: ERROR {}", ex);

            return new BaseEntity<>(BaseEntityException.NO_USER_FOUND);
        }

        log.debug("UserService :: getAllUsers :: Method Returned Users size of {}", allUsers.getPayload().size());
        return allUsers;
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Loggable(value = Loggable.DEBUG, name = "gr.cosmote")
    @Path("/makeUserInactive")
    public BaseEntity<UcmdbUsers> makeUserInactive(NameCodeObject nameCode) throws NamingException {
        log.debug("UserService.makeUserInactive() ::: Entered method");

        try {
            final UcmdbUsers inactiveUcmdbUsers = userRemote.makeUserInactive(nameCode);

            final BaseEntity<UcmdbUsers> value = new BaseEntity<UcmdbUsers>(inactiveUcmdbUsers);
            log.debug("UserService.makeUserInactive() ::: Return = {}",value);
            return value;
        } catch (Exception ex) {
            log.error("UserService.makeUserInactive() ::: Error in method = {}",ex);
            return new BaseEntity<>(BaseEntityException.NO_USER_INACTIVE);
        }
    }
}
