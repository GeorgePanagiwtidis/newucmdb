package gr.cosmote.ucmdb.web.entity;

import java.util.Date;

import javax.ws.rs.core.Response;

import gr.cosmote.ucmdb.models.base.ValueObject;
import lombok.Data;

@Data
public class JsonHeader extends ValueObject {
    private Date            timestamp = new Date();
    private Response.Status status    = Response.Status.OK;
    private String          username  = null;
    private String          info      = null;

    public static final JsonHeader getDefault() {
        return new JsonHeader();
    }
}
