package gr.cosmote.ucmdb.web.services;

import gr.cosmote.ucmdb.ejb.UserActionsRemote;
import gr.cosmote.ucmdb.models.database.UcmdbCisComment;
import gr.cosmote.ucmdb.models.database.UcmdbMissingNode;
import gr.cosmote.ucmdb.models.view.UserCommentDTO;
import gr.cosmote.ucmdb.web.entity.BaseEntity;
import gr.cosmote.ucmdb.web.entity.BaseEntityException;
import lombok.extern.slf4j.Slf4j;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Path("secure/userActions")
@Slf4j
public class UserActionsService {

    @EJB
    private UserActionsRemote userActionsRemote;

    @Path("addCiComment")
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addCiComment(@QueryParam("dispLabel") String dispLabel, @QueryParam("tenantOwner") String tenantOwner, @QueryParam("globalId") String globalId, String userComment) {
        log.debug("UserActionsService.addCiComment :: Method Entered for CI with Display Label {}, Tenant Owner {} & Global Id {}", dispLabel, tenantOwner, globalId);
        BaseEntity<UcmdbCisComment> commentResponse = new BaseEntity<>();

        try {
            UcmdbCisComment updatedComment = userActionsRemote.addNewCiComment(dispLabel, tenantOwner, globalId, userComment);

            commentResponse.setPayload(updatedComment);
        } catch (Exception ex) {
            log.error("UserActionsService.addCiComment :: ERROR", ex);

            commentResponse.setError(BaseEntityException.NO_UPDATE_PERFORMED);
            return Response.serverError().entity(commentResponse).build();
        }

        log.debug("UserActionsService.addCiComment :: Method Added new User Comment");
        return Response.ok(commentResponse).build();
    }

    @Path("getUserComments")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserComments() {
        log.debug("UserActionsService.getUserComments :: Method Entered");
        BaseEntity<List<UserCommentDTO>> userCommentsEntity = new BaseEntity<>();

        try {
            List<UserCommentDTO> userComments = userActionsRemote.getUserComments();
            userCommentsEntity.setPayload(userComments);
        } catch (Exception ex) {
            log.error("UserActionsService.getUserComments :: Error", ex);

            userCommentsEntity.setError(BaseEntityException.NO_CIS_FOUND);
            return Response.serverError().entity(userCommentsEntity).build();
        }

        log.debug("UserActionsService.getUserComments :: Method Returned Comments size of {}", userCommentsEntity.getPayload().size());
        return Response.ok(userCommentsEntity).build();
    }

    @Path("addMissingNode")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addMissingNode(UcmdbMissingNode missingNode) {
        log.debug("UserActionsService.addMissingNode :: Method Entered with {}", missingNode);
        BaseEntity<UcmdbMissingNode> addedMissingNodeEntity = new BaseEntity<>();

        try {
            UcmdbMissingNode addedMissingNode = userActionsRemote.addMissingNode(missingNode);

            addedMissingNodeEntity.setPayload(addedMissingNode);
        }
        catch (Exception ex) {
            log.error("UserActionsService.addMissingNode :: ERROR", ex);

            Optional<Throwable> exceptionSqlCause =
                    Stream
                            .iterate(ex, Throwable::getCause).filter(cause -> cause.getCause() instanceof SQLIntegrityConstraintViolationException)
                            .findFirst();

            addedMissingNodeEntity.setError(exceptionSqlCause.isPresent() ? BaseEntityException.NO_UPDATE_PERFORMED_DUPLICATE_RECORD : BaseEntityException.NO_UPDATE_PERFORMED);
            return Response.serverError().entity(addedMissingNodeEntity).build();
        }

        log.debug("UserActionsService.addMissingNode :: Method Successfully Added Missing Node");
        return Response.ok(addedMissingNodeEntity).build();
    }

    @Path("getUcmdbMissingNodes")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUcmdbMissingNodes() {
        log.debug("UserActionsService.getUcmdbMissingNodes :: Method Entered");
        BaseEntity<List<UcmdbMissingNode>> ucmdbMissingNodes = new BaseEntity<>();

        try {
            List<UcmdbMissingNode> ucmdbMissingNodesData = userActionsRemote.getUcmdbMissingNodes();

            ucmdbMissingNodes.setPayload(ucmdbMissingNodesData);
        }
        catch (Exception ex) {
            log.debug("UserActionsService.getUcmdbMissingNodes :: ERROR", ex);
            ucmdbMissingNodes.setError(BaseEntityException.NO_CIS_FOUND);

            return Response.serverError().entity(ucmdbMissingNodes).build();
        }

        log.debug("UserActionsService.getUcmdbMissingNodes :: Method Returned Missing Nodes size of {}", ucmdbMissingNodes.getPayload().size());
        return Response.ok(ucmdbMissingNodes).build();
    }
}
