package gr.cosmote.ucmdb.web.services;

import gr.cosmote.ucmdb.commons.exceptions.ApplicationException;
import gr.cosmote.ucmdb.commons.helper.DateHelper;
import gr.cosmote.ucmdb.commons.helper.MailHelper;
import gr.cosmote.ucmdb.commons.properties.ApplicationProperties;
import gr.cosmote.ucmdb.ejb.GroupRemote;
import gr.cosmote.ucmdb.ejb.NodeRemote;
import gr.cosmote.ucmdb.ejb.TasksRemote;
import gr.cosmote.ucmdb.ejb.UserRemote;
import gr.cosmote.ucmdb.models.base.CIAttribute;
import gr.cosmote.ucmdb.models.database.*;
import gr.cosmote.ucmdb.models.enumeration.InputCommandEnum;
import gr.cosmote.ucmdb.models.enumeration.TaskStatusEnum;
import gr.cosmote.ucmdb.models.view.*;
import gr.cosmote.ucmdb.service.ExportExcelService;
import gr.cosmote.ucmdb.web.entity.BaseEntity;
import gr.cosmote.ucmdb.web.entity.BaseEntityException;
import gr.cosmote.ucmdb.web.helper.EjbHelper;
import gr.cosmote.ucmdb.web.helper.ResponseHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
@Stateless
@Path("/secure/task")
public class TasksService {

    @EJB
    private NodeRemote nodeRemote;
    @EJB
    private UserRemote userRemote;
    @EJB
    private ExportExcelService exportExcelService;

    @EJB
    private TasksRemote tasksRemote;

    // TODO -- Remove when Drools Rules are finished
    @EJB
    private GroupRemote groupEjb;

    @Path("/getTasks")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<List<TaskDTO>> getGroupTasks() throws NamingException {
        // Get LoggedIn User Groups
        List<Group> groups = groupEjb.getUserGroups();

        log.debug(" TasksService.getGroupTasks :: Entered Method for groups {} ", groups);

        BaseEntity<List<TaskDTO>> value = new BaseEntity<>();

        List<TaskDTO> userTasks = getUserTasks(groups);

        value.setPayload(userTasks);

        log.debug(" TasksService.getGroupTasks :: Method Returned a list size of {} ", value.getPayload().size());

        return value;
    }

    @Path("/taskAssignment")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<TaskDTO> taskAssignment(TaskDTO taskDTO) throws NamingException {
        log.debug(" TasksService.taskAssignment :: Method Entered with Input {} ", taskDTO);

        BaseEntity<TaskDTO> value = new BaseEntity<>();

        UcmdbUsers loggedUser = userRemote.getUser();
        TaskSummary taskSummary = tasksRemote.getTaskSummaryRecord(Long.valueOf(taskDTO.getTaskId()));

        if (taskSummary != null) {
            try {
                Task task = taskSummary.getTask();
                if (taskSummary.getAssignedTo() != null) {
                    if (taskSummary.getAssignedTo().equals(loggedUser.getUsername())) {
                        taskSummary.setAssignedTo(null);
                        task.setStatus(TaskStatusEnum.OPEN);
                        task.setCurrentAssignee(null);
                    } else {
                        taskSummary.setAssignedTo(loggedUser.getUsername());
                        task.setStatus(TaskStatusEnum.IN_PROGRESS);
                        task.setCurrentAssignee(loggedUser.getUsername());
                    }
                } else {
                    taskSummary.setAssignedTo(loggedUser.getUsername());
                    task.setStatus(TaskStatusEnum.IN_PROGRESS);
                    task.setCurrentAssignee(loggedUser.getUsername());
                }
                try {
                    taskSummary.setTask(task);
                } catch (IOException e) {
                    throw new ApplicationException(e);
                }
                tasksRemote.setTaskAssignee(taskSummary);
                value.setPayload(TransformationService.transformTaskSummaryToDTO(taskSummary));
            } catch (ApplicationException appException) {
                log.debug(" TasksService.taskAssignment :: Error while assigning task. Error:{}", appException);
                value.setError(BaseEntityException.NO_UPDATE_PERFORMED);
                return value;
            }
        } else {
            log.debug(" TasksService.taskAssignment :: Error while getting TaskSummary record from DB");
            value.setError(BaseEntityException.NO_UPDATE_PERFORMED);
            return value;
        }

        log.debug(" TasksService.taskAssignment :: Method Returned with payload {}", value.getPayload());
        return value;
    }

    @Path("/getTaskAttrs")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<TaskEditDTO> getTaskAttributes(TaskDTO taskDTO) throws NamingException {
        log.debug(" TasksService.getTasksAttributes() ::: Method Entered with input {} ", taskDTO);
        BaseEntity<TaskEditDTO> value = new BaseEntity<>();
        try {
            TaskSummary taskSummary = tasksRemote.getTaskSummaryRecord(Long.valueOf(taskDTO.getTaskId()));
            TaskEditDTO taskEditDTO = transformTaskToTaskEditDTO(taskSummary);


            value.setPayload(taskEditDTO);
        } catch (ApplicationException e) {
            value.setPayload(null);
            value.setError(BaseEntityException.NO_UPDATE_PERFORMED);
        }
        log.debug(" TasksService.getTasksAttributes() ::: Method Return list size of {} ", value.getPayload().getGroups().size());

        // TODO -- Drools Job (Remove when possible)
        List<TaskGroupDTO> taskGroups = value.getPayload().getGroups();
        for (TaskGroupDTO taskGroup : taskGroups) {
            if (taskGroup.getGroupName().equals("Application Attributes")) {
                List<TaskAttributeDTO> taskAttributes = taskGroup.getAttributes();

                for (TaskAttributeDTO taskAttribute : taskAttributes) {
                    if (taskAttribute.getFieldLabel().equals("Is Monitored")) {
                        long operators_groups =
                                groupEjb.getUserGroups()
                                        .stream()
                                        .filter((group -> group.getDescription().contains("operators")))
                                        .count();

                        if (operators_groups == 0) {
                            taskAttribute.setEditable(false);
                        }
                    }
                }
            }
        }

        return value;
    }

    @Path("/update")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<List<TaskDTO>> updateAction(TaskEditDTO taskEdit) throws NamingException {
        log.debug(" TasksService.updateAction method entered with input {} ", taskEdit);

        // Required for Auditing
        List<TaskAttributeDTO> taskAttributesBeforeUpdate = getTaskAttributes(taskEdit.getTaskId());
        Group taskGroupBeforeUpdate = null;

        BaseEntity<List<TaskDTO>> response = new BaseEntity<>();
        try {
            Task task = TransformationService.transformTaskEditDTOToTask(taskEdit);
            taskGroupBeforeUpdate = task.getUserGroup();
            TaskEditDTO.ACTION_STATUS action = taskEdit.getSelectedAction();

            if (isReassignTaskAction(action)) {
                response = generateReassignTaskResponse(task, taskEdit);
                sendReassignmentMail(taskEdit);
            } else {
                response = generateNormalTaskResponse(task, action, taskEdit);
            }
        } catch (ApplicationException e) {
            response.setHeader(ResponseHelper.constructHeader(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage()));
            response.setError(BaseEntityException.NO_UPDATE_PERFORMED);
        }
        log.debug(" TasksService.updateAction method returned  {} ", response);

        List<TaskAttributeDTO> taskAttributesAfterUpdate = getTaskAttributesFromGroupAttributes(taskEdit.getGroups());
        generateAuditing(taskEdit, taskGroupBeforeUpdate, taskAttributesBeforeUpdate, taskAttributesAfterUpdate);

        return response;
    }

    @Path("/getReports")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<TaskReportingResultDTO> getReports(TaskReportingDTO taskReportingDTO) throws NamingException {
        log.debug(" TasksService.getReports :: Entered Method for report search criteria {} ", taskReportingDTO);

        BaseEntity<TaskReportingResultDTO> value = new BaseEntity<>();
        //TODO: Add Jackson date deserializer
        List<Node> nodeList = nodeRemote.getNodesBy(taskReportingDTO.getTenants(), null, parseStringToDate(taskReportingDTO.getFromDate()), parseStringToDate(taskReportingDTO.getToDate()));
        TaskReportingResultDTO taskReports = transformTaskToReports(getAllTasksFromNode(nodeList), taskReportingDTO);
//        filterTaskReportsByTenant(taskReports, taskReportingDTO.getTenant());

        value.setPayload(taskReports);

        log.debug(" TasksService.getReports :: Method Returned  {} ", value.getPayload());

        return value;
    }

    @POST
    @Path("/exportReports")
    @Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    @Consumes({MediaType.APPLICATION_JSON})
    public byte[] exportReports(final TaskReportingDTO taskReportingDTO) throws NamingException {
        log.debug("TasksService.exportReports ::: Entered with input parameters {}", taskReportingDTO);

        List<Node> nodeList = nodeRemote.getNodesBy(taskReportingDTO.getTenants(), null, parseStringToDate(taskReportingDTO.getFromDate()), parseStringToDate(taskReportingDTO.getToDate()));
        TaskReportingResultDTO taskReports = transformTaskToReports(getAllTasksFromNode(nodeList), taskReportingDTO);
//        filterTaskReportsByTenant(taskReports, taskReportingDTO.getTenant());

        byte[] exportFile = exportExcelService.exportTaskReport(taskReports);

        log.debug("TasksService.exportReports ::: Returning export task reports file.");
        return exportFile;
    }

    private void generateAuditing(TaskEditDTO taskEdit, Group taskGroupBeforeUpdate, List<TaskAttributeDTO> beforeUpdateAttributes, List<TaskAttributeDTO> afterUpdateAttributes) throws NamingException {
        beforeUpdateAttributes = filterAttributesBeforePersist(getChangedAttributes(beforeUpdateAttributes, afterUpdateAttributes), beforeUpdateAttributes);
        afterUpdateAttributes = filterAttributesBeforePersist(getChangedAttributes(beforeUpdateAttributes, afterUpdateAttributes), afterUpdateAttributes);

        log.debug("TasksService :: generateAuditing :: Method Entered for Task Id {}, Node Id {}", taskEdit.getTaskId(), taskEdit.getNodeId());

        String userGroup = taskGroupBeforeUpdate != null ? taskGroupBeforeUpdate.getDescription() : "-";
        nodeRemote.createNodeAuditingReport(taskEdit, userGroup, beforeUpdateAttributes, afterUpdateAttributes);

        log.debug("TasksService :: generateAuditing :: Method created Node Auditing Report");

    }

    private void filterTaskReportsByTenant(TaskReportingResultDTO taskReports, String tenant) {
        if (!"ALL".equals(tenant)) {
            for (TaskReportingResultsDTO taskReportResult : taskReports.getTaskReportingResultsDTOList()) {
                List<TaskReportDTO> filteredTaskReports =
                        taskReportResult.getTaskReportDTOS()
                                .stream()
                                .filter(task -> task.getOwnerTenant().equals(tenant))
                                .collect(Collectors.toList());

                taskReportResult.setTaskReportDTOS(filteredTaskReports);
            }
        }
    }

    private List<TaskAttributeDTO> filterAttributesBeforePersist(List<String> changedAttributes, List<TaskAttributeDTO> attributesList) {
        return attributesList.stream()
                .filter((attr) -> changedAttributes.stream().anyMatch((changedAttr) -> attr.getFieldName().equals(changedAttr))).collect(Collectors.toList());
    }

    private List<String> getChangedAttributes(List<TaskAttributeDTO> beforeUpdateAttributes, List<TaskAttributeDTO> afterUpdateAttributes) {
        log.debug("TasksService :: getChangedAttributes :: Method Entered to filter changed Attributes");
        List<String> changedAttributes;

        changedAttributes = beforeUpdateAttributes.stream()
                .filter((beforeAttribute) ->
                        afterUpdateAttributes.stream()
                                .anyMatch((afterAttribute) -> afterAttribute.getFieldName().equals(beforeAttribute.getFieldName()) && !afterAttribute.getFieldValue().equals(beforeAttribute.getFieldValue()))
                )
                .map(filteredAttribute -> filteredAttribute.getFieldName())
                .collect(Collectors.toList());

        log.debug("TasksService :: getChangedAttributes :: Method Filtered changed Attributes");
        return changedAttributes;
    }

    private List<TaskAttributeDTO> getTaskAttributesFromGroupAttributes(List<TaskGroupDTO> groups) {
        List<TaskAttributeDTO> taskAttributes = new ArrayList<>();

        groups.stream()
                .forEach(group -> taskAttributes.addAll(group.getAttributes()));

        return taskAttributes;
    }

    private List<TaskAttributeDTO> getTaskAttributes(String taskId) throws NamingException {
        log.debug("TasksService :: getTaskAttributes :: Method Entered for Task Id {}", taskId);

        TaskSummary taskSummary = tasksRemote.getTaskSummaryRecord(Long.valueOf(taskId));
        List<TaskAttributeDTO> taskAttributes = TransformationService.transformCiAttributeListToTaskAttributeDTOList(taskSummary.getTask().getAttributes());

        log.debug("TasksService :: getTaskAttributes :: Method Returned Task Attributes {}", taskAttributes);
        return taskAttributes;
    }


    private boolean isReassignTaskAction(TaskEditDTO.ACTION_STATUS action) {
        return TaskEditDTO.ACTION_STATUS.Reassign.equals(action);
    }

    private BaseEntity<List<TaskDTO>> generateNormalTaskResponse(Task task, TaskEditDTO.ACTION_STATUS action, TaskEditDTO taskEdit) throws NamingException {
        BaseEntity<List<TaskDTO>> response = new BaseEntity<>();

        InputCommandEnum inputCommandEnum = transformCommand(action);
        tasksRemote.evaluateTaskAction(task, inputCommandEnum);

        List<TaskSummary> taskSummary = tasksRemote.getTaskSummaryRecordsByNodeId(Long.parseLong(taskEdit.getNodeId()));
        taskSummary = filterUpdatedTasksBasedOnUserGroups(taskSummary);

        List<TaskDTO> taskDTOs = null;
        if (taskSummary == null)
            response.setPayload(null);
        else {
            taskDTOs = taskSummary
                    .stream()
                    .map((taskRow) -> TransformationService.transformTaskSummaryToDTO(taskRow))
                    .collect(Collectors.toList());

            response.setPayload(taskDTOs);
        }

        response.setHeader(ResponseHelper.constructHeader(Response.Status.OK, generateMessagedByAction(taskEdit.getSelectedAction())));
        return response;
    }

    private BaseEntity<List<TaskDTO>> generateReassignTaskResponse(Task task, TaskEditDTO taskEdit) throws NamingException {
        BaseEntity<List<TaskDTO>> response = new BaseEntity<>();

        Task reassignedTask = tasksRemote.reassignTask(task, taskEdit.getReassignTask().getGroupId(), taskEdit.getReassignTask().getReassignReason());
        List<TaskSummary> taskSummary = new ArrayList<>();

        if (reassignedTask != null)
            taskSummary.add(tasksRemote.getTaskSummaryRecord(reassignedTask.getId()));

        if (!isTaskAccessibleToActiveUser(reassignedTask)) {
            taskSummary.clear();
        }

//        response.setPayload((taskSummary.size() != 0) ? TransformationService.transformTaskSummaryToDTO(taskSummary) : taskSummary);
        response.setPayload(TransformationService.transformTaskSummaryToDTO(taskSummary));
        response.setHeader(ResponseHelper.constructHeader(Response.Status.OK, generateMessagedByAction(taskEdit.getSelectedAction())));

        return response;
    }

    private void sendReassignmentMail(TaskEditDTO taskEdit) {
        try {
            Group reAssignedGroup = groupEjb.getGroupByID(taskEdit.getReassignTask().getGroupId());
            String nodeDisplayLabel = taskEdit.getHeader().getTitle();
            String reAssignmentComments = taskEdit.getReassignTask().getReassignReason();


            String title = "";
            String message = "Task for node " + nodeDisplayLabel + " is reassigned to Group " + reAssignedGroup.getDescription();
            String comments = !reAssignmentComments.isEmpty() ? reAssignmentComments : null;

            MailHelper.sendHTMLEmail(title, message, comments, reAssignedGroup.getEmail(), null);
        } catch (ApplicationException ex) {
            log.error("TasksService :: sendReassignmentMail :: ERROR {}", ex);
        }
    }

    private List<TaskSummary> filterUpdatedTasksBasedOnUserGroups(List<TaskSummary> taskSummaryList) throws NamingException {
        log.debug("TasksService :: filterUpdatedTasksBasedOnUserGroups :: Method Entered for Input Tasks List {}", taskSummaryList);

        List<TaskSummary> filteredTaskSummaryList = null;

        // Get User Groups Ids
        List<Integer> userGroupsIds = groupEjb.getUserGroups()
                .stream()
                .map(Group::getId)
                .collect(Collectors.toList());

        // Return TaskSummary List filtered by User Groups
        filteredTaskSummaryList =
                taskSummaryList
                        .stream()
                        .filter((taskItem) -> userGroupsIds
                                .stream()
                                .anyMatch((userGroup) -> userGroup.equals(taskItem.getGroup().getId())))
                        .collect(Collectors.toList());

        log.debug("TasksService :: filterUpdatedTasksBasedOnUserGroups :: Method Returned filtered Tasks List {}", filteredTaskSummaryList);

        return filteredTaskSummaryList;
    }

    private boolean isTaskAccessibleToActiveUser(Task task) throws NamingException {
        List<Group> activeUserGroups = groupEjb.getUserGroups();

        return activeUserGroups.stream()
                .anyMatch(g -> g.getId() == task.getUserGroup().getId());
    }

    private List<Task> getAllTasksFromNode(List<Node> nodeList) {
        List<Task> taskList = new ArrayList<>();
        nodeList.forEach(n -> {
            if ((n.getTasksWrapper() != null)) {
                n.getTasksWrapper().getTasks(n.getId())
                        .forEach(t -> t.setProcessId("" + n.getId()));

                taskList.addAll(n.getTasksWrapper().getTasks(n.getId()));
            }
        });

        return taskList;
    }

    private InputCommandEnum transformCommand(TaskEditDTO.ACTION_STATUS selectedAction) {
        if (TaskEditDTO.ACTION_STATUS.Submit.equals(selectedAction)) {
            return InputCommandEnum.SUBMIT_TASK_COMMAND;
        } else if (TaskEditDTO.ACTION_STATUS.Reassign.equals(selectedAction)) {
            return InputCommandEnum.REJECT_TASK_COMMAND;
        } else if (TaskEditDTO.ACTION_STATUS.Save.equals(selectedAction)) {
            return InputCommandEnum.SAVE_TASK_COMMAND;
        } else {
            return InputCommandEnum.IGNORE_TASK_COMMAND;
        }
    }

    private List<TaskDTO> getUserTasks(List<Group> groups) throws NamingException {
        List<TaskSummary> taskSummaryList = tasksRemote.getTaskSummaryRecords(groups);

        return TransformationService.transformTaskSummaryToDTO(taskSummaryList);
    }

    public TaskEditDTO transformTaskToTaskEditDTO(TaskSummary taskSummary) throws NamingException {
        log.debug("TasksEjb.transformTaskToTaskEditDTO() ::: Method entered with input {}", taskSummary);

        TaskEditDTO taskEditDTO = new TaskEditDTO();
        taskEditDTO.setTaskId("" + taskSummary.getTask().getId());
        taskEditDTO.setNodeId("" + taskSummary.getNode().getId());
        taskEditDTO.setNodeGlobalId("" + taskSummary.getNode().getGlobalId());
        taskEditDTO.setAssignedDate(DateHelper.dateTimeToString(taskSummary.getTask().getAssignedDate()));
        taskEditDTO.setCiType(taskSummary.getTask().getCiType());
        taskEditDTO.setOsType(taskSummary.getOsType());
        taskEditDTO.setCompletionDate(taskSummary.getTask().getCompletionDate());
        taskEditDTO.setTaskStatus(taskSummary.getTask().getStatus().name());
        taskEditDTO.setCurrentAssignee(taskSummary.getAssignedTo());
        taskEditDTO.setMonitored(taskSummary.getTask().isMonitored());
        taskEditDTO.setCurrentStage(taskSummary.getTask().getCurrentStage());

        TaskHeaderDTO taskHeaderDTO = new TaskHeaderDTO();
        taskHeaderDTO.setTitle(taskSummary.getDisplayLabel());
        taskHeaderDTO.setAttributes(filterHeaderCIAttributes(taskSummary.getTask()));
        taskHeaderDTO.setMessage(ApplicationProperties.getInstance().getProperty("WORKFLOW_MODAL_HEADER_MESSAGE"));
        taskEditDTO.setHeader(taskHeaderDTO);

        taskEditDTO.setGroups(transformAttributeToTaskGroupDTO(taskSummary.getTask().getAttributes()));
        taskEditDTO.setSupportGroup(getSupportGroupFromAttributes(taskSummary.getTask().getAttributes()));

        log.debug("TasksEjb.transformTaskToTaskEditDTO() ::: Method returned transformed taskEditDTO {}", taskEditDTO);

        return taskEditDTO;
    }

    private List<TaskGroupDTO> transformAttributeToTaskGroupDTO(List<CIAttribute> attributes) {
        Map<String, TaskGroupDTO> map = findUniqueTaskGroupDTOs(attributes);
        map.remove("Header Attributes");
        map.remove("Support Attributes");
        attributes.forEach(a -> {
            TaskGroupDTO taskGroupDTO = map.get(a.getGroupName());
            if (taskGroupDTO != null) // bypass Header attributes which will return null
                taskGroupDTO.getAttributes().add(transformAttributeToAttributeDTO(a));
        });

        return getAllGroupsFromMap(map);
    }

    private TaskGroupDTO getSupportGroupFromAttributes(List<CIAttribute> attributes) {
        Map<String, TaskGroupDTO> map = findUniqueTaskGroupDTOs(attributes);

        TaskGroupDTO supportGroup = map.get("Support Attributes");
        List<CIAttribute> supportAttributes =
                attributes
                        .stream()
                        .filter(attr -> attr.getGroupName().equals("Support Attributes"))
                        .collect(Collectors.toList());

        supportAttributes.forEach(sa -> {
            supportGroup.getAttributes().add(transformAttributeToAttributeDTO(sa));
        });

        return supportGroup;
    }

    private List<TaskAttributeDTO> filterHeaderCIAttributes(Task task) {
        List<TaskAttributeDTO> response = new ArrayList<>();
        response.addAll(addTaskSummaryFieldsInHeader(task));

        List<CIAttribute> collect = task.getAttributes().stream()
                .filter(a -> a.getGroupName().equals("Header Attributes"))
                .collect(Collectors.toList());

        collect.forEach(a -> {
            response.add(transformAttributeToAttributeDTO(a));
        });

        return response;
    }

    private Collection<? extends TaskAttributeDTO> addTaskSummaryFieldsInHeader(Task task) {
        List<TaskAttributeDTO> taskHeaderAttributes = new ArrayList<>();

        // Add ProcessId as Task Header Attributes
        taskHeaderAttributes.add(transformAttributeToAttributeDTO(
                new CIAttribute("Process ID", "Process ID", task.getProcessId(), "TEXT", null, false, false, false, 0)));

        // Add Assigned Date as Task Header Attributes
        String formattedAssignedDate = new SimpleDateFormat(ApplicationProperties.getInstance().getProperty("GUI_DATE_FORMAT"), Locale.ENGLISH).format(task.getAssignedDate());
        taskHeaderAttributes.add(transformAttributeToAttributeDTO(
                new CIAttribute("Assigned Date", "Assigned Date", formattedAssignedDate, "DATE", null, false, false, false, 0)));

        return taskHeaderAttributes;
    }

    private Map<String, TaskGroupDTO> findUniqueTaskGroupDTOs(List<CIAttribute> attributes) {
        Map<String, TaskGroupDTO> map = new HashMap<>();

        attributes.forEach(a -> {
            TaskGroupDTO taskGroupDTO = new TaskGroupDTO();
            taskGroupDTO.setGroupName(a.getGroupName());
            taskGroupDTO.setGroupPriority(a.getGroupPriority());
            taskGroupDTO.setAttributes(new ArrayList<>());

            map.put(a.getGroupName(), taskGroupDTO);
        });

        return map;
    }

    private List<TaskGroupDTO> getAllGroupsFromMap(Map<String, TaskGroupDTO> map) {
        return map
                .entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }

    private TaskAttributeDTO transformAttributeToAttributeDTO(CIAttribute ciAttribute) {
        TaskAttributeDTO.FIELDTYPE attributeType = transformStringToFieldType(ciAttribute.getFieldType());

        TaskAttributeDTO taskAttributeDTO = new TaskAttributeDTO();
        taskAttributeDTO.setEditable(ciAttribute.isEditable());
        taskAttributeDTO.setMandatory(ciAttribute.isMandatory());
        taskAttributeDTO.setFieldLabel(ciAttribute.getFieldLabel());
        taskAttributeDTO.setFieldLOV(ciAttribute.getFieldLovs());
        taskAttributeDTO.setFieldName(ciAttribute.getFieldName());
        taskAttributeDTO.setFieldType(attributeType);
        taskAttributeDTO.setFieldValue(
                attributeType == TaskAttributeDTO.FIELDTYPE.date ? formatAttributeValueForDateTypes(ciAttribute.getFieldValue()) : ciAttribute.getFieldValue()
        );
        taskAttributeDTO.setAttributePriority(ciAttribute.getGroupPriority());

        return taskAttributeDTO;
    }

    private String formatAttributeValueForDateTypes(String ciAttributeValue) {
        String dateValue;
        try {
            // Convert String to Date Object
            Instant instant = Instant.parse(ciAttributeValue);
            Date attributeDate = new Date(instant.toEpochMilli());

            dateValue = new SimpleDateFormat(ApplicationProperties.getInstance().getProperty("GUI_DATE_FORMAT"), Locale.ENGLISH).format(attributeDate);
        } catch (Exception ex) {
            dateValue = ciAttributeValue;
        }

        return dateValue;
    }

    private TaskAttributeDTO.FIELDTYPE transformStringToFieldType(String type) {
        if ("TEXT".equals(type))
            return TaskAttributeDTO.FIELDTYPE.text;
        else if ("TEXT_LIST".equals(type))
            return TaskAttributeDTO.FIELDTYPE.dropdown;
        else if ("LIST_VALUES".equals(type))
            return TaskAttributeDTO.FIELDTYPE.dropdown_multiple;
        else if ("BOOLEAN".equals(type))
            return TaskAttributeDTO.FIELDTYPE.checkbox;
        else if ("LIST".equals(type))//TODO: HANDLE List attributes in UI
            return TaskAttributeDTO.FIELDTYPE.list;
        else if ("NUMBER".equals(type))//TODO: HANDLE Numeric attributes in UI
            return TaskAttributeDTO.FIELDTYPE.numeric;
        else if ("DATE".equals(type))//TODO: HANDLE Date attributes in UI
            return TaskAttributeDTO.FIELDTYPE.date;
        else
            return null;
    }

    private TaskReportingResultDTO transformTaskToReports(List<Task> taskList, TaskReportingDTO taskReportingDTO) {
        TaskReportingResultDTO taskReportingResultDTO = new TaskReportingResultDTO();

        Date fromDate = parseStringToDate(taskReportingDTO.getFromDate());
        Date toDate = parseStringToDate(taskReportingDTO.getToDate());
        List<TaskStatusEnum> taskStatusEnumList = transformToTaskStatusEnum(taskReportingDTO.getTaskStatus());

        List<Task> filteredTaskList = taskList.stream()
                .filter(t -> taskStatusEnumList.contains(t.getStatus()))
                .filter(t -> {
                    if (taskReportingDTO.getTaskGroup() == null) return true;

                    if (t.getUserGroup() != null) {
                        if (taskReportingDTO.getTaskGroup().equals(t.getUserGroup().getDescription())) return true;
                    }

                    return false;
                })
                .collect(Collectors.toList());

        filteredTaskList = filterTaskAfterDate(filteredTaskList, fromDate);
        filteredTaskList = filterTaskBeforeDate(filteredTaskList, toDate);

        taskReportingResultDTO.setTaskReportingResultsDTOList(transformToTaskReportResults(filteredTaskList));
        taskReportingResultDTO.setTotalTasks(filteredTaskList.size());

        return taskReportingResultDTO;
    }

    private List<TaskReportingResultsDTO> transformToTaskReportResults(List<Task> taskList) {
        List<Group> distinctGroups = getDistinctGroups(taskList);
        List<TaskReportingResultsDTO> taskReportingResultsDTOS = initTaskReportResults(distinctGroups);

        setTaskSummaryPerGroup(taskReportingResultsDTOS, taskList);

        return taskReportingResultsDTOS;
    }

    private void setTaskSummaryPerGroup(List<TaskReportingResultsDTO> taskReportingResultsDTOS, List<Task> taskList) {
        taskReportingResultsDTOS.forEach(tr -> {
            List<TaskReportDTO> taskDtoPerGroup = taskList.stream()
                    .filter(t -> tr.getGroupId() == t.getUserGroup().getId())
                    .map(TransformationService::transformTaskToReportDTO)
                    .collect(Collectors.toList());

            tr.setTasksAssigned((taskDtoPerGroup != null) ? taskDtoPerGroup.size() : 0);
            tr.setTaskReportDTOS(taskDtoPerGroup);
        });
    }

    private List<TaskReportingResultsDTO> initTaskReportResults(List<Group> groupList) {
        return groupList.stream()
                .map(g -> {
                    TaskReportingResultsDTO taskReportingResultsDTO = new TaskReportingResultsDTO();
                    taskReportingResultsDTO.setGroupId(g.getId());
                    taskReportingResultsDTO.setGroupName(g.getDescription());

                    return taskReportingResultsDTO;
                })
                .collect(Collectors.toList());
    }

    private List<Group> getDistinctGroups(List<Task> taskList) {
        return taskList.stream()
                .map(Task::getUserGroup)
                .filter(distinctByKey(Group::getId))
                .collect(Collectors.toList());
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    private List<Task> filterTaskBeforeDate(List<Task> taskList, Date date) {
        if (date != null) {
            taskList = taskList.stream()
                    .filter(t -> t.getAssignedDate() != null)
                    .filter(t -> t.getAssignedDate().before(date))
                    .collect(Collectors.toList());
        }
        return taskList;
    }

    private List<Task> filterTaskAfterDate(List<Task> taskList, Date date) {
        if (date != null) {
            // Transform Date in order to Include "From" date also
            Date transformedDate = addADayInDate(date);

            taskList = taskList.stream()
                    .filter(t -> t.getAssignedDate() != null)
                    .filter(t -> t.getAssignedDate().after(transformedDate))
                    .collect(Collectors.toList());
        }

        return taskList;
    }

    private Date addADayInDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -1);

        return cal.getTime();
    }

    private List<TaskStatusEnum> transformToTaskStatusEnum(TaskReportingDTO.TASK_STATUS taskStatus) {
        if (TaskReportingDTO.TASK_STATUS.ALL.equals(taskStatus)) {
            return Arrays.asList(TaskStatusEnum.IN_PROGRESS, TaskStatusEnum.INITIATED, TaskStatusEnum.OPEN, TaskStatusEnum.CANCELED, TaskStatusEnum.COMPLETED);
        } else if (TaskReportingDTO.TASK_STATUS.OPEN.equals(taskStatus)) {
            return Arrays.asList(TaskStatusEnum.OPEN);
        } else if (TaskReportingDTO.TASK_STATUS.IN_PROGRESS.equals(taskStatus)) {
            return Arrays.asList(TaskStatusEnum.IN_PROGRESS, TaskStatusEnum.INITIATED);
        } else if (TaskReportingDTO.TASK_STATUS.CANCELED.equals(taskStatus)) {
            return Arrays.asList(TaskStatusEnum.CANCELED);
        } else if (TaskReportingDTO.TASK_STATUS.COMPLETED.equals(taskStatus)) {
            return Arrays.asList(TaskStatusEnum.COMPLETED);
        }
        return null;
    }

    private Date parseStringToDate(String date) {
        Date result = null;

        if (StringUtils.isEmpty(date))
            return null;

        try {
            long longDate = Long.parseLong(date);
            result = new Date(longDate);
        } catch (NumberFormatException nfe) {
            log.error(String.format("NodeService.parseStringToDate ::: Error parsing string %s to date.", date));
        }
        return result;
    }

    private String generateMessagedByAction(TaskEditDTO.ACTION_STATUS actionStatus) {
        if (TaskEditDTO.ACTION_STATUS.Save.equals(actionStatus)) {
            return "Record saved successfully";
        } else if (TaskEditDTO.ACTION_STATUS.Reassign.equals(actionStatus)) {
            return "Record was reassigned";
        } else {
            return "UCMDB updated successfully";
        }

    }
}
