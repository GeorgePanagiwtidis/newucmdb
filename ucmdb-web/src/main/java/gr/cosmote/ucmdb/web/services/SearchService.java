package gr.cosmote.ucmdb.web.services;

import gr.cosmote.ucmdb.commons.exceptions.ApplicationException;
import gr.cosmote.ucmdb.ejb.SearchRemote;
import gr.cosmote.ucmdb.models.base.BusinessSearchCriteria;
import gr.cosmote.ucmdb.models.base.CIsOfType;
import gr.cosmote.ucmdb.models.base.Ci;
import gr.cosmote.ucmdb.models.base.GenericCI;
import gr.cosmote.ucmdb.models.base.NameCodeObject;
import gr.cosmote.ucmdb.models.database.SavedSearches;
import gr.cosmote.ucmdb.web.entity.BaseEntity;
import gr.cosmote.ucmdb.web.entity.BaseEntityException;
import gr.cosmote.ucmdb.web.helper.EjbHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;

import javax.ejb.EJB;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

@Slf4j
@Path("/secure/searches")
public class SearchService {

    @EJB
    private SearchRemote searchRemote;

    @Path("/getSearches")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<List<SavedSearches>> getSearches() {
        log.debug("SearchService :: Method Entered");
        BaseEntity<List<SavedSearches>> searches = new BaseEntity<>();

        try {
            searches.setPayload(searchRemote.getSearches());
        }
        catch (Exception ex) {
            log.error("SearchService :: getSearches :: ERROR {}", ex);
            return new BaseEntity<>(BaseEntityException.NO_SEARCHES_FOUND);
        }

        log.debug("SearchService :: Method Returned {} Saved Searches", searches.getPayload().size());
        return searches;
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @Path("/insertSearch")
    public BaseEntity<SavedSearches> insertSearch(SavedSearches savedSearches) {
        log.debug("SearchService.insertSearch ::: Entered");

        try{
            final SavedSearches insert = searchRemote.ucmdbInsertSearch(savedSearches);
            if (insert == null) {
                return new BaseEntity<>();
            }
            final BaseEntity<SavedSearches> value = new BaseEntity<SavedSearches>(insert);
            log.debug("SearchService.insertSearch ::: Return {}", value);
            return value;
        } catch (Exception e){
            log.error("SearchService.insertSearch ::: Error in insertSearch method",e);

            return new BaseEntity<>(BaseEntityException.NO_SEARCHES_INSERTED);
        }
    }

    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @Path("/removeSearch/{searchId}")
    public BaseEntity<Boolean> removeSearch(@PathParam("searchId") int searchId) throws NamingException {
        log.debug("SearchService :: removeSearch ::: Method Entered");
        BaseEntity<Boolean> removeSearchResponse = new BaseEntity<>();

        try {
            removeSearchResponse.setPayload(searchRemote.ucmdbRemoveSearch(searchId));
        }
        catch (ApplicationException ex) {
            log.error("SearchService :: removeSearch ::: ERROR {}", ex);

            removeSearchResponse.setPayload(false);
            return removeSearchResponse;
        }

        log.debug("SearchService :: removeSearch :: Method Returned");
        return removeSearchResponse;
    }
}