package gr.cosmote.ucmdb.web.entity;

public final class BaseEntityException extends ExceptionHolder {
    //TODO: Move to Angular Translate message.json
    public static final BaseEntityException NO_TENANTS_FOUND = new BaseEntityException("1000", "Δεν βρέθηκαν tenants");
    public static final BaseEntityException NO_CIS_FOUND = new BaseEntityException("1001", "Δεν βρέθηκαν CIs");
    public static final BaseEntityException NO_SIMPLE_CIS_FOUND = new BaseEntityException("1002", "Δεν βρέθηκαν Simple CIs");
    public static final BaseEntityException NO_USER_FOUND = new BaseEntityException("1003", "Δεν βρέθηκε ο χρήστης");
    public static final BaseEntityException NO_GROUPS_FOUND = new BaseEntityException("1004", "Δεν βρέθηκαν Groups");
    public static final BaseEntityException NO_GROUPS_INSERTED = new BaseEntityException("1005", "Η εισαγωγή του Group απέτυχε");
    public static final BaseEntityException NO_GROUPS_DELETED = new BaseEntityException("1006", "Η διαγραφή Group απέτυχε");
    public static final BaseEntityException NO_SEARCHES_FOUND = new BaseEntityException("1007", "Δεν βρέθηκαν αποθηκευμένες αναζητήσεις");
    public static final BaseEntityException NO_SEARCHES_INSERTED = new BaseEntityException("1010", "Δεν εισήχθηκε η αναζήτηση");
    public static final BaseEntityException NO_SEARCHES_REMOVED = new BaseEntityException("1011", "Δεν διεγράφηκε η αναζήτηση");
    public static final BaseEntityException NO_USER_INACTIVE = new BaseEntityException("1012", "Ο χρήστης δεν έγινε ανενεργός");
    public static final BaseEntityException NO_GROUP_INACTIVE = new BaseEntityException("1013", "To group δεν έγινε ανενεργό");
    public static final BaseEntityException NO_TENANTS_INSERTED = new BaseEntityException("1014", "Oι tenats δεν προστέθηκαν");
    public static final BaseEntityException NO_UPDATE_PERFORMED = new BaseEntityException("1015", "Error occurred - No record is updated");
    public static final BaseEntityException NO_TASK_ATTRIBUTES = new BaseEntityException("1016", "Δεν βρέθηκαν Attributes για το συγκεκριμένο Task");
    public static final BaseEntityException NO_SEARCH_TYPES = new BaseEntityException("1017", "No Search Types found");
    public static final BaseEntityException NO_VIEW_QUERIES = new BaseEntityException("1018", "No View Queries found");
    public static final BaseEntityException QUERY_EXECUTION_ERROR = new BaseEntityException("1019", "An error occurred while generating Query Response");
    public static final BaseEntityException NO_VIEW_QUERY_PROPERTIES = new BaseEntityException("1020", "No View Queries Properties found");
    public static final BaseEntityException NO_UPDATE_PERFORMED_DUPLICATE_RECORD = new BaseEntityException("1021", "Duplicate Record found");


    private BaseEntityException(final String errorCode, final String message) {
        super(errorCode, message);
    }

}
