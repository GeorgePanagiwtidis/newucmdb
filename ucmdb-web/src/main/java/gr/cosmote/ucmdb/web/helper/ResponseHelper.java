package gr.cosmote.ucmdb.web.helper;

import gr.cosmote.ucmdb.web.entity.JsonHeader;

import javax.ws.rs.core.Response;

/**
 * Created by Intrasoft Intl - dasimakopoulos on 27/8/2017.
 */
public class ResponseHelper {

    public static JsonHeader constructHeader(Response.Status status, String info){
        JsonHeader header = new JsonHeader();
        header.setStatus(status);
        header.setInfo(info);
        return header;
    }
}
