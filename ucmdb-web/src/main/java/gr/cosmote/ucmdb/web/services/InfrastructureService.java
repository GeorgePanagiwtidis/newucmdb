package gr.cosmote.ucmdb.web.services;

import gr.cosmote.ucmdb.commons.exceptions.ApplicationException;
import gr.cosmote.ucmdb.ejb.InfrastructureRemote;
import gr.cosmote.ucmdb.models.base.AdvancedCriteria;
import gr.cosmote.ucmdb.models.base.Infrastructure;
import gr.cosmote.ucmdb.models.database.InfrastructureCiTypes;
import gr.cosmote.ucmdb.models.database.InfrastructureCustomAttributes;
import gr.cosmote.ucmdb.web.entity.BaseEntity;
import gr.cosmote.ucmdb.web.entity.BaseEntityException;
import gr.cosmote.ucmdb.web.helper.EjbHelper;
import lombok.extern.slf4j.Slf4j;

import javax.ejb.EJB;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/infrastructure")
@Slf4j
public class InfrastructureService {


    @EJB
    private InfrastructureRemote infrastructureRemote;

    @Path("/clearCache")
    @GET
    public Response clearInfrastructureCache() throws NamingException {
        log.debug("InfrastructureEjb :: clearInfrastructureCache :: Method Entered in order to clear Infrastructure Cache");

        try {
            infrastructureRemote.clearCache();
        }
        catch (Exception ex) {
            log.error("InfrastructureEjb :: clearInfrastructureCache :: ERROR in method {}", ex);

            return Response.serverError().build();
        }

        log.debug("InfrastructureEjb :: clearInfrastructureCache :: Method Successfully cleared Infrastructure Cache");

        return Response.ok().build();
    }

    @Path("/clearCacheForSpecificRequest")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response clearInfrastructureCacheForSpecificRequest(AdvancedCriteria searchCriteria) throws NamingException {
        log.debug("InfrastructureEjb :: clearInfrastructureCacheForSpecificRequest :: Method Entered for search criteria {}", searchCriteria);

        try {
            infrastructureRemote.clearCacheForSpecificRequest(searchCriteria);
        }
        catch (ApplicationException ex) {
            log.error("InfrastructureEjb :: clearInfrastructureCacheForSpecificRequest :: ERROR in method {}", ex);

            return Response.serverError().build();
        }

        log.debug("InfrastructureEjb :: clearInfrastructureCacheForSpecificRequest :: Method Successfully cleared Infrastructure Cache");

        return Response.ok().build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public BaseEntity<List<Infrastructure>> searchForInfraService(final AdvancedCriteria search) {

        log.debug("InfrastructureService :: searchForInfraService ::: Entered with searchCriteria {} ", search);
        try{

            List<Infrastructure> response = infrastructureRemote.infrastructureSearch(search);

            log.debug("InfrastructureService :: searchForInfraService ::: Returning {} infra elements.",response.size());

            return new BaseEntity<>(response);

        } catch (Exception e){
            log.error("InfrastructureService :: searchForInfraService ::: ERROR",e);
            return new BaseEntity<List<Infrastructure>>(BaseEntityException.NO_SIMPLE_CIS_FOUND);
        }
    }

    @Path("/getInfrastructureTypes")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<List<InfrastructureCiTypes>> getInfrastructureTypes() throws NamingException {
        BaseEntity<List<InfrastructureCiTypes>> infrastructureTypes = new BaseEntity<>();
        log.debug("InfrastructureService :: getInfrastructureTypes :: Method Entered");

        infrastructureTypes.setPayload(infrastructureRemote.getInfrastructureTypes());

        log.debug("InfrastructureService :: getInfrastructureTypes :: Method Returned List of Infra Types size of {}", infrastructureTypes.getPayload().size());
        return infrastructureTypes;
    }


    @Path("/getCustomAttributes")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<List<InfrastructureCustomAttributes>> getCustomAttributes(@QueryParam("typeId") String id) throws NamingException {
        log.debug("InfrastructureService:: getCustomAttributes :: Method Entered");

        BaseEntity<List<InfrastructureCustomAttributes>> nodeAttributes = new BaseEntity();

        int typeId;
        try {
            typeId = Integer.valueOf(id);
        }
        catch (NumberFormatException ex) {
            log.error("InfrastructureService:: getCustomAttributes :: Could not convert path param to Integer Value");

            return new BaseEntity<>(BaseEntityException.NO_SIMPLE_CIS_FOUND);
        }

        nodeAttributes.setPayload(infrastructureRemote.getCustomAttributesByType(typeId));

        log.debug("InfrastructureService:: getCustomAttributes :: Method Returned {}", nodeAttributes);

        return nodeAttributes;
    }

    @Path("exportToDb")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response exportComputersToExcel() {
        log.debug("InfrastructureService.exportComputersToExcel :: Method Entered in Order to Export Infrastructure Computers to Database");

        try {
            infrastructureRemote.exportInfrastructureComputersToDatabase();
        }
        catch (Exception ex) {
            log.error("InfrastructureService.exportComputersToExcel :: Error", ex);

            return Response.serverError().entity("INFRASTRUCTURE EXPORT FAILED").build();
        }

        log.debug("InfrastructureService.exportComputersToExcel :: Export Completed");
        return Response.ok("INFRASTRUCTURE EXPORT COMPLETED").build();
    }
}
