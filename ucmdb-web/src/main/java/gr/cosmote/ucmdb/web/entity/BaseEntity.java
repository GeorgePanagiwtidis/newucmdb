package gr.cosmote.ucmdb.web.entity;

import gr.cosmote.ucmdb.models.base.ValueObject;

public class BaseEntity<T> extends ValueObject {

    private JsonHeader      header;
    private T               payload;
    private ExceptionHolder error;

    public BaseEntity() {
        this.header = JsonHeader.getDefault();
    }

    public BaseEntity(final T payload) {
        this();

        this.payload = payload;
    }

    public BaseEntity(final JsonHeader header, final T payload) {
        this(payload);
        this.header = header;
    }

    public BaseEntity(final ExceptionHolder error){
        this();

        this.error = error;
    }

    public JsonHeader getHeader() {
        return header;
    }

    public void setHeader(final JsonHeader header) {
        this.header = header;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(final T payload) {
        this.payload = payload;
    }

    public ExceptionHolder getError() {
        return error;
    }

    public void setError(final ExceptionHolder error) {
        this.error = error;
    }
}
