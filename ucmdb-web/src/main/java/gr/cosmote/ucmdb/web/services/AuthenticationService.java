package gr.cosmote.ucmdb.web.services;

import com.jcabi.aspects.Loggable;

import gr.cosmote.ucmdb.commons.properties.ApplicationProperties;
import gr.cosmote.ucmdb.ejb.UserRemote;
import gr.cosmote.ucmdb.models.database.UcmdbUsers;
import gr.cosmote.ucmdb.web.helper.EjbHelper;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

import javax.ejb.EJB;
import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import weblogic.security.URLCallbackHandler;
import weblogic.servlet.security.ServletAuthentication;

@Slf4j
@Path("/auth")
public class AuthenticationService {
    private final String WEB_APP_HOME   = ApplicationProperties.getInstance().getProperty("WEB_APP_HOME");
    private final String WEB_APP_INDEX  = WEB_APP_HOME + "/index.html#/";
    private final String LOGIN_ERROR    = WEB_APP_HOME + "/login.jsp#error";
    private final String LOGIN_INACTIVE    = WEB_APP_HOME + "/login.jsp#inactive";

    @EJB
    private UserRemote userRemote;

    @POST
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    @Loggable(value = Loggable.DEBUG, name = "gr.cosmote")
    public void auth(@FormParam("j_username") final String username, @FormParam("j_password") final String password,
                     @Context final HttpServletRequest request     , @Context final HttpServletResponse response) {

        try {
            log.debug("AuthenticationService({}, {}) ::: Called for username: {} ", request, response, username );

            final CallbackHandler handler = new URLCallbackHandler(username, password);
            final Subject subject = weblogic.security.services.Authentication.login(handler);

            log.debug("AuthenticationService.service({}, {}) ::: Authenticating username: {} ", request, response, username);
            weblogic.servlet.security.ServletAuthentication.runAs(subject, request);

            log.debug("AuthenticationService.service({}, {}) ::: Check authentication  for username: {} ", request, response, username );
            final boolean authenticated = ServletAuthentication.AUTHENTICATED == ServletAuthentication.weak(username, password, request, response);

            sendResponseBasedOnUserStatus(request.getSession(), response, username, authenticated);

        } catch (Exception e){
            try {
                response.sendRedirect("/client/login.jsp#error");
            } catch (IOException e1) {
                log.error("AuthenticationService.auth() ::: Error Redirecting User to error page {}", username,e1);
            }
            log.error("AuthenticationService.auth() ::: Error authenticating user {}", username,e);
        }
    }

    private void sendResponseBasedOnUserStatus(HttpSession session, HttpServletResponse response, String username, boolean authenticated) throws Exception {
        String homePageUrl = LOGIN_ERROR;
        if ( authenticated ) {
            // Check if user exists and is active
            UcmdbUsers currentUser = getCurrentUser();

            if (currentUser != null) {
                boolean isActiveUser = userRemote.getUser().getActive() == 1;

                if (isActiveUser) {
                    log.debug("AuthenticationService :: sendResponseBasedOnUserStatus :: User {} is active. Proceeding with Login Process", currentUser.getUsername());
                    homePageUrl = WEB_APP_INDEX;
                }
                else {
                    log.debug("AuthenticationService :: sendResponseBasedOnUserStatus :: User {} is inactive. Returning to Login Screen", currentUser.getUsername());
                    homePageUrl = LOGIN_INACTIVE;
                    session.invalidate();
                }
            }
            else {
                homePageUrl = WEB_APP_INDEX;
            }
        }

        response.sendRedirect(homePageUrl);
    }

    private UcmdbUsers getCurrentUser() {
        UcmdbUsers currentUser;
        try {
            currentUser = userRemote.getUser();
        }
        catch (Exception ex) {
            log.debug("AuthenticationService :: getCurrentUser :: User does not Exist in DB, proceeding with Insert User Process...");
            return null;
        }

        return currentUser;
    }
}
