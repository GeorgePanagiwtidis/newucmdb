package gr.cosmote.ucmdb.web.services;

import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import gr.cosmote.ucmdb.commons.exceptions.ApplicationException;
import gr.cosmote.ucmdb.ejb.BusinessRemote;
import gr.cosmote.ucmdb.models.base.*;
import gr.cosmote.ucmdb.models.view.BusinessCiCollectionRequestDTO;
import gr.cosmote.ucmdb.models.view.BusinessResultDTO;
import gr.cosmote.ucmdb.web.entity.BaseEntity;
import gr.cosmote.ucmdb.web.entity.BaseEntityException;
import gr.cosmote.ucmdb.web.helper.EjbHelper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Path("/business")
public class BusinessService {

    @EJB
    private BusinessRemote businessRemote;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<BusinessResultDTO> retrieveBusinessServices(BusinessSearchCriteria searchCriteria) {
        log.debug("BusinessService :: retrieveBusinessServices :: Method Entered {} ", searchCriteria);

        BaseEntity<BusinessResultDTO> result = new BaseEntity<>();

        try {
            result.setPayload(businessRemote.retrieveBusinessServices(searchCriteria));
        }
        catch (Exception ex) {
            log.error("BusinessService :: retrieveBusinessServices :: ERROR in Method {}", ex);
            result.setError(BaseEntityException.NO_CIS_FOUND);
        }

        log.debug("BusinessService :: retrieveBusinessServices :: Method Returned Business Services Size of {}", result.getPayload().getBusiness().size());

        return result;
    }

    @Path("/ciCollections")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<List<GenericCI>> retrieveCiCollectionsOfBusinessService(BusinessCiCollectionRequestDTO ciCollectionRequest) {
        log.debug("BusinessService :: retrieveCiCollectionsOfBusinessService :: Method Entered for Business Service with GlobalId {}", ciCollectionRequest.getId());

        BaseEntity<List<GenericCI>> result = new BaseEntity<>();

        try {
            result.setPayload(businessRemote.retrieveCiCollectionsOfBusinessService(ciCollectionRequest));
        }
        catch (Exception ex) {
            log.error("BusinessService :: retrieveCiCollectionsOfBusinessService :: ERROR in Method {}", ex);
            result.setError(BaseEntityException.NO_CIS_FOUND);
        }

        log.debug("BusinessService :: retrieveCiCollectionsOfBusinessService :: Method Returned CiCollections size of {}", result.getPayload().size());

        return result;
    }

    @Path("/expandCiCollection")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<List<GenericCI>> expandCiCollection (BusinessCiCollectionRequestDTO ciCollectionRequest) {
        log.debug("BusinessService :: expandCiCollection :: Method Entered for CiCollection with GlobalId: {}", ciCollectionRequest.getId());

        BaseEntity<List<GenericCI>> result = new BaseEntity<>();

        try {
            result.setPayload(businessRemote.expandCiCollection(ciCollectionRequest));
        }
        catch (Exception ex) {
            log.error("BusinessService :: expandCiCollection :: ERROR in Method {}", ex);
            result.setError(BaseEntityException.NO_CIS_FOUND);
        }

        log.debug("BusinessService :: expandCiCollection :: Method Returned list size of {}", result.getPayload().size());

        return result;
    }

    @Path("/getNodeElements")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<List<GenericCI>> getNodeElements(BusinessCiCollectionRequestDTO ciCollectionRequest) {
        log.debug("BusinessService :: getNodeElements ::: Method entered with ci: {}", ciCollectionRequest);

        final List<GenericCI> genericCIS;
        try {
            genericCIS = businessRemote.getBusinessApplicationOrHostNodes(ciCollectionRequest);
        } catch (Exception e) {
            return new BaseEntity<>(BaseEntityException.NO_SEARCHES_FOUND);
        }

        log.debug("BusinessService :: getNodeElements ::: Method Returned {}", genericCIS);

        return new BaseEntity<>(genericCIS);
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @Path("/menu")
    public BaseEntity<Map<String, String>> getMenu(List<String> ciIDs) throws NamingException {
        log.debug("BusinessService :: getMenu ::: Method entered for CIs size of {}", ciIDs.size());
        BaseEntity<Map<String, String>> menuItemsResponse = new BaseEntity<>();

        try {
            menuItemsResponse.setPayload(businessRemote.getMenuItems(ciIDs));
        } catch (ApplicationException ex){
            log.error("BusinessService :: getMenu ::: ERROR {}", ex);
            return new BaseEntity<>(BaseEntityException.NO_SEARCHES_FOUND);
        }

        return menuItemsResponse;
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @Path("/type")
    public BaseEntity<List<GenericCI>> getGenericCIsOfType(final CIsOfType cisOfType) {
        log.debug("BusinessService.getGenericCIsOfType ::: Method entered with cisOfType: {}", cisOfType.getType());

        final List<GenericCI> genericCIS;
        try {
            genericCIS = businessRemote.getCINeighboursOfType(cisOfType);
        } catch (Exception e) {
            log.error("BusinessService.getGenericCIsOfType ::: Error in getGenericCIsOfType method",e);
            return new BaseEntity<>(BaseEntityException.NO_SEARCHES_FOUND);
        }

        return new BaseEntity<>(genericCIS);
    }

}