package gr.cosmote.ucmdb.web.services;

import gr.cosmote.ucmdb.ejb.UcmdbSearchRemote;
import gr.cosmote.ucmdb.models.database.SearchType;
import gr.cosmote.ucmdb.web.entity.BaseEntity;
import gr.cosmote.ucmdb.web.entity.BaseEntityException;
import gr.cosmote.ucmdb.web.helper.EjbHelper;
import lombok.extern.slf4j.Slf4j;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Slf4j
@Path("/ucmdbSearch")
public class UcmdbSearchService {
    @EJB
    private UcmdbSearchRemote ucmdbSearchEjb;

    @Path("getSearchTypes")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<List<SearchType>> getSearchTypes() {
        log.debug("UcmdbSearchService.getSearchTypes :: Method Entered");
        BaseEntity<List<SearchType>> searchTypesResponse = new BaseEntity<>();

        try {
            List<SearchType> searchTypes = ucmdbSearchEjb.getSearchTypes();
            searchTypesResponse.setPayload(searchTypes);
        }
        catch (Exception ex) {
            searchTypesResponse.setError(BaseEntityException.NO_SEARCH_TYPES);
            log.error("UcmdbSearchService.getSearchTypes :: ERROR {}", ex);
        }

        log.debug("UcmdbSearchService.getSearchTypes :: Method Returned");
        return searchTypesResponse;
    }
}
