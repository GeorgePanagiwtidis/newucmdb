package gr.cosmote.ucmdb.web.services;

import com.jcabi.aspects.Loggable;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.ejb.EJB;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.naming.NamingException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import gr.cosmote.ucmdb.ejb.TasksRemote;
import gr.cosmote.ucmdb.ejb.NodeRemote;
import gr.cosmote.ucmdb.models.database.Group;
import gr.cosmote.ucmdb.models.database.Node;
import gr.cosmote.ucmdb.models.database.Task;
import gr.cosmote.ucmdb.models.database.TaskSummary;
import gr.cosmote.ucmdb.models.database.TasksWrapper;
import gr.cosmote.ucmdb.models.enumeration.NodeStatusEnum;
import gr.cosmote.ucmdb.models.enumeration.TaskStatusEnum;
import gr.cosmote.ucmdb.models.view.TaskDTO;
import gr.cosmote.ucmdb.web.entity.BaseEntity;
import gr.cosmote.ucmdb.web.helper.EjbHelper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Path("/unclassified")
public class UnclassifiedService {

    @EJB
    private NodeRemote nodeRemote;

    @EJB
    TasksRemote tasksRemote;

    private static final String NODE = "node";

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Loggable(value = Loggable.DEBUG, name = "gr.cosmote")
    @TransactionAttribute(value = TransactionAttributeType.NEVER)
    //TODO: Delete test method
    public BaseEntity<List<TaskDTO>> getUnclassifiedNodes() throws NamingException {
        BaseEntity<List<TaskDTO>> value = new BaseEntity<>();

        Node node = new Node();
        node.setId(29701);
        node.setGlobalId(UUID.randomUUID().toString());
        node.setCreationDate(new Date());
        node.setStatus(NodeStatusEnum.OPEN);

        TasksWrapper wrapper = new TasksWrapper();
        Task task = new Task();
        task.setStatus(TaskStatusEnum.INITIATED);
        Group group = new Group();
        group.setId(35);
        group.setDescription("GROUP_351");

        task.setUserGroup(group);
        task.setAssignedDate(new Date());
        wrapper.setTasks(Arrays.asList(task));

        try {
            node.setTasksWrapper(wrapper);
        } catch (IOException e) {
            e.printStackTrace();
        }

        nodeRemote.saveOrUpdateNodeAndTaskSummaryRecords(node);

        List<TaskSummary> taskSummaryList = tasksRemote.getTaskSummaryRecords(Arrays.asList(group));
        List<TaskDTO> taskDTOS = TransformationService.transformTaskSummaryToDTO(taskSummaryList);

        log.debug("UnclassifiedService.getUnclassifiedNodes() ::: returned {}", taskDTOS);

        value.setPayload(taskDTOS);
        return value;
    }
}