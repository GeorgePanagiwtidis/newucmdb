package gr.cosmote.ucmdb.web.entity;

import gr.cosmote.ucmdb.models.base.ValueObject;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ExceptionHolder extends ValueObject {
    String errorCode;
    String message;
}
