package gr.cosmote.ucmdb.web.services;

import gr.cosmote.ucmdb.ejb.NodeRemote;
import gr.cosmote.ucmdb.models.database.NodeDifferences;
import gr.cosmote.ucmdb.models.view.AuditingUserActionsDTO;
import gr.cosmote.ucmdb.models.view.NodeAuditingDTO;
import gr.cosmote.ucmdb.web.entity.BaseEntity;
import gr.cosmote.ucmdb.web.helper.EjbHelper;
import lombok.extern.slf4j.Slf4j;

import javax.ejb.EJB;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Path("/secure/audit")
@Slf4j
public class AuditingService {

    @EJB
    private NodeRemote nodeRemote;

    @Path("/nodeDiff/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<NodeAuditingDTO> getNodeDifferencesById (@PathParam("id") String nodeDiffId) throws NamingException {
        log.debug("AuditingService :: getNodeDifferencesById :: Method Entered with Id {}", nodeDiffId);
        BaseEntity<NodeAuditingDTO> nodeDiff;

        nodeDiff = new BaseEntity<>(transformNodeDifferencesToNodeAuditingDTO(nodeRemote.getNodeDifferencesById(nodeDiffId)));

        log.debug("AuditingService :: getNodeDifferencesById :: Method Returned Node Diffs {}", nodeDiff.getPayload());
        return nodeDiff;
    }

    @Path("/nodeDiff")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<NodeAuditingDTO> getNodeDifferencesByTaskId (@QueryParam("taskId") String taskId) throws NamingException {
        log.debug("AuditingService :: getNodeDifferencesByTaskId :: Method Entered with Task Id {}", taskId);
        BaseEntity<NodeAuditingDTO> nodeDiff;

        nodeDiff = new BaseEntity<>(transformNodeDifferencesToNodeAuditingDTO(nodeRemote.getNodeDifferencesByTaskId(taskId)));

        log.debug("AuditingService :: getNodeDifferencesByTaskId :: Method Returned Node Diffs {}", nodeDiff.getPayload());
        return nodeDiff;
    }

    @Path("/getUserActions")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<List<AuditingUserActionsDTO>> getUserActions() throws NamingException {
        log.debug("AuditingService :: getNodeDifferences :: Method Entered");
        BaseEntity<List<AuditingUserActionsDTO>> userActions = new BaseEntity<>();

        userActions.setPayload(transformNodeDifferencesToAuditingUserActionDTO(nodeRemote.getAllNodeActions()));

        log.debug("AuditingService :: getNodeDifferences :: Method Returned User Actions list size of {}", userActions.getPayload().size());
        return userActions;
    }

    private List<AuditingUserActionsDTO> transformNodeDifferencesToAuditingUserActionDTO(List<NodeDifferences> allNodeActions) {
        return allNodeActions.stream()
                .map((nodeAction) -> {
                    AuditingUserActionsDTO auditingUserAction = new AuditingUserActionsDTO();
                    auditingUserAction.setGlobalId(nodeAction.getGlobalId());
                    auditingUserAction.setNodeDiffId(String.valueOf(nodeAction.getId()));
                    auditingUserAction.setTaskId(nodeAction.getTaskId());
                    auditingUserAction.setProcessId(String.valueOf(nodeAction.getNodeId()));
                    auditingUserAction.setDisplayLabel(nodeAction.getDisplayLabel());
                    auditingUserAction.setOwnerTenant(nodeAction.getOwnerTenant());
                    auditingUserAction.setUserGroup(nodeAction.getUserGroup());
                    auditingUserAction.setAction(nodeAction.getUserAction());
                    auditingUserAction.setModifyTime(nodeAction.getLastModifiedTime());
                    auditingUserAction.setUpdatedBy(nodeAction.getUpdatedBy());

                    return auditingUserAction;
                })
                .collect(Collectors.toList());
    }

    private NodeAuditingDTO transformNodeDifferencesToNodeAuditingDTO (NodeDifferences nodeDifferences) {

        NodeAuditingDTO nodeAuditing;
        try {
            nodeAuditing = new NodeAuditingDTO();
            nodeAuditing.setDisplayLabel(nodeDifferences.getDisplayLabel());
            nodeAuditing.setGlobalId(nodeDifferences.getGlobalId());
            nodeAuditing.setUpdatedBy(nodeDifferences.getUpdatedBy());
            nodeAuditing.setComments(nodeDifferences.getComments());
            nodeAuditing.setLastModifiedTime(nodeDifferences.getLastModifiedTime());
            nodeAuditing.setBeforeUpdate(nodeDifferences.getBeforeUpdate());
            nodeAuditing.setAfterUpdate(nodeDifferences.getAfterUpdateLob());
        }
        catch (NullPointerException ex) {
            return new NodeAuditingDTO();
        }

        return nodeAuditing;
    }
}
