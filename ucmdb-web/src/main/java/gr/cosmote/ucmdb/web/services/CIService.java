package gr.cosmote.ucmdb.web.services;


import gr.cosmote.ucmdb.ejb.BusinessRemote;
import gr.cosmote.ucmdb.ejb.CIRemote;
import gr.cosmote.ucmdb.ejb.CiAttributeConfigurationRemote;
import gr.cosmote.ucmdb.ejb.CiAttributeLovRemote;
import gr.cosmote.ucmdb.models.base.CIAttribute;
import gr.cosmote.ucmdb.models.base.GenericCI;
import gr.cosmote.ucmdb.models.database.CiAttributeConfiguration;
import gr.cosmote.ucmdb.models.database.CiAttributeLov;
import gr.cosmote.ucmdb.models.view.UpdateCiDTO;
import gr.cosmote.ucmdb.models.view.cidetails.CiDetails;
import gr.cosmote.ucmdb.web.entity.BaseEntity;
import gr.cosmote.ucmdb.web.entity.BaseEntityException;
import gr.cosmote.ucmdb.web.helper.EjbHelper;
import lombok.extern.slf4j.Slf4j;
import org.jboss.weld.context.ejb.Ejb;

import javax.ejb.EJB;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Path("/cis")
public class CIService {

    @EJB
    private CIRemote ciRemote;
    @EJB
    private BusinessRemote businessRemote;
    @EJB
    private CiAttributeConfigurationRemote ciAttributeConfigurationRemote;
    @EJB
    private CiAttributeLovRemote ciAttributeLovRemote;

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @Path("/attributes")
    public BaseEntity<CiDetails> getAttributesByID(final GenericCI searchCI) {
        log.debug("CIService :: getAttributesByID :: Entered with id {} & type {}",
                searchCI.getId(), searchCI.getType());
        BaseEntity<CiDetails> response = new BaseEntity<>();

        try {
            String ciType = searchCI.getType();

            String[] properties =
                    ciType != null ? businessRemote.getClassAttributes(ciType).toArray(new String[0]) : null;
            response.setPayload(ciRemote.getAttributesByID(searchCI.getId(), properties));
        } catch (Exception ex) {
            log.error("CIService :: getAttributesByID :: ERROR {}", ex);

            return new BaseEntity<>(BaseEntityException.NO_CIS_FOUND);
        }

        log.debug("CIService :: getAttributesByID :: Method Returned CI Properties {}", response.getPayload());
        return response;
    }

    @Path("/isTypeOfNode/{class}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<Boolean> isCiTypeOfNode(@PathParam("class") String className) {
        log.debug("CIEjb :: isCiTypeOfNode :: Method Entered for Class Type {}", className);
        BaseEntity<Boolean> response = new BaseEntity<>();

        response.setPayload(ciRemote.isCiTypeOfNode(className));

        log.debug("CIEjb :: isCiTypeOfNode :: Method Returned");
        return response;
    }

    @Path("/isCiUpdatable/{globalId}/{ciType}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<Boolean> canSwitchToEditMode(@PathParam("globalId") String globalId, @PathParam("ciType") String ciType) {
        log.debug("CIService  :: isCiUpdatable :: Method Entered for CI with Global Id {} & CI Type {}", globalId, ciType);
        BaseEntity<Boolean> response = new BaseEntity<>();

        Boolean isNotWorkflowCi = ciRemote.isCiUpdatable(globalId);
        Boolean isCiTypeUpdatable = ciRemote.isCiTypeAppropriateForUpdate(ciType);

        response.setPayload(isNotWorkflowCi && isCiTypeUpdatable);

        log.debug("CIService  :: isCiUpdatable :: Method responded that CI Updatable Flag is {}", response.getPayload());
        return response;
    }

    @Path("/getCiAttributes/{type}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<List<CiAttributeConfiguration>> getCiAttributes(@PathParam("type") String type) {
        log.debug("CIService :: getCiAttributes :: Method Entered for type {}", type);
        BaseEntity<List<CiAttributeConfiguration>> response = new BaseEntity<>();

        response.setPayload(ciAttributeConfigurationRemote.getCiAttributesConfiguration(type));

        log.debug("CIService :: getCiAttributes :: Method Returned CI Attributes of {} size of {}", type, response.getPayload().size());
        return response;
    }

    @Path("/getCiCustomAttributes")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<List<CIAttribute>> getCiCustomAttributes(@QueryParam("type") String ciType, @QueryParam("tenant") String tenant) {
        log.debug("CIService :: getCiCustomAttributes :: Method Entered for Ci Type {} & Tenant {}", ciType, tenant);
        BaseEntity<List<CIAttribute>> ciCustomAttributes = new BaseEntity<>();

        ciCustomAttributes.setPayload(ciRemote.getCiCustomAttributes(ciType, tenant));

        log.debug("CIService :: getCiCustomAttributes :: Method Returned Ci Custom Attributes size of {}", ciCustomAttributes.getPayload().size());

        return ciCustomAttributes;
    }

    @Path("getLOVs")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLovs(@QueryParam("ciType") String ciType, @QueryParam("tenant") String tenant) {
        log.debug("CIService.getLovs :: Method Entered");
        BaseEntity<Map<String, List<CiAttributeLov>>> lovsEntity = new BaseEntity<>();

        try {
            List<CiAttributeLov> lovs;
            if (ciType == null || tenant == null) {
                lovs = ciAttributeLovRemote.getAllCiAttributeLovs();
            }
            else {
                String parentCiType = ciRemote.getCiParentClass(ciType);
                lovs = ciAttributeLovRemote.getCiAttributeLovsByTypeAndTenant(parentCiType, tenant);
            }

            Map<String, List<CiAttributeLov>> groupedLovsByAttribute = lovs.stream().collect(Collectors.groupingBy(CiAttributeLov::getType));

            lovsEntity.setPayload(groupedLovsByAttribute);
        } catch (Exception ex) {
            log.error("CIService.getLovs :: ERROR", ex);

            lovsEntity.setError(BaseEntityException.NO_CIS_FOUND);
            return Response.serverError().entity(lovsEntity).build();
        }

        log.debug("CIService.getLovs :: Method Returned LOVs size of {}", lovsEntity.getPayload().size());
        return Response.ok(lovsEntity).build();
    }

    @Path("/updateCi")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<GenericCI> updateCi(UpdateCiDTO ci) {
        log.debug("CIService :: updateCi :: Method Entered for Ci with Global Id {}", ci.getGlobalId());
        BaseEntity<GenericCI> response = new BaseEntity<>();

        response.setPayload(ciRemote.updateCiInUcmdb(ci));

        log.debug("CIService :: updateCi :: Method Returned Response{}", response.getPayload());
        return response;
    }


    @Path("updateCiAttributes/{className}")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response updateCiAttributes(@PathParam("className") String className) {
        log.debug("CIService.updateCiAttributes :: Method Entered for ClassName {}", className);

        try {
            ciAttributeConfigurationRemote.updateCiAttributes(className);
        }
        catch (Exception ex) {
            log.error("CIService.updateCiAttributes :: Error", ex);

            return Response.serverError().entity("UPDATE FAILED").build();
        }


        log.debug("CIService.updateCiAttributes :: Method Updated CiAttributes");
        return Response.ok("UPDATE SUCCESSFULLY").build();
    }
}
