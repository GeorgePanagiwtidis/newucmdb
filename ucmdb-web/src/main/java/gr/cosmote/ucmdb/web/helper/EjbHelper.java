package gr.cosmote.ucmdb.web.helper;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import gr.cosmote.ucmdb.ejb.*;

public final class EjbHelper {

    private static final String TENANT_EJB_JNDI_NAME = "ejb.ucmdb.tenantEjb#gr.cosmote.ucmdb.ejb.TenantRemote";
    private static final String CI_EJB_JNDI_NAME = "ejb.ucmdb.cIEjb#gr.cosmote.ucmdb.ejb.CIRemote";
    private static final String USER_EJB_JNDI_NAME = "ejb.ucmdb.userEjb#gr.cosmote.ucmdb.ejb.UserRemote";
    private static final String GROUP_EJB_JNDI_NAME = "ejb.ucmdb.groupEjb#gr.cosmote.ucmdb.ejb.GroupRemote";
    private static final String SEARCH_EJB_JNDI_NAME = "ejb.ucmdb.searchEjb#gr.cosmote.ucmdb.ejb.SearchRemote";
    private static final String BUSINESS_EJB_JNDI_NAME = "ejb.ucmdb.businessEjb#gr.cosmote.ucmdb.ejb.BusinessRemote";
    private static final String INFRASTRUCTURE_EJB_JNDI_NAME = "ejb.ucmdb.infrastructureEjb#gr.cosmote.ucmdb.ejb.InfrastructureRemote";
    private static final String NODE_EJB_JNDI_NAME = "ejb.ucmdb.nodeEjb#gr.cosmote.ucmdb.ejb.NodeRemote";
    private static final String CI_ATTRIBUTE_LOV_EJB_JNDI_NAME = "ejb.ucmdb.ciAttributeLovEjb#gr.cosmote.ucmdb.ejb.CiAttributeLovRemote";
    private static final String CI_ATTRIBUTE_EJB_JNDI_NAME = "ejb.ucmdb.ciAttributeConfigurationEjb#gr.cosmote.ucmdb.ejb.CiAttributeConfigurationRemote";
    private static final String UPDATE_NODE_EJB_JNDI_NAME = "ejb.ucmdb.updateNodeEjb#gr.cosmote.ucmdb.ejb.UpdateNodeRemote";
    private static final String TASKS_EJB_JNDI_NAME = "ejb.ucmdb.tasksEjb#gr.cosmote.ucmdb.ejb.TasksRemote";
    private static final String RULES_ENGINE_SERVICE_EJB_JNDI_NAME = "ejb.ucmdb.rulesEngineServiceEjb#gr.cosmote.ucmdb.ejb.RulesEngineRemote";


    private static final InitialContext getContext() throws NamingException {
        return new InitialContext();
    }

    private static final Object getEJB(final String ejbname) throws NamingException {
        return getContext().lookup(ejbname);
    }

    public static TenantRemote getTenantEjb() throws NamingException {
        return (TenantRemote) getEJB(TENANT_EJB_JNDI_NAME);
    }

    public static CIRemote getCIEjb() throws NamingException {
        return (CIRemote) getEJB(CI_EJB_JNDI_NAME);
    }

    public static UserRemote getUserEjb() throws NamingException {
        return (UserRemote) getEJB(USER_EJB_JNDI_NAME);
    }

    public static GroupRemote getGroupEjb() throws NamingException {
        return (GroupRemote) getEJB(GROUP_EJB_JNDI_NAME);
    }

    public static SearchRemote getSearchEjb() throws NamingException {
        return (SearchRemote) getEJB(SEARCH_EJB_JNDI_NAME);
    }

    public static BusinessRemote getBusinessEjb() throws NamingException {
        return (BusinessRemote) getEJB(BUSINESS_EJB_JNDI_NAME);
    }

    public static NodeRemote getNodeEjb() throws NamingException {
        return (NodeRemote) getEJB(NODE_EJB_JNDI_NAME);
    }

    public static CiAttributeLovRemote getCiAttributeLovEjb() throws NamingException {
        return (CiAttributeLovRemote) getEJB(CI_ATTRIBUTE_LOV_EJB_JNDI_NAME);
    }

    public static CiAttributeConfigurationRemote getCiAttributeEjb() throws NamingException {
        return (CiAttributeConfigurationRemote) getEJB(CI_ATTRIBUTE_EJB_JNDI_NAME);
    }

    public static UpdateNodeRemote getUpdateNodeEjb() throws NamingException {
        return (UpdateNodeRemote) getEJB(UPDATE_NODE_EJB_JNDI_NAME);
    }

    public static TasksRemote getTasksEjb() throws NamingException {
        return (TasksRemote) getEJB(TASKS_EJB_JNDI_NAME);
    }

    public static RulesEngineServiceRemote getRulesEngineServiceEjb() throws NamingException {
        return (RulesEngineServiceRemote) getEJB(RULES_ENGINE_SERVICE_EJB_JNDI_NAME);
    }

    public static InfrastructureRemote getInfrastructureEjb() throws NamingException {
        return (InfrastructureRemote) getEJB(INFRASTRUCTURE_EJB_JNDI_NAME);
    }
}
