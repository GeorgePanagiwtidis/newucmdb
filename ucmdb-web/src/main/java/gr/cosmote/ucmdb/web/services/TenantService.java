package gr.cosmote.ucmdb.web.services;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import com.jcabi.aspects.Loggable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.ejb.EJB;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import gr.cosmote.ucmdb.ejb.TenantRemote;
import gr.cosmote.ucmdb.models.database.Tenants;
import gr.cosmote.ucmdb.web.entity.BaseEntity;
import gr.cosmote.ucmdb.web.entity.BaseEntityException;
import gr.cosmote.ucmdb.web.helper.EjbHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;

@Slf4j
@Path("/tenant")
public class TenantService {

    @EJB
    private TenantRemote tenantRemote;

    private static final String   DUMMY_CACHE_KEY = "UCMDB_TENANTS";
    private static LoadingCache<String, List<Tenants>> TENANTCACHE = null;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Loggable(value = Loggable.DEBUG, name = "gr.cosmote")
    @Path("/getTenantsFromUCMDB")
    public BaseEntity<List<Tenants>> getAllTenantsFromUcmdb() throws NamingException {

        if(TENANTCACHE == null){
            TENANTCACHE = CacheBuilder.newBuilder().initialCapacity(1).refreshAfterWrite(5, TimeUnit.HOURS).build(
                    new CacheLoader<String, List<Tenants>>() {
                        @Override
                        public List<Tenants> load(String key) throws Exception {
                            log.debug("TenantService.getAllTenantsFromUcmdb() ::: Entered method");

                            final List<Tenants> tenants = tenantRemote.getTenantsFromUcmdb();

                            log.debug("TenantService.getAllTenantsFromUcmdb() ::: returned from cache loader {}",tenants);

                            return tenants;

                        }
                    });
        }

        try{

            final List<Tenants> tenantsFromCache = TENANTCACHE.get(DUMMY_CACHE_KEY);
            final BaseEntity<List<Tenants>> value = new BaseEntity<List<Tenants>>(tenantsFromCache);


            log.debug("TenantService.getAllTenantsFromUcmdb() ::: method returns {}",tenantsFromCache);

            return  value;

        }catch (Exception e){
            log.debug("Error from TenantService.getAllTenantsFromUcmdb() {}",e);
        }


        return new BaseEntity<List<Tenants>>(new ArrayList<Tenants>());
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @Loggable(value = Loggable.DEBUG, name = "gr.cosmote")
    @Path("/insertUcmdbTenants")
    public BaseEntity<List<Tenants>> ucmdbInsertUcmdbTenants(List<Tenants> tenants)
    {
        try{
            final List<Tenants> response = tenantRemote.ucmdbInsertUcmdbTenants(tenants);
            if (response == null) {
                return new BaseEntity<List<Tenants>>();
            }
            final BaseEntity<List<Tenants>> value = new BaseEntity<List<Tenants>>(response);
            log.debug("TenantService.insertUcmdbTenants ::: Return {}", value);
            return value;
        } catch (Exception e){
            log.error("TenantService.insertUcmdbTenants ::: Error in insertUcmdbTenants method",e);

            return new BaseEntity<List<Tenants>>(BaseEntityException.NO_TENANTS_INSERTED);
        }
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/getAllTenants")
    public BaseEntity<List<Tenants>> getAllTenants() {
        log.debug("TenantService.getAllTenants ::: Entered");

        try{
            final List<Tenants> tenants = tenantRemote.getAllTenants();
            if (CollectionUtils.isEmpty(tenants)) {
                return new BaseEntity<List<Tenants>>();
            }
            final BaseEntity<List<Tenants>> value = new BaseEntity<List<Tenants>>(tenants);
            log.debug("TenantService.getGroups ::: Return {}", value);
            return value;
        } catch (Exception e){
            log.error("TenantService.getGroups ::: Error in getGroups method",e);

            return new BaseEntity<List<Tenants>>(BaseEntityException.NO_TENANTS_FOUND);
        }
    }

    @Path("update")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateTenant(Tenants tenant) {
        log.debug("TenantService.updateTenant :: Method Entered for Tenant {} with Old Env flag {} & New Env Flag {}", tenant.getDescription(), tenant.isOldEnv(), tenant.isNewEnv());
        BaseEntity<Integer> responseEntity = new BaseEntity<>();

        try {
            tenantRemote.updateTenant(tenant);
            responseEntity.setPayload(1);
        }
        catch (Exception ex) {
            log.error("TenantService.updateTenant :: Error", ex);
            responseEntity.setError(BaseEntityException.NO_UPDATE_PERFORMED);
        }

        log.debug("TenantService.updateTenant :: Method Updated Tenant Status");
        return Response.ok(responseEntity).build();
    }
}
