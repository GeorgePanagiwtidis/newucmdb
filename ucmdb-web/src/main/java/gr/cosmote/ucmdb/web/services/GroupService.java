package gr.cosmote.ucmdb.web.services;

import com.jcabi.aspects.Loggable;
import gr.cosmote.ucmdb.commons.exceptions.ApplicationException;
import gr.cosmote.ucmdb.ejb.GroupRemote;
import gr.cosmote.ucmdb.models.base.NameCodeObject;
import gr.cosmote.ucmdb.models.database.Group;
import gr.cosmote.ucmdb.models.database.Tenants;
import gr.cosmote.ucmdb.models.database.UcmdbUsers;
import gr.cosmote.ucmdb.models.view.User;
import gr.cosmote.ucmdb.web.entity.BaseEntity;
import gr.cosmote.ucmdb.web.entity.BaseEntityException;
import gr.cosmote.ucmdb.web.helper.EjbHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import javax.ejb.EJB;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Path("/groupService")
public class GroupService {

    @EJB
    private GroupRemote groupRemote;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/getAllGroups")
    public BaseEntity<List<Group>> getAllGroups(@DefaultValue("false") @QueryParam("ordered") String shouldOrdered) {
        log.debug("GroupService.getGroups ::: Entered");

        try {
            List<Group> groups = groupRemote.getAllGroups();
            groups = getOrderGroupsIfNeeded(groups, shouldOrdered);

            if (CollectionUtils.isEmpty(groups)) {
                return new BaseEntity<>();
            }
            final BaseEntity<List<Group>> value = new BaseEntity<>(groups);
            log.debug("GroupService.getGroups ::: Return {}", value);
            return value;
        } catch (Exception e) {
            log.error("GroupService.getGroups ::: Error in getGroups method", e);

            return new BaseEntity<>(BaseEntityException.NO_GROUPS_FOUND);
        }
    }

    private List<Group> getOrderGroupsIfNeeded(List<Group> groups, String isOrdered) {
        if (!StringUtils.isEmpty(isOrdered) && "true".equals(isOrdered)) {
            groups = groups.stream()
                    .sorted(Comparator.comparing(Group::getDescription))
                    .collect(Collectors.toList());
        }

        return groups;
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/getUserGroups")
    public BaseEntity<List<Group>> getUserGroups() {
        log.debug("GroupService.getGroups ::: Entered");

        try {
            final List<Group> groups = groupRemote.getUserGroups();
            if (CollectionUtils.isEmpty(groups)) {
                log.debug("GroupService.getGroups ::: Returning empty group list");
                return new BaseEntity<List<Group>>();
            }
            final BaseEntity<List<Group>> value = new BaseEntity<List<Group>>(groups);
            log.debug("GroupService.getGroups ::: Return {}", value);
            return value;
        } catch (Exception e) {
            log.error("GroupService.getGroups ::: Error in getGroups method", e);
            return new BaseEntity<List<Group>>(BaseEntityException.NO_GROUPS_FOUND);
        }
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/getUserTenants")
    public BaseEntity<List<Tenants>> getUserTenants() {
        log.debug("GroupService.getUserTenants ::: Entered");

        try {
            final List<Tenants> tenants = groupRemote.getUserTenants();
            if (CollectionUtils.isEmpty(tenants)) {
                return new BaseEntity<List<Tenants>>();
            }
            final BaseEntity<List<Tenants>> value = new BaseEntity<List<Tenants>>(tenants);
            log.debug("GroupService.getUserTenants ::: Return {}", value);
            return value;
        } catch (Exception e) {
            log.error("GroupService.getUserTenants ::: Error in getUserTenants method", e);

            return new BaseEntity<List<Tenants>>(BaseEntityException.NO_TENANTS_FOUND);
        }
    }


    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @Path("/insertGroup")
    public BaseEntity<Group> insertGroup(Group group) throws NamingException {
        log.debug("GroupService.insertGroup ::: Method Entered");
        BaseEntity<Group> response = new BaseEntity<>();

        try {
            Group insertedGroup = groupRemote.ucmdbInsertGroup(group);
            response.setPayload(insertedGroup);
        } catch (ApplicationException ex) {
            log.error("GroupService.insertGroup ::: Error in insertGroup method", ex);
            return new BaseEntity<>(BaseEntityException.NO_GROUPS_INSERTED);
        }

        return response;
    }

    @Path("/updateGroup")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<Group> updateGroup(Group group) throws NamingException {
        log.debug("GroupService :: updateGroup :: Method Entered for updating Group with Id {}", group.getId());
        BaseEntity<Group> groupResponse = new BaseEntity<>();

        try {
            groupResponse.setPayload(groupRemote.updateGroup(group));
        }
        catch (ApplicationException ex) {
            log.error("GroupService :: updateGroup :: ERROR {}", ex);
            return new BaseEntity<>(BaseEntityException.NO_GROUPS_INSERTED);
        }

        log.debug("GroupService :: updateGroup :: Method Returned Updated Group {}", groupResponse.getPayload());
        return groupResponse;
    }


    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Loggable(value = Loggable.DEBUG, name = "gr.cosmote")
    @Path("/makeGroupInactive")
    public BaseEntity<Group> makeGroupInactive(NameCodeObject nameCode) {
        log.debug("GroupService.makeGroupInactive() ::: Entered method");

        try {
            final Group inactiveGroup = groupRemote.makeGroupInactive(nameCode);

            final BaseEntity<Group> value = new BaseEntity<Group>(inactiveGroup);
            log.debug("GroupService.makeGroupInactive() ::: Return = {}", value);
            return value;
        } catch (Exception e) {
            log.error("GroupService.makeGroupInactive() ::: Error in method = {}", e);
            return new BaseEntity<Group>(BaseEntityException.NO_GROUP_INACTIVE);
        }
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/getTenantsWithUsersAndGroups")
    public BaseEntity<List<Tenants>> getTenantsWithUsersAndGroups() {
        log.debug("GroupService.getTenantsWithUsersAndGroups() ::: Entered");

        try {
            final List<Tenants> tenants = groupRemote.getTenantsWithGroupsAndUsers();
            if (CollectionUtils.isEmpty(tenants)) {
                return new BaseEntity<List<Tenants>>();
            }
            final BaseEntity<List<Tenants>> value = new BaseEntity<List<Tenants>>(tenants);
            log.debug("GroupService.getTenantsWithUsersAndGroups() ::: Return {}", value);
            return value;
        } catch (Exception e) {
            log.error("GroupService.getTenantsWithUsersAndGroups() ::: ERROR", e);

            return new BaseEntity<List<Tenants>>(BaseEntityException.NO_TENANTS_FOUND);
        }
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/getAllUsers")
    public BaseEntity<List<User>> getAllUsers() {
        log.debug("GroupService.getAllUsers() ::: Entered");

        try {
            final List<User> users = groupRemote.getAllUsers();
            if (CollectionUtils.isEmpty(users)) {
                return new BaseEntity<List<User>>();
            }
            final BaseEntity<List<User>> value = new BaseEntity<List<User>>(users);
            log.debug("GroupService.getAllUsers() ::: Return {}", value);
            return value;
        } catch (Exception e) {
            log.error("GroupService.getAllUsers() ::: ERROR", e);

            return new BaseEntity<List<User>>(BaseEntityException.NO_USER_FOUND);
        }
    }
}
