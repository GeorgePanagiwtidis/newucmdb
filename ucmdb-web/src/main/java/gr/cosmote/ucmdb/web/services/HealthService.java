package gr.cosmote.ucmdb.web.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import gr.cosmote.ucmdb.commons.properties.ApplicationProperties;
import gr.cosmote.ucmdb.models.base.GenericCI;
import gr.cosmote.ucmdb.web.entity.BaseEntity;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.olap4j.impl.Base64;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Path("/health/check")
public class HealthService {

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response check() {
        log.debug("HealthService :: check :: method entered");

        String url = getHealthUrlBasedOnEnvironment();

        boolean shouldIgnoreCheck = false;
        if (url.equals("DEV_ENV")) {
            shouldIgnoreCheck = true;
        }

        if(shouldIgnoreCheck) {
            BaseEntity<GenericCI> entity = new BaseEntity<GenericCI>();
            GenericCI status = new GenericCI();
            Map<String, String> props = new HashMap<String, String>();
            props.put("ok", "true");
            status.setProperties(props);
            entity.setPayload(status);

            log.debug("HealthService.check :: DEV Environment - No Health Check Performed {}");

            return Response.ok().entity(entity).build();
        }

        // Basic Authentication for Parsing Health Site;
        String username = ApplicationProperties.getInstance().getProperty("UCMDB_API_USER");
        String password = ApplicationProperties.getInstance().getProperty("UCMDB_API_PASSWORD");
        String login = username + ":" + password;
        String base64login = new String(Base64.encodeBytes(login.getBytes()));

        GenericCI status = new GenericCI();
        status.setId("status");

        BaseEntity<GenericCI> entity = new BaseEntity<>();

        Map<String, String> props = new HashMap<>();
        try {
            Document document = Jsoup
                    .connect(url)
                    .timeout(15000)
                    .header("Authorization", "Basic " + base64login)
                    .get();

            Element table = document.getElementsByTag("table").first();
            Elements elements = table.select("tbody tr td");
            String ucmdbStatus = elements.get(2).select("font").first().text();


            // TODO fix selected element
            if ("Up".equals(ucmdbStatus)) {
                props.put("ok", "true");
            }
            else {
                props.put("ok", "true");
            }

        } catch (SocketTimeoutException timeout) {
            log.debug("HealthService :: check:: Socket Timeout in check method", timeout);
            return Response.serverError().build();
        } catch (Exception e) {
            log.debug("HealthService :: check:: Error in check method", e);
            return Response.serverError().build();
        }

        status.setProperties(props);
        entity.setPayload(status);

        log.debug("HealthService :: check :: method returned {}", entity);
        return Response.ok().entity(entity).build();
    }

    private String getHealthUrlBasedOnEnvironment() {
        String healthUrl;

        boolean isNewUcmdbEnv = Boolean.valueOf(ApplicationProperties.getInstance().getProperty("NEW_UCMDB"));
        if (isNewUcmdbEnv) {
            healthUrl = ApplicationProperties.getInstance().getProperty("GR.COSMOTE.UCMDB.UCMDBSERVICE.STATUS_NEW");
        }
        else {
            healthUrl = ApplicationProperties.getInstance().getProperty("GR.COSMOTE.UCMDB.UCMDBSERVICE.STATUS");
        }

        return healthUrl;
    }

}