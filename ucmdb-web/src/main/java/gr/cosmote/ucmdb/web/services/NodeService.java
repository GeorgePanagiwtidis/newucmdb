package gr.cosmote.ucmdb.web.services;

import gr.cosmote.ucmdb.ejb.NodeRemote;
import gr.cosmote.ucmdb.models.database.Node;
import gr.cosmote.ucmdb.models.database.Task;
import gr.cosmote.ucmdb.models.database.TaskSummary;
import gr.cosmote.ucmdb.models.database.TasksWrapper;
import gr.cosmote.ucmdb.models.enumeration.NodeStatusEnum;
import gr.cosmote.ucmdb.models.view.*;
import gr.cosmote.ucmdb.service.ExportExcelService;
import gr.cosmote.ucmdb.web.entity.BaseEntity;
import gr.cosmote.ucmdb.web.entity.BaseEntityException;
import gr.cosmote.ucmdb.web.helper.EjbHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Stateless
@Path("/secure/node")
public class NodeService {

    @EJB
    private NodeRemote nodeRemote;
    @EJB
    private ExportExcelService exportExcelService;

    @Path("/getReports")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<List<NodeReportingResultDTO>> getReports(NodeReportingDTO nodeReportingDTO) throws NamingException {
        log.debug(" NodeService.getReports :: Entered Method for report search criteria {} ", nodeReportingDTO);

        BaseEntity<List<NodeReportingResultDTO>> value = new BaseEntity<>();
        //TODO: Add Jackson date deserializer
        List<NodeReportingResultDTO> nodeReportingResultDTOList = getNodeReportingResultDTOS(nodeReportingDTO);
        value.setPayload(nodeReportingResultDTOList);

        log.debug(" NodeService.getReports :: Method Returned  {} ", value.getPayload());

        return value;
    }

    @Path("/{nodeId}/task")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<List<TaskReportDTO>> getTasks(@PathParam("nodeId") String nodeId) throws NamingException {
        log.debug("NodeService.getTasks :: Entered Method for report node {} ", nodeId);

        BaseEntity<List<TaskReportDTO>> value = new BaseEntity<>();

        List<TaskReportDTO> taskReportDTOList = new ArrayList<>();
        Node node;
        try {
            node = nodeRemote.getNode(Long.parseLong(nodeId));
        } catch (NumberFormatException ex) {
            log.error(String.format("NodeService.getTasks :: Error, could not parse node id %s ", nodeId));

            value.setPayload(taskReportDTOList);
            return value;
        }

        List<TaskReportDTO> result = transformNodeToTaskReportDTOs(node);
        value.setPayload(result);

        log.debug("NodeService.getTasks :: Method Returned  {} ", value.getPayload());

        return value;
    }

    @POST
    @Path("/exportReports")
    @Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    @Consumes({MediaType.APPLICATION_JSON})
    public byte[] exportReports(final NodeReportingDTO nodeReportingDTO) throws NamingException {
        log.debug("NodeService.exportReports ::: Entered with input parameters {}", nodeReportingDTO);

        List<NodeReportingResultDTO> nodeReportingResultDTOList = getNodeReportingResultDTOS(nodeReportingDTO);
        byte[] exportFile = exportExcelService.exportNodeReport(nodeReportingResultDTOList);

        log.debug("NodeService.exportReports ::: Returning export node reports file.");
        return exportFile;
    }

    private List<NodeReportingResultDTO> getNodeReportingResultDTOS(NodeReportingDTO nodeReportingDTO) throws NamingException {
        List<Node> nodeList = nodeRemote.getNodesBy(nodeReportingDTO.getTenants(), null, parseStringToDate(nodeReportingDTO.getFromDate()), parseStringToDate(nodeReportingDTO.getToDate()));
        return transformNodesToReports(filterNodesBasedOnStatus(nodeList, nodeReportingDTO));
    }

    private List<Node> filterNodesBasedOnStatus(List<Node> nodeList, NodeReportingDTO nodeReportingDTO) {
        List<Node> filteredNodes;

        String nodeStatus = nodeReportingDTO.getNodeStatus();
        if (nodeStatus == null || "ALL".equals(nodeStatus)) {
            filteredNodes = nodeList;
        }
        else {
            filteredNodes =
                    nodeList
                            .stream()
                            .filter(node -> nodeStatus.equals(node.getStatus().toString()))
                            .collect(Collectors.toList());
        }

        return filteredNodes;
    }

    @Path("/getWorkflowStats")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public BaseEntity<List<WorkflowStatsDTO>> getWorkflowStats(){
        log.debug("TasksService.getWorkflowStats :: Method Entered");
        BaseEntity<List<WorkflowStatsDTO>> response = new BaseEntity<>();

        try {
            List<WorkflowStatsDTO> workflowStats = nodeRemote.getWorkflowStats();
            response.setPayload(workflowStats);
        }
        catch (Exception ex) {
            log.error("TasksService.getWorkflowStats :: Error", ex);

            response.setError(BaseEntityException.NO_CIS_FOUND);
            return response;
        }

        log.debug("TasksService.getWorkflowStats :: Method Returned ");
        return response;
    }

    private List<TaskReportDTO> transformNodeToTaskReportDTOs(Node node) {
        return node.getTasksWrapper().getTasks(node.getId()).stream()
                .map(t -> {
                    TaskReportDTO taskReportDTO = new TaskReportDTO();
                    taskReportDTO.setNodeName(t.findCIAttributeValue("display_label"));
                    taskReportDTO.setOsType(t.getOsType());
                    // OS Installed Date
                    taskReportDTO.setOwnerTenant(t.findCIAttributeValue("TenantOwner"));
                    taskReportDTO.setTaskStatus((t.getStatus() != null) ? t.getStatus().toString() : null);
                    taskReportDTO.setAssignedDate((t.getAssignedDate() != null) ? "" + t.getAssignedDate().getTime() : null);
                    taskReportDTO.setAssignedGroup((t.getUserGroup() != null) ? t.getUserGroup().getDescription() : null);
                    taskReportDTO.setAssignedEngineer(t.getCurrentAssignee());
                    taskReportDTO.setProcessId(String.valueOf(node.getId()));

                    return taskReportDTO;
                })
                .collect(Collectors.toList());
    }

    private List<NodeReportingResultDTO> transformNodesToReports(List<Node> nodeList) {
        List<NodeReportingResultDTO> nodeReportingResultDTOS = new ArrayList<>();
        Map<NodeStatusEnum, List<Node>> groupedNodesByStatus = groupNodesByStatus(nodeList);

        groupedNodesByStatus.forEach((k, v) -> {
            NodeReportingResultDTO reportingResultDTO = new NodeReportingResultDTO();
            reportingResultDTO.setProcessStatus(k);

            List<ProcessReportDTO> processReportDTOList = v.stream()
                    .map(this::transformNodeToReport)
                    .collect(Collectors.toList());

            reportingResultDTO.setProcessReportDTOList(processReportDTOList);
            reportingResultDTO.setNumberOfNodes(processReportDTOList.size());

            nodeReportingResultDTOS.add(reportingResultDTO);
        });

        return nodeReportingResultDTOS;
    }

    private ProcessReportDTO transformNodeToReport(Node node) {
        ProcessReportDTO processReportDTO = new ProcessReportDTO();

        Task taskRelatedWithNode = null;
        try {
            List<Task> tasksRelatedWithNode = node.getTasksWrapper() != null ? node.getTasksWrapper().getTasks(node.getId()) : null;

            if (tasksRelatedWithNode != null) {
                Optional<Task> taskOptional;
                taskOptional = tasksRelatedWithNode
                        .stream()
                        .filter(task -> task.getUserGroup() != null)
                        .findFirst();

                if (taskOptional.isPresent()) {
                    taskRelatedWithNode = taskOptional.get();
                } else {
                    taskRelatedWithNode = null;
                }
            }
        } catch (Exception ex) {
            taskRelatedWithNode = null;
        }

        processReportDTO.setDisplayLabel(node.getDisplayLabel());
        processReportDTO.setProcessId("" + node.getId());
        processReportDTO.setNumberOfTasks((node.getTasksWrapper() != null) ? node.getTasksWrapper().getTasks(node.getId()).size() : 0);
        processReportDTO.setCiType(node.getCiType());
        processReportDTO.setStatus(node.getStatus());
        processReportDTO.setOsType(taskRelatedWithNode != null ? taskRelatedWithNode.getOsType() : "");
        processReportDTO.setOwnerTenant(node.getOwnerTenant());
        processReportDTO.setRetrievalDate(node.getCreationDate());
        processReportDTO.setAssignedGroup(taskRelatedWithNode != null ? taskRelatedWithNode.getUserGroup().getDescription() : "");

        return processReportDTO;
    }

    private Map<NodeStatusEnum, List<Node>> groupNodesByStatus(List<Node> nodeList) {
        return nodeList.stream()
                .collect(Collectors.groupingBy(Node::getStatus));
    }

    private Date parseStringToDate(String date) {
        Date result = null;

        if (StringUtils.isEmpty(date))
            return null;

        try {
            long longDate = Long.parseLong(date);
            result = new Date(longDate);
        } catch (NumberFormatException nfe) {
            log.error(String.format("NodeService.parseStringToDate ::: Error parsing string %s to date.", date));
        }
        return result;
    }
}
