(function () {

    'use strict';

    angular
        .module('logService')
        .factory('log', ['$log', function ($log) {
            var logService = {};

            var prefixLog = 'uCMDBLogger: ';

            logService.info = function (message) {
                $log.log(prefixLog + message);
            }

            logService.warn = function (message) {
                $log.warn(prefixLog + message);
            }

            logService.error = function (message) {
                $log.error(prefixLog + message);
            }

            return logService;
        }]);

})();