(
    function () {
        'use strict';

        angular
            .module('core.loader')
            .factory('loaderService', LoaderServiceFn);

        LoaderServiceFn.$inject = ['$rootScope'];
        function LoaderServiceFn($rootScope) {
            var loaderServ = {};

            loaderServ.showAppropriateLoader = function (modalWindow) {
                if (modalWindow) {
                    loaderServ.showModalWindowLoader();
                }
                else {
                    loaderServ.showAppWindowLoader();
                }
            };

            loaderServ.hideAppropriateLoader = function (modalWindow) {
                if (modalWindow) {
                    loaderServ.hideModalWindowLoader();
                }
                else {
                    loaderServ.hideAppWindowLoader();
                }
            };

            loaderServ.showAppWindowLoader = function () {
                $rootScope.waiting = true;
            };

            loaderServ.hideAppWindowLoader = function () {
                $rootScope.waiting = false;
            };

            loaderServ.showModalWindowLoader = function () {
                $rootScope.waitingModal = true;
            };

            loaderServ.hideModalWindowLoader = function () {
                $rootScope.waitingModal = false;
            };

            return loaderServ;
        }
    }
)();