(function () {

    'use strict';

    angular
        .module('authService')
        .factory('AuthService', AuthServiceFactory);

    AuthServiceFactory.$inject = ['__env', 'AclService', 'localStorageService', '$resource', '$q'];
    function AuthServiceFactory(__env, AclService, localStorageService, $resource, $q) {
        var self = this;
        var UserInfoResource = $resource(__env.PATH_OF_API + '/secure/user/getUserInfo');

        self.userInfo = null;

        var service =
            {
                initUserInfo: initUserInfo,
                getUsername: getUsername,
                isLoggedIn: isLoggedIn,
                isAdmin: isAdmin,
                isWorkflowUser: isWorkflowUser,

                exitApp: exitApp
            };

        function initUserInfo() {
            var defer = $q.defer();

            if (!self.userInfo) {
                UserInfoResource.get(function (userInfo) {
                    defer.resolve(userInfo.payload);
                    self.userInfo = userInfo.payload;
                });
            }
            else {
                defer.resolve(self.userInfo);
            }

            return defer.promise;
        }

        function getUsername() {
            var username = self.userInfo.username;

            // Remove domain (keep only username)
            var atIndex = username.indexOf('@');
            username = atIndex !== -1 ? username.substr(0, atIndex) : username;

            return username;
        }

        function isLoggedIn() {
            if (self.userInfo) {
                return self.userInfo.active;
            }
            else {
                false;
            }
        }

        function isAdmin() {
            return self.userInfo.admin;
        }

        function isWorkflowUser() {
            return self.userInfo.workflowUser;
        }

        function exitApp() {
            // Clear Local Storage
            localStorageService.clearAll();
            // Clear Acl Roles
            AclService.flushStorage();
        }

        return service;
    }
})();