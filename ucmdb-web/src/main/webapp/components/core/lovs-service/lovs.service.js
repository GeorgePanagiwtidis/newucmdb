(
    function () {
        'use strict';

        angular
            .module('core.lovs')
            .factory('lovsService', LovsFactoryFn);

        LovsFactoryFn.$inject = ['$rootScope', '$resource', 'loaderService', 'log'];

        function LovsFactoryFn($rootScope, $resource, loaderService, log) {
            var lovsFactory = {};
            var LovsByTenantAndTypeResource = $resource($rootScope.pathOfApi + "/cis/getLOVs?tenant=:tenant&ciType=:type", {
                tenant: '@tenant',
                type: '@type'
            });

            lovsFactory.getLovsByCiTypeAndTenant = function (ciType, tenant) {
                return LovsByTenantAndTypeResource.get({tenant: tenant, type: ciType}).$promise;
            }

            lovsFactory.changeAttributeLovOnEvtByTypeAndTenant = function (attributeObject, attributeName, ciType, tenant) {

                loaderService.showAppropriateLoader(true);
                lovsFactory.getLovsByCiTypeAndTenant(ciType, tenant).then(function (lovsResponse) {
                        var attributeLovs = lovsResponse.payload;
                        attributeObject.lovs = attributeLovs[attributeName].map(function (attribute) {
                            return attribute.value;
                        });

                        loaderService.hideAppropriateLoader(true);
                    },
                    function (getLovsByTenantAndTypeError) {
                        log.error('LovsFactroyFn.getLovsByTenantAndType :: Error: ', getLovsByTenantAndTypeError.statusText);
                    });
            };

            return lovsFactory;
        }
    }
)();