(function () {

    'use strict';

    angular
        .module('core.componentsDataFlow')
        .factory('ComponentsDataFlow', ['localStorageService', ComponentsDataFlowFactory]);

    function ComponentsDataFlowFactory(localStorageService) {
        var self = this;

        var service = {
            createObjectForDataFlow: createObjectForDataFlow,
            getObjectForDataFlow: getObjectForDataFlow,

            createSavedSearchObjectToEdit: createSavedSearchObjectToEdit,
            returnFromSavedSearches: returnFromSavedSearches,
            deleteDataFlowObjects: deleteDataFlowObjects,

            // WS Calls Request Helper
            createBusinessSearchRequest: createBusinessSearchRequest,
            createRequestForMenuItems: createRequestForMenuItems,
            createInfrastructureSearchRequest: createInfrastructureSearchRequest,
            createRequestForUserInactivation: createRequestForUserInactivation,
            createSaveSearchRequestObject: createSaveSearchRequestObject,
            createNewGroupRequestObject: createNewGroupRequestObject,
            createGroupUpdateRequestObject: createGroupUpdateRequestObject,
            createGroupRemovalRequestObject: createGroupRemovalRequestObject,
            createObjectEditGroup: createObjectEditGroup,
            getObjectForEditGroup: getObjectForEditGroup,
            createNodeReportRequestObject: createNodeReportRequestObject,
            createTaskReportRequestObject: createTaskReportRequestObject
        };

        return service;

        // -- Data Flow Functions --
        function createObjectForDataFlow(searchTerm, searchType, searchOptions, searchSelectedTenants) {
            self.dataFlowObject = {
                searchTerm: searchTerm,
                searchType: searchType,
                searchOptions: searchOptions,
                searchSelectedTenants: searchSelectedTenants
            }
        }

        function getObjectForDataFlow() {
            // Assign DataFlow object to a new Object and Delete it
            var dataFlowObjectCopy = angular.copy(self.dataFlowObject);
            self.dataFlowObject = null;

            return dataFlowObjectCopy;
        }

        function createSavedSearchObjectToEdit(searchTerm, searchType, searchOptions, searchTenants) {
            self.savedSearchDataFlowObject = {
                searchTerm: searchTerm,
                searchType: searchType,
                searchOptions: searchOptions,
                searchSelectedTenants: searchTenants.map(function (tenant) {
                    return {name: tenant.description};
                })
            }
        }


        function returnFromSavedSearches() {
            var savedSearchDataFlowObjectCopy = angular.copy(self.savedSearchDataFlowObject)
            self.savedSearchDataFlowObject = null;

            return savedSearchDataFlowObjectCopy;
        }

        function deleteDataFlowObjects() {
            delete self.dataFlowObject;
            delete self.savedSearchDataFlowObject;
        }


        // WS Request Helper Methods
        function createBusinessSearchRequest(searchTerm, tenants) {
            var jsonSearchRequestObject = {};

            jsonSearchRequestObject.searchTerm = searchTerm;
            jsonSearchRequestObject.tenants = [];

            tenants.forEach(function (tenant) {
                jsonSearchRequestObject.tenants.push({description: tenant.name});
            });

            return jsonSearchRequestObject;
        }

        function createRequestForMenuItems(computerItems) {
            return computerItems.map(function (computer) {
                return computer.id;
            })
        }

        function createInfrastructureSearchRequest(searchTerm, tenants, options, tableFilters, selectedColumns) {
            var jsonSearchRequestObject = {};

            var criteria = {};
            criteria.searchTerm = searchTerm;
            criteria.tenants = [];

            tenants.forEach(function (tenant) {
                criteria.tenants.push({description: tenant.name});
            });

            jsonSearchRequestObject.businessCriteria = criteria;
            jsonSearchRequestObject.advancedTerms = options;

            var requestTableFilters = [];
            angular.forEach(tableFilters, function (filterValue, filterName) {
                // Add only not empty filter properties
                if (filterValue.length !== 0) {
                    requestTableFilters.push({propertyName: filterName, propertyValue: filterValue});
                }
            });

            jsonSearchRequestObject.tableFilters = requestTableFilters;
            jsonSearchRequestObject.selectedTableColumns = selectedColumns;

            return jsonSearchRequestObject;
        }

        function createRequestForUserInactivation(id, username) {
            var jsonUser = {};

            jsonUser.name = username;
            jsonUser.id = id;

            return jsonUser;
        }

        function createSaveSearchRequestObject(searchName) {
            var locStorageSearchTerm = localStorageService.get('searchTerm');

            var searchTerm = locStorageSearchTerm.serverSearchTerm ? locStorageSearchTerm.serverSearchTerm : locStorageSearchTerm;
            var searchType = localStorageService.get('selectedClass');
            var searchSelectedTenants = localStorageService.get('selectedTenants');
            var searchOptions = localStorageService.get('optionList');

            var transformedTenants = [];
            searchSelectedTenants.forEach(function (item) {
                transformedTenants.push({id: item.id, description: item.name});
            });

            return {
                description: searchName,
                searchTerm: searchTerm,
                searchType: searchType,
                tenants: transformedTenants,
                searchOptions: searchOptions
            };
        }

        function createNewGroupRequestObject(description, tenants, users) {
            return {
                description: description,
                tenants: tenants,
                ucmdbUsers: users
            }
        }

        function createGroupUpdateRequestObject(groupId, users, tenants, isWorkflowRelatedFlag) {
            return {
                id: groupId,
                isWorkflowRelated: isWorkflowRelatedFlag,
                tenants: tenants,
                ucmdbUsers: users
            }
        }

        function createGroupRemovalRequestObject(id, name) {
            var groupRemoveJson = {};

            groupRemoveJson.id = id;
            groupRemoveJson.name = name;

            return groupRemoveJson;
        }

        function createObjectEditGroup(groupId, groupName, selectedUsers, accessTenants, isWorkflowRelatedGroup) {
            self.editGroupData = {};

            self.editGroupData.groupId = groupId;
            self.editGroupData.groupName = groupName;
            self.editGroupData.selectedUsers = selectedUsers;
            self.editGroupData.accessTenants = accessTenants;
            self.editGroupData.isWorkflowRelatedGroup = isWorkflowRelatedGroup;
        }

        function getObjectForEditGroup() {
            var itemForEdit = null;
            if (self.editGroupData) {
                itemForEdit = self.editGroupData;
                self.editGroupData = null;
            }

            return itemForEdit;
        }

        function createNodeReportRequestObject(tenants, nodeStatus, fromDate, toDate) {
            var searchObject = {};

            searchObject.tenants = tenants;
            searchObject.nodeStatus = nodeStatus;
            searchObject.fromDate = fromDate;
            searchObject.toDate = toDate;

            return searchObject;
        }

        function createTaskReportRequestObject(tenants, taskGroup, taskStatus, fromDate, toDate) {
            var searchObject = {};

            searchObject.tenants = tenants;
            searchObject.taskGroup = taskGroup;
            searchObject.taskStatus = taskStatus;
            searchObject.fromDate = fromDate;
            searchObject.toDate = toDate;

            return searchObject;
        }
    }
})();