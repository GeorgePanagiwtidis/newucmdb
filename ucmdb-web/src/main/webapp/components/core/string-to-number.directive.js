(
    function () {
        'use strict';

        angular
            .module('core')
            .directive('stringToNumber', function () {
                return {
                    require: 'ngModel',
                    link: linkFunction
                }

                function linkFunction(scope, element, attrs, ngModel) {
                    ngModel.$parsers.push(function (value) {
                        return '' + (value ? value : '');
                    });
                    ngModel.$formatters.push(function (value) {
                        return parseFloat(value);
                    });
                }
            });
    }
)();