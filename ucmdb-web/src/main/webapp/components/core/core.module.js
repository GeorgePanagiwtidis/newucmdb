(function () {

    'use strict';

    angular.module('core', [
        'core.loader',
        'core.componentsDataFlow',
        'core.searchService',
        'core.updateFromSearchModule',
        'core.lovs',
        'authService',
        'bootboxModule',
        'logService'
    ]);

})();