(function () {
    'use strict';

    angular
        .module('core.updateFromSearchModule')
        .factory('updateService', UpdateFromSearchFactoryFunction)

    UpdateFromSearchFactoryFunction.$inject = ['$rootScope', '$resource', '$q', 'coreSearchService', 'log', 'bootboxService'];

    function UpdateFromSearchFactoryFunction($rootScope, $resource, $q, coreSearchService, log, bootboxService) {
        // Resource Objects
        var IsCiUpdatableResource = $resource($rootScope.pathOfApi + '/cis/isCiUpdatable/:globalId/:ciType', {ciType: '@type', globalId: '@id'});
        var CustomAttributes = $resource($rootScope.pathOfApi + '/cis/getCiCustomAttributes?type=:type&tenant=:tenant', {type: 'node', tenant: '@tenant'});
        var UpdateResource = $resource($rootScope.pathOfApi + '/cis/updateCi');
        var ClearInfrastructureCacheResource = $resource($rootScope.pathOfApi + '/infrastructure/clearCache');

        var updateService = {
            isCiUpdatable: isCiUpdatable,
            openModalAndResetDefaults: openModalAndResetDefaults,
            switchToEditMode: switchToEditMode,
            switchToViewMode: switchToViewMode,
            updateUcmdb: updateUcmdb,
            transformGroupsObjectToPropertiesObject: concatUpdatedGroupAttributesToSingleObject,
            clearInfrastructureCache: clearInfrastructureCache
        };

        return updateService;

        function isCiUpdatable(globalId, ciType) {
            var asyncResults = {
                isWorkflowUser: checkIfUserIsWorkflowUser(),
                isCiUpdatable: isCiTypeUpdatable(globalId, ciType)
            };

            // When All Calls Have Finished
            return $q.all(asyncResults)
                .then(function (serverResponse) {
                    return serverResponse.isWorkflowUser && serverResponse.isCiUpdatable;
                });
        }

        function openModalAndResetDefaults(controllerObject) {
            // Remove Sticky Header bug styles
            angular.element('.is-sticky').removeAttr('style');

            var modalElement = angular.element('#result');
            modalElement.modal('show');

            // Reset Default Edit Mode
            controllerObject.modalEditMode = false;
        }

        function switchToEditMode(controllerObject, ciType) {
            // Switch to Edit Mode
            controllerObject.editMode = true;

            // Retrieve Tenant Owner Required for Attribute LOVs
            var tenant = controllerObject.businessItem.decoupledCiProperties['TenantOwner'];

            $rootScope.waitingModal = true;
            getCiCustomAttributes(ciType, tenant).then(function (ciCustomAttributes) {
                $rootScope.waitingModal = false;

                var ciCustomAttributesPayload = ciCustomAttributes.payload;
                var decoupledCiAttributes = controllerObject.businessItem.decoupledCiProperties;

                // Copy Already Displayed Object & Change Its Groups
                var editableObject = angular.copy(controllerObject.businessItem);
                editableObject.groupAttributes = [];
                transformCiCustomAttributesToGroupsObject(editableObject, ciCustomAttributesPayload, decoupledCiAttributes);

                // Filter Application Domain Property based on Tenant
                var ciTenantOwner = decoupledCiAttributes['TenantOwner'];
                initMultipleSelectProperties(ciTenantOwner, editableObject.groupAttributes);

                // Assign Editable Object to Modal View Object
                controllerObject.businessItem = editableObject;
            });
        }

        function switchToViewMode(controllerObject) {
            controllerObject.editMode = false;
            controllerObject.businessItem = angular.copy(controllerObject.originalItem);
        }

        function updateUcmdb(globalId, beforeUpdateGroups, updatedGroups) {
            var beforeUpdateAttributes = concatGroupAttributesToSingleObject(beforeUpdateGroups);
            var updatedAttributes = concatUpdatedGroupAttributesToSingleObject(updatedGroups);

            var noChangesDetected = checkForChanges(beforeUpdateAttributes, updatedAttributes);

            if (noChangesDetected) {
                bootboxService.info('Update Status', 'No changes detected. Update will not proceed');
                return;
            }

            closeModalOnUpdate();
            var updateObject = generateUpdateObject(globalId, beforeUpdateAttributes, updatedAttributes);

            $rootScope.waiting = true;
            performUpdateRequest(updateObject).then(
                function (response) {
                    var successfulUpdate = (response.payload.properties.status === 'true');

                    if (successfulUpdate) {
                        bootboxService.alert('Update Status', 'uCMDB Update was successful!', true);
                    }
                    else {
                        bootboxService.alert('Update Status', 'uCMDB Update failed!', false);
                    }

                    $rootScope.waiting = false;
                },
                function (updateUcmdbError) {
                    log.error('updateService :: updateUcmdb:: ERROR: ' + updateUcmdbError.statusText);
                    bootboxService.alert('Update Status', 'uCMDB Update failed!', false);

                    $rootScope.waiting = false;
                });
        }

        function clearInfrastructureCache() {
            ClearInfrastructureCacheResource.get(function () {
                    log.info('UpdateFromSearchFactoryFunction :: clearInfrastructureCache :: Cache Cleared');
                },
                function (clearInfrastructureCacheError) {
                    log.error('UpdateFromSearchFactoryFunction :: clearInfrastructureCache :: ERROR: ' + clearInfrastructureCacheError.statusText);
                })
        }

        // Helper Functions
        function checkIfUserIsWorkflowUser() {
            var defer = $q.defer();
            coreSearchService.getUserGroups.query(
                function (groupsResponse) {
                    var groups = groupsResponse.payload;

                    var workflowGroups =
                        groups.filter(function (group) {
                            return group.isWorkflowRelated === 1;
                        });

                    defer.resolve(workflowGroups.length !== 0);
                },
                function (getUserGroupsError) {
                    log.info('ResultsPageInfrastructureController :: checkIfUserIsWorkflowUser :: Error ' + getUserGroupsError.statusText);

                    defer.reject(false);
                });

            return defer.promise;
        }

        function isCiTypeUpdatable(globalId, ciType) {
            var defer = $q.defer();

            if (ciType === 'host_node' || ciType === 'netdevice') {
                defer.resolve(true);
            }
            else {
                $rootScope.waitingModal = true;
                IsCiUpdatableResource.get({ciType: ciType, globalId: globalId},
                    function (response) {
                        defer.resolve(response.payload);

                        $rootScope.waitingModal = false;
                    },
                    function (isTypeOfNodeError) {
                        log.info('UpdateFromSearchFactoryFunction :: isCiTypeOfNode :: ERROR: ' + isTypeOfNodeError.statusText);
                        defer.reject(false);

                        $rootScope.waitingModal = false;
                    });

            }

            return defer.promise;
        }

        function initMultipleSelectProperties(tenantOwner, groupAttributes) {
            angular.forEach(groupAttributes, function(group) {
                angular.forEach(group.properties, function (attribute) {
                    if (attribute.type === 'LIST_VALUES') {
                        // Init already checked values
                        var selectedValues = attribute.value.split(',');

                        attribute.selectValues = [];
                        attribute.selectedValuesFromMultiple = [];

                        angular.forEach(attribute.lovs, function (lov, attrIndex) {
                            var disabledItem = lov === tenantOwner;
                            var selectItem = {id: attrIndex, label: lov, disabled: disabledItem};
                            attribute.selectValues.push(selectItem);

                            if (selectedValues.indexOf(lov) !== -1) {
                                attribute.selectedValuesFromMultiple.push(selectItem);
                            }
                        });
                    }
                });
            });
        }

        function concatGroupAttributesToSingleObject(groups) {
            var groupProperties = {};
            angular.forEach(groups, function (groupObject) {
                _.assign(groupProperties, groupObject.properties);
            });

            return groupProperties;
        }

        function concatUpdatedGroupAttributesToSingleObject(groups) {
            var groupProperties = [];
            angular.forEach(groups, function (groupObject) {
                angular.forEach(groupObject.properties, function (property) {
                    groupProperties.push(property);
                });
            });

            return groupProperties;
        }

        function checkForChanges(beforeUpdateAttributes, updatedAttributes) {
            var changedAttributes =
                updatedAttributes.filter(function (attribute) {
                    var oldAttributeValue = beforeUpdateAttributes[attribute.ucmdbPropName] ? beforeUpdateAttributes[attribute.ucmdbPropName] : '';

                    return oldAttributeValue != attribute.value;
                });

            return changedAttributes.length === 0;
        }

        function getCiCustomAttributes(ciType, tenant) {
            return CustomAttributes.get({type: ciType, tenant: tenant}).$promise;
        }

        function transformCiCustomAttributesToGroupsObject(editableObject, ciCustomAttributesPayload, decoupledCiAttributes) {
            // Construct Groups with Attributes

            angular.forEach(ciCustomAttributesPayload, function (ciAttribute) {
                var groupName = ciAttribute.groupName;
                var attributeValue = decoupledCiAttributes[ciAttribute.fieldName];

                var alreadyExistingGroup = _.find(editableObject.groupAttributes, {groupName: groupName});

                if (alreadyExistingGroup) {
                    // If group Exists add Property to Group Attributes
                    alreadyExistingGroup.properties.push(
                        {
                            ucmdbPropName: ciAttribute.fieldName,
                            displayName: ciAttribute.fieldLabel,
                            value: attributeValue !== undefined ? attributeValue : '',
                            type: ciAttribute.fieldType,
                            mandatory: ciAttribute.mandatory,
                            canBeNull: ciAttribute.canBeNull,
                            editable: ciAttribute.editable,
                            lovs: ciAttribute.fieldLovs,
                            priority: ciAttribute.attributePriority
                        });
                }
                else {
                    // Create Group & Add current Property to Group Attributes
                    var properties = [
                        {
                            ucmdbPropName: ciAttribute.fieldName,
                            displayName: ciAttribute.fieldLabel,
                            value: attributeValue !== undefined ? attributeValue : '',
                            type: ciAttribute.fieldType,
                            mandatory: ciAttribute.mandatory,
                            canBeNull: ciAttribute.canBeNull,
                            editable: ciAttribute.editable,
                            lovs: ciAttribute.fieldLovs,
                            priority: ciAttribute.attributePriority
                        }];

                    var groupObject = {
                        groupName: groupName,
                        groupPriority: ciAttribute.groupPriority,
                        properties: properties
                    };

                    editableObject.groupAttributes.push(groupObject);
                }
            });
        }

        function generateUpdateObject(globalId, beforeUpdateAttributes, updatedAttributes) {
            var oldAttributes = [];
            var newAttributes = [];

            angular.forEach(updatedAttributes, function (attribute) {
                var oldAttributeValue = beforeUpdateAttributes[attribute.ucmdbPropName] ? beforeUpdateAttributes[attribute.ucmdbPropName] : '';

                oldAttributes.push({
                    fieldName: attribute.ucmdbPropName,
                    fieldLabel: attribute.displayName,
                    fieldValue: oldAttributeValue
                });
                newAttributes.push({
                    fieldName: attribute.ucmdbPropName,
                    fieldLabel: attribute.displayName,
                    fieldValue: attribute.value
                });
            });

            var tenantOwner = _.find(newAttributes, {fieldName: 'TenantOwner'});

            return {
                globalId: globalId,
                displayLabel: beforeUpdateAttributes["display_label"],
                tenantOwner: tenantOwner ? tenantOwner.fieldValue : '',
                beforeUpdateAttrs: oldAttributes,
                updatedAttributes: newAttributes
            }
        }

        function performUpdateRequest(updateObject) {
            return UpdateResource.save(updateObject).$promise;
        }

        function closeModalOnUpdate() {
            var modalElement = angular.element('#result');
            modalElement.modal('hide');
        }
    }
})();
