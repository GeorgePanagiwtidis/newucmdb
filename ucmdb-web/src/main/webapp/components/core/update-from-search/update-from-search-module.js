(function () {
    'use strict';

    angular.module('core.updateFromSearchModule', [
        'core.searchService',
        'ngResource'
    ]);
})();
