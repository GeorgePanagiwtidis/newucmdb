(function () {

    'use strict';

    angular
        .module('bootboxModule')
        .factory('bootboxService', function () {

            var bootboxServ = {};

            var confirmTenantTemplate =
                "<div class='form-group'>" +
                "<label>Please confirm Tenant Owner: <span style='color: red;'>*&nbsp;</span></label>" +
                "<select id='tenantLov' class='form-control' required>" +
                "<option>COSMOTE</option>" +
                "<option>T-ALB</option>" +
                "<option >T-ROM</option>" +
                "<option >RURAL</option>" +
                "<option >TMNL</option>" +
                "<option >CCH</option>" +
                "<option >DT-ONE</option>" +
                "</select>" +
                "</div>";

            var reassignmentModalTemplate =
                "<div class='form-group'>" +
                "<label>Reassign to Group: <span style='color: red;'>*&nbsp;</span></label>" +
                "<select id='reassignmentGroups' class='form-control' required ng-model='selectedGroup' ng-options='group.description for group in totalGroups track by group.id'></select>" +
                "</div>" +

                "<br>" +

                "<div class='form-group'>" +
                "<label for='comments'>Reassignment Reason: &nbsp;</label>" +
                "<textarea id='reassignmentReason' class='form-control' rows='3' maxlength='255'></textarea>" +
                "</div>";
            
            bootboxServ.info = function (title, message) {
                var infoIcon = '<i class="material-icons alert-bootbox-info">info</i>';
                var className = 'alert-bootbox-info';

                bootbox.alert({
                    title: title,
                    message: infoIcon + '&nbsp;' + '<span class="status-message">' + message + '</span>',
                    className: className,
                    backdrop: true,
                    onEscape: true
                });
            }

            bootboxServ.alert = function (title, message, success) {
                var className = success ? 'alert-bootbox-success' : 'alert-bootbox-failed';
                var statusIcon = success ? '<i class="fa fa-check status-icon" aria-hidden="true"></i>' : '<i class="fa fa-exclamation-circle status-icon" aria-hidden="true"></i>';

                bootbox.alert({
                    title: title,
                    message: statusIcon + '&nbsp' + '<span class="status-message">' + message + '</span>',
                    className: className,
                    backdrop: true,
                    onEscape: true
                });
            }

            bootboxServ.alertWithCallback = function (title, message, success, callback) {
                var className = success ? 'alert-bootbox-success' : 'alert-bootbox-failed';
                var statusIcon = success ? '<i class="fa fa-check status-icon" aria-hidden="true"></i>' : '<i class="fa fa-exclamation-circle status-icon" aria-hidden="true"></i>';

                bootbox.alert({
                    title: title,
                    message: statusIcon + '&nbsp' + '<span class="status-message">' + message + '</span>',
                    className: className,
                    backdrop: true,
                    callback: callback
                });
            }

            bootboxServ.confirm = function (title, message, buttonLabels, callbackFunction) {
                if (buttonLabels.length == 0) {
                    buttonLabels = ['Cancel', 'Confirm'];
                }

                bootbox.confirm({
                    title: title,
                    message: message,
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> ' + buttonLabels[0]
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> ' + buttonLabels[1]
                        }
                    },
                    callback: callbackFunction
                });
            }

            bootboxServ.customDialog = function (title, type, callbackFunction, compile, scope) {
                var template;
                if (type === 'reassignTask') {
                    template = reassignmentModalTemplate;
                    var templateAng = angular.element(template);
                    var linkFn = compile(templateAng);
                    template = linkFn(scope);
                }
                else if (type === 'tenantValidation') {
                    template = confirmTenantTemplate;
                }

                var dialog = bootbox.dialog({
                    title: title,
                    message: template,
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Cancel',
                            className: 'btn-default'
                        },
                        ok: {
                            label: '<i class="fa fa-check"></i> Ok',
                            className: 'btn-primary',
                            callback: callbackFunction
                        }
                    }
                });
            }

            return bootboxServ;
        });
})();