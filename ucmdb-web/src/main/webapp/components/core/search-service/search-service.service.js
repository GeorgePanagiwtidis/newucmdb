(function () {

    'use strict';

    angular
        .module('core.searchService')
        .factory('coreSearchService', ['$rootScope', '$resource', function ($rootScope, $resource) {

            var service = {};

            service.getSearchTypes = $resource($rootScope.pathOfApi + "/ucmdbSearch/getSearchTypes", {}, {
                query: {
                    method: 'GET',
                    isArray: false,
                    cache: true
                }
            });

            service.checkHealth = $resource($rootScope.pathOfApi + "/health/check", {}, {
                query: {
                    method: 'GET',
                    isArray: false,
                    cache: true
                }
            });

            service.getTenantsForUser = $resource($rootScope.pathOfApi + '/groupService/getUserTenants', {}, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });

            service.updateTenant = $resource($rootScope.pathOfApi + '/tenant/update', {}, {
                query: {
                    method: 'PUT',
                    isArray: false
                }
            });

            service.getTenants = $resource($rootScope.pathOfApi + '/tenant/getAllTenants', {}, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });

            service.getViewQueries = $resource($rootScope.pathOfApi + "/viewQueries/getViewQueries", {}, {
                query: {
                    method: 'GET',
                    isArray: false,
                    cache: true
                }
            });

            service.getViewQueryProperties = $resource($rootScope.pathOfApi + "/viewQueries/getQueryProperties/:name", {name: '@name'}, {
                query: {
                    method: 'GET',
                    isArray: false,
                    cache: true
                }
            });

            service.viewQuerySearch = $resource($rootScope.pathOfApi + '/viewQueries/executeQuery', {}, {
                query: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.exportViewQueriesExcel = $resource($rootScope.pathOfApi + '/export/exportViewQuery', {}, {
                export: {
                    method: 'POST',
                    responseType: 'arraybuffer',
                    transformResponse: function (data) {
                        return {data: data};
                    }
                }
            });

            service.clearViewQueriesCache = $resource($rootScope.pathOfApi + '/viewQueries/clearCache');

            service.getBusinessServices = $resource($rootScope.pathOfApi + '/business', {}, {
                query: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.getCiCollections = $resource($rootScope.pathOfApi + '/business/ciCollections', {}, {
                query: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.expandCiCollections = $resource($rootScope.pathOfApi + '/business/expandCiCollection', {}, {
                query: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.getNodeElements = $resource($rootScope.pathOfApi + '/business/getNodeElements', {}, {
                query: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.getMenuItems = $resource($rootScope.pathOfApi + '/business/menu', {}, {
                query: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.getComputerChildren = $resource($rootScope.pathOfApi + '/business/type', {}, {
                query: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.getInfoForUser = $resource($rootScope.pathOfApi + '/secure/user/getUser', {}, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });

            service.getUserGroups = $resource($rootScope.pathOfApi + '/groupService/getUserGroups', {}, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });

            service.saveSearchRequest = $resource($rootScope.pathOfApi + '/secure/searches/insertSearch', {}, {
                update: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.getGroups = $resource($rootScope.pathOfApi + '/groupService/getAllGroups?ordered=:param', {param: '@param'}, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            })

            service.createGroupRequest = $resource($rootScope.pathOfApi + '/groupService/insertGroup', {}, {
                update: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.updateGroupRequest = $resource($rootScope.pathOfApi + '/groupService/updateGroup', {}, {
                update: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.removeGroupRequest = $resource($rootScope.pathOfApi + '/groupService/makeGroupInactive', {}, {//,removeGroup
                update: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.getAllUsers = $resource($rootScope.pathOfApi + '/groupService/getAllUsers', {}, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });

            service.makeUserInactive = $resource($rootScope.pathOfApi + '/secure/user/makeUserInactive', {}, {
                query: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.getSearches = $resource($rootScope.pathOfApi + '/secure/searches/getSearches', {}, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });

            service.downloadExcelService = $resource($rootScope.pathOfApi + '/export/xls', {}, {
                query: {
                    method: 'POST',
                    responseType: 'arraybuffer',
                    transformResponse: function (data, headersGetter) {
                        // Stores the ArrayBuffer object in a property called "data"
                        return {data: data};
                    }
                }
            });

            service.downloadExcelServiceInfrastructure = $resource($rootScope.pathOfApi + '/export/xlsInfrastructure', {}, {
                query: {
                    method: 'POST',
                    responseType: 'arraybuffer',
                    transformResponse: function (data, headersGetter) {
                        // Stores the ArrayBuffer object in a property called "data"
                        return {data: data};
                    }
                }
            });

            service.searchBusinessRequest = $resource($rootScope.pathOfApi + '/business', {}, {
                query: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.searchInfrastructure = $resource($rootScope.pathOfApi + '/infrastructure', {}, {
                query: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.getInfrastructureItemDetails = $resource($rootScope.pathOfApi + '/infrastructure/infrastructureDetails', {}, {
                query: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.retrieveInfraTypes = $resource($rootScope.pathOfApi + '/infrastructure/getInfrastructureTypes', {}, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });

            service.getInfraCustomAttributes = $resource($rootScope.pathOfApi + '/infrastructure/getCustomAttributes?typeId=:typeId', {typeId: '@id'}, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });

            service.getTenantsFromUCMDB = $resource($rootScope.pathOfApi + '/tenant/getTenantsFromUCMDB', {}, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });

            service.getTenantsUsersGroups = $resource($rootScope.pathOfApi + '/groupService/getTenantsWithUsersAndGroups', {}, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });

            service.removeSearch = $resource($rootScope.pathOfApi + '/secure/searches/removeSearch/:searchId', {searchId: '@searchId'}, {
                delete: {
                    method: 'DELETE',
                    isArray: false
                }
            });

            service.getUserTasks = $resource($rootScope.pathOfApi + '/secure/task/getTasks', {}, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });

            service.getTaskAttrs = $resource($rootScope.pathOfApi + '/secure/task/getTaskAttrs', {}, {
                query: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.taskAssignment = $resource($rootScope.pathOfApi + '/secure/task/taskAssignment', {}, {
                update: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.getWorkflowStats = $resource($rootScope.pathOfApi + '/secure/node/getWorkflowStats', {},{
                query: 'GET',
                isArray:false
            });

            service.getBusinessRowData = $resource($rootScope.pathOfApi + '/cis/attributes', {}, {
                query: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.updateUcmdb = $resource($rootScope.pathOfApi + '/secure/task/update', {}, {
                update: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.reportingNodeSearch = $resource($rootScope.pathOfApi + '/secure/node/getReports', {}, {
                query: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.reportingTaskSearch = $resource($rootScope.pathOfApi + '/secure/task/getReports', {}, {
                query: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.exportNodeSearch = $resource($rootScope.pathOfApi + '/secure/node/exportReports', {}, {
                query: {
                    method: 'POST',
                    responseType: 'arraybuffer',
                    transformResponse: function (data) {
                        return {data: data};
                    }
                }
            });

            service.exportTaskSearch = $resource($rootScope.pathOfApi + '/secure/task/exportReports', {}, {
                query: {
                    method: 'POST',
                    responseType: 'arraybuffer',
                    transformResponse: function (data) {
                        return {data: data};
                    }
                }
            });

            service.getNodeDetails = $resource($rootScope.pathOfApi + '/secure/node/:nodeId/task', {}, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });

            service.getAuditingDataByTaskId = $resource($rootScope.pathOfApi + '/secure/audit/nodeDiff/?taskId=:taskId', {
                taskId: '@taskId',
            }, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });

            service.getAuditingData = $resource($rootScope.pathOfApi + '/secure/audit/nodeDiff/:auditId', {auditId: '@id'}, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });

            service.getAllUserActions = $resource($rootScope.pathOfApi + '/secure/audit/getUserActions', {}, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });

            service.getCiAttributes = $resource($rootScope.pathOfApi + '/cis/getCiAttributes/:type', {type: 'node'}, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });

            service.getCiCustomAttributes = $resource($rootScope.pathOfApi + '/cis/getCiCustomAttributes?type=:type', {type: 'node'}, {
                query: {
                    method: 'GET',
                    isArray: false,
                    cache: true
                }
            });

            service.isCiTypeOfNode = $resource($rootScope.pathOfApi + '/cis/isTypeOfNode/:type', {type: '@type'}, {
                query: {
                    method: 'GET',
                    isArray: false,
                    cache: true
                }
            });

            service.addComment = $resource($rootScope.pathOfApi + '/secure/userActions/addCiComment?dispLabel=:displayLabel&tenantOwner=:tenantOwner',
                {displayLabel: '@displayLabel', tenantOwner: '@tenantOwner'},
                {
                    query: {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'text/plain'
                        },
                        isArray: false,
                        cache: true
                    }
                });

            service.getUserComments = $resource($rootScope.pathOfApi + '/secure/userActions/getUserComments', {}, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });

            service.exportUserComments = $resource($rootScope.pathOfApi + '/export/exportUserComments', {}, {
                query: {
                    method: 'GET',
                    responseType: 'arraybuffer',
                    transformResponse: function (data) {
                        return {data: data};
                    }
                }
            });

            service.exportMissingCis = $resource($rootScope.pathOfApi + '/export/exportMissingCis', {}, {
                query: {
                    method: 'GET',
                    responseType: 'arraybuffer',
                    transformResponse: function (data) {
                        return {data: data};
                    }
                }
            });

            service.exportWorkflowStats = $resource($rootScope.pathOfApi + '/export/exportWorkflowStats', {}, {
                query: {
                    method: 'GET',
                    responseType: 'arraybuffer',
                    transformResponse: function (data) {
                        return {data: data};
                    }
                }
            });

            service.getUcmdbMissingNodes = $resource($rootScope.pathOfApi + '/secure/userActions/getUcmdbMissingNodes', {}, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });

            service.addUcmdbMissingNode = $resource($rootScope.pathOfApi + '/secure/userActions/addMissingNode', {}, {
                query: {
                    method: 'POST',
                    isArray: false
                }
            });

            service.getLovs = $resource($rootScope.pathOfApi + '/cis/getLOVs', {}, {
                query: {
                    method: 'GET',
                    isArray: false
                }
            });

            service.exportUserActionAuditLog = $resource($rootScope.pathOfApi + '/export/exportUserActionAuditLog',{},{
                query: {
                    method: 'GET',
                    responseType: 'arraybuffer',
                    transformResponse: function (data) {
                        return {data: data};
                    }
                }
            });

            return service;
        }]);

})();