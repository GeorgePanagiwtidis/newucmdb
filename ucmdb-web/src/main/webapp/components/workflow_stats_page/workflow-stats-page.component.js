(
    function () {
        'use strict';

        angular
            .module('workflowStatsModule')
            .component('workflowStatsPage', {
                templateUrl: 'components/workflow_stats_page/workflow-stats-page.template.html',
                controller: WorkflowStatsController
            });

        WorkflowStatsController.$inject = ['$rootScope', 'log', 'NgTableParams', 'coreSearchService', 'bootboxService'];

        function WorkflowStatsController($rootScope, log, NgTableParams, coreSearchService ,bootboxService) {
            var self = this;

            self.$onInit = onInit;
            self.downloadWorkflowStatsExcel = downloadWorkflowStatsExcel;
            self.isLastElement = isLastElement;

            function onInit() {
                $rootScope.waiting = true;

                coreSearchService.getWorkflowStats.query(
                    function (workflowTasks) {
                        var workflowTaskResponse = workflowTasks.payload;

                        if (workflowTaskResponse && workflowTaskResponse.length !== 0) {
                            self.noWorkflowStats = false;
                            initTable(workflowTaskResponse);
                        } else {
                            self.noWorkflowStats = true;
                        }

                        $rootScope.waiting = false;
                    },
                    function (getWorkflowStatsError) {
                        log.error('WorkflowStatsController.onInit :: ERROR: ' + getWorkflowStatsError.statusText);

                        self.noWorkflowStats = true;
                        $rootScope.waiting = false;
                    }
                );
            }

            function initTable(data) {
                self.workflowTable = new NgTableParams(
                    {
                        sorting: {workflowMonth: "asc"},
                        page: 1,
                        count: data.length
                    },
                    {
                        counts: [],
                        dataset: data
                    }
                );
            }

            function isLastElement(statRow) {
                // TODO -- Check

                var tablDataset = self.workflowTable.settings().dataset;
                return tablDataset.indexOf(statRow) === tablDataset.length - 1;
            }

            function downloadWorkflowStatsExcel() {
                $rootScope.waiting = true;
                coreSearchService.exportWorkflowStats.query(
                    function (excelBytes) {
                        download(excelBytes.data, 'Workflow Stats.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

                        $rootScope.waiting = false;
                    }
                    ,function () {
                        bootboxService.alert('Server Error', 'Workflow Stats Excel could not be exported!');

                        $rootScope.waiting = false;
                    }
                );
            }

        }
    }
)();