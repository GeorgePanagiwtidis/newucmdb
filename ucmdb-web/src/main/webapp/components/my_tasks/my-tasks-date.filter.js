(function () {

    'use strict';

    angular
        .module('myTasksModule')
        .filter('tasksDateFilter', filterFn);

        function filterFn() {
        // tasks is Table Data & filterObject is Object that contains col-filter values
        return function (tasks, filterObject) {
            if (!filterObject) return data;

            var filteredArray = [];
            angular.forEach(tasks, function (task) {
                var sameProperties = 0;

                angular.forEach(filterObject, function (filter, col) {
                    var value = transformValue(col, task[col]);

                    if (value && value.toLowerCase().startsWith(filter.toLowerCase())) {
                        sameProperties++;
                    }
                });

                addTaskInFilteredResultsIfNecessary(filteredArray, filterObject, sameProperties, task);
            });

            return filteredArray;
        }
    }

    function transformValue(col, val) {
        var transformedVal;

        if (!val) return null;

        if (isDateColumn(col)) {
            transformedVal = moment(val).format('DD/MM/YYYY HH:mm');
        }
        else {
            transformedVal = val.toString();
        }

        return transformedVal;
    }

    function isDateColumn(col) {
        var lowerColumnName = col.toLowerCase();
        if (lowerColumnName.indexOf('date') !== -1) {
            return true;
        }
        else {
            return false;
        }
    }

    function addTaskInFilteredResultsIfNecessary(filteredArray, filterObject, sameProperties, task) {
        if (filterObject) {
            var filtersApplied = Object.keys(filterObject).length;

            if (filtersApplied === sameProperties) {
                filteredArray.push(task);
            }
        }
    }
}
)();