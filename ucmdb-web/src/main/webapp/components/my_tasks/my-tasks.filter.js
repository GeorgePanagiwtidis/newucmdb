(function () {

    'use strict';

    angular
        .module('myTasksModule')
        .filter('multiTask', function () {
            return function (data, taskCriteria) {
                var filteredResults = [];

                angular.forEach(data, function (dataRow) {
                    if (((dataRow.nodeId && dataRow.nodeId.startsWith(taskCriteria.text)) || (dataRow.displayLabel && dataRow.displayLabel.startsWith(taskCriteria.text)))) {
                        filteredResults.push(dataRow);
                    }
                });

                return filteredResults;
            };
        });

})();