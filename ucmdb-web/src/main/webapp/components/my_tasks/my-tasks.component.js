(function () {

    'use strict';

    angular
        .module('myTasksModule')
        .component('myTasksComponent', {
            templateUrl: 'components/my_tasks/my-tasks.template.html',
            controller: ['$rootScope', '$timeout', '$q', 'log', 'bootboxService', 'NgTableParams', 'localStorageService', 'coreSearchService', '$filter', MyTasksController]
        });

    function MyTasksController($rootScope, $timeout, $q, log, bootboxService, NgTableParams, localStorageService, coreSearchService, $filter) {
        var self = this;

        // JSON that holds Tasks & their Attributes
        self.tasksData =
            {
                tasks: []
            };

        self.tableParams;
        self.taskFilter = {nodeId: ''};
        self.noTasks = false;

        self.selectedTask = {};
        self.nodeCustomAttributes;
        self.nodeInfo = {};

        // Username for error msg
        self.username;

        // Pagination Object
        self.paginationObject =
            {
                pageNumber: 1,
                numOfRows: 10
            }

        self.$onInit = function () {
            $rootScope.waiting = true;

            self.tableParams = new NgTableParams(
                {
                    sorting: {assignedDate: 'desc'},
                    filter: self.taskFilter
                },
                {
                    // filterOptions: { filterFilterName: "multiTask" },
                    filterOptions: {filterFilterName: 'tasksDateFilter'},
                    counts: [],
                    dataset: []
                });

            coreSearchService.getUserTasks.query(function (tasks) {
                    // Set Username
                    self.username = localStorageService.get('loggedUser');

                    if (!self.isPayloadEmpty(tasks)) {
                        // Store Tasks Badge Number
                        localStorageService.set('numOfTasks', tasks.payload.length);

                        self.noTasks = false;

                        angular.forEach(tasks.payload, function (task) {
                            task.assigned = false;

                            if (self.isTaskAssignedOnCurrentUser(task.assignedTo)) {
                                task.myTask = true;
                            }
                            else {
                                task.myTask = false;
                            }

                            self.tasksData.tasks.push(task);
                        });

                        self.tableParams.settings().dataset = self.tasksData.tasks;
                        // self.tableParams.count(self.tasksData.tasks.length);
                        self.tableParams.reload();
                    }
                    else {
                        self.noTasks = true;
                    }

                    $rootScope.waiting = false;
                },
                function (getUserTasks) {
                    log.error("MyTasksController.onInit :: getUserTasks :: " + getUserTasks.statusText);

                    self.noTasks = true;
                    $rootScope.waiting = false;
                });
        }

        self.putFieldsInHeader = function (fieldLabel, fieldValue, taskRow) {
            taskRow.attrs.header.attributes.push({fieldLabel: fieldLabel, fieldValue: fieldValue});
        }

        self.editTask = function (task) {
            // Find Task Index
            var index = _.findIndex(self.tableParams.settings().dataset, task);

            // Flag to Disable Attributes Edit
            if (task.myTask) {
                self.selectedTask.editableFields = true;
            }
            else {
                self.selectedTask.editableFields = false;
            }

            $rootScope.waiting = true;
            $q.all(
                {
                    taskAttributes: self.getTaskAttributes(task),
                    nodeCustomAttributes: getNodeCustomAttributes()
                })
                .then(
                    function (responses) {
                        $rootScope.waiting = false;

                        manipulateTaskAttributesAndOpenModal(task, responses);
                    },
                    function (responsesError) {
                        $rootScope.waiting = false;

                        var taskAttributesError = responsesError.taskAttributes;
                        var nodeCustomAttributesError = responsesError.nodeCustomAttributes;

                        if (taskAttributesError.type === 'empty response') {
                            self.selectedTask.task = null;
                            self.selectedTask.attrs = null;
                            self.selectedTask.error = 'No Attributes found for selected Task!'
                        }
                        else {
                            $('#taskObject-' + index).notify("Could not fetch Tasks Attributes for this task", 'error');
                        }

                        log.error(nodeCustomAttributesError);
                    });
        }

        function manipulateTaskAttributesAndOpenModal(task, responses) {
            // Get Modal Element
            var editModal = angular.element('#edit-modal');

            var taskAttributes = responses.taskAttributes;
            var nodeCustomAttributes = responses.nodeCustomAttributes;

            self.selectedTask.task = task;
            self.selectedTask.attrs = taskAttributes;
            // Used this in order to restore Task when User Decides to Discard Changes -- COPY ATTRIBUTES
            self.selectedTask.copyOfAttributes = {};
            angular.copy(taskAttributes, self.selectedTask.copyOfAttributes);
            // -- END OF COPY
            self.selectedTask.error = null;
            self.nodeCustomAttributes = nodeCustomAttributes;

            // self.putFieldsInHeader('Assigned Date', task.assignedDate, self.selectedTask);
            // self.putFieldsInHeader('Process ID', task.nodeId, self.selectedTask);

            // TODO -- Check this
            self.storeApplicationDomainAttributeLOVs(taskAttributes.groups);
            self.storeCategoryAttributeLOVs(taskAttributes.groups)
            self.initApplicationDomainLOVBasedOnTenant(taskAttributes.groups);
            self.initCategoryLOVBasedOnTenantAndApplicationDomainValue(taskAttributes.groups);

            // Implementation of Multiple Values Select-Box
            self.initMultipleSelectProperties(task.ownerTenant, taskAttributes.groups);

            self.splitAttributes();

            editModal.modal('show');
            editModal.on('shown.bs.modal', function(){
                angular.element(this).find('#edit-modal-body').scrollTop(0);
            });
        }

        self.getTaskAttributes = function (task) {
            var deferred = $q.defer();

            coreSearchService.getTaskAttrs.query(task, function (taskAttrs) {
                    if (taskAttrs.payload) {
                        deferred.resolve(taskAttrs.payload);
                    }
                    else {
                        var errorObject = {type: 'empty response', message: 'No Attributes found for selected Task!'};
                        deferred.reject(errorObject);
                    }
                },
                function (editTaskError) {
                    var errorObject = {type: 'server error', message: editTaskError};
                    deferred.reject(errorObject);
                    log.error("MyTasksController.editTask :: getTaskAttrs :: " + editTaskError.statusText);
                });

            return deferred.promise;
        }

        self.getGroups = function () {
            var deferred = $q.defer();

            $rootScope.waitingModal = true;
            coreSearchService.getGroups.query({'param': 'true'}, function (groupData) {
                if (groupData && groupData.payload) {
                    // Filter Workflow Groups
                    deferred.resolve(self.filterGroupsByIsWorkflowRelatedFlag(groupData.payload));
                } else {
                    var errorObject = {type: 'empty response', message: 'No groups were found!'};
                    deferred.reject(errorObject);
                }
                $rootScope.waitingModal = false;
            });

            return deferred.promise;
        };

        self.filterGroupsByIsWorkflowRelatedFlag = function (groups) {
            var filteredGroups = groups.filter(function (group) {
                return group.isWorkflowRelated === 1;
            });

            return filteredGroups;
        }

        self.splitAttributes = function () {
            // Check for Task Attributes Header existence
            if (self.selectedTask.attrs.header.attributes) {
                var headerAttributes = self.selectedTask.attrs.header.attributes;

                // Split Header Attributes
                var splittedAttrs = [];

                var col = 0;
                var attrsLength = headerAttributes.length;
                for (var i = 0; i < 2; i++) {
                    splittedAttrs.push(headerAttributes.slice(col, col + (Math.ceil(attrsLength / 2))));
                    col += Math.ceil(attrsLength / 2);
                }
                self.selectedTask.attrs.header.attributes = splittedAttrs;
            }

            // Split Groups
            var groups = self.selectedTask.attrs.groups;
            self.sortGroupsAndAttributes(groups);
        }

        self.sortGroupsAndAttributes = function (groups) {
            angular.forEach(groups, function (group, index) {
                // Group Priorities
                if (group.groupName.contains('Description')) {
                    group.groupPriority = 1;
                }
                else if (group.groupName.contains('Application')) {
                    group.groupPriority = 2;

                }
                else if (group.groupName.contains('Service')) {
                    group.groupPriority = 3;

                }
                else if (group.groupName.contains('Technical')) {
                    group.groupPriority = 4;
                }
                else {
                    // Default Value
                    group.groupPriority = 10;
                }

                var groupAttrs = group.attributes;
                // Sort Attributes by Attribute Priority before Split
                groupAttrs.sort(function (attr1, attr2) {
                    return attr1.attributePriority - attr2.attributePriority;
                });

                var splittedAttrs = [];
                var col = 0;
                var attrsLength = groupAttrs.length;
                for (var i = 0; i < 3; i++) {
                    splittedAttrs.push(groupAttrs.slice(col, col + (Math.ceil(attrsLength / 3))));
                    col += Math.ceil(attrsLength / 3);
                }
                self.selectedTask.attrs.groups[index].attributes = splittedAttrs;
            });
        }

        // TODO -- Check this
        self.storeApplicationDomainAttributeLOVs = function (attributeGroups) {
            var applicationAttributesGroup = _.find(attributeGroups, {groupName: 'Application Attributes'});
            var applicationAttributeLOV = _.find(applicationAttributesGroup.attributes, {fieldName: 'domain'}).fieldLOV;

            self.selectedTask.applicationDomainLOV = [];
            angular.copy(applicationAttributeLOV, self.selectedTask.applicationDomainLOV);
        }

        self.storeCategoryAttributeLOVs = function (attributeGroups) {
            var descriptionAttributesGroup = _.find(attributeGroups, {groupName: 'Description Attributes'});
            var categoryAttributeLOV = _.find(descriptionAttributesGroup.attributes, {fieldName: 'category'}).fieldLOV;

            self.selectedTask.categoryLOV = [];
            angular.copy(categoryAttributeLOV, self.selectedTask.categoryLOV);
        }

        self.initApplicationDomainLOVBasedOnTenant = function (attributeGroups) {
            var tenantValue = _.find(_.find(attributeGroups, {groupName: 'Description Attributes'}).attributes, {fieldName: 'TenantOwner'}).fieldValue;

            angular.forEach(attributeGroups, function (group) {
                if (group.groupName.indexOf('Application') !== -1) {
                    angular.forEach(group.attributes, function (attribute) {
                        if (attribute.fieldName === 'domain') {
                            if (tenantValue.indexOf('CCH') !== -1) {
                                attribute.fieldLOV = self.selectedTask.applicationDomainLOV.slice(0, 12);
                            }
                            else {
                                attribute.fieldLOV = self.selectedTask.applicationDomainLOV.slice(12, self.selectedTask.applicationDomainLOV.length);
                            }
                        }

                        return;
                    });
                }
            });
        }

        self.initCategoryLOVBasedOnTenantAndApplicationDomainValue = function (attributeGroups) {
            var tenantValue = _.find(_.find(attributeGroups, {groupName: 'Description Attributes'}).attributes, {fieldName: 'TenantOwner'}).fieldValue;
            var applicationDomainValue = _.find(_.find(attributeGroups, {groupName: 'Application Attributes'}).attributes, {fieldName: 'domain'}).fieldValue;

            angular.forEach(attributeGroups, function (group) {
                if (group.groupName.indexOf('Description') !== -1) {
                    angular.forEach(group.attributes, function (attribute) {
                        if (attribute.fieldName === 'category') {
                            if (tenantValue.indexOf('CCH') !== -1 && applicationDomainValue === 'CCH_OTHER') {
                                attribute.fieldLOV = ['TO-BE-DELETED(JUNK)', 'TO-BE-DECOMMISSIONED'];
                            }
                            else {
                                attribute.fieldLOV = self.selectedTask.categoryLOV;
                            }

                            return;
                        }
                    });
                }
            });
        }

        self.initMultipleSelectProperties = function (ownerTenant, attributeGroups) {
            angular.forEach(attributeGroups, function (group) {
                angular.forEach(group.attributes, function (attribute) {
                    if (attribute.fieldType === 'dropdown_multiple') {
                        // Init already checked values
                        var selectedValues = attribute.fieldValue.split(',');

                        attribute.selectValues = [];
                        attribute.selectedValuesFromMultiple = [];

                        angular.forEach(attribute.fieldLOV, function (lov, attrIndex) {
                            var disabledItem = lov === ownerTenant;
                            var selectItem = {id: attrIndex, label: lov, disabled: disabledItem};
                            attribute.selectValues.push(selectItem);

                            if (selectedValues.indexOf(lov) !== -1) {
                                attribute.selectedValuesFromMultiple.push(selectItem);
                            }
                        });
                    }
                });
            })
        }

        self.openEditTaskModal = function () {
            var editTaskModal = angular.element('#edit-modal');
            editTaskModal.modal('show');
        }

        self.transformJsonForRequest = function (ob) {
            var obCopy = {};
            angular.merge(obCopy, ob);

            angular.forEach(obCopy.groups, function (group) {
                var attributes = [];

                angular.forEach(group.attributes, function (attributesArray) {
                    angular.forEach(attributesArray, function (attribute) {
                        attributes.push(attribute);
                    });
                });

                group.attributes = attributes;
            });

            // Add Support Group to Groups Object
            if (obCopy.supportGroup) obCopy.groups.push(obCopy.supportGroup);

            return obCopy;
        }

        self.getUsernameWithoutCompanyDomain = function (username) {
            var atIndex = username.indexOf('@');

            return atIndex !== -1 ? username.substr(0, atIndex) : username;
        }

        self.assignTaskConfirmation = function (task, fromModal) {
            var message;
            if (!task.myTask) {
                message = task.displayLabel ? 'Assign task for Node: <strong>' + task.displayLabel + '</strong> to <i>' + self.username + '</i>?' : 'Assign task to <i>' + self.username + '</i>?';
            }
            else {
                message = task.displayLabel ? 'Unassign task for Node: <strong>' + task.displayLabel + '</strong> to <i>' + self.username + '</i>?' : 'Unassign task to <i>' + self.username + '</i>?';
            }

            bootboxService.confirm('Task Assignment', message, [], function (result) {
                if (result) {
                    self.assignTask(task, fromModal);
                }
            });
        }

        self.assignTask = function (task, fromModal) {
            var tableDataset = self.tableParams.settings().dataset;

            $rootScope.waiting = true;
            coreSearchService.taskAssignment.update(task, function (updatedTask) {
                    if (updatedTask.payload) {
                        self.selectedTask.task = updatedTask;

                        var taskItem = _.find(tableDataset, task);

                        if (self.isTaskAssignedOnCurrentUser(updatedTask.payload.assignedTo)) {
                            taskItem.assigned = true;
                            taskItem.myTask = true;
                        }
                        else {
                            updatedTask.assignedTo = null;
                            taskItem.assigned = false;
                            taskItem.myTask = false;
                        }


                        angular.extend(taskItem, updatedTask.payload);
                        self.tableParams.reload();

                        $rootScope.waiting = false;

                        if (fromModal) {
                            $timeout(function () {
                                self.editTask(task);
                            }, 500);
                        }
                    }
                    else {

                    }
                },
                function (assignTaskError) {
                    log.error('MyTasksController.assignTask :: taskAssignment :: ' + assignTaskError.statusText);
                    // TODO -- Alert Dialog

                    $rootScope.waiting = false;
                });
        }

        self.assignTaskFromModal = function (task) {
            var editModal = angular.element('#edit-modal');
            editModal.modal('hide');

            self.assignTaskConfirmation(task, true);
        }

        self.getNodeDetails = function (nodeId) {
            // Hide Edit Tasks Modal
            var editModal = angular.element('#edit-modal');
            editModal.modal('hide');

            // do the request
            $rootScope.waiting = true;
            coreSearchService.getBusinessRowData.query({id: nodeId}, function (data) {
                    if (data.payload) {
                        var ciDetailsPayload = data.payload;

                        var nodeAttributeNames = null;
                        getNodeAttributes(ciDetailsPayload.type).then(function (attributes) {
                            nodeAttributeNames = attributes;
                            openInfoModal(ciDetailsPayload, nodeAttributeNames);
                        });
                    }
                    $rootScope.waiting = false;
                },
                function (nodeDetailsError) {
                    log.error('MyTasksController.getNodeDetails :: getBusinessRowData :: ' + nodeDetailsError.statusText);

                    $('#nodeDetailsAhref').notify("Could not fetch more info about this node", 'error');

                    $rootScope.waiting = false;
                });
        }

        self.commitChanges = function (taskAttrs, discard) {
            angular.element('#edit-modal').modal('hide');

            var requestOb = discard ? taskAttrs.copyOfAttributes : self.transformJsonForRequest(taskAttrs.attrs);

            //Restore LOVs to its initial State
            self.restoreLOVs(requestOb.groups);

            $rootScope.waiting = true;
            coreSearchService.updateUcmdb.update(requestOb, function (data) {
                    var tasksDataset = self.tableParams.settings().dataset;
                    var taskIndex = _.findIndex(tasksDataset, taskAttrs.task);

                    // Retrieve List of Updated Tasks
                    var updatedTasksList = data.payload;

                    self.updateTasksNumberBadge(true);
                    tasksDataset.splice(taskIndex, 1);
                    self.showNoTasksMessage(tasksDataset);

                    if (updatedTasksList.length !== 0) {
                        angular.forEach(updatedTasksList, function (taskItem) {
                            var updatedTask = taskItem;

                            if (self.isTaskAssignedOnCurrentUser(taskItem.assignedTo)) {
                                updatedTask.assigned = true;
                                updatedTask.myTask = true;
                            }
                            else {
                                updatedTask.assigned = false;
                                updatedTask.myTask = false;
                            }

                            self.updateTable(tasksDataset, updatedTask);
                        });
                    }

                    // Update MyTasks Table
                    self.tableParams.reload();

                    if (requestOb.selectedAction === 'Reassign') {
                        bootboxService.alert('Reassign Status', "Task was reassigned.", true);
                    } else if (requestOb.selectedAction === 'Submit') {
                        bootboxService.alert('Update Status', "uCDMB Update was successful.", true);
                    }
                    else if (requestOb.selectedAction === 'Save') {
                        bootboxService.alert('Update Status', "Your changes have been saved.", true);
                    }

                    $rootScope.waiting = false;
                },
                function (updateUcmdbError) {
                    $rootScope.waiting = false;

                    log.error('MyTasksController.commitChanges :: updateUcmdb :: ' + updateUcmdbError.statusText);
                    bootboxService.alert('Status', 'An error occurred.', false);

                });
        }

        // Helper Functions
        self.updateTable = function (tableDataset, updatedTask) {
            var taskAlreadyExists = _.find(tableDataset, {taskId: updatedTask.taskId});

            if (!taskAlreadyExists) {
                tableDataset.push(updatedTask);
                // Update Tasks Number Badge
                self.updateTasksNumberBadge(false);
            }
        }

        self.showNoTasksMessage = function (dataset) {
            if (dataset.length === 0) {
                self.noTasks = true;
            }
            else {
                self.noTasks = false;
            }
        }

        self.restoreLOVs = function (requestGroups) {
            angular.forEach(requestGroups, function (group) {
                if (group.groupName.indexOf('Application') !== -1 || group.groupName.indexOf('Description') !== -1) {
                    angular.forEach(group.attributes, function (attribute) {
                        if (attribute.fieldName === 'domain') {
                            attribute.fieldLOV = self.selectedTask.applicationDomainLOV;
                        }
                        else if (attribute.fieldName === 'category') {
                            attribute.fieldLOV = self.selectedTask.categoryLOV;
                        }
                    });
                }
            })
        }

        self.isTaskAssignedOnCurrentUser = function (user) {
            if (user) {
                if (self.getUsernameWithoutCompanyDomain(user) === self.username) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }

        self.updateTasksNumberBadge = function (removedTask) {
            var currentTasksNumber = localStorageService.get('numOfTasks');

            if (removedTask) {
                // Decrease Tasks Number
                var newTasksNumber = --currentTasksNumber;
                localStorageService.set('numOfTasks', newTasksNumber);
            }
            else {
                // Decrease Tasks Number
                var newTasksNumber = ++currentTasksNumber;
                localStorageService.set('numOfTasks', newTasksNumber);
            }
        }

        self.isPayloadEmpty = function (data) {
            if (data.payload) {
                return data.payload.length === 0;
            }
            else {
                return true;
            }
        }

        function getNodeAttributes(nodeCiType) {
            var defer = $q.defer();

            coreSearchService.getCiAttributes.query({type: nodeCiType},
                function (ciAttributes) {
                    var ciAttributesPayload = ciAttributes.payload;

                    var transformedAttributes = [];
                    angular.forEach(ciAttributesPayload, function (attribute) {
                        transformedAttributes[attribute.attributeName] = attribute.attributeLabel;
                    });

                    defer.resolve(transformedAttributes);
                },
                function (getCiAttributesError) {
                    var errorMessage = 'ResultsPageInfrastructureController.getCiCustomAttributes :: ERROR: ' + getCiAttributesError.statusText;
                    log.error(errorMessage);

                    defer.reject(errorMessage);
                });

            return defer.promise;
        }

        // TODO -- Check if we should pass CiType as Arg
        function getNodeCustomAttributes() {
            var defer = $q.defer();

            coreSearchService.getCiCustomAttributes.query({type: 'node'},
                function (customAttributes) {
                    defer.resolve(customAttributes.payload);
                },
                function (getCiCustomAttributesError) {
                    defer.reject('MyTasksController.getNodeCustomAttributes :: ERROR: ' + getCiCustomAttributesError.statusText);
                });

            return defer.promise;
        }

        function openInfoModal(ciDetails, nodeAttributeNames) {
            var displayLabel =
                _.find(ciDetails.groups, {groupName: 'Technical Attributes'}).properties['display_label'];

            self.nodeInfo = {
                globalId: ciDetails.globalId,
                name: displayLabel,
                attributeNames: nodeAttributeNames,
                groupAttributes: ciDetails.groups
            }

            angular.element('#result').modal('show');
        }
    }
})();