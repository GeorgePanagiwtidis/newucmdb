(function () {

    'use strict';

    angular
        .module('savedSearches')
        .component('savedSearches', {
            templateUrl: 'components/saved_searches/saved-searches.template.html',
            controller: ['$q', '$location', '$rootScope', 'log', 'coreSearchService', 'bootboxService', 'ComponentsDataFlow', 'localStorageService', 'NgTableParams', SavedSearchesController]
        });

    function SavedSearchesController($q, $location, $rootScope, log, coreSearchService, bootboxService, ComponentsDataFlow, localStorageService, NgTableParams) {

        var self = this;

        self.savedFilters = {description: ''};

        self.userCredentials = {};
        self.admin = '';

        self.tableParams;

        self.$onInit = function () {
            $rootScope.waiting = true;

            coreSearchService.getSearches.query(
                function (data) {
                    var savedSearchesResponse = data.payload;

                    if (savedSearchesResponse.length !== 0) {
                        self.noSavedSearches = false;

                        self.tableParams = new NgTableParams(
                            {
                                sorting: {description: "asc"},
                                page: 1,
                                count: 10,
                                filter: self.savedFilters
                            },
                            {
                                counts: [],
                                dataset: savedSearchesResponse
                            }
                        );
                    }
                    else {
                        self.noSavedSearches = true;
                    }

                    $rootScope.waiting = false;
                },
                function (getSearchesError) {
                    log.error('SavedSearchesController :: onInit :: getSearches :: ERROR: ' + getSearchesError.statusText);

                    self.noSavedSearches = true;
                    $rootScope.waiting = false;
                });
        }

        self.goBackToSearch = function (searchItem) {
            // TODO -- Check this -> Filters in Results not passed in Search Page
            ComponentsDataFlow.createSavedSearchObjectToEdit(searchItem.searchTerm, searchItem.searchType,
                searchItem.searchOptions, searchItem.tenants);

            $location.path('/search_page');
        }

        // Helper Functions
        self.mapSearchTermToInfraTypes = function (searchTerm) {
            switch (searchTerm) {
                case 'host_node':
                    searchTerm = {serverSearchTerm: 'host_node', visibleSearchTerm: 'Computers'};
                    break;
                case 'database':
                    searchTerm = {serverSearchTerm: 'database', visibleSearchTerm: 'Databases'};
                    break;
                case 'application_server':
                    searchTerm = {serverSearchTerm: 'application_server', visibleSearchTerm: 'Application Servers'};
                    break;
                default:
                    searchTerm = {serverSearchTerm: 'host_node', visibleSearchTerm: 'Computers'};
                    break;
            }

            return searchTerm;
        }

        self.transformSearchTermObject = function (searchTerm, searchType) {
            if (searchType === 'Business' || searchType === 'View Queries') {
                searchTerm = searchTerm ? searchTerm : '';
            }
            else if (searchType === 'Infrastructure') {
                searchTerm = self.mapSearchTermToInfraTypes(searchTerm);
            }

            return searchTerm;
        }

        self.searchObToString = function (searchOb) {
            var searchObString;

            if (searchOb.searchTerm) {
                // Select Search Term based on Search Type
                searchObString = searchOb.searchTerm.visibleSearchTerm
                    ? searchOb.searchTerm.visibleSearchTerm : searchOb.searchTerm;

                searchObString = searchObString.concat(', ' + searchOb.searchType);
            }
            else {
                searchObString = searchOb.searchType;
            }

            for (var index in searchOb.tenants) {
                searchObString = searchObString.concat(', ' + searchOb.tenants[index].description);
            }

            // Show Search Options in Terms based on Search Type
            var extraOptions = showSearchOptionsInTerms(searchOb);
            searchObString = extraOptions ? searchObString.concat(' | ' + extraOptions) : searchObString;

            return searchObString;
        }

        self.removeSavedSearchConfirmation = function (searchOb) {
            var message = 'You are going to delete saved search <strong>' + searchOb.description + '</strong>. Are you sure?';

            bootboxService.confirm('Delete Saved Search', message, [], function (result) {
                if (result) {
                    self.removeSavedSearch(searchOb.id);
                }
            });
        }

        self.removeSavedSearch = function (searchId) {
            // Delete from DB
            $rootScope.waiting = true;
            coreSearchService.removeSearch.delete({searchId: searchId},
                function (response) {
                    var successDeletion = response.payload;

                    if (successDeletion) {
                        updateTableDataset(searchId);
                    }

                    $rootScope.waiting = false;
                },
                function (removeSearchError) {
                    log.error('SavedSearchesController :: removeSavedSearch :: removeSearch :: ERROR: ' + removeSearchError.statusText);
                    bootboxService.alert('Error!', 'An error occured while trying to Remove Search', false);

                    $rootScope.waiting = false;
                });
        }

        self.searchFromSavedSearch = function (searchItem) {
            var transformedSearchTerm = self.transformSearchTermObject(searchItem.searchTerm, searchItem.searchType);

            localStorageService.set('searchTerm', transformedSearchTerm);
            localStorageService.set('selectedClass', searchItem.searchType);

            var selectedTenants = [];
            searchItem.tenants.forEach(function (tenant) {
                selectedTenants.push({id: tenant.id, name: tenant.description});
            });

            localStorageService.set('selectedTenants', selectedTenants);
            localStorageService.set('optionList', searchItem.searchOptions);

            // Create Search Object in order to be Editable from Results Page
            ComponentsDataFlow.createObjectForDataFlow(transformedSearchTerm, searchItem.searchType,
                searchItem.searchOptions, selectedTenants);

            if (searchItem.searchType === 'Business') {
                $location.path('/results_page');
            }
            else if (searchItem.searchType === 'Infrastructure') {
                $location.path('/results_infrastructure');
            }
            else if (searchItem.searchType === 'View Queries') {
                $location.path('/view_queries');
            }
        }

        // Helper Functions
        function updateTableDataset(searchId) {
            var dataset = self.tableParams.settings().dataset;
            dataset.splice(_.findIndex(dataset, {id: searchId}), 1);

            if (dataset.length === 0) {
                self.noSavedSearches = true;
            }

            self.tableParams.reload();
        }

        function showSearchOptionsInTerms(searchOb) {
            if (searchOb.searchOptions) {
                if (searchOb.searchType === 'Business') {
                    return showBusinessSearchExtraOptions(searchOb.searchOptions.businessOptions);
                }
                else {
                    return showInfraSearchExtraOptions(searchOb.searchOptions.infraOptions);
                }
            }
            else {
                return '';
            }
        }

        function showBusinessSearchExtraOptions(businessOptions) {
            var optionsStr = '';

            // TODO -- Review it, what should be shown for multiple CiCollections?
            angular.forEach(businessOptions, function (option) {
                var tableFilters = option.tableFilters;
                angular.forEach(tableFilters, function (filter) {
                    var filterStr = filter.propertyName + ": " + (filter.propertyValue === '!' ? 'NULL' : filter.propertyValue) ;
                    optionsStr = filterStr.concat(', ' + optionsStr);
                });
            });

            return optionsStr.substr(0, optionsStr.length-2);
        }

        function showInfraSearchExtraOptions(infraOptions) {
            var optionsStr = '';
            var infraSearchOptions = infraOptions.infraOptions;
            angular.forEach(infraSearchOptions, function (searchOption) {
                var optionStr = searchOption.attributeLabel + ': ' + searchOption.attributeValue;
                optionsStr = optionStr.concat(', ' + optionsStr);
            });
            optionsStr.substr(0, optionsStr.length-2);

            var filtersStr = '';
            var tableFilters = infraOptions.tableFilters;
            angular.forEach(tableFilters, function (filter) {
                var filterStr = filter.propertyName + ": " + (filter.propertyValue === '!' ? 'NULL' : filter.propertyValue);
                filtersStr = filterStr.concat(', ' + filtersStr);
            });
            filtersStr = filtersStr.substr(0, filtersStr.length-2);

            return optionsStr + " | " + filtersStr;
        }
    }

})();