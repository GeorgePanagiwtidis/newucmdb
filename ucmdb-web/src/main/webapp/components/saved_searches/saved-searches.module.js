(function () {

    'use strict';

    angular.module('savedSearches', [
        'core.searchService',
        'core.componentsDataFlow'
    ]);

})();