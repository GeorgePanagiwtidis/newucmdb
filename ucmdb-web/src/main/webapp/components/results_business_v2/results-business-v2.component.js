'use strict';

angular
    .module('resultsBusinessV2')
    .component('resultsBusiness', {
        templateUrl: 'components/results_business_v2/results-business-v2.template.html',
        controller: ['$rootScope', '$location', '$timeout', 'log', 'localStorageService', 'NgTableParams', 'ComponentsDataFlow', 'coreSearchService', ResultsBusinessV2Controller]
    });

function ResultsBusinessV2Controller($rootScope, $location, $timeout, log, localStorageService, NgTableParams, ComponentsDataFlow, coreSearchService) {
    var self = this;

    self.businessItems = [];
    self.noSearchRequest = false;
    self.noResults = false;

    // Selected Row for Modal
    self.selectedRow;

    // Keep Computers Dataset in Order to Update Table;
    self.computerItems = [];

    self.$onInit = function () {
        // Create Object for Search Request
        var searchTerm = localStorageService.get('searchTerm');
        var selectedTenants = localStorageService.get('selectedTenants');

        var jsonSearchObject;
        if (searchTerm !== null && selectedTenants !== null) {
            self.noSearchRequest = false;
            jsonSearchObject = ComponentsDataFlow.createObjectForSearchRequestV2(searchTerm, selectedTenants);
        }
        else {
            self.noSearchRequest = true;
            return;
        }

        $rootScope.waiting = true;
        coreSearchService.getBusinessServices.query(jsonSearchObject, function (data) {
                if (data.payload) {
                    if (data.payload.length !== 0) {

                        angular.forEach(data.payload, function (item) {
                            self.businessItems.push({root: item, children: [], show: false});
                        });
                    } else {
                        self.noResults = true;
                    }
                }
                else {
                    log.warn('ResultsBusinessV2Controller.onInit :: searchRequestV2 :: No Business Results');
                }
                $rootScope.waiting = false;
            },
            function (searchResponseError) {
                log.error('ResultsBusinessV2Controller.onInit :: searchRequestV2 :: ' + searchResponseError.statusText);

                self.noResults = true;
                $rootScope.waiting = false;
            });
    }

    self.goBackToSearch = function () {
        $location.path('/search_page');
    }

    self.downloadExcel = function (index, description) {
        // var excelFile = JSON.stringify(self.resultObject);
        var searchTerm = localStorageService.get('searchTerm');
        var tenants = localStorageService.get('selectedTenants');

        var postObject = {};
        postObject.exportDetails = {};
        postObject.criteria = {};

        postObject.exportDetails.name = description;
        postObject.exportDetails.id = index;

        postObject.criteria.searchTerm = searchTerm;

        var postTenants = [];
        tenants.forEach(function (tenant) {
            postTenants.push({description: tenant.name});
        });
        postObject.criteria.tenants = postTenants;

        var fileName = _.startCase(searchTerm);

        fileName = fileName.concat('_' + description);

        var date = new Date();
        var dayStr = date.getDate().toString();
        var day = dayStr.length < 2 ? '0' + dayStr : dayStr;

        var monthStr = (date.getMonth() + 1).toString();
        var month = monthStr.length < 2 ? '0' + monthStr : monthStr;

        var year = date.getFullYear();

        fileName = fileName.concat('_' + day + month + year);

        $rootScope.waiting = true;
        coreSearchService.downloadExcelService.query(postObject, function (excelFile) {
                // download.js plugin
                download(excelFile.data, fileName + '.xls', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                $rootScope.waiting = false;
            }
            ,
            function (downloadExcelError) {
                log.error('ResultsBusinessControllerV2.downloadExcel :: downloadExcelService :: ' + downloadExcelError.statusText);
            });

        // -- JS Native Method -- (Does not work on IE)
        // var blob = new Blob([excelFile], {type: 'application/json'});
        // self.excelUrl = (window.URL || window.webkitURL).createObjectURL( blob );
    }

    self.saveSearch = function saveSearch() {
        if (!self.searchName) {
            self.saveFailedValidation = true;
            return;
        }

        var userId;

        var savedObject = ComponentsDataFlow.getObjectForSavingInResults();

        if (savedObject) {
            $rootScope.waiting = true;
            coreSearchService.getInfoForUser.query(function (data) {
                userId = data.payload.id;

                var searchRequestJson = ComponentsDataFlow.createObjectForSaveRequest(
                    userId, self.searchName, savedObject.searchTerm, savedObject.searchType, savedObject.searchTenants, []);


                coreSearchService.saveSearchRequest.update(searchRequestJson, function (data) {
                    if (data) {
                        // Flags for Bootstrap Alerts
                        self.saveFailedValidation = false;
                        self.saveFailed = false;
                        self.successSave = true;
                    }
                    else {
                        // Flags for Bootstrap Alerts
                        self.saveFailed = true;
                        self.saveFailedValidation = false;
                        self.successSave = false;
                    }
                    $rootScope.waiting = false;
                });
            });
        }
        else {
            self.noSearch = true;
            return;
        }
    }

    self.showHideBusiness = function (elm, rootIndex, childIndex) {
        var domElement = angular.element(elm);

        domElement.toggle('slow');

        if (childIndex !== -1) {
            self.businessItems[rootIndex].children[childIndex].show = !self.businessItems[rootIndex].children[childIndex].show;
        }
        else {
            self.businessItems[rootIndex].show = !self.businessItems[rootIndex].show;
        }
    }

    self.showMorePcs = function (parentIndex, childIndex, expanded) {
        var domElement = angular.element('#pcs-' + parentIndex + childIndex);
        var moreIcon = angular.element('#morePc-' + parentIndex + childIndex + ' i');

        // domElement.toggle('slow');

        self.businessItems[parentIndex].children[childIndex].computers.expanded = !expanded;
        expanded = !expanded;

        if (!expanded) {
            domElement.css('opacity', 0);

            $timeout(function () {
                domElement.toggleClass('expandedElm');
            }, 500);
        }
        else {
            domElement.css('opacity', 1);
            domElement.toggleClass('expandedElm');
        }

        moreIcon.toggleClass('fa-arrow-circle-down fa-arrow-circle-up');
    }

    self.splitMenu = function (length) {
        var splitTwo = Math.floor(length / 2);

        if (splitTwo % 2 === 0) {
            splitTwo += 1;
        }

        return splitTwo;
    }

    self.hideFilters = function (parentIndex, childIndex) {
        var domElement = angular.element('#ciCollection-' + parentIndex + childIndex).find('.ng-table-filters');
        domElement.toggle('slow');
    }

    self.getChilden = function (str, type, rootIndex, childIndex) {
        if (str === 'root') {
            if (childIndex === -1) {
                var rootId = {id: self.businessItems[rootIndex].root.id};

                if (self.businessItems[rootIndex].children.length === 0) {
                    $rootScope.waiting = true;
                    coreSearchService.getCiCollections.query(rootId, function (children) {
                            angular.forEach(children.payload, function (child) {
                                self.businessItems[rootIndex].children.push({
                                    child: child,
                                    menu: [],
                                    selectedMenuItem: {name: 'Computer', key: 'computer'},
                                    show: false,
                                    currentView: {
                                        grandChildrenTableCols: [],
                                        grandChildrenTableParams: null
                                    },
                                    computers: {
                                        computerLabels: [],
                                        expanded: false,
                                        grandChildrenTableCols: [],
                                        grandChildrenTableParams: null
                                    }
                                });
                            });

                            // // Get Computers on 1st Call
                            // angular.forEach(self.businessItems[rootIndex].children, function (child, chIndex) {
                            //     self.getChilden('child', null, rootIndex, chIndex);
                            // });

                            $rootScope.waiting = false;
                        },
                        function (getChildenError) {
                            log.error('ResultsBusinessV2Controller.getChildren :: getChildren :: ' + getChildenError.statusText);

                            $rootScope.waiting = false;
                        });
                }
            }
            else {

            }
        }
        else if (str === 'child') {
            if (type === null) {
                var childId = {id: self.businessItems[rootIndex].children[childIndex].child.id};

                if (self.businessItems[rootIndex].children[childIndex].computers.grandChildrenTableParams === null) {
                    $rootScope.waiting = true;
                    coreSearchService.expandCiCollections.query(childId, function (children) {
                            var tableDataset = [];

                            if (children.payload.length !== 0) {
                                if (self.businessItems[rootIndex].children[childIndex].menu.length === 0) {
                                    var compId = {id: children.payload[0].id};
                                    coreSearchService.getMenuItems.query(compId, function (menuData) {
                                            for (var menuKey in menuData.payload) {
                                                self.businessItems[rootIndex].children[childIndex].menu.push({
                                                    key: menuKey,
                                                    name: _.startCase(menuData.payload[menuKey]),
                                                    active: false
                                                });
                                            }

                                            self.businessItems[rootIndex].children[childIndex].menu.unshift({
                                                key: 'computer',
                                                name: 'Computer',
                                                active: false
                                            });

                                            $rootScope.waiting = false;
                                        },
                                        function (getMenuItemsError) {
                                            log.error('ResultsBusinessV2Controller.getChildren :: getMenuItems :: ' + getMenuItemsError.statusText);

                                            $rootScope.waiting = false;
                                        });
                                }
                            }
                            else {
                                $rootScope.waiting = false;
                                self.businessItems[rootIndex].children[childIndex].noElementsMsg = 'No elements found';
                            }

                            angular.forEach(children.payload, function (child) {
                                var childProperties = child.properties;

                                // Save Computer Item
                                if (!self.computerItems[rootIndex]) {
                                    self.computerItems[rootIndex] = [];
                                }
                                if (!self.computerItems[rootIndex][childIndex]) {
                                    self.computerItems[rootIndex][childIndex] = [];
                                }
                                self.computerItems[rootIndex][childIndex].push(childProperties);

                                self.constructColumnObject(childProperties, self.businessItems[rootIndex].children[childIndex].computers.grandChildrenTableCols);
                                self.addClickableDisplayLabelAsFirstElement(self.businessItems[rootIndex].children[childIndex].computers.grandChildrenTableCols, childProperties, childProperties.display_label);

                                self.businessItems[rootIndex].children[childIndex].computers.computerLabels.push({
                                    field: childProperties.display_label,
                                    id: child.id,
                                    selected: true
                                });
                                tableDataset.push(childProperties);
                            });

                            self.businessItems[rootIndex].children[childIndex].computers.grandChildrenTableParams = new NgTableParams
                            ({
                                    count: 10,
                                    sorting: {filterCol: "asc"}
                                },
                                {
                                    counts: [],
                                    // filterOptions: {
                                    //     filterFilterName: "displayLabelFilter"
                                    // },
                                    dataset: tableDataset
                                });

                            self.businessItems[rootIndex].children[childIndex].currentView.grandChildrenTableCols = self.businessItems[rootIndex].children[childIndex].computers.grandChildrenTableCols;
                            self.businessItems[rootIndex].children[childIndex].currentView.grandChildrenTableParams = self.businessItems[rootIndex].children[childIndex].computers.grandChildrenTableParams;
                        }
                    );
                }
            }
            else {
                var typeKey = type.key;
                if (typeKey === 'computer') {
                    var filteredComputers = [];
                    self.businessItems[rootIndex].children[childIndex].computers.grandChildrenTableCols = [];
                    angular.forEach(self.computerItems[rootIndex][childIndex], function (comp) {
                        angular.forEach(self.businessItems[rootIndex].children[childIndex].computers.computerLabels, function (compLabel) {
                            if (compLabel.selected) {
                                if (compLabel.field === comp.display_label) {
                                    // Update Columns and Columns Filters
                                    self.constructColumnObject(comp, self.businessItems[rootIndex].children[childIndex].computers.grandChildrenTableCols);
                                    self.addClickableDisplayLabelAsFirstElement(self.businessItems[rootIndex].children[childIndex].computers.grandChildrenTableCols, comp, comp.display_label);
                                    filteredComputers.push(comp);
                                }
                            }
                        });
                    });

                    // Change NgTable Dataset
                    self.businessItems[rootIndex].children[childIndex].currentView.grandChildrenTableParams.settings({
                        dataset: filteredComputers
                    });

                    self.businessItems[rootIndex].children[childIndex].currentView.grandChildrenTableCols = self.businessItems[rootIndex].children[childIndex].computers.grandChildrenTableCols;
                    self.businessItems[rootIndex].children[childIndex].currentView.grandChildrenTableParams = self.businessItems[rootIndex].children[childIndex].computers.grandChildrenTableParams;

                    self.businessItems[rootIndex].children[childIndex].selectedMenuItem.name = 'Computer';
                    self.businessItems[rootIndex].children[childIndex].selectedMenuItem.key = type.key;
                }
                else {
                    self.businessItems[rootIndex].children[childIndex].selectedMenuItem.name = type.name;
                    self.businessItems[rootIndex].children[childIndex].selectedMenuItem.key = type.key;

                    // Construct Json for Server Request
                    var jsonRequest = {};
                    jsonRequest.cis = [];
                    angular.forEach(self.businessItems[rootIndex].children[childIndex].computers.computerLabels, function (computer) {
                        if (computer.selected) {
                            jsonRequest.cis.push({id: computer.id, type: computer.field});
                        }
                    });
                    jsonRequest.type = type.key;

                    if (self.businessItems[rootIndex].children[childIndex][type.key]) {
                        var tableDataset = self.businessItems[rootIndex].children[childIndex][type.key].grandChildrenTableParams.settings().dataset;
                        var tableColumns = self.businessItems[rootIndex].children[childIndex][type.key].grandChildrenTableCols;
                        var selectedPCs = self.businessItems[rootIndex].children[childIndex][type.key].selectedPCs;

                        // Find Different PCs
                        var differentPCs = _.differenceBy(jsonRequest.cis, selectedPCs, 'type');

                        if (differentPCs.length === 0) {
                            var newCurrentView = [];
                            var newCurrentColumns = [];

                            angular.forEach(jsonRequest.cis, function (ci) {
                                var newCurrentViewPartial = _.filter(tableDataset, function (row) {
                                    return row.parentName === ci.type;
                                });

                                angular.forEach(newCurrentViewPartial, function (properties) {
                                    // Update Columns and Columns Filters
                                    self.constructColumnObject(properties, newCurrentColumns);
                                    self.addClickableDisplayLabelAsFirstElement(newCurrentColumns, properties, properties.parentName);
                                });

                                newCurrentView = newCurrentView.concat(newCurrentViewPartial);
                            });

                            var currentNgTableParams = new NgTableParams
                            (
                                {
                                    count: 10,
                                    sorting: {filterCol: "asc"}
                                },
                                {
                                    counts: [],
                                    dataset: newCurrentView
                                }
                            )

                            // Update Current View
                            self.businessItems[rootIndex].children[childIndex].currentView.grandChildrenTableCols = newCurrentColumns;
                            self.businessItems[rootIndex].children[childIndex].currentView.grandChildrenTableParams = currentNgTableParams;

                            self.businessItems[rootIndex].children[childIndex].currentView.grandChildrenTableParams.reload();
                        }
                        else {
                            jsonRequest.cis = differentPCs;

                            $rootScope.waiting = true;
                            coreSearchService.getComputerChildren.query(jsonRequest, function (children) {
                                    if (children.payload) {
                                        var newDataset = [];
                                        var newColumns = [];

                                        var children = children.payload;
                                        angular.forEach(children, function (child) {
                                            var childProperties = child.properties;

                                            // Update Columns and Columns Filters
                                            self.constructColumnObject(childProperties, newColumns);
                                            self.addClickableDisplayLabelAsFirstElement(newColumns, childProperties, childProperties.parentName);

                                            newDataset.push(childProperties);
                                        });
                                        newDataset = tableDataset.concat(newDataset);

                                        // newColumns = tableColumns.concat(newColumns);
                                        angular.forEach(tableColumns, function (col) {
                                            var newColumnFilterData = _.find(newColumns, {title: col.title}).filterData;
                                            col.filterData = col.filterData.concat(newColumnFilterData);
                                        });

                                        var updatedNgTableParams = new NgTableParams
                                        (
                                            {
                                                count: 10,
                                                sorting: {filterCol: "asc"}
                                            },
                                            {
                                                counts: [],
                                                dataset: newDataset
                                            }
                                        );

                                        self.businessItems[rootIndex].children[childIndex][type.key].grandChildrenTableCols = tableColumns;
                                        self.businessItems[rootIndex].children[childIndex][type.key].grandChildrenTableParams.settings().dataset = newDataset;
                                        self.businessItems[rootIndex].children[childIndex][type.key].grandChildrenTableParams.reload();

                                        // Extra Flag to know which PCs is selected each time
                                        var newSelectedPCs = selectedPCs.concat(jsonRequest.cis);
                                        self.businessItems[rootIndex].children[childIndex][type.key].selectedPCs = newSelectedPCs;

                                        // Update Current View
                                        self.businessItems[rootIndex].children[childIndex].currentView.grandChildrenTableCols = tableColumns;
                                        self.businessItems[rootIndex].children[childIndex].currentView.grandChildrenTableParams = updatedNgTableParams;
                                        self.businessItems[rootIndex].children[childIndex].currentView.grandChildrenTableParams.reload();
                                    }

                                    $rootScope.waiting = false;
                                },
                                function (getComputerChildrenError) {
                                    log.error('ResultsBusinessV2Controller.getChildren :: getComputerChildren :: ' + getComputerChildrenError.statusText);

                                    $rootScope.waiting = false;
                                });
                        }
                    }
                    else {
                        $rootScope.waiting = true;
                        coreSearchService.getComputerChildren.query(jsonRequest, function (children) {
                                var tableDataset = [];

                                if (children.payload) {
                                    self.businessItems[rootIndex].children[childIndex][type.key] = {
                                        grandChildrenTableCols: [],
                                        grandChildrenTableParams: null
                                    };

                                    angular.forEach(children.payload, function (child) {
                                        var childProperties = child.properties;

                                        self.constructColumnObject(childProperties, self.businessItems[rootIndex].children[childIndex][type.key].grandChildrenTableCols);
                                        self.addClickableDisplayLabelAsFirstElement(self.businessItems[rootIndex].children[childIndex][type.key].grandChildrenTableCols, childProperties, childProperties.parentName);
                                        tableDataset.push(childProperties);
                                    });

                                    self.businessItems[rootIndex].children[childIndex][type.key].grandChildrenTableParams = new NgTableParams
                                    ({
                                            count: 10,
                                            sorting: {filterCol: "asc"}
                                        },
                                        {
                                            counts: [],
                                            // filterOptions: {
                                            //     filterFilterName: "displayLabelFilter"
                                            // },
                                            dataset: tableDataset
                                        });

                                    // Update Current View
                                    self.businessItems[rootIndex].children[childIndex].currentView.grandChildrenTableCols = self.businessItems[rootIndex].children[childIndex][type.key].grandChildrenTableCols;
                                    self.businessItems[rootIndex].children[childIndex].currentView.grandChildrenTableParams = self.businessItems[rootIndex].children[childIndex][type.key].grandChildrenTableParams;

                                    // Extra Flag to know which PCs is selected each time
                                    self.businessItems[rootIndex].children[childIndex][type.key].selectedPCs = jsonRequest.cis;

                                    $rootScope.waiting = false;
                                }

                                $rootScope.waiting = false;
                            },
                            function (getComputerChildrenError) {
                                log.error('ResultsBusinessV2Controller.getChildren :: getComputerChildren :: ' + getComputerChildrenError.statusText);

                                $rootScope.waiting = false;
                            });
                    }
                }
            }
        }
    }

    self.selectItem = function (parentIndex, childIndex, item) {
        // -1 Code for Select All Items
        if (item === -1) {
            angular.forEach(self.businessItems[parentIndex].children[childIndex].computers.computerLabels, function (computer) {
                computer.selected = true;
            });
        }
        // -2 Code for Deselect All Items
        else if (item === -2) {
            angular.forEach(self.businessItems[parentIndex].children[childIndex].computers.computerLabels, function (computer) {
                computer.selected = false;
            });
        }
        // Select specific item
        else {
            item.selected = !item.selected;
        }
    }

    self.showModal = function (id) {
        var jsonId = {id: id};

        $rootScope.waiting = true;
        coreSearchService.getBusinessRowData.query(jsonId, function (data) {
            self.selectedRow = {name: '', properties: []};

            angular.forEach(data.payload.properties, function (prop, key) {
                if (key === 'display_label') {
                    self.selectedRow.name = prop;
                }
                else {
                    self.selectedRow.properties.push({name: _.startCase(key), value: prop});
                }
            });

            $rootScope.waiting = false;

            // Remove inline styles
            angular.element('.is-sticky').removeAttr('style');
            angular.element('#result').modal('show');
        });
    }

    self.constructColumnObject = function (properties, ngTableColumnsObject) {
        for (var colKey in properties) {
            var colOb = _.find(ngTableColumnsObject, {field: colKey});

            if (!colOb) {
                // Construct Column Object
                colOb = {};
                colOb.title = _.startCase(colKey);
                colOb.field = colKey;
                colOb.filter = {};
                colOb.filter[colKey] = 'select';
                colOb.sortable = colKey;
                colOb.show = true;

                self.addColumnObjectInColumnsArrayBasedOnFieldName(ngTableColumnsObject, colOb, properties[colKey]);
            }
            self.addFilterValueToColumnObject(colOb, properties[colKey]);
        }
    }

    self.addClickableDisplayLabelAsFirstElement = function (ngTableColumnsObject, properties, value) {
        var columnIsAlreadyAdded = _.find(ngTableColumnsObject, {field: 'filterCol'});

        if (!columnIsAlreadyAdded) {
            ngTableColumnsObject.unshift({
                title: '',
                field: 'filterCol',
                filter: {filterCol: 'select-multiple'},
                filterData: [],
                show: true
            });
        }
        properties.filterCol = value;
    }

    self.addColumnObjectInColumnsArrayBasedOnFieldName = function (ngTableColumnsObject, columnObject, columnValue) {
        var namePattern = /(name|display|description)/i;
        var parentPattern = /(parent)/i;

        var shouldPutColumnInFront = (namePattern.test(columnObject.title) && !parentPattern.test(columnObject.title)) && columnValue.length !== 0;

        if (shouldPutColumnInFront) {
            ngTableColumnsObject.unshift(columnObject);
        }
        else {
            ngTableColumnsObject.push(columnObject);
        }
    }

    self.addFilterValueToColumnObject = function (columnObject, propertyValue) {
        columnObject.filterData = !columnObject.filterData ? [] : columnObject.filterData;
        var isPropertyValueAlreadyAdded = _.find(columnObject.filterData, {title: propertyValue});

        if (!isPropertyValueAlreadyAdded) {
            columnObject.filterData.push({id: propertyValue, title: propertyValue});
        }
    }
}