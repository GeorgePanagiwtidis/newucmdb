'use strict';

angular
    .module('resultsBusinessV2')
    .filter('displayLabelFilter', function () {
        return function (data, inputOb) {
            var returnedValues = [];

            var filteredCol = Object.keys(inputOb)[0];
            var input = inputOb[filteredCol];
            var inputValues = input.split(' ');

            returnedValues = data.filter(function (dataItem) {
                angular.forEach(input, function(value) {
                    return dataItem[filteredCol].indexOf(value) !== -1;
                });
            });

            // angular.forEach(data, function (dataItem) {
            //     angular.forEach(inputValues, function (value) {
            //         if (dataItem.display_label.indexOf(value) !== -1) {
            //             returnedValues.push(dataItem);
            //         };
            //     });
            // });

            return returnedValues;
        }
    });