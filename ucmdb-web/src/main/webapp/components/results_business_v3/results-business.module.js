(function () {

    'use strict';

    angular.module('resultsBusiness', [
        'resultsBusinessModal',
        'ngTable',
        'ui.bootstrap'
    ]);

})();