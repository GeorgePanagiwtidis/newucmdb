(function () {

    'use strict';

    angular
        .module('resultsBusiness')
        .component('resultsBusiness', {
            templateUrl: 'components/results_business_v3/results-business.template.html',
            controller: ['$rootScope', '$location', '$timeout', '$q', 'log', 'bootboxService', 'coreSearchService',
                'ComponentsDataFlow', 'localStorageService', 'NgTableParams', 'updateService', businessResultsController]
        });

    function businessResultsController($rootScope, $location, $timeout, $q, log, bootboxService, coreSearchService,
                                       ComponentsDataFlow, localStorageService, NgTableParams, updateService) {
        var self = this;

        self.location = $location;

        // -- Modal Variables --
        self.ciAttributes;

        self.ciCollectionOfOpenItem;
        self.selectedRow;
        self.modalEditMode;
        self.isCiUpdatable;
        self.ciType;
        // -- End of Modal Variables --

        // Custom Attribute Columns -- TODO - Always Node Attributes
        self.ciCustomAttributes = [];
        self.ciAttributeNames = {};

        self.$onInit = function () {
            var businessRequestObject = self.initRequestObject();
            self.retrieveBusinessDataFromServer(businessRequestObject).then(function (businessResult) {
                self.businessItems = [];

                var businessResultsPerTenant = businessResult.payload.business;
                for (var tenant in businessResultsPerTenant) {
                    var businessServiceArray = [];
                    angular.forEach(businessResultsPerTenant[tenant], function (businessService) {
                        businessServiceArray.push({
                            businessService: businessService,
                            label: _.startCase(businessService.ciType),
                            ciCollections: [],
                            show: false
                        });
                    });
                    self.businessItems.push({tenantOwner: tenant, businessServices: businessServiceArray, show: false});
                }

                // Empty Results Flag
                self.emptyResults = self.businessItems.length === 0 ? true : false;

                $rootScope.waiting = false;
            });
        }

        self.initRequestObject = function () {
            var searchTerm = localStorageService.get('searchTerm');
            var tenants = localStorageService.get('selectedTenants');

            return ComponentsDataFlow.createBusinessSearchRequest(searchTerm, tenants);
        }

        self.retrieveBusinessDataFromServer = function (businessRequestObject) {
            var deferred = $q.defer();

            $rootScope.waiting = true;
            coreSearchService.getBusinessServices.query(businessRequestObject,
                function (businessData) {
                    deferred.resolve(businessData);
                },
                function (getBusinessServicesError) {
                    var errorMessage = 'businessResultsController.retrieveBusinessDataFromServer :: getBusinessServices :: ' + getBusinessServicesError.statusText;
                    deferred.reject(errorMessage);
                    log.error(errorMessage);
                });

            return deferred.promise;
        }

        self.retrieveCiCollections = function (tenantIndex, tenantBusinessIndex, businessItemObject) {
            var requestCiCollectionsObject = {id: businessItemObject.globalId, ciType: businessItemObject.ciType};

            if (!self.hasAlreadyRetrieved(self.businessItems[tenantIndex].businessServices[tenantBusinessIndex].ciCollections)) {
                $rootScope.waiting = true;
                coreSearchService.getCiCollections.query(requestCiCollectionsObject,
                    function (ciCollections) {
                        angular.forEach(ciCollections.payload, function (ciCollection) {
                            self.businessItems[tenantIndex].businessServices[tenantBusinessIndex].ciCollections.push({
                                ciCollection: ciCollection,
                                nestedCiCollections: [],
                                show: false
                            });
                        });
                        $rootScope.waiting = false;
                    },
                    function (getCiCollectionsError) {
                        log.error('businessResultsController.retrieveCiCollections :: getCiCollections :: ' + getCiCollectionsError.statusText);

                        $rootScope.waiting = false;
                    });
            }
        }

        self.expandCiCollection = function (ciCollection, businessService) {
            var expandCiCollectionRequestObject = {
                id: ciCollection.ciCollection.id,
                ciType: businessService.ciType,
                parentBusinessId: businessService.globalId
            };

            var hasNestedCiCollections = self.hasAlreadyRetrieved(ciCollection.nestedCiCollections);
            var hasCiItems = self.hasAlreadyRetrieved(ciCollection.host_node) || self.hasAlreadyRetrieved(ciCollection.web_server);
            var hasRetrievedNetDevices = self.hasAlreadyRetrieved(ciCollection.netdevice);
            var hasBusinessApplicationItems = self.hasAlreadyRetrieved(ciCollection.business_application);

            if (!(hasNestedCiCollections || hasCiItems || hasBusinessApplicationItems || hasRetrievedNetDevices || ciCollection.noElementsMsg)) {

                $rootScope.waiting = true;
                coreSearchService.expandCiCollections.query(expandCiCollectionRequestObject,
                    function (ciCollectionChildren) {
                        if (self.responseHasPayload(ciCollectionChildren)) {
                            var firstChild = ciCollectionChildren.payload[0];
                            var ciType = firstChild.type;

                            self.getCiAttributes(ciType).then(function () {
                                var data = [];
                                angular.forEach(ciCollectionChildren.payload, function (ciCollectionChild) {
                                    data.push(ciCollectionChild.properties);
                                });

                                self.storeResponseBasedOnCiType(ciType, ciCollection, ciCollectionChildren, data);

                                $rootScope.waiting = false;
                            });
                        }
                        else {
                            ciCollection.noElementsMsg = 'No Elements found!';

                            $rootScope.waiting = false;
                        }
                    },
                    function (expandCiCollectionsError) {
                        log.error('businessResultsController.expandCiCollection :: expandCiCollections :: ' + expandCiCollectionsError.statusText);

                        ciCollection.noElementsMsg = 'No Elements found!';
                        $rootScope.waiting = false;
                    });
            }
        }

        self.expandNestedCiCollection = function (nestedCiCollection) {
            var hasRetrievedComputers = self.hasAlreadyRetrieved(nestedCiCollection.host_node);
            var hasRetrievedNetDevices = self.hasAlreadyRetrieved(nestedCiCollection.netdevice);

            if (!(hasRetrievedComputers || hasRetrievedNetDevices)) {
                var nestedCiCollectionId = nestedCiCollection.ciCollection.global_id;

                $rootScope.waiting = true;
                coreSearchService.getNodeElements.query({id: nestedCiCollectionId},
                    function (nestedCiCollectionData) {
                        if (self.responseHasPayload(nestedCiCollectionData)) {
                            var firstChild = nestedCiCollectionData.payload[0];
                            var ciType = firstChild.type;

                            self.getCiAttributes(ciType).then(function () {
                                var data = [];
                                angular.forEach(nestedCiCollectionData.payload, function (ciCollectionChild) {
                                    data.push(ciCollectionChild.properties);
                                });

                                self.storeResponseBasedOnCiType(ciType, nestedCiCollection, nestedCiCollectionData, data);

                                $rootScope.waiting = false;
                            });
                        }
                        else {
                            nestedCiCollection.noElementsMsg = 'No Elements found!';

                            $rootScope.waiting = false;
                        }
                    },
                    function (getComputersError) {
                        log.error("ResultsBusinessController.expandNestedCiCollection :: getComputers :: " + getComputersError.statusText);

                        nestedCiCollection.noElementsMsg = 'No Elements found!';
                        $rootScope.waiting = false;
                        return;
                    });
            }
        }

        self.getCIsByType = function (ciCollection, menuItem) {
            menuItem = menuItem ? menuItem : ciCollection.selectedMenuItem;
            ciCollection.selectedMenuItem = menuItem;

            var selectedType = menuItem.key;
            if (selectedType === 'host_node') {
                var filterBasedOnColumn = 'display_label';
                self.filterCiItems(ciCollection, selectedType, filterBasedOnColumn);
            }
            else {
                self.getCiAttributes(selectedType).then(function () {
                    self.getCiTypePropertiesObject(ciCollection);
                });
            }
        }

        self.getCiTypePropertiesObject = function (ciCollection) {
            var selectedType = ciCollection.selectedMenuItem.key;
            var selectedComputers = self.filterSelectedComputerItems(ciCollection.computerItems);
            var requestObjectForCiType = self.constructObjectForServerRequest(selectedComputers, selectedType);

            if (ciCollection[selectedType]) {
                // Get already Selected PC's
                var differentPCs = _.differenceBy(requestObjectForCiType.cis, ciCollection[selectedType].selectedPCs, 'type');

                if (differentPCs.length !== 0) {
                    requestObjectForCiType.cis = differentPCs;
                    self.retrieveCiTypePropertiesFromServerForSomePCs(requestObjectForCiType, ciCollection, selectedType);
                }
                else {
                    self.filterCiItems(ciCollection, selectedType, 'parentName');
                }
            }
            else {
                self.retrieveCiTypePropertiesFromServer(requestObjectForCiType, ciCollection, selectedType)
            }
        }

        self.retrieveCiTypePropertiesFromServerForSomePCs = function (requestObject, ciCollection, selectedType) {
            var oldSelectedPCs = ciCollection[selectedType].selectedPCs.length !== 0 ? ciCollection[selectedType].selectedPCs : [];

            $rootScope.waiting = true;
            coreSearchService.getComputerChildren.query(requestObject, function (children) {
                    if (children.payload) {
                        var data = [];
                        angular.forEach(children.payload, function (child) {
                            data.push(child.properties);
                        });

                        // Remove existing 1st Column before creating the whole array and NgTable Object
                        self.removeFilterColumnPropertyFromData(ciCollection[selectedType].data);

                        data = ciCollection[selectedType].data.concat(data);
                        self.createObjectAndPushData(ciCollection, selectedType, data);
                        self.initNgTableWithColumns(ciCollection, selectedType, 'parentName');

                        // Extra Flag to know which PCs is selected each time
                        var newSelectedPCs = oldSelectedPCs.concat(requestObject.cis);
                        ciCollection[selectedType].selectedPCs = newSelectedPCs;
                    }

                    $rootScope.waiting = false;
                },
                function (getComputerChildrenError) {
                    log.error('ResultsBusinessV2Controller.getChildren :: getComputerChildren :: ' + getComputerChildrenError.statusText);

                    $rootScope.waiting = false;
                });
        }

        self.retrieveCiTypePropertiesFromServer = function (requestObjectForCiType, ciCollection, selectedType) {
            $rootScope.waiting = true;
            coreSearchService.getComputerChildren.query(requestObjectForCiType, function (children) {
                    if (children.payload) {
                        var data = [];
                        angular.forEach(children.payload, function (child) {
                            var childProperties = child.properties;
                            data.push(childProperties);
                        });

                        self.createObjectAndPushData(ciCollection, selectedType, data);
                        self.initNgTableWithColumns(ciCollection, selectedType, 'parentName');

                        // Extra Flag to know which PCs is selected each time
                        ciCollection[selectedType].selectedPCs = requestObjectForCiType.cis;
                        $rootScope.waiting = false;
                    }

                    $rootScope.waiting = false;
                },
                function (getComputerChildrenError) {
                    log.error('ResultsBusinessV2Controller.getChildren :: getComputerChildren :: ' + getComputerChildrenError.statusText);

                    $rootScope.waiting = false;
                });
        }

        self.filterCiItems = function (ciCollection, ciType, ciProperty) {
            ciCollection.currentView.tableColumns = ciCollection[ciType].tableColumns;
            ciCollection.currentView.tableParams = ciCollection[ciType].tableParams;

            var selectedComputers = self.filterSelectedComputerItems(ciCollection.computerItems);
            var filteredCiItems = [];
            angular.forEach(selectedComputers, function (computerItem) {
                var partialFilteredCiItems = ciCollection[ciType].data.filter(function (ciItem) {
                    delete ciItem.filterCol;
                    return computerItem.field === ciItem[ciProperty];
                });

                filteredCiItems = filteredCiItems.concat(partialFilteredCiItems);
            });

            // Update CurrentView table & Columns
            self.updateTableColumns(filteredCiItems, ciType, ciCollection, ciProperty);
            ciCollection.currentView.tableParams.settings({
                dataset: filteredCiItems
            });

            angular.forEach(ciCollection.menu, function (menuArray) {
                var menuItem = _.find(menuArray, {key: ciType});
                ciCollection.selectedMenuItem = menuItem ? menuItem : ciCollection.selectedMenuItem;
            })
        }

        self.initNgTableWithColumns = function (ciCollection, ciType, firstCol) {
            ciCollection[ciType].tableColumns = [];
            ciCollection[ciType].tableParams = null;

            // Init Columns
            angular.forEach(ciCollection[ciType].data, function (properties) {
                self.constructColumnObject(ciType, properties, ciCollection[ciType].tableColumns, getSelectedColumnsForCiCollection(ciCollection.ciCollection.id, ciType));
                self.addClickableDisplayLabelAsFirstElement(ciCollection[ciType].tableColumns, properties, firstCol);
            });
            // ciCollection[ciType].tableColumns = changeOrderOfCustomAttributesColumnsAndAddTheToTableCols(ciCollection[ciType].tableColumns);
            sortColumnsBasedOnPriority(ciCollection[ciType].tableColumns);
            addNullFilterValues(ciCollection[ciType].tableColumns);

            // Init NgTableObject
            var ngTableParams = new NgTableParams(
                {
                    count: 10,
                    sorting: {filterCol: "asc"}
                },
                {
                    counts: [],
                    dataset: ciCollection[ciType].data
                });
            ciCollection[ciType].tableParams = ngTableParams;

            // Set Table Filters from Saved Searches
            setTableFilters(ciCollection, ciType);

            // Update Current View
            self.initCiCollectionCurrentView(ciCollection);
            ciCollection.currentView.tableColumns = ciCollection[ciType].tableColumns;
            ciCollection.currentView.tableParams = ngTableParams;
        }

        self.initMenuItems = function (ciCollection, ciType) {
            if (ciType !== 'business_application') {
                if (!self.hasAlreadyRetrieved(ciCollection.menu)) {
                    ciCollection.menu = [];
                    var requestMenuItemsObject = ComponentsDataFlow.createRequestForMenuItems(ciCollection.computerItems);
                    coreSearchService.getMenuItems.query(requestMenuItemsObject, function (menuData) {
                            var menuItems = menuData.payload;
                            for (var menuKey in menuItems) {
                                ciCollection.menu.push({
                                    key: menuKey,
                                    name: menuItems[menuKey],
                                    active: false
                                });
                            }

                            // Insert Current Type "by hand"
                            ciCollection.menu.unshift({
                                key: ciType,
                                name: _.startCase(ciType),
                                active: true
                            });

                            // Select ciType Menu Item
                            ciCollection.selectedMenuItem = _.find(ciCollection.menu, {key: ciType});

                            // Split Menu Array in 2 Sub-arrays
                            ciCollection.menu = _.chunk(ciCollection.menu, ciCollection.menu.length / 2);
                        },
                        function (getMenuItemsError) {
                            log.error('ResultsBusinessV2Controller.getChildren :: getMenuItems :: ' + getMenuItemsError.statusText);
                        });
                }
            }
            else {
                if (!self.hasAlreadyRetrieved(ciCollection.menu)) {
                    // Init Menu Array Structure
                    ciCollection.menu = [];
                    ciCollection.menu[0] = [];

                    ciCollection.menu[0].push({key: 'business_application', name: 'Application', active: true});
                    ciCollection.selectedMenuItem = ciCollection.menu[0][0];

                    $rootScope.waiting = false;
                }
            }

        }

        self.initComputerItems = function (ciCollection, data) {
            if (!self.hasAlreadyRetrieved(ciCollection.computerItems)) {
                ciCollection.computerItems = [];

                angular.forEach(data.payload, function (dataItem) {
                    ciCollection.computerItems.push({
                        field: dataItem.properties.display_label,
                        id: dataItem.id,
                        selected: true
                    });
                });
            }
        }

        self.initCiCollectionCurrentView = function (ciCollection) {
            ciCollection.currentView = {};
            ciCollection.currentView.allColumnsChecked = true;
            ciCollection.currentView.tableColumns = [];
            ciCollection.currentView.tableParams = null;
        }

        self.downloadExcel = function (businessService, tenantIndex, businessIndex) {
            var requestObject = self.constructExcelRequestObject(businessService);

            var notifyOptions = {
                clickToHide: true,
                autoHide: true,
                autoHideDelay: 5000,
                arrowShow: true,
                className: 'info',
                position: 'bottom'
            }

            $('#excel' + tenantIndex + businessIndex).notify("Download in progress...", notifyOptions);
            coreSearchService.downloadExcelService.query(requestObject, function (excelFile) {
                    var excelBinary = excelFile.data;

                    // Check if there are Data
                    if (excelBinary.byteLength !== 0) {
                        download(excelBinary, self.constructExcelFileName(businessService) + '.xls', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    }
                    else {
                        bootboxService.info('Excel Export Info Dialog', 'The <strong>' + requestObject.businessServiceName + '</strong> Business Service is empty. There is nothing to export!');
                    }
                }
                ,
                function (downloadExcelError) {
                    log.error('ResultsBusinessControllerV2.downloadExcel :: downloadExcelService :: ' + downloadExcelError.statusText);
                    bootboxService.alert('Excel Export Error', 'An error occurred while trying to export Business Service Excel file.', false);
                });
        }

        self.saveSearch = function saveSearch() {
            if (!self.searchName) {
                self.saveFailedValidation = true;
                return;
            }
            saveTableFiltersInOptions();

            $rootScope.waiting = true;
            var saveRequestObject = ComponentsDataFlow.createSaveSearchRequestObject(self.searchName);
            coreSearchService.saveSearchRequest.update(saveRequestObject,
                function (data) {
                    showHideValidationMessages(data.payload);
                    $rootScope.waiting = false;
                },
                function (saveSearchRequestError) {
                    log.error('ResultsPageInfrastructureController.saveSearch :: ERROR: ' + saveSearchRequestError.statusText);
                    showHideValidationMessages(false);

                    $rootScope.waiting = false;
                });
        }

        self.showMorePcs = function (tenantIndex, businessTenantIndex, ciCollectionIndex, ci) {
            var domElement = angular.element('#pcs-' + tenantIndex + businessTenantIndex + ciCollectionIndex);
            var moreIcon = angular.element('#morePc-' + tenantIndex + businessTenantIndex + ciCollectionIndex + ' i');

            if (ci.expandedComputerItems) {
                domElement.css('opacity', 0);

                $timeout(function () {
                    domElement.toggleClass('expandedElm');
                }, 500);
            }
            else {
                domElement.css('opacity', 1);
                domElement.toggleClass('expandedElm');
            }

            ci.expandedComputerItems = !ci.expandedComputerItems;
            //moreIcon.toggleClass('fa-arrow-circle-down fa-arrow-circle-up');
            ci.labelValue = ci.expandedComputerItems ? 'Less ...' : (ci.computerItems.length - 4) + ' more ...';
        }

        self.hideFilters = function (tenantIndex, businessTenantIndex, ciCollectionIndex) {
            var domElement = angular.element('#ciCollection-' + tenantIndex + businessTenantIndex + ciCollectionIndex).find('.ng-table-filters');
            domElement.toggle('slow');
        }

        self.hideNestedCiCollectionFilters = function (tenantIndex, businessTenantIndex, ciCollectionIndex, nestedCiCollectionIndex) {
            var domElement = angular.element('#nestedCiCollectionData-' + tenantIndex + businessTenantIndex + ciCollectionIndex + nestedCiCollectionIndex).find('.ng-table-filters');
            domElement.toggle('slow');
        }

        self.selectItem = function (ciCollection, item) {
            // -1 Code for Select All Items
            if (item === -1) {
                angular.forEach(ciCollection.computerItems, function (computer) {
                    computer.selected = true;
                });
            }
            // -2 Code for Deselect All Items
            else if (item === -2) {
                angular.forEach(ciCollection.computerItems, function (computer) {
                    computer.selected = false;
                });
            }
            // Select specific item
            else {
                item.selected = !item.selected;
            }
        }

        self.showModal = function (id, ciType, ciCollection) {
            self.ciType = ciType;

            // We keep track of ciCollection of Open Item too in order to update inline the Table
            self.ciCollectionOfOpenItem = ciCollection;

            var jsonId = {id: id};

            updateService.isCiUpdatable(id, ciType).then(function (response) {
                self.isCiUpdatable = response;
            });

            $rootScope.waiting = true;
            coreSearchService.getBusinessRowData.query(jsonId, function (ciDetails) {
                var ciDetailsPayload = ciDetails.payload;

                var ciProperties = {};
                angular.forEach(ciDetailsPayload.groups, function (groupObject) {
                    _.assign(ciProperties, groupObject.properties);
                });

                self.selectedRow = {
                    globalId: ciDetailsPayload.globalId,
                    name: ciProperties['display_label'],
                    attributeNames: self.ciAttributeNames[ciType] ?
                        getAtttibuteNamesFromRetrievedObject(self.ciAttributeNames[ciType]) : getAtttibuteNamesFromRetrievedObject(self.ciAttributeNames['host_node']),
                    groupAttributes: ciDetailsPayload.groups,
                    decoupledCiProperties: ciProperties
                }

                // Store Original Ci Attributes for Update Purposes
                self.ciAttributes = angular.copy(self.selectedRow);

                updateService.openModalAndResetDefaults(self);
                $rootScope.waiting = false;
            });
        }

        // Helper Functions
        self.responseHasPayload = function (response) {
            if (response) {
                var payloadSize = response.payload ? response.payload.length : 0;
                return payloadSize !== 0;
            }
            else {
                return false;
            }
        }

        self.removeFilterColumnPropertyFromData = function (data) {
            angular.forEach(data, function (dataObject) {
                delete dataObject.filterCol;
            })
        }

        self.storeResponseBasedOnCiType = function (ciType, ciCollection, ciCollectionChildren, data) {
            var isBusinessItem = ciType.toLowerCase().indexOf('collection') === -1;

            if (isBusinessItem) {
                self.createObjectAndPushData(ciCollection, ciType, data);
                self.initNgTableWithColumns(ciCollection, ciType, 'display_label');

                self.initComputerItems(ciCollection, ciCollectionChildren);
                self.initMenuItems(ciCollection, ciType);
            }
            else {
                angular.forEach(data, function (nestedCiCollectionItem) {
                    ciCollection.nestedCiCollections.push({ciCollection: nestedCiCollectionItem, show: false});
                });

                $rootScope.waiting = false;
            }

        }

        self.constructObjectForServerRequest = function (selectedComputers, ciType) {
            var serverRequestObject = {};
            serverRequestObject.cis = [];
            angular.forEach(selectedComputers, function (computer) {
                serverRequestObject.cis.push({id: computer.id, type: computer.field});
            });
            serverRequestObject.type = ciType;

            return serverRequestObject;
        }

        self.updateTableColumns = function (data, ciType, ciCollection, ciProperty) {
            ciCollection.currentView.tableColumns = [];
            angular.forEach(data, function (row) {
                self.constructColumnObject(ciType, row, ciCollection.currentView.tableColumns);
                self.addClickableDisplayLabelAsFirstElement(ciCollection.currentView.tableColumns, row, ciProperty);
            });
            // ciCollection.currentView.tableColumns = changeOrderOfCustomAttributesColumnsAndAddTheToTableCols(ciCollection.currentView.tableColumns);
            sortColumnsBasedOnPriority(ciCollection.currentView.tableColumns);
            addNullFilterValues(ciCollection.currentView.tableColumns);

            setTableFilters(ciCollection, ciType);
        }

        self.filterSelectedComputerItems = function (computerItems) {
            return computerItems.filter(function (computer) {
                return computer.selected === true;
            })
        }

        self.createObjectAndPushData = function (ciCollection, ciType, serverData) {
            ciCollection[ciType] = {
                data: serverData,
                selectedPCs: []
            };
        }


        self.hasAlreadyRetrieved = function (data) {
            if (data) {
                return data.length !== 0;
            }
            else {
                return false;
            }
        }

        self.expandCollapseTenantBusinessDiv = function (index) {
            // Swap show flag
            self.businessItems[index].show = !self.businessItems[index].show;

            var elementId = '#tenant-business-' + index;
            angular.element(elementId).toggle('slow');
        }

        self.expandCollapseBusinessServices = function (tenantIndex, businessServiceIndex, businessService) {
            // Swap show flag
            businessService.show = !businessService.show;

            var elementId = '#business-' + tenantIndex + businessServiceIndex;
            angular.element(elementId).toggle('slow');
        }

        self.expandCollapseCiCollection = function (tenantIndex, businessServiceIndex, ciCollectionIndex) {
            var ciCollection = self.businessItems[tenantIndex].businessServices[businessServiceIndex].ciCollections[ciCollectionIndex];
            // Swap show flag
            ciCollection.show = !ciCollection.show;

            var elementId;
            if (ciCollection.nestedCiCollections.length !== 0) {
                elementId = '#nestedCiCollection-' + tenantIndex + businessServiceIndex + ciCollectionIndex;
            }
            else {
                elementId = '#ciCollection-' + tenantIndex + businessServiceIndex + ciCollectionIndex;
            }

            angular.element(elementId).toggle('slow');
        }

        self.expandCollapseNestedCiCollection = function (tenantIndex, businessTenantIndex, ciCollectionIndex, nestedCiCollectionIndex, nestedCiCollectionObject) {
            var element = angular.element('#nestedCiCollectionData-' + tenantIndex + businessTenantIndex + ciCollectionIndex + nestedCiCollectionIndex);

            nestedCiCollectionObject.show = !nestedCiCollectionIndex.show;
            element.toggle('slow');
        }

        self.constructColumnObject = function (ciType, properties, ngTableColumnsObject, selectedColumns) {
            var ciType = ciType ? ciType : properties['root_class'];
            for (var colKey in properties) {
                var colOb = _.find(ngTableColumnsObject, {field: colKey});

                if (!colOb) {
                    // Construct Column Object
                    colOb = {};
                    colOb.title = self.ciAttributeNames[ciType][colKey] ? self.ciAttributeNames[ciType][colKey].label : _.startCase(colKey);
                    colOb.field = colKey;
                    colOb.filter = {};
                    colOb.filter[colKey] = 'select';
                    colOb.sortable = colKey;
                    colOb.show = selectedColumns && (selectedColumns.indexOf(colKey) !== -1 || selectedColumns.length === 0);
                    colOb.priority = self.ciAttributeNames[ciType][colKey] ? self.ciAttributeNames[ciType][colKey].priority : 100;

                    ngTableColumnsObject.push(colOb);
                }
                self.addFilterValueToColumnObject(colOb, properties[colKey]);
            }
        }

        self.addClickableDisplayLabelAsFirstElement = function (ngTableColumnsObject, properties, ciProperty) {
            var columnIsAlreadyAdded = _.find(ngTableColumnsObject, {field: 'filterCol'});

            if (!columnIsAlreadyAdded) {
                ngTableColumnsObject.unshift({
                    title: '',
                    field: 'filterCol',
                    filter: {filterCol: 'select-multiple'},
                    filterData: [],
                    show: true
                });
            }
            properties.filterCol = ciProperty === 'parentName' ? createConcatinatedValueForFirstColumn(properties) : properties[ciProperty];
        }

        self.addFilterValueToColumnObject = function (columnObject, propertyValue) {
            columnObject.filterData = !columnObject.filterData ? [] : columnObject.filterData;
            var isPropertyValueAlreadyAdded = _.find(columnObject.filterData, {title: propertyValue});

            if (!isPropertyValueAlreadyAdded) {
                columnObject.filterData.push({id: propertyValue, title: propertyValue});
            }
        }

        self.selectAllColumns = function (currentViewObject) {
            var columnsObject = currentViewObject.tableColumns;
            angular.forEach(columnsObject, function (col) {
                col.show = currentViewObject.allColumnsChecked;
            });
        }

        function addNullFilterValues(tableColumns) {
            var nullFilter = {
                id: '!',
                title: '(NULL)'
            }

            angular.forEach(tableColumns, function (col) {
                col.filterData.unshift(nullFilter);
            });
        };

        self.constructExcelFileName = function (businessService) {
            var fileName = businessService.displayLabel;

            var date = new Date();
            var dayStr = date.getDate().toString();
            var day = dayStr.length < 2 ? '0' + dayStr : dayStr;
            var monthStr = (date.getMonth() + 1).toString();
            var month = monthStr.length < 2 ? '0' + monthStr : monthStr;
            var year = date.getFullYear();

            fileName = fileName.concat('_' + day + month + year);

            return fileName;
        }

        self.constructExcelRequestObject = function (businessService) {
            var requestObject = {
                businessServiceName: businessService.displayLabel,
                tenant: businessService.tenantOwner
            }

            return requestObject;
        }

        self.updateDataset = function (displayLabel, updatedAttributes) {
            // Get the Right Dataset
            var dataset = self.ciCollectionOfOpenItem.currentView.tableParams.settings().dataset;

            var tableRow = _.find(dataset, {display_label: displayLabel});

            angular.forEach(updatedAttributes, function (attribute) {
                if (tableRow[attribute.ucmdbPropName]) {
                    tableRow[attribute.ucmdbPropName] = attribute.value;
                }
            });
        }

        self.getCiAttributes = function (ciType) {
            var defer = $q.defer();

            if (!self.ciAttributeNames[ciType]) {
                // Transform CI Type because Custom Attributes belong to 'node' instead of 'host_node'
                ciType = ciType === 'host_node' ? 'node' : ciType;
                coreSearchService.getCiAttributes.query({type: ciType},
                    function (ciAttributes) {
                        var ciAttributesPayload = ciAttributes.payload;

                        storeCiAttributeNames(ciType, ciAttributesPayload);

                        if (ciType === 'node') {
                            storeCiCustomAttributes(ciAttributesPayload);
                        }

                        defer.resolve('ok!');
                    },
                    function (getCiCustomAttributesError) {
                        defer.reject('failed!');
                        log.error('ResultsPageInfrastructureController.getCiCustomAttributes :: ERROR: ' + getCiCustomAttributesError.statusText);
                    });
            }
            else {
                defer.resolve('ok!');
            }

            return defer.promise;
        }

        function sortColumnsBasedOnPriority(tableColumns) {
            tableColumns.sort(function (col1, col2) {
                if (col1.priority === 100 && col2.priority === 100) {
                    if (col1.title < col2.title) return -1;
                    if (col1.title > col2.title) return 1;
                    return 0;
                }
                else {
                    return col1.priority - col2.priority;
                }
            });
        }

        function changeOrderOfCustomAttributesColumnsAndAddTheToTableCols(ngTableColumns) {
            // Separate Custom Attributes Columns and Sort them ASC
            var customAttributes = separateCustomAttributeColumns(ngTableColumns);
            customAttributes.sort(sortColumns);

            // Also Sort the other Columns & Reformat Columns Object
            ngTableColumns.sort(sortColumns);
            return reformatColumnsObject(ngTableColumns, customAttributes, false);
        }

        function reformatColumnsObject(ngTableColumnsObject, customAttributesColumns, customSortFlag) {
            // Custom Sort is a flag for custom sorting based on Client Criteria

            var ngTableColumnsObject = customAttributesColumns.concat(ngTableColumnsObject);

            var updatedTableColumnsObject = [];
            angular.forEach(ngTableColumnsObject, function (col) {
                var namePattern = /(name|display|description)/i;
                var rootClassPattern = /(root_class)/i;
                var parentPattern = /(parent)/i;

                var shouldPutColumnInFront;
                if (customSortFlag) {
                    shouldPutColumnInFront = ((namePattern.test(col.field) || rootClassPattern.test(col.field)) && !parentPattern.test(col.field));
                }
                else {
                    shouldPutColumnInFront = rootClassPattern.test(col.field);
                }

                if (shouldPutColumnInFront) {
                    var columnIndex = col.title.indexOf('CI Type') !== -1 ? 0 : 1;

                    updatedTableColumnsObject.splice(columnIndex, 0, col);
                }
                else {
                    updatedTableColumnsObject.push(col);
                }
            });

            // Re-order 1st Column (Display Label)
            var dlIndex = _.findIndex(updatedTableColumnsObject, {field: 'filterCol'});
            updatedTableColumnsObject.unshift(updatedTableColumnsObject.splice(dlIndex, 1)[0]);

            return updatedTableColumnsObject;
        }

        // Helper Functions
        function storeCiAttributeNames(ciType, ciAttributesPayload) {
            ciType = ciType === 'node' ? 'host_node' : ciType;

            var ciTypeAttributeNames = {};
            angular.forEach(ciAttributesPayload, function (attribute) {
                ciTypeAttributeNames[attribute.attributeName] = {
                    label: attribute.attributeLabel,
                    priority: attribute.attributePriority
                };
            });

            self.ciAttributeNames[ciType] = ciTypeAttributeNames;
        }

        function storeCiCustomAttributes(ciAttributesPayload) {
            self.ciCustomAttributes =
                ciAttributesPayload
                    .filter(function (attribute) {
                        return attribute.workflowRelated;
                    })
                    .map(function (item) {
                        return item.attributeName;
                    });
        }

        function separateCustomAttributeColumns(ngTableColumns) {
            var customAttributeColumns = [];
            angular.forEach(ngTableColumns, function (col) {
                if (self.ciCustomAttributes.indexOf(col.field) !== -1) {
                    customAttributeColumns.push(angular.copy(col));
                }
            });

            // Remove Custom Attribute Columns from Original NgTable Columns
            _.remove(ngTableColumns, function (col) {
                return self.ciCustomAttributes.indexOf(col.field) !== -1;
            });

            return customAttributeColumns;
        }

        // Table Columns Sorting Function
        function sortColumns(col1, col2) {
            if (col1.title > col2.title) {
                return 1;
            }
            else {
                return -1;
            }
        }

        function getAtttibuteNamesFromRetrievedObject(attributesObject) {
            var attributeNames = {};
            for (var attrName in attributesObject) {
                attributeNames[attrName] = attributesObject[attrName].label;
            }

            return attributeNames;
        }

        function saveTableFiltersInOptions() {
            // Get Options object from LocalStorage
            var searchOptions = localStorageService.get('optionList');

            // Check if User has Selected any Column Filters and save them
            var businessOptions = [];
            angular.forEach(self.businessItems, function (businessItem) {
                angular.forEach(businessItem.businessServices, function (businessService) {
                    angular.forEach(businessService.ciCollections, function (ciCollection) {

                        // For Nested CiCollections
                        if (ciCollection.nestedCiCollections) {
                            var nestedCiCollections = ciCollection.nestedCiCollections;

                            angular.forEach(nestedCiCollections, function (nestedCiCollection) {
                                for (var nestedCiCollectionKey in nestedCiCollection) {
                                    if (nestedCiCollection[nestedCiCollectionKey].tableParams && nestedCiCollectionKey !== 'currentView') {
                                        getNgTableFiltersHelper(businessOptions, nestedCiCollection, nestedCiCollectionKey);
                                    }
                                }
                            });
                        }

                        // For Table Results for all CI Types
                        for (var ciCollectionKey in ciCollection) {
                            if (ciCollection[ciCollectionKey].tableParams && ciCollectionKey !== 'currentView') {
                                getNgTableFiltersHelper(businessOptions, ciCollection, ciCollectionKey)
                            }
                        }
                    })
                });
            });

            // Replace Existing filters in LocalStorageObject or Add new Values if Already Exists
            var businessFiltersAlreadyExist = searchOptions.businessOptions.length !== 0;

            if (businessFiltersAlreadyExist) {
                searchOptions.businessOptions = searchOptions.businessOptions.concat(businessOptions);
            }
            else {
                searchOptions.businessOptions = businessOptions;
            }
            localStorageService.set('optionList', searchOptions);
        }

        function getNgTableFiltersHelper(businessOptions, ciCollection, ciCollectionKey) {
            var ciCollectionObject = ciCollection.ciCollection;

            // Init Business Option Item
            var businessOptionItem =
                {
                    // Select Id Based on Object format
                    ciCollectionGlobalId: ciCollectionObject.id ? ciCollectionObject.id : ciCollectionObject.global_id,
                    itemsType: ciCollectionKey,
                    tableFilters: [],
                    selectedColumns: getSelectedColumns(ciCollection[ciCollectionKey].tableColumns)
                }

            var tpFilters = ciCollection[ciCollectionKey].tableParams.filter();
            // Filter non-empty filters
            angular.forEach(tpFilters, function (filterValue, filterName) {
                // Add only not empty filter properties
                if (filterValue.length !== 0) {
                    businessOptionItem.tableFilters.push({propertyName: filterName, propertyValue: filterValue});
                }
            });

            businessOptions.push(businessOptionItem);
        }

        function getSelectedColumns(tableColumns) {
            var selectedColumns = tableColumns
                .filter(function (col) {
                    return col.show;
                })
                .map(function (col) {
                    return col.field;
                });

            return selectedColumns;
        }

        function getSelectedColumnsForCiCollection(ciCollectionId, ciType) {
            var businessOptions = localStorageService.get('optionList').businessOptions;

            var selectedColumns;
            if (businessOptions && businessOptions.length !== 0) {
                var ciCollectionOptions = _.find(businessOptions, {ciCollectionGlobalId: ciCollectionId, itemsType: ciType});
                selectedColumns = ciCollectionOptions.selectedColumns;
            }
            else {
                selectedColumns = []
            }

            return selectedColumns;
        }

        function setTableFilters(ciCollection, ciType) {
            // Make sure that it's Business Item (not nested ci collection etc)
            var isBusinessItem = ciType.toLowerCase().indexOf('collection') === -1;

            if (isBusinessItem) {
                // Store CiCollection Global Id (based on object format) in order to Compare with BusinessOptions object
                var ciCollectionGlobalId =
                    ciCollection.ciCollection.id ? ciCollection.ciCollection.id : ciCollection.ciCollection.global_id;

                // Get Filters Object for Infra Filters from LocalStorage
                var businessOptions = localStorageService.get('optionList').businessOptions;

                angular.forEach(businessOptions, function (businessOption) {
                    if (businessOption.ciCollectionGlobalId === ciCollectionGlobalId) {
                        if (businessOption.itemsType === ciType) {
                            // Construct filters object for NgTable
                            var ngTableFilters = {};
                            angular.forEach(businessOption.tableFilters, function (item) {
                                ngTableFilters[item.propertyName] = item.propertyValue;
                            });

                            // Apply Filters on NgTable object
                            ciCollection[ciType].tableParams.filter(ngTableFilters);
                            ciCollection[ciType].tableParams.reload();
                        }
                    }
                });
            }
        }

        function createConcatinatedValueForFirstColumn(propertiesObject) {
            var colTitle = propertiesObject["display_label"];
            colTitle = colTitle.concat(' (' + propertiesObject['parentName'] + ') ');

            return colTitle;
        }

        function showHideValidationMessages(serverResponse) {
            // Restore Search Name input
            self.searchName = '';

            if (serverResponse) {
                // Flags for Bootstrap Alerts
                self.saveFailedValidation = false;
                self.saveFailed = false;
                self.successSave = true;

                closeSaveSearchModalAfterAWhile();
            }
            else {
                // Flags for Bootstrap Alerts
                self.saveFailed = true;
                self.saveFailedValidation = false;
                self.successSave = false;
            }
        }

        function closeSaveSearchModalAfterAWhile() {
            var saveSearchModal = angular.element('#saveSearchModal');

            $timeout(function () {
                saveSearchModal.modal('hide');

                // Restore Bootstrap Flags
                self.saveFailed = false;
                self.saveFailedValidation = false;
                self.successSave = false;
            }, 1000);
        }
    }
})();