(function () {

    'use strict';

    angular
        .module('userManagement')
        .component('userManagement', {
            templateUrl: 'components/user_management/user-management.template.html',
            controller: ['$rootScope', '$routeParams', '$location', '__env', 'log', 'ComponentsDataFlow', 'coreSearchService', 'bootboxService', 'NgTableParams', UserManagementController]
        });

    function UserManagementController($rootScope, $routeParams, $location, __env, log, ComponentsDataFlow, coreSearchService, bootboxService, NgTableParams) {
        var self = this;
        self.tabs = [];
        self.selectedTab = 'groups';

        self.groupsFilter = {description: ''};
        self.usersFilter = {username: ''};
        self.tenantsFilter = {description: ''};

        // Flags for Null Results
        self.noGroups;
        self.noUsers;
        self.noTenants;

        // Flags for Group Selection
        self.allSearchGroups;
        self.allWorkflowGroups;

        // Table Params Dataset
        self.groupsDataset = [];
        self.usersDataset = [];
        self.tenantsDataset = [];

        // Table Params for each Category
        self.groupsTP;
        self.usersTP;
        self.tenantsTP;

        // Modal Variables
        self.modalTitle;
        self.modalTitleDescription;
        self.modalData;

        // Tenants List for 'Give Access'
        self.tenantsDB = [];
        self.groupsDB = [];

        self.otherEnvUrl = __env.OTHER_ENV_URL;
        self.isNewUcmdb  = __env.NEW_UCMDB;

        self.$onInit = function () {
            self.tabs.push({name: 'GROUPS', field: 'groups', active: false});
            self.tabs.push({name: 'USERS', field: 'users', active: false});
            self.tabs.push({name: 'TENANTS', field: 'tenants', active: false});

            self.getData();
        }

        self.getData = function () {
            $rootScope.waiting = true;
            coreSearchService.getGroups.query({'param': 'false'}, function (data) {
                self.getUsers();
                self.getTenants();

                if (data.payload) {
                    if (data.payload.length !== 0) {
                        self.noGroups = false;

                        angular.forEach(data.payload, function (group) {
                            self.groupsDB.push(
                                {
                                    id: group.id,
                                    description: group.description,
                                    tenants: group.tenants,
                                    ucmdbUsers: group.ucmdbUsers,
                                    checked: false,

                                    isWorkflowRelated: group.isWorkflowRelated
                                }
                            );
                        });

                        self.groupsDataset = data.payload;
                        self.groupsTP = new NgTableParams(
                            {
                                sorting: {description: "asc"},
                                page: 1,
                                count: 10,
                                filter: self.groupsFilter
                            },
                            {
                                counts: [],
                                dataset: self.groupsDataset
                            }
                        );
                    }
                }
                else {
                    self.noGroups = true;
                }

                var tab = $routeParams.tab;
                self.changeTab(tab);

                $rootScope.waiting = false;
            });
        }

        self.changeTab = function (tab) {
            // Selected Tab
            self.selectedTab = tab;

            angular.forEach(self.tabs, function (tabOb) {
                if (tabOb.name === tab.toUpperCase()) {
                    tabOb.active = true;
                }
                else {
                    tabOb.active = false;
                }
            });

            tab = '#' + tab;
            angular.element('.nav-tabs a[data-target="' + tab + '"]').tab('show');
        }

        self.showModal = function (title, description, data) {
            self.modalTitle = title;
            self.modalTitleDescription = description;
            self.modalData = data;

            angular.element('#modal_info').modal('show');
        }

        self.giveAccessToGroups = function (item) {
            var editGroupObject;

            $rootScope.waiting = true;
            // Uncheck All Checkboxes
            self.allSearchGroups = false;
            self.allWorkflowGroups = false;
            angular.forEach(self.groupsDB, function (group) {
                if (group.checked) {
                    // Un-check after Save
                    group.checked = false;

                    var groupIndex = self.groupsDB.indexOf(group);

                    if (item.username) {
                        var newUsers = group.ucmdbUsers;
                        newUsers.push({id: item.id, username: item.username});
                        editGroupObject = ComponentsDataFlow.createGroupUpdateRequestObject(group.id, newUsers, group.tenants, group.isWorkflowRelated);

                        // Update Current Model Data
                        self.groupsDataset[groupIndex].numberOfUsersInEachGroup += 1;
                        self.groupsTP.reload();
                    }
                    else {
                        var tenantIndex = self.tenantsDataset.indexOf(item);

                        var newTenants = group.tenants;
                        newTenants.push({id: item.id, description: item.description});
                        editGroupObject = ComponentsDataFlow.createGroupUpdateRequestObject(group.id, group.ucmdbUsers, newTenants, group.isWorkflowRelated);

                        // Update Current Model Data for Groups
                        self.groupsDataset[groupIndex].tenants = newTenants;
                        self.groupsTP.reload();

                        // Update Current Model Data for Tenants
                        self.tenantsDataset[tenantIndex].tenantGroups.push({
                            id: group.id,
                            description: group.description
                        });
                        self.tenantsTP.reload();
                    }

                    coreSearchService.updateGroupRequest.update(editGroupObject, function (data) {
                        if (data) {
                            if (item.username) {
                                var userObject = _.find(self.usersDataset, {id: item.id});
                                if (!userObject.group) {
                                    userObject.group = [];
                                }
                                userObject.group.push(group);
                            }
                            else {
                                // -- Update to Tenants Dataset has already DONE at line 160

                                // var tenantObject = _.find(self.tenantsDataset, {id: item.id});
                                // tenantObject.tenantGroups.push({
                                //     id: group.id,
                                //     description: group.description
                                // });

                                // -- END --
                            }

                            self.groupsTP.reload();
                            self.usersTP.reload();
                            self.tenantsTP.reload();
                        }
                    });
                }
            });
            $rootScope.waiting = false;
        }

        self.giveAccessToTenants = function (group) {
            var selectedTenants = [];

            angular.forEach(self.tenantsDB, function (item) {
                if (item.checked) {
                    selectedTenants.push({id: item.id, description: item.description});
                    item.checked = false;
                }
            });
            var groupUsers = group.ucmdbUsers;
            var groupTenants = selectedTenants;

            var newGroupObject = ComponentsDataFlow.createGroupUpdateRequestObject(group.id, groupUsers, groupTenants, group.isWorkflowRelated);

            $rootScope.waiting = true;
            coreSearchService.updateGroupRequest.update(newGroupObject, function (data) {
                if (data) {
                    // Find Corresponding Object in Dataset
                    var groupObject = _.find(self.groupsDataset, {id: group.id});

                    angular.forEach(selectedTenants, function (tenant) {
                        groupObject.tenants.push(tenant);
                    });

                    self.groupsTP.reload();
                }

                $rootScope.waiting = false;
            });
        }

        // -- GROUPS METHODS --
        self.createGroup = function () {
            $location.path('user_management_create');
        }

        self.editGroup = function (group) {
            ComponentsDataFlow.createObjectEditGroup(group.id, group.description, group.ucmdbUsers, group.tenants, group.isWorkflowRelated);

            $location.path('/user_management_create');
        }

        self.removeGroupConfirmation = function (group) {
            var message = 'You are going to delete Group <strong>' + group.description + '</strong>. Are you sure?';
            bootboxService.confirm('Delete Group', message, [], function (result) {
                if (result) {
                    self.removeGroup(group);
                }
            });
        }

        self.removeGroup = function (group) {
            var groupDeleteRequest = ComponentsDataFlow.createGroupRemovalRequestObject(group.id, group.description);

            $rootScope.waiting = true;
            coreSearchService.removeGroupRequest.update(groupDeleteRequest, function (data) {
                if (data) {
                    var deletedGroupIndex = self.groupsDataset.indexOf(group);
                    self.groupsDataset.splice(deletedGroupIndex, 1);
                    self.groupsTP.reload();

                    $rootScope.waiting = false;
                }
            });
        }

        // -- USERS METHODS --
        self.getUsers = function () {
            coreSearchService.getAllUsers.query(function (data) {
                if (data.payload) {
                    if (data.payload.length !== 0) {
                        var usersResponse = data.payload;

                        var usersWithoutGroups = usersResponse.filter(function (user) {
                            return !user.group;
                        });
                        usersWithoutGroups = _.sortBy(usersWithoutGroups, ['username']);

                        var usersWithGroups = usersResponse.filter(function (user) {
                            return user.group;
                        });
                        usersWithGroups = _.sortBy(usersWithGroups, ['username']);

                        self.usersDataset = self.usersDataset.concat(usersWithoutGroups);
                        self.usersDataset = self.usersDataset.concat(usersWithGroups);

                        self.usersTP = new NgTableParams(
                            {
                                page: 1,
                                count: 10,
                                filter: self.usersFilter
                            },
                            {
                                counts: [],
                                dataset: self.usersDataset
                            }
                        )
                    }
                }
                else {
                    self.noUsers = true;
                }
            });
        }

        self.deleteUserConfirmation = function (username, id) {
            var message = 'You are going to delete user <strong>' + username + '</strong>. Are you sure?';

            bootboxService.confirm('Delete User', message, [], function (result) {
                if (result) {
                    self.deleteUserFromGroup(username, id);
                }
            });
        }

        self.deleteUserFromGroup = function (username, id) {
            var usersDataset = self.usersTP.settings().dataset;

            // Remove User from Dataset
            usersDataset = _.remove(usersDataset, function (user) {
                return user.id === id;
            });

            self.usersTP.reload();

            var jsonUser = ComponentsDataFlow.createRequestForUserInactivation(id, username);

            coreSearchService.makeUserInactive.query(jsonUser, function (data) {

            });
        }

        // -- TENANTS METHODS --
        self.getTenants = function () {
            coreSearchService.getTenantsUsersGroups.query(function (data) {
                if (data.payload) {

                    if (data.payload.length !== 0) {
                        self.noTenants = false;

                        angular.forEach(data.payload, function (tenant) {
                            self.tenantsDB.push({id: tenant.id, description: tenant.description, checked: false});
                        });

                        self.tenantsDataset = data.payload;
                        self.tenantsTP = new NgTableParams(
                            {
                                page: 1,
                                count: 10,
                                filter: self.tenantsFilter,
                                sorting: {description: "asc"},
                            },
                            {
                                counts: [],
                                dataset: self.tenantsDataset
                            }
                        );


                    }
                    else {
                        self.noTenants = true;
                    }
                }
                else {
                    self.noTenants = true;
                }
            });
        }

        self.changeTenantStatus = function (tenant) {
            coreSearchService.updateTenant.query(tenant,
                function (response) {

                },
                function (updateTenantError) {
                    // Restore Tenant Status
                    tenant.notMigrated = !tenant.notMigrated;

                    var errorMessage = (updateTenantError.data && updateTenantError.data.error)
                        ? updateTenantError.data.error.message : 'Error while updating Tenant Status';

                    bootboxService.alert('Server Error', errorMessage, false);
                    log.error('UserManagementController.changeTenantStatus :: Error: ' + updateTenantError.statusText);
                });
        }

        // Helper Methods
        self.checkAllGroups = function (isWorkflowGroup) {
            if (isWorkflowGroup) {
                self.checkAllWorkflowGroups();
            }
            else {
                self.checkAllSearchGroups();
            }
        }

        self.checkAllWorkflowGroups = function () {
            angular.forEach(self.groupsDB, function (group) {
                if (group.isWorkflowRelated) {
                    if (self.allWorkflowGroups) {
                        group.checked = true;
                    }
                    else {
                        group.checked = false;
                    }
                }
            });
        }

        self.checkAllSearchGroups = function () {
            angular.forEach(self.groupsDB, function (group) {
                if (!group.isWorkflowRelated) {
                    if (self.allSearchGroups) {
                        group.checked = true;
                    }
                    else {
                        group.checked = false;
                    }
                }
            });
        }
    }

})();