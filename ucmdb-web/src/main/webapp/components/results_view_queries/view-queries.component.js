(
    function () {
        'use strict';

        angular
            .module('resultsViewQueries')
            .component('resultsViewQueries', {
                templateUrl: 'components/results_view_queries/view-queries.template.html',
                controller: ViewQueriesController
            });

        ViewQueriesController.$inject = ['$rootScope', '$q', '$location', '$timeout', 'log', 'localStorageService', 'ComponentsDataFlow', 'coreSearchService', 'bootboxService', 'updateService', 'NgTableParams'];

        function ViewQueriesController($rootScope, $q, $location, $timeout, log, localStorageService, ComponentsDataFlow, coreSearchService, bootboxService, updateService, NgTableParams) {
            var self = this;

            self.$onInit = onInit;
            self.showQueryFilters = showQueryFilters;
            self.showParentCiProperties = showParentCiProperties;
            self.showHostNodeProperties = showHostNodeProperties;
            self.expandGuests = expandGuests;
            self.downloadExcel = downloadExcel;
            self.saveSearch = saveSearch;
            self.getCiDetails = getCiDetails;
            self.updateDataset = updateDataset;

            self.viewQueryObject;
            self.queryResults;
            self.queryProperties;
            self.location = $location;

            function onInit() {
                self.viewQueryObject = constructRequestObject();

                $rootScope.waiting = true;
                getViewQueryResults(self.viewQueryObject)
                    .then(function (successResponse) {
                            self.queryResults = successResponse;
                            self.queryResultsForView = transformResponseToViewObject(successResponse);
                            getQueryProperties();

                            self.noResults = (self.queryResults.length === 0);
                            $rootScope.waiting = false;
                        },
                        function (failedResponse) {
                            log.error(failedResponse);

                            self.noResults = true;
                            $rootScope.waiting = false;
                        }
                    );
            }

            function showQueryFilters() {
                return self.viewQueryObject.queryFilters[0].attributeLabel + ': ' + self.viewQueryObject.queryFilters[0].attributeValue;
            }

            function showParentCiProperties(ci) {
                var propsStr = '';

                angular.forEach(ci.properties, function (prop) {
                    propsStr = propsStr ? propsStr.concat(', ' + prop) : prop;
                });

                return propsStr;
            }

            function showHostNodeProperties(host) {
                var hostProperties = '';

                if (host.properties) {
                    var hostPropertiesObject = host.properties;
                    hostProperties = hostPropertiesObject.application_name + '(' + hostPropertiesObject.display_label + ')';
                }

                return hostProperties;
            }

            function expandGuests(parent, index) {
                if (!parent.expandedGuests) {
                    // Construct Cols
                    var tableColumns = constructTableColumns(parent.guests);
                    // Construct Data
                    var tableData = constructTableData(parent.guests);

                    parent.guestTableColumns = tableColumns;
                    parent.guestTableParams = new NgTableParams({
                        sorting: {display_label: "asc"},
                        count: 10
                    }, {dataset: tableData, counts: []});
                }
                else {
                    parent.guestTableColumns = [];
                    parent.guestTableParams = null;
                }

                parent.expandedGuests = !parent.expandedGuests;
                angular.element('#host-' + index).toggle('slow');
            }

            function downloadExcel() {
                showNotifyPopup('#excel-btn');
                coreSearchService.exportViewQueriesExcel.export(self.viewQueryObject,
                    function (response) {
                        download(response.data, constructExcelFileName(), 'application/vnd.ms-excel');
                    },
                    function () {
                        bootboxService.alert('Server Error', 'An error occurred while Exporting Excel', false);
                    });
            }

            function saveSearch() {
                if (!self.searchName) {
                    self.saveFailedValidation = true;
                    return;
                }

                $rootScope.waiting = true;
                var saveRequestObject = ComponentsDataFlow.createSaveSearchRequestObject(self.searchName);
                coreSearchService.saveSearchRequest.update(saveRequestObject,
                    function (data) {
                        showHideValidationMessages(data.payload);
                        $rootScope.waiting = false;
                    },
                    function (saveSearchRequestError) {
                        log.error('ViewQueriesController.saveSearch :: ERROR: ' + saveSearchRequestError.statusText);
                        showHideValidationMessages(false);

                        $rootScope.waiting = false;
                    });
            }

            function getCiDetails(hostCi, guestCi) {
                // Required Fields for Modal
                self.isCiUpdatable = false;
                self.modalEditMode = false;
                self.originalItem;
                self.businessItem;
                self.ciType = guestCi['root_class'];

                var relatedCiGlobalId = _.find(hostCi.guests, {display_label: guestCi['display_label']}).id;

                $rootScope.waiting = true;
                $q.all
                ({
                    updatableCi: isCiUpdatable(relatedCiGlobalId, self.ciType),
                    ciDetails: getCiDetailsServiceCall(relatedCiGlobalId, self.ciType),
                    ciAttributes: getCiAttributes(self.ciType)
                })
                    .then(
                        function (successResponses) {
                            self.isCiUpdatable = successResponses.updatableCi;

                            self.businessItem = {
                                globalId: successResponses.ciDetails.ciDetailsPayload.globalId,
                                name: successResponses.ciDetails.ciProperties['display_label'],
                                attributeNames: successResponses.ciAttributes,
                                groupAttributes: successResponses.ciDetails.ciDetailsPayload.groups,
                                decoupledCiProperties: successResponses.ciDetails.ciProperties
                            }

                            // Store Original Ci Attributes for Update Purposes
                            self.originalItem = angular.copy(self.businessItem);

                            updateService.openModalAndResetDefaults(self);

                            $rootScope.waiting = false;
                        },
                        function (failedResponses) {
                            log.error(failedResponses);

                            $rootScope.waiting = false;
                        });
            }

            function updateDataset(displayLabel, updatedAttributes) {
                var queriesLength = self.queryResultsForView.length;
                for (var qIndex = 0; qIndex < queriesLength; qIndex++) {
                    var currentQueryItem = self.queryResultsForView[qIndex];
                    var foundGuest = _.find(currentQueryItem.guests, {display_label: displayLabel});

                    if (foundGuest) {
                        angular.forEach(updatedAttributes, function (attr) {
                            if (foundGuest[attr.ucmdbPropName]) foundGuest[attr.ucmdbPropName] = attr.value;
                        });

                        currentQueryItem.guestTableParams.reload();

                        break;
                    }
                }

                coreSearchService.clearViewQueriesCache.get();
            }

            // Helper Functions
            function constructRequestObject() {
                var selectedTenants =
                    localStorageService
                        .get('selectedTenants')
                        .map(function (tenant) {
                            return tenant.name;
                        });

                return {
                    queryName: localStorageService.get('searchTerm'),
                    selectedTenants: selectedTenants,
                    queryFilters: localStorageService.get('optionList').infraOptions.infraOptions
                }
            }

            function getViewQueryResults(requestObject) {
                var defer = $q.defer();

                coreSearchService.viewQuerySearch.query(requestObject,
                    // Success Response
                    function (queryResponse) {
                        if (queryResponse.payload) {
                            defer.resolve(queryResponse.payload);
                        }
                        else if (queryResponse.error) {
                            defer.reject('ViewQueriesController.getViewQueryResults :: ERROR: ' + queryResponse.error);
                        }
                    },
                    // Failed Response
                    function (queryResponseError) {
                        defer.reject('ViewQueriesController.getViewQueryResults :: ERROR: ' + queryResponseError.statusText);
                    }
                );

                return defer.promise;
            }

            function getQueryProperties() {
                var queryName = self.viewQueryObject.queryName;

                coreSearchService.getViewQueryProperties.query({name: queryName},
                    function (viewQueryPropertiesResponse) {
                        self.queryProperties = viewQueryPropertiesResponse.payload;
                    },
                    function (getViewQueryPropertiesError) {
                        log.error('ViewQueriesController.getNodeCiAttributes :: ERROR: ' + getViewQueryPropertiesError);
                    });
            }

            function transformResponseToViewObject(successResponse) {
                var transformedObject = successResponse.map(function (ciObject) {
                    var hostDisplayLabel = ciObject.ci.properties.display_label;
                    return {
                        host: ciObject.ci,
                        hostDisplayLabel: hostDisplayLabel,
                        guests: ciObject.relationCIs.map(function (guest) {
                            var guestObject = {id: guest.relatedCiProperties.id};
                            var businessServices = {BusinessServices: guest.businessServiceDisplayLabel.join()};

                            _.assign(guestObject, guest.relatedCiProperties.properties, businessServices);

                            return guestObject;
                        }),
                        guestTableParams: null,
                        guestTableColumns: [],
                        expandedGuests: false
                    };
                });

                return transformedObject;
            }

            function constructTableColumns(guests) {
                // Construct Filter Vales
                var guestsFilterData = constructTableFilterValues(guests);

                var tableColumns = [];
                angular.forEach(self.queryProperties, function (col) {
                    var colObject = {
                        field: col.propertyName,
                        title: col.propertyLabel,
                        sortable: col.propertyName,
                        filter: {},
                        filterData: guestsFilterData[col.propertyName],
                        show: true,
                        priority: col.propertyPriority ? col.propertyPriority : 100
                    }
                    colObject.filter[col.propertyName] = 'select';

                    tableColumns.push(colObject);
                });

                sortTableColumnsBasedOnAttributePriorities(tableColumns);
                return tableColumns;
            }

            function sortTableColumnsBasedOnAttributePriorities(tableColumns) {
                tableColumns.sort(function (col1, col2) {
                    if (col1.priority === 100 && col2.priority === 100) {
                        if (col1.title < col2.title) return -1;
                        if (col1.title > col2.title) return 1;
                        return 0;
                    }
                    else {
                        return col1.priority - col2.priority;
                    }
                });
            }

            function constructTableFilterValues(guests) {
                var guestsFilterData = {};
                angular.forEach(guests, function (guest) {
                    for (var prop in guest) {
                        var propValue = guest[prop];

                        if (guestsFilterData[prop]) {
                            if (!_.find(guestsFilterData[prop], {id: propValue})) {
                                guestsFilterData[prop].push({id: propValue, title: propValue});
                            }
                        }
                        else {
                            guestsFilterData[prop] = [{id: propValue, title: propValue}];
                        }
                    }
                });

                return guestsFilterData;
            }

            function constructTableData(guests) {
                return guests.map(function (guest) {
                    return guest;
                });
            }

            function showHideValidationMessages(serverResponse) {
                // Restore Search Name input
                self.searchName = '';

                if (serverResponse) {
                    // Flags for Bootstrap Alerts
                    self.saveFailedValidation = false;
                    self.saveFailed = false;
                    self.successSave = true;

                    closeSaveSearchModalAfterAWhile();
                }
                else {
                    // Flags for Bootstrap Alerts
                    self.saveFailed = true;
                    self.saveFailedValidation = false;
                    self.successSave = false;
                }
            }

            function closeSaveSearchModalAfterAWhile() {
                var saveSearchModal = angular.element('#saveSearchModal');

                $timeout(function () {
                    saveSearchModal.modal('hide');

                    // Restore Bootstrap Flags
                    self.saveFailed = false;
                    self.saveFailedValidation = false;
                    self.successSave = false;
                }, 1000);
            }

            // Helper Methods for Excel
            function showNotifyPopup(elm) {
                var notifyOptions = {
                    clickToHide: true,
                    autoHide: true,
                    autoHideDelay: 5000,
                    arrowShow: true,
                    className: 'info',
                    position: 'left'
                }

                $(elm).notify("Download in progress...", notifyOptions);
            }

            function constructExcelFileName() {
                var fileName = '';

                // Set Query Name
                fileName = fileName.concat(self.viewQueryObject.queryName + '-');
                // Set Selected Tenants
                fileName = fileName.concat(self.viewQueryObject.selectedTenants.join('_'));
                // Set File Extension
                fileName = fileName.concat('.xls');

                return fileName;
            }

            // Helper Methods for uCMDB Info Window
            function isCiUpdatable(ciGlobalId, ciType) {
                var ciUpdatableDefer = $q.defer();

                updateService.isCiUpdatable(ciGlobalId, ciType).then(function (isUpdatable) {
                    ciUpdatableDefer.resolve(isUpdatable);
                });

                return ciUpdatableDefer.promise;
            }

            function getCiDetailsServiceCall(ciGlobalId, ciType) {
                var ciDetailsDefer = $q.defer();

                coreSearchService.getBusinessRowData.query({id: ciGlobalId, type: ciType},
                    function (ciDetails) {
                        var ciDetailsPayload = ciDetails.payload;

                        var ciProperties = {};
                        angular.forEach(ciDetailsPayload.groups, function (groupObject) {
                            _.assign(ciProperties, groupObject.properties);
                        });

                        ciDetailsDefer.resolve({
                            ciDetailsPayload: ciDetailsPayload,
                            ciProperties: ciProperties
                        });
                    }, function (getCiDetailsError) {
                        ciDetailsDefer.reject('ViewQueriesController.getCiDetails :: getCiDetailsError' + getCiDetailsError.statusText);
                    });

                return ciDetailsDefer.promise;
            }

            function getCiAttributes(ciType) {
                var ciAttributesDefer = $q.defer();

                var ciAttributeNames = [];
                coreSearchService.getCiAttributes.query({type: ciType},
                    function (ciAttributes) {
                        var ciAttributesPayload = ciAttributes.payload;

                        angular.forEach(ciAttributesPayload, function (attribute) {
                            ciAttributeNames[attribute.attributeName] = {
                                label: attribute.attributeLabel,
                                priority: attribute.attributePriority
                            };
                        });

                        ciAttributesDefer.resolve(getAtttibuteNamesFromRetrievedObject(ciAttributeNames));
                    },
                    function (getCiAttributesError) {
                        ciAttributesDefer.reject('ResultsPageInfrastructureController.getCiCustomAttributes :: ERROR: ' + getCiAttributesError.statusText);
                    });

                return ciAttributesDefer.promise;
            }

            function getAtttibuteNamesFromRetrievedObject(attributesObject) {
                var attributeNames = {};
                for (var attrName in attributesObject) {
                    attributeNames[attrName] = attributesObject[attrName].label;
                }

                return attributeNames;
            }
        }
    }
)();