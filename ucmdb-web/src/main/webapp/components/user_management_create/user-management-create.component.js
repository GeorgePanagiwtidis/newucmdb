(function () {

    'use strict';

    angular
        .module('userManagementCreate')
        .component('userManagementCreate', {
            templateUrl: 'components/user_management_create/user-management-create.template.html',
            controller: ['$rootScope', '$location', '$timeout', 'bootboxService', 'ComponentsDataFlow', 'coreSearchService', 'log', UserManagementCreateController]
        });

    function UserManagementCreateController($rootScope, $location, $timeout, bootboxService, ComponentsDataFlow, coreSearchService, log) {
        var self = this;

        self.tenants = [];

        self.usersFromDb = [];
        self.selectedUsers = [];

        self.isAllChecked = false;

        // Flag to show/hide the alert
        self.showAlert = false;

        self.isNewGroup = true;
        self.groupId;
        self.groupName;
        self.isWorkflowRelatedGroup;

        self.pageTitle = 'Create Group';
        self.groupButton = 'CREATE';

        self.$onInit = function () {
            $rootScope.waiting = true;
            coreSearchService.getTenants.query(function (data) {
                if (data.payload) {
                    data.payload.forEach(function (tenant) {
                        self.tenants.push({id: tenant.id, description: tenant.description, checked: false});
                    });
                }

                coreSearchService.getAllUsers.query(function (data) {
                    if (data.payload) {
                        data.payload.forEach(function (user) {
                            self.usersFromDb.push({id: user.id, username: user.username, selected: false});
                        });
                        self.completeFields();
                    }
                });

                $rootScope.waiting = false;
            });
        }

        self.completeFields = function () {
            var editGroup = ComponentsDataFlow.getObjectForEditGroup();

            if (editGroup) {
                self.isNewGroup = false;
                self.pageTitle = 'Edit Group';
                self.groupButton = 'SAVE CHANGES';

                self.groupId = editGroup.groupId;
                self.groupName = editGroup.groupName;
                self.isWorkflowRelatedGroup = editGroup.isWorkflowRelatedGroup;

                editGroup.selectedUsers.forEach(function (user) {
                    self.usersFromDb.forEach(function (userDb, index, array) {
                        if (userDb.username === user.username) {
                            array[index].selected = true;
                        }
                    });
                });
                self.moveUsers('onInit');

                var selectedTenantsNum = 0;
                editGroup.accessTenants.forEach(function (tenant) {
                    self.tenants.forEach(function (tenantDb, index, array) {
                        if (tenantDb.id === tenant.id) {
                            array[index].checked = true;
                            selectedTenantsNum++;
                        }
                    });
                });
                if (selectedTenantsNum === self.tenants.length) {
                    self.isAllChecked = true;
                }
            }
        }

        self.selectUser = function (user) {
            user.selected = user.selected ? false : true;
        }

        self.moveUsers = function (table) {
            if (table === 'selectedUsers') {
                if (self.selectedUsers) {
                    for (var key = self.selectedUsers.length - 1; key >= 0; key--) {
                        if (self.selectedUsers[key].selected) {
                            self.selectedUsers[key].selected = false;
                            self.usersFromDb.push(self.selectedUsers[key]);
                            self.selectedUsers.splice(key, 1);
                        }
                    }
                }
            }
            else {
                for (key = self.usersFromDb.length - 1; key >= 0; key--) {
                    if (self.usersFromDb[key].selected) {
                        self.usersFromDb[key].selected = false;
                        self.selectedUsers.push(self.usersFromDb[key]);
                        self.usersFromDb.splice(key, 1);
                    }
                }
            }

        }

        self.checkTenants = function checkAllTenants(tenant) {
            if (typeof(tenant) === 'string') {
                if (tenant === 'All') {
                    if (!self.isAllChecked) {
                        self.tenants.forEach(function (item) {
                            item.checked = true;
                            self.isAllChecked = true;
                        });
                    }
                    else {
                        self.tenants.forEach(function (item) {
                            item.checked = false;
                            self.isAllChecked = false;
                        });
                    }
                }
            }
            else {
                var index = self.tenants.indexOf(tenant);

                if (self.tenants[index].checked) {
                    if (self.isAllChecked) {
                        self.isAllChecked = false;
                    }
                    self.tenants[index].checked = false;
                }
                else {
                    var numOfSelectedTenants = 0;
                    self.tenants[index].checked = true;

                    self.tenants.forEach(function (currentTenant) {
                        if (currentTenant.checked) {
                            numOfSelectedTenants++;
                        }
                    });
                    if (numOfSelectedTenants === self.tenants.length) {
                        self.isAllChecked = true;
                    }
                }
            }
        };

        self.validateFields = function () {
            var selectedTenants = self.tenants.filter(function (currentTenant) {
                if (currentTenant.checked) {
                    return {id: currentTenant.id, description: currentTenant.description};
                }
            });

            if (self.groupName && self.selectedUsers.length !== 0 && selectedTenants) {
                return true;
            }
            else {
                return false;
            }
        }

        self.createGroup = function () {
            if (!self.validateFields()) {
                self.showAlert = true;
            }
            else {
                var groupUsers = [];
                self.selectedUsers.forEach(function (currentUser) {
                    groupUsers.push({id: currentUser.id});
                });

                var groupTenants = [];
                self.tenants.forEach(function (currentTenant) {
                    if (currentTenant.checked) {
                        groupTenants.push({id: currentTenant.id});
                    }
                });

                if (self.isNewGroup) {
                    self.createNewGroup(groupTenants, groupUsers);
                }
                else {
                    self.updateExistingGroup(groupTenants, groupUsers);
                }
            }
        }

        self.createNewGroup = function (groupTenants, groupUsers) {
            var newGroupObject = ComponentsDataFlow.createNewGroupRequestObject(self.groupName, groupTenants, groupUsers);

            if (newGroupObject) {
                $rootScope.waiting = true;
                coreSearchService.createGroupRequest.update(newGroupObject,
                    function (data) {
                        if (data.payload) {
                            bootboxService.alertWithCallback('Group Creation', 'Group <strong>' + self.groupName + '</strong> created Successfully!', true, self.goToGroups);
                        }
                        else {
                            bootboxService.alert('Group Creation', 'Creation of Group <strong>' + self.groupName + '</strong> failed!', false);
                        }

                        $rootScope.waiting = false;
                    },
                    function (createGroupError) {
                        log.error('UserManagementCreateController :: createNewGroup :: ERROR ' + createGroupError.statusText);

                        bootboxService.alert('Group Creation', 'Creation of Group <strong>' + self.groupName + '</strong> failed!', false);
                        $rootScope.waiting = false;
                    });
            }
        }

        self.updateExistingGroup = function (groupTenants, groupUsers) {
            var newGroupObject = ComponentsDataFlow.createGroupUpdateRequestObject(self.groupId, groupUsers, groupTenants, self.isWorkflowRelatedGroup);

            if (newGroupObject) {
                $rootScope.waiting = true;
                coreSearchService.updateGroupRequest.update(newGroupObject,
                    function (data) {
                        if (data.payload) {
                            bootboxService.alertWithCallback('Group Update', 'Group <strong>' + self.groupName + '</strong> updated Successfully!', true, self.goToGroups);
                        }
                        else {
                            bootboxService.alert('Group Update', 'Update of Group <strong>' + self.groupName + '</strong> failed!', false);
                        }

                        $rootScope.waiting = false;
                    },
                    function (updateExistingGroupError) {
                        log.error('UserManagementCreateController :: updateExistingGroup :: ERROR ' + updateExistingGroupError.statusText);

                        bootboxService.alert('Group Update', 'Update of Group <strong>' + self.groupName + '</strong> failed!', false);
                        $rootScope.waiting = false;
                    });
            }
        }

        self.goHome = function () {
            $location.path('/search_page');
        }

        self.goToGroups = function () {
            $location.path('/user_management/groups');
        }
    }

})();