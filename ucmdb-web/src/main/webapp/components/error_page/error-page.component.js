(function () {

    'use strict';

    angular
        .module('errorPage')
        .component('errorPage', {
            templateUrl: 'components/error_page/error-page.template.html',
            controller: ['$location', ErrorPageController]
        });

    function ErrorPageController($location) {
        var self = this;

        self.goBack = function goBack() {
            window.history.back();
        }

        self.goHome = function () {
            $location.path('/search_page');
        }
    }
})();