(function () {

    'use strict';

    angular
        .module('reportActionsModule')
        .component('reportActionsComponent', {
            templateUrl: 'components/reporting_user_actions/reporting-user-actions.template.html',
            controller: ['$rootScope', '$q', 'NgTableParams', 'log', 'bootboxService', 'coreSearchService', ReportingUserActionsController]
        });

    function ReportingUserActionsController($rootScope, $q, NgTableParams, log, bootboxService, coreSearchService) {
        var self = this;

        self.tableFilter = {text: ''};

        //Datepicker Options
        self.datePopupsStatus = {from: false, to: false};
        self.dateFormat = 'dd/MM/yyyy';
        self.dateObjects = {from: moment().subtract(7, 'days'), to: moment()};
        self.fromDateOptions = {
            maxDate: self.dateObjects.to
        }
        self.toDateOptions = {
            minDate: self.dateObjects.from,
            maxDate: moment()._d
        }

        self.$onInit = onInit;
        self.getAuditDetailsForNode = getAuditNodeDetailsForNode;
        self.downloadActionAuditLogExcel = downloadActionAuditLogExcel;

        function onInit() {
            $rootScope.waiting = true;

            coreSearchService.getAllUserActions.query(
                function (userActions) {
                    if (userActions.payload.length !== 0) {
                        self.tableParams = new NgTableParams({
                            sorting: {modifyTime: "desc"},
                            page: 1,
                            count: 10,
                            filter: self.tableFilter
                        }, {
                            counts: [],
                            filterOptions: {
                                filterFilterName: 'userActionsFilter'
                            },
                            dataset: userActions.payload
                        });

                        self.noUserActions = false;
                    }
                    else {
                        self.noUserActions = true;
                    }


                    $rootScope.waiting = false;
                },
                function (getAllUserActionsError) {
                    log.error('ReportingUserActionsController.onInit :: getAllUserActions :: ' + getAllUserActionsError.statusText);
                    self.noUserActions = true;

                    $rootScope.waiting = false;
                });
        }

        function getAuditNodeDetailsForNode(nodeDiffId) {
            // Get the Data & Open Modal
            getAuditingDataForNode(nodeDiffId).then(
                function (data) {
                    self.modalData = data;
                    angular.element('#auditing-modal').modal('show');
                },
                function (errorMsg) {
                    log.info(errorMsg);
                    bootboxService.alert('Server Error!', 'Internal Server Error Occurred.', false);
                });
        }

        function getAuditingDataForNode(nodeDiffId) {
            var defer = $q.defer();

            $rootScope.waiting = true;
            coreSearchService.getAuditingData.query({auditId: nodeDiffId},
                function (response) {
                    defer.resolve(response.payload);
                    $rootScope.waiting = false;
                },
                function (getAuditingDataError) {
                    defer.reject('ReportingUserActionsController :: getAuditingDataForNode :: Error ' + getAuditingDataError.statusText);
                    $rootScope.waiting = false;
                });

            return defer.promise;
        }

        function downloadActionAuditLogExcel() {
            $rootScope.waiting = true;
            coreSearchService.exportUserActionAuditLog.query(
                function (excelBytes) {
                    download(excelBytes.data, 'Action Audit Log.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

                    $rootScope.waiting = false;
                }
                , function () {
                    bootboxService.alert('Server Error', 'User ActionAuditLog Excel could not be exported!', false);

                    $rootScope.waiting = false;
                });
        }
    }

})();