(function () {
    'use strict';
    
    angular
        .module('reportActionsModule')
        .filter('userActionsFilter', userActionsFilter);

    function userActionsFilter() {
        return function(data, filterText){
            return (data || []).filter(function(row){
                return (row.processId && row.processId.indexOf(filterText.text) === 0) || (row.displayLabel && row.displayLabel.indexOf(filterText.text) === 0);
            });
        };
    }
})();