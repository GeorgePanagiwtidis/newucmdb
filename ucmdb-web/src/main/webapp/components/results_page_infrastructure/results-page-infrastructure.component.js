(function () {

    'use strict';

    angular
        .module('resultsPageInfrastructureModule')
        .component('resultsPageInfrastructure', {
            templateUrl: 'components/results_page_infrastructure/results-page-infrastructure.template.html',
            controller: ResultsPageInfrastructureController
        });

    ResultsPageInfrastructureController.$inject = ['$rootScope', '$timeout', '$q', 'loaderService', 'localStorageService', 'ComponentsDataFlow',
        'NgTableParams', 'coreSearchService', 'updateService', 'log', 'bootboxService'];

    function ResultsPageInfrastructureController($rootScope, $timeout, $q, loaderService, localStorageService, ComponentsDataFlow,
                                                 NgTableParams, coreSearchService, updateService, log, bootboxService) {
        var self = this;

        self.noResults = false;

        self.infrastructureObject = [];

        // Ng-Table Properties
        self.tableCols = [];
        self.itemProperties = [];

        self.allColumnsChecked = true;

        // -- Modal Variables --
        // Original Response
        self.ciAttributes;
        // Edited Response
        self.selectedNode;

        self.isCiUpdatable;
        self.ciType;
        // -- End of Modal Variables --

        self.tableParams;

        // Object of Attributes Display Names
        self.ciAttributeNames = {};

        self.$onInit = function () {
            $rootScope.waiting = true;
            $q.all({
                ciAttributes: getCiAttributes(),
                tableResults: getData()
            }).then(function (successResponses) {
                    manipulateCiAttributesResponse(successResponses.ciAttributes);
                    manipulateTableResultsResponse(successResponses.tableResults);

                    $rootScope.waiting = false;
                },
                function (errorResponses) {
                    log.error(errorResponses);

                    // bootboxService.alert('Server Error', 'Error while Retrieving Data!', false);
                    self.noResults = true;
                    $rootScope.waiting = false;
                });
        }

        self.selectAllColumns = function () {
            angular.forEach(self.tableCols, function (col) {
                col.show = self.allColumnsChecked;
            });
        }

        self.downloadExcel = function () {
            var searchTerm = localStorageService.get('searchTerm');
            var tenants = localStorageService.get('selectedTenants');
            var options = localStorageService.get('optionList').infraOptions.infraOptions;
            var selectedTableColumns = getSelectedColumns();

            if (selectedTableColumns.length === 0) {
                bootboxService.info('No Columns Selected', 'You should have selected at least 1 Column in order to Export Results to Excel!');
                return;
            }

            var postObject = ComponentsDataFlow.createInfrastructureSearchRequest(searchTerm.serverSearchTerm, tenants, options, self.tableParams.filter(), selectedTableColumns);
            var fileName = self.infrastructureObject.searchTerm;

            angular.forEach(tenants, function (tenant) {
                fileName = fileName.concat('_' + tenant.name);
            });

            var date = new Date();
            var dayStr = date.getDate().toString();
            var day = dayStr.length < 2 ? '0' + dayStr : dayStr;

            var monthStr = (date.getMonth() + 1).toString();
            var month = monthStr.length < 2 ? '0' + monthStr : monthStr;

            var year = date.getFullYear();

            fileName = fileName.concat('_' + day + month + year);

            $rootScope.waiting = true;
            coreSearchService.downloadExcelServiceInfrastructure.query(postObject, function (excelFile) {
                // download.js plugin
                download(excelFile.data, fileName + '.xls', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                $rootScope.waiting = false;
            });
        }

        self.saveSearch = function saveSearch() {
            if (!self.searchName) {
                self.saveFailedValidation = true;
                return;
            }

            saveTableFiltersInOptions();

            $rootScope.waiting = true;
            var saveRequestObject = ComponentsDataFlow.createSaveSearchRequestObject(self.searchName);
            coreSearchService.saveSearchRequest.update(saveRequestObject,
                function (data) {
                    showHideValidationMessages(data.payload);
                    $rootScope.waiting = false;
                },
                function (saveSearchRequestError) {
                    log.error('ResultsPageInfrastructureController.saveSearch :: ERROR: ' + saveSearchRequestError.statusText);
                    showHideValidationMessages(false);

                    $rootScope.waiting = false;
                });
        }

        // For Modal
        self.showDetails = function (item, fromRelatedModal) {
            self.ciType = fromRelatedModal ? item['root_class'] : localStorageService.get('searchTerm').serverSearchTerm;

            updateService.isCiUpdatable(item.global_id, self.ciType).then(function (isUpdatable) {
                self.isCiUpdatable = isUpdatable;
            });

            var requestObject = {
                id: item.global_id,
                type: self.ciType
            };

            loaderService.showAppropriateLoader(fromRelatedModal);
            coreSearchService.getBusinessRowData.query(requestObject,
                function (ciDetails) {
                    var ciDetailsPayload = ciDetails.payload;

                    var ciProperties = {};
                    angular.forEach(ciDetailsPayload.groups, function (groupObject) {
                        _.assign(ciProperties, groupObject.properties);
                    });

                    self.selectedNode = {
                        globalId: ciDetailsPayload.globalId,
                        name: ciProperties['display_label'],
                        attributeNames: getAtttibuteNamesFromRetrievedObject(self.ciAttributeNames),
                        groupAttributes: ciDetailsPayload.groups,
                        decoupledCiProperties: ciProperties
                    }

                    // Store Original Ci Attributes for Update Purposes
                    self.ciAttributes = angular.copy(self.selectedNode);

                    updateService.openModalAndResetDefaults(self);
                    loaderService.hideAppropriateLoader(fromRelatedModal);
                }, function (getInfrastructureItemDetailsError) {
                    log.error('ResultsPageInfrastructureController.showDetails :: getInfrastructureItemDetails' + getInfrastructureItemDetailsError.statusText);

                    loaderService.hideAppropriateLoader(fromRelatedModal);
                });
        }

        self.getRelatedCis = function (item) {
            self.ciGlobalId = item['globalId'];
            self.ciDisplayLabel = item['display_label'];
            self.relatedCiTypes = null;

            $rootScope.waiting = true;
            getRelatedCiTypes(self.ciGlobalId).then(function (relatedCiTypes) {
                    var transformedCiTypes = [];
                    angular.forEach(relatedCiTypes, function(ciTypeLabel, ciTypeName) {
                        transformedCiTypes.push({name: ciTypeName, label: ciTypeLabel});
                    });

                    self.relatedCiTypes = _.chunk(transformedCiTypes, transformedCiTypes.length /2);
                    $rootScope.waiting = false;

                    // Open Modal
                    var modalElement = angular.element('#relatedCisModal');
                    modalElement.modal('show');
                },
                function (relatedCiTypesError) {
                    bootboxService.alert('Server Error', relatedCiTypesError, false);

                    $rootScope.waiting = false;
                });
        }

        self.updateDataset = function (displayLabel, updatedAttributes) {
            // Get Table Dataset
            var dataset = self.tableParams.settings().dataset;
            // Get the right Dataset Row
            var tableRow = _.find(dataset, {display_label: displayLabel});

            angular.forEach(updatedAttributes, function (attribute) {
                if (tableRow[attribute.ucmdbPropName]) {
                    tableRow[attribute.ucmdbPropName] = attribute.value;
                }
            });

            self.tableParams.reload();

            // Clear Infrastructure Cache -- Has Implemented in Backend Instead --
            // updateService.clearInfrastructureCache();
            // updateService.clearInfrastructureCache(self.constructRequestObject());
        }

        // Helper Functions
        function getData() {
            var defer = $q.defer();
            var requestObject = constructRequestObject();

            coreSearchService.searchInfrastructure.query(requestObject, function (data) {
                    var payload = data.payload;

                    if (payload && payload.length !== 0) {
                        defer.resolve(payload);
                    }
                    else {
                        defer.reject('ResultsPageInfrastructure.getData :: No Results Returned!');
                    }
                },
                function (searchInfrastructureError) {
                    defer.reject('ResultsPageInfrastructure.getData :: Error: ' + searchInfrastructureError.statusText);
                });

            return defer.promise;
        }

        function manipulateTableResultsResponse(tableResults) {
            createInfraObject(tableResults);
            initNgTable();

            // Add Null Values in Table Columns Filters for Filtering empty Results
            addNullFilterValues();
            // Set saved filters in Table Results
            setTableFilters();
        }

        function manipulateCiAttributesResponse(ciAttributes) {
            angular.forEach(ciAttributes, function (attribute) {
                self.ciAttributeNames[attribute.attributeName] = {
                    label: attribute.attributeLabel,
                    priority: attribute.attributePriority
                };
            });
        }

        function constructRequestObject() {
            // Create Object for Search Request
            var searchTerm = localStorageService.get('searchTerm');
            var selectedTenants = localStorageService.get('selectedTenants');
            var options = localStorageService.get('optionList').infraOptions.infraOptions;

            return ComponentsDataFlow.createInfrastructureSearchRequest(searchTerm.serverSearchTerm, selectedTenants, options);
        }

        function getCiAttributes() {
            var defer = $q.defer();

            var ciType = localStorageService.get('searchTerm').serverSearchTerm;

            // TODO -- Check this
            ciType = ciType === 'host_node' ? 'node' : ciType;

            coreSearchService.getCiAttributes.query({type: ciType},
                function (ciAttributes) {
                    var ciAttributesPayload = ciAttributes.payload;
                    defer.resolve(ciAttributesPayload);
                },
                function (getCiAttributesError) {
                    defer.reject('ResultsPageInfrastructureController.getCiCustomAttributes :: ERROR: ' + getCiAttributesError.statusText);
                });

            return defer.promise;
        }

        function createInfraObject(payload) {
            var options = localStorageService.get('optionList').infraOptions.infraOptions;
            var searchTerm = localStorageService.get('searchTerm');
            var optionsConcatStr;

            if (options) {
                optionsConcatStr = '';
                angular.forEach(options, function (optionItem) {
                    optionsConcatStr = optionsConcatStr.concat(optionItem.attributeValue + ', ');
                });

                optionsConcatStr = optionsConcatStr.slice(0, optionsConcatStr.length - 2);
            }

            self.infrastructureObject = {
                searchTerm: searchTerm.visibleSearchTerm,
                options: optionsConcatStr,
                object: payload
            };
        }

        function initNgTable() {
            clearData();
            initTableColumns(self.infrastructureObject.object);
            addFilterValuesToTableColumns();


            self.tableParams = new NgTableParams({count: 10}, {counts: [], dataset: convertRetrievedData()});
        }

        function clearData() {
            if (self.itemProperties.length !== 0) {
                self.itemProperties.splice(0);
            }

            if (self.tableCols.length !== 0) {
                self.tableCols.splice(0);
            }
        }

        function initTableColumns(infraObject) {
            // Get Infra Object Index with most properties
            var tableProperties = getObjectPropertiesForTableColumn(infraObject);

            // Fill Table Columns
            var selectedColumns = localStorageService.get('optionList').infraOptions.selectedColumns ? localStorageService.get('optionList').infraOptions.selectedColumns : [];
            angular.forEach(tableProperties, function (key) {
                self.itemProperties.push(key);
                // self.tableCols.push(_.startCase(key));
                var colOb = {
                    title: self.ciAttributeNames[key] ? self.ciAttributeNames[key].label : _.startCase(key),
                    field: key,
                    filter: {},
                    filterData: [],
                    sortable: key,
                    priority: self.ciAttributeNames[key] ? self.ciAttributeNames[key].priority : 100,
                    show: selectedColumns.indexOf(key) !== -1 || selectedColumns.length === 0
                };
                colOb.filter[key] = "select";

                self.tableCols.push(colOb);
            });

            sortTableColumnsBasedOnAttributePriorities();
        }

        function getObjectPropertiesForTableColumn(infraObject) {
            var tableProperties = [];
            angular.forEach(infraObject, function (infraRow) {
                var infraRowProperties = infraRow.properties;

                for (var prop in infraRowProperties) {
                    if (tableProperties.indexOf(prop) === -1) {
                        tableProperties.push(prop);
                    }
                }
            });

            return tableProperties;
        }

        function addFilterValuesToTableColumns() {
            self.infrastructureObject.object.forEach(function (infraOb) {
                self.itemProperties.forEach(function (prop) {
                    for (var key in self.tableCols) {
                        if (self.tableCols[key].field === prop) {
                            if (infraOb.properties[prop]) {
                                var filterData = self.tableCols[key].filterData;
                                if (filterData.length !== 0) {

                                    var duplicateFilter = false;

                                    self.tableCols[key].filterData.forEach(function (filter) {
                                        if (filter) {
                                            if (filter.id === infraOb.properties[prop]) {
                                                duplicateFilter = true;
                                            }
                                        }
                                    });

                                    if (!duplicateFilter) {
                                        self.tableCols[key].filterData.push({
                                            id: infraOb.properties[prop],
                                            title: infraOb.properties[prop]
                                        });
                                    }
                                }
                                else {
                                    self.tableCols[key].filterData.push({
                                        id: infraOb.properties[prop],
                                        title: infraOb.properties[prop]
                                    });
                                }
                            }
                            break;
                        }
                    }
                });
            });

            // Sorting Filter Values
            self.tableCols.forEach(function (sortObj) {
                sortObj.filterData = sortObj.filterData.sort(function (a, b) {
                    var nameA = a.title.toUpperCase(); // ignore upper and lowercase
                    var nameB = b.title.toUpperCase(); // ignore upper and lowercase
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    // names must be equal
                    return 0;
                });
            });
        }

        function convertRetrievedData() {
            // Include GlobalId in Object also
            var data = self.infrastructureObject.object.map(function (infraOb) {
                var infraRow = angular.copy(infraOb.properties);
                infraRow.globalId = infraOb.globalId;

                return infraRow;
            });

            return data;
        }

        function addNullFilterValues() {
            var nullFilter = {
                id: '!',
                title: '(NULL)'
            }

            angular.forEach(self.tableCols, function (col) {
                col.filterData.unshift(nullFilter);
            });
        }

        function sortTableColumnsBasedOnAttributePriorities() {
            self.tableCols.sort(function (col1, col2) {
                if (col1.priority === 100 && col2.priority === 100) {
                    if (col1.title < col2.title) return -1;
                    if (col1.title > col2.title) return 1;
                    return 0;
                }
                else {
                    return col1.priority - col2.priority;
                }
            });
        }

        function saveTableFiltersInOptions() {
            // Get Options object from LocalStorage
            var searchOptions = localStorageService.get('optionList');

            // Check if User has Selected any Column Filters and save them
            var tableFilters = self.tableParams.filter();

            var nonEmptyFilters = [];
            angular.forEach(tableFilters, function (filterValue, filterName) {
                // Add only not empty filter properties
                if (filterValue.length !== 0) {
                    nonEmptyFilters.push({propertyName: filterName, propertyValue: filterValue});
                }
            });

            // Replace Existing filters in LocalStorageObject
            searchOptions.infraOptions.tableFilters = nonEmptyFilters;
            searchOptions.infraOptions.selectedColumns = getSelectedColumns();
            localStorageService.set('optionList', searchOptions);
        }

        function getSelectedColumns() {
            var selectedColumns = [];

            if (self.allColumnsChecked) {
                selectedColumns = self.tableCols
                    .map(function (col) {
                        return col.field;
                    });
            }
            else {
                selectedColumns = self.tableCols
                    .filter(function (col) {
                        return col.show;
                    })
                    .map(function (col) {
                        return col.field;
                    });
            }

            return selectedColumns;
        }

        function getAtttibuteNamesFromRetrievedObject(attributesObject) {
            var attributeNames = {};
            for (var attrName in attributesObject) {
                attributeNames[attrName] = attributesObject[attrName].label;
            }

            return attributeNames;
        }

        function getRelatedCiTypes(globalId) {
            var defer = $q.defer();

            coreSearchService.getMenuItems.query([globalId],
                function (relatedCiTypes) {
                    defer.resolve(relatedCiTypes.payload);
                },
                function (relatedCiTypesError) {
                    log.error('ResultsPageInfrastructureController.getRelatedCiTypes :: ERROR: ' + relatedCiTypesError.statusText);

                    defer.reject('Could not Retrieve Related CI Types!');
                });

            return defer.promise;
        }

        function setTableFilters() {
            // Get Filters Object for Infra Filters from LocalStorage
            var tableFilters = localStorageService.get('optionList').infraOptions.tableFilters;

            // Construct filters object for NgTable
            var ngTableFilters = {};
            angular.forEach(tableFilters, function (item) {
                ngTableFilters[item.propertyName] = item.propertyValue;
            });

            // Apply Filters on NgTable object
            self.tableParams.filter(ngTableFilters);
            self.tableParams.reload();
        }

        function showHideValidationMessages(serverResponse) {
            // Restore Search Name input
            self.searchName = '';

            if (serverResponse) {
                // Flags for Bootstrap Alerts
                self.saveFailedValidation = false;
                self.saveFailed = false;
                self.successSave = true;

                closeSaveSearchModalAfterAWhile();
            }
            else {
                // Flags for Bootstrap Alerts
                self.saveFailed = true;
                self.saveFailedValidation = false;
                self.successSave = false;
            }
        }

        function closeSaveSearchModalAfterAWhile() {
            var saveSearchModal = angular.element('#saveSearchModal');

            $timeout(function () {
                saveSearchModal.modal('hide');

                // Restore Bootstrap Flags
                self.saveFailed = false;
                self.saveFailedValidation = false;
                self.successSave = false;
            }, 1000);
        }
    }

})();