(
    function () {
        'use strict';

        angular
            .module('commentPage')
            .component('commentPage', {
                templateUrl: 'components/comment_page/comment-page.template.html',
                controller: ['$q', '$rootScope', '$scope', 'coreSearchService', 'bootboxService', 'NgTableParams', CommentController]
            });

        function CommentController($q, $rootScope, $scope, coreSearchService, bootboxService, NgTableParams) {

            var self = this;

            self.savedCommentFilters = {displayLabel: ''};
            self.ciDetails = {};

            self.$onInit = function () {
                $rootScope.waiting = true;

                coreSearchService.getUserComments.query(
                    function (comments) {
                        var savedCommentsResponse = comments.payload;

                        if (savedCommentsResponse.length !== 0) {
                            self.noAddedComment = false;

                            self.commentTable = new NgTableParams(
                                {
                                    sorting: {date: "desc"},
                                    page: 1,
                                    count: 10,
                                    filter: self.savedCommentFilters
                                },
                                {
                                    counts: [],
                                    filterOptions: {filterFilterName: 'tasksDateFilter'},
                                    dataset: savedCommentsResponse
                                }
                            );
                        }
                        else {
                            self.noAddedComment = true;
                        }

                        $rootScope.waiting = false;
                    },
                    function (getCommentsError) {
                        log.error('CommentController :: onInit :: getCiComments :: ERROR: ' + getCommentsError.statusText);

                        self.noAddedComment = true;
                        $rootScope.waiting = false;
                    });
            }

            self.showCiDetails = function (globalId) {
                globalId = globalId.toLowerCase();

                $rootScope.waiting = true;
                coreSearchService.getBusinessRowData.query({id: globalId},
                    function (ciDetails) {
                        var ciDetailsPayload = ciDetails.payload;

                        getAttributeNames(ciDetailsPayload.type)
                            .then(function (attributeNames) {
                                createModalObject(ciDetailsPayload, attributeNames);
                            });

                        angular.element('#result').modal('show');
                        $rootScope.waiting = false;
                    }, function (getInfrastructureItemDetailsError) {
                        log.error('ResultsPageInfrastructureController.showDetails :: getInfrastructureItemDetails' + getInfrastructureItemDetailsError.statusText);

                        $rootScope.waiting = false;
                    });
            }

            // Helper Functions
            function getAttributeNames(type) {
                var defer = $q.defer();
                coreSearchService.getCiAttributes.query({type: type},
                    function (ciAttributes) {
                        var attributes = {};
                        angular.forEach(ciAttributes.payload, function (attribute) {
                            attributes[attribute.attributeName] = attribute.attributeLabel;
                        });

                        defer.resolve(attributes);
                    },
                    function (getCiAttributesError) {
                        defer.reject('AuditModal (anonymous function) :: getAttributeNames :: ERROR: ' + getCiAttributesError.statusText);
                    });

                return defer.promise;
            }

            function createModalObject(ciDetails, attributeNames) {
                var displayLabel =
                    _.find(ciDetails.groups, {groupName: 'Technical Attributes'}).properties['display_label'];

                var ciProperties = {};
                angular.forEach(ciDetails.groups, function (groupObject) {
                    _.assign(ciProperties, groupObject.properties);
                });

                self.ciDetails = {
                    globalId: ciDetails.globalId,
                    name: displayLabel,
                    attributeNames: attributeNames,
                    groupAttributes: ciDetails.groups,
                    decoupledCiProperties: ciProperties
                }
            }

            self.downloadCommentExcel = function () {
                $rootScope.waiting = true;
                coreSearchService.exportUserComments.query(
                    function (excelBytes) {
                        download(excelBytes.data, 'User Comments.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

                        $rootScope.waiting = false;
                    }
                    , function () {
                        bootboxService.alert('Server Error', 'User Comments Excel could not be exported!', false);

                        $rootScope.waiting = false;
                    });

            }
        }
    }

)();