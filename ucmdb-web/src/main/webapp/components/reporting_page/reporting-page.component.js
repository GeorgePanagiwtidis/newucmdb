(function () {

    'use strict';

    angular
        .module('reportingModule')
        .component('reportingPage', {
            templateUrl: 'components/reporting_page/reporting-page.template.html',
            controller: ['$rootScope', '$routeParams', '$q', 'log', 'bootboxService', 'ComponentsDataFlow', 'coreSearchService', ReportingPageController]
        });

    function ReportingPageController($rootScope, $routeParams, $q, log, bootboxService, ComponentsDataFlow, coreSearchService) {

        var self = this;

        self.reportType;
        self.reportTitle;

        self.nodeStatus = 'ALL';
        self.taskStatus = 'ALL';

        // Multiselect Options for Tenant Selection
        self.multiSelectSettings =
            {
                smartButtonMaxItems: 3
            }
        self.multiSelectEvents =
            {
                onItemSelect: onTenantSelect,
                onItemDeselect: onTenantSelect,
                onSelectAll: onAllTenantSelect,
                onDeselectAll: onAllTenantDeselect
            }
        // End Of Multiselect Configuration
        self.selectedTenants = [];
        self.tenants = [];
        self.tenantsMultiselect = [];

        //Datepicker Options
        self.datePopupsStatus = {from: false, to: false};
        self.dateFormat = 'dd/MM/yyyy';
        self.dateObjects = {from: moment("01/11/2017", "DD/MM/YYYY"), to: moment()};
        self.fromDateOptions = {
            maxDate: self.dateObjects.to
        }
        self.toDateOptions = {
            minDate: self.dateObjects.from,
            maxDate: moment()._d
        }

        self.chartOptions = {
            legend: {
                display: true,
                position: 'bottom'
            },
            elements: {
                arc: {
                    borderWidth: 0
                }
            }
        }

        self.$onInit = function () {
            // Get Tenants onInit before going to specific Report Type
            self.getTenants().then(function (response) {
                if (response) {
                    // Based on type we present the right Content
                    self.reportType = $routeParams.type;

                    if (self.reportType === 'node') {
                        self.reportTitle = 'Nodes';
                        self.searchNodeReports();
                    } else {
                        self.reportTitle = 'Tasks';
                        self.searchTaskReports();
                    }
                }
            });
        };

        self.drawNodePieChart = function (data) {
            if (data) {
                if (data.length !== 0) {
                    self.noReportingData = false;

                    var openStatusNodes = _.find(data, {processStatus: "OPEN"});
                    var completedStatusNodes = _.find(data, {processStatus: "COMPLETED"});

                    var numOfOpenNodes = 0;
                    var numOfCompletedNodes = 0;
                    if (openStatusNodes) {
                        numOfOpenNodes = openStatusNodes.numberOfNodes;
                    }
                    if (completedStatusNodes) {
                        numOfCompletedNodes = _.find(data, {processStatus: "COMPLETED"}).numberOfNodes;
                    }

                    self.nodePieChartData = {labels: [], data: [], total: 0, colors: [], serverData: []};
                    self.nodePieChartData.colors = ['#2e565', '#2c7278', '#3a7a66', '#3291b7', '#56b3bd', '#74bba5', '#78898b', '#81a9b3', '#2481c3'];

                    self.nodePieChartData.labels = ['Completed Status Nodes', 'Open Status Nodes'];
                    self.nodePieChartData.data = [numOfCompletedNodes, numOfOpenNodes];
                    self.nodePieChartData.total = numOfOpenNodes + numOfCompletedNodes;

                    self.nodePieChartData.serverData = data;
                }
                else {
                    self.noReportingData = true;
                }
            }


        };

        self.drawTaskPieChart = function (data) {
            self.taskPieChartData = {labels: [], data: [], total: 0, colors: [], serverData: []};
            self.taskPieChartData.colors = ['#2e565', '#2c7278', '#3a7a66', '#3291b7', '#56b3bd', '#74bba5', '#78898b', '#81a9b3', '#2481c3'];

            if (data.taskReportingResultsDTOList === null || data.taskReportingResultsDTOList.length === 0) {
                self.noReportingData = true;
            }
            else {
                self.noReportingData = false;
            }

            angular.forEach(data.taskReportingResultsDTOList, function (taskReportingResultsDTO) {
                self.taskPieChartData.labels.push(taskReportingResultsDTO.groupName);
                self.taskPieChartData.data.push(taskReportingResultsDTO.tasksAssigned);
            });

            self.taskPieChartData.total = data.totalTasks;
            self.taskPieChartData.serverData = data;
        };

        self.drawPieChart = function (data) {
            // Get Labels & Data
            self.pieChartData = {labels: [], data: [], colors: []};
            self.pieChartData.colors = ['#ea2c2c', '#52bf52', '#8c8cea'];

            // Find unique Task Labels
            var uniqueLabels = _.uniqBy(self.taskGroups, 'groupName');
            angular.forEach(uniqueLabels, function (group) {
                self.pieChartData.labels.push(group.groupName);
            });

            self.calculateChartsData(uniqueLabels, data);
        }

        self.calculateChartsData = function (labels, data) {
            angular.forEach(labels, function (lab) {
                var Values = data.filter(function (val) {
                    return val.groupName === lab.groupName;
                });

                self.pieChartData.data.push(Values.length);
            });
        }

        self.openDatePopup = function (type) {
            if (type === 'from') {
                self.datePopupsStatus.from = true;
            }
            else if (type === 'to') {
                self.datePopupsStatus.to = true;
            }
        }

        self.nodeChartDetails = function (data) {
            var chartData = data.pop();
            if (chartData) {
                var nodeStatus = chartData._view.label.toLowerCase().indexOf('completed') !== -1 ? 'COMPLETED' : 'OPEN';
                var modalObject = _.find(self.nodePieChartData.serverData, {processStatus: nodeStatus});

                // Pass Request Object as Param in Directive
                var fromDateTimestamp = self.dateObjects.from.valueOf();
                var toDateTimestamp = self.dateObjects.to.valueOf();
                self.requestExcelObject = ComponentsDataFlow.createNodeReportRequestObject(self.selectedTenants, nodeStatus, fromDateTimestamp, toDateTimestamp);

                self.openReportingModal(modalObject, 1);
            }
        }

        self.taskChartDetails = function (data) {
            var chartData = data.pop();
            if (chartData) {
                var groupName = chartData._view.label;

                var taskData = _.find(self.taskPieChartData.serverData.taskReportingResultsDTOList, {groupName: groupName});

                // Pass Request Object as Param in Directive
                var fromDateTimestamp = self.dateObjects.from.valueOf();
                var toDateTimestamp = self.dateObjects.to.valueOf();
                self.requestExcelObject = ComponentsDataFlow.createTaskReportRequestObject(self.selectedTenants, groupName, self.taskStatus, fromDateTimestamp, toDateTimestamp);

                self.openReportingModal(taskData.taskReportDTOS, 2);
            }
        }

        self.openReportingModal = function (data, type) {
            // Type 1 is Node, Type 2 is Task
            var modalID = type === 1 ? '#nodes-modal' : '#tasks-modal';

            self.modalData = data;
            var reportModal = angular.element(modalID);
            reportModal.modal('show');

            $rootScope.$apply();
        }

        self.getTenants = function () {
            var defer = $q.defer();

            coreSearchService.getTenants.query(
                function (tenantsResponse) {
                    self.tenants =
                        tenantsResponse.payload.map(function (tenant) {
                            return {
                                id: tenant.id,
                                label: tenant.description
                            }
                        });

                    // Init Multiselect Model Object
                    self.tenantsMultiselect = self.tenantsMultiselect.concat(self.tenants);
                    // Init Selected Tenants for Server Request
                    self.selectedTenants = self.tenantsMultiselect.map(function (tenant) {return tenant.label});

                    defer.resolve(true);
                },
                function (getTenants) {
                    defer.reject(false);
                    log.error('ReportingPageController :: $onInit :: getTenants :: ERROR: ' + getTenants.statusText);
                }
            );

            return defer.promise;
        }

        self.searchNodeReports = function () {
            var fromDateTimestamp = self.dateObjects.from.valueOf();
            var toDateTimestamp = self.dateObjects.to.valueOf();

            var searchObject = ComponentsDataFlow.createNodeReportRequestObject(self.selectedTenants, self.nodeStatus, fromDateTimestamp, toDateTimestamp);

            $rootScope.waiting = true;
            coreSearchService.reportingNodeSearch.query(searchObject,
                function (reportsData) {
                    if (reportsData.payload) {

                        self.drawNodePieChart(reportsData.payload);
                    }
                    else {
                        self.noReportingData = true;
                    }

                    $rootScope.waiting = false;
                },
                function (reportingSearchError) {
                    log.error("ReportingPageController.onInit :: getUserTasks :: " + reportingSearchError.statusText);

                    self.noReportingData = true;
                    $rootScope.waiting = false;
                });
        };

        self.searchTaskReports = function () {
            var fromDateTimestamp = self.dateObjects.from.valueOf();
            var toDateTimestamp = self.dateObjects.to.valueOf();

            var searchObject = ComponentsDataFlow.createTaskReportRequestObject(self.selectedTenants, null, self.taskStatus, fromDateTimestamp, toDateTimestamp);

            $rootScope.waiting = true;
            coreSearchService.reportingTaskSearch.query(searchObject,
                function (reportsData) {
                    if (reportsData.payload) {

                        self.drawTaskPieChart(reportsData.payload);
                    }
                    else {
                        self.noReportingData = true;
                    }

                    $rootScope.waiting = false;
                },
                function (reportingSearchError) {
                    log.error("ReportingPageController.onInit :: getUserTasks :: " + reportingSearchError.statusText);

                    self.noReportingData = true;
                    $rootScope.waiting = false;
                });
        };

        self.exportNodeReports = function () {
            var fromDateTimestamp = self.dateObjects.from.valueOf();
            var toDateTimestamp = self.dateObjects.to.valueOf();

            var exportObject = ComponentsDataFlow.createNodeReportRequestObject(self.selectedTenants, null, fromDateTimestamp, toDateTimestamp);
            var fileName = generateExportFileName('node', self.selectedTenants, null);

            $rootScope.waiting = true;
            coreSearchService.exportNodeSearch.query(exportObject,
                function (excelFile) {
                    download(excelFile.data, fileName, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    $rootScope.waiting = false;
                },
                function (reportingExportError) {
                    log.error("reportingModule.exportNodeReports() :: Error while exporting nodes for " + fromDateTimestamp + " and " + toDateTimestamp + " got " + reportingExportError);

                    self.noReportingData = true;
                    $rootScope.waiting = false;
                });
        };

        self.exportTaskReports = function () {
            var fromDateTimestamp = self.dateObjects.from.valueOf();
            var toDateTimestamp = self.dateObjects.to.valueOf();

            var exportObject = ComponentsDataFlow.createTaskReportRequestObject(self.selectedTenants, null, self.taskStatus, fromDateTimestamp, toDateTimestamp);
            var fileName = generateExportFileName('task', self.selectedTenants, self.taskStatus);

            $rootScope.waiting = true;
            coreSearchService.exportTaskSearch.query(exportObject,
                function (excelFile) {
                    download(excelFile.data, fileName, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    $rootScope.waiting = false;
                },
                function (reportingExportError) {
                    log.error("reportingModule.exportTaskReports() :: Error while exporting nodes for " + fromDateTimestamp + " and " + toDateTimestamp + " got " + reportingExportError);

                    self.noReportingData = true;
                    $rootScope.waiting = false;
                });
        };

        self.getNodeDetails = function (processId) {
            var deferred = $q.defer();

            $rootScope.waitingModal = true;
            coreSearchService.getNodeDetails.query({nodeId: processId}, function (data) {
                    deferred.resolve(data.payload);

                    $rootScope.waitingModal = false;
                },
                function (getNodeDetailsError) {
                    log.error("ReportingPageController.getNodeDetails :: getNodeDetails :: " + getNodeDetailsError.statusText);
                    deferred.reject(getNodeDetailsError.statusText);

                    $rootScope.waitingModal = false;
                });

            return deferred.promise;
        }

        self.getNodeAuditing = function (taskId, nodeId) {
            var debug = false;

            var nodeModalElement = angular.element('#nodes-modal');
            var auditingModalElement = angular.element('#auditing-modal');
            // Close Node Reporting Modal before showing the new one
            nodeModalElement.modal('hide');

            if (!debug) {
                // Retrieve Auditing data for task from Server & Open the new Modal
                $rootScope.waiting = true;
                coreSearchService.getAuditingData.query({taskId: taskId},
                    function (auditingData) {
                        if (auditingData.payload.beforeUpdate === null) {
                            bootboxService.alert('No Auditing Data', 'No Auditing Data found for Process Id: ' + nodeId, false);
                        }
                        else {
                            self.nodeAuditingData = auditingData.payload;
                            auditingModalElement.modal('show');
                        }

                        $rootScope.waiting = false;
                    },
                    function (getAuditingDataError) {
                        log.error("ReportingPageController.getNodeAuditing :: getAuditingData :: " + getAuditingDataError.statusText);

                        $rootScope.waiting = false;
                    });
            }
            else {
                var oldDataArray = [{title: 'field1', value: 'value1'}, {title: 'field2', value: 'value2'}];
                var currentDataArray = [{title: 'field1', value: 'value3'}, {title: 'field2', value: 'value4'}];


                self.nodeAuditingData = {oldData: oldDataArray, currentData: currentDataArray};
                auditingModalElement.modal('show');
            }
        }

        function onTenantSelect() {
            self.selectedTenants = self.tenantsMultiselect.map(function (tenant) {
                return tenant.label;
            });

            if (self.reportType === 'node') {
                self.searchNodeReports();
            } else {
                self.searchTaskReports();
            }
        }

        function onAllTenantSelect() {
            self.selectedTenants = self.tenants.map(function (tenant) {
                return tenant.label;
            });

            if (self.reportType === 'node') {
                self.searchNodeReports();
            } else {
                self.searchTaskReports();
            }
        }

        function onAllTenantDeselect() {
            self.selectedTenants = [];

            if (self.reportType === 'node') {
                self.searchNodeReports();
            } else {
                self.searchTaskReports();
            }
        }

        var generateExportFileName = function (type, tenants, status) {
            var tenantsStr = self.tenants.length === self.tenants.length ? 'ALL' : tenants;

            var fileName = '';

            if (type === 'task') {
                fileName = 'taskReport';
            } else {
                fileName = 'nodeReport';
            }

            // Append Tenant Name
            fileName = fileName.concat('_' + tenantsStr);

            if (status) {
                fileName = fileName.concat('_' + status);
            }

            fileName = fileName.concat('.xls');

            return fileName;
        }
    }
})();