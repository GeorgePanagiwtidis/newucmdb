(
    function () {
        'use strict';

        angular
            .module('homePageModule')
            .component('homePageComponent', {
                templateUrl: 'components/home_page/home-page.template.html',
                controller: HomePageController
            });

        HomePageController.$inject = ['__env', 'AclService', 'log'];

        // A dummy controller that selects HomePage
        function HomePageController(__env, AclService, log) {
            var homepage = this;

            homepage.acl = AclService;
            homepage.isNewUcmdb = __env.NEW_UCMDB;

            homepage.$onInit = function () {
                log.info('HomePage.$onInit');
            }
        }
    }
)();