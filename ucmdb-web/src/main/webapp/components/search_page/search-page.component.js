(function () {

    'use strict';

    angular
        .module('searchPage')
        .component('searchPage', {
            templateUrl: 'components/search_page/search-page.template.html',
            controller: ['$location', '$rootScope', '$window', '$q', 'AclService', 'log', 'coreSearchService', 'ComponentsDataFlow', 'localStorageService', SearchPageController]
        });


    function SearchPageController($location, $rootScope, $window, $q, AclService, log, coreSearchService, ComponentsDataFlow, localStorageService) {
        var self = this;

        self.searchTerm;

        // Classes List
        self.classes = [];
        self.selectedClass = 'Infrastructure';
        // Infrastructure Options List
        self.optionList = [];
        // Tenants List
        self.tenants = [];
        self.noTenants = false;

        self.selectedInfrastructureType = 'host_node';
        self.attributes = [];

        // Flag for check-uncheck the checkboxes
        self.isAllChecked = false;

        // Flag for Health Status of UCMDB
        $rootScope.isUp = true;

        // Object that keeps all Saved Search Options
        self.searchOptions;

        self.$onInit = function () {
            $rootScope.waiting = true;

            $q.all(constructResourceChain()).then(
                function (responses) {
                    setSearchTypes(responses.searchTypes);
                    setHealthStatus(responses.health);
                    setSearchTenants(responses.searchTenants);
                    setDefaultInfraType(responses.infraTypes);

                    returnFromResultsPage();
                    editSavedSearch();

                    $rootScope.waiting = false;
                },
                function (errorResponse) {
                    handleError(errorResponse);
                });
        };

        self.changeClassType = function (options) {
            if (self.selectedClass.toLowerCase().indexOf('infrastructure') !== -1) {
                retrieveInfrastructureTypes().then(
                    function (retrievedTypes) {
                        angular.forEach(retrievedTypes, function (type) {
                            self.infrastructureTypes.push({
                                id: type.typeId,
                                name: type.ciType,
                                title: type.ciTypeLabel
                            });
                        });

                        var selectedInfrastructureTypeId = _.find(self.infrastructureTypes, {name: self.selectedInfrastructureType}).id;

                        if (options && options.length !== 0) {
                            self.retrieveInfrastructureCustomAttributesBasedOnType(selectedInfrastructureTypeId, true, options);
                        }
                        else {
                            self.retrieveInfrastructureCustomAttributesBasedOnType(selectedInfrastructureTypeId, false, null);
                        }
                    }
                    , function (errorMessage) {
                        log.error(errorMessage);
                        $rootScope.waiting = false;
                    });
            }
            else if (self.selectedClass.toLowerCase().indexOf('queries') !== -1) {
                getViewQueries(options);
            }
        }

        self.retrieveInfrastructureCustomAttributesBasedOnType = function (infraTypeId, fromOtherPage, options) {
            var typeId = infraTypeId ? infraTypeId : _.find(self.infrastructureTypes, {name: self.selectedInfrastructureType}).id;

            coreSearchService.getInfraCustomAttributes.query({typeId: typeId},
                function (attributesResponse) {
                    var attributes = attributesResponse.payload;
                    initInfrastructureAttributes(attributes);

                    if (fromOtherPage) {
                        angular.forEach(options, function (option) {
                            // Change Option Value
                            _.find(attributes, {attributeName: option.attributeName}).attributeValue = option.attributeValue;

                            // Update Attributes that have value
                            initInfrastructureAttributes(attributes);
                            initOptionsList(self.attributes, true);
                        });
                    }
                    else {
                        initOptionsList(self.attributes, false);
                    }

                    $rootScope.waiting = false;
                },
                function (getCustomAttributesError) {
                    log.error('SearchPageController.changeClassType :: getCustomAttributes :: ' + getCustomAttributesError.statusText);

                    $rootScope.waiting = false;
                });
        }

        self.initViewQueryAttributes = function (queryIndex) {
            self.attributes = mapQueryPropertiesToAttributes(self.viewQueries[queryIndex].viewQueriesProperties);
        }

        self.selectOptionAttribute = function (option, attributeLabel, attributeName) {
            option.attributeLabel = attributeLabel;
            option.attributeName = attributeName;
        }

        self.shouldDisplayTenantsFilter = function () {
            // Maybe will be useful later

            return true;
        }

        self.checkAllTenants = function checkAllTenants(tenant) {
            if (typeof(tenant) === 'string') {
                if (tenant === 'All') {
                    if (!self.isAllChecked) {
                        self.tenants.forEach(function (item) {
                            item.checked = true;
                            self.isAllChecked = true;
                        });
                    }
                    else {
                        self.tenants.forEach(function (item) {
                            item.checked = false;
                            self.isAllChecked = false;
                        });
                    }
                }
            }
            else {
                var index = self.tenants.indexOf(tenant);

                if (self.tenants[index].checked) {
                    if (self.isAllChecked) {
                        self.isAllChecked = false;
                    }
                    self.tenants[index].checked = false;
                }
                else {
                    var numOfSelectedTenants = 0;
                    self.tenants[index].checked = true;

                    self.tenants.forEach(function (currentTenant) {
                        if (currentTenant.checked) {
                            numOfSelectedTenants++;
                        }
                    });
                    if (numOfSelectedTenants === self.tenants.length) {
                        self.isAllChecked = true;
                    }
                }
            }
        };

        self.searchUcmdb = function searchUcmdb() {
            if (validateFields()) {
                if (self.selectedClass === 'Business') {
                    searchBusiness();
                }
                else if (self.selectedClass === 'Infrastructure') {
                    searchInfrastructure();
                }
                // Implementation for View Queries
                else if (self.selectedClass === 'View Queries') {
                    searchViewQueries();
                }
            }
        }

        // -- Helper Functions --
        function constructResourceChain() {
            var resources = {
                searchTypes:    coreSearchService.getSearchTypes.query().$promise,
                health:         coreSearchService.checkHealth.query().$promise,
                newTenants:     coreSearchService.getTenantsFromUCMDB.query().$promise,
                searchTenants:  coreSearchService.getTenantsForUser.query().$promise,
                infraTypes:     retrieveInfrastructureTypes()
            };

            return resources;
        }

        function setSearchTypes(searchTypes) {
            self.classes = searchTypes.payload.map(function (type) {
                return {name: type.typeLabel, description: type.typeName};
            });

            // // Exclude View_Queries Search Type for Non-Admin Users -- UAT TODO: DELETE!
            // if (!AclService.hasRole('admin')) {
            //     self.classes = self.classes.filter(function (searchType) { return searchType.description !== 'view_queries' });
            // }
        }

        function setHealthStatus(health) {
            if (health) {
                if (health.payload.properties.ok === 'true') {
                    $rootScope.isUp = true;
                } else {
                    $rootScope.isUp = false;
                }
            }
            else {
                $rootScope.isUp = false;
            }
        }

        function setSearchTenants(tenants) {
            if (tenants.payload) {
                if (tenants.payload.length !== 0) {
                    tenants.payload.forEach(function (tenant) {
                        self.tenants.push({
                            id: tenant.id,
                            name: tenant.description,
                            checked: false
                        });
                    });
                }
            }
            else {
                self.noTenants = true;
            }
        }

        function setDefaultInfraType(infraTypes) {
            angular.forEach(infraTypes, function (type) {
                self.infrastructureTypes.push({
                    id: type.typeId,
                    name: type.ciType,
                    title: type.ciTypeLabel
                });
            });

            self.retrieveInfrastructureCustomAttributesBasedOnType(null, false, null);
        }

        function handleError(errorResponse) {
            $rootScope.waiting = false;
            $rootScope.isUp = false;

            log.error('SearchPageController.handleErrorResponses :: A REST Call of Chain Failed!');
        }

        function returnFromResultsPage() {
            // Get Object from Results Page
            var dataFlowObject = ComponentsDataFlow.getObjectForDataFlow();

            if (dataFlowObject) {
                self.searchOptions = dataFlowObject.searchOptions;
                fillSearchFieldsBasedOnSearchType(dataFlowObject);
            }
        }

        function editSavedSearch() {
            var dataFlowObjectFromSavedSearches = ComponentsDataFlow.returnFromSavedSearches();

            if (dataFlowObjectFromSavedSearches) {
                self.searchOptions = dataFlowObjectFromSavedSearches.searchOptions;
                fillSearchFieldsBasedOnSearchType(dataFlowObjectFromSavedSearches);
            }
        }

        function fillSearchFieldsBasedOnSearchType(dataFlowObject) {
            // Common Parts
            self.selectedClass = dataFlowObject.searchType;

            var selectedTenants = dataFlowObject.searchSelectedTenants;
            selectedTenants.forEach(function (currentItem) {
                for (var tenant in self.tenants) {
                    if (currentItem.name === self.tenants[tenant].name) {
                        self.tenants[tenant].checked = true;
                    }
                }
            });
            if (selectedTenants.length === self.tenants.length) {
                self.isAllChecked = true;
            }

            if (dataFlowObject.searchType === 'Business') {
                self.searchTerm = dataFlowObject.searchTerm;
            }
            else if (dataFlowObject.searchType === 'Infrastructure') {
                // Get Infra Type Name based on Object Format
                self.selectedInfrastructureType = dataFlowObject.searchTerm.serverSearchTerm
                    ? dataFlowObject.searchTerm.serverSearchTerm : dataFlowObject.searchTerm;
                self.changeClassType(dataFlowObject.searchOptions.infraOptions.infraOptions);
            }
            else if (dataFlowObject.searchType === 'View Queries') {
                // Get Infra Type Name based on Object Format
                self.selectedViewQuery = dataFlowObject.searchTerm;
                self.changeClassType(dataFlowObject.searchOptions.infraOptions.infraOptions);
            }
        }

        function retrieveInfrastructureTypes() {
            var defer = $q.defer();

            if (!self.infrastructureTypes) {
                self.infrastructureTypes = [];

                $rootScope.waiting = true;
                coreSearchService.retrieveInfraTypes.query(
                    function (infrastructureTypes) {
                        var retrievedTypes = infrastructureTypes.payload;
                        defer.resolve(retrievedTypes);
                    },
                    function (retrieveInfraTypesError) {
                        var errorMessage = 'SearchPageController.retrieveInfrastructureTypes :: retrieveInfraTypes :: ' + retrieveInfraTypesError.statusText;
                        defer.reject(errorMessage);
                    });

            }
            else {
                defer.resolve([]);
            }

            return defer.promise;
        }

        function initInfrastructureAttributes(attributes) {
            // For Filter Attributes
            var filterAttributes = attributes.filter(function (attribute) {
                return attribute.customAttribute === false && attribute.commentFilter === false && attribute.supportFilter === false;
            })
                .map(function (attribute) {
                    return {
                        attributeLabel: attribute.attributeLabel,
                        attributeName: attribute.attributeName,
                        attributeValue: attribute.attributeValue ? attribute.attributeValue : ''
                    }
                });

            // For Custom Attributes
            var customAttributes = attributes.filter(function (attribute) {
                return attribute.customAttribute === true;
            })
                .map(function (attribute) {
                    return {
                        attributeLabel: attribute.attributeLabel,
                        attributeName: attribute.attributeName,
                        attributeValue: attribute.attributeValue ? attribute.attributeValue : ''
                    }
                });

            // For Comment Filter
            var commentFilters = attributes.filter(function (attr) {
                return attr.commentFilter === true;
            })
                .map(function (attribute) {
                    return {
                        attributeLabel: attribute.attributeLabel,
                        attributeName: attribute.attributeName,
                        attributeValue: attribute.attributeValue ? attribute.attributeValue : ''
                    }
                });

            // For Support Attributes Filter
            var supportFilters = attributes.filter(function (attribute){
                return attribute.supportFilter === true;
            })
                .map(function (attribute) {
                    return {
                        attributeLabel: attribute.attributeLabel,
                        attributeName: attribute.attributeName,
                        attributeValue: attribute.attributeValue ? attribute.attributeValue : ''
                    }
                });

            self.attributes = [filterAttributes, customAttributes, commentFilters, supportFilters];
        }

        function initOptionsList(attributesArray, keepValue) {
            self.optionList.splice(0);
            angular.forEach(attributesArray, function (attributes) {
                if (attributes.length !== 0) {
                    var attrIndex = _.findIndex(attributes, function (attr) {
                        // Boolean check if Attribute Value is not empty
                        return attr.attributeValue;
                    });

                    if (attrIndex == -1) {
                        attrIndex = 0;
                    }

                    self.optionList.push({
                        attributeLabel: attributes[attrIndex].attributeLabel,
                        attributeName: attributes[attrIndex].attributeName,
                        attributeValue: keepValue ? attributes[attrIndex].attributeValue : ''
                    });
                }
            });
        }

        function getViewQueries(options) {
            if (!self.viewQueries) {
                $rootScope.waiting = true;
                coreSearchService.getViewQueries.query(
                    function (viewQueries) {
                        self.viewQueries = viewQueries.payload;
                        self.selectedViewQuery =
                            self.selectedViewQuery ? self.selectedViewQuery : self.viewQueries[0].queryName;

                        self.initViewQueryAttributes(_.findIndex(self.viewQueries, {queryName: self.selectedViewQuery}));

                        var initOptions = (options && options.length !== 0) ? [options] : mapQueryPropertiesToAttributes(self.viewQueries[0].viewQueriesProperties);
                        initOptionsList(initOptions, Boolean(options));

                        $rootScope.waiting = false;
                    },
                    function (getViewQueriesError) {
                        log.error("SearchPageController.getViewQueries :: getViewQueries :: ERROR: " + getViewQueriesError.statusText);
                        $rootScope.waiting = false;
                    }
                );
            }
            else {
                self.initViewQueryAttributes(_.findIndex(self.viewQueries, {queryName: self.selectedViewQuery}));

                var initOptions = (options && options.length !== 0) ? [options] : mapQueryPropertiesToAttributes(self.viewQueries[0].viewQueriesProperties);
                initOptionsList(initOptions, Boolean(options));
            }
        }

        function mapQueryPropertiesToAttributes(queryProperties) {
            var transformedProperties =
                queryProperties
                    .filter(function (prop) {
                        return prop.searchFilterProperty;
                    }).map(function (prop) {
                    return {attributeLabel: prop.propertyLabel, attributeName: prop.propertyName, attributeValue: ''}
                });

            return [transformedProperties];
        }

        function validateFields() {
            var successValidation = false;

            // if (self.selectedClass === 'View Queries') {
            //     angular.forEach(self.optionList, function (option) {
            //         if (!option.attributeValue) {
            //             self.viewQueryFailedValidation = true;
            //         }
            //         else {
            //             self.viewQueryFailedValidation = false;
            //         }
            //     });
            //
            //     successValidation = !self.viewQueryFailedValidation;
            // }
            // else {
            var selectedTenants = self.tenants.filter(function (item) {
                return item ? item.checked === true : null;
            });

            if (selectedTenants.length !== 0) {
                self.failedValidation = false;
                successValidation = true;
            }
            else {
                self.failedValidation = true;
                successValidation = false;
            }
            // }

            return successValidation;
        }

        function mapOptionsListToSearchOptions(options) {
            var infraOptions = {
                infraOptions: options,
                tableFilters: []
            }
            var businessOptions = [];

            return {
                infraOptions: infraOptions,
                businessOptions: businessOptions
            };
        }

        function getSelectedTenants() {
            return self.tenants.filter(function (item) {
                return item ? item.checked === true : null;
            });
        }

        function storeSearchFieldInLocalStorage(searchTerm, selectedClass, selectedTenants, searchOptions) {
            localStorageService.set('searchTerm', searchTerm);
            localStorageService.set('selectedClass', selectedClass);
            localStorageService.set('selectedTenants', selectedTenants);
            localStorageService.set('optionList', mapOptionsListToSearchOptions(searchOptions));
        }

        function searchBusiness() {
            var options = [];
            self.optionList = [];
            if (!self.searchTerm) {
                var searchTerm = '';
            }
            else {
                var searchTerm = self.searchTerm;
            }

            ComponentsDataFlow.createObjectForDataFlow(searchTerm, self.selectedClass, mapOptionsListToSearchOptions(options), getSelectedTenants());
            storeSearchFieldInLocalStorage(searchTerm, self.selectedClass, getSelectedTenants(), options);

            $location.path('/results_page');

        }

        function searchInfrastructure() {
            var searchTerm = {
                serverSearchTerm: self.selectedInfrastructureType,
                visibleSearchTerm: _.find(self.infrastructureTypes, {name: self.selectedInfrastructureType}).title
            }

            var options = self.optionList.filter(function (option) {
                return option.attributeValue.length !== 0;
            });

            ComponentsDataFlow.createObjectForDataFlow(searchTerm, self.selectedClass, mapOptionsListToSearchOptions(options), getSelectedTenants());
            storeSearchFieldInLocalStorage(searchTerm, self.selectedClass, getSelectedTenants(), options);

            $location.path('/results_infrastructure');
        }

        function searchViewQueries() {
            var searchTerm = self.selectedViewQuery;
            var options = self.optionList.filter(function (option) {
                return option.attributeValue.length !== 0;
            });

            // TODO -- Return from Results page
            ComponentsDataFlow.createObjectForDataFlow(searchTerm, self.selectedClass, mapOptionsListToSearchOptions(options), getSelectedTenants());
            storeSearchFieldInLocalStorage(searchTerm, self.selectedClass, getSelectedTenants(), options);

            $location.path('/view_queries');
        }
    }

})();