(function () {

    'use strict';

    angular.module('searchPage', [
        'core.componentsDataFlow',
        'core.searchService'
    ]);

})();