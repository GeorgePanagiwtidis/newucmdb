(
    function () {
        'use strict';

        angular
            .module('missingCisModule')
            .component('missingCisPage', {
                templateUrl: 'components/missing_cis_page/missing-cis-page.template.html',
                controller: MissingCisController
            });

        MissingCisController.$inject = ['$rootScope', '$q', 'NgTableParams', 'coreSearchService', 'bootboxService', 'log'];

        function MissingCisController($rootScope, $q, NgTableParams, coreSearchService, bootboxService, log) {
            var self = this;

            // View Properties
            self.missingCisData;
            self.lovs = {};
            self.tableParams;
            self.newCi = {};

            // View Methods
            self.$onInit = onInit;
            self.shouldShowInput = shouldShowInput;
            self.addNewCi = addNewCiConfirmationDialog;

            function onInit() {
                $rootScope.waiting = true;
                $q.all({serverData: getData(), lovs: getLovs()})
                    .then(function (responses) {
                            initTable(responses.serverData);
                            initLovs(responses.lovs);

                            $rootScope.waiting = false;
                        },
                        function (responsesError) {
                            bootboxService.alert('Server Error', responsesError.serverData, false);

                            self.noMissingCis = true;
                            $rootScope.waiting = false;
                        });
            }

            function shouldShowInput(index) {
                var filterArrayValues = _.values(self.tableParams.filter()).filter(function (str) {
                    return str.length !== 0
                });

                // Show Input Row only in 1st Row, 1st Page & user has not filter the results
                return ((index === 0) && (_.isEmpty(filterArrayValues)) && (self.tableParams.page() === 1));
            }

            function addNewCiConfirmationDialog() {
                if (userInputIsValid(self.newCi)) {
                    bootboxService.confirm('Confirmation Dialog', 'New record will be added in Database. Are you sure?', [], addNewCi);
                }
                else {
                    bootboxService.alert('Validation Error', 'Tenant Owner & Display Label are Required!', false);
                }
            }

            function userInputIsValid(newCi) {
                if (!(newCi.tenantOwner && newCi.displayLabel)) {
                    return false;
                }
                else {
                    return true;
                }
            }

            // Helper Functions
            function getData() {
                var defer = $q.defer();
                coreSearchService.getUcmdbMissingNodes.query(
                    function (missingCis) {
                        defer.resolve(missingCis.payload);
                    },
                    function (getUcmdbMissingNodesError) {
                        log.error('MissingCisController.getData :: Error: ' + getUcmdbMissingNodesError.statusText);

                        defer.reject('Error while Retrieving uCMDB Missing Nodes');
                    });

                return defer.promise;
            }

            function getLovs() {
                var defer = $q.defer();
                coreSearchService.getLovs.query(
                    function (lovs) {
                        defer.resolve(lovs.payload);
                    },
                    function (getLovsError) {
                        log.error('MissingCisController.getLovs :: Error: ' + getLovsError.statusText);

                        defer.reject('Error while Retrieving Lovs');
                    });

                return defer.promise;
            }

            function initTable(serverData) {
                if (serverData.length !== 0) {
                    self.missingCisData = serverData;
                    self.missingCisData.unshift({});

                    self.noMissingCis = false;

                    self.tableParams = new NgTableParams(
                        {
                            sorting: {insertedDate: "desc"},
                            page: 1,
                            count: 10
                        },
                        {
                            counts: [],
                            filterOptions: {filterFilterName: 'tasksDateFilter'},
                            dataset: self.missingCisData
                        }
                    );
                }
                else {
                    self.noMissingCis = true;
                }
            }

            function initLovs(lovs) {
                self.lovs.environmentLovs = lovs['Node_Environment'];
                self.lovs.tenantOwnerLovs = lovs['TenantOwner'];
            }

            function addNewCi(response) {
                if (response) {
                    $rootScope.waiting = true;
                    coreSearchService.addUcmdbMissingNode.query(self.newCi,
                        function (addedCi) {
                            self.missingCisData[0] = addedCi.payload;
                            self.missingCisData.unshift({});

                            self.newCi = {};
                            self.tableParams.reload();

                            $rootScope.waiting = false;
                        },
                        function (addUcmdbMissingNodeError) {
                            var errorMessage = (addUcmdbMissingNodeError.data && addUcmdbMissingNodeError.data.error)
                                                ? addUcmdbMissingNodeError.data.error.message : 'Error while adding new CI';

                            log.error('MissingCisController.addNewCi :: Error: ' + addUcmdbMissingNodeError.statusText);
                            bootboxService.alert('Server Error', errorMessage, false);

                            $rootScope.waiting = false;
                        });
                }
            }

            self.downloadMissingCisExcel = function () {
                $rootScope.waiting = true;
                coreSearchService.exportMissingCis.query(
                    function (excelBytes) {
                        download(excelBytes.data, 'Missing CIs.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

                        $rootScope.waiting = false;
                    }
                    , function () {
                        bootboxService.alert('Server Error', 'Missing CIs Excel could not be exported!', false);

                        $rootScope.waiting = false;
                    });

            }


        }
    }

)();