(function () {

    'use strict';

// Declare app level module which depends on views, and components
    angular.module('UcmdbWebApp', [
        'core',
        'ucmdbHeader',
        'homePageModule',
        'resultsBusiness',
        'resultsPageInfrastructureModule',
        'resultsViewQueries',
        'savedSearches',
        'reportingModule',
        'searchPage',
        'userManagement',
        'modalInfo',
        'userManagementCreate',
        'myTasksModule',
        'errorPage',
        'editTaskModal',
        'resultsBusinessModal',
        'tasksReportingModal',
        'nodeReporting',
        'auditModal',
        'reportActionsModule',
        'download-excel-btn',
        'columnsFilteringBtn',
        'support-modal',
        'commentModal',
        'commentPage',
        'missingCisModule',
        'relatedCisModule',
        'workflowStatsModule',
        'infraTableDirectiveModule',

        'ngRoute',
        'ngAnimate',
        'angular.filter',
        'LocalStorageModule',
        'ngTable',
        'mm.acl',
        'stickyHeader',
        'ngIdle',
        'hl.sticky',
        'ui.bootstrap',
        'angularjs-dropdown-multiselect'
    ]);
})();
