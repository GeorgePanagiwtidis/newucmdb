(function () {
    'use strict';

    angular
        .module('support-modal')
        .directive('supportAttributesModal', SupportModalDirectiveFn);
    
    function SupportModalDirectiveFn() {
        return {
            restrict: 'E',
            scope: {
                fromWorkflow: '<',
                editableFields: '<',
                supportAttributes: '='
            },
            templateUrl: 'directives/support-attributes-modal/support-modal.template.html',
            controller: SupportModalDirectiveControllerFn,
            controllerAs: 'sCtrl',
            bindToController: true
        }
        
        function SupportModalDirectiveControllerFn() {
            var self = this;

            self.onModalClose = onModalClose;
            
            
            function onModalClose(evt) {
                evt.preventDefault();
                angular.element('#edit-support-attributes').modal('hide');
            }

            // Hacky Way to Close Modal if Other Modal are Closed
            angular.element('.modal').each(function () {
                angular.element(this).on('hidden.bs.modal', function () {
                    angular.element('#edit-support-attributes').modal('hide');
                });
            });
        }
    }
})();