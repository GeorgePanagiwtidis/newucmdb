(function () {

    'use strict';

    angular
        .module('ucmdbHeader')
        .directive('ucmdbHeader', function () {
            return {
                restrict: 'E',
                templateUrl: 'directives/header_directive/header-directive.template.html',
                controller: ['__env', '$scope', '$window', '$location', 'ComponentsDataFlow', 'AuthService', 'localStorageService',
                    function (__env, $scope, $window, $location, ComponentsDataFlow, AuthService, localStorageService) {
                        var header = $scope;

                        header.isAdmin;
                        header.isWorkflowUser;
                        header.loggedUser;
                        header.numOfTasks;
                        header.activeTab = splitUrl($location.path());

                        // UAT Flag -- Header Title
                        header.isUATEnv = __env.UAT_ENV;
                        header.isNewUcmdb = __env.NEW_UCMDB;

                        header.exitUcmdb = function () {
                            AuthService.exitApp();

                            $window.location.href = 'exit.jsp';
                        }

                        // --- WATCHERS START ---
                        // Watcher for Logged User Flag
                        header.$watch(function () {
                                return localStorageService.get('loggedUser');
                            },
                            function () {
                                if (!header.loggedUser) {
                                    header.loggedUser = localStorageService.get('loggedUser');
                                }
                            });

                        // Watcher for Number of Tasks Badge
                        header.$watch(function () {
                                return localStorageService.get('numOfTasks');
                            },
                            function () {
                                header.numOfTasks = localStorageService.get('numOfTasks');
                            });

                        // Watcher for isAdmin flag
                        header.$watch(function () {
                                return localStorageService.get('isAdmin');
                            },
                            function () {
                                header.isAdmin = localStorageService.get('isAdmin');
                            });

                        // Watcher for isWorkflowUser flag
                        header.$watch(function () {
                                return localStorageService.get('isWorkflowUser');
                            },
                            function () {
                                header.isWorkflowUser = localStorageService.get('isWorkflowUser');
                            });
                        // --- WATCHERS END ---

                        // Delete Dataflow Objects when clicking on Search Menu Item
                        header.deleteDataFlowObjects = function () {
                            ComponentsDataFlow.deleteDataFlowObjects();
                        }

                        // Set Active Tab
                        header.$on('$locationChangeSuccess', function () {
                            header.activeTab = splitUrl($location.path());
                        });

                        // Helper Method for Active Tabs
                        function splitUrl(url) {
                            var lastSlashFound = url.lastIndexOf('/');
                            var splittedUrl = lastSlashFound === 0 ? url : url.substr(0, lastSlashFound);
                            return splittedUrl;
                        }
                    }
                ]
            }
        });

})();