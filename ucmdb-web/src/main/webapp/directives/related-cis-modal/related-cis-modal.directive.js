(function () {
    'use strict';

    angular
        .module('relatedCisModule')
        .directive('relatedCis', RelatedCisDirective);

    function RelatedCisDirective() {
        RelatedCisControllerFn.$inject = ['$rootScope', '$q', 'log', 'bootboxService', 'coreSearchService', 'NgTableParams'];

        return {
            restrict: 'E',
            templateUrl: 'directives/related-cis-modal/related-cis-modal.template.html',
            scope: {
                ciGlobalId: '=',
                ciDisplayLabel: '=',
                ciTypes: '=',
                showDetails: '&'
            },

            controller: RelatedCisControllerFn,
            controllerAs: 'relatedCtrl',
            bindToController: true
        };

        function RelatedCisControllerFn($rootScope, $q, log, bootboxService, coreSearchService, NgTableParams) {
            var self = this;
            self.ngTableColumns = [];
            self.ngTable = null;

            self.selectedCiType = 'Select CI Type';
            self.noCiTypeSelected = true;

            self.getRelatedCisOfType = function (ciTypeObject) {
                self.selectedCiType = ciTypeObject.label;
                self.noCiTypeSelected = false;

                $rootScope.waitingModal = true;
                $q.all({
                    ciAttributes: getCiAttributesOfType(ciTypeObject.name),
                    relatedCisData: getRelatedCIsData(ciTypeObject)
                }).then(function (responses) {
                        initTable(responses.ciAttributes, responses.relatedCisData);

                        $rootScope.waitingModal = false;
                    },
                    function (responsesError) {
                        bootboxService.alert('Server Error', 'An error occurred while Retrieving Related CIs!' ,false);
                        $rootScope.waitingModal = false;
                    });

            }

            $('#relatedCisModal').on('hidden.bs.modal', function () {
                self.selectedCiType = 'Select CI Type';
                self.ngTableColumns = [];
                self.ngTable = null;

                $rootScope.waitingModal = false;
                self.noCiTypeSelected = true;
            });

            self.showRelatedDetails = function (relatedItem) {
                self.showDetails({item : relatedItem, fromRelatedModal: true});
            }

            // Helper Methods
            function constructRequestObject(ciTypeObject) {
                return {
                    type: ciTypeObject.name,
                    cis: [{id: self.ciGlobalId}]
                };
            }

            function getCiAttributesOfType(ciType) {
                var defer = $q.defer();

                coreSearchService.getCiAttributes.query({type: ciType},
                    function (ciAttributes) {
                        defer.resolve(ciAttributes.payload);
                    },
                    function (getCiAttributesError) {
                        log.error('RelatedCisControllerFn.getCiAttributesOfType :: Error ' + getCiAttributesError.statusText);
                        defer.reject('Could not Retrieve Ci Attributes');
                    });

                return defer.promise;
            }

            function getRelatedCIsData(ciTypeObject) {
                var defer = $q.defer();

                coreSearchService.getComputerChildren.query(constructRequestObject(ciTypeObject),
                    function (relatedCisOfType) {
                        var cisProperties = relatedCisOfType.payload.map(function (ci) {
                            return ci.properties;
                        });

                        defer.resolve(cisProperties);
                    },
                    function (relatedCisOfTypeError) {
                        log.error('RelatedCisControllerFn.getRelatedCIsData :: Error ' + relatedCisOfTypeError.statusText);
                        defer.reject('Could not Retrieve Related CIs');
                    });

                return defer.promise;
            }

            function initTable(ciAttributes, relatedCiData) {
                // Construct Columns
                angular.forEach(relatedCiData, function (relatedCi) {
                    constructColumnObject(ciAttributes, relatedCi)
                });
                sortColumnsByPriority();

                self.ngTable = new NgTableParams({
                        page: 1,
                        count: 10
                    },
                    {
                        counts: [],
                        dataset: relatedCiData
                    });
            }

            function constructColumnObject(ciAttributes, properties) {
                for (var colKey in properties) {
                    var colOb = _.find(self.ngTableColumns, {field: colKey});

                    if (!colOb) {
                        // Construct Column Object
                        colOb = {};
                        colOb.title = ciAttributes ? _.find(ciAttributes, {attributeName: colKey}).attributeLabel : _.startCase(colKey);
                        colOb.field = colKey;
                        // colOb.filter = {};
                        // colOb.filter[colKey] = 'select';
                        colOb.sortable = colKey;
                        colOb.show = true;
                        colOb.priority = ciAttributes ? _.find(ciAttributes, {attributeName: colKey}).attributePriority : 100;

                        self.ngTableColumns.push(colOb);
                    }
                }
            }

            function sortColumnsByPriority() {
                self.ngTableColumns.sort(function (col1, col2) {
                    return col1.priority - col2.priority;
                })
            }
        }
    }
})();