(function () {
    'use strict';

    angular
        .module('infraTableDirectiveModule')
        .directive('infraTableDir', InfraTable);

    function InfraTable() {
        return {
            restrict: 'A',
            scope: {
                infraTableDir: '='
            },
            link: linkFn
        }
        
        function linkFn(scope, elm, attrs) {
            elm.on('show.bs.dropdown', function (evt) {
                if (scope.infraTableDir.total() < 10) {
                    var currentHeight = $(evt.currentTarget).height();
                    var dropdownMenuHeight = $(evt.currentTarget).find('.dropdown-menu').outerHeight();

                    $(evt.currentTarget).height(currentHeight + dropdownMenuHeight);
                }
            });
            
            elm.on('hide.bs.dropdown', function (evt) {
                // Restore Height
                $(evt.currentTarget).height('auto');
            });
        }
    }
})();