(function () {

    'use strict';

    angular
        .module('modalInfo')
        .directive('modalInfo', function () {
            return {
                restrict: 'AE',
                scope: {
                    title: '=',
                    description: '=',
                    data: '='
                },
                templateUrl: 'directives/modal_info/modal.template.html'
            }
        });

})();