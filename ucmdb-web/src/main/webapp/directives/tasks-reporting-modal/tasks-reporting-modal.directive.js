(function () {

    'use strict';

    angular
        .module('tasksReportingModal')
        .directive('tasksReportModal', function () {
            return {
                restrict: 'E',
                scope: {
                    reportData: '=',
                    requestObject: '='
                },
                templateUrl: 'directives/tasks-reporting-modal/tasks-reporting-modal.template.html',
                controller: ['$scope', '$rootScope', 'NgTableParams', function ($scope, $rootScope, NgTableParams) {
                    var self = $scope;

                    self.tableColumns = [];
                    self.tableParams;

                    // Check for Changes in Reports Data
                    $scope.$watch('reportData',
                        function () {
                            $rootScope.waitingModal = false;

                            if (self.reportData) {
                                // Init Table Columns
                                self.tableColumns = [];
                                for (var col in self.reportData[0]) {
                                    var colObject = {
                                        field: col,
                                        title: _.startCase(col),
                                        sortable: col,
                                        filter: {},
                                        class: 'ngTableCol',
                                        show: true
                                    }
                                    colObject.filter[col] = 'text';

                                    self.tableColumns.push(colObject);
                                }

                                self.tableParams = new NgTableParams({count: 50}, {counts: [10, 50, 100], dataset: self.reportData});
                            }
                        });
                }]
            }
        });

})();