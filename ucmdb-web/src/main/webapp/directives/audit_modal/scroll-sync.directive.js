(function () {

    'use strict';

    angular
        .module('auditModal')
        .directive('scrollingDiv', function () {
            return function (scope, element, attrs) {
                var oppositeDiv = element[0].id === 'before' ? '#after' : '#before';

                element.on("scroll", function (e) {
                    e.preventDefault();

                    $(oppositeDiv).scrollTop($(this).scrollTop());
                });
            };
        });
})();