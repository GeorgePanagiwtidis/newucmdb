(function () {

    'use strict';

    angular
        .module('auditModal')
        .directive('auditModal', function () {
            return {
                restrict: 'E',
                templateUrl: 'directives/audit_modal/audit-modal.template.html',
                scope: {
                    data: '='
                },
                controller: ['$rootScope', '$scope', '$q', 'coreSearchService', 'log', function ($rootScope, $scope, $q, coreSearchService, log) {
                    var self = $scope;

                    $scope.$watch('data', function () {
                        if (self.data) {
                            self.transformDataForPresentation();
                        }
                    });

                    self.transformDataForPresentation = function () {
                        var oldData = self.data.beforeUpdate;
                        var currentData = self.data.afterUpdate;

                        self.transformedOldData = oldData.map(function (item) {
                            return {name: item.fieldLabel, value: item.fieldValue};
                        });

                        self.transformedCurrentData = currentData.map(function (item) {
                            return {name: item.fieldLabel, value: item.fieldValue};
                        });
                    }

                    self.showCiDetails = function (globalId) {
                        // Close this modal
                        angular.element('#auditing-modal').modal('hide');
                        // Scroll Page to Top in order to fix Static Header bug
                        angular.element('html').scrollTop(0);

                        $rootScope.waiting = true;
                        coreSearchService.getBusinessRowData.query({id: globalId},
                            function (response) {
                                // CI Type from GenericCI Properties
                                var ciType = response.payload.type;

                                // Get Display Names of Properties from Promise
                                getAttributeNames(ciType).then(
                                    function (attributeNames) {
                                        createModalObject(response.payload, attributeNames);

                                        // Open Modal
                                        angular.element('#result').modal('show');

                                        $rootScope.waiting = false;
                                    },
                                    function (getAttributeNamesError) {
                                        log.error(getAttributeNamesError);
                                        $rootScope.waiting = false;
                                    }
                                )
                            }, function (showCiDetailsError) {
                                log.error('audit-modal :: showCiDetails :: ERROR: ' + showCiDetailsError.statusText);

                                $rootScope.waiting = false;
                            })
                    }

                    // Helper Functions
                    function getAttributeNames(type) {
                        var defer = $q.defer();
                        coreSearchService.getCiAttributes.query({type: type},
                            function (ciAttributes) {
                                var attributes = {};
                                angular.forEach(ciAttributes.payload, function (attribute) {
                                    attributes[attribute.attributeName] = attribute.attributeLabel;
                                });

                                defer.resolve(attributes);
                            },
                            function (getCiAttributesError) {
                                defer.reject('AuditModal (anonymous function) :: getAttributeNames :: ERROR: ' + getCiAttributesError.statusText);
                            });

                        return defer.promise;
                    }

                    function createModalObject(ciDetails, attributeNames) {
                        var displayLabel =
                            _.find(ciDetails.groups, {groupName: 'Technical Attributes'}).properties['display_label'];

                        var ciProperties = {};
                        angular.forEach(ciDetails.groups, function (groupObject) {
                            _.assign(ciProperties, groupObject.properties);
                        });

                        self.ciDetails = {
                            globalId: ciDetails.globalId,
                            name: displayLabel,
                            attributeNames: attributeNames,
                            groupAttributes: ciDetails.groups,
                            decoupledCiProperties: ciProperties
                        }
                    }
                }]
            }
        });
})();