(
    function () {
        'use strict';

        angular
            .module('download-excel-btn')
            .directive('excelBtn', excelBtnDirectiveFunction);

        function excelBtnDirectiveFunction() {
            var template =
                '<button style="float: left;" class="btn btn-default" ng-click="exportExcel()">' +
                '<span class="custom-excel"></span>Export' +
                '</button>';

            DownloadExcelController.$inject = ['$rootScope', '$scope', 'coreSearchService']

            return {
                restrict: 'E',
                template: template,
                scope: {
                    reportType: '@',
                    requestObject: '='
                },
                controller: DownloadExcelController
            };

            function DownloadExcelController($rootScope, $scope, coreSearchService) {
                $scope.exportExcel = function () {
                    $scope.reportType === 'nodes' ? exportNodeReportsBasedOnStatus() : exportTaskReportsBasedOnGroup();
                }

                function exportNodeReportsBasedOnStatus() {
                    $rootScope.waitingModal = true;
                    coreSearchService.exportNodeSearch.query($scope.requestObject,
                        // Success Response
                        function (excelData) {
                            var fileName = generateExportFileName('node');
                            download(excelData.data, fileName, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                            $rootScope.waitingModal = false;
                        },
                        // Error Response
                        function (exportError) {

                            $rootScope.waitingModal = false;
                        });
                }

                function exportTaskReportsBasedOnGroup() {
                    $rootScope.waitingModal = true;
                    coreSearchService.exportTaskSearch.query($scope.requestObject,
                        // Success Response
                        function (excelData) {
                            var fileName = generateExportFileName('task');
                            download(excelData.data, fileName, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                            $rootScope.waitingModal = false;
                        },
                        // Error Response
                        function (exportError) {

                            $rootScope.waitingModal = false;
                        });
                }

                function generateExportFileName(type) {
                    var fileName = '';

                    if (type === 'task') {
                        fileName = 'taskReport_' + $scope.requestObject.taskGroup;
                    } else {
                        fileName = 'nodeReport_' + $scope.requestObject.nodeStatus;
                    }

                    fileName = fileName.concat('.xls');

                    return fileName;
                }
            }
        }
    }
)();