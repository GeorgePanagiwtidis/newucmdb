(
    function () {
        'use strict';

        angular.module('download-excel-btn', [
            'core.searchService'
        ]);
    }
)();