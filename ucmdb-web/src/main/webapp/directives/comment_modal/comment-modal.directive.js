(function () {

    'use strict';

    angular
        .module('commentModal')
        .directive('commentModal', function () {
            return {
                restrict: 'E',
                scope: {
                    ciType: '=',
                    onCommentAddition: '&'
                },
                templateUrl: 'directives/comment_modal/comment-modal.template.html',
                controller: ['$scope', 'coreSearchService', function ($scope, coreSearchService) {
                    var self = $scope;
                    self.commentText;

                    self.isCiTypeOfNode = false;

                    // Check if CI is Appropriate for Comment Addition
                    self.$watch('ciType', function () {
                        if (self.ciType) {
                            coreSearchService.isCiTypeOfNode.query({type: self.ciType}, function (isCiTypeOfNodeResp) {
                                self.isCiTypeOfNode = isCiTypeOfNodeResp.payload;
                            });
                        }
                        else {
                            self.isCiTypeOfNode = false;
                        }
                    });

                    self.closeCommentModal = function (evt) {
                        evt.preventDefault();
                        angular.element('#commentModal').modal('hide');
                    }

                    // Hacky Way to Close Modal if Other Modal are Closed
                    angular.element('.modal').each(function () {
                        angular.element(this).on('hidden.bs.modal', function () {
                            angular.element('#commentModal').modal('hide');
                        });
                    });

                    self.saveComment = function () {
                        angular.element('#commentModal').modal('hide');

                        self.onCommentAddition({comment: self.commentText});
                        self.commentText = '';
                    }

                }]
            }
        });
})();