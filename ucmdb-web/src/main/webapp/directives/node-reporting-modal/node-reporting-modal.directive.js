(function () {

    'use strict';

    angular
        .module('nodeReporting')
        .directive('nodeModal', function () {
            return {
                restrict: 'E',
                templateUrl: 'directives/node-reporting-modal/node-reporting-modal.template.html',
                scope: {
                    reportData: '=',
                    requestObject: '=',
                    onNodeClick: '&',
                    onAuditingRequest: '&'
                },
                controller: ['$scope', 'log', 'NgTableParams', function ($scope, log, NgTableParams) {
                    var self = $scope;
                    self.dynamicCols = [];
                    self.currentPage = 1;
                    self.totalItems = 0;

                    // Custom Counts
                    self.countsObject = {
                        selectedCount: 50,
                        counts: [10, 50, 100]
                    };

                    self.filterText = '';

                    // Check for Changes in Reports Data
                    self.$watch('reportData',
                        function () {
                            if (self.reportData) {
                                self.showFirstPage();
                            }
                        });

                    self.showFirstPage = function () {
                        self.modalData = [];
                        self.totalItems = self.reportData.processReportDTOList.length;
                        self.currentPage = 1;

                        var paginatedData = self.reportData.processReportDTOList.slice(0, self.countsObject.selectedCount);
                        angular.forEach(paginatedData, function (row) {
                            row.isExpanded = false;
                            self.modalData.push(row);
                        });
                    }

                    self.expandNode = function (str, index, processId) {
                        self.getNodeDetails(index, processId);

                        var nodeElement = angular.element(str + index);
                        self.modalData[index].isExpanded = !self.modalData[index].isExpanded;
                        nodeElement.toggle('slow');
                    }

                    self.changePage = function () {
                        if (!self.filterText) {
                            self.modalData = [];
                            var paginatedData =
                                self.reportData.processReportDTOList.slice((self.currentPage - 1) * self.countsObject.selectedCount, ((self.currentPage - 1) * self.countsObject.selectedCount) + self.countsObject.selectedCount);
                            angular.forEach(paginatedData, function (row) {
                                row.isExpanded = false;
                                self.modalData.push(row);
                            });
                        }
                        else {
                            self.filterResults((self.currentPage - 1) * self.countsObject.selectedCount);
                        }
                    }

                    self.filterResults = function (fromIndex) {
                        if (self.filterText) {
                            self.modalData = self.reportData.processReportDTOList.filter(function (report) {
                                return report.displayLabel.startsWith(self.filterText) || report.processId.startsWith(self.filterText);
                            });

                            self.totalItems = self.modalData.length;
                            self.currentPage = (fromIndex / self.countsObject.selectedCount) + 1;
                            self.modalData = self.modalData.slice(fromIndex, fromIndex + self.countsObject.selectedCount);
                        }
                        else {
                            self.showFirstPage();
                        }
                    }

                    self.getNodeDetails = function (index, procId) {
                        if (!self.modalData[index].nodeTable) {
                            self.onNodeClick({processId: procId}).then(function (serverData) {
                                    // Initialize NgTableParams
                                    self.modalData[index].nodeTable = new NgTableParams({count: 10}, {counts:[], dataset: serverData});

                                    // Initialize Columns
                                    if (self.dynamicCols.length === 0) {
                                        angular.forEach(Object.keys(serverData[0]), function (col) {
                                            var isDate = col.toLowerCase().indexOf('date') != -1 ? true : false;
                                            var colObject = {
                                                field: col,
                                                title: _.startCase(col),
                                                sortable: col,
                                                filter: {},
                                                class: 'ngTableCol',
                                                show: true,

                                                isDate: isDate
                                            }
                                            colObject.filter[col] = 'text';
                                            self.dynamicCols.push(colObject);
                                        });

                                        self.removeTaskIdFromTableColumns(self.dynamicCols);
                                    }
                                },
                                function (serverError) {
                                    log.error("nodeModal.getNodeDetails :: onNodeClick :: " + serverError.statusText);
                                }
                            );
                        }
                    }

                    self.removeTaskIdFromTableColumns = function (tableColumns) {
                        self.dynamicCols = tableColumns.filter(function (column) {
                            return column.field !== 'taskId';
                        })
                    }

                    self.getNodeAuditing = function (taskId, nodeId) {
                        self.onAuditingRequest({taskId: taskId, nodeId: nodeId});
                    }
                }]
            }
        });

})();