(function () {

    'use strict';

    angular
        .module('editTaskModal')
        .directive('editTask', function () {
            return {
                restrict: 'E',
                templateUrl: 'directives/edit-task-modal/edit-task-modal.template.html',
                scope: {
                    taskAttrs: '=task',
                    onCommit: '&',
                    onAssign: '&',
                    onReassign: '&',
                    viewNodeInfo: '&',
                    nodeCustomAttributes: '='
                },
                controller: ['$scope', '$compile', 'bootboxService', 'lovsService', function ($scope, $compile, bootboxService, lovsService) {
                    var self = $scope;

                    self.multiSelectSettings = {
                        showCheckAll: false,
                        showUncheckAll: false,
                        smartButtonMaxItems: 4
                    }

                    self.validForm = true;

                    self.validateFields = function () {
                        self.validForm = true;
                        var taskGroups = self.taskAttrs.attrs.groups;

                        angular.forEach(taskGroups, function (group) {
                            angular.forEach(group.attributes, function (attributesArray) {
                                angular.forEach(attributesArray, function (attr) {
                                    if (attr.mandatory || attributeValueCannotBeNull(group.groupName, attr)) {
                                        if (!attr.fieldValue) {
                                            attr.validationError = true;
                                            self.validForm = false;
                                        }
                                        else {
                                            attr.validationError = false;
                                        }
                                    }
                                    else {
                                        attr.validationError = false;
                                    }
                                });
                            });
                        });

                        if (!self.validForm) {
                            var editModal = angular.element('#edit-modal-body');
                            editModal.animate({scrollTop: 0}, 'slow');
                            bootboxService.alert('Validation Error', 'Please check again the form. You should complete all the mandatory fields.', false);
                        }

                        return self.validForm;
                    }

                    function attributeValueCannotBeNull(groupName, attribute) {
                        var copiedGroup = _.find(self.taskAttrs.copyOfAttributes.groups, {groupName: groupName});
                        var copiedAttribute = _.find(copiedGroup.attributes, {fieldName: attribute.fieldName});
                        var customAttributeFromDb = _.find(self.nodeCustomAttributes, {fieldName: attribute.fieldName});

                        return (customAttributeFromDb.mandatory && (copiedAttribute.fieldValue.length !== 0 && attribute.fieldValue.length === 0));
                    }

                    self.tenantValidation = function (type) {
                        // Validate Tenant COSMOTE
                        angular.forEach(self.taskAttrs.attrs.groups, function (group) {
                            if (group.groupName.contains('Description')) {
                                angular.forEach(group.attributes, function (attributesArray) {
                                    angular.forEach(attributesArray, function (attribute) {
                                        if (attribute.fieldLabel === 'Owner Tenant') {
                                            if (attribute.fieldValue.toLowerCase().trim() === 'cosmote') {
                                                bootboxService.customDialog('Validation Info', 'tenantValidation', function (result) {
                                                    if (result) {
                                                        attribute.fieldValue = angular.element('#tenantLov option:selected').text();
                                                        self.confirmAction(type, false);
                                                        return;
                                                    }
                                                });
                                            }
                                            else {
                                                self.confirmAction(type, false);
                                                return;
                                            }
                                        }
                                    })
                                });
                            }
                        });
                    }

                    self.confirmAction = function (type, discardChanges) {
                        var message;
                        if (type === 'Save') {
                            message = 'Your changes will be saved. Are you sure?'
                        } else if (type === 'Submit') {
                            message = 'The task will be submitted. Are you sure?'
                        }
                        if (type === 'Reassign') {
                            message = 'The task will be reassigned. Are you sure?'
                        }
                        bootboxService.confirm('Confirmation', message, [], function (result) {
                            if (result) {
                                self.onCommit({taskAttrs: self.taskAttrs, discard: discardChanges});
                            }
                        });
                    }

                    self.confirmSaveChangesOnReassignTask = function () {
                        bootboxService.confirm('Changes Detected', 'Do you want to save your changes before reassigning this task?', ['No', 'Yes'],
                            function (response) {
                                // Yes Response
                                if (response) {
                                    self.confirmAction('Reassign', false);
                                }
                                // No Response
                                else {
                                    self.confirmAction('Reassign', true);
                                }
                            });
                    }

                    self.doAction = function (type) {
                        transformUserTypedProperties();

                        if (type === 'Reassign') {
                            // Init reassignTask Object
                            self.taskAttrs.reassignedTask = {};

                            var reassignTask = self.taskAttrs.reassignedTask;

                            self.onReassign().then(
                                function (groups) {
                                    self.totalGroups = groups;
                                    self.selectedGroup = _.find(self.totalGroups, {description: 'it-opsdev'});

                                    bootboxService.customDialog('Reassign Task', 'reassignTask', function () {
                                        reassignTask.reassignReason = angular.element('#reassignmentReason').val();
                                        reassignTask.groupId = angular.element('#reassignmentGroups').val();

                                        self.taskAttrs.attrs.selectedAction = type;
                                        self.taskAttrs.attrs.reassignTask = reassignTask;
                                        // Fixed Bug on Saved Changes
                                        self.taskAttrs.copyOfAttributes.selectedAction = type;
                                        self.taskAttrs.copyOfAttributes.reassignTask = reassignTask;

                                        self.confirmSaveChangesOnReassignTask();
                                    }, $compile, $scope);
                                });
                        }
                        else if (type === 'Submit') {
                            if (self.validateFields()) {
                                self.taskAttrs.attrs.selectedAction = type;
                                self.tenantValidation(type);
                            }
                        }
                        else if (type === 'Save') {
                            self.taskAttrs.attrs.selectedAction = type;
                            self.confirmAction(type, false);
                        }
                    }

                    self.assignOnMe = function () {
                        self.onAssign({task: self.taskAttrs.task});
                    }

                    self.nodeDetails = function () {
                        if (self.taskAttrs.attrs.nodeGlobalId) {
                            self.viewNodeInfo({nodeId: self.taskAttrs.attrs.nodeGlobalId});
                        }
                    }

                    // Helper Functions
                    self.getAttributeValueBasedOnGroup = function (group, attributeName) {
                        angular.forEach(self.taskAttrs.attrs.groups, function (group) {
                            if (group.groupName.indexOf(group) !== -1) {
                                angular.forEach(group.attributes, function (attributesArray) {
                                    angular.forEach(attributesArray, function (atttribute) {
                                        if (atttribute.fieldName === attributeName) {
                                            return atttribute.fieldValue;
                                        }
                                    });
                                });
                            }
                        });
                    }

                    self.setAttributesLOVBasedOnGroup = function (groupName, attributeName, valuesLOV, attributeValue, mandatory) {
                        angular.forEach(self.taskAttrs.attrs.groups, function (group) {
                            if (group.groupName.indexOf(groupName) !== -1) {
                                angular.forEach(group.attributes, function (attributesArray) {
                                    angular.forEach(attributesArray, function (atttribute) {
                                        if (atttribute.fieldName === attributeName) {
                                            atttribute.fieldLOV = valuesLOV;

                                            if (mandatory !== undefined) {
                                                atttribute.mandatory = mandatory;
                                            }

                                            if (attributeValue !== undefined && attributeValue !== null) {
                                                atttribute.fieldValue = attributeValue;
                                            }
                                        }
                                    });
                                });
                            }
                        });
                    }

                    self.changeMandatoryAttributesFromArray = function (attributeNamesArray) {
                        angular.forEach(self.taskAttrs.attrs.groups, function (group) {
                                angular.forEach(group.attributes, function (attributesArray) {
                                    angular.forEach(attributesArray, function (atttribute) {
                                        if (attributeNamesArray.indexOf(atttribute.fieldName) !== -1) {
                                            atttribute.mandatory = true;
                                        }
                                        else {
                                            atttribute.mandatory = false;
                                        }
                                    });
                                });
                        });
                    }

                    self.changeOptionAction = function (fieldName, fieldValue) {
                        if (fieldName == 'TenantOwner') {
                            self.changeMandatoryAttributes(fieldValue);
                            self.changeApplicationDomainLOVs(fieldValue);
                        }
                        else if (fieldName == 'domain') {
                            self.changeCategoryLOVs(fieldValue);
                        }
                    }

                    self.changeMandatoryAttributes = function (selectedTenant) {
                        var mandatoryAttributes = getMandatoryAttributesForTenant(selectedTenant);
                        self.changeMandatoryAttributesFromArray(mandatoryAttributes);
                    }

                    self.changeApplicationDomainLOVs = function (tenant) {
                        if (tenant) {
                            var ciType = self.taskAttrs.copyOfAttributes.ciType;

                            lovsService.getLovsByCiTypeAndTenant(ciType, tenant).then(function (lovsResponse) {
                                var domainLovs = lovsResponse.payload.domain.map(function (lov) {
                                    return lov.value;
                                });
                                self.setAttributesLOVBasedOnGroup('Application Attributes', 'domain', domainLovs, '', (tenant === 'CCH'));
                            });
                        }
                    }

                    self.changeCategoryLOVs = function (applicationDomainValue) {
                        // Find Tenant Value
                        // var descriptionAtributesGroup = _.find(self.taskAttrs.attrs.groups, {groupName: 'Description Attributes'});
                        // var tenantOwnerValue = _.find(descriptionAtributesGroup.attributes, {fieldName: 'TenantOwner'}).fieldValue;

                        if (applicationDomainValue === 'CCH-OTHER') {
                            self.setAttributesLOVBasedOnGroup('Description Attributes', 'category', ['TO-BE-DELETED(JUNK)', 'TO-BE-DECOMMISSIONED']);
                        }
                        else {
                            self.setAttributesLOVBasedOnGroup('Description Attributes', 'category', self.taskAttrs.categoryLOV);
                        }
                    }

                    self.showInputBasedOnConditions = function (property) {
                        var valueIsIncludedInPropertyLov = property.fieldLOV.indexOf(property.fieldValue) !== -1;
                        var valueIsEmpty = property.fieldValue ? property.fieldValue.length === 0 : true;

                        if (!(valueIsIncludedInPropertyLov || valueIsEmpty)) {
                            property.customValue = property.fieldValue;
                            // property.fieldValue= 'Other...';
                            return true;
                        }
                        else {
                            return false;
                        }
                    }

                    self.manipulateMultiSelection = function (attribute, selectFlag) {
                        return function (item) {
                            if (selectFlag) {
                                attribute.fieldValue = attribute.fieldValue ? attribute.fieldValue.concat(',' + item.label) : item.label;
                            }
                            else {
                                var values = attribute.fieldValue.split(',');
                                values = values.filter(function (val) {
                                    return val != item.label;
                                });

                                attribute.fieldValue = values.join();
                            }

                            // angular.forEach(attribute.selectedValuesFromMultiple, function (selectValue) {
                            //     attribute.fieldValue = attribute.fieldValue.concat(selectValue.label + ',');
                            // });
                            //
                            // attribute.fieldValue = attribute.fieldValue.substr(0, attribute.fieldValue.length - 1);
                        }
                    }

                    function getMandatoryAttributesForTenant(tenant) {
                        // TODO -- Check if we can implement a WS for this

                        var mandatoryAttributes;
                        if (tenant === 'CCH') {
                            mandatoryAttributes = ['TenantOwner', 'domain'];
                        }
                        else if (tenant === 'TMNL') {
                            mandatoryAttributes = ['TenantOwner', 'category', 'role', 'Node_Environment', 'project_code', 'Is_DR', 'dr_ranking'];
                        }
                        else {
                            mandatoryAttributes = ['TenantOwner', 'category', 'role', 'Node_Environment'];
                        }

                        return mandatoryAttributes;
                    }

                    function transformUserTypedProperties() {
                        angular.forEach(self.taskAttrs.attrs.groups, function (group) {
                            angular.forEach(group.attributes, function (attributesArray) {
                                angular.forEach(attributesArray, function (attribute) {
                                    if (attribute.fieldValue == 'Other...') {
                                        attribute.fieldValue = attribute.customValue ? attribute.customValue : '';
                                    }
                                });
                            });
                        });
                    }
                }]
            }
        });
})();