(function () {
    'use strict';

    angular
        .module('columnsFilteringBtn')
        .directive('columnsSelection', ColumnsSelectionDirectiveFunction);
    
    function ColumnsSelectionDirectiveFunction() {
        ColumnsSelectionDirectiveControllerFunction.$inject = ['$scope'];

        return {
            templateUrl: 'directives/columns-filtering-button/columns-filtering-btn.template.html',
            restrict: 'E',
            scope: {
                allColumnsChecked: '=?',
                tableColumns: '=',
            },
            controller: ColumnsSelectionDirectiveControllerFunction
        }

        function ColumnsSelectionDirectiveControllerFunction($scope) {
            var self = $scope;

            self.allColumnsChecked = true;

            self.selectAllColumns = function () {
                angular.forEach(self.tableColumns, function (col) {
                    col.show = self.allColumnsChecked;
                });
            }
        }
    }
})();