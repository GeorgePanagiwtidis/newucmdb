(function () {

    'use strict';

    angular
        .module('resultsBusinessModal')
        .directive('tableModal', function () {
            return {
                restrict: 'E',
                templateUrl: 'directives/results_business_modal/results-business-modal.template.html',
                scope: {
                    buttonLabel: '@',

                    canUpdated: '=',
                    editMode: '=',
                    originalItem: '=',
                    businessItem: '=',
                    ciType: '=',

                    onClose: '&',
                    onUpdate: '&'
                },
                controller: ['$scope', '$timeout', '$window', 'coreSearchService', 'updateService', 'lovsService', 'bootboxService', function ($scope, $timeout, $window, coreSearchService, updateService, lovsService, bootboxService) {
                    var self = $scope;

                    // Listener Functions
                    self.openEditTaskModal = openEditTaskModal;
                    self.printUcmdbInfo = printUcmdbInfo;
                    self.changeMode = changeMode;
                    self.showInputBasedOnConditions = showInputBasedOnConditions;
                    self.filterApplicationDomainLOVsBasedOnTenant = filterApplicationDomainLOVsBasedOnTenant;
                    self.saveChanges = saveChanges;
                    self.addComment = addComment;
                    self.manipulateMultiSelection = manipulateMultiSelection;

                    self.multiSelectSettings = {
                        showCheckAll: false,
                        showUncheckAll: false,
                        smartButtonMaxItems: 4
                    }

                    function tranformGroupAttributes(groups) {
                        angular.forEach(groups, function (group) {
                            var objectProperties = Object.keys(group.properties);

                            var properties = [];
                            angular.forEach(objectProperties, function (prop) {
                                properties.push(
                                    {
                                        name: prop,
                                        displayName: self.businessItem.attributeNames[prop] ? self.businessItem.attributeNames[prop] : _.startCase(prop),
                                        value: group.properties[prop]
                                    });
                            });

                            // Alphabetically Sort of Properties
                            properties.sort(function (prop1, prop2) {
                                return prop1.displayName - prop2.displayName;
                            });
                            group.properties = properties;
                        });
                    }

                    // Business Item Watcher in Order to Transform Object & Reset Page Defaults
                    self.$watch('businessItem', function () {
                        if (self.businessItem) {
                            if (!self.editMode) {
                                tranformGroupAttributes(self.businessItem.groupAttributes)
                            }
                            angular.element('html').scrollTop(0);
                            angular.element('#scrollable-body').scrollTop(0);
                        }
                    });

                    function openEditTaskModal() {
                        if (self.onClose) {
                            self.onClose();
                        }
                    }

                    function printUcmdbInfo() {
                        // Apply Styles to HTML Head Element
                        angular.element('head').append('<link id="print-link" rel="stylesheet" href="assets/css/print-styles.css" type="text/css" media="print"/>');

                        // Show Print Window
                        $timeout(function () {
                            $window.print();
                        }, 1000);

                        // Remove Styles from HTML Head Element
                        $timeout(function () {
                            angular.element('#print-link').remove();
                        }, 1000);
                    }

                    function changeMode() {
                        // Scroll Properties Div to Top
                        scrollPropertiesTop();

                        if (self.editMode) {
                            updateService.switchToViewMode(self);
                        }
                        else {
                            updateService.switchToEditMode(self, self.ciType);
                        }
                    }

                    function addComment(comment) {
                        var addCommentObject = constructAddCommentObject(comment);

                        self.waitingModal = true;
                        coreSearchService.addComment.query(addCommentObject, comment,
                            function (response) {
                                var updatedComment = response.payload;
                                updateViewObjectForComment(updatedComment);

                                self.waitingModal = false;
                            },
                            function (addCommentError) {
                                bootboxService.alert('Server Error', 'Comment could not be added!', false);

                                self.waitingModal = false;
                            });
                    }

                    function saveChanges() {
                        if (!validateValues()) return;

                        transformUserTypedProperties();
                        updateService.updateUcmdb(self.businessItem.globalId, self.originalItem.groupAttributes, self.businessItem.groupAttributes);
                        self.onUpdate
                        (
                            {
                                displayLabel: self.businessItem.name,
                                updatedAttributes: updateService.transformGroupsObjectToPropertiesObject(self.businessItem.groupAttributes)
                            }
                        );
                    }

                    // Helper Functions
                    function scrollPropertiesTop() {
                        var list = angular.element('#scrollable-body');
                        list.scrollTop(0);
                    }

                    function constructAddCommentObject() {
                        var displayLabel    = self.businessItem.decoupledCiProperties['display_label'];
                        var tenantOwner     = self.businessItem.decoupledCiProperties['TenantOwner'];
                        var globalId        = self.businessItem.decoupledCiProperties['global_id'];

                        return {
                            displayLabel: displayLabel,
                            tenantOwner: tenantOwner,
                            globalId: globalId
                        }
                    }

                    function updateViewObjectForComment(updatedComment) {
                        var userCommentsGroupOriginal = self.originalItem   ? _.find(self.originalItem.groupAttributes, {groupName: 'User Comments'}) : null;
                        var userCommentsGroup         = self.businessItem   ? _.find(self.businessItem.groupAttributes, {groupName: 'User Comments'}) : null;

                        if (userCommentsGroup) userCommentsGroup.cisComments.push(updatedComment);
                        if (userCommentsGroupOriginal) userCommentsGroupOriginal.cisComments.push(updatedComment);
                    }

                    function showInputBasedOnConditions(property) {
                        var valueIsIncludedInPropertyLov = property.lovs.indexOf(property.value) !== -1;
                        var valueIsEmpty = property.value.length === 0;

                        if (!(valueIsIncludedInPropertyLov || valueIsEmpty)) {
                            property.customValue = property.value;
                            property.value = 'Other...';
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    
                    function filterApplicationDomainLOVsBasedOnTenant(tenantOwner) {
                        // Restore Application Domain Value
                        var applicationDomainAtrr = _.find(_.find(self.businessItem.groupAttributes, {groupName: 'Application Attributes'}).properties, {ucmdbPropName: 'domain'});
                        applicationDomainAtrr.value = '';

                        // Filter Application Domain LOVs based on Tenant
                        lovsService.changeAttributeLovOnEvtByTypeAndTenant(applicationDomainAtrr, 'domain', self.ciType, tenantOwner);
                    }

                    function manipulateMultiSelection(attribute, selectFlag) {
                        return function (item) {
                            if (selectFlag) {
                                attribute.value = attribute.value ? attribute.value.concat(',' + item.label) : item.label;
                            }
                            else {
                                var values = attribute.value.split(',');
                                values = values.filter(function (val) {
                                    return val != item.label;
                                });

                                attribute.value = values.join();
                            }
                        }
                    }

                    function validateValues() {
                        var propertiesWithValidationError = [];

                        // Iterate over Groups
                        angular.forEach(self.businessItem.groupAttributes, function (group) {
                            // Iterate over Group Attributes
                            angular.forEach(group.properties, function (property) {
                                if ((!property.canBeNull || property.mandatory) && property.value.trim().length === 0) {
                                    property.validationError = mandatoryAttributeValueSetToNull(property);

                                    if (property.validationError) propertiesWithValidationError.push(property.displayName);
                                }
                                else {
                                    property.validationError = false;
                                }
                            });
                        });

                        if (propertiesWithValidationError.length !== 0) {
                            bootboxService.info('Validation Error!', 'You can\'t set Null value to a Mandatory Attribute.');
                            return false;
                        }
                        else {
                            return true;
                        }
                    }

                    function transformUserTypedProperties() {
                        // Iterate over Groups
                        angular.forEach(self.businessItem.groupAttributes, function (group) {
                            // Iterate over Group Attributes
                            angular.forEach(group.properties, function (property) {
                                if (property.value == 'Other...') {
                                    property.value = property.customValue ? property.customValue : '';
                                }
                            });
                        });
                    }
                    
                    function mandatoryAttributeValueSetToNull(attribute) {
                        var attributeValueBeforeChange = self.originalItem.decoupledCiProperties[attribute.ucmdbPropName];

                        return Boolean(attributeValueBeforeChange && attributeValueBeforeChange.length !== 0);
                    }
                }]
            }
        });

})();