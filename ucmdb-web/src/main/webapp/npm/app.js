// function requireAll(r) { r.keys().forEach(r); }
// requireAll(require.context('../components', true, /\.js$/));

// ---- Core Modules ----
require('../components/core/core.module');
require('../components/core/string-to-number.directive');
require('../components/core/auth-service/auth-service.module');
require('../components/core/auth-service/auth-service.service');
require('../components/core/bootbox-service/bootbox-service.module');
require('../components/core/bootbox-service/bootbox-service.service');
require('../components/core/components-data-flow/components-data-flow.module');
require('../components/core/components-data-flow/components-data-flow.service');
require('../components/core/log-service/log-service.module');
require('../components/core/log-service/log-service.service');
require('../components/core/search-service/search-service.module');
require('../components/core/search-service/search-service.service');
require('../components/core/update-from-search/update-from-search-module');
require('../components/core/update-from-search/update-from-search-factory');
require('../components/core/loader-service/loader-service.module');
require('../components/core/loader-service/loader-service.service');
require('../components/core/lovs-service/lovs.module');
require('../components/core/lovs-service/lovs.service');

// --- Angular Modules
require('../components/home_page/home-page.module');
require('../directives/download_excel_directive/download-excel.module');
require('../directives/audit_modal/audit-modal.module');
require('../directives/edit-task-modal/edit-task-modal.module');
require('../components/error_page/error-page.module');
require('../directives/header_directive/header-directive.module');
require('../directives/modal_info/modal.module');
require('../components/my_tasks/my-tasks.module');
require('../directives/node-reporting-modal/node-reporting-modal.module');
require('../components/reporting_page/reporting-page.module');
require('../components/reporting_user_actions/reporting-user-actions.module');
require('../directives/results_business_modal/results-business-modal.module');
require('../components/results_business_v3/results-business.module');
require('../components/results_page_infrastructure/results-page-infrastructure.module');
require('../components/results_view_queries/view-queries.module');
require('../components/saved_searches/saved-searches.module');
require('../components/search_page/search-page.module');
require('../directives/sticky_header/sticky-header.module');
require('../directives/tasks-reporting-modal/tasks-reporting-modal.module');
require('../components/user_management/user-management.module');
require('../components/user_management_create/user-management-create.module');
require('../directives/columns-filtering-button/columns-filtering-btn.module');
require('../directives/support-attributes-modal/support-modal.module');
require('../directives/comment_modal/comment-modal.module');
require('../components/comment_page/comment-page.module');
require('../components/missing_cis_page/missing-cis-page.module');
require('../directives/related-cis-modal/related-cis-modal.module');
require('../components/workflow_stats_page/workflow-stats-page.module');
require('../directives/infra-table-directive/infra-table-directive.module');

// -- Angular Components
require('../components/home_page/home-page.component');
require('../components/error_page/error-page.component');
require('../components/my_tasks/my-tasks.component');
require('../components/reporting_page/reporting-page.component');
require('../components/reporting_user_actions/reporting-user-actions.component');
require('../components/results_business_v3/results-business.component');
require('../components/results_page_infrastructure/results-page-infrastructure.component');
require('../components/results_view_queries/view-queries.component');
require('../components/saved_searches/saved-searches.component');
require('../components/search_page/search-page.component');
require('../components/user_management/user-management.component');
require('../components/user_management_create/user-management-create.component');
require('../components/comment_page/comment-page.component');
require('../components/missing_cis_page/missing-cis-page.component');
require('../components/workflow_stats_page/workflow-stats-page.component');

// -- Angular Directives
require('../directives/download_excel_directive/download-excel.directive');
require('../directives/audit_modal/audit-modal.directive');
require('../directives/audit_modal/scroll-sync.directive');
require('../directives/edit-task-modal/edit-task-modal.directive');
require('../directives/header_directive/header-directive.directive');
require('../directives/modal_info/modal.directive');
require('../directives/node-reporting-modal/node-reporting-modal.directive');
require('../directives/results_business_modal/results-business-modal.directive');
require('../directives/sticky_header/sticky-header.directive');
require('../directives/tasks-reporting-modal/tasks-reporting-modal.directive');
require('../components/user_management_create/user-management-create.directive');
require('../directives/columns-filtering-button/columns-filtering-btn.directive');
require('../directives/support-attributes-modal/support-modal.directive');
require('../directives/comment_modal/comment-modal.directive');
require('../directives/related-cis-modal/related-cis-modal.directive');
require('../directives/infra-table-directive/infra-table-directive.directive');

// -- Angular Filters
require('../components/my_tasks/my-tasks.filter');
require('../components/my_tasks/my-tasks-date.filter');
require('../components/reporting_user_actions/reporting-user-actions.filter');



