var webpack = require('webpack');
var path = require('path');
var WebpackGitHash = require('webpack-git-hash');
var htmlWebpackPlugin = require('html-webpack-plugin');
var HtmlPluginRemove =  require('html-webpack-plugin-remove');

module.exports = {
    context: __dirname,
    entry: {
        app: './app.js',
        // vendor: './vendor.js'
    },
    output: {
        path: path.resolve(__dirname, '../'),
        filename: 'ucmdb.bundle.[githash].js'
    },
    plugins: [
        new htmlWebpackPlugin ({
            inject: true,
            template: '../index.html'
        }),
        new HtmlPluginRemove(/<script.*?src="ucmdb.bundle\..*?\.js".*?<\/script>/),
        new WebpackGitHash({cleanup: true}),
        new webpack.optimize.UglifyJsPlugin({compress: {warnings: false}}),
    ]
};