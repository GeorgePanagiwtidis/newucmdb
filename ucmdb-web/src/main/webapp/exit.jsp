<script type="text/javascript">
    <%
    session.invalidate();
    %>

    var inactiveUser = window.location.hash.substr(1) == "inactive";

    if (inactiveUser) {
        parent.location.href="login.jsp#inactive";
    }
    else {
        parent.location.href="login.jsp";
    }

</script>
