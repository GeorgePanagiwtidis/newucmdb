(function () {
    'use strict';

    angular
        .module('UcmdbWebApp')

        // Http Interceptor for User Session Invalidation
        .factory('redirectInterceptor', function ($q, $window) {
            return {
                'response': function (response) {
                    if (typeof response.data === 'string' && response.data.indexOf("Login") > -1) {
                        $window.location.href = 'login.jsp';
                        return $q.reject(response);
                    } else {
                        return response;
                    }
                }
            }

        })
        .config(['$httpProvider', function ($httpProvider) {
            $httpProvider.interceptors.push('redirectInterceptor');
        }])

        .config(['$routeProvider', '$compileProvider', '$httpProvider', 'localStorageServiceProvider',
            'AclServiceProvider', 'IdleProvider', 'KeepaliveProvider', 'TitleProvider', '__env',
            function ($routeProvider, $compileProvider, $httpProvider, localStorageServiceProvider,
                      AclServiceProvider, IdleProvider, KeepaliveProvider, TitleProvider, __env) {
                //  Download Excel Configuration
                $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob):/);

                // Setup AclServiceProvider in order to load the ACL data from web storage earlier in the app bootstrap process
                AclServiceProvider.resume();

                // Setup Idle Provider
                IdleProvider.idle(7200); // 120 Minutes (2 HR) Idle Timeout
                IdleProvider.timeout(10); // in seconds
                KeepaliveProvider.interval(2); // in seconds

                //Set http Headers in order to Disable Cache
                var headers = {
                    'Cache-Control': 'no-cache',
                    'Expires': '0',
                    'Pragma': 'no-cache'
                };
                $httpProvider.defaults.headers.get = headers;

                // Prevent Title Change due to timeout
                TitleProvider.enabled(false);

                // Configuration for Local Storage
                localStorageServiceProvider
                    .setPrefix('ucmdbApp')
                    .setStorageType('sessionStorage')
                    .setNotify(true, true);

                $routeProvider
                    .when('/', {
                        template: '<home-page-component></home-page-component>'
                    })
                    .when('/search_page', {
                        template: '<search-page></search-page>',
                        resolve: {
                            'acl': ['$q', 'AclService', function ($q, AclService) {
                                if (AclService.can('view_content')) {
                                    // Has proper permissions
                                    return true;
                                } else {
                                    // Does not have permission
                                    return $q.reject('LoggedOff');
                                }
                            }]
                        }
                    })
                    .when('/results_page', {
                        template: '<results-business></results-business>',
                        resolve: {
                            'acl': ['$q', 'AclService', function ($q, AclService) {
                                if (AclService.can('view_content')) {
                                    // Has proper permissions
                                    return true;
                                } else {
                                    // Does not have permission
                                    return $q.reject('LoggedOff');
                                }
                            }]
                        }
                    })
                    .when('/results_infrastructure', {
                        template: '<results-page-infrastructure></results-page-infrastructure>',
                        resolve: {
                            'acl': ['$q', 'AclService', function ($q, AclService) {
                                if (AclService.can('view_content')) {
                                    // Has proper permissions
                                    return true;
                                } else {
                                    // Does not have permission
                                    return $q.reject('LoggedOff');
                                }
                            }]
                        }
                    })
                    .when('/view_queries', {
                        template: '<results-view-queries></results-view-queries>',
                        resolve: {
                            'acl': ['$q', 'AclService', function ($q, AclService) {
                                if (AclService.can('view_content')) {
                                    // Has proper permissions
                                    return true;
                                } else {
                                    // Does not have permission
                                    return $q.reject('LoggedOff');
                                }
                            }]
                        }
                    })
                    .when('/missing_cis', {
                        template: '<missing-cis-page></missing-cis-page>',
                        resolve: {
                            'acl': ['$q', 'AclService', function ($q, AclService) {
                                if (AclService.can('view_content') && (!__env.NEW_UCMDB || AclService.hasRole('admin'))) {
                                    // Has proper permissions
                                    return true;
                                } else if (AclService.can('view_content')) {
                                    return $q.reject('Unauthorized');
                                } else {
                                    // Does not have permission
                                    return $q.reject('LoggedOff');
                                }
                            }]
                        }
                    })
                    .when('/comment_page', {
                        template: '<comment-page></comment-page>',
                        resolve: {
                            'acl': ['$q', 'AclService', function ($q, AclService) {
                                if (AclService.can('view_content') && (!__env.NEW_UCMDB || AclService.hasRole('admin'))) {
                                    // Has proper permissions
                                    return true;
                                } else if (AclService.can('view_content')) {
                                    return $q.reject('Unauthorized');
                                } else {
                                    // Does not have permission
                                    return $q.reject('LoggedOff');
                                }
                            }]
                        }
                    })
                    .when('/saved_searches', {
                        template: '<saved-searches></saved-searches>',
                        resolve: {
                            'acl': ['$q', 'AclService', function ($q, AclService) {
                                if (AclService.can('view_content') && (!__env.NEW_UCMDB || AclService.hasRole('admin'))) {
                                    // Has proper permissions
                                    return true;
                                } else if (AclService.can('view_content')) {
                                    return $q.reject('Unauthorized');
                                } else {
                                    // Does not have permission
                                    return $q.reject('LoggedOff');
                                }
                            }]
                        }
                    })
                    .when('/reporting_page/:type', {
                        template: '<reporting-page></reporting-page>',
                        resolve: {
                            'acl': ['$q', 'AclService', function ($q, AclService) {
                                if (AclService.can('view_tasks') && (!__env.NEW_UCMDB || AclService.hasRole('admin'))) {
                                    // Has proper permissions
                                    return true;
                                } else if (AclService.can('view_content')) {
                                    // Does not have permission
                                    return $q.reject('Unauthorized');
                                }
                                else {
                                    return $q.reject('LoggedOff');
                                }
                            }]
                        }
                    })
                    .when('/user_actions', {
                        template: '<report-actions-component></report-actions-component>',
                        resolve: {
                            'acl': ['$q', 'AclService', function ($q, AclService) {
                                if (AclService.can('view_tasks') && (!__env.NEW_UCMDB || AclService.hasRole('admin'))) {
                                    // Has proper permissions
                                    return true;
                                } else if (AclService.can('view_content')) {
                                    // Does not have permission
                                    return $q.reject('Unauthorized');
                                }
                                else {
                                    return $q.reject('LoggedOff');
                                }
                            }]
                        }
                    })
                    .when('/workflow_stats', {
                        template: '<workflow-stats-page></workflow-stats-page>',
                        resolve: {
                            'acl': ['$q', 'AclService', function ($q, AclService) {
                                if (AclService.can('view_tasks') && (!__env.NEW_UCMDB || AclService.hasRole('admin'))) {
                                    // Has proper permissions
                                    return true;
                                } else if (AclService.can('view_content')) {
                                    // Does not have permission
                                    return $q.reject('Unauthorized');
                                }
                                else {
                                    return $q.reject('LoggedOff');
                                }
                            }]
                        }
                    })
                    .when('/my_tasks', {
                        template: '<my-tasks-component></my-tasks-component>',
                        resolve: {
                            'acl': ['$q', 'AclService', function ($q, AclService) {
                                if (AclService.can('view_tasks') && (!__env.NEW_UCMDB || AclService.hasRole('admin'))) {
                                    // Has proper permissions
                                    return true;
                                } else if (AclService.can('view_content')) {
                                    // Does not have permission
                                    return $q.reject('Unauthorized');
                                }
                                else {
                                    return $q.reject('LoggedOff');
                                }
                            }]
                        }
                    })
                    .when('/user_management/:tab', {
                        template: '<user-management></user-management>',
                        resolve: {
                            'acl': ['$q', 'AclService', function ($q, AclService) {
                                if (AclService.can('manage_content')) {
                                    // Has proper permissions
                                    return true;
                                } else {
                                    if (AclService.can('view_content')) {
                                        return $q.reject('Unauthorized');
                                    }
                                    else {
                                        return $q.reject('LoggedOff');
                                    }
                                }
                            }]
                        }
                    })
                    .when('/user_management_create', {
                        template: '<user-management-create></user-management-create>',
                        resolve: {
                            'acl': ['$q', 'AclService', function ($q, AclService) {
                                if (AclService.can('manage_content')) {
                                    // Has proper permissions
                                    return true;
                                } else {
                                    if (AclService.can('view_content')) {
                                        return $q.reject('Unauthorized');
                                    }
                                    else {
                                        return $q.reject('LoggedOff');
                                    }
                                }
                            }]
                        }
                    })
                    .when('/error_page', {
                        template: '<error-page></error-page>'
                    })
                    .otherwise('/error_page');
            }
        ])
        .run(['$rootScope', '$location', '$window', '__env',
                'log', 'localStorageService', 'AclService', 'AuthService', 'Idle',
                function ($rootScope, $location, $window, __env,
                          log, localStorageService, AclService, AuthService, Idle) {
                    logAppInfo(log);

                    $rootScope.pathOfApi = __env.PATH_OF_API;
                    $rootScope.waiting = false;

                    // Close Modal On Back Button
                    $rootScope.$on('$locationChangeStart', function (event, newUrl, oldUrl) {
                        // Select open modal(s)
                        var $openModalSelector = angular.element(".modal.fade.in");
                        if (($openModalSelector.data('bs.modal') || {}).isShown == true) {
                            // Close open modal(s)
                            $openModalSelector.modal("hide");
                            // Prevent page transition
                            event.preventDefault();
                        }
                    });

                    $rootScope.$on('$routeChangeError', function (event, current, previous, rejection) {
                        if (rejection === 'Unauthorized') {
                            // $window.location.href = 'login.jsp';
                            $location.path('/error_page');
                        }
                        else if (rejection === 'NonWorkflowUser') {
                            $location.path('/search_page');
                        }
                        else if (rejection === 'LoggedOff') {
                            // AuthService.exitApp();
                            $window.location.href = 'login.jsp';
                        }
                    });

                    AuthService.initUserInfo()
                        .then(function () {
                            performSessionTimeoutOperations($rootScope, $window, Idle, AclService, localStorageService);
                            performAuthenticationOperations(AclService);
                            performUserRelatedOperations(AuthService, localStorageService, AclService);
                        });
                }
            ]
        );

    function logAppInfo(log) {
        var angularVersion = angular.version.full;

        log.info('uCMDB Web App has been initialized');
        log.info('AngularJs version is: ' + angularVersion);
    }

    function constructPathOfApi(window) {
        var browserUrl = window.location.pathname;

        return browserUrl.substring(0, browserUrl.indexOf('/', 1)) + '/api';
    }

    function performSessionTimeoutOperations(rootScope, window, Idle, AclService, localStorageService) {
        // Session Timeouts - Start
        Idle.watch();

        // Periodically Check for Timeouts
        rootScope.$on('IdleTimeout', function () {
            AclService.flushRoles();
            localStorageService.clearAll();
            window.location.href = 'exit.jsp';
        });
    }

    function performAuthenticationOperations(AclService) {
        // Authentication
        var aclData = {
            guest: ['login'],
            member: ['logout', 'view_content'],
            workflowUser: ['logout', 'view_content', 'view_tasks'],
            admin: ['logout', 'view_content', 'manage_content', 'view_tasks']
        }
        AclService.setAbilities(aclData);
    }

    function performUserRelatedOperations(AuthService, localStorageService, AclService) {
        var isLoggedIn = localStorageService.get('isLoggedIn') ? localStorageService.get('isLoggedIn') : AuthService.isLoggedIn();
        var isAdmin = localStorageService.get('isAdmin') ? localStorageService.get('isAdmin') : AuthService.isAdmin();
        var isWorkflowUser = localStorageService.get('isWorkflowUser') ? localStorageService.get('isWorkflowUser') : AuthService.isWorkflowUser();
        var loggedUser = localStorageService.get('loggedUser') ? localStorageService.get('loggedUser') : AuthService.getUsername();

        // Give User Role
        giveUserRole(AclService, isLoggedIn, isAdmin, isWorkflowUser);
        storeValuesInBrowserStorage(localStorageService, isLoggedIn, isAdmin, isWorkflowUser, loggedUser);
    }

    function storeValuesInBrowserStorage(localStorageService, isLoggedIn, isAdmin, isWorkflowUser, loggedUser) {
        localStorageService.set('isLoggedIn', isLoggedIn);
        localStorageService.set('isAdmin', isAdmin);
        localStorageService.set('isWorkflowUser', isWorkflowUser);
        localStorageService.set('loggedUser', loggedUser);
    }

    function giveUserRole(AclService, isLoggedIn, isAdmin, isWorkflowUser) {
        if (isLoggedIn) {
            if (isAdmin) {
                AclService.attachRole('admin');
            }
            else if (isWorkflowUser) {
                AclService.attachRole('workflowUser');
            }
            else {
                AclService.attachRole('member');
            }
        }
        else {
            AclService.attachRole('guest');
        }
    }
})();