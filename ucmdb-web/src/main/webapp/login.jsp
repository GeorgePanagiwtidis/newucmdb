<%@ page import="gr.cosmote.ucmdb.commons.properties.ApplicationProperties" %>
<%@ page pageEncoding="UTF-8" %>
<%
    response.setHeader("Cache-Control","no-cache"); // HTTP1.1
    response.setHeader("Pragma","no-cache"); //HTTP 1.0
    response.setDateHeader ("Expires", 0); //prevents caching at the proxy server

    Boolean isNewUcmdb = ApplicationProperties.getInstance().getProperty("NEW_UCMDB").equalsIgnoreCase("true");
    String webAppUrl = ApplicationProperties.getInstance().getProperty("WEB_APP_HOME");
%>


<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

    <title>Cosmote - Login</title>

    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="bower_components/font-awesome-4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="bower_components/material-icons/material-icons.css"/>
    <link rel="stylesheet" href="assets/css/login-styles.css"/>

</head>

<body class="login-page">

<!-- Header -->
<header id="header">
    <div class="container-fluid">
        <div class="row">
        </div>
    </div>
</header>

<!-- Login -->
<section class="section section-login">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4">
                        <div class="row text-center">
                            <img src="assets/images/logo.png"/>
                        </div>
                    </div>
                    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                        <div class="row">
                            <hr/>
                        </div>
                    </div>
                    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                        <div class="text-center">
                            <div class="main-title login-title">
                                <h1 class="">uCMDB</h1>
                            </div>
                            <!-- Messages -->
                            <div class="welcome_msg">
                                <h1>Login</h1>
                                <p></p>
                            </div>
                            <div id="inactiveUserMsg" class="alert alert-danger" role="alert">
                                <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                                <span class="sr-only">Error:</span>
                                User is inactive! Please contact Administrator.
                            </div>
                            <div id="errorMsg" class="alert alert-danger" role="alert">
                                <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                                <span class="sr-only">Error:</span>
                                Wrong Username or Password
                            </div>

                            <!-- Form -->
                            <form action="<%=webAppUrl%>/api/auth" class="form-cosmote login-cosmote" method="post"
                                  novalidate>
                                <div class="form-group">
                                    <input type="text" class="form-control input-cosmote" id="j_username"
                                           name="j_username"
                                           placeholder="Username" onfocus="this.select()" required/>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control input-cosmote" name="j_password"
                                           id="j_password" placeholder="Password"
                                           onfocus="this.select()" required/>
                                </div>
                                <div class="form-group form-actions">
                                    <button type="submit" class="btn btn-default btn-cosmote full-width">LOGIN</button>
                                </div>
                            </form>
                            <!-- Info Div -->
                            <% if (!isNewUcmdb) { %>
                            <!-- New uCMDB Div -->
                            <div>
                                <div class="col-sm-1">
                                    <i class="material-icons login-material">info</i>
                                </div>
                                <div class="col-sm-11 login-message">
                                    Migration of Tenants in new uCMDB in Progress...<br>
                                    <a href="#" onclick="location.replace('/client_new')"> New link</a> for Tenants
                                    completed.
                                </div>
                            </div>
                            <% } else { %>
                            <!-- Old uCMDB Div -->
                            <div></div>
                            <% }%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>

<!-- Footer -->
<footer id="footer">
    <div class="container-fluid">
        <div class="row text-center">
            &copy; 2017 Cosmote. All Rights Reserved
        </div>
    </div>
</footer>

<script type="text/javascript">
    var error = window.location.hash.substr(1) == "error";
    var inactiveUser = window.location.hash.substr(1) == "inactive";

    if (inactiveUser) {
        document.getElementById('inactiveUserMsg').style.display = 'block';
    }
    else {
        document.getElementById('inactiveUserMsg').style.display = 'none';
    }

    if (error) {
        document.getElementById('errorMsg').style.display = "block";
    } else {
        document.getElementById('errorMsg').style.display = "none";
    }

    // Redirect if User is LoggedIn
    var isUserLoggedIn = sessionStorage.getItem('ucmdbApp.isLoggedIn');
    if (isUserLoggedIn === 'true') {
        location.replace('/client/index.html');
        sessionStorage.removeItem('ucmdbApp.isLoggedIn');
    }

</script>

</body>
</html>