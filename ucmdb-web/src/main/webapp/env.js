(
    function () {
        'use strict';


        // In order to Change Environment to NEW a change to user-management.template.html lines 279,280 should made
        var env = {
            NEW_UCMDB: false,
            OTHER_ENV_URL: 'http://' + window.location.host + '/client_new',

            PATH_OF_API: '/client/api',
            UAT_ENV: false
        };

        // Import variables if present (from env.js)
        if (window) {
            Object.assign(env, window.__env);
        }

        // Register environment in AngularJS as constant
        angular.module('UcmdbWebApp').constant('__env', env);
    }
)();