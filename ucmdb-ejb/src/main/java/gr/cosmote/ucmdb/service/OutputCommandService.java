package gr.cosmote.ucmdb.service;

import gr.cosmote.ucmdb.ejb.CommandDispatcherBean;
import gr.cosmote.ucmdb.ejb.NodeRemote;
import gr.cosmote.ucmdb.ejb.UpdateNodeRemote;
import gr.cosmote.ucmdb.models.database.Node;
import gr.cosmote.ucmdb.models.database.Task;
import gr.cosmote.ucmdb.models.database.TasksWrapper;
import gr.cosmote.ucmdb.models.enumeration.NodeStatusEnum;
import gr.cosmote.ucmdb.models.enumeration.OutputCommandEnum;
import gr.cosmote.ucmdb.models.rules.RulesEngineCommand;
import lombok.extern.slf4j.Slf4j;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@Slf4j
public class OutputCommandService {

    @EJB
    private NodeRemote nodeRemote;

    @EJB
    private CommandDispatcherBean commandDispatcherBean;

    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public Node updateNodeForCommands(Node node, List<RulesEngineCommand<OutputCommandEnum>> outCommand) {
        if(isNodeFlowCompleted(outCommand)) {
            node.setStatus(NodeStatusEnum.COMPLETED);
        }

        List<Task> outputCommandsTasks = collectTasksFromCommands(outCommand);
        TasksWrapper tw =  node.getTasksWrapper();
        tw.setTasks(outputCommandsTasks);
        try {
            node.setTasksWrapper(tw);
        }catch (IOException e) {
            log.error("OutputCommandsService.updateNodeForCommands() ::: Serialization exception of {}",tw);
        }
        nodeRemote.saveOrUpdateNodeAndTaskSummaryRecords(node);
        outCommand.forEach(c -> {
                c.getTask().setGlobalId(node.getGlobalId());
                commandDispatcherBean.dispatch(c);
            });

        return node;
    }

    private List<Task> collectTasksFromCommands(List<RulesEngineCommand<OutputCommandEnum>> outCommand) {
        return outCommand.stream()
                .map(RulesEngineCommand::getTask)
                .collect(Collectors.toList());
    }

    private boolean isNodeFlowCompleted(List<RulesEngineCommand<OutputCommandEnum>> outCommand) {
        List<Task> notClosedTasks = outCommand.stream()
                .filter(oc -> oc.getTask().isOpen())
                .map(RulesEngineCommand::getTask)
                .collect(Collectors.toList());

        return (notClosedTasks == null || notClosedTasks.size() == 0);
    }
}
