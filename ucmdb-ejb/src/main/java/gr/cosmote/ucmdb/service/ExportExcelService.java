package gr.cosmote.ucmdb.service;

import gr.cosmote.ucmdb.models.view.NodeReportingResultDTO;
import gr.cosmote.ucmdb.models.view.ProcessReportDTO;
import gr.cosmote.ucmdb.models.view.TaskReportDTO;
import gr.cosmote.ucmdb.models.view.TaskReportingResultDTO;
import gr.cosmote.ucmdb.models.view.TaskReportingResultsDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import javax.ejb.Stateless;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

@Stateless
@Slf4j
public class ExportExcelService {

    public byte[] exportNodeReport(List<NodeReportingResultDTO> nodeReportingResultDTOList) {
        log.debug("ExportExcelService.exportNodeReport() ::: Entering method with input {}", nodeReportingResultDTOList);

        Workbook wb = generateNodeReportWorkbook(nodeReportingResultDTOList);
        byte[] bytes = getWorkbookBytes(wb);

        log.debug("ExportExcelService.exportNodeReport() ::: Returning from method");
        return bytes;
    }

    public byte[] exportTaskReport(TaskReportingResultDTO taskReportingResultDTO) {
        log.debug("ExportExcelService.exportTaskReport() ::: Entering method with input {}", taskReportingResultDTO);

        Workbook wb = generateTaskReportWorkbook(taskReportingResultDTO);
        byte[] bytes = getWorkbookBytes(wb);

        log.debug("ExportExcelService.exportTaskReport() ::: Returning from method");
        return bytes;
    }

    private Workbook generateNodeReportWorkbook(List<NodeReportingResultDTO> nodeReportingResultDTOList) {
        Workbook wb = new HSSFWorkbook();  // XSSFWorkbook;

        nodeReportingResultDTOList.forEach(statusGroup -> {
            Sheet sheet = createSheetForNodeReportToWorkbook(statusGroup, wb);

            createHeaderRowForNodeReportToSheet(sheet);
            statusGroup.getProcessReportDTOList().forEach(pr -> {
                int currentRowNum = sheet.getLastRowNum() + 1;
                createRowForNodeReportToSheet(pr, (short)currentRowNum, sheet);
            });

            autoSizingSheetColumns(sheet);
        });
        return wb;
    }

    private Workbook generateTaskReportWorkbook(TaskReportingResultDTO taskReportingResultDTO) {
        Workbook wb = new HSSFWorkbook();

        taskReportingResultDTO.getTaskReportingResultsDTOList().forEach(reportResult -> {
            Sheet sheet = createSheetForTaskReportToWorkbook(reportResult, wb);

            createHeaderRowForTaskReportToSheet(sheet);
            reportResult.getTaskReportDTOS().forEach(tr -> {
                int currentRowNum = sheet.getLastRowNum() + 1;
                createRowForTaskReportToSheet(tr, (short)currentRowNum, sheet);
            });

            autoSizingSheetColumns(sheet);
        });

        return wb;
    }

    private byte[] getWorkbookBytes(Workbook wb) {
        byte[] bytes;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            wb.write(bos);

            bytes = bos.toByteArray();
        } catch (IOException ex) {
            throw new gr.cosmote.ucmdb.commons.exceptions.ApplicationException("ExportExcelService.getWorkbookBytes() ::: Error failed to export node report", ex);
        }
        return bytes;
    }

    private Sheet createSheetForNodeReportToWorkbook(NodeReportingResultDTO nodeReportingResultDTO, Workbook wb) {
        return wb.createSheet((nodeReportingResultDTO.getProcessStatus() != null)
                ? nodeReportingResultDTO.getProcessStatus().toString()
                : "");
    }

    private Sheet createSheetForTaskReportToWorkbook(TaskReportingResultsDTO taskReportingResultsDTO, Workbook wb) {
        return wb.createSheet((taskReportingResultsDTO.getGroupName()));
    }

    private void createHeaderRowForNodeReportToSheet(Sheet sheet) {
        Row row = sheet.createRow(0);

        row.createCell(0).setCellValue("NODE NAME");
        row.createCell(1).setCellValue("OS TYPE");
        // OS Installed Date
        row.createCell(2).setCellValue("OWNER TENANT");
        row.createCell(3).setCellValue("NODE STATUS");
        row.createCell(4).setCellValue("RETRIEVAL DATE");
        row.createCell(5).setCellValue("ASSIGNED GROUP");
        row.createCell(6).setCellValue("PROCESS ID");
        row.createCell(7).setCellValue("CI TYPE");
    }

    private void createHeaderRowForTaskReportToSheet(Sheet sheet) {
        Row row = sheet.createRow(0);

        row.createCell(0).setCellValue("NODE NAME");
        row.createCell(1).setCellValue("OS TYPE");
        // OS Installed Date?
        row.createCell(2).setCellValue("OWNER TENANT");
        row.createCell(3).setCellValue("TASK STATUS");
        row.createCell(4).setCellValue("ASSIGNED DATE");
        row.createCell(5).setCellValue("ASSIGNED GROUP");
        row.createCell(6).setCellValue("ASSIGNED ENGINEER");
        row.createCell(7).setCellValue("PROCESS ID");
    }

    private void createRowForNodeReportToSheet(ProcessReportDTO processReportDTO, short cellRowNum, Sheet sheet) {
        Row row = sheet.createRow(cellRowNum);

        row.createCell(0).setCellValue(processReportDTO.getDisplayLabel());
        row.createCell(1).setCellValue(processReportDTO.getOsType());
        // OS Installed Date
        row.createCell(2).setCellValue(processReportDTO.getOwnerTenant());
        row.createCell(3).setCellValue((processReportDTO.getStatus() !=null) ? processReportDTO.getStatus().toString() : "");

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        row.createCell(4).setCellValue( formatter.format(processReportDTO.getRetrievalDate()));

//        Cell dateCell = row.createCell(4);
//        dateCell.setCellValue(processReportDTO.getRetrievalDate());
//        dateCell.setCellStyle(dateCellStyle);

        row.createCell(5).setCellValue(processReportDTO.getAssignedGroup());
        row.createCell(6).setCellValue(processReportDTO.getProcessId());
        row.createCell(7).setCellValue(processReportDTO.getCiType());
    }

    private void createRowForTaskReportToSheet(TaskReportDTO taskReportDTO, short cellRowNum, Sheet sheet) {
        Row row = sheet.createRow(cellRowNum);

        row.createCell(0).setCellValue(taskReportDTO.getNodeName());
        row.createCell(1).setCellValue(taskReportDTO.getOsType());
        row.createCell(2).setCellValue(taskReportDTO.getOwnerTenant());
        row.createCell(3).setCellValue(taskReportDTO.getTaskStatus());
        row.createCell(4).setCellValue(taskReportDTO.getAssignedDate());
        row.createCell(5).setCellValue(taskReportDTO.getAssignedGroup());
        row.createCell(6).setCellValue(taskReportDTO.getAssignedEngineer());
        row.createCell(7).setCellValue(taskReportDTO.getProcessId());
    }

    private CellStyle getDateCellStyle(Sheet sheet) {
        Workbook wb = sheet.getWorkbook();

        if(wb == null)
            return null;

        CellStyle cellStyle = wb.createCellStyle();
        CreationHelper createHelper = wb.getCreationHelper();
        cellStyle.setDataFormat(
                createHelper.createDataFormat().getFormat("m/d/yy h:mm"));

        return cellStyle;
    }

    private void autoSizingSheetColumns(Sheet sheet) {
        int numOfCells = sheet.getRow(0).getPhysicalNumberOfCells();

        for (int column = 0; column < numOfCells; column++) {
            sheet.autoSizeColumn(column);
        }
    }
}
