package gr.cosmote.ucmdb.service;

import gr.cosmote.ucmdb.ejb.NodeRemote;
import gr.cosmote.ucmdb.ejb.RulesEngineServiceRemote;
import gr.cosmote.ucmdb.models.database.Node;
import gr.cosmote.ucmdb.models.database.Task;
import gr.cosmote.ucmdb.models.enumeration.InputCommandEnum;
import gr.cosmote.ucmdb.models.enumeration.OutputCommandEnum;
import gr.cosmote.ucmdb.models.rules.RulesEngineCommand;
import lombok.extern.slf4j.Slf4j;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@Slf4j
public class InputCommandService {

    @EJB
    private RulesEngineServiceRemote rulesEngineService;

    @EJB
    private OutputCommandService outputCommandService;

    @EJB
    private NodeRemote nodeRemote;

    @TransactionAttribute(value = TransactionAttributeType.NEVER)
    public void evaluateNode(Node node) {
        List<RulesEngineCommand<InputCommandEnum>> inCommandList = generateRuleEngineCommandsFor(node);
        List<RulesEngineCommand<OutputCommandEnum>> outCommand = rulesEngineService.evaluateRules(inCommandList);

        outputCommandService.updateNodeForCommands(node, outCommand);
    }

    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public void evaluateTask(Task task, InputCommandEnum inputCommandEnum) {
        Node node = nodeRemote.getNodeBy(task.getId());
        List<RulesEngineCommand<InputCommandEnum>> inCommandList = generateRuleEngineCommandsFor(node, task, Arrays.asList(inputCommandEnum));
        List<RulesEngineCommand<OutputCommandEnum>> outCommand = rulesEngineService.evaluateRules(inCommandList);

        outputCommandService.updateNodeForCommands(node, outCommand);
    }

    private List<RulesEngineCommand<InputCommandEnum>> generateRuleEngineCommandsFor(Node node, Task updatedTask, List<InputCommandEnum> commandsForTask) {
        List<RulesEngineCommand<InputCommandEnum>> inCommandList = new ArrayList<>();

        RulesEngineCommand<InputCommandEnum> updateCommand = createCommandList(commandsForTask, updatedTask);
        inCommandList.add(updateCommand);


        List<RulesEngineCommand<InputCommandEnum>> ignoredTaskList = node.getTasksWrapper().getTasks(node.getId()).stream()
                .filter(t -> t.getId() != updatedTask.getId())
                .map(t -> createCommandList(Arrays.asList(InputCommandEnum.IGNORE_TASK_COMMAND), t))
                .collect(Collectors.toList());

        inCommandList.addAll(ignoredTaskList);

        return inCommandList;
    }

    private RulesEngineCommand<InputCommandEnum> createCommandList(List<InputCommandEnum> inputCommandEnums, Task task) {
        RulesEngineCommand<InputCommandEnum> rulesEngineCommand = new RulesEngineCommand<>();
        rulesEngineCommand.setCommands(inputCommandEnums);
        rulesEngineCommand.setTask(task);

        return rulesEngineCommand;
    }

    private List<RulesEngineCommand<InputCommandEnum>> generateRuleEngineCommandsFor(Node node) {
        List<RulesEngineCommand<InputCommandEnum>> inputCommandsList = new ArrayList<>();

        RulesEngineCommand<InputCommandEnum> initiateCommand = new RulesEngineCommand<>();
        initiateCommand.setCommands(Arrays.asList(InputCommandEnum.INITIATE_TASK_COMMAND));

        if(isInitiateTaskAvailable(node)) {
            initiateCommand.setTask(node.getTasksWrapper().getTasks(node.getId()).get(0));
        } else {
            throw new gr.cosmote.ucmdb.commons.exceptions.ApplicationException(String.format("TaskEjb.generateRuleEngineCommandsFrom :: Error, could not find initiate task for unclassified node %s", node));
        }

        inputCommandsList.add(initiateCommand);

        return inputCommandsList;
    }

    private boolean isInitiateTaskAvailable(Node node) {
        return node != null && node.getTasksWrapper() != null && node.getTasksWrapper().getTasks(node.getId()) != null
                && node.getTasksWrapper().getTasks(node.getId()).size() == 1 && node.getTasksWrapper().getTasks(node.getId()).get(0) != null;
    }
}
