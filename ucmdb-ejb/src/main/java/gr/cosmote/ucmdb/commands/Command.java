package gr.cosmote.ucmdb.commands;

public interface Command {
    void execute();
}
