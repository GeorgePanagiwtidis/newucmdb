package gr.cosmote.ucmdb.commands;

import gr.cosmote.ucmdb.commons.exceptions.ApplicationException;
import gr.cosmote.ucmdb.commons.helper.MailHelper;
import gr.cosmote.ucmdb.commons.properties.ApplicationProperties;
import gr.cosmote.ucmdb.models.database.Group;
import gr.cosmote.ucmdb.models.database.Task;
import gr.cosmote.ucmdb.models.enumeration.TaskStatusReasonEnum;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.text.MessageFormat;
import java.util.*;

@Slf4j
@Data
public abstract class SendEmailCommand implements Command {
    private Task task;
    private static Map<Group, Map<String, String>> groupsWithNodesAssignedToThem = new HashMap<>();
    private boolean readyToSent = false;

    private static int counter = 0;

    // TODO -- Review Path
    //private InputStream HTML_TEMPLATE = MailHelper.class.getClassLoader().getResourceAsStream("resources/mail-html-template.html");
    //private InputStream LOGO_IMAGE = MailHelper.class.getClassLoader().getResourceAsStream("resources/assets/logo.png");

    public SendEmailCommand(Task task) {
        this.task = task;
    }

    public void executeMailCommand(Task task) {
        Group taskGroup = this.task.getUserGroup();

        if (task.getStatusReason() == TaskStatusReasonEnum.INVALID_GROUP_ASSIGNMENT) {
            /*Single reassignment*/
            try {
                String processId = this.task.getProcessId();
                String reassignmentText = ApplicationProperties.getInstance().getProperty("REASSIGNMENT_TEXT");
                reassignmentText = MessageFormat.format(reassignmentText, processId, taskGroup.getDescription());
                MailHelper.sendHTMLEmail("", reassignmentText, null, taskGroup.getEmail(), null);
            } catch (ApplicationException e) {
                log.error(String.format("SendEmailCommand.execute ::: Failed to send email to group %s for task %s ", task.getUserGroup().getEmail(), task), e);
            }
        } else if (task.getStatusReason() == TaskStatusReasonEnum.NEW_TASK_CREATED_BY_USER_ACTIONS) {
            try {
                String nodeName = String.format("%s (%s)", this.task.findCIAttributeValue("display_label"), this.task.getOsType());

                String scheduleJobText = ApplicationProperties.getInstance().getProperty("SCHEDULE_JOB_TEXT");
                scheduleJobText = MessageFormat.format(scheduleJobText, taskGroup.getDescription());
                MailHelper.sendHTMLEmail("", scheduleJobText, null, taskGroup.getEmail(), Arrays.asList(nodeName));
            } catch (ApplicationException e) {
                log.error(String.format("SendEmailCommand.execute ::: Failed to send email to group %s for task %s ", task.getUserGroup().getEmail(), task), e);
            }
        } else {
            addGroupAndNodes(task);
        }
    }

    private void addGroupAndNodes(Task task) {
        Group taskGroup = task.getUserGroup();

        if (groupsWithNodesAssignedToThem.get(taskGroup) != null) {
            groupsWithNodesAssignedToThem.get(taskGroup).put(task.findCIAttributeValue("display_label"), task.getOsType());
        } else {
            Map<String, String> nodeOsMapping = new HashMap<>();
            nodeOsMapping.put(task.findCIAttributeValue("display_label"), task.getOsType());

            groupsWithNodesAssignedToThem.put(taskGroup, nodeOsMapping);
        }
    }

    public static void massivelySendEmails() {
        String scheduleJobText = ApplicationProperties.getInstance().getProperty("SCHEDULE_JOB_TEXT");

        groupsWithNodesAssignedToThem
                .keySet()
                .stream()
                .forEach(group -> {
                    try {
                        String messageBody = MessageFormat.format(scheduleJobText, group.getDescription());

                        Map<String, String> nodesOsTypeMapping = groupsWithNodesAssignedToThem.get(group);

                        List<String> nodesWithOsType = new ArrayList<>();
                        nodesOsTypeMapping
                                .keySet()
                                .forEach(node -> nodesWithOsType.add(transformNodeOsTypeMapToSingeString(nodesOsTypeMapping, node)));

                        MailHelper.sendHTMLEmail("", messageBody, null, group.getEmail(), nodesWithOsType);
                    } catch (ApplicationException e) {
                        log.error("SendEmailCommand.massivelySendEmails ::: ERROR {} " + e);
                    }
                });
    }

    private static String transformNodeOsTypeMapToSingeString(Map nodesOsTypeMapping, String node) {
        if ("".equals(nodesOsTypeMapping.get(node))) {
            return node;
        } else {
            return node + " (" + nodesOsTypeMapping.get(node) + ")";
        }
    }

    public static Map<Group, Map<String, String>> getGroupsNodesMap() {
        return groupsWithNodesAssignedToThem;
    }

    public static void setGroupsNodesMap(Map<Group, Map<String, String>> groupsWithNodeNamesAssignedToThem) {
        SendEmailCommand.groupsWithNodesAssignedToThem = groupsWithNodeNamesAssignedToThem;
    }
}

