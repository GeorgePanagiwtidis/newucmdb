package gr.cosmote.ucmdb.commands;

import gr.cosmote.ucmdb.ejb.UpdateNodeRemote;
import gr.cosmote.ucmdb.models.database.Task;
import lombok.extern.slf4j.Slf4j;

import javax.ejb.EJB;

@Slf4j
public abstract class UpdateUcmdbCommand implements Command {


    private Task task;

    public UpdateUcmdbCommand(Task task) {
        this.task = task;
    }

    public abstract void execute();
}
