package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.commands.SendEmailCommand;
import gr.cosmote.ucmdb.commons.properties.ApplicationProperties;
import gr.cosmote.ucmdb.model.TopologyMapResponse;
import gr.cosmote.ucmdb.models.commons.SchedulerConfiguration;
import gr.cosmote.ucmdb.models.database.Group;
import gr.cosmote.ucmdb.models.database.Node;
import gr.cosmote.ucmdb.models.database.ScheduleJobConfiguration;
import gr.cosmote.ucmdb.service.InputCommandService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Startup
@Singleton
@Data
@Slf4j
public class ScheduleJobEjb {

    private static final String GET_SCHEDULE_JON_CONFIGURATION = "SELECT ScheduleConfiguration FROM ScheduleJobConfiguration ScheduleConfiguration";
    private static SchedulerConfiguration schedulerConfiguration = new SchedulerConfiguration();
    private static final boolean IS_NEW_UCMDB = ApplicationProperties.getInstance().getProperty("NEW_UCMDB").equalsIgnoreCase("true");

    @PersistenceContext
    private EntityManager em;

    @EJB
    private NodeRemote nodeRemote;
    @EJB
    private InputCommandService inputCommandService;
    @EJB
    private InfrastructureRemote infrastructureRemote;

    @Resource
    private TimerService timerService;
    private Timer infraTimer;
    private Timer infraTimerNew;
    private Timer timer;

    @PostConstruct
    @TransactionAttribute(value = TransactionAttributeType.NOT_SUPPORTED)
    public void init() {
        log.debug("ScheduleJobEjb :: init() :: Started Schedule Job on Initialization");

        removeAlreadyPersistedTimers();
        checkForSchedulerIntervalChanges();

        log.debug("ScheduleJobEjb :: init() :: Schedule Job on Initialization finished");
    }

    private void removeAlreadyPersistedTimers() {
        // Disable all Timers except from "databaseRead" Timer
        timerService.getAllTimers()
                .stream()
                .filter(timer -> !"databaseRead".equals(timer.getInfo()))
                .forEach(timer -> timer.cancel());
    }

    @Schedule(info = "databaseRead", minute = "*/5", hour = "*")
    @TransactionAttribute(value = TransactionAttributeType.NOT_SUPPORTED)
    private void checkForSchedulerIntervalChanges() {
        log.debug("ScheduleJobEjb :: checkForSchedulerIntervalChanges() :: Method Entered");
        setScheduleJobConfigurationFromDatabase();
        log.debug("ScheduleJobEjb :: checkForSchedulerIntervalChanges() :: Method Updated Schedule Job Configuration");
    }

    @Lock(LockType.READ)
    @TransactionAttribute(value = TransactionAttributeType.NOT_SUPPORTED)
    @Timeout
    public void runScheduleJob(Timer timer) {
        if (isUnclassifiedNodesTimeout(timer)) {
            log.debug("ScheduleJobEjb.runScheduleJob() ::: Schedule Job for Retrieving Unclassified Nodes started");

            List<Node> nodesToBeStored;
            TopologyMapResponse topologyMapResponse = nodeRemote.getTopologyMapResponse();

            if (isUnclassifiedNodesResponseChuncked(topologyMapResponse)) {
                boolean shouldPullSingleChunk = schedulerConfiguration.isOnlySingleChunk();
                int numberOfChunksAvailable = (shouldPullSingleChunk) ? 1 : topologyMapResponse.getChunkHolder().value.getNumberOfChunks();

                for (int i = 1; i <= numberOfChunksAvailable; i++) {
                    nodesToBeStored = nodeRemote.getUnclassifiedNodesChunk(topologyMapResponse, i);

                    nodesToBeStored.forEach(n -> inputCommandService.evaluateNode(n));
                }
            } else {
                nodesToBeStored = nodeRemote.getAllUnclassifiedNodes();

                nodesToBeStored.forEach(n -> inputCommandService.evaluateNode(n));
            }

            // Run After Scheduler Tasks (like Remove Duplicate Nodes, Send Emails etc)
            runAfterSchedulerTasks();

            log.debug("ScheduleJobEjb.runScheduleJob() ::: Schedule Job for Retrieving Unclassified Nodes finished");
        } else if (isInfrastructureExportTimer(timer)) {
            log.debug("ScheduleJobEjb.runScheduleJob() ::: Schedule Job for Exporting Infrastructure Computers started");

            infrastructureRemote.exportInfrastructureComputersToDatabase();

            log.debug("ScheduleJobEjb.runScheduleJob() ::: Schedule Job for Exporting Infrastructure Computers finished");
        }
    }

    private boolean isUnclassifiedNodesTimeout(Timer timer) {
        if (IS_NEW_UCMDB) return false;

        boolean unclassifiedNodesTimeout = false;
        if (timer.getInfo() != null) {
            String timerDescription = (String) timer.getInfo();

            if ("unclassifiedNodesRetrieval".equals(timerDescription)) {
                unclassifiedNodesTimeout = true;
            }
        }

        return unclassifiedNodesTimeout;
    }

    private boolean isInfrastructureExportTimer(Timer timer) {
        boolean infraExportTimeout = false;
        if (timer.getInfo() != null) {
            String timerDescription = (String) timer.getInfo();

            if ("infraExport".equals(timerDescription)) {
                infraExportTimeout = true;
            }
        }

        return infraExportTimeout;
    }

    private boolean isUnclassifiedNodesResponseChuncked(TopologyMapResponse topologyMapResponse) {
        return !(topologyMapResponse.getHolder().value.getCINodes().getCINode().size() > 0) && topologyMapResponse.getChunkHolder().value.getNumberOfChunks() > 0;
    }

    private void runAfterSchedulerTasks() {
        log.debug("ScheduleJobEjb :: runAfterSchedulerTasks :: Method Entered");

        try {
            em.createStoredProcedureQuery(schedulerConfiguration.getProcedureName()).execute();
        } catch (Exception storedProcedureException) {
            log.error("ScheduleJobEjb :: runAfterSchedulerTasks :: ERROR on Procedure Execution", storedProcedureException);
            return;
        }

        filterEmailNodeNamesBasedOnProcedureExecution();

        log.debug("ScheduleJobEjb :: runAfterSchedulerTasks :: Method Finished");
    }

    private void filterEmailNodeNamesBasedOnProcedureExecution() {
        Map<Group, Map<String, String>> prefilteredGroupNodes = SendEmailCommand.getGroupsNodesMap();
        Map<Group, Map<String, String>> filteredGroupNodes = new HashMap<>();

        // Get Today Retrieved Nodes
        List<Node> todaysNodes = nodeRemote.getTodayRetrievedNodes();

        prefilteredGroupNodes
                .forEach((group, nodes) -> {
                    Map<String, String> filteredNodes =
                            nodes.keySet()
                                    .stream()
                                    .filter(node -> todaysNodes.stream().anyMatch(retrievedNode -> node.equals(retrievedNode.getDisplayLabel())))
                                    .collect(Collectors.toMap(node -> node, node -> nodes.get(node) != null ? nodes.get(node) : ""));

                    if (!filteredNodes.isEmpty()) {
                        filteredGroupNodes.put(group, filteredNodes);
                    }
                });

        SendEmailCommand.setGroupsNodesMap(filteredGroupNodes);
        SendEmailCommand.massivelySendEmails();
        SendEmailCommand.getGroupsNodesMap().clear();
    }

    private void setScheduleJobConfigurationFromDatabase() {
        log.debug("ScheduleJobEjb :: setScheduleJobConfigurationFromDatabase :: Method Entered");

        List<ScheduleJobConfiguration> configurationFromDb = em.createQuery(GET_SCHEDULE_JON_CONFIGURATION, ScheduleJobConfiguration.class).getResultList();

        Map<String, String> transformedConfiguration = transformScheduleJobConfigurationToMap(configurationFromDb);

        boolean changesDetected = createOrUpdateScheduleJobConfiguration(transformedConfiguration);
        configureTimerBasedOnChanges(changesDetected);
        setUnclassifiedNodeScheduleProperties();

        log.debug("ScheduleJobEjb :: setScheduleJobConfigurationFromDatabase :: Method set Schedule Job Configuration with number of Properties {}", transformedConfiguration.keySet().size());
    }

    private void configureTimerBasedOnChanges(boolean changesDetected) {
        log.debug("ScheduleJobEjb :: configureTimerBasedOnChanges() :: Method Entered");

        if (changesDetected) {
            setInfrastructureExportTimer();
            setScheduleTimer();
        } else {
            log.debug("ScheduleJobEjb :: configureTimerBasedOnChanges() :: No Changes Detected");
        }
    }

    private void setUnclassifiedNodeScheduleProperties() {
        log.debug("ScheduleJobEjb :: getScheduleJobConfigurationFromDatabase :: Method Entered");

        nodeRemote.setUnclassifiedNodesViewName(schedulerConfiguration.getQueryName());
        nodeRemote.setUnclassifiedNodesDaysRestriction(String.valueOf(schedulerConfiguration.getRestrictionDays()));

        log.debug("ScheduleJobEjb :: getScheduleJobConfigurationFromDatabase :: Method set Unclassified Nodes Query name {}", schedulerConfiguration.getQueryName());
        log.debug("ScheduleJobEjb :: getScheduleJobConfigurationFromDatabase :: Method set Node Day Restriction {}", schedulerConfiguration.getRestrictionDays());
    }

    private void setInfrastructureExportTimer() {
        log.debug("ScheduleJobEjb :: setInfrastructureExportTimer() :: Method Entered");
        cancelTimer(this.infraTimer);
        cancelTimer(this.infraTimerNew);

        if (schedulerConfiguration.isInfraExportEnabled()) {
            if (IS_NEW_UCMDB) {
                createInfraTimerForNewUcmdb();
            } else {
                createInfraTimer();
            }
        } else {
            log.debug("ScheduleJobEjb :: setInfrastructureExportTimer() :: Scheduler is Disabled from DB");
        }
    }

    private void createInfraTimer() {
        ScheduleExpression scheduleExpression = new ScheduleExpression();
        // Give Timer a name in order to Manipulate in later in @Timeout Method
        TimerConfig timerConfig = new TimerConfig("infraExport", true);

        String hourExpression = schedulerConfiguration.getInfraExportHourInterval();
        String minuteExpression = schedulerConfiguration.getInfraExportMinInterval();

        scheduleExpression.hour(hourExpression);
        scheduleExpression.minute(minuteExpression);

        this.infraTimer = timerService.createCalendarTimer(scheduleExpression, timerConfig);
        log.debug("ScheduleJobEjb.createInfraTimer() :: Method Created New Timer");
    }

    private void createInfraTimerForNewUcmdb() {
        ScheduleExpression scheduleExpression = new ScheduleExpression();
        // Give Timer a name in order to Manipulate in later in @Timeout Method
        TimerConfig timerConfig = new TimerConfig("infraExport", true);

        String hourExpression = schedulerConfiguration.getInfraExportHourInterval();
        String minuteExpression = schedulerConfiguration.getInfraExportNewMinInterval();

        scheduleExpression.hour(hourExpression);
        scheduleExpression.minute(minuteExpression);

        this.infraTimerNew = timerService.createCalendarTimer(scheduleExpression, timerConfig);
        log.debug("ScheduleJobEjb.createInfraTimerForNewUcmdb() :: Method Created New Timer");
    }

    private void setScheduleTimer() {
        log.debug("ScheduleJobEjb :: setScheduleTimer() :: Method Entered");
        cancelTimer(this.timer);

        if (schedulerConfiguration.isEnabled()) {
            ScheduleExpression scheduleExpression = new ScheduleExpression();
            // Give Timer a name in order to Manipulate in later in @Timeout Method
            TimerConfig timerConfig = new TimerConfig("unclassifiedNodesRetrieval", true);

            if (!schedulerConfiguration.isMidnightScheduler()) {
                String hourExpression = schedulerConfiguration.getHourInterval() == 0 ? "*" : "*/" + schedulerConfiguration.getHourInterval();
                String minuteExpression = "*/" + schedulerConfiguration.getMinuteInterval();

                scheduleExpression.hour(hourExpression);
                scheduleExpression.minute(minuteExpression);
            }

            this.timer = timerService.createCalendarTimer(scheduleExpression, timerConfig);
            log.debug("ScheduleJobEjb :: setScheduleTimer() :: Method Created New Timer");
        } else {
            log.debug("ScheduleJobEjb :: setScheduleTimer() :: Scheduler is Disabled from DB");
        }
    }


    // Helper Methods
    private Map<String, String> transformScheduleJobConfigurationToMap(List<ScheduleJobConfiguration> configurationFromDb) {
        log.debug("ScheduleJobEjb :: transformScheduleJobConfigurationToMap :: Method Entered with configuration list {}", configurationFromDb);
        Map<String, String> configuration = new HashMap<>();

        configurationFromDb.stream()
                .forEach((conf) -> configuration.put(conf.getPropertyName(), conf.getPropertyValue()));

        log.debug("ScheduleJobEjb :: transformScheduleJobConfigurationToMap :: Method Returned Transformed Configuration Object {}", configuration);
        return configuration;
    }

    private boolean createOrUpdateScheduleJobConfiguration(Map<String, String> updatedConfiguration) {
        SchedulerConfiguration updatedSchedulerConfiguration = new SchedulerConfiguration(updatedConfiguration);

        boolean changesDetected = !schedulerConfiguration.equals(updatedSchedulerConfiguration);

        schedulerConfiguration = changesDetected ? updatedSchedulerConfiguration : schedulerConfiguration;

        return changesDetected;
    }

    private void cancelTimer(Timer timer) {
        try {
            timer.cancel();
        } catch (Exception ex) {
            log.debug("ScheduleJobEjb :: cancelTimer :: There is no timer to cancel");
        }
    }
}
