package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.models.base.NameCodeObject;
import gr.cosmote.ucmdb.models.database.Group;
import gr.cosmote.ucmdb.models.database.Tenants;
import gr.cosmote.ucmdb.models.view.User;

import javax.ejb.Remote;
import java.io.Serializable;
import java.util.List;

@Remote
public interface GroupRemote extends Serializable {
    List<Group> getAllGroups();
    List<Group> getUserGroups();
    List<Tenants> getUserTenants();
    List<Tenants> getTenantsWithGroupsAndUsers();
    List<User> getAllUsers();

    Group ucmdbInsertGroup(Group group);
    Group updateGroup(Group group);
    Group getGroupByID(String groupID);
    Group makeGroupInactive (NameCodeObject nameCode);
}
