package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.models.database.CiAttributeConfiguration;

import javax.ejb.Remote;
import java.io.Serializable;
import java.util.List;

@Remote
public interface CiAttributeConfigurationRemote extends Serializable {
    List<CiAttributeConfiguration> getCiAttributesConfiguration(String ciType);
    List<CiAttributeConfiguration> getCiAttributesConfiguration(String ciType, List<String> attributeNames);
    List<CiAttributeConfiguration> getCiAttributesConfigurationByLabels(String ciType, List<String> attributeLabels);
    CiAttributeConfiguration getCiAttributeConfiguration(String ciType, String attributeName);

    void updateCiAttributes(String className);
}
