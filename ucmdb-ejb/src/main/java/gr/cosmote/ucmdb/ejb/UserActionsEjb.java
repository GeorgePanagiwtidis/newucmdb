package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.commons.exceptions.ApplicationException;
import gr.cosmote.ucmdb.models.database.UcmdbCI;
import gr.cosmote.ucmdb.models.database.UcmdbCisComment;
import gr.cosmote.ucmdb.models.database.UcmdbMissingNode;
import gr.cosmote.ucmdb.models.view.UserCommentDTO;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@Slf4j
public class UserActionsEjb implements UserActionsRemote {

    @PersistenceContext
    private EntityManager entityManager;

    @Resource
    private SessionContext sessionContext;

    public UcmdbCisComment addNewCiComment(String ciDiplayLabel, String tenantOwner, String globalId, String newComment) throws ApplicationException {
        log.debug("UserActionsEjb.addNewCiComment :: Method Entered with Comment {}", newComment);

        UcmdbCisComment comment = null;
        try {
            UcmdbCI ci = entityManager
                    .createNamedQuery("UcmdbCIs.getCiBasedOnTenantOwnerDispLabelAndGlobId", UcmdbCI.class)
                    .setParameter("dispLabel", ciDiplayLabel)
                    .setParameter("tenantOwner", tenantOwner)
                    .setParameter("globalId", globalId)
                    .getSingleResult();

            comment = new UcmdbCisComment();
            comment.setUcmdbId(ci.getId());
            comment.setCommentText(newComment);
            comment.setCommentAuthor(sessionContext.getCallerPrincipal().getName());
            comment.setCommentDate(new Date());

            ci.getCiComments().add(comment);
            entityManager.merge(ci);
        } catch (Exception ex) {
            throw new ApplicationException("UserActionsEjb.addNewCiComment :: Error", ex);
        }

        return comment;
    }

    public List<UserCommentDTO> getUserComments() {
        log.debug("UserActionsEjb.getUserComments :: Method Entered");
        List<UserCommentDTO> userComments;

        userComments = entityManager
                .createNamedQuery("UcmdbCIs.getAllUcmdbCIs", UcmdbCI.class)
                .getResultList()
                .stream()
                .map(ci -> {
                    List<UserCommentDTO> ciComments = new ArrayList<>();

                    ci.getCiComments().stream().forEach(com -> {
                        UserCommentDTO comment = new UserCommentDTO();
                        comment.setGlobalId(ci.getGlobalId());
                        comment.setDisplayLabel(ci.getDisplayLabel());
                        comment.setCiType(ci.getCiType());
                        comment.setTenantOwner(ci.getTenantOwner());
                        comment.setCategory(ci.getCategory());
                        comment.setEnvironment(ci.getEnvironment());
                        comment.setRole(ci.getRole());
                        comment.setDomain(ci.getApplicationDomain());

                        comment.setAuthor(com.getCommentAuthor());
                        comment.setDate(com.getCommentDate());
                        comment.setComment(com.getCommentText());

                        ciComments.add(comment);
                    });

                    return ciComments;
                })
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        log.debug("UserActionsEjb.getUserComments :: Method Returned");
        return userComments;
    }

    public UcmdbMissingNode addMissingNode(UcmdbMissingNode missingNode) {
        log.debug("UserActionsEjb.addMissingNode :: Method Entered");

        missingNode.setInsertedDate(new Date());
        missingNode.setInsertedBy(sessionContext.getCallerPrincipal().getName());

        try {
            entityManager.persist(missingNode);
            entityManager.flush();
        }
        catch (Exception ex) {
            throw new ApplicationException("UserActionsEjb.addMissingNode :: ERROR", ex);
        }

        log.debug("UserActionsEjb.addMissingNode :: Method Returned Added Missing Node {}", missingNode);
        return missingNode;
    }

    public List<UcmdbMissingNode> getUcmdbMissingNodes() {
        log.debug("UserActionsEjb.getUcmdbMissingNodes :: Method Entered");
        List<UcmdbMissingNode> ucmdbMissingNodes;

        ucmdbMissingNodes = entityManager
                .createNamedQuery("UcmdbMissingNode.getUcmdbMissingNodes", UcmdbMissingNode.class)
                .getResultList();

        log.debug("UserActionsEjb.getUcmdbMissingNodes :: Method Returned");
        return ucmdbMissingNodes;
    }
}
