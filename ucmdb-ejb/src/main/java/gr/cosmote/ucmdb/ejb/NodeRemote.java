package gr.cosmote.ucmdb.ejb;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import gr.cosmote.ucmdb.model.TopologyMapResponse;
import gr.cosmote.ucmdb.models.database.CiAttributeConfiguration;
import gr.cosmote.ucmdb.models.database.Node;
import gr.cosmote.ucmdb.models.database.NodeDifferences;
import gr.cosmote.ucmdb.models.enumeration.NodeStatusEnum;
import gr.cosmote.ucmdb.models.view.TaskAttributeDTO;
import gr.cosmote.ucmdb.models.view.TaskEditDTO;
import gr.cosmote.ucmdb.models.view.WorkflowStatsDTO;

@Remote
public interface NodeRemote extends Serializable {
    List<Node> getAllUnclassifiedNodes();
    List<Node> getUnclassifiedNodesChunk(TopologyMapResponse topologyMapResponse, int chunkNumber);
    TopologyMapResponse getTopologyMapResponse();
    Node saveOrUpdateNodeAndTaskSummaryRecords(Node node);
    Node saveOrUpdateNode(Node node);
    List<Node> getNodeBy(List<NodeStatusEnum> status);
    Node getNodeBy(long taskId);
    List<Node> getNodesBy(List<String> tenants, List<NodeStatusEnum> status, Date dateFrom, Date dateTo);
    long getNumberOfAllNodes();
    Node getNode(long nodeId);
    List<CiAttributeConfiguration> getNodeAttributes();
    void insertUnclassifiedNodes(List<Node> nodes);
    void insertUnclassifiedNode(Node node);

    List<Node> getTodayRetrievedNodes();
    List<NodeDifferences> getAllNodeActions();
    NodeDifferences getNodeDifferencesById(String nodeDiffId);
    NodeDifferences getNodeDifferencesByTaskId(String taskId);
    void createNodeAuditingReport(TaskEditDTO taskEdit, String userGroup, List<TaskAttributeDTO> beforeUpdateAttributes, List<TaskAttributeDTO> afterUpdateAttributes);
    void setUnclassifiedNodesViewName(String viewName);
    void setUnclassifiedNodesDaysRestriction(String days);

    List<WorkflowStatsDTO> getWorkflowStats();
}
