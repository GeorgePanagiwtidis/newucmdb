package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.client.UCMDBCallHelper;
import gr.cosmote.ucmdb.models.base.CIAttribute;
import gr.cosmote.ucmdb.models.database.CiAttributeConfiguration;
import gr.cosmote.ucmdb.models.database.Task;
import gr.cosmote.ucmdb.models.view.TaskAttributeDTO;
import gr.cosmote.ucmdb.models.view.TaskEditDTO;
import gr.cosmote.ucmdb.models.view.TaskGroupDTO;
import gr.cosmote.ucmdb.models.view.cidetails.CiDetails;
import gr.cosmote.ucmdb.ucmdbWS.*;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Remote(UpdateNodeRemote.class)
@Stateless
@Slf4j
public class UpdateNodeEjb {

    @EJB
    private CIRemote ciRemote;
    @EJB
    private CiAttributeConfigurationRemote ciAttribute;
    @EJB
    private InfrastructureRemote infrastructureEjb;

    @Resource
    private SessionContext sessionContext;

    @PersistenceContext
    private EntityManager em;

    //TODO: Refactor when we find time
    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public boolean updateUcmdb(String globalId, List<CIAttribute> attributes) throws gr.cosmote.ucmdb.commons.exceptions.ApplicationException {
        String nodeId = globalId;
        boolean isUpdateSuccessful;

        if (nodeId == null) {
            throw new gr.cosmote.ucmdb.commons.exceptions.ApplicationException(String.format("UpdateNodeEjb.updateUcmdb() ::: Error failed to update uCMDB. Global id of node wasn't found %s", globalId));
        }

        log.debug(" UpdateEjb.updateUcmdb entered method with NodeId {} ", nodeId);
        try {
            String loggedUser = sessionContext.getCallerPrincipal().getName();
            String type = getParentClassName(globalId);

            if (type == null) {
                return false;
            }

            Map<String, String> attributesForUcmdb = transformCiAttributeToUcmdbForm(attributes, getCiAttributesFromDb(type));

            // TODO -- Check if we should implement Backend Validation for Empty Values
//            boolean foundValidationError = validateTransformedAttributes(attributesForUcmdb);
//            if (foundValidationError) {
//                return false;
//            }

            CIsAndRelationsUpdates updateObject = generateUpdateObject(globalId, type, attributesForUcmdb);

            isUpdateSuccessful = UCMDBCallHelper.updateCIsAndRelations(loggedUser, updateObject);
            infrastructureEjb.clearCache();
        } catch (Exception e) {
            throw new gr.cosmote.ucmdb.commons.exceptions.ApplicationException(String.format("UpdateNodeEjb.updateUcmdb() ::: Error failed to update uCMDB for CI with Global Id %s", globalId), e);
        }

        log.debug(" UpdateEjb.updateUcmdb Method returned with successful update {}");

        return isUpdateSuccessful;
    }

    private String getParentClassName(String globalId) {
        String parentClass;
        try {
            CiDetails ciDetails = ciRemote.getAttributesByID(globalId, null);
            parentClass = ciRemote.getCiParentClass(ciDetails.getType());
        }
        catch (Exception ex) {
            parentClass = null;
        }

        return parentClass;
    }

    private CIsAndRelationsUpdates generateUpdateObject(String globalId, String type, Map<String, String> attributesForUcmdb) {
        CIsAndRelationsUpdates updateObject = new CIsAndRelationsUpdates();
        CIs cis = new CIs();
        List<CI> listCI = cis.getCI();

        CI ci = new CI();
        ID id = new ID();
        id.setValue(globalId);
        id.setTemp(false);
        ci.setID(id);
        ci.setType(type);
        ci.setProps(generateUcmdbProperties(attributesForUcmdb, type));
        listCI.add(ci);
        updateObject.setCIsForUpdate(cis);


        return updateObject;
    }

    private CIProperties generateUcmdbProperties(Map<String, String> attributesForUcmdb, String type) {
        List<String> attributesNames = new ArrayList<>(attributesForUcmdb.keySet());
        List<CiAttributeConfiguration> ciAttributes = ciAttribute.getCiAttributesConfiguration(type, attributesNames);

        ObjectFactory objectFactory = new ObjectFactory();

        CIProperties props = new CIProperties();
        IntProps intProps = new IntProps();
        StrProps strProps = new StrProps();
        DateProps dateProps = new DateProps();
        StrListProps strListProps = new StrListProps();
        BooleanProps booleanProps = new BooleanProps();

        //Create uCMDB type properties
        //TODO: Implementation for STR_LIST
        for (CiAttributeConfiguration attribute : ciAttributes) {
            String attributeName = attribute.getAttributeName();
            if (attributesForUcmdb.get(attributeName) != null) {
                String attributeType = attribute.getType().getText();
                if (attributeType.equals("TEXT") || attributeType.equals("TEXT_LIST")) {
                    // TODO -- Check this
                    if (attributeName.equals("dr_ranking")) {
                        intProps.getIntProp().add(generateIntProps(attribute, attributesForUcmdb, attributeName, objectFactory));
                        continue;
                    }
                    strProps.getStrProp().add(generateStringProps(attribute, attributesForUcmdb, attributeName, objectFactory));
                } else if (attributeType.equals("LIST")) {
                    strListProps.getStrListProp().add(generateStringListProps(attribute, attributesForUcmdb, attributeName, objectFactory));
                } else if (attributeType.equals("LIST_VALUES")) {
                    strListProps.getStrListProp().add(generateStringListProps(attribute, attributesForUcmdb, attributeName, objectFactory));
                } else if (attributeType.equals("BOOLEAN")) {
                    booleanProps.getBooleanProp().add(generateBooleanProps(attribute, attributesForUcmdb, attributeName, objectFactory));
                } else if (attributeType.equals("NUMBER")) {
                    intProps.getIntProp().add(generateIntProps(attribute, attributesForUcmdb, attributeName, objectFactory));
                } else if (attributeType.equals("DATE")) {
                    //TODO: Add implementation for date
                }
            }
        }

        //Add uCMDB type properties to ucmdb properties
        if (!dateProps.getDateProp().isEmpty()) {
            props.setDateProps(dateProps);
        }
        if (!strProps.getStrProp().isEmpty()) {
            props.setStrProps(strProps);
        }
        if (!strListProps.getStrListProp().isEmpty()) {
            props.setStrListProps(strListProps);
        }
        if (!intProps.getIntProp().isEmpty()) {
            props.setIntProps(intProps);
        }
        if (!booleanProps.getBooleanProp().isEmpty()) {
            props.setBooleanProps(booleanProps);
        }

        return props;
    }

    private IntProp generateIntProps(CiAttributeConfiguration attribute, Map<String, String> attributesForUcmdb, String attributeLabel, ObjectFactory objectFactory) {
        IntProp intProp = new IntProp();
        intProp.setName(attribute.getAttributeName());

        BigInteger propValue = null;
        try {
            propValue = (BigInteger.valueOf(Long.valueOf(attributesForUcmdb.get(attributeLabel))));
        } catch (NumberFormatException ex) {
            log.error(String.format("UpdateEjb.updateUcmdb() ::: Could not create int property for attribute Label", attributeLabel));
        }

        intProp.setValue(objectFactory.createIntPropValue(propValue));

        return intProp;
    }

    private BooleanProp generateBooleanProps(CiAttributeConfiguration attribute, Map<String, String> attributesForUcmdb, String attributeLabel, ObjectFactory objectFactory) {
        BooleanProp booleanProp = new BooleanProp();
        booleanProp.setName(attribute.getAttributeName());
        booleanProp.setValue(objectFactory.createBooleanPropValue((Boolean.valueOf(attributesForUcmdb.get(attributeLabel)))));

        return booleanProp;
    }

    private StrProp generateStringProps(CiAttributeConfiguration attribute, Map<String, String> attributesForUcmdb, String attributeLabel, ObjectFactory objectFactory) {
        StrProp strProp = new StrProp();
        strProp.setName(attribute.getAttributeName());
        strProp.setValue(objectFactory.createStrPropValue(attributesForUcmdb.get(attributeLabel).trim()));

        return strProp;
    }

    private StrListProp generateStringListProps(CiAttributeConfiguration attribute, Map<String, String> attributesForUcmdb, String attributeName, ObjectFactory objectFactory) {
        StrListProp strListProp1 = new StrListProp();
        strListProp1.setName(attribute.getAttributeName());

        String attributeValue = attributesForUcmdb.get(attributeName);
        List<String> attributeValueList = Arrays.asList(attributeValue.split(","));

        StrList strList = objectFactory.createStrList();
        attributeValueList.forEach(avl -> strList.getStrValue().add(avl != null ? avl.trim() : null));

        strListProp1.setStrValues(objectFactory.createStrListPropStrValues(strList));

        return strListProp1;
    }

    private List<CiAttributeConfiguration> getCiAttributesFromDb (String ciType) {
        log.debug("UpdateNodeEjb.getCiAttributesFromDb :: Method Entered for CI Type {}", ciType);
        List<CiAttributeConfiguration> ciAttributes;

        ciAttributes = em
                .createNamedQuery("ci_attributes.getAttributesByCiType", CiAttributeConfiguration.class)
                .setParameter("ciType", ciType)
                .getResultList();

        log.debug("UpdateNodeEjb.getCiAttributesFromDb :: Method Returned with #{} Attributes", ciAttributes.size());
        return ciAttributes;
    }

    private Map<String, String> transformCiAttributeToUcmdbForm(List<CIAttribute> taskAttributes, List<CiAttributeConfiguration> dbAttributes) {
        log.debug(" UpdateEjb.transformCiAttributeToUcmdbForm :: Taking attributes for task attributes {}", taskAttributes);
        Map<String, String> attributesForUcmdb = new HashMap<>();

        taskAttributes.forEach(ta -> {
            log.debug(" UpdateEjb.transformCiAttributeToUcmdbForm :: Working with Attribute {}", ta);

            CiAttributeConfiguration currentAttribute = getCiAttributesFromDbAttributesByFieldName(dbAttributes, ta.getFieldName());

            if (!currentAttribute.isCanBeNull()) {
                if (ta.getFieldValue().trim().isEmpty()) {
                    attributesForUcmdb.put(ta.getFieldName(), null);
                }
                else {
                    attributesForUcmdb.put(ta.getFieldName(), ta.getFieldValue().trim());
                }
            }
            else {
                attributesForUcmdb.put(ta.getFieldName(), ta.getFieldValue().trim());
            }
        });

        return attributesForUcmdb;
    }

    private boolean validateTransformedAttributes(Map<String, String> attributesForUcmdb) {
        return attributesForUcmdb.values().contains(null);
    }

    private CiAttributeConfiguration getCiAttributesFromDbAttributesByFieldName(List<CiAttributeConfiguration> dbAttributes, String fieldName) {
        return dbAttributes.stream().filter(attribute -> attribute.getAttributeName().equals(fieldName)).findFirst().orElse(null);
    }
}
