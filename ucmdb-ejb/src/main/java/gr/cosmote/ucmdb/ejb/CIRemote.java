package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.models.base.*;
import gr.cosmote.ucmdb.models.database.UcmdbCisComment;
import gr.cosmote.ucmdb.models.view.UpdateCiDTO;
import gr.cosmote.ucmdb.models.view.UserCommentDTO;
import gr.cosmote.ucmdb.models.view.cidetails.CiDetails;

import javax.ejb.Remote;
import java.io.Serializable;
import java.util.List;

@Remote
public interface CIRemote extends Serializable{
    CiDetails getAttributesByID (String id, String[] properties);

    String getCiParentClass(String ciType);

    Boolean isCiTypeOfNode(String className);
    Boolean isCiTypeAppropriateForUpdate(String className);
    Boolean isCiUpdatable (String globalId);
    List<CIAttribute> getCiCustomAttributes(String ciType, String tenant);
    GenericCI updateCiInUcmdb(UpdateCiDTO ci);
}
