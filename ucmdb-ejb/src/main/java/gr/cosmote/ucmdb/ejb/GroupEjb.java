package gr.cosmote.ucmdb.ejb;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.ejb.*;
import javax.persistence.*;

import gr.cosmote.ucmdb.commons.exceptions.ApplicationException;
import gr.cosmote.ucmdb.commons.properties.ApplicationProperties;
import gr.cosmote.ucmdb.models.base.NameCodeObject;
import gr.cosmote.ucmdb.models.database.Group;
import gr.cosmote.ucmdb.models.database.Tenants;
import gr.cosmote.ucmdb.models.database.UcmdbUsers;
import gr.cosmote.ucmdb.models.view.GroupDTO;
import gr.cosmote.ucmdb.models.view.Tenant;
import gr.cosmote.ucmdb.models.view.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;

@Stateless
@Remote(GroupRemote.class)
@Slf4j
public class GroupEjb {
    private final String IS_NEW_UCMDB = ApplicationProperties.getInstance().getProperty("NEW_UCMDB");

    @PersistenceContext(unitName = "ucmdbPU", type = PersistenceContextType.TRANSACTION)
    EntityManager entityManager;

    @Resource
    private SessionContext sessionContext;

    @EJB
    UserRemote userEjb;

    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public List<Group> getAllGroups() {


        List<Group> groups;

        log.debug("GroupEjb :: getGroups :: method entered");

        try {
            groups = entityManager.createNativeQuery(GET_GROUPS_SQL, Group.class).getResultList();

            for (Group group : groups) {

                int userCnt = 0, subGroupCnt = 0;
                for (UcmdbUsers user : group.getUcmdbUsers()) {
                    if (user.getActive() == 1) {
                        userCnt++;
                    }
                }
                for (Group subGroup : group.getSubGroups()) {
                    if (subGroup.getActive() == 1) {
                        subGroupCnt++;
                        log.debug("sub group cnt " + subGroupCnt);
                    }
                }
                group.setNumberOfUsersInEachGroup(userCnt);
                group.setNumberOfSubgroupsInEachGroup(subGroupCnt);
            }

        } catch (NoResultException noResultException) {
            groups = null;
            log.error("GroupEjb :: getGroups :: ERROR {}", noResultException);
        }

        log.debug("GroupEjb :: getGroups :: method returning list of size: {}", groups.size());

        return groups;
    }

    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public Group getGroupByID(String groupID) {
        Group group;
        log.debug("GroupEjb :: getGroupsByID :: method entered");

        try {
            Query query = entityManager.createNativeQuery(GET_GROUP_BY_ID_SQL, Group.class);
            query.setParameter(1, groupID);
            group = (Group) query.getSingleResult();

            int userCnt = 0, subGroupCnt = 0;
            for (UcmdbUsers user : group.getUcmdbUsers()) {
                if (user.getActive() == 1) {
                    userCnt++;
                }
            }
            for (Group subGroup : group.getSubGroups()) {
                if (subGroup.getActive() == 1) {
                    subGroupCnt++;
                    log.debug("sub group cnt " + subGroupCnt);
                }
            }
            group.setNumberOfUsersInEachGroup(userCnt);
            group.setNumberOfSubgroupsInEachGroup(subGroupCnt);

        } catch (NoResultException noResultException) {
            group = null;
            log.error("GroupEjb :: getGroupByID :: ERROR {}", noResultException);
        }

        log.debug("GroupEjb :: getGroupByID :: method returning : {}", group);

        return group;
    }

    public List<Group> getUserGroups() {
        log.debug("GroupEjb.getUserGroups ::: Entered");

        final String username = sessionContext.getCallerPrincipal().getName();

        List<Group> groupsToReturn = new ArrayList<Group>();
        List<Group> groups = this.getAllGroups();
        if (groups != null) {
            if (CollectionUtils.isNotEmpty(groups)) {
                for (int i = 0; i < groups.size(); i++) {
                    for (int j = 0; j < groups.get(i).getUcmdbUsers().size(); j++) {
                        if (username.equals(groups.get(i).getUcmdbUsers().get(j).getUsername())) {
                            groupsToReturn.add(groups.get(i));
                        }
                    }
                }
            }
        }

        log.debug("GroupEjb.getUserGroups ::: Returned size {}", groupsToReturn.size());
        return groupsToReturn;
    }

    public List<Tenants> getUserTenants() {
        log.debug("GroupEjb.getUserTenants :: Method Entered");
        Predicate<Tenants> tenantsPredicate = getTenantsPredicate();

        List<Tenants> userTenants = this.getUserGroups()
                .stream()
                .map(Group::getTenants)
                .flatMap(Collection::stream)
                .filter(tenantsPredicate)
                .distinct()
                .collect(Collectors.toList());

        log.debug("GroupEjb.getUserTenants :: Method Returned Tenants size of {}", userTenants.size());
        return userTenants;
    }

    private Predicate<Tenants> getTenantsPredicate() {
        Predicate<Tenants> tenantPredicate = (t -> t.isOldEnv() && !t.isNewEnv());
        boolean isNewUcmdb = Boolean.valueOf(IS_NEW_UCMDB);

        if (isNewUcmdb) {
            tenantPredicate = (t -> !t.isOldEnv() && t.isNewEnv());
        }

        return tenantPredicate;
    }

    public List<Tenants> getTenantsWithGroupsAndUsers() {

        log.debug("GroupEjb :: getTenantsWithGroupsAndUsers() :: entered");

        List<Group> groups = getAllGroups();
        List<Tenants> tenants;

        try {
            tenants = entityManager.createNativeQuery("SELECT T.TENANT_ID, T.DESCRIPTION FROM TENANTS T",
                    Tenants.class).getResultList();

            for (Tenants tenant : tenants) {
                List<GroupDTO> tmpListGroups = new ArrayList<>();
                List<User> tmpListUsers = new ArrayList<>();
                for (Group group : groups) {
                    if (group.getTenants().contains(tenant)) {
                        GroupDTO tmpGroup = new GroupDTO();
                        tmpGroup.setDescription(group.getDescription());
                        tmpGroup.setId(group.getId());
                        tmpListGroups.add(tmpGroup);
                        for (UcmdbUsers user : group.getUcmdbUsers()) {
                            User tmpUser = new User();
                            tmpUser.setId(user.getId());
                            tmpUser.setUsername(user.getUsername());
                            tmpListUsers.add(tmpUser);
                        }
                    }
                }
                tenant.setTenantGroups(tmpListGroups);
                Set<User> uniqueUsers = new LinkedHashSet<>(tmpListUsers);
                tmpListUsers.clear();
                tmpListUsers.addAll(uniqueUsers);
                tenant.setTenantUsers(tmpListUsers);
                tenant.setNumberOfUsers(tenant.getTenantUsers().size());
            }

        } catch (NoResultException noResultException) {
            tenants = null;
            log.error("GroupEjb :: getTenantsWithGroupsAndUsers() :: ERROR");
        }

        log.debug("GroupEjb :: getTenantsWithGroupsAndUsers() :: returning");

        return tenants;
    }

    public Group ucmdbInsertGroup(Group group) {
        log.debug("GroupEjb :: ucmdbInsertGroup :: method entered for group creation {}", group.getDescription());

        Group groupToInsert = null;

        if (group != null) {

            try {
                groupToInsert = new Group();
                groupToInsert.setCreationDate(new Date());
                groupToInsert.setDescription(group.getDescription());

                // Cannot Set IsWorkflow Related flag from GUI so it will be always 0
                groupToInsert.setIsWorkflowRelated(0);
                groupToInsert.setActive(1);

                // Add Users
                List<UcmdbUsers> groupUsers =
                        group.getUcmdbUsers()
                                .stream()
                                .map(user -> entityManager.find(UcmdbUsers.class, user.getId()))
                                .collect(Collectors.toList());
                groupToInsert.setUcmdbUsers(groupUsers);
                log.debug("GroupEjb :: ucmdbInsertGroup :: Added {} users in Group {}", groupUsers.size(), group.getDescription());

                // Add Tenants
                List<Tenants> groupTenants =
                        group.getTenants()
                                .stream()
                                .map(tenant -> entityManager.find(Tenants.class, tenant.getId()))
                                .collect(Collectors.toList());
                groupToInsert.setTenants(groupTenants);
                log.debug("GroupEjb :: ucmdbInsertGroup :: Added {} tenants in Group {}", groupTenants.size(), group.getDescription());

                entityManager.persist(groupToInsert);
            } catch (Exception e) {
                throw new ApplicationException("GroupEjb :: ucmdbInsertGroup :: ERROR while Persisting new Group");
            }
        }

        log.debug("GroupEjb :: ucmdbInsertGroup :: method created Group {}", group.getDescription());
        return groupToInsert;
    }

    public Group updateGroup(Group group) {
        log.debug("GroupEjb :: updateGroup :: Method Entered for updating Group {}", group);
        Group updatedGroup;

        try {
            Group dbGroup = entityManager.find(Group.class, group.getId());

            updatedGroup = dbGroup;

            // Clear Old Users, Tenants
            updatedGroup.getUcmdbUsers().clear();
            updatedGroup.getTenants().clear();

            // Added only new Users, Tenants
            group.getTenants().forEach(tenant -> {
                Tenants dbTenant = entityManager.find(Tenants.class, tenant.getId());
                updatedGroup.getTenants().add(dbTenant);
            });
            group.getUcmdbUsers().forEach(user -> {
                UcmdbUsers dbUser = entityManager.find(UcmdbUsers.class, user.getId());
                updatedGroup.getUcmdbUsers().add(dbUser);
            });
            entityManager.merge(updatedGroup);
        } catch (Exception ex) {
            throw new ApplicationException("GroupEjb :: updateGroup :: ERROR " + ex);
        }

        log.debug("GroupEjb :: updateGroup :: Method Successfully Updated Group");
        return updatedGroup;
    }

    public List<User> getAllUsers() {

        List<Group> allGroups = getAllGroups();
        List<User> allUsersView = new ArrayList<>();

        List<UcmdbUsers> allUsers = new ArrayList<>();

        List<UcmdbUsers> databaseUcmdbUsers =
                userEjb.getAllUsers()
                        .stream()
                        .filter(user -> user.getActive() == 1)
                        .collect(Collectors.toList());

        for (Group group : allGroups) {
            List<UcmdbUsers> groupUsers = group.getUcmdbUsers();

            for (UcmdbUsers user : groupUsers) {

                if (!allUsers.contains(user)) {
                    allUsers.add(user);

                    List<Group> tmpGroups = new ArrayList<>();
                    for (Group tmpGroup : allGroups) {
                        List<UcmdbUsers> tmpUsers = tmpGroup.getUcmdbUsers();
                        if (tmpUsers.contains(user)) {
                            if (!tmpGroups.contains(tmpGroup)) {
                                tmpGroups.add(tmpGroup);
                            }
                        }
                    }

                    List<Tenants> tmpTenants = new ArrayList<>();

                    String groupsStr = "";
                    Set<Group> uniqueGroups = new LinkedHashSet<>(tmpGroups);
                    tmpGroups.clear();
                    tmpGroups.addAll(uniqueGroups);

                    for (Group gr : tmpGroups) {
                        List<Tenants> tmp2Tenants = gr.getTenants();
                        for (Tenants t : tmp2Tenants) {
                            if (!tmpTenants.contains(t)) {
                                tmpTenants.add(t);
                            }
                        }
                        groupsStr += gr.getDescription() + ",";
                    }

                    String tenantsStr = "";
                    for (Tenants t : tmpTenants) {
                        tenantsStr += t.getDescription() + ",";
                    }

                    if (user.getActive() == 1) {

                        User tmpUser = new User();
                        tmpUser.setUsername(user.getUsername());
                        tmpUser.setId(user.getId());
                        tmpUser.setCreationDate(user.getCreationDate());
                        tmpUser.setTenantsNumber(tmpTenants.size());
                        tmpUser.setNumberOfGroups(tmpGroups.size());

                        if (tenantsStr.length() != 0) {
                            List<String> tenantsDescriptionList = Arrays.asList(tenantsStr.split(","));
                            List<Tenant> tenantList = new ArrayList<>();
                            for (String str : tenantsDescriptionList) {
                                Tenant tenant = new Tenant();
                                tenant.setDescription(str);
                                tenantList.add(tenant);
                            }

                            tmpUser.setTenantsDescriptions(tenantsStr.substring(0, tenantsStr.length() - 1));
                            tmpUser.setTenants(tenantList);
                        } else {
                            tmpUser.setTenantsDescriptions("");
                        }
                        if (groupsStr.length() != 0) {
                            tmpUser.setGroupsDescriptions(groupsStr.substring(0, groupsStr.length() - 1));
                        } else {
                            tmpUser.setGroupsDescriptions("");
                        }
                        tmpUser.setGroup(tmpGroups);

                        allUsersView.add(tmpUser);
                    }
                }
            }

            allUsers.addAll(groupUsers);
        }

        int found = 0;
        for (UcmdbUsers userTmp : databaseUcmdbUsers) {
            found = 0;
            for (User userTmp2 : allUsersView) {
                if (userTmp2.getUsername().equals(userTmp.getUsername())) found = 1;
            }
            if (found == 0) {
                User viewUser = new User();
                viewUser.setUsername(userTmp.getUsername());
                viewUser.setId(userTmp.getId());
                viewUser.setCreationDate(userTmp.getCreationDate());
                allUsersView.add(viewUser);
            }
        }

        return allUsersView;
    }

    public Group makeGroupInactive(NameCodeObject nameCode) {
        log.debug("GroupEjb :: makeGroupInactive :: Method Entered for Group with ID: {}", nameCode.getId());

        Group groupToMakeInactive;
        try {
            int groupId = Integer.valueOf(nameCode.getId());
            groupToMakeInactive = entityManager.find(Group.class, groupId);
            groupToMakeInactive.setActive(0);

            entityManager.merge(groupToMakeInactive);
        } catch (Exception ex) {
            throw new ApplicationException("GroupEjb :: makeGroupInactive :: ERROR {}", ex);
        }

        log.debug("GroupEjb :: makeGroupInactive :: Method Deactivated Group {}", groupToMakeInactive.getDescription());
        return groupToMakeInactive;
    }

    private static final String GET_GROUPS_SQL =
            "SELECT G.GROUP_ID, G.DESCRIPTION, G.CREATION_DATE, G.PARENT_ID, G.ACTIVE, G.EMAIL, G.IS_WORKFLOW_RELATED FROM GROUPS G " +
                    "WHERE ACTIVE=1";

    private static final String GET_GROUP_BY_ID_SQL =
            "SELECT G.GROUP_ID, G.DESCRIPTION, G.CREATION_DATE, G.PARENT_ID, G.ACTIVE, G.EMAIL, G.IS_WORKFLOW_RELATED FROM GROUPS G " +
                    "WHERE G.GROUP_ID = ?";

}
