package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.models.base.NameCodeObject;
import gr.cosmote.ucmdb.models.database.SavedSearches;

import java.io.Serializable;
import java.util.List;

public interface SearchRemote extends Serializable {
    List<SavedSearches> getSearches();
    SavedSearches ucmdbInsertSearch(SavedSearches savedSearches);
    Boolean ucmdbRemoveSearch(int searchId);
}
