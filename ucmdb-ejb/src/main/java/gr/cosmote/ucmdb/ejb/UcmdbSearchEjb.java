package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.models.database.SearchType;
import lombok.extern.slf4j.Slf4j;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Slf4j
@Stateless
public class UcmdbSearchEjb implements UcmdbSearchRemote {
    @PersistenceContext
    private EntityManager em;

    public List<SearchType> getSearchTypes() {
        log.debug("UcmdbSearchEjb.getSearchTypes :: Method Entered");

        List<SearchType> searchTypes = em.createNamedQuery("SearchType.getAll", SearchType.class).getResultList();

        log.debug("UcmdbSearchEjb.getSearchTypes :: Method Returned search types size of {}", searchTypes.size());
        return searchTypes;
    }
}
