package gr.cosmote.ucmdb.ejb.rules;

import lombok.extern.slf4j.Slf4j;
import gr.cosmote.ucmdb.commons.logging.AgendaEventLogger;
import gr.cosmote.ucmdb.commons.logging.BaseEventLogger;
import gr.cosmote.ucmdb.commons.logging.RuleEventLogger;
import gr.cosmote.ucmdb.commons.properties.ApplicationProperties;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.builder.KieScanner;
import org.kie.api.builder.ReleaseId;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.StatelessKieSession;


@Slf4j
public class RuleEngineSessionFactory {
    private static final String loggingEnable = ApplicationProperties.getInstance().getProperty("GR.COSMOTE.UCMDB.RULESENGINE.LOGGING.ENABLED");
    private static final String scanningPeriod = ApplicationProperties.getInstance().getProperty("GR.COSMOTE.UCMDB.RULESENGINE.SCANNER.PERIOD.MS");
    public static final RuleEngineSessionFactory Instance = new RuleEngineSessionFactory();
    public static final String SessionAttrName = "$RuleEngine$";

    private final boolean enableLogging;
    private final KieBase kBase;

    private RuleEngineSessionFactory() {
        enableLogging = Boolean.parseBoolean(loggingEnable);
        long scannerTimeoutMS = Long.parseLong(scanningPeriod);

        KieServices kieServices = KieServices.Factory.get();
        ReleaseId releaseId = kieServices.newReleaseId("gr.cosmote.ucmdb", "ucmdb-rules", "3.5");
        KieContainer kContainer = kieServices.newKieContainer(releaseId);
        KieScanner kScanner = kieServices.newKieScanner(kContainer);
        kScanner.start(scannerTimeoutMS);
        long beginTm = System.currentTimeMillis();
        kBase = kContainer.getKieBase();
        if (enableLogging)
            kBase.addEventListener(new BaseEventLogger());
        long duration = System.currentTimeMillis() - beginTm;
        log.info("RuleEngineSessionFactory initialization time:" + duration + " - Logging enabled:" + enableLogging);
    }

    public synchronized KieSession buildNewSession() {
        KieSession kSession = kBase.newKieSession();
        if (enableLogging) {
            kSession.addEventListener(new RuleEventLogger());
            kSession.addEventListener(new AgendaEventLogger());
        }
        return kSession;
    }

    public synchronized StatelessKieSession buildNewStatelessSession() {
        StatelessKieSession statelessKieSession = kBase.newStatelessKieSession();
        if (enableLogging) {
            statelessKieSession.addEventListener(new RuleEventLogger());
            statelessKieSession.addEventListener(new AgendaEventLogger());
        }
        return statelessKieSession;
    }
}
