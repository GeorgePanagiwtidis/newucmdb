package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.models.database.Group;
import gr.cosmote.ucmdb.models.database.Node;
import gr.cosmote.ucmdb.models.database.Task;
import gr.cosmote.ucmdb.models.database.TaskSummary;
import gr.cosmote.ucmdb.models.enumeration.InputCommandEnum;
import gr.cosmote.ucmdb.models.view.WorkflowStatsDTO;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface TasksRemote {
    void updateTaskSummaryRecords(Node node);

    List<TaskSummary> getTaskSummaryRecords(List<Group> groupList);

    List<TaskSummary> getTaskSummaryRecordsByNodeId(long nodeId);

    List<TaskSummary> getTaskSummaryRecords();

    TaskSummary getTaskSummaryRecord(long taskId);

    void setTaskAssignee(TaskSummary taskSummary);

    void evaluateTaskAction(Task task, InputCommandEnum inputCommandEnum);

    Task reassignTask(Task task, String groupId, String reassignReason);
}
