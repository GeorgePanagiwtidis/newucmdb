package gr.cosmote.ucmdb.ejb;

import java.io.Serializable;
import java.util.List;
import javax.ejb.Remote;

import gr.cosmote.ucmdb.models.base.NameCodeObject;
import gr.cosmote.ucmdb.models.database.UcmdbUsers;
import gr.cosmote.ucmdb.models.view.UserInfoDTO;
import gr.cosmote.ucmdb.models.view.UserLoginInfo;

@Remote
public interface UserRemote extends Serializable{
    int ucmdbLogin(UserLoginInfo userLoginInfo);
    UcmdbUsers getUser();
    UserInfoDTO getUserInfo();
    UcmdbUsers ucmdbInsertUser(String username);
    List<UcmdbUsers> getAllUsers();
    UcmdbUsers makeUserInactive (NameCodeObject nameCode);
}
