package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.ejb.rules.RuleEngineSessionFactory;
import gr.cosmote.ucmdb.models.commons.RulesExecutionStatus;
import gr.cosmote.ucmdb.models.database.Task;
import gr.cosmote.ucmdb.models.enumeration.InputCommandEnum;
import gr.cosmote.ucmdb.models.enumeration.OutputCommandEnum;
import gr.cosmote.ucmdb.models.rules.RulesEngineCommand;
import lombok.extern.slf4j.Slf4j;
import org.kie.api.runtime.KieSession;

import javax.ejb.*;
import java.util.*;
import java.util.stream.Collectors;

@Stateless
@Remote(RulesEngineServiceRemote.class)
@Slf4j
public class RulesEngineServiceEjb {
    private String className = this.getClass().getName();
    @EJB()
    private GroupRemote groupEjb;

    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public List<RulesEngineCommand<OutputCommandEnum>> evaluateRules(List<RulesEngineCommand<InputCommandEnum>> inCommandList) {
        log.debug("RulesEngineServiceEjb :: evaluateRules :: method entered with input: {}", inCommandList);

        List<RulesEngineCommand<OutputCommandEnum>> outputCommandList = inCommandList != null ? new ArrayList<>() : null;
        Optional.ofNullable(inCommandList).ifPresent(x -> {
            inCommandList.forEach(commandEntity -> {
                if (commandEntity.getCommands().stream().anyMatch(c -> c != InputCommandEnum.IGNORE_TASK_COMMAND
                        && c != InputCommandEnum.SAVE_TASK_COMMAND)) {

                    KieSession kSession = RuleEngineSessionFactory.Instance.buildNewSession();
                    RulesExecutionStatus executionStatus = new RulesExecutionStatus();
                    executionStatus.setStatus("NOT_MATCHED");
                    kSession.setGlobal("Groups", groupEjb.getAllGroups());
                    kSession.insert(executionStatus);
                    kSession.insert(commandEntity);
                    kSession.fireAllRules();
                    Collection<?> resultList = kSession.getObjects();
                    resultList.forEach(t -> {
                        if (t instanceof RulesEngineCommand) {
                            outputCommandList.add((RulesEngineCommand<OutputCommandEnum>) t);
                        }
                    });
                    kSession.dispose();
                } else {
                    RulesEngineCommand<OutputCommandEnum> outputCommand = new RulesEngineCommand<>();
                    outputCommand.setTask(commandEntity.getTask());
                    outputCommandList.add(outputCommand);
                }
            });
        });

        log.debug("RulesEngineServiceEjb :: evaluateRules :: rules-engine-output: {}", outputCommandList);

        return outputCommandList;

    }

    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public Task reassignTask(Task task, String groupId, String reassignReason) {
        log.debug("RulesEngineServiceEjb.reassignTask() ::: Reassigning task {} to group {} with reason {}", task, groupId, reassignReason);
        Task outputTask = null;
        if (task != null && !groupId.isEmpty()) {

            KieSession kSession = RuleEngineSessionFactory.Instance.buildNewSession();
            RulesExecutionStatus executionStatus = new RulesExecutionStatus();
            executionStatus.setStatus("REASSIGN_TASK");
            kSession.insert(executionStatus);
            kSession.insert(task);
            kSession.insert(reassignReason);
            kSession.insert(groupEjb.getGroupByID(groupId));
            kSession.fireAllRules();
            Collection<?> resultList = kSession.getObjects();
            for (Object t : resultList)
                if (t instanceof Task) {
                    outputTask = (Task) t;
                    break;
                }
            kSession.dispose();
        }

        log.debug("RulesEngineServiceEjb.reassignTask() ::: Reassigning output task {} ", outputTask);

        return outputTask;
    }

    private boolean isNodeFlowCompleted(List<RulesEngineCommand<OutputCommandEnum>> outCommand) {
        List<Task> notClosedTasks = outCommand.stream()
                .filter(oc -> oc.getTask().isOpen())
                .map(RulesEngineCommand::getTask)
                .collect(Collectors.toList());

        return (notClosedTasks == null || notClosedTasks.size() == 0);
    }

}
