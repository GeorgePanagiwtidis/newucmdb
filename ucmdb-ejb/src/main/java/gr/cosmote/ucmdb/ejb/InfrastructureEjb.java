package gr.cosmote.ucmdb.ejb;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import gr.cosmote.ucmdb.client.UCMDBCallHelper;
import gr.cosmote.ucmdb.commons.exceptions.ApplicationException;
import gr.cosmote.ucmdb.commons.properties.ApplicationProperties;
import gr.cosmote.ucmdb.models.base.AdvancedCriteria;
import gr.cosmote.ucmdb.models.base.BusinessSearchCriteria;
import gr.cosmote.ucmdb.models.base.Infrastructure;
import gr.cosmote.ucmdb.models.base.Tenant;
import gr.cosmote.ucmdb.models.database.*;
import gr.cosmote.ucmdb.models.view.infrastructureDTOs.InfraExcelExportRequest;
import gr.cosmote.ucmdb.models.view.savedsearches.InfrastructureOptionDTO;
import gr.cosmote.ucmdb.ucmdbWS.*;
import lombok.extern.slf4j.Slf4j;

import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.ws.Holder;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Remote(InfrastructureRemote.class)
@Stateless
@Slf4j
public class InfrastructureEjb {

    // TODO -- Remove After uCDMB Migration Completes
    private final Boolean IS_NEW_UCMDB_ENV  = ApplicationProperties.getInstance().getProperty("NEW_UCMDB").equalsIgnoreCase("true");
    private final List<String> MIGRATED_TENANTS = Arrays.asList("ICT-OTHER", "DT-ONE", "RURAL");

    @PersistenceContext
    private EntityManager em;
    @EJB
    CIRemote ciEjb;
    @EJB
    TenantRemote tenantsRemote;

    private static LoadingCache<AdvancedCriteria, List<Infrastructure>> SEARCHINFRACACHE = null;

    private static final String GET_CI_ATTRIBUTES_BY_TYPE = "SELECT Ci FROM CiAttributeConfiguration Ci WHERE Ci.ciType=:type";
    private static final String GET_INFRA_PROPERTIES_BY_TYPE = "SELECT Prop FROM InfrastructureCiProperties Prop WHERE Prop.type.ciType=:type";
    private static final String GET_INFRA_TYPES = "SELECT it from InfrastructureCiTypes it";
    private static final String GET_INFRA_CUSTOM_ATTRIBUTES_BY_TYPE = "SELECT infraAttributes FROM InfrastructureCustomAttributes infraAttributes WHERE infraAttributes.infraType.typeId=:typeId";

    public void clearCache() {
        log.debug("InfrastructureEjb :: clearCache :: Method Entered");

        if (SEARCHINFRACACHE != null) {
            SEARCHINFRACACHE.invalidateAll();
        }

        log.debug("InfrastructureEjb :: clearCache :: Method Cleared Cache");
    }

    public void clearCacheForSpecificRequest(AdvancedCriteria searchCriteria) {
        log.debug("InfrastructureEjb :: clearCacheForSpecificRequest :: Method Entered for cache key {}", searchCriteria);

        if (SEARCHINFRACACHE == null) {
            throw new gr.cosmote.ucmdb.commons.exceptions.ApplicationException("InfrastructureEjb :: clearCacheForSpecificRequest :: ERROR in Method - Cache object is null");
        } else {
            SEARCHINFRACACHE.invalidate(searchCriteria);
        }

        log.debug("InfrastructureEjb :: clearCacheForSpecificRequest :: Method Cleared Cache for key {}", searchCriteria);
    }

    @TransactionAttribute(value = TransactionAttributeType.NEVER)
    public List<Infrastructure> infrastructureSearch(AdvancedCriteria searchCriteria) {
        log.debug("InfrastructureEjb :: infrastructureSearch :: entered with searchTerm {} and tenants {}"
                , searchCriteria.getBusinessCriteria().getSearchTerm(), searchCriteria.getBusinessCriteria().getTenants());

        if (SEARCHINFRACACHE == null) {
            log.debug("InfrastructureEjb :: infrastructureSearch :: Result is not Cached, Retrieving from uCMDB");
            SEARCHINFRACACHE = CacheBuilder.newBuilder()
                    .expireAfterWrite(5, TimeUnit.HOURS)
                    .refreshAfterWrite(10, TimeUnit.HOURS).build(
                            new CacheLoader<AdvancedCriteria, List<Infrastructure>>() {
                                @Override
                                public List<Infrastructure> load(AdvancedCriteria key) throws Exception {
                                    List<Infrastructure> infraResult;

                                    String ciType = key.getBusinessCriteria().getSearchTerm();
                                    List<Tenant> tenants = key.getBusinessCriteria().getTenants();
                                    List<InfrastructureOptionDTO> options = key.getAdvancedTerms();

                                    List<String> propertiesList = getInfrastructureCIsPropertiesByCategory(ciType);

                                    List<Infrastructure> ucmdbInfrastructureResult = callUcmdbForResult(ciType, propertiesList, initTenantConditions(tenants));
                                    //List<Infrastructure> infrastructureResultsWithChildren = getInfrastructureElemenentChildrenByType(ucmdbInfrastructureResult, "ip_address" ,new String[]{"display_label"});
                                    infraResult = filterResults(ucmdbInfrastructureResult, options);

                                    return infraResult;
                                }
                            });
        }

        return getResultsFromCache(searchCriteria);
    }

    public List<InfrastructureCiTypes> getInfrastructureTypes() {
        List<InfrastructureCiTypes> infrastructureTypes = null;
        log.debug("InfrastructureEjb :: getInfrastructureTypes :: Method Entered");

        infrastructureTypes = em.createQuery(GET_INFRA_TYPES, InfrastructureCiTypes.class).getResultList();

        log.debug("InfrastructureEjb :: getInfrastructureTypes :: Method Returned infra types {}", infrastructureTypes);
        return infrastructureTypes;
    }

    public List<InfrastructureCustomAttributes> getCustomAttributesByType(int infraTypeId) {
        log.debug("InfrastructureEjb :: getCustomAttributes :: Method Entered for infrastructure type Id {}", infraTypeId);

        List<InfrastructureCustomAttributes> customAttributes = em.createQuery(GET_INFRA_CUSTOM_ATTRIBUTES_BY_TYPE, InfrastructureCustomAttributes.class).setParameter("typeId", infraTypeId).getResultList();

        log.debug("InfrastructureEjb :: getCustomAttributes :: Method Returned Infra Custom Attributes {} ", customAttributes);

        return customAttributes;
    }

    public List<Infrastructure> getInfraResultsForExport(InfraExcelExportRequest exportRequest) {
        log.debug("InfrastructureEjb :: getInfraResultsForExport :: Method Entered");

        // Transform Request Object
        AdvancedCriteria criteria = new AdvancedCriteria(exportRequest);
        List<Infrastructure> filteredResult;

        try {
            List<Infrastructure> infraResult = infrastructureSearch(criteria);

            if (exportRequest.getTableFilters().isEmpty()) {
                filteredResult = infraResult;
            } else {
                filteredResult =
                        infraResult.stream()
                                .filter(infra ->
                                        exportRequest
                                                .getTableFilters()
                                                .stream().allMatch(filter -> {
                                            if (filter.getPropertyValue().equals("!")) {
                                                return infra.getProperties().get(filter.getPropertyName()) == null;
                                            } else {
                                                if (infra.getProperties().get(filter.getPropertyName()) != null) {
                                                    return infra.getProperties().get(filter.getPropertyName()).contains(filter.getPropertyValue());
                                                } else {
                                                    return false;
                                                }
                                            }
                                        })
                                )
                                .collect(Collectors.toList());
            }
        } catch (Exception ex) {
            log.error("InfrastructureEjb :: getInfraResultsForExport :: ERROR {}", ex);
            return new ArrayList<>();
        }

        log.debug("InfrastructureEjb :: getInfraResultsForExport :: Method Returned Result to ExportService");
        return filteredResult;
    }

    public void exportInfrastructureComputersToDatabase() {
        log.debug("InfrastructureEjb.exportInfrastructureComputersToDatabase :: Method Entered for {} Environment", (IS_NEW_UCMDB_ENV? "--NEW--" : "--OLD--"));

        String       exportTableName        = ApplicationProperties.getInstance().getProperty("INFRASTRUCTURE_COMPUTERS_DB_TABLE");
        List<String> exportedTableColumns   = getInfrastructureCIsPropertiesByCategory("host_node");

        try {
            if (!IS_NEW_UCMDB_ENV) {
                dropInfrastuctureTable(exportTableName);
                reCreateInfrastructureTable(exportTableName, exportedTableColumns);
            }

            // Get Data and Insert it in Table
            List<Infrastructure> allComputers = infrastructureSearch(getAllInfrastructureComputersCriteria());
            insertComputersDataInTable(exportTableName, exportedTableColumns, allComputers);
        } catch (ApplicationException appEx) {
            log.error("InfrastructureEjb.exportInfrastructureComputersToDatabase :: Error", appEx);
        }

        log.debug("InfrastructureEjb.exportInfrastructureComputersToDatabase :: Method Returned");
    }

    private String transformCiTypeToDatabaseCiTypeBasedOnTemplates(String ciType) {
        String regex = "(?i)(.*(host|database|application_server).*)";

        return ciType.matches(regex) ? ciType : "combinational";
    }

    private List<Infrastructure> getResultsFromCache(AdvancedCriteria searchCriteria) {
        try {
            List<Infrastructure> resFromCache = SEARCHINFRACACHE.get(searchCriteria);

            log.debug("InfrastructureEjb.getResultsFromCache ::: returning from CACHE list of size: {}", resFromCache.size());

            return resFromCache;

        } catch (Exception e) {
            log.error("InfrastructureEjb.getResultsFromCache ::: Error in getResultsFromCache method", e);
            return null;
        }
    }

    private List<String> getInfrastructureCIsPropertiesByCategory(String type) {
        log.debug("InfrastructureEjb :: getInfrastructureCIsPropertiesByCategory :: Entered Method for CI Type: ", type);

        List<String> propertiesList = null;

        List<InfrastructureCiProperties> infrastructureCiProperties = em.createQuery(GET_INFRA_PROPERTIES_BY_TYPE, InfrastructureCiProperties.class).setParameter("type", type).getResultList();

        propertiesList =
                infrastructureCiProperties
                        .stream()
                        .map((prop) -> prop.getPropertyName())
                        .collect(Collectors.toList());

        log.debug("InfrastructureEjb :: getInfrastructureCIsPropertiesByCategory :: Method returned properties {} ", propertiesList);

        return propertiesList;
    }

    private Conditions initTenantConditions(List<Tenant> tenants) {
        log.debug("InfrastructureEjb :: initTenantConditions :: Method entered with tenants {}", tenants);
        Conditions conditions = new Conditions();
        StrConditions strConditions = new StrConditions();
        conditions.setStrConditions(strConditions);

        tenants
                .stream()
                .forEach((tenant -> {
                    StrCondition strCondition = new StrCondition();
                    StrProp strProp = new StrProp();
                    strProp.setName("TenantOwner");
                    strProp.setValue(new ObjectFactory().createStrPropValue(tenant.getDescription()));

                    strCondition.setStrOperator("Equal");
                    strCondition.setCondition(strProp);

                    strConditions.getStrCondition().add(strCondition);
                }));


        log.debug("InfrastructureEjb :: initTenantConditions :: Method Returned Conditions object {}", conditions);
        return conditions;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private List<Infrastructure> callUcmdbForResult(String ciType, List<String> propertiesList, Conditions tenantConditions) {
        log.debug("InfrastructureEjb :: callUcmdbForResult :: Entered Method with input {} ", ciType, propertiesList);

        List<Infrastructure> infraResult = new ArrayList<>();

        CustomProperties customProperties = new CustomProperties();
        PropertiesList properties = new PropertiesList();
        properties.getPropertyName().addAll(propertiesList);

        customProperties.setPropertiesList(properties);

        Holder<CIs> ucmdbInfraResponse = UCMDBCallHelper.getFilteredCIsByType(ciType, "OR", customProperties, tenantConditions);
        List<CI> cis = ucmdbInfraResponse.value.getCI();

        infraResult = cis.stream().map(ci -> {
            Infrastructure infra = new Infrastructure();

            infra.setProperties(UCMDBCallHelper.getProperties(ci.getID().getValue(), cis));
            infra.setGlobalId(infra.getProperties().get("global_id"));

            return infra;
        }).collect(Collectors.toList());

        log.debug("InfrastructureEjb :: callUcmdbForResult :: Method returned result {} ", infraResult);

        return infraResult;
    }

    private List<Infrastructure> getInfrastructureElemenentChildrenByType(List<Infrastructure> infrastructureElement, String ciType, String[] properties) {
        List<Infrastructure> resultWithChildrenElements = infrastructureElement
                .stream()
                .map((element) -> {
                    ID id = new ID();
                    id.setValue(element.getProperties().get("global_id"));

                    TypedPropertiesCollection typedPropertiesCollection = constructTypedPropertiesCollectionObject(ciType, properties);
                    Holder<Topology> topologyHolder = UCMDBCallHelper.getCINeighbours(id, ciType, typedPropertiesCollection);

                    Map<String, String> props = getChildrenPropertiesAsList(topologyHolder.value.getCIs().getCI());

                    props.keySet()
                            .stream()
                            .forEach((prop) -> element.getProperties().putIfAbsent(prop, props.get(prop)));

                    return element;
                })
                .collect(Collectors.toList());

        return resultWithChildrenElements;
    }

    private List<Infrastructure> storeGlobalIdAndDeleteItFromProperties(List<Infrastructure> infraResult) {
        infraResult =
                infraResult
                        .stream()
                        .map((infraItem) -> {
                            infraItem.setGlobalId(infraItem.getProperties().get("global_id"));
                            return infraItem;
                        })
                        .collect(Collectors.toList());

        return infraResult;
    }

    private List<Infrastructure> filterResults(List<Infrastructure> ucmdbResult, List<InfrastructureOptionDTO> searchOptions) {
        return filterResultBasedOnOptions(filterDuplicateCIs(ucmdbResult), searchOptions);
    }

    private List<Infrastructure> filterDuplicateCIs(List<Infrastructure> ucmdbResult) {
        log.debug("InfrastructureEjb.filterDuplicateCIs :: Method Entered with unfiltered CIs size of {}", ucmdbResult.size());
        List<Infrastructure> filteredDuplicateCIs;

        filteredDuplicateCIs =
                ucmdbResult
                        .stream()
                        .filter(ci -> !"TO-BE-DELETED(DUPLICATE)".equals(ci.getProperties().get("category")))
                        .filter(ci -> !"TO-BE-DELETED(JUNK)".equals(ci.getProperties().get("category")))
                        .filter(ci -> !"DECOMMISSIONED".equals(ci.getProperties().get("category")))
                        .collect(Collectors.toList());

        log.debug("InfrastructureEjb.filterDuplicateCIs :: Method Returned filtered CIs size of {}", filteredDuplicateCIs.size());
        return filteredDuplicateCIs;
    }

    private List<Infrastructure> filterResultBasedOnOptions(List<Infrastructure> filteredInfraResult, List<InfrastructureOptionDTO> options) {
        log.debug("InfrastructureEjb.filterResultBasedOnOptions :: Method Entered with unfiltered CIs size of {}", filteredInfraResult.size());

        List<Infrastructure> filteredInfrastructureResultByOptions;

        // Transform Options Values for Wildcards
        List<InfrastructureOptionDTO> wildCardOptions = options
                .stream()
                .map((option) -> {
                    option.setAttributeValue(transformWildcardOptionToRegex(option.getAttributeValue()));
                    return option;
                })
                .filter((option) -> !option.getAttributeValue().isEmpty())
                .collect(Collectors.toList());

        if (options.isEmpty()) {
            log.debug("InfrastructureEjb.filterResultBasedOnOptions :: No Extra Filter Terms Provided ");

            filteredInfrastructureResultByOptions = filteredInfraResult;
        } else {
            filteredInfrastructureResultByOptions =
                    filteredInfraResult
                            .stream()
                            .filter((infraItem) ->
                                    wildCardOptions
                                            .stream()
                                            .allMatch(
                                                    (option) -> {
                                                        String attibuteName = option.getAttributeName();

                                                        if ("comment".equals(option.getAttributeName().toLowerCase())) {
                                                            return filterCiByUserComment(infraItem, option);
                                                        }
                                                        if ("support".equals(option.getAttributeName().toLowerCase())) {
                                                            return filterCiBySupportAttribute(infraItem, option);
                                                        }
                                                        if ("primary_ip_address".equals(attibuteName)) {
                                                            return shouldAddInfraItemBasedByIP(infraItem, option);
                                                        } else {
                                                            if ((infraItem.getProperties().get(attibuteName)) != null) {
                                                                return infraItem.getProperties().get(attibuteName).toUpperCase()
                                                                        .matches(option.getAttributeValue().toUpperCase());
                                                            } else {
                                                                return false;
                                                            }
                                                        }
                                                    }))
                            .collect(Collectors.toList());
        }


        log.debug("InfrastructureEjb.filterResultBasedOnOptions :: Method Returned filtered Infra Result size of {}", filteredInfrastructureResultByOptions.size());

        return filteredInfrastructureResultByOptions;
    }

    private boolean filterCiBySupportAttribute(Infrastructure infraResult, InfrastructureOptionDTO supportFilter) {
        boolean matchesSupportAttributes;
        try {
            List<UcmdbInfoWindowGroupAttributes> supportAttributes =
                    em.createNamedQuery("UcmdbSupportAttribute.getSupportAttributes", UcmdbInfoWindowGroupAttributes.class).getResultList();

            long numberOfMatchingSupportAttributes =
                    supportAttributes
                            .stream()
                            .filter(supportAttribute ->
                                    infraResult.getProperties().get(supportAttribute.getAttributeName()) != null
                                            && (infraResult.getProperties().get(supportAttribute.getAttributeName()).toUpperCase().matches((supportFilter.getAttributeValue()).toUpperCase())))
                            .count();

            matchesSupportAttributes = (numberOfMatchingSupportAttributes != 0);

        } catch (Exception ex) {
            log.error("InfrastructureEjb.filterCiBySupportAttribute :: Error", ex);
            return false;
        }

        return matchesSupportAttributes;
    }

    private boolean filterCiByUserComment(Infrastructure infraResult, InfrastructureOptionDTO commentFilter) {
        boolean matchesComment;

        Pattern pattern = Pattern.compile(commentFilter.getAttributeValue(), Pattern.DOTALL);

        try {
            String ciDisplayLabel = infraResult.getProperties().get("display_label");
            String ciTenantOwner = infraResult.getProperties().get("TenantOwner");
            String globalId      = infraResult.getProperties().get("global_id");

            UcmdbCI ucmdbCI =
                    em.createNamedQuery("UcmdbCIs.getCiBasedOnTenantOwnerDispLabelAndGlobId", UcmdbCI.class)
                            .setParameter("dispLabel", ciDisplayLabel)
                            .setParameter("tenantOwner", ciTenantOwner)
                            .setParameter("globalId", globalId)
                            .getSingleResult();

            long numberOfMathingComments = ucmdbCI.getCiComments().stream()
                    .filter(comment -> pattern.matcher(comment.getCommentText()).find())
                    .count();

            matchesComment = (numberOfMathingComments != 0);

        } catch (Exception ex) {
            return false;
        }

        return matchesComment;
    }

    private boolean shouldAddInfraItemBasedByIP(Infrastructure infraItem, InfrastructureOptionDTO ipOption) {
        // Get Properties related to IP Address
        String primaryIpAddress = infraItem.getProperties().get("primary_ip_address") != null ? infraItem.getProperties().get("primary_ip_address") : "";
        String defaultGatewayIpAddress = infraItem.getProperties().get("default_gateway_ip_address") != null ? infraItem.getProperties().get("default_gateway_ip_address") : "";
        String hostKey = infraItem.getProperties().get("host_key") != null ? infraItem.getProperties().get("host_key") : "";
        String[] dnsServers = infraItem.getProperties().get("dns_servers") != null ? infraItem.getProperties().get("dns_servers").split(",") : new String[0];

        String userTypedIp = ipOption.getAttributeValue();

        boolean shoudlAddInfraItem;
        if (primaryIpAddress.matches(userTypedIp) || defaultGatewayIpAddress.matches(userTypedIp) || hostKey.matches(userTypedIp + "(.*)")) {
            shoudlAddInfraItem = true;
        } else {
            shoudlAddInfraItem =
                    Arrays.stream(dnsServers)
                            .filter(dnsServer -> dnsServer.matches(userTypedIp))
                            .count() != 0;
        }

        return shoudlAddInfraItem;
    }

    private String transformWildcardOptionToRegex(String option) {
        StringBuffer sb = new StringBuffer(option);

        int allWildcardPosition = sb.indexOf("*");
        while (allWildcardPosition != -1) {
            sb.replace(allWildcardPosition, allWildcardPosition + 1, "(.*)");

            allWildcardPosition = sb.indexOf("*", allWildcardPosition + 3);
        }

        int aSymbolWildcardPosition = sb.indexOf("%");
        while (aSymbolWildcardPosition != -1) {
            sb.replace(aSymbolWildcardPosition, aSymbolWildcardPosition + 1, "(.?)");

            aSymbolWildcardPosition = sb.indexOf("%", aSymbolWildcardPosition + 3);
        }

        return sb.toString();
    }

    private TypedPropertiesCollection constructTypedPropertiesCollectionObject(String ciType, String[] properties) {
        log.debug("InfrastructureEjb.constructTypedPropertiesCollectionObject :: " +
                "Method Entered for Infra Elemenent with CiType: {}, Properties: {}", ciType, properties);


        TypedPropertiesCollection typedPropertiesCollection = new TypedPropertiesCollection();
        TypedProperties typedProperties = new TypedProperties();
        typedProperties.setType(ciType);

        CustomTypedProperties customTypedProperties = new CustomTypedProperties();

        PropertiesList propertiesList = new PropertiesList();
        Arrays.asList(properties)
                .stream()
                .forEach((property) -> propertiesList.getPropertyName().add(property));

        customTypedProperties.setPropertiesList(propertiesList);

        typedProperties.setProperties(customTypedProperties);
        typedPropertiesCollection.getTypedProperties().add(typedProperties);

        log.debug("InfrastructureEjb.constructTypedPropertiesCollectionObject :: Method Returned TypedPropertiesCollection {}", typedPropertiesCollection);

        return typedPropertiesCollection;
    }

    private Map<String, String> getChildrenPropertiesAsList(List<CI> cis) {
        Map<String, String> properties = new HashMap<>();

        cis.remove(0);
        for (CI ci : cis) {
            ci.getProps().getStrProps().getStrProp()
                    .stream()
                    .forEach((strProp -> {
                        String mapProp = properties.get(ci.getType());
                        if (mapProp == null) {
                            properties.put(ci.getType(), strProp.getValue().getValue());
                        } else {
                            String newProp = mapProp.concat(", " + strProp.getValue().getValue());
                            properties.put(ci.getType(), newProp);
                        }
                    }));
        }

        return properties;
    }

    // Export Infra Computers to DB -- Helper Methods
    private void dropInfrastuctureTable(String tableName) throws ApplicationException {
        log.debug("InfrastructureEjb.dropInfrastuctureTable :: Method Entered for Table {}", tableName);

        try {
            if (!tableExists(tableName)) return;

            String sqlStmt = String.format("DROP TABLE %s", tableName);
            em.createNativeQuery(sqlStmt).executeUpdate();
        } catch (Exception ex) {
            throw new ApplicationException("InfrastructureEjb.dropInfrastuctureTable :: Error while Dropping Table", ex);
        }

        log.debug("InfrastructureEjb.dropInfrastuctureTable :: Method Dropped Table");
    }

    private boolean tableExists(String tableName) {
        String sqlCheckStmt = String.format("SELECT TABLE_NAME FROM USER_TABLES WHERE TABLE_NAME ='%s'", tableName);
        boolean tableExists = (em.createNativeQuery(sqlCheckStmt).getResultList().size() != 0);

        return tableExists;
    }

    private void reCreateInfrastructureTable(String tableName, List<String> tableColumns) throws ApplicationException {
        log.debug("InfrastructureEjb.reCreateInfrastructureTable :: Method Entered for Table {}", tableName);

        try {
            StringBuffer sqlCreateStmt = new StringBuffer();
            sqlCreateStmt.append(String.format("CREATE TABLE %s \n(\n", tableName));

            // Append Column Name foreach Column
            tableColumns.forEach(col -> sqlCreateStmt.append(String.format("%s VARCHAR2(1000), \n", col.toUpperCase())));
            sqlCreateStmt.replace(sqlCreateStmt.lastIndexOf(","), sqlCreateStmt.length(), ")");

            em.createNativeQuery(sqlCreateStmt.toString()).executeUpdate();
        } catch (Exception ex) {
            log.error("InfrastructureEjb.reCreateInfrastructureTable :: Error while Creating Table", ex);
        }


        log.debug("InfrastructureEjb.reCreateInfrastructureTable :: Method Created Table {}", tableName);
    }

    private AdvancedCriteria getAllInfrastructureComputersCriteria() {
        log.debug("InfrastructureEjb.getAllInfrastructureComputersCriteria :: Method Entered");
        List<Tenant> allTenants = tenantsRemote
                .getAllTenants()
                .stream()
                // TODO -- Remove after Migration
                .filter(tenant -> IS_NEW_UCMDB_ENV == MIGRATED_TENANTS.contains(tenant.getDescription()))
                .map(tenant -> {
                    Tenant transformedTenant = new Tenant();

                    transformedTenant.setId(String.valueOf(tenant.getId()));
                    transformedTenant.setDescription(tenant.getDescription());

                    return transformedTenant;
                })
                .collect(Collectors.toList());

        AdvancedCriteria criteria = new AdvancedCriteria();

        BusinessSearchCriteria searchCriteria = new BusinessSearchCriteria();
        searchCriteria.setSearchTerm("host_node");
        searchCriteria.setTenants(allTenants);

        criteria.setBusinessCriteria(searchCriteria);
        criteria.setAdvancedTerms(new ArrayList<>());

        log.debug("InfrastructureEjb.getAllInfrastructureComputersCriteria :: Method Returned Tenants {}", allTenants);
        return criteria;
    }

    private void insertComputersDataInTable(String exportTableName, List<String> tableColumns, List<Infrastructure> allComputers) throws ApplicationException {
        log.debug("InfrastructureEjb.insertComputersDataInTable :: Method Entered");

        StringBuilder insertStmtBuilder = new StringBuilder();
        insertStmtBuilder.append(String.format("INSERT INTO %s(", exportTableName));
        tableColumns.forEach(col -> insertStmtBuilder.append(String.format("%s, ", col.toUpperCase())));

        insertStmtBuilder.replace(insertStmtBuilder.lastIndexOf(","), insertStmtBuilder.length(), ")");
        insertStmtBuilder.append("\n VALUES (");

        try {
            allComputers.forEach(computer -> {
                final StringBuilder insertStmt = new StringBuilder();
                insertStmt.append(insertStmtBuilder.toString());

                tableColumns.forEach(propName -> {
                    String propValue = computer.getProperties().get(propName);

                    if (propValue != null) {
                        propValue = propValue.replaceAll("'", "''");
                        insertStmt.append(String.format("'%s', ", propValue));
                    }
                    else {
                        insertStmt.append(String.format("'%s', ",""));
                    }
                });
                insertStmt.replace(insertStmt.lastIndexOf(","), insertStmt.length(), ")");

                em.createNativeQuery(insertStmt.toString()).executeUpdate();
            });
        }
        catch (Exception ex) {
            throw new ApplicationException("InfrastructureEjb.insertComputersDataInTable :: Error", ex);
        }

        log.debug("InfrastructureEjb.insertComputersDataInTable :: Method Returned");
    }
}
