package gr.cosmote.ucmdb.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.transaction.Transactional;
import javax.xml.ws.Holder;

import gr.cosmote.ucmdb.client.UCMDBCallHelper;
import gr.cosmote.ucmdb.commons.exceptions.ApplicationException;
import gr.cosmote.ucmdb.models.database.Tenants;
import gr.cosmote.ucmdb.ucmdbWS.CI;
import gr.cosmote.ucmdb.ucmdbWS.CIs;
import gr.cosmote.ucmdb.ucmdbWS.CustomProperties;
import gr.cosmote.ucmdb.ucmdbWS.PropertiesList;
import lombok.extern.slf4j.Slf4j;

@Stateless
@Remote(TenantRemote.class)
@Slf4j
public class TenantEjb {
    @PersistenceContext(unitName = "ucmdbPU", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    @Resource
    private SessionContext sessionContext;

    public List<Tenants> getTenantsFromUcmdb() {
        log.debug("TenantEjb :: getTenantsFromUcmdb :: method entered");

        CustomProperties cp = new CustomProperties();
        PropertiesList pl = new PropertiesList();
        pl.getPropertyName().add("TenantOwner");
        pl.getPropertyName().add("display_label");
        cp.setPropertiesList(pl);
        Holder<CIs> cIsHolder = UCMDBCallHelper.getCIsByType("business_service", cp, null);
        log.debug("TenantEjb :: getTenantsFromUcmdb :: got {} CIs from {}", cIsHolder.value.getCI().size(), "business_service");

        Collection<Tenants> tenanats = new ArrayList<>();

        for (CI ci : cIsHolder.value.getCI()) {
            Tenants t = new Tenants();
            t.setDescription(ci.getProps().getStrProps().getStrProp().get(0).getValue().getName().equals("TenantOwner") ?
                    ci.getProps().getStrProps().getStrProp().get(0).getValue().getValue() :
                    ci.getProps().getStrProps().getStrProp().get(1).getValue().getValue());
            tenanats.add(t);
        }

        List<Tenants> distinctTenants =
                tenanats.stream()
                        .distinct()
                        .filter(tenant -> !("System Default Tenant".equals(tenant.getDescription())))
                        .collect(Collectors.toList());

        log.debug("TenantEjb :: getTenantsFromUCMDB :: Tenants: {}", distinctTenants);

        log.debug("TenantEjb :: getTenantsFromUcmdb :: method returning {}", distinctTenants);

        ucmdbInsertUcmdbTenants(distinctTenants);

        return distinctTenants;
    }

    private Tenants checkIfTenantExists(Tenants tenant) {
        log.debug("TenantEjb.checkIfTenantExists :: Method Entered for Tenant {}", tenant.getDescription());
        List<Tenants> databaseTenants = this.getAllTenants();

        Tenants existringDbTenant =
                databaseTenants.stream().filter(t ->
                t.getDescription().equals(tenant.getDescription())).findFirst().orElse(null);


        log.debug("TenantEjb.checkIfTenantExists :: Method Returned {}", existringDbTenant);
        return existringDbTenant;
    }

    public Tenants ucmdbInsertTenant(Tenants tenant) {
        log.debug("TenantEjb :: ucmdbInsertTenant :: method entered with {} ", tenant);

        Tenants existingTenant = this.checkIfTenantExists(tenant);
        if (existingTenant == null) {
            log.debug("TenantEjb :: ucmdbInsertTenant :: found new tenant", tenant);
            Tenants newTenant = new Tenants();
            newTenant.setDescription(tenant.getDescription());

            persistTenantInDb(newTenant);
        }

        return existingTenant;
    }

    private void persistTenantInDb(Tenants existingTenant) {
        log.debug("TenantEjb.persistTenantInDb :: Method Entered for Tenant", existingTenant);
        try {
            entityManager.persist(existingTenant);
        }
        catch (Exception ex) {
            log.error("TenantEjb.persistTenantInDb :: Error while Persisting Tenant", ex);
        }
    }

    public List<Tenants> ucmdbInsertUcmdbTenants(List<Tenants> tenants) {

        log.debug("TenantEjb :: ucmdbInsertUcmdbTenants :: method entered ");

        for (Tenants t : tenants) {
            Tenants tmpTenant = this.ucmdbInsertTenant(t);
//            t.setId(tmpTenant.getId());
        }

        return tenants;
    }

    public List<Tenants> getAllTenants() {
        List<Tenants> tenants;

        log.debug("TenantEjb :: getAllTenants :: method entered");

        try {
            tenants = entityManager.createNativeQuery(GET_TENANTS_SQL, Tenants.class).getResultList();

        } catch (NoResultException noResultException) {
            tenants = null;
            log.error("TenantEjb :: getAllTenants :: ERROR {}", noResultException);
        }

        log.debug("TenantEjb :: getAllTenants :: method returning {}", tenants);

        return tenants;
    }

    public void updateTenant(Tenants tenant) throws ApplicationException {
        log.debug("TenantEjb.updateTenant :: Method Entered");

        try {
            entityManager.merge(tenant);
        }
        catch (Exception ex) {
            throw new ApplicationException("TenantEjb.updateTenant :: Error", ex);
        }

        log.debug("TenantEjb.updateTenant :: Method Returned");
    }

    private static final String GET_TENANTS_SQL =
            "SELECT T.TENANT_ID, T.DESCRIPTION FROM TENANTS T";

}
