package gr.cosmote.ucmdb.ejb;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import gr.cosmote.ucmdb.client.UCMDBCallHelper;
import gr.cosmote.ucmdb.commons.exceptions.ApplicationException;
import gr.cosmote.ucmdb.model.TopologyMapResponse;
import gr.cosmote.ucmdb.models.base.GenericCI;
import gr.cosmote.ucmdb.models.database.ViewQueries;
import gr.cosmote.ucmdb.models.database.ViewQueriesProperties;
import gr.cosmote.ucmdb.models.view.savedsearches.InfrastructureOptionDTO;
import gr.cosmote.ucmdb.models.view.viewquery.ViewQueryRelatedCI;
import gr.cosmote.ucmdb.models.view.viewquery.ViewQueryRequestDTO;
import gr.cosmote.ucmdb.models.view.viewquery.ViewQueryResponseDTO;
import gr.cosmote.ucmdb.ucmdbWS.*;
import lombok.extern.slf4j.Slf4j;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Stateless
@Slf4j
public class ViewQueriesEjb implements ViewQueriesRemote {
    @PersistenceContext
    private EntityManager em;

    private static LoadingCache<ViewQueryRequestDTO, List<ViewQueryResponseDTO>> QUERY_CACHE_LOADER;

    public List<ViewQueries> getViewQueries() {
        log.debug("ViewQueriesEjb.getViewQueries :: Method Entered");

        List<ViewQueries> viewQueries = em.createNamedQuery("Queries.getAll", ViewQueries.class).getResultList();

        log.debug("ViewQueriesEjb.getViewQueries :: Method Returned View Queries size of {}", viewQueries.size());
        return viewQueries;
    }

    public List<ViewQueriesProperties> getViewQueryProperties(String queryName) {
        log.debug("ViewQueriesEjb.getViewQueryProperties :: Method Entered with Query Name {}", queryName);
        List<ViewQueriesProperties> viewQueryProperties;

        ViewQueries query = em.createNamedQuery("Queries.getByName", ViewQueries.class).setParameter("queryName", queryName).getSingleResult();
        viewQueryProperties = query.getViewQueriesProperties();

        log.debug("ViewQueriesEjb.getViewQueryProperties :: Method Returned View Query Properties size of {}", viewQueryProperties.size());
        return viewQueryProperties;
    }

    public void clearCache() {
        log.debug("ViewQueriesEjb.clearCache :: Method Entered");

        if (QUERY_CACHE_LOADER != null) {
            QUERY_CACHE_LOADER.invalidateAll();
        } else {
            throw new ApplicationException("ViewQueriesEjb.clearCache :: Cache is Null");
        }

        log.debug("ViewQueriesEjb.clearCache :: Method Cleared Cache");
    }

    @TransactionAttribute(TransactionAttributeType.NEVER)
    public List<ViewQueryResponseDTO> executeTopologyQuery(ViewQueryRequestDTO viewQueryRequest) throws ApplicationException {
        String queryName = viewQueryRequest.getQueryName();
        log.debug("ViewQueriesEjb.executeTopologyQuery :: Method Entered with Name {}", queryName);

        if (QUERY_CACHE_LOADER == null) {
            QUERY_CACHE_LOADER = CacheBuilder
                    .newBuilder()
                    .expireAfterAccess(1, TimeUnit.HOURS)
                    .build(new CacheLoader<ViewQueryRequestDTO, List<ViewQueryResponseDTO>>() {
                        @Override
                        public List<ViewQueryResponseDTO> load(ViewQueryRequestDTO key) throws ApplicationException {
                            List<ViewQueryResponseDTO> queryResponseDTO;

                            try {
                                TypedPropertiesCollection properties = initTypedPropertiesBasedOnQuery(queryName);
                                TopologyMapResponse queryResponse = UCMDBCallHelper.getTopologyMap(queryName, properties);

                                if (responseIsChunked(queryResponse)) {
                                    TopologyMap chunkedQueryResponse = UCMDBCallHelper.retrieveResultsForAllChunks(queryResponse.getHolder(), queryResponse.getChunkHolder(), properties).value;

                                    queryResponseDTO = manageAndEditQueryResponse(queryName, chunkedQueryResponse);
                                } else {
                                    queryResponseDTO = manageAndEditQueryResponse(queryName, queryResponse.getHolder().value);
                                }

                                queryResponseDTO = filterQueryResponse(queryResponseDTO, key);
                            } catch (Exception ex) {
                                throw new ApplicationException("ViewQueriesEjb.executeTopologyQuery :: ERROR", ex);
                            }

                            log.debug("ViewQueriesEjb.executeTopologyQuery :: Method Returned Query Result");
                            return queryResponseDTO;
                        }
                    });
        }

        return returnFromCache(viewQueryRequest);
    }

    private List<ViewQueryResponseDTO> returnFromCache(ViewQueryRequestDTO viewQueryRequest) {
        log.debug("ViewQueriesEjb.returnFromCache :: Response for Query Request {} is Cached", viewQueryRequest);

        try {
            return QUERY_CACHE_LOADER.get(viewQueryRequest);
        } catch (ExecutionException e) {
            log.debug("ViewQueriesEjb.returnFromCache :: ERROR {}", e);
            return null;
        }
    }

    private TypedPropertiesCollection initTypedPropertiesBasedOnQuery(String queryName) {
        ViewQueries query = em.createNamedQuery("Queries.getByName", ViewQueries.class).setParameter("queryName", queryName).getSingleResult();

        List<String> queryProps = query.getViewQueriesProperties().stream().map(prop -> prop.getPropertyName()).collect(Collectors.toList());

        TypedPropertiesCollection typedPropertiesCollection = new TypedPropertiesCollection();

        PropertiesList propertiesList = new PropertiesList();
        propertiesList.getPropertyName().addAll(queryProps);

        CustomTypedProperties customProperties = new CustomTypedProperties();
        customProperties.setPropertiesList(propertiesList);

        TypedProperties typedProperties = new TypedProperties();
        typedProperties.setType("object");
        typedProperties.setProperties(customProperties);

        typedPropertiesCollection.getTypedProperties().add(typedProperties);

        return typedPropertiesCollection;
    }

    private List<ViewQueryResponseDTO> manageAndEditQueryResponse(String queryName, TopologyMap queryResponse) {
        List<ViewQueryResponseDTO> queryResponseObject;

        CINodes ciNodes = queryResponse.getCINodes();
        RelationNodes relationNodes = queryResponse.getRelationNodes();

        if (queryName.toLowerCase().contains("hypervisors")) {
            queryResponseObject = manageAndEditHypervisorsQuery(ciNodes, relationNodes);
        } else {
            queryResponseObject = null;
        }

        return queryResponseObject;
    }

    private List<ViewQueryResponseDTO> filterQueryResponse(List<ViewQueryResponseDTO> queryResponseDTO, ViewQueryRequestDTO viewQueryRequest) {
        List<ViewQueryResponseDTO> filteredQueryResult;

        List<ViewQueryResponseDTO> filteredByTenants = filterResultsBasedOnSelectedTenants(queryResponseDTO, viewQueryRequest.getSelectedTenants());
        filteredQueryResult = filterResultsBasedOnFilters(filteredByTenants, viewQueryRequest.getQueryFilters());

        return filteredQueryResult;
    }

    private List<ViewQueryResponseDTO> filterResultsBasedOnSelectedTenants(List<ViewQueryResponseDTO> queryResponseDTO, List<String> selectedTenants) {
        log.debug("ViewQueriesEjb.filterResultsBasedOnSelectedTenants :: Method Entered with unfiltered result size of {} & # of selected Tenants {}",
                queryResponseDTO.size(), selectedTenants.size());
        List<ViewQueryResponseDTO> filteredResult;

        if (selectedTenants != null && !selectedTenants.isEmpty()) {
            filteredResult =
                    queryResponseDTO
                            .stream()
                            .filter(q -> selectedTenants.stream().anyMatch(tenant -> tenant.equals(q.getCi().getProperties().get("TenantOwner"))))
                    .collect(Collectors.toList());
        } else {
            filteredResult = queryResponseDTO;
        }

        return filteredResult;
    }

    private List<ViewQueryResponseDTO> filterResultsBasedOnFilters(List<ViewQueryResponseDTO> queryResponseDTO, List<InfrastructureOptionDTO> queryFilters) {
        log.debug("ViewQueriesEjb.filterResultsBasedOnFilters :: Method Entered with unfiltered result size of {} & filters {}", queryResponseDTO.size(), queryFilters);
        List<ViewQueryResponseDTO> filteredResult;

        if (queryFilters.isEmpty()) {
            filteredResult = queryResponseDTO;
        } else {
            filteredResult =
                    queryResponseDTO
                            .stream()
                            .filter(q ->
                                    queryFilters
                                            .stream()
                                            .anyMatch(userFilter -> {
                                                String value = q.getCi().getProperties().get(userFilter.getAttributeName());
                                                return value != null && value.matches(transformValueIfContainsWildCard(userFilter.getAttributeValue()));
                                            })
                            )
                            .collect(Collectors.toList());
        }

        log.debug("ViewQueriesEjb.filterResultsBasedOnFilters :: Method Returned filtered result size of {}", filteredResult.size());
        return filteredResult;
    }

    private String transformValueIfContainsWildCard(String option) {
        StringBuffer sb = new StringBuffer(option);

        int allWildcardPosition = sb.indexOf("*");
        while (allWildcardPosition != -1) {
            sb.replace(allWildcardPosition, allWildcardPosition + 1, "(.*)");

            allWildcardPosition = sb.indexOf("*", allWildcardPosition + 3);
        }

        int aSymbolWildcardPosition = sb.indexOf("%");
        while (aSymbolWildcardPosition != -1) {
            sb.replace(aSymbolWildcardPosition, aSymbolWildcardPosition + 1, "(.?)");

            aSymbolWildcardPosition = sb.indexOf("%", aSymbolWildcardPosition + 3);
        }

        return sb.toString();
    }

    private List<ViewQueryResponseDTO> manageAndEditHypervisorsQuery(CINodes ciNodes, RelationNodes relationNodes) {
        List<CI> hostNodes = getCIsBasedOnLabel("Host", ciNodes);
        List<CI> hypervisors = getCIsBasedOnLabel("Hypervisor", ciNodes);
        List<CI> guestNodes = getCIsBasedOnLabel("Guest", ciNodes);
        List<CI> businessServices = getCIsBasedOnLabel("BusinessService", ciNodes);

        List<ViewQueryResponseDTO> hostsWithGuests =
                hostNodes
                        .stream()
                        .map(host -> {
                            GenericCI hostGenericCI = new GenericCI();
                            hostGenericCI.setId(host.getID().getValue());
                            hostGenericCI.setType(host.getType());
                            hostGenericCI.setProperties(UCMDBCallHelper.getProperties(host.getID().getValue(), hostNodes));

                            List<ViewQueryRelatedCI> host_GuestNodes = collectGuestNodesBusinessServices(host.getID().getValue(), hypervisors, guestNodes, businessServices, relationNodes.getRelationNode());

                            ViewQueryResponseDTO viewQueryResponseDTO = new ViewQueryResponseDTO();
                            viewQueryResponseDTO.setCi(hostGenericCI);
                            viewQueryResponseDTO.setRelationCIs(host_GuestNodes);

                            return viewQueryResponseDTO;
                        })
                        .collect(Collectors.toList());

        return hostsWithGuests;
    }

    private List<CI> getCIsBasedOnLabel(String label, CINodes ciNodes) {
        return ciNodes.getCINode()
                .stream()
                .filter(ciNode -> ciNode.getLabel().equals(label))
                .map(ciNode -> ciNode.getCIs().getCI())
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    private List<ViewQueryRelatedCI> collectGuestNodesBusinessServices(String hostNodeId, List<CI> hypervisors, List<CI> guestNodes, List<CI> businessServices, List<RelationNode> relationsNodes) {
        List<ViewQueryRelatedCI> guestNodesList = new ArrayList<>();

        // Get Guests with Direct Relationship (like HP Complex)
        List<ViewQueryRelatedCI> directGuests =
                relationsNodes
                        .stream()
                        .filter(relationNode -> "Membership".equals(relationNode.getLabel()))
                        .map(relationNode ->
                                relationNode
                                        .getRelations()
                                        .getRelation()
                                        .stream()
                                        .filter(rel -> rel.getEnd1ID().getValue().equals(hostNodeId))
                                        .map(rel -> {
                                            CI guestCI = guestNodes.stream().filter(ci -> ci.getID().getValue().equals(rel.getEnd2ID().getValue())).findFirst().orElse(null);

                                            ViewQueryRelatedCI relatedGuest = convertCIToViewQueryRelatedCI(guestCI, getGuestRelatedBusinessService(guestCI, businessServices, relationsNodes));
                                            return relatedGuest;
                                        })
                                        .filter(guest -> guest != null)
                                        .collect(Collectors.toList())
                        )
                        .flatMap(List::stream)
                        .collect(Collectors.toList());

        // Get Guests with Hypervisor Relation to Node
        List<CI> nodesHypervisorsRelation = getNodeHypervisorsRelation(hostNodeId, hypervisors, relationsNodes);
        List<ViewQueryRelatedCI> hypervisorGuests = new ArrayList<>();

        nodesHypervisorsRelation
                .stream()
                .forEach(hypervisor -> hypervisorGuests.addAll(getGuestNodesFromHypervisors(hypervisor.getID().getValue(), guestNodes, businessServices, relationsNodes)));

        guestNodesList.addAll(directGuests);
        guestNodesList.addAll(hypervisorGuests);
        return guestNodesList;
    }

    private List<CI> getNodeHypervisorsRelation(String hostNodeId, List<CI> hypervisors, List<RelationNode> relationNodes) {
        List<CI> nodesHypervisorsRel =
                relationNodes
                        .stream()
                        .filter(relationNode -> "Composition".equals(relationNode.getLabel()))
                        .map(relationNode ->
                                relationNode
                                        .getRelations()
                                        .getRelation()
                                        .stream()
                                        .filter(rel -> rel.getEnd1ID().getValue().equals(hostNodeId))
                                        .map(rel -> hypervisors.stream().filter(hyper -> hyper.getID().getValue().equals(rel.getEnd2ID().getValue())).findFirst().orElse(null))
                                        .filter(nodeHyper -> nodeHyper != null)
                                        .collect(Collectors.toList())
                        )
                        .flatMap(List::stream)
                        .collect(Collectors.toList());

        return nodesHypervisorsRel;
    }

    private List<ViewQueryRelatedCI> getGuestNodesFromHypervisors(String hypervisorId, List<CI> guestNodes, List<CI> businessServices, List<RelationNode> relationsNodes) {
        return
                relationsNodes
                        .stream()
                        .filter(relationNode -> "ExecutionEnvironment".equals(relationNode.getLabel()))
                        .map(relationNode ->
                                relationNode
                                        .getRelations()
                                        .getRelation()
                                        .stream()
                                        .filter(rel -> rel.getEnd1ID().getValue().equals(hypervisorId))
                                        .map(rel -> {
                                            CI guestCI = guestNodes.stream().filter(ci -> ci.getID().getValue().equals(rel.getEnd2ID().getValue())).findFirst().orElse(null);

                                            ViewQueryRelatedCI relatedGuest = convertCIToViewQueryRelatedCI(guestCI, getGuestRelatedBusinessService(guestCI, businessServices, relationsNodes));
                                            return relatedGuest;
                                        })
                                        .filter(guest -> guest != null)
                                        .collect(Collectors.toList())
                        )
                        .flatMap(List::stream)
                        .collect(Collectors.toList());
    }

    private List<String> getGuestRelatedBusinessService(CI guestCI, List<CI> businessServices, List<RelationNode> relationsNodes) {
        // Find Related Business Service with Guest CI
        List<String> guestBusinessServiceContainment = relationsNodes
                .stream()
                .filter(relationNode -> "Virtual - Compound".equals(relationNode.getLabel()))
                .map(relationNode ->
                        relationNode.getRelations().getRelation()
                                .stream()
                                .filter(rel -> rel.getEnd2ID().getValue().equals(guestCI.getID().getValue()))
                                .map(rel -> {
                                    CI businessService = businessServices.stream().filter(bs -> bs.getID().getValue().equals(rel.getEnd1ID().getValue())).findFirst().orElse(null);

                                    return getBusinessServiceDisplayLabel(businessService);
                                })
                                .collect(Collectors.toList())
                )
                .flatMap(List::stream)
                .collect(Collectors.toList());

        return guestBusinessServiceContainment;
    }

    private String getBusinessServiceDisplayLabel(CI bs) {
        String businessServiceDisplayLabel;
        if (bs == null) {
            businessServiceDisplayLabel = null;
        } else {
            businessServiceDisplayLabel = bs.getProps().getStrProps().getStrProp()
                    .stream()
                    .filter(prop -> prop.getName().equals("display_label"))
                    .map(prop -> prop.getValue().getValue())
                    .findFirst().orElse(null);
        }

        return businessServiceDisplayLabel;
    }

    private ViewQueryRelatedCI convertCIToViewQueryRelatedCI(CI guestCI, List<String> businessService) {
        ViewQueryRelatedCI relatedGuest = null;

        if (guestCI != null) {
            GenericCI guest = new GenericCI();
            guest.setId(guestCI.getID().getValue());
            guest.setType(guestCI.getType());
            guest.setProperties(UCMDBCallHelper.getProperties(guestCI.getID().getValue(), Arrays.asList(guestCI)));

            relatedGuest = new ViewQueryRelatedCI();
            // TODO -- Add BusinessService Name for Guest
            relatedGuest.setBusinessServiceDisplayLabel(businessService);
            relatedGuest.setRelatedCiProperties(guest);
        }

        return relatedGuest;
    }

    private boolean responseIsChunked(TopologyMapResponse queryResponse) {
        return (queryResponse.getChunkHolder().value.getNumberOfChunks() > 0);
    }
}
