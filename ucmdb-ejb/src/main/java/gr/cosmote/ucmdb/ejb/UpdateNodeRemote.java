package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.models.base.CIAttribute;
import gr.cosmote.ucmdb.models.database.Task;
import gr.cosmote.ucmdb.models.view.TaskEditDTO;

import javax.ejb.Remote;
import java.io.Serializable;
import java.util.List;

@Remote
public interface UpdateNodeRemote extends Serializable {
    boolean updateUcmdb(String globalId, List<CIAttribute> attributes) throws gr.cosmote.ucmdb.commons.exceptions.ApplicationException;
}
