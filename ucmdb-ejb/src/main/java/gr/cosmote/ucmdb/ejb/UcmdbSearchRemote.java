package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.models.database.SearchType;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface UcmdbSearchRemote {
    List<SearchType> getSearchTypes();
}
