package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.models.database.UcmdbCisComment;
import gr.cosmote.ucmdb.models.database.UcmdbMissingNode;
import gr.cosmote.ucmdb.models.view.UserCommentDTO;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface UserActionsRemote {
    UcmdbCisComment         addNewCiComment(String ciDiplayLabel, String tenantOwner, String globalId, String newComment);
    List<UserCommentDTO>    getUserComments();

    UcmdbMissingNode        addMissingNode(UcmdbMissingNode missingNode);
    List<UcmdbMissingNode>  getUcmdbMissingNodes();
}
