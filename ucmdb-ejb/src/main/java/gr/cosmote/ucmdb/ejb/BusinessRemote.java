package gr.cosmote.ucmdb.ejb;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import gr.cosmote.ucmdb.models.base.*;
import gr.cosmote.ucmdb.models.database.CiAttributeConfiguration;
import gr.cosmote.ucmdb.models.view.BusinessResultDTO;
import gr.cosmote.ucmdb.models.view.BusinessCiCollectionRequestDTO;
import gr.cosmote.ucmdb.ucmdbWS.CI;
import gr.cosmote.ucmdb.ucmdbWS.TypedPropertiesCollection;

@Remote
public interface BusinessRemote extends Serializable{
    BusinessResultDTO retrieveBusinessServices(BusinessSearchCriteria criteria);
    List<GenericCI> retrieveCiCollectionsOfBusinessService(BusinessCiCollectionRequestDTO requestObject);
    List<GenericCI> expandCiCollection(BusinessCiCollectionRequestDTO requestObject);
    List<GenericCI> getBusinessApplicationOrHostNodes(BusinessCiCollectionRequestDTO requestObject);

    BusinessResult getBusinessService(ExcelCriteria criteria);
    List<GenericCI> getCINeighboursOfType(CIsOfType cisOfType);
    Map<String, String> getMenuItems(List<String> IDs);
    List<String> getClassAttributes(String className);
    List<String> getNodeClassAttributes(String className);
    TypedPropertiesCollection getTypedPropertiesCollection(List<String> properties);
}