package gr.cosmote.ucmdb.ejb;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import gr.cosmote.ucmdb.client.UCMDBCallHelper;
import gr.cosmote.ucmdb.models.base.CIAttribute;
import gr.cosmote.ucmdb.models.base.GenericCI;
import gr.cosmote.ucmdb.models.database.*;
import gr.cosmote.ucmdb.models.enumeration.NodeStatusEnum;
import gr.cosmote.ucmdb.models.view.TaskAttributeDTO;
import gr.cosmote.ucmdb.models.view.UpdateCiDTO;
import gr.cosmote.ucmdb.models.view.cidetails.CiDetails;
import gr.cosmote.ucmdb.ucmdbWS.*;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.persistence.*;
import javax.xml.ws.Holder;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Stateless
@Remote(CIRemote.class)
@Slf4j
public class CIEjb implements CIRemote {

    private static final String GET_UCMDB_INFO_WINDOW_GROUPS = "SELECT Group FROM UcmdbInfoWindowGroups Group";
    private static final String GET_NODES_BY_GLOBAL_ID_AND_STATUS = "SELECT Node FROM Node Node WHERE Node.globalId=:id AND Node.status=:status";
    private static final String GET_CUSTOM_ATTRIBUTES_OF_CI = "SELECT CiAttribute FROM CiAttributeConfiguration CiAttribute WHERE CiAttribute.ciType=:type AND CiAttribute.isWorkflowRelated=true";
    private static final String GET_ATTRIBUTE_LOV = "SELECT AttributeLov FROM CiAttributeLov AttributeLov WHERE AttributeLov.type=:name AND AttributeLov.ciType=:ciType";

    private static LoadingCache<String, String> PARENT_CLASS_CACHE = null;

    @EJB
    private BusinessRemote businessRemoteEjb;
    @EJB
    private UpdateNodeRemote updateNodeRemoteEjb;
    @EJB
    private GroupRemote groupEjb;

    @PersistenceContext(unitName = "ucmdbPU", type = PersistenceContextType.TRANSACTION)
    EntityManager entityManager;

    @Resource
    private SessionContext sessionContext;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public CiDetails getAttributesByID(String id, String[] properties) {
        log.debug(" CIEjb ::getAttributesById :: Method Entered method with id {} & properties {}", id, properties);
        CI ucmdbCI;

        try {
            List<String> attributes =
                    properties != null ? Arrays.asList(properties) : businessRemoteEjb.getClassAttributes(callUcmdbToGetCiType(id));

            // Construct Request Object
            IDs Ids = new IDs();
            ID ciID = new ID();
            ciID.setValue(id);
            Ids.getID().add(ciID);

            // Construct Properties Object
            TypedPropertiesCollection typedPropertiesCollection = businessRemoteEjb.getTypedPropertiesCollection(attributes);
            ucmdbCI = UCMDBCallHelper.getCIsById(typedPropertiesCollection, Ids).value.getCI().get(0);
        } catch (Exception ex) {
            throw new gr.cosmote.ucmdb.commons.exceptions.ApplicationException("CIEjb :: getAttributesByID :: ERROR in Method");
        }

        log.debug(" CIEjb :: getAttributesById :: Method Returned Properties for CI");
        return transformCiToCiDetails(ucmdbCI);
    }

    public Boolean isCiTypeOfNode(String className) {
        log.debug("CIEjb.isCiTypeOfNode :: Method Entered");

        Boolean isCiTypeOfNode = false;
        try {
            Holder<UcmdbClassModelHierarchy> classModelHierarchyHolder = UCMDBCallHelper.getClassAncestors(className);
            List<UcmdbClassHierarchyNode> ucmdbClassHierarchyNodeList = classModelHierarchyHolder.value.getClassHierarchyNode();

            isCiTypeOfNode =
                    ucmdbClassHierarchyNodeList
                            .stream()
                            .filter(ucmdbClass -> "host_node".equals(ucmdbClass.getClassNames().getClassName()))
                            .count() != 0;
        } catch (Exception ex) {
            log.error("CIEjb.isCiTypeOfNode :: Error", ex);
        }

        log.debug("CIEjb.isCiTypeOfNode :: Method Returned");
        return isCiTypeOfNode;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public Boolean isCiTypeAppropriateForUpdate(String className) {
        log.debug("CIEjb :: isCiTypeAppropriateForUpdate :: Method Entered for Class {}", className);
        boolean isCiTypeUpdatable;

        List<String> updatableCiTypes = entityManager.createNamedQuery("UpdatableCiType.getUpdatableCiTypes", String.class).getResultList();

        if (updatableCiTypes.contains(className)) {
            isCiTypeUpdatable = true;
        } else {
            isCiTypeUpdatable = (getCiParentClass(className) != null);
        }

        log.debug("CIEjb :: isCiTypeAppropriateForUpdate :: {} is type of Node: {}", className, isCiTypeUpdatable);

        return isCiTypeUpdatable;
    }

    public Boolean isCiUpdatable(String globalId) {
        log.debug("CIEjb :: isCiUpdatable :: Method Entered for CI with Global Id {}", globalId);

        Boolean isUpdatable;
        try {
            long openNodes =
                    entityManager.createQuery(GET_NODES_BY_GLOBAL_ID_AND_STATUS, Node.class)
                            .setParameter("id", globalId)
                            .setParameter("status", NodeStatusEnum.OPEN)
                            .getResultList()
                            .stream()
                            .count();

            isUpdatable = (openNodes == 0);
        } catch (Exception ex) {
            log.error("CIEjb :: isCiUpdatable :: ERROR {}", ex);
            return false;
        }

        return isUpdatable;
    }

    public List<CIAttribute> getCiCustomAttributes(String ciType, String tenant) {
        log.debug("CIEjb :: getCiCustomAttributes :: Method Entered for CiType {} & Tenant {}", ciType, tenant);
        ciType = getCiParentClass(ciType);

        List<CiAttributeConfiguration> ciAttributes =
                entityManager.createQuery(GET_CUSTOM_ATTRIBUTES_OF_CI, CiAttributeConfiguration.class)
                        .setParameter("type", ciType)
                        .getResultList();
//                        .stream().filter(attr -> attr.isUpdatable())
//                        .collect(Collectors.toList());

        List<CIAttribute> ciCustomAttributes = transformDatabaseDataToCiAttributes(ciAttributes, ciType, tenant);

        log.debug("CIEjb :: getCiCustomAttributes :: Method Returned Ci Custom Attributes", ciCustomAttributes);

        return ciCustomAttributes;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public GenericCI updateCiInUcmdb(UpdateCiDTO ci) {
        log.debug("CIEjb :: updateCiInUcmdb :: Method Entered for Ci {}", ci);

        boolean ucmdbSuccessResponse;
        try {
            ucmdbSuccessResponse = updateNodeRemoteEjb.updateUcmdb(ci.getGlobalId(), ci.getUpdatedAttributes());
        } catch (gr.cosmote.ucmdb.commons.exceptions.ApplicationException ex) {
            log.error("CIEjb :: updateCiInUcmdb :: ERROR in Method {}", ex);
            ucmdbSuccessResponse = false;
        }

        doActionsBasedOnResponse(ucmdbSuccessResponse, ci);
        return generateResponseForUpdateCi(ci.getGlobalId(), ucmdbSuccessResponse);
    }

    private void doActionsBasedOnResponse(boolean ucmdbSuccessResponse, UpdateCiDTO ci) {
        if (ucmdbSuccessResponse) {
            log.debug("CIEjb.doActionsBasedOnResponse :: Method Successfully Updated uCMDB");
            generateAuditingReport(ci);
        } else {
            log.debug("CIEjb.doActionsBasedOnResponse :: uCMDB Update Failed Probably for Validation Reasons");
        }
    }

    private String callUcmdbToGetCiType(String globalId) {
        // Construct Request Object
        IDs Ids = new IDs();
        ID ciID = new ID();
        ciID.setValue(globalId);
        Ids.getID().add(ciID);

        String type;
        try {
            // Make Request in order to Retrieve CI Type
            type = UCMDBCallHelper.getCIsById(null, Ids)
                    .value.getCI().get(0).getType();
        } catch (Exception ex) {
            type = null;
            log.error("CIEjb.callUcmdbToGetCiType :: ERROR {}", ex);
        }


        return type;
    }

    private CiDetails transformCiToCiDetails(CI ucmdbCI) {
        // Get CI GlobalId Property
        String globalId = ucmdbCI.getID().getValue();

        // Retrieved Properties Map
        Map<String, String> properties = UCMDBCallHelper.getProperties(globalId, new ArrayList<>(Arrays.asList(ucmdbCI)));

        // Get Predefined Groups from DB
        List<UcmdbInfoWindowGroups> ucmdbInfoWindowGroups = entityManager.createQuery(GET_UCMDB_INFO_WINDOW_GROUPS, UcmdbInfoWindowGroups.class).getResultList();

        CiDetails ciDetails = new CiDetails();
        ciDetails.setGlobalId(globalId);
        ciDetails.setType(ucmdbCI.getType());

        List<UcmdbInfoWindowGroups> ciGroups = ucmdbInfoWindowGroups
                .stream()
                .map(group -> {
                    Map<String, String> groupProperties = new HashMap<>();
                    properties.keySet().forEach(prop -> {
                        if (shouldAddAttribute(ucmdbInfoWindowGroups, group, prop)) {
                            groupProperties.put(prop, properties.get(prop));
                        }
                    });

                    group.setProperties(groupProperties);
                    return group;
                })
                .collect(Collectors.toList());

        ciDetails.setGroups(ciGroups);

        addUserCommentsInCiDetails(ciDetails, properties.get("display_label"), properties.get("TenantOwner"), properties.get("global_id"));

        return ciDetails;
    }

    private boolean shouldAddAttribute(List<UcmdbInfoWindowGroups> infoWindowGroups, UcmdbInfoWindowGroups group, String attributeName) {
        UcmdbInfoWindowGroups assignedGroup =
                infoWindowGroups.stream()
                        .filter(g -> g.getGroupAttributes()
                                .stream()
                                .anyMatch(attr -> attributeName.equals(attr.getAttributeName())))
                        .findFirst().orElse(null);

        return (assignedGroup == null && group.getGroupName().contains("Technical")) || (assignedGroup != null && assignedGroup.getGroupName().equals(group.getGroupName()));
    }


    private void addUserCommentsInCiDetails(CiDetails ciDetails, String displayLabel, String tenantOwner, String globalId) {
        log.debug("CIEjb.addUserCommentsInCommentsGroup :: Method Entered for CI with Display Label {}, Tenant Owner {} & Global Id {}", displayLabel, tenantOwner, globalId);

        // Find Comments Group and Delete the Old
        UcmdbInfoWindowGroups commentsGroup = ciDetails.getGroups().stream().filter(group -> ("User Comments".equals(group.getGroupName()))).findFirst().orElse(null);

        if (commentsGroup != null) {
            ciDetails.getGroups().remove(commentsGroup);
        } else {
            return;
        }

        UcmdbCI ucmdbCI;
        try {
            ucmdbCI = entityManager
                    .createNamedQuery("UcmdbCIs.getCiBasedOnTenantOwnerDispLabelAndGlobId", UcmdbCI.class)
                    .setParameter("dispLabel", displayLabel)
                    .setParameter("tenantOwner", tenantOwner)
                    .setParameter("globalId", globalId)
                    .getSingleResult();

            commentsGroup.setCisComments(ucmdbCI.getCiComments());
        } catch (NoResultException | NonUniqueResultException ex) {
            log.debug("CIEjb.addUserCommentsInCiDetails :: No User Comments found for CI");

            commentsGroup.setCisComments(new ArrayList<>());
        }

        log.debug("CIEjb.addUserCommentsInCommentsGroup :: Method found {} Comments for CI", commentsGroup.getCisComments().size());
        ciDetails.getGroups().add(commentsGroup);
    }

    public String getCiParentClass(String ciType) {
        log.debug("CIEjb :: getCiParentClass :: Method Entered for Class {}", ciType);
        String parentClass = null;

        if (PARENT_CLASS_CACHE == null) {
            log.debug("CIEjb :: getCiParentClass :: Result is not Cached, Retrieving from uCMDB");
            PARENT_CLASS_CACHE = CacheBuilder.newBuilder()
                    .expireAfterWrite(5, TimeUnit.HOURS)
                    .refreshAfterWrite(10, TimeUnit.HOURS)
                    .build(
                            new CacheLoader<String, String>() {
                                @Override
                                public String load(String ciType) throws Exception {
                                    String parentClass = null;

                                    List<String> updatableCiTypes = entityManager.createNamedQuery("UpdatableCiType.getUpdatableCiTypes", String.class).getResultList();
                                    List<UcmdbClassHierarchyNode> classHierarchyNodes;
                                    try {
                                        classHierarchyNodes = UCMDBCallHelper.getClassAncestors(ciType).value.getClassHierarchyNode();

                                        for (UcmdbClassHierarchyNode ucmdbClass : classHierarchyNodes) {
                                            String className = ucmdbClass.getClassNames().getClassName();
                                            if (updatableCiTypes.contains(className)) {
                                                parentClass = className;
                                                break;
                                            }
                                        }
                                    } catch (Exception ex) {
                                        log.error("CIEjb :: getCiParentClass :: ERROR in method {}", ex);

                                        parentClass = null;
                                    }

                                    return parentClass;
                                }
                            });
        }

        parentClass = getCiParentClassFromCache(ciType);

        log.debug("CIEjb :: getCiParentClass :: Parent Class of {} is {}", ciType, parentClass);
        return parentClass;
    }

    private String getCiParentClassFromCache(String ciType) {
        String parentClass = null;
        try {
            parentClass = PARENT_CLASS_CACHE.get(ciType);
        } catch (Exception ex) {
            log.error("CIEjb.getCiParentClassFromCache :: Error", ex);
        }

        return parentClass;
    }

    private void generateAuditingReport(UpdateCiDTO ci) {
        log.debug("CIEjb :: generateAuditingReport :: Method Entered");

        filterChangesOnAttributes(ci);
        NodeDifferences ciDiff = constructNodeDifferencesObject(ci);
        entityManager.persist(ciDiff);

        log.debug("CIEjb :: generateAuditingReport :: Method Generated Auditing Report");
    }

    private NodeDifferences constructNodeDifferencesObject(UpdateCiDTO ci) {
        NodeDifferences ciDiff = new NodeDifferences();
        ciDiff.setGlobalId(ci.getGlobalId());
        ciDiff.setTaskId("SEARCH");
        ciDiff.setNodeId(0);
        ciDiff.setDisplayLabel(ci.getDisplayLabel());
        ciDiff.setOwnerTenant(ci.getTenantOwner());
        ciDiff.setUserGroup(getFirstUserGroup());
        ciDiff.setUpdatedBy(sessionContext.getCallerPrincipal().getName());
        ciDiff.setLastModifiedTime(new Date());
        ciDiff.setUserAction("Updated from Search");

        List<TaskAttributeDTO> beforeUpdateAttributes =
                ci.getBeforeUpdateAttrs().stream()
                        .map((ciAttribute -> {
                            TaskAttributeDTO taskAttributeDTO = new TaskAttributeDTO();
                            taskAttributeDTO.setFieldName(ciAttribute.getFieldName());
                            taskAttributeDTO.setFieldLabel(ciAttribute.getFieldLabel());
                            taskAttributeDTO.setFieldValue(ciAttribute.getFieldValue());

                            return taskAttributeDTO;
                        }))
                        .collect(Collectors.toList());

        List<TaskAttributeDTO> afterUpdateAttributes =
                ci.getUpdatedAttributes().stream()
                        .map((ciAttribute -> {
                            TaskAttributeDTO taskAttributeDTO = new TaskAttributeDTO();
                            taskAttributeDTO.setFieldName(ciAttribute.getFieldName());
                            taskAttributeDTO.setFieldLabel(ciAttribute.getFieldLabel());
                            taskAttributeDTO.setFieldValue(ciAttribute.getFieldValue());

                            return taskAttributeDTO;
                        }))
                        .collect(Collectors.toList());

        ciDiff.setBeforeUpdate(beforeUpdateAttributes);
        ciDiff.setAfterUpdate(afterUpdateAttributes);

        return ciDiff;
    }

    private String getFirstUserGroup() {
        String userGroup;
        try {
            List<Group> userGroups = groupEjb.getUserGroups();
            Group firstUserGroup = userGroups.stream().min(Comparator.comparing(Group::getId)).get();

            userGroup = firstUserGroup.getDescription();
        } catch (Exception ex) {
            userGroup = "-";

            log.error("CIEjb.getFirstUserGroup :: Error while retrieving user group {}", ex);
        }

        return userGroup;
    }

    private void filterChangesOnAttributes(UpdateCiDTO ci) {
        List<CIAttribute> oldAttributes = ci.getBeforeUpdateAttrs();
        List<CIAttribute> newAttributes = ci.getUpdatedAttributes();

        List<String> changedAttributes =
                newAttributes.stream()
                        .filter(newAttribute -> oldAttributes.stream()
                                .anyMatch(oldAttribute -> oldAttribute.getFieldName().equals(newAttribute.getFieldName())
                                        && !oldAttribute.getFieldValue().equals(newAttribute.getFieldValue())))
                        .map(attribute -> attribute.getFieldName())
                        .collect(Collectors.toList());

        List<CIAttribute> filteredOldAttributes =
                oldAttributes.stream()
                        .filter((oldAttribute) -> changedAttributes.stream().anyMatch((attibute) -> attibute.equals(oldAttribute.getFieldName())))
                        .collect(Collectors.toList());

        List<CIAttribute> filteredNewAttributes =
                newAttributes.stream()
                        .filter((newAttribute) -> changedAttributes.stream().anyMatch((attibute) -> attibute.equals(newAttribute.getFieldName())))
                        .collect(Collectors.toList());

        ci.setBeforeUpdateAttrs(filteredOldAttributes);
        ci.setUpdatedAttributes(filteredNewAttributes);
    }

    private List<CIAttribute> transformDatabaseDataToCiAttributes(List<CiAttributeConfiguration> ciAttributes, String ciType, String tenant) {
        List<UcmdbInfoWindowGroups> ucmdbInfoWindowGroups = entityManager.createQuery(GET_UCMDB_INFO_WINDOW_GROUPS, UcmdbInfoWindowGroups.class).getResultList();

        return ciAttributes.stream()
                .map((attribute) -> {
                    CIAttribute ciAttribute = new CIAttribute();

                    Optional<UcmdbInfoWindowGroups> attributeGroupOptional =
                            ucmdbInfoWindowGroups
                                    .stream()
                                    .filter(group -> group.getGroupAttributes().stream().anyMatch(attr -> attr.getAttributeName().equals(attribute.getAttributeName())))
                                    .findFirst();

                    if (attributeGroupOptional.isPresent()) {
                        ciAttribute.setGroupName(attributeGroupOptional.get().getGroupName());
                        ciAttribute.setGroupPriority(attributeGroupOptional.get().getGroupPriority());
                    } else {
                        ciAttribute.setGroupName("Technical Attributes");
                        ciAttribute.setGroupPriority(5);
                    }

                    ciAttribute.setFieldName(attribute.getAttributeName());
                    ciAttribute.setFieldLabel(attribute.getAttributeLabel());
                    ciAttribute.setFieldType(attribute.getType().getText());
                    ciAttribute.setAttributePriority(attribute.getAttributePriority());
                    ciAttribute.setMandatory(attribute.isMandatory());
                    ciAttribute.setCanBeNull(attribute.isCanBeNull());
                    ciAttribute.setEditable(attribute.isUpdatable());

                    boolean hasLovValues = "TEXT_LIST".equals(ciAttribute.getFieldType()) || "LIST_VALUES".equals(ciAttribute.getFieldType());
                    if (hasLovValues) {
                        ciAttribute.setFieldLovs(createAttributeLovs(attribute.getAttributeName(), ciType, tenant));
                    }

                    return ciAttribute;
                })
                .collect(Collectors.toList());
    }

    private List<String> createAttributeLovs(String attributeName, String ciType, String tenant) {
        List<String> ciAttributeLovs = new ArrayList<>();

        String parentCi = getCiParentClass(ciType);
        List<CiAttributeLov> attributeLovs = entityManager.createQuery(GET_ATTRIBUTE_LOV, CiAttributeLov.class)
                .setParameter("name", attributeName)
                .setParameter("ciType", parentCi)
                .getResultList();

        attributeLovs.stream()
                .filter(lov -> lov.getTenant() == null || lov.getTenant().getDescription().equals(tenant))
                .forEach((attributeLov) -> ciAttributeLovs.add(attributeLov.getValue()));

        return ciAttributeLovs;
    }

    private GenericCI generateResponseForUpdateCi(String globalId, boolean updateStatus) {
        GenericCI updateResponse = new GenericCI();

        updateResponse.setId(globalId);
        updateResponse.setType("updateResponse");

        Map<String, String> properties = new HashMap<>();
        properties.put("status", String.valueOf(updateStatus));
        updateResponse.setProperties(properties);

        return updateResponse;
    }
}
