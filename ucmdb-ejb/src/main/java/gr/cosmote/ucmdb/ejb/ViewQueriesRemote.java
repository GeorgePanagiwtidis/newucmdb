package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.commons.exceptions.ApplicationException;
import gr.cosmote.ucmdb.models.database.ViewQueries;
import gr.cosmote.ucmdb.models.database.ViewQueriesProperties;
import gr.cosmote.ucmdb.models.view.viewquery.ViewQueryRequestDTO;
import gr.cosmote.ucmdb.models.view.viewquery.ViewQueryResponseDTO;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface ViewQueriesRemote {
    List<ViewQueries> getViewQueries();
    List<ViewQueriesProperties> getViewQueryProperties(String queryName);
    List<ViewQueryResponseDTO> executeTopologyQuery(ViewQueryRequestDTO viewQueryRequest) throws ApplicationException;
    void clearCache();
}
