package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.models.database.CiAttributeLov;

import javax.ejb.Remote;
import java.io.Serializable;
import java.util.List;

@Remote
public interface CiAttributeLovRemote extends Serializable {
    List<CiAttributeLov> getCiAttributeLovs(String attributeName, String ciType);
    List<CiAttributeLov> getCiAttributeLovs(List<String> attributeNames, String ciType);
    List<CiAttributeLov> getCiAttributeLovsByTypeAndTenant(String ciType, String tenant);
    List<CiAttributeLov> getAllCiAttributeLovs();
}
