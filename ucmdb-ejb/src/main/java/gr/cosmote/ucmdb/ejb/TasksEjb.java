package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.models.database.*;
import gr.cosmote.ucmdb.models.enumeration.InputCommandEnum;
import gr.cosmote.ucmdb.models.enumeration.NodeStatusEnum;
import gr.cosmote.ucmdb.models.enumeration.TaskStatusEnum;
import gr.cosmote.ucmdb.models.view.WorkflowStatsDTO;
import gr.cosmote.ucmdb.service.InputCommandService;
import lombok.extern.slf4j.Slf4j;

import javax.ejb.*;
import javax.persistence.*;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;


@Remote(TasksRemote.class)
@Stateless
@Slf4j
public class TasksEjb {
    private final String className = this.getClass().getName();

    private static final String DELETE_TASK_SUMMARY_RECORDS_BY_NODEID_SQL = "DELETE FROM TASK_SUMMARY TS WHERE TS.NODE_ID = ?";
    private static final String GET_TASK_SUMMARY_RECORDS_BY_GROUP_SQL = "SELECT * FROM TASK_SUMMARY TS WHERE TS.GROUP_ID IN ?";
    private static final String GET_ALL_TASK_SUMMARY_RECORDS_SQL = "SELECT * FROM TASK_SUMMARY TS";
    private static final String GET_TASK_SUMMARY_RECORDS_BY_TASK_ID_SQL = "SELECT * FROM TASK_SUMMARY TS WHERE TS.TASK_ID = ?";
    private static final String GET_TASK_SUMMARY_RECORDS_BY_NODE_ID_SQL = "SELECT * FROM TASK_SUMMARY TS WHERE TS.NODE_ID = ?";


    @PersistenceContext(unitName = "ucmdbPU", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    @EJB
    InputCommandService inputCommandService;

    @EJB
    private RulesEngineServiceRemote rulesEngineService;

    @EJB
    NodeRemote nodeRemote;

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void updateTaskSummaryRecords(Node node) {
        deleteTaskSummaryRecordsForNode(node.getId());
        List<Task> openTasks = filterOpenTasks(node.getTasksWrapper(), node.getId());
        List<TaskSummary> taskSummaryList = createTaskSummaryRecordsForNode(node, openTasks);
        taskSummaryList.forEach(this::saveOrUpdateTaskSummary);
    }

    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public List<TaskSummary> getTaskSummaryRecords(List<Group> groupList) {
        List<TaskSummary> taskSummaryList = new ArrayList<>();

        groupList.forEach(group -> {
            Query query = entityManager.createNativeQuery(GET_TASK_SUMMARY_RECORDS_BY_GROUP_SQL, TaskSummary.class);
            query.setParameter(1, group.getId());
            Optional.ofNullable(query.getResultList()).ifPresent(x -> {
                taskSummaryList.addAll((List<TaskSummary>) x);
            });
        });
        return taskSummaryList;
    }

    @TransactionAttribute(value = TransactionAttributeType.NEVER)
    public List<TaskSummary> getTaskSummaryRecords() {
        List<TaskSummary> taskSummaryList;

        Query query = entityManager.createNativeQuery(GET_ALL_TASK_SUMMARY_RECORDS_SQL, TaskSummary.class);
        taskSummaryList = (List<TaskSummary>) query.getResultList();

        return taskSummaryList;
    }

    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public List<TaskSummary> getTaskSummaryRecordsByNodeId(long nodeId) {
        List<TaskSummary> taskSummaryList;

        Query query = entityManager.createNativeQuery(GET_TASK_SUMMARY_RECORDS_BY_NODE_ID_SQL, TaskSummary.class).setParameter(1, nodeId);
        taskSummaryList = (List<TaskSummary>) query.getResultList();

        return taskSummaryList;
    }

    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public TaskSummary getTaskSummaryRecord(long taskId) {
        TaskSummary taskSummary;

        try {
            Query query = entityManager.createNativeQuery(GET_TASK_SUMMARY_RECORDS_BY_TASK_ID_SQL, TaskSummary.class);
            query.setParameter(1, taskId);
            taskSummary = (TaskSummary) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        return taskSummary;
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRES_NEW)
    public void setTaskAssignee(TaskSummary taskSummary) {
        try {
            entityManager.merge(taskSummary);
            Node node = setTaskOnNode(taskSummary.getNode(), taskSummary.getTask());
            nodeRemote.saveOrUpdateNode(node);

        } catch (Exception e) {
            log.error("TasksEjb.setTaskAssignee() ::: error in method", e);
            throw new gr.cosmote.ucmdb.commons.exceptions.ApplicationException("TasksEjb.setTaskAssignee :: Error in setTaskAssignee method", e);
        }
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRES_NEW)
    public void evaluateTaskAction(Task task, InputCommandEnum inputCommandEnum) {
        log.debug(" TaskEjb.evaluateTaskAction method entered on task {} with command {}", task, inputCommandEnum);

        try {
            inputCommandService.evaluateTask(task, inputCommandEnum);
        } catch (Exception e) {
            throw new gr.cosmote.ucmdb.commons.exceptions.ApplicationException("TasksEjb.evaluateTaskAction :: Error in evaluateTaskAction method", e);
        }
        log.debug(" TasksService.evaluateTaskAction method returned ");
    }

    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public Task reassignTask(Task task, String groupId, String reassignReason) {
        log.debug(" TaskEjb.reassignTask method entered with task {} and for group id {}", task, groupId);

        try {
            Node node = nodeRemote.getNodeBy(task.getId());

            Task newTask = rulesEngineService.reassignTask(task, groupId, reassignReason);

            task.setStatus(TaskStatusEnum.COMPLETED);
            task.setCompletionDate(new Date());

            Node updatedNode = updateNodeAfterReassignTask(node, task, newTask);

            nodeRemote.saveOrUpdateNodeAndTaskSummaryRecords(updatedNode);

            return newTask;
        } catch (Exception e) {
            throw new gr.cosmote.ucmdb.commons.exceptions.ApplicationException("TasksEjb.reassignTask :: Error in evaluateTaskAction method", e);
        }
    }

    private Node updateNodeAfterReassignTask(Node node, Task oldTask, Task newTask) {
        TasksWrapper taskWrapper = node.getTasksWrapper();
        List<Task> updatedTaskList = taskWrapper.getTasks(node.getId()).stream()
                .filter(t -> !(t.getId() == oldTask.getId()))
                .collect(Collectors.toList());

        updatedTaskList.add(newTask);
        updatedTaskList.add(oldTask);
        taskWrapper.setTasks(updatedTaskList);
        try {
            node.setTasksWrapper(taskWrapper);
        } catch (IOException e) {
            throw new gr.cosmote.ucmdb.commons.exceptions.ApplicationException("TaskEjb.updateNodeForReassign :: Error in setting task wrapper.", e);
        }
        return node;
    }

    @TransactionAttribute(value = TransactionAttributeType.NEVER)
    private List<Task> filterOpenTasks(TasksWrapper wrapper, long processId) {
        return wrapper.getTasks(processId).stream()
                .filter(Task::isOpen)
                .collect(Collectors.toList());
    }

    @TransactionAttribute(value = TransactionAttributeType.NEVER)
    private void deleteTaskSummaryRecordsForNode(long nodeId) {
        Query query = entityManager.createNativeQuery(DELETE_TASK_SUMMARY_RECORDS_BY_NODEID_SQL);
        query.setParameter(1, nodeId);
        query.executeUpdate();
    }

    @TransactionAttribute(value = TransactionAttributeType.NEVER)
    private List<TaskSummary> createTaskSummaryRecordsForNode(Node node, List<Task> taskList) {
        List<TaskSummary> taskSummaryList = taskList.stream()
                .map(t -> {
                    TaskSummary taskSummary = new TaskSummary();
                    taskSummary.setNode(node);
                    taskSummary.setTaskId(t.getId());
                    taskSummary.setCiType(node.getCiType());
                    taskSummary.setOwnerTenant(t.findCIAttributeValue("TenantOwner"));
                    taskSummary.setDisplayLabel(t.findCIAttributeValue("display_label"));
                    taskSummary.setGroup(t.getUserGroup());
                    taskSummary.setAssignedTo(t.getCurrentAssignee());
                    taskSummary.setOsType(t.getOsType());

                    try {
                        taskSummary.setTask(t);
                    } catch (IOException e) {
                        log.error("TasksEjb.createTaskSummaryRecordsForNode() ::: error in method", e);
                        throw new gr.cosmote.ucmdb.commons.exceptions.ApplicationException("TasksEjb.createTaskSummaryRecordsForNode :: Error in createTaskSummaryRecordsForNode method", e);
                    }
                    return taskSummary;
                })
                .collect(Collectors.toList());

        return taskSummaryList;
    }

    @TransactionAttribute(value = TransactionAttributeType.NEVER)
    private void saveOrUpdateTaskSummary(TaskSummary taskSummary) {
        try {
            entityManager.merge(taskSummary);
            log.debug("TasksEjb.saveOrUpdateTaskSummary() ::: method merged task summary record {}", taskSummary);
        } catch (Exception e) {
            log.error("TasksEjb.saveOrUpdateTaskSummary() ::: error in method", e);
            throw new gr.cosmote.ucmdb.commons.exceptions.ApplicationException("TasksEjb.saveOrUpdateTaskSummary :: Error in saveOrUpdateTaskSummary method", e);
        }
    }

    private Node setTaskOnNode(Node node, Task task) {
        TasksWrapper tw = node.getTasksWrapper();

        List<Task> tasks = tw.getTasks(node.getId()).stream().map(t -> {
            if (t.getId() == task.getId()) {
                return task;
            }
            return t;
        }).collect(Collectors.toList());
        tw.setTasks(tasks);
        try {
            node.setTasksWrapper(tw);
        } catch (IOException e) {
            throw new gr.cosmote.ucmdb.commons.exceptions.ApplicationException(e);
        }
        return node;
    }
}
