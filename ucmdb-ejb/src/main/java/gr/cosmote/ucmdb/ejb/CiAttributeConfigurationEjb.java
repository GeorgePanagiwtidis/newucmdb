package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.models.database.CiAttributeConfiguration;
import lombok.extern.slf4j.Slf4j;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import java.util.List;

@Stateless
@Remote(CiAttributeConfigurationRemote.class)
@Slf4j
public class CiAttributeConfigurationEjb {
    @PersistenceContext(unitName = "ucmdbPU", type = PersistenceContextType.TRANSACTION)
    EntityManager entityManager;

    public List<CiAttributeConfiguration> getCiAttributesConfiguration(String ciType) {
        List<CiAttributeConfiguration> CiAttributeConfigurationList;

        Query query = entityManager.createQuery(GET_CI_ATTRIBUTES_SQL, CiAttributeConfiguration.class);
        query.setParameter("type", ciType);
        CiAttributeConfigurationList = query.getResultList();

        return CiAttributeConfigurationList;
    }

    public List<CiAttributeConfiguration> getCiAttributesConfiguration(String ciType, List<String> attributeNames) {
        List<CiAttributeConfiguration> ciAttributeConfigurationList;

        Query query = entityManager.createQuery(GET_CI_ATTRIBUTE_BY_LIST_SQL, CiAttributeConfiguration.class);
        query.setParameter("type", ciType);
        query.setParameter("names", attributeNames);
        ciAttributeConfigurationList = query.getResultList();

        return ciAttributeConfigurationList;
    }

    public List<CiAttributeConfiguration> getCiAttributesConfigurationByLabels(String ciType, List<String> attributeLabels) {
        List<CiAttributeConfiguration> CiAttributeConfigurationList;

        Query query = entityManager.createQuery(GET_CI_ATTRIBUTE_BY_LABELS, CiAttributeConfiguration.class);
        query.setParameter("type", ciType);
        query.setParameter("names", attributeLabels);
        CiAttributeConfigurationList = query.getResultList();

        return CiAttributeConfigurationList;
    }

    public CiAttributeConfiguration getCiAttributeConfiguration(String ciType, String attributeName) {
        CiAttributeConfiguration CiAttributeConfiguration;

        try {
            Query query = entityManager.createQuery(GET_CI_ATTRIBUTE_BY_TYPE_AND_NAME_SQL, CiAttributeConfiguration.class);
            query.setParameter("type", ciType);
            query.setParameter("name", attributeName);
            CiAttributeConfiguration = (CiAttributeConfiguration)query.getSingleResult();
        } catch (NoResultException nre) {
            log.error(String.format("CiAttributeConfigurationEjb.getCiAttributeConfiguration() - Error uCMDB field for type %s and attribute %s", ciType, attributeName));
            return null;
        }

        return CiAttributeConfiguration;
    }

    public void updateCiAttributes(String className) {

    }

    private static final String GET_CI_ATTRIBUTES_SQL = "SELECT ca \n" +
                                                        "FROM CiAttributeConfiguration ca \n" +
                                                        "WHERE ca.ciType = :type";

    private static final String GET_CI_ATTRIBUTE_BY_LIST_SQL = "SELECT ca \n" +
                                                                " FROM CiAttributeConfiguration ca \n" +
                                                                " WHERE ca.ciType = :type" +
                                                                " AND ca.attributeName IN :names" +
                                                                " AND ca.isWorkflowRelated = TRUE";

    private static final String GET_CI_ATTRIBUTE_BY_LABELS = "SELECT ca \n" +
                                                            " FROM CiAttributeConfiguration ca \n" +
                                                            " WHERE ca.ciType = :type" +
                                                            " AND ca.attributeLabel IN :names" +
                                                            " AND ca.isWorkflowRelated = TRUE";

    private static final String GET_CI_ATTRIBUTE_BY_TYPE_AND_NAME_SQL = "SELECT ca \n" +
                                                                " FROM CiAttributeConfiguration ca \n" +
                                                                " WHERE ca.ciType = :type" +
                                                                " AND ca.attributeName = :name" +
                                                                " AND ca.isWorkflowRelated = TRUE";


}
