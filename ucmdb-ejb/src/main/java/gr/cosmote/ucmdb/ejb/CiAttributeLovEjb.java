package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.models.database.CiAttributeLov;
import lombok.extern.slf4j.Slf4j;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import java.util.List;

@Stateless
@Remote(CiAttributeLovRemote.class)
@Slf4j
public class CiAttributeLovEjb implements CiAttributeLovRemote {
    @PersistenceContext(unitName = "ucmdbPU", type = PersistenceContextType.TRANSACTION)
    EntityManager entityManager;

    public List<CiAttributeLov> getCiAttributeLovs(String attributeName, String ciType){
        List<CiAttributeLov> ciAttributeLovList;

        Query query = entityManager.createNativeQuery(GET_CI_ATTRIBUTE_LOVS_SQL, CiAttributeLov.class);
        query.setParameter(1, attributeName);
        query.setParameter(2, ciType);
        ciAttributeLovList = query.getResultList();

        return ciAttributeLovList.isEmpty() ? null : ciAttributeLovList;
    }

    public List<CiAttributeLov> getCiAttributeLovs(List<String> attributeNames, String ciType){
        List<CiAttributeLov> ciAttributeLovList;

        Query query = entityManager.createQuery(GET_CI_ATTRIBUTE_LOVS_BY_LIST_SQL, CiAttributeLov.class);
        query.setParameter("names", attributeNames);
        query.setParameter("ciType", ciType);
        ciAttributeLovList = query.getResultList();

        return ciAttributeLovList;
    }

    public List<CiAttributeLov> getCiAttributeLovsByTypeAndTenant(String ciType, String tenant) {
        log.debug("CiAttributeLovEjb.getCiAttributeLovsByTypeAndTenant :: Method Entered with CI Type {} & Tenant {}", ciType, tenant);

        List<CiAttributeLov> lovs = null;

        try {
            Query query = entityManager.createQuery(GET_CI_ATTRIBUTE_LOVS_BY_CI_TYPE_AND_TENANT, CiAttributeLov.class)
                    .setParameter("ciType", ciType)
                    .setParameter("tenant", tenant);

            lovs = query.getResultList();
        }
        catch (Exception ex) {
            log.error("CiAttributeLovEjb.getCiAttributeLovsByTypeAndTenant :: Error", ex);
        }

        log.debug("CiAttributeLovEjb.getCiAttributeLovsByTypeAndTenant :: Method Returned lovs size of {}", (lovs != null ? lovs.size() : 0));
        return lovs;
    }


    public List<CiAttributeLov> getAllCiAttributeLovs(){
        List<CiAttributeLov> ciAttributeLovList;

        Query query = entityManager.createNativeQuery(GET_ALL_CI_ATTRIBUTE_LOVS_SQL, CiAttributeLov.class);
        ciAttributeLovList = query.getResultList();

        return ciAttributeLovList;
    }

    private static final String GET_CI_ATTRIBUTE_LOVS_SQL = "SELECT L.ID, L.ATTRIBUTE_NAME, L.ATTRIBUTE_VALUE, L.CI_TYPE, L.TENANT_ID FROM CI_ATTRIBUTE_LOV L WHERE L.ATTRIBUTE_NAME = ? AND L.CI_TYPE=?";
    private static final String GET_CI_ATTRIBUTE_LOVS_BY_LIST_SQL = "SELECT L FROM CiAttributeLov L WHERE L.type IN :names AND L.ciType=:ciType";
    private static final String GET_CI_ATTRIBUTE_LOVS_BY_CI_TYPE_AND_TENANT = "SELECT Lov FROM CiAttributeLov Lov WHERE Lov.ciType=:ciType AND Lov.tenant.description=:tenant";
    private static final String GET_ALL_CI_ATTRIBUTE_LOVS_SQL = "SELECT L.ID, L.ATTRIBUTE_NAME, L.ATTRIBUTE_VALUE, L.CI_TYPE, L.TENANT_ID FROM CI_ATTRIBUTE_LOV L";
}
