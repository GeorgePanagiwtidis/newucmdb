package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.client.UCMDBCallHelper;
import gr.cosmote.ucmdb.commons.helper.DateHelper;
import gr.cosmote.ucmdb.commons.properties.ApplicationProperties;
import gr.cosmote.ucmdb.model.TopologyMapResponse;
import gr.cosmote.ucmdb.models.base.CIAttribute;
import gr.cosmote.ucmdb.models.base.GenericCI;
import gr.cosmote.ucmdb.models.database.*;
import gr.cosmote.ucmdb.models.enumeration.NodeStatusEnum;
import gr.cosmote.ucmdb.models.enumeration.TaskStatusEnum;
import gr.cosmote.ucmdb.models.view.TaskAttributeDTO;
import gr.cosmote.ucmdb.models.view.TaskEditDTO;
import gr.cosmote.ucmdb.models.view.WorkflowStatsDTO;
import gr.cosmote.ucmdb.ucmdbWS.TypedPropertiesCollection;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

@Stateless
@Remote(NodeRemote.class)
@Slf4j
public class NodeEjb {
    private static final String NODE = "node";
    private static String UNCLASSIFIED_NODES_VIEW_NAME = ApplicationProperties.getInstance().getProperty("UNCLASSIFIED_NODES_VIEW_QUERY");
    private static String UNCLASSIFIED_NODES_DAYS_RESTRICTION = ApplicationProperties.getInstance().getProperty("UNCLASSIFIED_NODES_DAYS_RESTRICTION");

    private static final String GET_NODE_WORKFLOW_ATTRIBUTES = "SELECT CI_ATTR FROM CiAttributeConfiguration CI_ATTR WHERE CI_ATTR.ciType = 'node' AND CI_ATTR.isWorkflowRelated = TRUE";
    private static final String GET_NODES_BY_TYPE_SQL = "SELECT N FROM Node N WHERE N.status IN :status";
    private static final String GET_TODAY_RETRIEVED_NODES = "SELECT N FROM Node N WHERE N.creationDate = :today";
    private static final String GET_TOTAL_NUMBER_OF_NODES_SQL = "SELECT COUNT(N) FROM Node N";

    private static final String GET_ALL_USER_ACTIONS = "SELECT nodeDiff FROM NodeDifferences nodeDiff";
    private static final String GET_COMPLETED_NODE_DIFF_BY_TASK_ID = "SELECT nodeDiff FROM NodeDifferences nodeDiff WHERE nodeDiff.taskId=:taskId AND nodeDiff.userAction = 'Submit'";

    @PersistenceContext(unitName = "ucmdbPU", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    @Resource
    private SessionContext sessionContext;

    @EJB
    private BusinessRemote businessRemoteEjb;

    @EJB
    private CiAttributeConfigurationRemote ciAttributeConfigurationRemote;

    @EJB
    private CiAttributeLovRemote ciAttributeLovRemote;

    @EJB
    private TasksRemote tasksRemote;

    @EJB
    private TenantRemote tenantsEjb;

    @TransactionAttribute(value = TransactionAttributeType.NEVER)
    public List<Node> getAllUnclassifiedNodes() {
        log.debug("NodeEjb :: getAllUnclassifiedNodes :: Method Entered for non-chunked Response");

        List<String> attributes = businessRemoteEjb.getNodeClassAttributes(NODE);
        TypedPropertiesCollection typedProperties = businessRemoteEjb.getTypedPropertiesCollection(attributes);

        List<GenericCI> genericCIList = UCMDBCallHelper.executeTopologyQueryByName(UNCLASSIFIED_NODES_VIEW_NAME, typedProperties);

        log.debug("NodeEjb :: getAllUnclassifiedNodes :: Method Retrieved {} Nodes before filtering", genericCIList.size());

        // Filter Retrieved Nodes
        List<GenericCI> filterResultByDaysInterval = filterNodesByCreateTime(getDaysIntervalFromPropertyFiles(), genericCIList);
        List<Node> unclassifiedNodeList = transformToUnclassifiedNode(filterResultByDaysInterval);
        List<Node> nodesToBeStored = filterNodesNeedToInsert(unclassifiedNodeList);
        nodesToBeStored = filterUnixNtNodes(nodesToBeStored);

        log.debug("NodeEjb :: getAllUnclassifiedNodes :: Method added {} Nodes after Filtering", nodesToBeStored.size());
        return nodesToBeStored;
    }

    @TransactionAttribute(value = TransactionAttributeType.NEVER)
    public List<Node> getUnclassifiedNodesChunk(TopologyMapResponse topologyMapResponse, int chunkNumber) {
        log.debug("NodeEjb :: getUnclassifiedNodesChunk :: Method Entered for chunked Response");

        List<String> attributes = businessRemoteEjb.getNodeClassAttributes(NODE);
        TypedPropertiesCollection typedProperties = businessRemoteEjb.getTypedPropertiesCollection(attributes);

        List<GenericCI> genericCIList = UCMDBCallHelper.getNodeAsGenericCIByChunk(UNCLASSIFIED_NODES_VIEW_NAME, typedProperties, topologyMapResponse, chunkNumber);

        log.debug("NodeEjb :: getUnclassifiedNodesChunk :: Method Retrieved {} Nodes before filtering", genericCIList.size());

        // Filter Retrieved Nodes
        List<GenericCI> filterResultByDaysInterval = filterNodesByCreateTime(getDaysIntervalFromPropertyFiles(), genericCIList);
        List<Node> unclassifiedNodeList = transformToUnclassifiedNode(filterResultByDaysInterval);
        List<Node> nodesToBeStored = filterNodesNeedToInsert(unclassifiedNodeList);
        nodesToBeStored = filterUnixNtNodes(nodesToBeStored);

        log.debug("NodeEjb :: getUnclassifiedNodesChunk :: Method added {} Nodes after Filtering", nodesToBeStored.size());
        return nodesToBeStored;
    }

    @TransactionAttribute(value = TransactionAttributeType.NEVER)
    public TopologyMapResponse getTopologyMapResponse() {
        List<String> attributes = businessRemoteEjb.getNodeClassAttributes(NODE);
        TypedPropertiesCollection typedProperties = businessRemoteEjb.getTypedPropertiesCollection(attributes);
        return UCMDBCallHelper.getTopologyMap(UNCLASSIFIED_NODES_VIEW_NAME, typedProperties);
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public Node saveOrUpdateNodeAndTaskSummaryRecords(Node node) {
        if (node != null) {
            try {
                node = entityManager.merge(node);
                tasksRemote.updateTaskSummaryRecords(node);
                log.debug("NodeEjb.saveOrUpdateNodeAndTaskSummaryRecords() ::: method returning {} ", node);
            } catch (Exception e) {
                log.error("NodeEjb.saveOrUpdateNodeAndTaskSummaryRecords() ::: error in method", e);
                throw new gr.cosmote.ucmdb.commons.exceptions.ApplicationException("NodeEjb.saveOrUpdateNodeAndTaskSummaryRecords :: Error in saveOrUpdateNodeAndTaskSummaryRecords method", e);
            }
        }
        return node;
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRES_NEW)
    public Node saveOrUpdateNode(Node node) {
        if (node != null) {
            try {
                node = entityManager.merge(node);
                log.debug("NodeEjb.saveOrUpdateNode() ::: method returning {} ", node);
            } catch (Exception e) {
                log.error("NodeEjb.saveOrUpdateNode() ::: error in method", e);
                throw new gr.cosmote.ucmdb.commons.exceptions.ApplicationException("NodeEjb.saveOrUpdateNode :: Error in saveOrUpdateNodeAndTaskSummaryRecords method", e);
            }
        }
        return node;
    }

    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public List<Node> getNodeBy(List<NodeStatusEnum> status) {
        List<Node> nodeList;

        Query query = entityManager.createQuery(GET_NODES_BY_TYPE_SQL, Node.class);
        query.setParameter("status", status);
        nodeList = query.getResultList();

        return nodeList;
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRES_NEW)
    public void insertUnclassifiedNodes(List<Node> nodeList) {
        log.debug("NodeEjb.insertUnclassifiedNodes() ::: method entered with nodes {}", nodeList);

        nodeList.forEach((n) -> {
            entityManager.persist(n);
            entityManager.flush();
        });
        log.debug("NodeEjb.insertUnclassifiedNodes() ::: method returning");
    }

    @TransactionAttribute(value = TransactionAttributeType.REQUIRES_NEW)
    public void insertUnclassifiedNode(Node node) {
        log.debug("NodeEjb.insertUnclassifiedNode() ::: method entered with node {}", node);

        entityManager.persist(node);
        entityManager.flush();

        log.debug("NodeEjb.insertUnclassifiedNode() ::: method returning");
    }

    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public Node getNodeBy(long taskId) {
        log.debug("NodeEjb.getNodeBy() ::: method entered with task id {}", taskId);
        TaskSummary taskSummary = tasksRemote.getTaskSummaryRecord(taskId);
        Node node = getNode(taskSummary.getNode().getId());
        log.debug("NodeEjb.getNodeBy() ::: method method returning");
        return node;
    }

    public long getNumberOfAllNodes() {
        Query query = entityManager.createQuery(GET_TOTAL_NUMBER_OF_NODES_SQL);
        long count = (long) query.getSingleResult();

        return count;
    }

    public List<Node> getNodesBy(List<String> tenants, List<NodeStatusEnum> status, Date dateFrom, Date dateTo) {
        log.debug("NodeEjb.getNodesBy() ::: method entered with status {}, date from {}, date to {}", transformNodeStatusToString(status), dateFrom, dateTo);

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Node> cq = cb.createQuery(Node.class);
        Root<Node> node = cq.from(Node.class);

        List<Predicate> predicates = generateQueryForNodeCriteria(status, dateFrom, dateTo, cb, node);
        cq.where(predicates.toArray(new Predicate[predicates.size()]));

        TypedQuery<Node> query = entityManager.createQuery(cq);
        List<Node> nodeList = query.getResultList();
        nodeList = filterNodesByTenant(nodeList, tenants);

        log.debug("NodeEjb.getNodesBy() ::: method method returning with nodes size {}", 0);
        return nodeList;
    }

    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public Node getNode(long nodeId) {
        Node node = entityManager.find(Node.class, nodeId);

        return node;
    }

    public List<CiAttributeConfiguration> getNodeAttributes() {
        log.debug("NodeEjb :: getNodeAttributes :: Method Entered");

        List<CiAttributeConfiguration> nodeAttributes = entityManager.createQuery(GET_NODE_WORKFLOW_ATTRIBUTES).getResultList();

        log.debug("NodeEjb :: getNodeAttributes :: Method returned Node Attributes {}", nodeAttributes);

        return nodeAttributes;
    }

    public List<Node> getTodayRetrievedNodes() {
        log.debug("NodeEjb.getTodayRetrievedNodes :: Method Entered");

        List<Node> todaysUnclassifiedNodes =
                entityManager.createQuery(GET_TODAY_RETRIEVED_NODES, Node.class)
                        .setParameter("today", new Date(), TemporalType.DATE)
                        .getResultList();

        log.debug("NodeEjb.getTodayRetrievedNodes :: Method Returned {} Nodes", todaysUnclassifiedNodes.size());

        return todaysUnclassifiedNodes;
    }

    public List<NodeDifferences> getAllNodeActions() {
        log.debug("NodeEjb :: getAllNodeActions :: Method Entered");

        List<NodeDifferences> userActions = entityManager.createQuery(GET_ALL_USER_ACTIONS, NodeDifferences.class).getResultList();

        log.debug("NodeEjb :: getAllNodeActions :: Method Returned All User Actions", userActions);
        return userActions;
    }

    public NodeDifferences getNodeDifferencesById(String nodeDiffId) {
        log.debug("NodeEjb :: getNodeDifferencesById :: Method Entered for Id {}", nodeDiffId);
        NodeDifferences nodeDifferences;

        nodeDifferences = entityManager.find(NodeDifferences.class, Long.valueOf(nodeDiffId));

        log.debug("NodeEjb :: getNodeDifferencesById :: Method Returned Node Differences Object {}", nodeDifferences);
        return nodeDifferences;
    }

    public NodeDifferences getNodeDifferencesByTaskId(String taskId) {
        log.debug("NodeEjb :: getNodeDifferencesByTaskId :: Method Entered for Task with Id {}", taskId);
        NodeDifferences nodeDifferences;

        try {
            nodeDifferences = entityManager.createQuery(GET_COMPLETED_NODE_DIFF_BY_TASK_ID, NodeDifferences.class).setParameter("taskId", taskId).getSingleResult();
        } catch (NonUniqueResultException noUniqueResult) {
            log.error("NodeEjb :: getNodeDifferencesByTaskId :: No Single Result found, returning the 1st one");

            nodeDifferences = entityManager.createQuery(GET_COMPLETED_NODE_DIFF_BY_TASK_ID, NodeDifferences.class).setParameter("taskId", taskId).getResultList().get(0);
        }

        log.debug("NodeEjb :: getNodeDifferencesByTaskId :: Method Returned Node Differences Object {}", nodeDifferences);
        return nodeDifferences;
    }

    public void createNodeAuditingReport(TaskEditDTO taskEdit, String userGroup, List<TaskAttributeDTO> beforeUpdateAttributes, List<TaskAttributeDTO> afterUpdateAttributes) {
        log.debug("NodeEjb :: createNodeAuditingReport :: Method Entered for Task Id {}, Node Id {}", taskEdit.getTaskId(), taskEdit.getNodeId());

        Node unclassifiedNode = entityManager.find(Node.class, Long.valueOf(taskEdit.getNodeId()));

        NodeDifferences nodeDifferences = new NodeDifferences();
        nodeDifferences.setGlobalId(unclassifiedNode.getGlobalId());
        nodeDifferences.setNodeId(Long.valueOf(taskEdit.getNodeId()));
        nodeDifferences.setTaskId(taskEdit.getTaskId());
        nodeDifferences.setDisplayLabel(unclassifiedNode.getDisplayLabel());
        nodeDifferences.setOwnerTenant(unclassifiedNode.getOwnerTenant());
        nodeDifferences.setUserGroup(userGroup);
        nodeDifferences.setUpdatedBy(sessionContext.getCallerPrincipal().getName());
        nodeDifferences.setUserAction(taskEdit.getSelectedAction().name());
        nodeDifferences.setLastModifiedTime(new Date());
        nodeDifferences.setBeforeUpdate(beforeUpdateAttributes);
        nodeDifferences.setAfterUpdate(afterUpdateAttributes);

        // Add Comments also for Reassign Action
        if (taskEdit.getSelectedAction().equals(TaskEditDTO.ACTION_STATUS.Reassign)) {
            nodeDifferences.setComments(taskEdit.getReassignTask().getReassignReason());
        }

        entityManager.persist(nodeDifferences);

        log.debug("NodeEjb :: createNodeAuditingReport :: Method persisted entity {}", nodeDifferences);
    }

    public List<WorkflowStatsDTO> getWorkflowStats() {
        log.debug("TasksEjb.getWorkflowStats :: Method Entered");
        List<WorkflowStatsDTO> stats;

        Map<LocalDate, List<Node>> tasksPerMonth = getAllWorkflowNodesPerMonth();
        stats = computeMonthWorkflowStatsPerTenant(tasksPerMonth);

        log.debug("TasksEjb.getWorkflowStats :: Method Returned Workflow Stats size of {}", stats.size());
        return stats;
    }

    private Map<LocalDate, List<Node>> getAllWorkflowNodesPerMonth() {
        List<Node> workflowNodes = this.getNodeBy(Arrays.asList(NodeStatusEnum.OPEN, NodeStatusEnum.COMPLETED, NodeStatusEnum.REJECTED));

        Map<LocalDate, List<Node>> nodesPerMonth = workflowNodes.stream()
                .collect(groupingBy(node -> {
                    if (node.getCreationDate() != null) {
                        return node.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().withDayOfMonth(1);
                    }
                    else {
                        return node.getCreationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().withDayOfMonth(1);
                    }
                }));

        return nodesPerMonth;
    }

    private List<WorkflowStatsDTO> computeMonthWorkflowStatsPerTenant(Map<LocalDate, List<Node>> nodesPerMonth) {
        log.debug("TasksEjb.computeWorkflowStats :: Method Entered for # of Months {}", nodesPerMonth.keySet().size());
        List<WorkflowStatsDTO> workflowStats = new ArrayList<>();

        nodesPerMonth
                .keySet()
                .forEach(month -> {
                    List<Node> monthTasks = nodesPerMonth.get(month);

                    Map<String, List<Node>> monthNodesPerTenant = monthTasks
                            .stream()
                            .collect(Collectors.groupingBy(Node::getOwnerTenant));

                    monthNodesPerTenant
                            .keySet()
                            .forEach(tenant -> {
                                List<Node> tenantTasks = monthNodesPerTenant.get(tenant);

                                long completedNodes = tenantTasks.stream().filter(n -> n.getStatus() == NodeStatusEnum.COMPLETED).count();
                                long openNodes      = tenantTasks.stream().filter(n -> n.getStatus() == NodeStatusEnum.OPEN).count();

                                WorkflowStatsDTO workflowMonthStat = new WorkflowStatsDTO();

                                workflowMonthStat.setWorkflowMonth(Date.from(month.atStartOfDay(ZoneId.systemDefault()).toInstant()));
                                workflowMonthStat.setTenantOwner(tenant);
                                workflowMonthStat.setOpenTasks(Math.toIntExact(openNodes));
                                workflowMonthStat.setCompletedTasks(Math.toIntExact(completedNodes));
                                workflowMonthStat.setTotalTasks(Math.toIntExact(openNodes+completedNodes));

                                workflowStats.add(workflowMonthStat);
                            });
                });

        workflowStats.add(computeTotalWorkflowTasks(workflowStats));

        log.debug("TasksEjb.computeWorkflowStats :: Method Return list of Stats size of {}", workflowStats);
        return workflowStats;
    }

    private WorkflowStatsDTO computeTotalWorkflowTasks(List<WorkflowStatsDTO> workflowStats) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");

        return workflowStats
                .stream()
                .reduce(new WorkflowStatsDTO(), (stat1, stat2) -> {
                    stat1.setWorkflowMonth(new Date());
                    stat1.setTotalMonthsMessage(String.format("Report for Unclassified Nodes: %s", sdf.format(new Date())));
                    stat1.setTenantOwner("ALL");

                    stat1.setOpenTasks(stat1.getOpenTasks() + stat2.getOpenTasks());
                    stat1.setCompletedTasks(stat1.getCompletedTasks() + stat2.getCompletedTasks());
                    stat1.setTotalTasks(stat1.getTotalTasks() + stat2.getTotalTasks());

                    return stat1;
                });
    }

    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    private List<Node> filterNodesByTenant(List<Node> nodeList, List<String> tenants) {
        boolean allTenantsSelected = tenantsEjb.getAllTenants().size() == tenants.size();
        List<Node> filteredNodes;

        if (allTenantsSelected) {
            filteredNodes = nodeList;
        }
        else {
            filteredNodes =
                    nodeList.stream()
                            .filter(node -> {
                                if (node.getOwnerTenant() != null) {
                                    return tenants.contains(node.getOwnerTenant());
                                }
                                else {
                                    // For old tasks OwnerTenant field in null so we have to look for Task Group
                                    return nodeBelongsToTenants(node, tenants);
                                }
                            })
                            .collect(Collectors.toList());
        }

        return filteredNodes;
    }

    private boolean nodeBelongsToTenants(Node node, List<String> selectedTenants) {
        boolean nodeBelongsToTenant;

        try {
            String userGroup = node.getTasksWrapper().getTasks(node.getId()).get(0).getUserGroup().getDescription();
            String nodeTenant = userGroup.substring(userGroup.indexOf("_")+1).toUpperCase();

            nodeBelongsToTenant = selectedTenants.contains(nodeTenant);
        }
        catch (Exception ex) {
            log.error("NodeEjb.nodeBelongsToTenants :: ERROR {}", ex);

            nodeBelongsToTenant = false;
        }

        return nodeBelongsToTenant;
    }

    private List<Predicate> generateQueryForNodeCriteria(List<NodeStatusEnum> status, Date dateFrom, Date dateTo, CriteriaBuilder cb, Root<Node> node) {
        List<Predicate> predicateList = new ArrayList<>();
        Predicate statusPredicate = generateStatusPredicatesForNodeCriteria(status, cb, node);
        Predicate datePredicate = generateDatePredicatesForNodeCriteria(dateFrom, dateTo, cb, node);

        if (statusPredicate != null)
            predicateList.add(statusPredicate);

        if (datePredicate != null)
            predicateList.add(datePredicate);

        return predicateList;
    }

    private Predicate generateStatusPredicatesForNodeCriteria(List<NodeStatusEnum> status, CriteriaBuilder cb, Root<Node> node) {
        Predicate predicate = null;

        if (status != null && status.size() > 0) {
            predicate = node.get(Node_.status).in(status);
        }

        return predicate;
    }

    private Predicate generateDatePredicatesForNodeCriteria(Date dateFrom, Date dateTo, CriteriaBuilder cb, Root<Node> node) {
        if (dateFrom != null && dateTo != null) {
            return cb.between(node.get(Node_.creationDate), dateFrom, dateTo);
        } else if (dateFrom != null) {
            return cb.greaterThanOrEqualTo(node.get(Node_.creationDate), dateFrom);
        } else if (dateTo != null) {
            return cb.lessThanOrEqualTo(node.get(Node_.creationDate), dateTo);
        } else {
            return null;
        }
    }

    private List<Node> filterNodesNeedToInsert(List<Node> unclassifiedNodeList) {
        List<Node> insertedNodes = getNodeBy(Arrays.asList(NodeStatusEnum.OPEN));
        return unclassifiedNodeList.stream()
                .filter(node -> !insertedNodes.contains(node))
                .collect(Collectors.toList());
    }

    private List<Node> filterUnixNtNodes(List<Node> nodes) {
        return nodes.stream()
                .filter(node -> isUnixOrNtType(node.getCiType()))
                .collect(Collectors.toList());
    }

    private boolean isUnixOrNtType(String type) {
        return type.equals("unix") || type.equals("nt");
    }

    private List<GenericCI> filterNodesByCreateTime(int days, List<GenericCI> nodes) {
        Date currentDate = new Date();

        return nodes.stream()
                .filter(node -> {
                    String nodeLastAccessTimeStr = node.getProperties().get("create_time");
                    Instant nodeLastAccessTime = Instant.parse(nodeLastAccessTimeStr);

                    long datesDiff = TimeUnit.MILLISECONDS.toDays(currentDate.getTime() - nodeLastAccessTime.toEpochMilli());

                    if (datesDiff <= days) {
                        return true;
                    } else {
                        return false;
                    }
                })
                .collect(Collectors.toList());
    }

    private List<Node> transformToUnclassifiedNode(List<GenericCI> genericCIList) {
        List<Node> unclassifiedNodeEntityList = genericCIList.stream()
                .map(this::generateUnclassifiedNode)
                .collect(Collectors.toList());

        return unclassifiedNodeEntityList;
    }

    private Node generateUnclassifiedNode(GenericCI genericCI) {
        Node unclassifiedNode = new Node();
        unclassifiedNode.setGlobalId(genericCI.getId());
        unclassifiedNode.setCreationDate(new Date());
        unclassifiedNode.setStatus(NodeStatusEnum.OPEN);
        unclassifiedNode.setCiType(genericCI.getType());
        unclassifiedNode.setOwnerTenant(genericCI.getProperties().get("TenantOwner"));

        TasksWrapper taskWrapper = new TasksWrapper();
        taskWrapper.setName(genericCI.getId());
        Task initialState = new Task();
        initialState.setName("New Node");
        initialState.setId(ThreadLocalRandom.current().nextLong(Long.MAX_VALUE));
        initialState.setStatus(TaskStatusEnum.INITIATED);
        initialState.setCiType(unclassifiedNode.getCiType());
        initialState.setGlobalId(unclassifiedNode.getGlobalId());
        initialState.setOsType(generateOsTypeForTask(genericCI));
        initialState.setCreationDate(generateCreateTime(genericCI));

        // TODO -- Check this
        genericCI.getProperties().remove("description");
        genericCI.getProperties().remove("discovered_os_name");

        // For Each Ci Attribute get Lov per Tenant
        String tenantOwner = genericCI.getProperties().get("TenantOwner");
        genericCI.getProperties().forEach((k, v) -> {
            CIAttribute ciAttribute = createCIAttribute(k, v, tenantOwner);
            if (ciAttribute != null) {
                initialState.getAttributes().add(ciAttribute);
            }
        });

        addMissingNodeFields(initialState.getAttributes(), tenantOwner);
        unclassifiedNode.setDisplayLabel(initialState.findCIAttributeValue("display_label"));

        taskWrapper.setTasks(Arrays.asList(initialState));

        try {
            unclassifiedNode.setTasksWrapper(taskWrapper);
        } catch (IOException e) {
            log.error("UnclassifiedService.generateTasksWrapper() ::: Serialization exception of {}", taskWrapper);
        }

        return unclassifiedNode;
    }

    private Date generateCreateTime(GenericCI genericCI) {
        String createTimeProperty = genericCI.getProperties().remove("create_time");

        Date createTime;
        if (createTimeProperty != null) {
            createTime = DateHelper.parseUcmdbDate(createTimeProperty);
        }
        else {
            createTime = null;
        }

        return createTime;
    }

    private String generateOsTypeForTask(GenericCI genericCI) {
        String osType;
        try {
            String description = genericCI.getProperties().get("description");
            String discoveredOsName = genericCI.getProperties().get("discovered_os_name");

            osType = discoveredOsName != null ? discoveredOsName : description;
        } catch (Exception ex) {
            return null;
        }

        return osType;
    }

    private void addMissingNodeFields(List<CIAttribute> nodeAttributes, String tenantOwner) {
        List<String> attributes = businessRemoteEjb.getNodeClassAttributes(NODE);

        List<String> missingStrAttributes = findMissingStringAttributes(nodeAttributes, attributes);
        List<CIAttribute> missingAttributes = createEmptyCIAttributes(missingStrAttributes, tenantOwner);

        nodeAttributes.addAll(missingAttributes);
    }

    private List<String> findMissingStringAttributes(List<CIAttribute> nodeAttributes, List<String> attributes) {
        return attributes.stream()
                .filter(a -> !isAttributeIncluded(nodeAttributes, a))
                .collect(Collectors.toList());
    }

    private boolean isAttributeIncluded(List<CIAttribute> nodeAttributes, String attribute) {
        return nodeAttributes.stream()
                .anyMatch(na -> attribute.equals(na.getFieldName()));
    }

    private List<CIAttribute> createEmptyCIAttributes(List<String> attributeNames, String tenantOwner) {
        List<CiAttributeConfiguration> ciAttributes = ciAttributeConfigurationRemote.getCiAttributesConfiguration(NODE, attributeNames);
        List<CiAttributeLov> ciAttributeLovList = ciAttributeLovRemote.getCiAttributeLovs(attributeNames, NODE);

        List<CIAttribute> attributes = attributeNames.stream()
                .map(an -> {
                    CIAttribute attribute = new CIAttribute();
                    attribute.setFieldName(an);
                    attribute.setFieldValue("");
                    updateCIAttributeConfiguration(ciAttributes, attribute);
//                    if(attribute.isListType()) {
                    updateCIAttributeLOVs(ciAttributeLovList, attribute, tenantOwner);
//                    }

                    return attribute;
                })
                .collect(Collectors.toList());

        return attributes;
    }

    private void updateCIAttributeConfiguration(List<CiAttributeConfiguration> ciAttributes, CIAttribute attribute) {
        CiAttributeConfiguration configuration = ciAttributes.stream()
                .filter(a -> a.getAttributeName().equals(attribute.getFieldName()))
                .findFirst()
                .orElse(null);

        if (configuration != null) {
            attribute.setFieldType(configuration.getType().getText());
            attribute.setFieldLabel(configuration.getAttributeLabel());
        }
    }

    private void updateCIAttributeLOVs(List<CiAttributeLov> ciAttributeLovList, CIAttribute attribute, String tenantOwner) {
        List<CiAttributeLov> lovs = ciAttributeLovList.stream()
                .filter(a -> a.getType().equals(attribute.getFieldName()))
                .filter(a -> a.getTenant() == null || a.getTenant().getDescription().equals(tenantOwner))
                .collect(Collectors.toList());

        if (lovs != null && lovs.size() > 0) {
            List<String> lovsStr =
                    lovs.stream()
                            .map(CiAttributeLov::getValue)
                            .collect(Collectors.toList());

            attribute.setFieldLovs(lovsStr);
        }
    }

    private CIAttribute createCIAttribute(String attributeName, String attributeValue, String tenantOwner) {
        CIAttribute attribute = new CIAttribute();
        attribute.setFieldName(attributeName);
        attribute.setFieldValue(attributeValue);

        final CiAttributeConfiguration attr = ciAttributeConfigurationRemote.getCiAttributeConfiguration(NODE, attribute.getFieldName());
        if (attr == null) {
            return null;
        }

        attribute.setFieldType(attr.getType().getText());
        attribute.setFieldLabel(attr.getAttributeLabel());
//        if(attr.isListType()) {
        addLovValues(attribute, tenantOwner);
//        }

        return attribute;
    }

    private void addLovValues(CIAttribute attribute, String tenantOwner) {
        List<CiAttributeLov> ciAttributeLovList = ciAttributeLovRemote.getCiAttributeLovs(attribute.getFieldName(), NODE);

        List<String> lovs = null;
        if (ciAttributeLovList != null) {
            lovs =
                    ciAttributeLovList.stream()
                            // TODO -- Check!
//                            .filter(l -> l.getType().equals(attribute.getFieldName()))
                            .filter(l -> l.getTenant() == null || l.getTenant().getDescription().equals(tenantOwner))
                            .map(CiAttributeLov::getValue)
                            .collect(Collectors.toList());
        }

        attribute.setFieldLovs(lovs);
    }

    private String transformNodeStatusToString(List<NodeStatusEnum> nodeStatusEnums) {
        if (nodeStatusEnums == null)
            return null;

        return nodeStatusEnums.stream()
                .map(Enum::toString)
                .collect(Collectors.joining(", "));
    }

    public void setUnclassifiedNodesViewName(String viewName) {
        UNCLASSIFIED_NODES_VIEW_NAME = viewName;
    }

    public void setUnclassifiedNodesDaysRestriction(String days) {
        UNCLASSIFIED_NODES_DAYS_RESTRICTION = days;
    }

    public int getDaysIntervalFromPropertyFiles() {
        int days;
        try {
            days = Integer.valueOf(UNCLASSIFIED_NODES_DAYS_RESTRICTION);
        } catch (NumberFormatException ex) {
            days = 90;
        }

        return days;
    }
}
