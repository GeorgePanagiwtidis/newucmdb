package gr.cosmote.ucmdb.ejb;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import gr.cosmote.ucmdb.commons.exceptions.ApplicationException;
import gr.cosmote.ucmdb.commons.properties.ApplicationProperties;
import gr.cosmote.ucmdb.models.database.CiAttributeConfiguration;
import gr.cosmote.ucmdb.models.database.Tenants;
import gr.cosmote.ucmdb.models.view.BusinessResultDTO;
import gr.cosmote.ucmdb.models.view.BusinessServicesDTO;
import gr.cosmote.ucmdb.models.view.BusinessCiCollectionRequestDTO;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.ejb.*;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.xml.bind.JAXBElement;
import javax.xml.ws.Holder;

import gr.cosmote.ucmdb.client.UCMDBCallHelper;
import gr.cosmote.ucmdb.models.base.*;
import gr.cosmote.ucmdb.models.database.TenantSearchableType;
import gr.cosmote.ucmdb.ucmdbWS.*;
import lombok.extern.slf4j.Slf4j;

@Stateless
@Remote(BusinessRemote.class)
@Slf4j
public class BusinessEjb {

    @PersistenceContext(unitName = "ucmdbPU", type = PersistenceContextType.TRANSACTION)
    EntityManager entityManager;

    private static LoadingCache<ExcelCriteria, BusinessResult> SEARCHCACHE = null;

    private final List<String> SEARCH_PROPERTIES = new ArrayList<>(Arrays.asList(
            "ack_id", "BODY_ICON", "calculated_id", "city", "classification", "codepage", "contextmenu", "core_number", "country", "cpu_clock_speed",
            "cpu_id", "cpu_specifier", "cpu_speed", "cpu_type", "cpu_usage", "cpu_vendor",
            "description", "digest", "display_label", "document_list", "FAMILY_ICON", "global_id", "global_id_scope", "dns_servers", "domain_name",
            "hyper_thread", "is_save_persistency", "isvirtual", "language", "last_modified_time", "layer", "logical_cpu_count", "matching_rules", "MENU",
            "name", "root_actualdeletetime", "root_actualdeletionperiod", "root_candidatefordeletetime", "root_class", "root_container",
            "root_enableageing", "root_iconproperties", "bios_source", "root_iscandidatefordeletion", "root_lastaccesstime", "root_system", "serial_number", "state", "TenantOwner",
            "TenantsUses", "track_changes", "user_label", "disk_size", "disk_type", "model_name", "vendor", "fchba_domainid", "fchba_driverversion",
            "fchba_firmware", "fchba_ip", "fchba_model", "fchba_targetportwwn", "fchba_type", "fchba_vendor", "fchba_version", "fchba_wwn",
            "fchba_wwpn", "disk_failures", "filesystem_type", "free_space", "mount_device", "mount_point", "board_index", "discovered_model",
            "bus", "firmware_version", "hardware_board_index", "hardware_version", "software_version", "type", "vendor_card_id", "discovered_os_name",
            "discovered_os_vendor", "discovered_os_version", "discovered_vendor", "default_gateway_ip_address_type", "memory_size",
            "active_cpu_in_pool", "active_physical_cpu", "auto_start", "boot_mode", "conn_monitoring", "default_profile_name", "desired_mem", "desired_num_huge_pages",
            "desired_proc_units", "desired_procs", "entitled_capacity", "logical_serial_number", "lpar_id", "lpar_io_pool_ids", "lpar_mode",
            "lpar_name", "lpar_state", "lpar_type", "max_mem", "max_num_huge_pages", "max_proc_units", "max_procs", "max_virtual_slots",
            "min_mem", "min_num_huge_pages", "min_proc_units", "min_procs", "online_virtual_cpu", "power_ctrl_lpar_ids", "proc_mode",
            "profile_name", "redundant_err_path_reporting", "shared_pool_id", "sharing_mode", "uncap_weight", "virtual_serial_adapters", "work_group_id",
            "applicationresource_type", "physical_cpus_available", "physical_cpus_configurable", "physical_cpus_pending", "pool_id", "resource_path", "gateways",
            "interface_admin_status", "interface_alias", "interface_description", "interface_index", "interface_macaddr", "interface_name",
            "interface_operational_status", "interface_role", "interface_speed", "interface_type", "is_system_internal", "isPseudo", "l2help", "mac_address",
            "primary_wins", "secondary_wins", "arp_mac", "authoritative_dns_name", "ip_address", "ip_address_property", "ip_address_type", "ip_address_value",
            "ip_dhcpdomainname", "ip_isbroadcast", "ip_isdhcp", "ip_ismanaged", "ip_lease_time", "ip_netaddr", "ip_netclass", "ip_netmask", "ip_nettype",
            "ip_probename", "routing_domain", "bound_to_ip_address", "bound_to_ip_address_type", "ip_service_name", "ipport_type", "ipserver_address",
            "ipserver_banner", "ipserver_clients", "network_port_number", "port_type", "service_names", "ip_prefix_length", "network_broadcastaddress",
            "network_managed", "network_netclass", "network_netmask", "network_nettype", "network_probename", "logical_volume_global_id", "logicalvolume_accesstype",
            "logicalvolume_availability", "logicalvolume_domainid", "logicalvolume_free", "logicalvolume_fstype", "logicalvolume_id", "logicalvolume_sharename",
            "logicalvolume_size", "logicalvolume_status", "logicalvolume_storagecapabilities", "logicalvolume_used", "network_adapter_type", "application_category",
            "application_instancename", "application_ip", "application_ip_domain", "application_ip_type", "application_password", "application_path", "application_port",
            "application_timeout", "application_username", "application_version", "build_number", "database_dbconnectstring", "discovered_location",
            "database_dbinstallpath", "database_dbport", "database_dbsid", "database_dbtype", "database_dbversion", "database_role", "discovered_product_name",
            "has_config_files", "oracle_filetype", "oracle_instancenumber", "oracle_tnsnames", "oracle_undotablespace", "product_name",
            "root_container_name", "software_edition", "startup_time", "version", "listened_ips", "listener_name", "default_gateway_ip_address",
            "group_id", "homedir", "osuser_gid", "osuser_uid", "user_id", "volume_id", "volume_size", "connection_state", "connection_url",
            "enabled_for_live_migration", "hypervisor_name", "maintenance_mode", "status", "vmotion_enabled", "volume_group_id",
            "administration_domain", "application_server_type", "j2eeserver_fullname", "j2eeserver_isadminserver", "j2eeserver_listenadress", "j2eeserver_objectname",
            "j2eeserver_protocol", "j2eeserver_servername", "j2eeserver_sslport", "j2eeserver_starttime", "j2eeserver_vendor", "j2eeserver_version",
            "weblogic_certPemPath", "weblogic_isadminserver", "weblogic_keyFileName", "weblogic_keyPemPath", "weblogic_trustFi", "network_count", "network_ismanaged"
    )
    );

    private final String WEB_SERVER_CLASS = "web_server";

    // ------------------------------------------- WEB SERVICE METHODS -----------------------------------------

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public BusinessResultDTO retrieveBusinessServices(BusinessSearchCriteria criteria) {
        log.debug("BusinessEjb :: retrieveBusinessServices :: method entered with searchCriteria: {}, tenants: {}", criteria.getSearchTerm(), criteria.getTenants());

        List<GenericCI> tempResult;

        List<String> types = getSearchableTypesForTenants(criteria.getTenants());
        List<CI> list = new ArrayList<>();

        for (String type : types) {
            list.addAll(getCIsByType(type, initTypedPropertiesCollectionForSoapRequests()));
        }

        tempResult = filterBusinessServicesByTenant(list, criteria.getTenants());
        tempResult = filterBusinessServicesBySearchTerm(tempResult, criteria.getSearchTerm());
        tempResult = removeUnnecessaryProperties(tempResult);

        BusinessResultDTO businessResult = groupResultsByTenant(tempResult);

        log.debug("BusinessEjb :: retrieveBusinessServices :: method returning grouped results size of: {}", businessResult.getBusiness().size());

        return businessResult;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<GenericCI> retrieveCiCollectionsOfBusinessService(BusinessCiCollectionRequestDTO requestObject) {
        log.debug("BusinessEjb :: retrieveCiCollectionsOfBusinessService :: Method Entered for Business Service with Id {}", requestObject.getId());

        // Init SOAP Call Arguments
        ID businessServiceId = new ID();
        businessServiceId.setValue(requestObject.getId());

        List<CI> cisResponse = getCiCollectionsBasedOnServiceType(businessServiceId, requestObject.getCiType());
        List<GenericCI> result = mapResponseToGenericCiList(cisResponse, requestObject.getId());
        List<GenericCI> businessServiceWebServers = retrieveWebServers(requestObject.getId(), false);

        log.debug("BusinessEjb :: retrieveCiCollectionsOfBusinessService :: Method Returned CiCollections {}", result);

        // Exclude "Application of.." CiCollections & Add WebServers CiCollections if Any
        return modifyExistingCiCollections(requestObject.getId(), result, businessServiceWebServers);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<GenericCI> expandCiCollection(BusinessCiCollectionRequestDTO requestObject) {
        // When 2 Ids are the same then we should retrieve WebServers for BusinessService
        if (requestObject.getId().equals(requestObject.getParentBusinessId())) {
            return retrieveWebServers(requestObject.getId(), true);
        }

        // Init SOAP Call Arguments
        ID businessServiceId = new ID();
        businessServiceId.setValue(requestObject.getId());

        Holder<Topology> holder = UCMDBCallHelper.getCINeighbours(businessServiceId, "ci_collection", initTypedPropertiesCollectionForSoapRequests());

        Topology ciCollectionsResponse = holder.value;
        List<GenericCI> ciCollectionsResult = mapResponseToGenericCiList(getChildCiCollectionFromHolder(ciCollectionsResponse, requestObject.getId()), requestObject.getId());
        ciCollectionsResult = filterDuplicateCIs(ciCollectionsResult);

        if (ciCollectionsResult.isEmpty() && requestObject.getCiType() != null) {
            return filterDuplicateCIs(getCiCollectionDataBasedOnBusinessType(requestObject));
        }

        return ciCollectionsResult;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<GenericCI> getBusinessApplicationOrHostNodes(BusinessCiCollectionRequestDTO requestObject) {
        log.debug("BusinessEjb :: getBusinessApplicationOrHostNodes :: Method Entered for Ci with GlobalId {}", requestObject.getId());
        List<GenericCI> result;

        result = getNodeElements(requestObject.getId());

        if (result.isEmpty()) {
            result = getBusinessApplications(requestObject.getId(), getClassAttributes("business_application"));
        }

        log.debug("BusinessEjb :: getBusinessApplicationOrHostNodes :: Method Returned Node Elemenets {}", requestObject.getId());
        return result;
    }

    @TransactionAttribute(TransactionAttributeType.NEVER)
    public Map<String, String> getMenuItems(List<String> IDs) {
        Map<String, String> menuItems = new HashMap<>();
        try {
            Map<String, String> ucmdbClasses = getAllClassesHierarchyFromUcmdb();

            IDs
                    .stream()
                    .forEach(id -> {
                        ID ucmdbId = new ID();
                        ucmdbId.setValue(id);
                        List<CI> cis =
                                UCMDBCallHelper.getCINeighbours(ucmdbId, "object", getTypedPropertiesCollection(Arrays.asList("root_class")))
                                        .value.getCIs().getCI();

                        // Get Only Distinct Types
                        Set<String> ciNeighboursTypes = new HashSet<>();
                        cis.stream()
                                .filter(ci -> !isUnnecessaryType(ci.getType()))
                                .forEach(ci -> ciNeighboursTypes.add(ci.getType()));

                        ciNeighboursTypes.stream()
                                .forEach(type -> menuItems.putIfAbsent(type, ucmdbClasses.get(type)));
                    });

        } catch (Exception ex) {
            throw new ApplicationException("BusinessEjb :: getMenuItems :: ERROR: " + ex);
        }

        return menuItems;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<GenericCI> getCINeighboursOfType(CIsOfType cisOfType) {
        log.debug("BusinessEjb :: getCINeighboursOfType :: method entered with type: {}", cisOfType.getType());
        List<GenericCI> resultList = new ArrayList<>();
        for (Ci ci : cisOfType.getCis()) {
            log.debug("id: {}", ci.getId());
            log.debug("properties: {}", ci.getType());
            resultList.addAll(getGenericCINeighbours(ci.getId(), cisOfType.getType(), ci.getType()));
        }

        return resultList;
    }

    // Used in Node Ejb
    @TransactionAttribute(value = TransactionAttributeType.NEVER)
    public List<String> getNodeClassAttributes(String className) {
        log.debug("BusinessEjb :: getNodeClassAttributes :: method entered with className: {}", className);

        List<String> attributes = new ArrayList<>();
        List<CiAttributeConfiguration> temp_attributes = entityManager.createNativeQuery(GET_WORKFLOW_ATTRIBUTES, CiAttributeConfiguration.class).setParameter(1, className).getResultList();
        for (CiAttributeConfiguration attribute : temp_attributes) {
            attributes.add(attribute.getAttributeName());
        }

        log.debug("BusinessEjb :: getNodeClassAttributes :: method returning: {}", attributes);
        return attributes;
    }

    // ---------------------------------------- END OF WEB SERVICE METHODS ------------------------------------

    // ---------------------------------------- Filter Functions ----------------------------------------------
    private List<GenericCI> modifyExistingCiCollections(String businessServiceId, List<GenericCI> result, List<GenericCI> webServers) {
        log.debug("BusinessEjb.modifyExistingCiCollections :: Method Entered with unfiltered CiCollections size of {}", result.size());
        List<GenericCI> filteredCiCollections;

        // Exclude Pattern
        Pattern applicationOfPattern = Pattern.compile("^(?!Application of).+");

        filteredCiCollections =
                result
                        .stream()
                        .filter(ciCollection -> {
                            String ciCollectionDiplayLabel = ciCollection.getProperties().get("display_label");
                            Matcher matcher = applicationOfPattern.matcher(ciCollectionDiplayLabel);

                            return matcher.matches();
                        })
                        .collect(Collectors.toList());

        if (!webServers.isEmpty()) {
            Map<String, String> webServersProps = new HashMap<>();
            webServersProps.put("display_label", "Web Servers " + getBusinessServiceNameFromCiCollection(filteredCiCollections.get(0)));
            webServersProps.put("global_id", businessServiceId);

            GenericCI webServersCiCollection = new GenericCI();
            webServersCiCollection.setId(businessServiceId);
            webServersCiCollection.setType("web_servers");
            webServersCiCollection.setProperties(webServersProps);

            filteredCiCollections.add(webServersCiCollection);
        }

        log.debug("BusinessEjb.modifyExistingCiCollections :: Method Returned filtered CiCollections size of {}", filteredCiCollections.size());
        return filteredCiCollections;
    }

    private List<GenericCI> filterDuplicateCIs(List<GenericCI> ciCollectionsResult) {
        log.debug("BusinessEjb.filterDuplicateCIs :: Method Entered with unfiltered CIs size of {}", ciCollectionsResult.size());
        List<GenericCI> filteredDuplicateCIs;

        filteredDuplicateCIs =
                ciCollectionsResult
                        .stream()
                        .filter(ci -> !"TO-BE-DELETED(DUPLICATE)".equals(ci.getProperties().get("category")))
                        .filter(ci -> !"TO-BE-DELETED(JUNK)".equals(ci.getProperties().get("category")))
                        .filter(ci -> !"DECOMMISSIONED".equals(ci.getProperties().get("category")))
                        .collect(Collectors.toList());

        log.debug("BusinessEjb.filterDuplicateCIs :: Method Returned filtered CIs size of {}", filteredDuplicateCIs.size());
        return filteredDuplicateCIs;
    }
    // ---------------------------------- End of Filter Functions ---------------------------------------------

    // ---------------------------------------- HELPER METHODS ------------------------------------------------

    private Map<String, String> getAllClassesHierarchyFromUcmdb() {
        Holder<UcmdbClassModelHierarchy> hierarchyHolder = UCMDBCallHelper.getAllClassesHierarchy();

        Map<String, String> ucmdbClasses = new HashMap<>();
        try {
            List<UcmdbClassHierarchyNode> ucmdbClassHierarchy = hierarchyHolder.value.getClassHierarchyNode();

            ucmdbClassHierarchy
                    .stream()
                    .forEach(ucmdbClass -> {
                        String className = ucmdbClass.getClassNames().getClassName();
                        String displayName = ucmdbClass.getClassNames().getDisplayName();

                        ucmdbClasses.put(className, displayName);
                    });
        } catch (Exception ex) {
            throw new gr.cosmote.ucmdb.commons.exceptions.ApplicationException("BusinessEjb :: getAllClassesHierarchyFromUcmdb :: ERROR: " + ex);
        }

        return ucmdbClasses;
    }

    private boolean isUnnecessaryType(String type) {
        boolean isCiCollectionType = "ci_collection".equals(type);
        boolean isUnixType = "unix".equals(type);
        boolean isNtType = "nt".equals(type);

        // Has Kept -- CiCollection type
        return isUnixType || isNtType;
    }

    private String getBusinessServiceNameFromCiCollection(GenericCI genericCI) {
        String businessServiceName;
        try {
            String ciCollectionName = genericCI.getProperties().get("display_label");

            businessServiceName = ciCollectionName.substring(ciCollectionName.lastIndexOf("of"));
        } catch (Exception ex) {
            businessServiceName = "";
        }

        return businessServiceName;
    }

    private List<GenericCI> retrieveWebServers(String businessServiceGlobalId, boolean retrieveProcess) {
        log.debug("BusinessEjb.retrieveWebServers :: Method Entered for BusinessService with Global Id {}", businessServiceGlobalId);
        String webServersQuery = ApplicationProperties.getInstance().getProperty("WEB_SERVERS_TQL");

        List<GenericCI> webServers = new ArrayList<>();
        try {
            webServers = UCMDBCallHelper.executeTopologyQueryByNameWithParams(webServersQuery, generateParameterizedNodesForWebServersRetrieval(businessServiceGlobalId), getTypedPropertiesCollection(getClassAttributes(WEB_SERVER_CLASS)));

            if (retrieveProcess) {
                List<String> webserversTypes = getWebServerSubTypes();

                // Filter Only Webserver CIs
                webServers = webServers
                        .stream()
                        .filter(ws -> webserversTypes.contains(ws.getType()))
                        .map(ws -> {
                            ws.setType(WEB_SERVER_CLASS);
                            return ws;
                        })
                        .collect(Collectors.toList());
            }
        } catch (Exception ex) {
            log.error("BusinessEjb.retrieveWebServers :: Error", ex);
        }

        return webServers;
    }

    private List<String> getWebServerSubTypes() {
        log.debug("BusinessEjb.getWebServerSubTypes :: Method Entered");
        List<String> ucmdbWebserverSubTypes = new ArrayList<>();

        UcmdbClassModelHierarchy ucmdbClasses = UCMDBCallHelper.getAllClassesHierarchy().value;

        ucmdbWebserverSubTypes = ucmdbClasses.getClassHierarchyNode()
                .stream()
                .filter(className -> className.getClassParentName().equals(WEB_SERVER_CLASS))
                .map(className -> className.getClassNames().getClassName())
                .collect(Collectors.toList());

        log.debug("BusinessEjb.getWebServerSubTypes :: Method Returned Subtypes size of {}", ucmdbWebserverSubTypes.size());
        return ucmdbWebserverSubTypes;
    }

    private List<GenericCI> getCiCollectionDataBasedOnBusinessType(BusinessCiCollectionRequestDTO requestObject) {
        log.debug("BusinessEjb :: getCiCollectionDataBasedOnBusinessType :: Method Entered");

        String businessType;
        try {
            businessType = requestObject.getCiType();
        } catch (NullPointerException ex) {
            log.error("BusinessEjb :: getCiCollectionDataBasedOnBusinessType :: Null Ci Type detected", ex);

            return new ArrayList<>();
        }

        log.debug("BusinessEjb :: getCiCollectionDataBasedOnBusinessType :: Business Type {}", businessType);

        List<GenericCI> result = getBusinessApplicationOrHostNodeElements(requestObject);
        if ("business_application".equals(requestObject.getCiType())) {
            return filterBusinessApplicationBasedOnBusinessService(result, requestObject);
        } else if ("business_service".equals(requestObject.getCiType())) {
            return result;
        } else {
            return new ArrayList<>();
        }
    }

    private List<GenericCI> filterBusinessApplicationBasedOnBusinessService(List<GenericCI> unfilteredResult, BusinessCiCollectionRequestDTO requestObject) {
        log.debug("BusinessEjb.filterBusinessApplicationBasedOnBusinessService :: Method Entered for Ci with Global Id {} and Parent Global Id {}", requestObject.getId(), requestObject.getParentBusinessId());
        List<GenericCI> businessApplicationChildren;

        businessApplicationChildren = getNodeElements(requestObject.getParentBusinessId());

        List<GenericCI> filteredResults =
                unfilteredResult.stream()
                        .filter((unfiltered) ->
                                businessApplicationChildren.stream()
                                        .anyMatch((child) -> child.getId().equals(unfiltered.getId()))
                        )
                        .collect(Collectors.toList());

        log.debug("BusinessEjb.filterBusinessApplicationBasedOnBusinessService :: Method Returned filtered result size of {}", filteredResults.size());
        return filteredResults;
    }

    private List<CI> getChildCiCollectionFromHolder(Topology retrievedCIs, String id) {
        List<String> childrenIds = retrievedCIs.getRelations().getRelation().
                stream()
                .filter((relation -> relation.getEnd1ID().getValue().equals(id)))
                .map((relation -> relation.getEnd2ID().getValue()))
                .collect(Collectors.toList());

        return retrievedCIs.getCIs().getCI()
                .stream()
                .filter((ci -> childrenIds.stream().anyMatch((relId) -> relId.equals(ci.getID().getValue()))))
                .collect(Collectors.toList());
    }

    private List<CI> getCiCollectionsBasedOnServiceType(ID businessServiceId, String ciType) {
        String neighbourType = ciType.equals("business_service") ? "ci_collection" : "object";

        Holder<Topology> holder = UCMDBCallHelper.getCINeighbours(businessServiceId, neighbourType, initTypedPropertiesCollectionForSoapRequests());
        List<CI> cisResponse = null;

        if (ciType.equals("business_service")) {
            cisResponse = holder.value.getCIs().getCI();
        } else if (ciType.equals("business_application")) {
            List<CI> ciCollectionsOfBusinessApplication = getCiCollectionsOfBusinessApplication(holder.value.getCIs());
            cisResponse = filterCiCollectionsBasedOnGlobalId(ciCollectionsOfBusinessApplication);
        }

        return cisResponse;
    }

    private List<CI> getCiCollectionsOfBusinessApplication(CIs cis) {
        List<CI> cisList = cis.getCI().subList(1, cis.getCI().size());
        log.debug("BusinessEjb.getCiCollectionsOfBusinessApplication  :: Method Entered with BusinessApplications size of {}", cisList.size());
        List<CI> ciCollections;

        ciCollections = cisList
                .stream()
                .map(ci -> {
                    Holder<Topology> ciCollection = UCMDBCallHelper.getCINeighbours(ci.getID(), "ci_collection", initTypedPropertiesCollectionForSoapRequests());

                    return ciCollection.value.getCIs().getCI().get(1);
                })
                .collect(Collectors.toList());

        log.debug("BusinessEjb.getCiCollectionsOfBusinessApplication :: Method Returned All Nodes CiCollections size of {}", ciCollections.size());
        return ciCollections;
    }

    private List<CI> filterCiCollectionsBasedOnGlobalId(List<CI> ciCollectionsOfBusinessApplication) {
        log.debug("BusinessEjb.getCiCollectionsOfBusinessApplication :: Method Entered with Unfiltered CiCollections size of {}", ciCollectionsOfBusinessApplication.size());
        List<CI> uniqueCiCollections;

        Map<String, CI> helperMapForExcludingDuplicates = new HashMap<>();
        ciCollectionsOfBusinessApplication
                .stream()
                .forEach(ciCollection -> helperMapForExcludingDuplicates.putIfAbsent(ciCollection.getID().getValue(), ciCollection));

        uniqueCiCollections = new ArrayList<>(helperMapForExcludingDuplicates.values());

        log.debug("BusinessEjb.getCiCollectionsOfBusinessApplication :: Method Returned Filtered CiCollections size of {}", uniqueCiCollections.size());
        return uniqueCiCollections;
    }

    private List<GenericCI> getBusinessApplicationOrHostNodeElements(BusinessCiCollectionRequestDTO requestObject) {
        List<GenericCI> ciChildren = getNodeElements(requestObject.getId());

        if (ciChildren.isEmpty()) {
            ciChildren = getBusinessApplicationOfCi(requestObject.getId());
        }

        return ciChildren;
    }

    private List<GenericCI> getBusinessApplicationOfCi(String id) {
        ID idElm = new ID();
        idElm.setValue(id);

        Holder<Topology> response = UCMDBCallHelper.getCINeighbours(idElm, "business_application", getTypedPropertiesCollection(getClassAttributes("business_application")));

        List<CI> cis = response.value.getCIs().getCI();

        List<GenericCI> businessApplications = mapResponseToGenericCiList(cis, id);

        return businessApplications;
    }

    private List<GenericCI> mapResponseToGenericCiList(List<CI> response, String id) {
        List<GenericCI> mappedList = response
                .stream()
                .filter(ci -> !ci.getID().getValue().equals(id))
                .map((ci) -> {
                    GenericCI genericCi = new GenericCI();
                    genericCi.setId(ci.getID().getValue());
                    genericCi.setType(ci.getType());
                    genericCi.setProperties(UCMDBCallHelper.getProperties(ci.getID().getValue(), response));

                    return genericCi;
                })
                .collect(Collectors.toList());

        return mappedList;
    }

    private TypedPropertiesCollection initTypedPropertiesCollectionForSoapRequests() {
        log.debug("BusinessEjb :: initTypedPropertiesCollectionForCiCollectionRetrieval :: Method Entered");
        TypedPropertiesCollection typedPropertiesCollection = new TypedPropertiesCollection();
        List<TypedProperties> typedPropertiesList = typedPropertiesCollection.getTypedProperties();

        TypedProperties typedProperties = new TypedProperties();
        typedProperties.setType("object");

        CustomTypedProperties customProperties = new CustomTypedProperties();

        PropertiesList propertiesList = new PropertiesList();
        propertiesList.getPropertyName().add("global_id");
        propertiesList.getPropertyName().add("display_label");

        customProperties.setPropertiesList(propertiesList);

        typedProperties.setProperties(customProperties);
        typedPropertiesList.add(typedProperties);

        log.debug("BusinessEjb :: initTypedPropertiesCollectionForCiCollectionRetrieval :: Method Returned Initialized Properties {} ", typedPropertiesCollection);

        return typedPropertiesCollection;
    }

    private List<GenericCI> filterBusinessServicesByTenant(List<CI> results, List<Tenant> tenants) {
        List<String> tenantsList =
                tenants
                        .stream()
                        .map((tenant) -> tenant.getDescription())
                        .collect(Collectors.toList());

        List<GenericCI> filteredByTenantResult =
                results
                        .stream()
                        .map((businessResult) -> {
                            GenericCI genericCI = new GenericCI();
                            genericCI.setId(businessResult.getID().getValue());
                            genericCI.setType(businessResult.getType());
                            genericCI.setProperties(UCMDBCallHelper.getProperties(businessResult.getID().getValue(), results));

                            return genericCI;
                        })
                        .filter((businessResult) ->
                                tenantsList
                                        .stream()
                                        .anyMatch((tenant) -> tenant.contains(businessResult.getProperties().get("TenantOwner"))))
                        .collect(Collectors.toList());

        return filteredByTenantResult;
    }

    private List<GenericCI> filterBusinessServicesBySearchTerm(List<GenericCI> results, String searchTerm) {
        results = results
                .stream()
                .filter((businessResult) -> businessResult.getProperties().get("display_label").toUpperCase().contains(searchTerm.toUpperCase()))
                .collect(Collectors.toList());

        return results;
    }

    private List<GenericCI> removeUnnecessaryProperties(List<GenericCI> result) {
        result =
                result
                        .stream()
                        .map((businessResult) -> {
                            Map<String, String> props = businessResult.getProperties();
                            Map<String, String> newProps = new HashMap<>();
                            newProps.put("display_label", props.get("display_label"));
                            newProps.put("global_id", props.get("global_id"));
                            newProps.put("TenantOwner", props.get("TenantOwner"));

                            businessResult.setProperties(newProps);
                            return businessResult;
                        })
                        .collect(Collectors.toList());

        return result;
    }

    private BusinessResultDTO groupResultsByTenant(List<GenericCI> preGroupedResults) {
        BusinessResultDTO businessResult = new BusinessResultDTO();

        Map<String, List<BusinessServicesDTO>> businessObject =
                preGroupedResults
                        .stream()
                        .map((businessItem) -> {
                            BusinessServicesDTO businessServices = new BusinessServicesDTO();
                            businessServices.setGlobalId(businessItem.getId());
                            businessServices.setDisplayLabel(businessItem.getProperties().get("display_label"));
                            businessServices.setCiType(businessItem.getType());
                            businessServices.setTenantOwner(businessItem.getProperties().get("TenantOwner"));

                            return businessServices;
                        })
                        .collect(Collectors.groupingBy((business) -> business.getTenantOwner()));

        businessResult.setBusiness(businessObject);

        return businessResult;
    }

    private List<GenericCI> getNodeElements(String id) {
        log.debug("BusinessEjb :: getNodeElements :: Method Entered for Ci with GlobalId {}", id);
        List<GenericCI> result;

        List<String> typeAttributes = getClassAttributes("node");

        result = getHostNodeNeighbours(id, typeAttributes);

        if (result.isEmpty()) {
            result = getNetdeviceNeighbours(id, typeAttributes);
        }

        log.debug("BusinessEjb :: getNodeElements :: Method Returned Node Elements size of {}", result.size());
        return result;
    }

    private List<GenericCI> getHostNodeNeighbours(String globalId, List<String> typeAttributes) {
        log.debug("BusinessEjb :: getHostNodeNeighbours :: Method Entered for Ci with Global Id {}", globalId);
        List<GenericCI> hostNodeProperties;

        hostNodeProperties = getCINeighbours(globalId, "host_node", typeAttributes);

        log.debug("BusinessEjb :: getHostNodeNeighbours :: Method Returned list of host node size of {}", hostNodeProperties.size());
        return hostNodeProperties;
    }

    private List<GenericCI> getNetdeviceNeighbours(String globalId, List<String> typeAttributes) {
        log.debug("BusinessEjb :: getNetdeviceNeighbours :: Method Entered for Ci with Global Id {}", globalId);
        List<GenericCI> hostNodeProperties;

        hostNodeProperties = getCINeighbours(globalId, "netdevice", typeAttributes);

        log.debug("BusinessEjb :: getNetdeviceNeighbours :: Method Returned list of host node size of {}", hostNodeProperties.size());
        return hostNodeProperties;
    }

    private List<GenericCI> getBusinessApplications(String globalId, List<String> typeAttributes) {
        log.debug("BusinessEjb :: getBusinessApplications :: Method Entered for Ci with Global Id {}", globalId);
        List<GenericCI> businessApplicationProperties;

        businessApplicationProperties = getCINeighbours(globalId, "business_application", typeAttributes);

        log.debug("BusinessEjb :: getBusinessApplications :: Method Returned list of host node size of {}", businessApplicationProperties.size());
        return businessApplicationProperties;
    }

    private Holder<CIs> callWebMethodBasedOnCiType(String type, CustomProperties customProperties, TypedPropertiesCollection typedPropertiesCollection) {
        if (type.contains("application")) {
            List<TenantSearchableType> tenantTypes = entityManager.createQuery(GET_TENANT_SEARCHABLE_TYPES_BY_TYPE, TenantSearchableType.class).setParameter("type", "%application%").getResultList();

            List<String> tenantsWithbusinessApplications =
                    tenantTypes.stream()
                            .map((tenantType) -> entityManager.createQuery(GET_TENANTS_BY_ID, Tenants.class).setParameter("id", tenantType.getTenantId()).getSingleResult().getDescription())
                            .collect(Collectors.toList());

            return UCMDBCallHelper.getFilteredCIsByType(type, "OR", customProperties, initConditionsObjectForBusinessApplicationFilteredResults(tenantsWithbusinessApplications));
        } else {
            return UCMDBCallHelper.getCIsByType(type, customProperties, typedPropertiesCollection);
        }
    }

    private Conditions initConditionsObjectForBusinessApplicationFilteredResults(List<String> businessApplicationTenants) {
        Conditions conditions = new Conditions();
        StrConditions tenantConditions = new StrConditions();

        // For Each Tenant Add Condition
        businessApplicationTenants.stream()
                .forEach(tenant -> {
                    StrCondition tenantCondition = new StrCondition();

                    StrProp tenantProp = new StrProp();
                    tenantProp.setName("TenantOwner");
                    tenantProp.setValue(new ObjectFactory().createStrPropValue(tenant));

                    tenantCondition.setCondition(tenantProp);
                    tenantCondition.setStrOperator("Equal");

                    tenantConditions.getStrCondition().add(tenantCondition);
                });

        conditions.setStrConditions(tenantConditions);

        return conditions;
    }

    private List<CI> getFilteredCIsByBusinessServiceName(String type, String businessServiceName, String tenant) {
        log.debug("BusinessEjb :: getFilteredCIsByBusinessServiceName :: Method Entered for Business type {} with Business Name {} of Tenant {}", type, businessServiceName, tenant);
        List<CI> cis;

        cis = UCMDBCallHelper.getFilteredCIsByType(type, "AND", initCustomProperties(type), initConditionsObjectForExcelExport(businessServiceName, tenant)).value.getCI();

        log.debug("BusinessEjb :: getFilteredCIsByBusinessServiceName :: Method Returned CIs List size of {} ", cis.size());
        return cis;
    }

    private Conditions initConditionsObjectForExcelExport(String businessServiceName, String tenant) {
        Conditions conditions = new Conditions();

        StrConditions strConditions = new StrConditions();

        StrCondition displayLabelCondition = new StrCondition();
        StrProp displayLabelProp = new StrProp();
        displayLabelProp.setName("display_label");
        displayLabelProp.setValue(new ObjectFactory().createStrPropValue(businessServiceName));
        displayLabelCondition.setCondition(displayLabelProp);
        displayLabelCondition.setStrOperator("Equal");

        StrCondition tenantCondition = new StrCondition();
        StrProp tenantProp = new StrProp();
        tenantProp.setName("TenantOwner");
        tenantProp.setValue(new ObjectFactory().createStrPropValue(tenant));
        tenantCondition.setCondition(tenantProp);
        tenantCondition.setStrOperator("Equal");

        strConditions.getStrCondition().add(displayLabelCondition);
        strConditions.getStrCondition().add(tenantCondition);

        conditions.setStrConditions(strConditions);
        return conditions;
    }

    private CustomProperties initCustomProperties(String type) {
        CustomProperties customProperties = new CustomProperties();

        PropertiesList propertiesList = new PropertiesList();
        propertiesList.getPropertyName().addAll(getClassAttributes(type));

        customProperties.setPropertiesList(propertiesList);

        return customProperties;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<String> getClassAttributes(String className) {
        log.debug("BusinessEjb :: getClassAttributes :: method entered with className: {}", className);

        List<String> attributes = new ArrayList<>();
        List<CiAttributeConfiguration> temp_attributes = entityManager.createNativeQuery(GET_CI_TYPE_ATTRIBUTES, CiAttributeConfiguration.class).setParameter(1, className).getResultList();
        for (CiAttributeConfiguration attribute : temp_attributes) {
            attributes.add(attribute.getAttributeName());
        }

        log.debug("BusinessEjb :: getClassAttributes :: method returning: {}", attributes);
        return attributes;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public TypedPropertiesCollection getTypedPropertiesCollection(List<String> properties) {
        TypedPropertiesCollection typedPropertiesCollection = new TypedPropertiesCollection();
        typedPropertiesCollection.getTypedProperties();

        TypedProperties typedProperties = new TypedProperties();
        typedProperties.setType("object");

        CustomTypedProperties customTypedProperties = new CustomTypedProperties();
        PropertiesList propertiesList = new PropertiesList();
        for (String property : properties) {
            propertiesList.getPropertyName().add(property);
        }
        customTypedProperties.setPropertiesList(propertiesList);

        typedProperties.setProperties(customTypedProperties);
        typedPropertiesCollection.getTypedProperties().add(typedProperties);
        return typedPropertiesCollection;
    }

    private List<String> getSearchableTypesForTenants(List<Tenant> tenants) {
        log.debug("BusinessEjb :: getSearchableTypesForTenants :: method entered with tenantName: {}", tenants);

        Set<String> types = new HashSet<>();

        for (Tenant tenant : tenants) {
            List<TenantSearchableType> tempTypes = getSearchableTypes(tenant.getDescription());
            for (TenantSearchableType type : tempTypes) {
                types.add(type.getType());
            }
        }

        log.debug("BusinessEjb :: getSearchableTypesForTenants :: method returning: {}", types);
        return new ArrayList<>(types);
    }

    private List<TenantSearchableType> getSearchableTypes(String tenantName) {
        log.debug("BusinessEjb :: getSearchableTypes :: method entered with tenantName: {}", tenantName);

        List<TenantSearchableType> types = entityManager.createNativeQuery(GET_TENANT_SEARCHABLE_TYPES_SQL, TenantSearchableType.class).setParameter(1, tenantName).getResultList();

        log.debug("BusinessEjb :: getSearchableTypes :: method returning {}", types);
        return types;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<CI> getCIsByType(String type, TypedPropertiesCollection typedPropertiesCollection) {
        log.debug("BusinessEjb :: getCIsByType :: method entered with type: {}", type);

        CustomProperties customProperties = new CustomProperties();
        PropertiesList propertiesList = new PropertiesList();
        propertiesList.getPropertyName().addAll(getClassAttributes(type));
        customProperties.setPropertiesList(propertiesList);

        Holder<CIs> holder = callWebMethodBasedOnCiType(type, customProperties, typedPropertiesCollection);
        if (holder != null) {
            log.debug("BusinessEjb :: getCIsByType :: method returning list of size: {}", holder.value.getCI().size());
            return holder.value.getCI();
        }

        log.debug("BusinessEjb :: getCIsByType :: method returning empty list");
        return new ArrayList<>();
    }

    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public List<GenericCI> getCINeighbours(String id, String type, List<String> properties) {
        log.debug("BusinessEjb :: getCINeighbours :: method entered with id: {}", id);

        ID tempId = new ID();
        tempId.setValue(id);

        TypedPropertiesCollection typedPropertiesCollection = new TypedPropertiesCollection();

        TypedProperties typedProperties = new TypedProperties();
        typedProperties.setType(type);

        CustomTypedProperties customTypedProperties = new CustomTypedProperties();
        PropertiesList propertiesList = new PropertiesList();
        for (String property : properties) {
            propertiesList.getPropertyName().add(property);
        }
        customTypedProperties.setPropertiesList(propertiesList);

        typedProperties.setProperties(customTypedProperties);
        typedPropertiesCollection.getTypedProperties().add(typedProperties);

        List<CI> cis = UCMDBCallHelper.getCINeighbours(tempId, type, typedPropertiesCollection).value.getCIs().getCI();
        cis.remove(0);

        List<GenericCI> results =
                cis.stream()
                        .map((ci) -> {
                            GenericCI genericCI = new GenericCI();
                            genericCI.setId(ci.getID().getValue());
                            genericCI.setType(type);
                            genericCI.setProperties(UCMDBCallHelper.getProperties(ci.getID().getValue(), cis));

                            return genericCI;
                        })
                        .collect(Collectors.toList());

        log.debug("BusinessEjb :: getCINeighbours :: method returning Properties size of : {}", results.size());
        return results;
    }

    private List<GenericCI> getGenericCINeighbours(String id, String ciType, String parentName) {
        log.debug("BusinessEjb :: getCINeighbours :: method entered with id: {}, ciType: {}", id, ciType);

        ID idElement = new ID();
        idElement.setValue(id);

        List<CI> ciNeighbours =
                UCMDBCallHelper.getCINeighbours(idElement, ciType, getTypedPropertiesCollection(getClassAttributes(ciType)))
                        .value.getCIs().getCI();

        return ciNeighbours
                .stream()
                .filter(ci -> ci.getType().equals(ciType))
                .map(ciNeighbour -> {
                    GenericCI genericCI = new GenericCI();
                    genericCI.setId(ciNeighbour.getID().getValue());
                    genericCI.setType(ciNeighbour.getType());
                    genericCI.setProperties(UCMDBCallHelper.getProperties(ciNeighbour.getID().getValue(), ciNeighbours));
                    // Add Parent Node Name
                    if (parentName != null) genericCI.getProperties().put("parentName", parentName);

                    return genericCI;
                })
                .collect(Collectors.toList());
    }

    private List<ParameterizedNode> generateParameterizedNodesForWebServersRetrieval(String businessServiceGlobalId) {
        List<ParameterizedNode> parameterizedNodesList = new ArrayList<>();

        // Init Str Prop
        StrProp strProp = new StrProp();
        strProp.setName("global_id");
        ObjectFactory factory = new ObjectFactory();
        JAXBElement<String> strPropValue = factory.createStrPropValue(businessServiceGlobalId);
        strProp.setValue(strPropValue);

        // Init StrProps
        StrProps strProps = new StrProps();
        strProps.getStrProp().add(strProp);

        // Init CIProperites
        CIProperties ciProperties = new CIProperties();
        ciProperties.setStrProps(strProps);

        // Init ParameterizedNode
        ParameterizedNode parameterizedNode = new ParameterizedNode();
        parameterizedNode.setNodeLabel("BusinessService");
        parameterizedNode.setParameters(ciProperties);

        parameterizedNodesList.add(parameterizedNode);

        return parameterizedNodesList;
    }

    // -------------------- END OF HELPER METHODS -------------------------------------

    // ----------------------- EXCEL METHODS ------------------------------------------
    @TransactionAttribute(value = TransactionAttributeType.NEVER)
    public BusinessResult getBusinessService(ExcelCriteria criteria) {

        log.debug("BusinessEjb :: getBusinessService :: Method Entered for Business Service {} of Tenant {}", criteria.getBusinessServiceName(), criteria.getTenant());

        if (SEARCHCACHE == null) {
            SEARCHCACHE = CacheBuilder.newBuilder().refreshAfterWrite(6, TimeUnit.HOURS).maximumSize(50).build(
                    new CacheLoader<ExcelCriteria, BusinessResult>() {
                        @Override
                        public BusinessResult load(ExcelCriteria key) throws Exception {
                            TenantSearchableType tenantSearchableType = (TenantSearchableType) entityManager.createNativeQuery(GET_TENANT_SEARCHABLE_TYPES_SQL, TenantSearchableType.class)
                                    .setParameter(1, key.getTenant()).getSingleResult();

                            CI ci = getFilteredCIsByBusinessServiceName(tenantSearchableType.getType(), key.getBusinessServiceName(), key.getTenant()).get(0);

                            BusinessResult result;
                            if ("business_service".equals(tenantSearchableType.getType())) {
                                result = getCiChildrenAndConstructExcelObjectForBusinessServices(ci);
                            } else {
                                result = getCiChildrenAndConstructExcelObjectForBusinessApplications(ci, criteria.getBusinessServiceName());
                            }

                            log.debug("BusinessEjb :: getBusinessService :: Method Returned {}", result);
                            return result;
                        }
                    });
        }

        try {
            log.debug("BusinessEjb.getBusinessService ::: returning from cache");
            return SEARCHCACHE.get(criteria);
        } catch (Exception e) {
            log.error("BusinessEjb.getBusinessService ::: Error in getBusinessService method", e);
            return null;
        }
    }

    private BusinessResult getCiChildrenAndConstructExcelObjectForBusinessServices(CI ci) {
        BusinessResult result = new BusinessResult();

        List<CI> cis = new ArrayList<>();
        cis.add(ci);

        Map<String, String> itemProperties = UCMDBCallHelper.getProperties(ci.getID().getValue(), cis);
        result.setId(ci.getID().getValue());
        result.setProperties(itemProperties);
        result.setCiCollection(getCIList(ci.getID().getValue()));

        return result;
    }

    private BusinessResult getCiChildrenAndConstructExcelObjectForBusinessApplications(CI ci, String businessApplicationName) {
        // Get Business Application CiCollection
        ID businessApplicationId = new ID();
        businessApplicationId.setValue(ci.getID().getValue());

        Topology businessApplicationChildrenTopology = UCMDBCallHelper.getCINeighbours(businessApplicationId, "object", initTypedPropertiesCollectionForSoapRequests()).value;
        List<CI> unfilteredCiCollections = getCiCollectionsOfBusinessApplication(businessApplicationChildrenTopology.getCIs());
        List<CI> uniqueCiCollections = filterCiCollectionsBasedOnGlobalId(unfilteredCiCollections);

        // Get CiCollection Data
        BusinessResult result = new BusinessResult();

        List<CI> cis = new ArrayList<>();
        cis.add(ci);

        Map<String, String> itemProperties = UCMDBCallHelper.getProperties(ci.getID().getValue(), cis);
        result.setId(ci.getID().getValue());
        result.setProperties(itemProperties);
        result.setCiCollection(getCIListForBusinessApplication(businessApplicationChildrenTopology, uniqueCiCollections, businessApplicationName));

        return result;
    }

    private List<Ci> getCIListForBusinessApplication(Topology businessApplicationChildrenTopology, List<CI> ciCollections, String businessApplicationName) {
        log.debug("BusinessEjb :: getCIListForBusinessApplication :: method entered for business Application {}", businessApplicationName);
        List<Ci> returningCIs = new ArrayList<>();

        List<CI> businessApplicationChildren = businessApplicationChildrenTopology.getCIs().getCI();
        for (CI ciCollection : ciCollections) {
            // Filter Results based on Business Application Children
            List<CI> ciCollectionOfBAChildren = UCMDBCallHelper.getCINeighbours(ciCollection.getID(), "host_node", getTypedPropertiesCollection(getClassAttributes("host_node"))).value.getCIs().getCI();

            List<CI> filteredCiCollectionofBAChildren = ciCollectionOfBAChildren.stream()
                    .filter((ciCollectionItem) -> businessApplicationChildren.stream().anyMatch(baChild -> baChild.getID().getValue().equals(ciCollectionItem.getID().getValue())))
                    .collect(Collectors.toList());

            // Construct Object
            List<CI> ciCollectionList = new ArrayList<>();
            ciCollectionList.add(ciCollection);

            Ci businessApplicationCiCollection = new Ci();
            businessApplicationCiCollection.setId(ciCollection.getID().getValue());
            businessApplicationCiCollection.setProperties(UCMDBCallHelper.getProperties(ciCollection.getID().getValue(), ciCollectionList));
            businessApplicationCiCollection.setComputers(new ArrayList<>());

            filteredCiCollectionofBAChildren.stream()
                    .forEach((child) -> {
                        Computer computer = new Computer();
                        computer.setId(child.getID().getValue());
                        computer.setProperties(UCMDBCallHelper.getProperties(child.getID().getValue(), filteredCiCollectionofBAChildren));
                        computer.setAttributes(getAttributes(child.getID().getValue()));

                        businessApplicationCiCollection.getComputers().add(computer);
                    });

            returningCIs.add(businessApplicationCiCollection);
        }

        log.debug("BusinessEjb :: getCIList :: method returning list: ", returningCIs);
        return returningCIs;
    }

    @TransactionAttribute(value = TransactionAttributeType.NEVER)
    private List<Ci> getCIList(String id) {
        log.debug("BusinessEjb :: getCIList :: method entered with id: {}", id);
        List<Ci> returningCIs = new ArrayList<>();

        ID tempId = new ID();
        tempId.setValue(id);

        String type = "ci_collection";

        TypedPropertiesCollection typedPropertiesCollection = getTypedPropertiesCollection(getClassAttributes(type));

        Holder<Topology> holder = UCMDBCallHelper.getCINeighbours(tempId, type, typedPropertiesCollection);

        // Already Visited CiCollections (if CiCollection has Nested CiCollections)
        List<String> alreadyVisitedCiCollectionsGlobalIds = new ArrayList<>();

        List<Relation> relations = holder.value.getRelations().getRelation();
        for (Relation relation : relations) {
            String relationId = relation.getEnd2ID().getValue();
            if (!relationId.equals(id)) {
                Ci ci = new Ci();
                ci.setId(relation.getEnd2ID().getValue());
                ci.setProperties(UCMDBCallHelper.getProperties(relationId, holder.value.getCIs().getCI()));
                ci.setComputers(getComputers(relationId));
                if (ci.getComputers().size() == 0) {
                    // Check for Nested Ci Collections
                    List<Ci> nestedCiCollections = getNestedCiCollectionsForExcel(relationId, alreadyVisitedCiCollectionsGlobalIds);

                    // If no Nested Ci Collections found then check for Business Applications
                    if (nestedCiCollections.isEmpty()) {
                        ci.setNetdevices(getNetDevicesForExcel(relationId));
//                        ci.setBusinessApplications(getBusinessApplicationsForExcel(relationId));

                        // Add Ci Collection only if it's not Empty
                        if (!(ci.getBusinessApplications() == null && ci.getNetdevices() == null) && alreadyVisitedCiCollectionsGlobalIds.isEmpty()) {
                            returningCIs.add(ci);
                        }
                    } else {
                        returningCIs.addAll(nestedCiCollections);
                    }
                } else {
                    returningCIs.add(ci);
                }
            }
        }

        log.debug("BusinessEjb :: getCIList :: method returning list: ", returningCIs);
        return returningCIs;
    }

    private List<Ci> getNestedCiCollectionsForExcel(String id, List alreadyVisitedCiCollections) {
        List<Ci> nestedCiCollections;

        // Create Request Object
        BusinessCiCollectionRequestDTO requestDTO = new BusinessCiCollectionRequestDTO();
        requestDTO.setId(id);

        // Call WS Method
        List<GenericCI> expandedCiCollectionResponse = expandCiCollection(requestDTO);

        nestedCiCollections =
                expandedCiCollectionResponse
                        .stream()
                        .map(genericCI -> {
                            if (!alreadyVisitedCiCollections.contains(genericCI.getId())) {
                                alreadyVisitedCiCollections.add(genericCI.getId());

                                Ci ci = new Ci();
                                ci.setId(genericCI.getId());
                                ci.setType(genericCI.getType());
                                ci.setProperties(genericCI.getProperties());
                                ci.setComputers(getComputers(ci.getId()));

                                if (ci.getComputers().size() == 0) {
                                    ci.setComputers(new ArrayList<>());
//                                    ci.setBusinessApplications(getBusinessApplicationsForExcel(ci.getId()));
                                    ci.setNetdevices(getNetDevicesForExcel(ci.getId()));
                                }

                                if (!(ci.getComputers().isEmpty() && ci.getBusinessApplications() == null && ci.getNetdevices() == null)) {
                                    return ci;
                                } else {
                                    return null;
                                }
                            } else {
                                return null;
                            }
                        })
                        .filter(ci -> ci != null)
                        .collect(Collectors.toList());

        return nestedCiCollections;
    }

    private List<Ci_Simple> getNetDevicesForExcel(String globalId) {
        List<String> nodeAttributes = getClassAttributes("node");
        List<GenericCI> netdevices = getNetdeviceNeighbours(globalId, nodeAttributes);

        List<Ci_Simple> netdevicesCis =
                netdevices.stream()
                        .map(netdevice -> {
                            Ci_Simple ci = new Ci_Simple();
                            ci.setId(netdevice.getId());
                            ci.setProperties(netdevice.getProperties());

                            return ci;
                        })
                        .collect(Collectors.toList());

        return netdevicesCis.isEmpty() ? null : netdevicesCis;
    }

    private List<Ci_Simple> getBusinessApplicationsForExcel(String id) {
        List<Ci_Simple> businessApplications;

        List<GenericCI> businessApplicationsGenericCi = getBusinessApplicationOfCi(id);

        businessApplications =
                businessApplicationsGenericCi.stream()
                        .map((ba) -> {
                            Ci_Simple businessApplication = new Ci_Simple();
                            businessApplication.setId(ba.getId());
                            businessApplication.setProperties(ba.getProperties());

                            return businessApplication;
                        })
                        .collect(Collectors.toList());

        return businessApplications.isEmpty() ? null : businessApplications;
    }

    @TransactionAttribute(value = TransactionAttributeType.NEVER)
    private List<Computer> getComputers(String id) {
        log.debug("BusinessEjb :: getComputers :: method entered with id: {}", id);
        List<Computer> returningComputers = new ArrayList<>();

        ID tempId = new ID();
        tempId.setValue(id);

        String ciType = "host_node";

        TypedPropertiesCollection typedPropertiesCollection = getTypedPropertiesCollection(getClassAttributes(ciType));
        Holder<Topology> holder = UCMDBCallHelper.getCINeighbours(tempId, ciType, typedPropertiesCollection);

        for (Relation relation : holder.value.getRelations().getRelation()) {
            if (!relation.getEnd2ID().getValue().equals(id)) {
                Computer computer = new Computer();
                computer.setId(relation.getEnd2ID().getValue());
                computer.setProperties(UCMDBCallHelper.getProperties(relation.getEnd2ID().getValue(), holder.value.getCIs().getCI()));
                computer.setAttributes(getAttributes(relation.getEnd2ID().getValue()));
                returningComputers.add(computer);
            }
        }

        log.debug("BusinessEjb :: getComputers :: method returning list of size: {}, list: ", returningComputers.size(), returningComputers);
        return returningComputers;
    }

    @TransactionAttribute(value = TransactionAttributeType.NEVER)
    private List<Ci> getAttributes(String id) {
        log.debug("BusinessEjb :: getAttributes :: method entered with id: {}", id);
        List<Ci> returningItems = new ArrayList<>();

        ID tempId = new ID();
        tempId.setValue(id);

        TypedPropertiesCollection typedPropertiesCollection = getTypedPropertiesCollection(SEARCH_PROPERTIES);

        Holder<Topology> holder = UCMDBCallHelper.getCINeighbours(tempId, "object", typedPropertiesCollection);

        for (CI item : holder.value.getCIs().getCI()) {
            if (!item.getID().getValue().equals(id)) {
                Ci ci = new Ci();
                ci.setId(item.getID().getValue());
                Map<String, String> props = UCMDBCallHelper.getProperties(item.getID().getValue(), holder.value.getCIs().getCI());
                props.put("type", item.getType());
                ci.setProperties(props);

                returningItems.add(ci);
            }
        }
        log.debug("BusinessEjb :: getAttributes :: method returning list of size: {}, list: ", returningItems.size(), returningItems);
        return returningItems;
    }

    // ----------------------- END OF EXCEL METHODS ----------------------

    private static final String GET_TENANT_SEARCHABLE_TYPES_SQL =
            "SELECT TST.TENANT_ID, TST.TYPE FROM TENANT_SEARCHABLE_TYPES TST, TENANTS T " +
                    " WHERE TST.TENANT_ID = T.TENANT_ID AND T.DESCRIPTION = ? ";

    private static final String GET_TENANT_SEARCHABLE_TYPES_BY_TYPE =
            "SELECT TenantType FROM TenantSearchableType TenantType " +
                    " WHERE TenantType.type LIKE :type";

    private static final String GET_TENANTS_BY_ID = "SELECT Tenant from Tenants Tenant WHERE Tenant.id = :id";

    private static final String GET_CI_TYPE_ATTRIBUTES =
            "SELECT ATTRIBUTE_ID, CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL FROM CI_ATTRIBUTES WHERE CI_TYPE = ? ";

    private static final String GET_WORKFLOW_ATTRIBUTES =
            "SELECT ATTRIBUTE_ID, CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL FROM CI_ATTRIBUTES WHERE CI_TYPE = ? AND IS_WORKFLOW_RELATED = 1";

}