package gr.cosmote.ucmdb.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import gr.cosmote.ucmdb.commons.exceptions.ApplicationException;
import gr.cosmote.ucmdb.models.base.NameCodeObject;
import gr.cosmote.ucmdb.models.database.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;

@Stateless
@Remote(SearchRemote.class)
@Slf4j
public class SearchEjb {
    @PersistenceContext(unitName = "ucmdbPU", type = PersistenceContextType.TRANSACTION)
    EntityManager entityManager;

    @EJB
    UserRemote userEjb;

    @EJB
    TenantRemote tenantEjb;

    private static final String GET_ALL_SEARCHES_SQL = "SELECT Searches FROM SavedSearches Searches";
    private static final String GET_USER_SEARCHES_SQL = "SELECT Searches FROM SavedSearches Searches WHERE Searches.userId=:userId";
    private static final String DELETE_SEARCH_BY_ID = "DELETE FROM SAVED_SEARCHES WHERE SEARCH_ID=?";

    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public List<SavedSearches> getSearches() {
        log.debug("SearchEjb :: getSearches :: Method Entered Retrieving Saved Searches");

        UcmdbUsers user;
        List<SavedSearches> searches;
        try {
            user = userEjb.getUser();

            if (user.getAdmin() != null) {
                searches = user.getAdmin().equals("1") ? getAllSearches() : getUserSearches(user.getId());
            } else {
                searches = getUserSearches(user.getId());
            }

        } catch (Exception ex) {
            throw new ApplicationException("SearchEjb :: getSearches :: ERROR {}", ex);
        }

        log.debug("SearchEjb :: getSearches :: Method Returned {}", searches);
        return searches;
    }

    public SavedSearches ucmdbInsertSearch(SavedSearches savedSearches) {
        log.debug("SearchEjb :: ucmdbInsertSearch :: Method Entered for Saving Search with Name {}", savedSearches.getDescription());

        SavedSearches searchToInsert = null;
        if (savedSearches != null) {
            log.debug("SearchEjb :: ucmdbInsertSearch :: method entered with input {}", savedSearches.toString());
            try {
                searchToInsert = new SavedSearches();

                searchToInsert.setCreationDate(new Date());
                searchToInsert.setDescription(savedSearches.getDescription());
                searchToInsert.setSearchTerm(savedSearches.getSearchTerm());
                searchToInsert.setSearchType(savedSearches.getSearchType());
                searchToInsert.setUserId(userEjb.getUser().getId());

                // Get all Tenants from DB and Filter those who are included in SavedSearch Object
                List<Tenants> tenants = tenantEjb.getAllTenants();
                searchToInsert.setTenants
                        (tenants.stream()
                                .filter(tenant -> savedSearches.getTenants()
                                        .stream()
                                        .anyMatch(savedTenants -> savedTenants.getId() == tenant.getId()))
                                .collect(Collectors.toList()));

                searchToInsert.setUcmdbUsers(entityManager.find(UcmdbUsers.class, searchToInsert.getUserId()));
                searchToInsert.setOptions(savedSearches.getSearchOptions());

                entityManager.persist(searchToInsert);
            } catch (Exception e) {
                log.error("SearchEjb :: ucmdbInsertSearch :: ERROR {}", e);
            }
        }

        log.debug("SearchEjb :: ucmdbInsertSearch :: Method Saved User Search -- ID: {}", searchToInsert.getId());
        return searchToInsert;
    }

    public Boolean ucmdbRemoveSearch(int searchId) {
        log.debug("SearchEjb :: ucmdbRemoveSearch :: method entered for Search with ID: {}", searchId);

        try {
            entityManager.createNativeQuery(DELETE_SEARCH_BY_ID).setParameter(1, searchId).executeUpdate();
        } catch (Exception e) {
            throw new ApplicationException("SearchEjb :: ucmdbRemoveSearch :: ERROR {}", e);
        }

        log.debug("SearchEjb :: ucmdbRemoveSearch :: Method Successfully remove Search");
        return true;
    }

    private List<SavedSearches> getAllSearches() throws Exception {
        log.debug("SearchEjb :: getAllSearches :: Method Entered");

        List<SavedSearches> dbSearches = entityManager.createQuery(GET_ALL_SEARCHES_SQL, SavedSearches.class).getResultList();

        return dbSearches
                .stream()
                .map((search) -> {
                    search.setSearchOptions(search.getOptions());
                    return search;
                })
                .collect(Collectors.toList());
    }

    private List<SavedSearches> getUserSearches(int userId) {
        log.debug("SearchEjb :: getUserSearches :: Method Entered for User with Id {}", userId);

        List<SavedSearches> dbSearches = entityManager.createQuery(GET_USER_SEARCHES_SQL, SavedSearches.class)
                .setParameter("userId", userId)
                .getResultList();

        return dbSearches
                .stream()
                .map((search) -> {
                    search.setSearchOptions(search.getOptions());
                    return search;
                })
                .collect(Collectors.toList());
    }
}