package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.models.database.Group;
import gr.cosmote.ucmdb.models.database.Task;
import gr.cosmote.ucmdb.models.enumeration.OutputCommandEnum;
import gr.cosmote.ucmdb.models.rules.RulesEngineCommand;
import gr.cosmote.ucmdb.commands.Command;
import gr.cosmote.ucmdb.commands.UpdateUcmdbCommand;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.List;
import java.util.Optional;

import gr.cosmote.ucmdb.commands.SendEmailCommand;

@Stateless
public class CommandDispatcherBean {
    @EJB
    private UpdateNodeRemote updateNodeRemote;

    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public void dispatch(RulesEngineCommand<OutputCommandEnum> command) {
        List<OutputCommandEnum> commands = command.getCommands();
        Optional.ofNullable(commands).ifPresent(x -> {
            commands.forEach(c -> {
                Command dispatchCommand = dispatchCommand(c, command.getTask());
                if (dispatchCommand != null)
                    dispatchCommand.execute();
            });
        });
    }

    private Command dispatchCommand(OutputCommandEnum outputCommandEnum, final Task task) throws gr.cosmote.ucmdb.commons.exceptions.ApplicationException {
        switch (outputCommandEnum) {
            case UPDATE_UCMDB_COMMAND:
                return new UpdateUcmdbCommand(task) {
                    @Override
                    public void execute() {
                        updateNodeRemote.updateUcmdb(task.getGlobalId(), task.getAttributes());
                    }
                };
            case SEND_EMAIL_COMMAND:
                return new SendEmailCommand(task) {
                    @Override
                    public void execute() {
                        Group taskGroup = task.getUserGroup();
                        if (taskGroup != null && taskGroup.getEmail() != null) {
                            executeMailCommand(task);
                        }
                    }
                };
            case ADD_OR_UPDATE_TASK_IN_TASK_WRAPPER_COMMAND:
                //TODO: Check if it should be handled somehow in the future
                return null;
            case DELETE_TASK_FROM_TASK_WRAPPER_COMMAND:
                //TODO: Check if it should be handled somehow in the future
                return null;
            default:
                throw new gr.cosmote.ucmdb.commons.exceptions.ApplicationException("CommandDispatcher.dispatchCommand :: Invalid output command " + outputCommandEnum.toString());
        }
    }
}
