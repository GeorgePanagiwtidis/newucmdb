package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.models.database.Task;
import gr.cosmote.ucmdb.models.enumeration.InputCommandEnum;
import gr.cosmote.ucmdb.models.enumeration.OutputCommandEnum;
import gr.cosmote.ucmdb.models.rules.RulesEngineCommand;

import javax.ejb.Remote;
import java.io.Serializable;
import java.util.List;

@Remote
public interface RulesEngineServiceRemote extends Serializable {
    List<RulesEngineCommand<OutputCommandEnum>> evaluateRules(List<RulesEngineCommand<InputCommandEnum>> inCommandList);
    Task reassignTask(Task task, String groupId, String reassignReason);

}
