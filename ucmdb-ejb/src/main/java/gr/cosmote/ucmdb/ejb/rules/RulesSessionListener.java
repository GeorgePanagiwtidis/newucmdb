package gr.cosmote.ucmdb.ejb.rules;

import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class RulesSessionListener implements HttpSessionListener {
    private static final Logger log = LoggerFactory.getLogger(RulesSessionListener.class);

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        log.info("new session created");
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        HttpSession session = httpSessionEvent.getSession();
        KieSession kSession = (KieSession) session.getAttribute(RuleEngineSessionFactory.SessionAttrName);
        boolean hadRules = kSession != null;
        if (hadRules) {
            session.setAttribute(RuleEngineSessionFactory.SessionAttrName, null);
            kSession.dispose();
        }

        log.info("session destroyed (had rules session: {})", hadRules);
    }
}
