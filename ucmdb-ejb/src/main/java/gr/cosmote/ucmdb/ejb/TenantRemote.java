package gr.cosmote.ucmdb.ejb;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Remote;

import gr.cosmote.ucmdb.commons.exceptions.ApplicationException;
import gr.cosmote.ucmdb.models.database.Group;
import gr.cosmote.ucmdb.models.database.Tenants;
import gr.cosmote.ucmdb.models.view.Tenant;

@Remote
public interface TenantRemote extends Serializable {

    List<Tenants> getTenantsFromUcmdb();
    List<Tenants> getAllTenants();
    List<Tenants> ucmdbInsertUcmdbTenants(List<Tenants> tenants);
    void updateTenant(Tenants tenant) throws ApplicationException;
}
