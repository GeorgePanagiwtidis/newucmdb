package gr.cosmote.ucmdb.ejb;

import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.*;

import gr.cosmote.ucmdb.commons.exceptions.ApplicationException;
import gr.cosmote.ucmdb.commons.helper.MailHelper;
import gr.cosmote.ucmdb.models.base.NameCodeObject;
import gr.cosmote.ucmdb.models.database.UcmdbUsers;
import gr.cosmote.ucmdb.models.view.UserInfoDTO;
import gr.cosmote.ucmdb.models.view.UserLoginInfo;
import lombok.extern.slf4j.Slf4j;


@Stateless
@Remote(UserRemote.class)
@Slf4j
public class UserEjb {

    @EJB
    GroupRemote groupEjb;

    @PersistenceContext(unitName = "ucmdbPU", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    @Resource
    private SessionContext sessionContext;

    public UcmdbUsers getUser() {
        log.debug("UserEjb :: getUser :: method entered");

        final String username = sessionContext.getCallerPrincipal().getName();
        log.debug("UserEjb :: getUser :: Username: {}", username);

        UcmdbUsers databaseUcmdbUsers = null;
        try {
            databaseUcmdbUsers =
                    (UcmdbUsers) entityManager.createNativeQuery(GET_USER_SQL, UcmdbUsers.class)
                            .setParameter(1, username).getSingleResult();
        }
        catch (NoResultException nre) {
            throw nre;
        } catch (Exception ex) {
            log.error("UserEjb :: getUser :: ERROR {}", ex);
        }

        log.debug("UserEjb :: getUser :: method returning {}", databaseUcmdbUsers);
        return databaseUcmdbUsers;
    }

    public int ucmdbLogin(UserLoginInfo userLoginInfo) {
        log.debug("UserEjb :: ucmdbLogin :: method entered with {}", userLoginInfo);

        log.debug("UserEjb :: ucmdbLogin :: method returning {}", 1);
        return 1;
    }

    public UserInfoDTO getUserInfo() {
        String username = sessionContext.getCallerPrincipal().getName();
        log.debug("UserEjb :: getUserInfo :: Method Entered for User {}", username);

        UserInfoDTO userInfoDTO = new UserInfoDTO();
        try {
            UcmdbUsers user = getUser();
            boolean isWorkflowUser =
                    groupEjb.getUserGroups()
                            .stream()
                            .filter(group -> group.getIsWorkflowRelated() == 1)
                            .count() != 0;

            userInfoDTO.setUsername(user.getUsername());
            userInfoDTO.setActive(user.getActive() == 1);
            userInfoDTO.setAdmin(Integer.valueOf(user.getAdmin() != null ? user.getAdmin() : "0") == 1);
            userInfoDTO.setWorkflowUser(isWorkflowUser);

        } catch (NoResultException nre) {
            log.debug("UserEjb :: getUserInfo :: User {} is not in Database. Trying to add him/her right now...", username);

            try {
                ucmdbInsertUser(username);
                sendInfoMailToAdmins(username);

                return new UserInfoDTO(username, true, false, false);
            } catch (Exception ex) {
                throw new ApplicationException("UserEjb :: getUserInfo :: ERROR: " + ex);
            }
        } catch (Exception ex) {
            throw new ApplicationException("UserEjb :: getUserInfo :: ERROR: " + ex);
        }

        log.debug("UserEjb :: getUserInfo :: Method Returned {}", userInfoDTO);
        return userInfoDTO;
    }

    public List<UcmdbUsers> getAllUsers() {
        log.debug("UserEjb :: ucmdbGetAllUsers :: method entered ");
        List<UcmdbUsers> allUsers;

        allUsers = entityManager.createQuery(GET_ALL_USERS_SQL, UcmdbUsers.class).getResultList();

        log.debug("UserEjb :: ucmdbGetAllUsers :: method returning {}", allUsers);
        return allUsers;
    }

    public UcmdbUsers ucmdbInsertUser(String username) throws Exception {
        log.debug("UserEjb :: ucmdbInsertUser :: method entered with {}", username);

        UcmdbUsers newUser = new UcmdbUsers();
        newUser.setUsername(username);
        newUser.setActive(1);
        newUser.setAdmin("0");
        newUser.setCreationDate(new Date());

        entityManager.merge(newUser);

        log.debug("UserEjb :: ucmdbInsertUser :: method returning {}", newUser);
        return newUser;
    }

    public UcmdbUsers makeUserInactive(NameCodeObject nameCode) {
        log.debug("UserEjb :: makeUserInactive :: Method Entered for User with ID: {}", nameCode.getId());

        UcmdbUsers userToMakeInactive;
        try {
            int userId = Integer.valueOf(nameCode.getId());
            userToMakeInactive = entityManager.find(UcmdbUsers.class, userId);
            userToMakeInactive.setActive(0);

            removeAllUserGroups(userToMakeInactive);

            entityManager.merge(userToMakeInactive);
        }
        catch (Exception ex) {
            throw new ApplicationException("UserEjb :: makeUserInactive :: ERROR: {}", ex);
        }

        log.debug("UserEjb:: makeUserInactive :: Successfully Deactivated User {}", userToMakeInactive.getUsername());
        return userToMakeInactive;
    }

    public void removeAllUserGroups(UcmdbUsers user) {
        log.error("UserEjb.removeAllUserGroups :: Method Entered with User {}", user);

        try {
            Query query = entityManager.createNativeQuery(REMOVE_USER_GROUPS_SQL);
            query.setParameter(1, user.getId());
            query.executeUpdate();
        } catch (Exception ex) {
            log.error("UserEjb.removeAllUserGroups :: Error while removing user Groups {}", ex);
        }

        log.debug("UserEjb.removeAllUserGroups :: Method Removed User Groups {}");
    }

    private void sendInfoMailToAdmins(String username) throws ApplicationException {
        log.debug("UserEjb :: sendInfoMailToAdmins :: Method Entered for User {}", username);

        String title = "";
        String message = "User " + username + " has logged in. You can now give access to a  Group.";
        MailHelper.sendHTMLEmail(title, message, null, getAdminEmails(), null);

        log.debug("UserEjb :: sendInfoMailToAdmins :: Mail Has been sent");
    }

    private String getAdminEmails() {
        Pattern validEmailRegex = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

        List<UcmdbUsers> adminUsers =
                getAllUsers()
                        .stream()
                        .filter(user -> "1".equals(user.getAdmin()))
                        .filter(user -> validEmailRegex.matcher(user.getUsername()).matches())
                        .collect(Collectors.toList());

        // Construct String with Admin Emails
        StringBuilder adminEmails = new StringBuilder();
        adminUsers.stream()
                .forEach(users -> adminEmails.append(users.getUsername() + ";"));

        return adminEmails.toString();
    }

    private static final String GET_USER_SQL =
            "SELECT u.USER_ID, u.USERNAME, u.ACTIVE, u.EMAIL,u.ADMIN" +
                    " FROM UCMDB_USERS u WHERE u.USERNAME=?";

    private static final String GET_ALL_USERS_SQL =
            "SELECT user FROM UcmdbUsers user";

    private static final String REMOVE_USER_GROUPS_SQL =
            "DELETE FROM GROUP_USER GU" +
                    " WHERE GU.USER_ID=?";
}
