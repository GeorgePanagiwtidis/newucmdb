package gr.cosmote.ucmdb.ejb;

import gr.cosmote.ucmdb.models.base.AdvancedCriteria;
import gr.cosmote.ucmdb.models.base.Infrastructure;
import gr.cosmote.ucmdb.models.database.InfrastructureCiTypes;
import gr.cosmote.ucmdb.models.database.InfrastructureCustomAttributes;
import gr.cosmote.ucmdb.models.view.infrastructureDTOs.InfraExcelExportRequest;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface InfrastructureRemote {
    void clearCache();
    void clearCacheForSpecificRequest(AdvancedCriteria searchCriteria);
    List<Infrastructure> infrastructureSearch(AdvancedCriteria searchCriteria);
    List<InfrastructureCiTypes> getInfrastructureTypes();
    List<InfrastructureCustomAttributes> getCustomAttributesByType(int infraTypeId);

    List<Infrastructure> getInfraResultsForExport(InfraExcelExportRequest exportRequest);
    void exportInfrastructureComputersToDatabase();
}
