package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class HardwareBoard implements Serializable {
    private String displayLabel;
    private String boardIndex;
    private String bus;
    private String ciType;
    private boolean isVirtual;
    private String lastAccessTime;
    private String lastModifiedTime;
    private String name;
    private String ownerTenant;
    private String ciCollectionDisplayLabel;
}
