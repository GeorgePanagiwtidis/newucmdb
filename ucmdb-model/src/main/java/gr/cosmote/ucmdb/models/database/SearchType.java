package gr.cosmote.ucmdb.models.database;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SEARCH_TYPES")
@NamedQuery(name="SearchType.getAll", query="SELECT searchType FROM SearchType searchType")
@Data
public class SearchType implements Serializable {
    @Id
    @Column(name = "TYPE_ID")
    private int typeId;

    @Column(name = "TYPE_NAME")
    private String typeName;

    @Column(name = "TYPE_LABEL")
    private String typeLabel;
}
