package gr.cosmote.ucmdb.models.database;

import gr.cosmote.ucmdb.models.enumeration.NodeStatusEnum;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;
import org.eclipse.persistence.annotations.ConversionValue;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.ObjectTypeConverter;
import org.eclipse.persistence.annotations.ObjectTypeConverters;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.*;
import java.util.Date;

@Entity
@Data
@Cache(type = CacheType.NONE)
@ToString
@Table(name="UNCLASSIFIED_NODES")
@ObjectTypeConverters({
    @ObjectTypeConverter(
        name="nodeStatusConverter",
        dataType=String.class,
        objectType=NodeStatusEnum.class,
        conversionValues={
                @ConversionValue(dataValue="COMPLETED", objectValue="COMPLETED"),
                @ConversionValue(dataValue="REJECTED", objectValue="REJECTED"),
                @ConversionValue(dataValue="OPEN", objectValue="OPEN")
        }
    )
})
@Slf4j
public class Node implements Serializable {
    @Id
    @GeneratedValue(generator="node_seq")
    @SequenceGenerator(name="node_seq",sequenceName="NODE_SEQ", allocationSize=1)
    @Column(name = "ID")
    private long id;

    @Column(name = "STATUS")
    @Convert("nodeStatusConverter")
    private NodeStatusEnum status;

    @Column(name = "DISPLAY_LABEL")
    private String displayLabel;

    @Column(name = "GLOBAL_ID")
    private String globalId;

    @Column(name = "CI_TYPE")
    private String ciType;

    @Column(name = "OWNER_TENANT")
    private String ownerTenant;

    @Column(name = "CREATION_DATE")
    @Temporal(TemporalType.DATE)
    private Date creationDate;

    @Lob
    @Column(name="NODE_TASK_DATA")
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    protected byte[] tasksWrapper;

    public void setTasksWrapper(Object obj) throws IOException {
        try{
            if(obj == null)
                this.tasksWrapper = null;

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(out);
            os.writeObject(obj);
            this.tasksWrapper = out.toByteArray();
        } catch(IOException e) {
            log.error("Node :: setTasksWrapper() :: Exception while serializing object to byte array", e);
            this.tasksWrapper = null;
        }
    }

    public TasksWrapper getTasksWrapper() {
        try{
            if(this.tasksWrapper == null)
                return null;

            ByteArrayInputStream in = new ByteArrayInputStream(this.tasksWrapper);
            ObjectInputStream is = new ObjectInputStream(in);
            return (TasksWrapper)is.readObject();
        } catch(IOException e) {
            log.error("Node :: getTasksWrapper() :: Exception while deserializing object", e);
            return null;
        } catch(ClassNotFoundException cnfe) {
            log.error("Node :: getTasksWrapper() :: Exception while casting to object class", cnfe);
            return null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        //if (!super.equals(o)) return false;

        Node node = (Node) o;

        return globalId != null ? globalId.equals(node.globalId) : node.globalId == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (globalId != null ? globalId.hashCode() : 0);
        return result;
    }
}
