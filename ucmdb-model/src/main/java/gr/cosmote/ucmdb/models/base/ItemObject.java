package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;

@Data
public class ItemObject implements Serializable{

    private String itemType;
    private HashSet<String> itemValue;
    private String itemName;
}
