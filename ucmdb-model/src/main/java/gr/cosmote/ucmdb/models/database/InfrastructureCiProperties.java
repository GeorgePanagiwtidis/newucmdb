package gr.cosmote.ucmdb.models.database;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "INFRA_CI_PROPERTIES")
public class InfrastructureCiProperties implements Serializable {

    @Id
    @GeneratedValue(generator="infra_ci_seq")
    @SequenceGenerator(name="infra_ci_seq",sequenceName="INFRA_CI_PROPERTIES_SEQ", allocationSize=1)
    @Column(name = "PROPERTY_ID")
    private int propertyId;

    @Column(name = "PROPERTY_NAME")
    private String propertyName;

    @Column(name = "PROPERTY_LABEL")
    private String propertyLabel;

    @ManyToOne(optional = false, targetEntity = InfrastructureCiTypes.class)
    @JoinColumn(name = "TYPE_ID", referencedColumnName = "TYPE_ID", nullable = false, updatable = false)
    private InfrastructureCiTypes type;
}
