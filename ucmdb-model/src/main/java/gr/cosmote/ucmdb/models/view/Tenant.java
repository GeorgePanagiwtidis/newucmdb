package gr.cosmote.ucmdb.models.view;

import gr.cosmote.ucmdb.models.base.ValueObject;
import lombok.Data;

@Data
public class Tenant extends ValueObject {
    private int id;
    private String description;
}
