package gr.cosmote.ucmdb.models.database;

import lombok.Data;
import lombok.ToString;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@ToString
@Table(name="CI_ATTRIBUTE_LOV")
@Cacheable(true)
@Cache(
        type=CacheType.SOFT, // Cache everything until the JVM decides memory is low.
        size=5000,
        expiry=84800000,  // 24 minutes
        coordinationType= CacheCoordinationType.NONE  // if cache coordination is used, only send invalidation messages.
)
public class CiAttributeLov implements Serializable {
    @Id
    @Column(name = "ID")
    private int id;

    @Column(name = "ATTRIBUTE_NAME")
    private String type;

    @Column(name = "ATTRIBUTE_VALUE")
    private String value;

    @Column(name = "CI_TYPE")
    private String ciType;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TENANT_ID")
    private Tenants tenant;
}
