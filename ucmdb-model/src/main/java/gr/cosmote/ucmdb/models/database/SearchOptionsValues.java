package gr.cosmote.ucmdb.models.database;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;
import java.io.Serializable;
import javax.persistence.*;
import lombok.Data;

@Entity
@Data
@Cache(type = CacheType.NONE)
@Table(name="SEARCH_OPTIONS_VALUES")
public class SearchOptionsValues implements Serializable {

    @Id
    @GeneratedValue(generator="OPTION_SEQ")
    @SequenceGenerator(name="OPTION_SEQ",sequenceName="OPTION_SEQ", allocationSize=1)
    @Column(name = "OPTION_SEQ")
    private int seqId;

    @Column(name = "OPTION_ID")
    private int optionId;

    @Column(name="SEARCH_ID")
    private String searchId;


    @Column(name = "OPTION_VALUE")
    private String optionValue;
}
