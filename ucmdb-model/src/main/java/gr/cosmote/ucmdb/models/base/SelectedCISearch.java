package gr.cosmote.ucmdb.models.base;

import lombok.Data;
import java.io.Serializable;

@Data
public class SelectedCISearch implements Serializable {

    private String ciId;
    private String ciType;
}
