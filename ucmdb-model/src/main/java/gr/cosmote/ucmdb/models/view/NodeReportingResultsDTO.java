package gr.cosmote.ucmdb.models.view;

import gr.cosmote.ucmdb.models.base.ValueObject;
import gr.cosmote.ucmdb.models.enumeration.NodeStatusEnum;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class NodeReportingResultsDTO extends ValueObject {
    private long id;
    private NodeStatusEnum status;
    private String ciType;
    private Date creationDate;
}
