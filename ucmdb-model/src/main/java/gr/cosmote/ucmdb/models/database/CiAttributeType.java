package gr.cosmote.ucmdb.models.database;

import lombok.Data;
import lombok.ToString;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@ToString
@Table(name="CI_ATTRIBUTE_TYPES")
@Cacheable(true)
@Cache(
        type=CacheType.SOFT,
        size=10000,
        expiry=84800000,  // 24 minutes
        coordinationType= CacheCoordinationType.NONE
)
public class CiAttributeType implements Serializable {
    @Id
    @Column(name = "ID")
    private int id;

    @Column(name = "TYPE")
    private String text;
}
