package gr.cosmote.ucmdb.models.base;

import gr.cosmote.ucmdb.models.view.infrastructureDTOs.InfraExcelExportRequest;
import gr.cosmote.ucmdb.models.view.savedsearches.InfrastructureOptionDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class AdvancedCriteria implements Serializable{
    public AdvancedCriteria() {}

    // Transform Constructor
    public AdvancedCriteria (InfraExcelExportRequest exportRequest) {
        this.businessCriteria = exportRequest.getBusinessCriteria();
        this.advancedTerms = exportRequest.getAdvancedTerms();
    }

    private BusinessSearchCriteria businessCriteria;
    private List<InfrastructureOptionDTO> advancedTerms;
}
