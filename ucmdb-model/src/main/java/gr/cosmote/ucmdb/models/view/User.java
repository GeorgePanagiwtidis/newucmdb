package gr.cosmote.ucmdb.models.view;

import java.util.Date;
import java.util.List;

import gr.cosmote.ucmdb.models.base.ValueObject;
import gr.cosmote.ucmdb.models.database.*;
import lombok.Data;

@Data
public class User extends ValueObject {
    private int         id;
    private String      username;
    private List<gr.cosmote.ucmdb.models.database.Group> group;
    private Date creationDate;
    private String groupsDescriptions;
    private String tenantsDescriptions;
    private List<Tenant> tenants;
    private int numberOfGroups;
    private int tenantsNumber;
}
