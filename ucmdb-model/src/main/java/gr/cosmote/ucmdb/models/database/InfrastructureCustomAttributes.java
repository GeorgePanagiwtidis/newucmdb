package gr.cosmote.ucmdb.models.database;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "INFRA_CUSTOM_ATTRIBUTES")
public class InfrastructureCustomAttributes implements Serializable {
    private static final long serialVersionUID = -6488247652791644236L;

    @Id
    @Column(name = "ID")
    private int id;

    @Column(name = "CUSTOM_ATTRIBUTE_NAME")
    private String attributeName;

    @Column(name = "CUSTOM_ATTRIBUTE_LABEL")
    private String attributeLabel;

    @Column (name = "IS_CUSTOM_ATTRIBUTE")
    private boolean isCustomAttribute;

    @Column (name = "IS_COMMENT_FILTER")
    private boolean isCommentFilter;

    @Column (name = "IS_SUPPORT_FILTER")
    private  boolean isSupportFilter;

    @ManyToOne
    @JoinColumn(name="TYPE", nullable=false, updatable=false)
    private InfrastructureCiTypes infraType;

}
