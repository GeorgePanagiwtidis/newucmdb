package gr.cosmote.ucmdb.models.database;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "UPDATABLE_CI_TYPES")
@NamedQuery(name = "UpdatableCiType.getUpdatableCiTypes", query = "Select ciType.ciTypeName from UpdatableCiType ciType")
public class UpdatableCiType implements Serializable{
    @Id
    @Column(name = "CI_TYPE_ID")
    private Integer ciTypeId;

    @Column(name = "CI_TYPE_NAME")
    private String ciTypeName;
}
