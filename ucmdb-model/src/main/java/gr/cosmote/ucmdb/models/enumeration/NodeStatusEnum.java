package gr.cosmote.ucmdb.models.enumeration;

public enum NodeStatusEnum {
    OPEN,
    REJECTED,
    COMPLETED
}
