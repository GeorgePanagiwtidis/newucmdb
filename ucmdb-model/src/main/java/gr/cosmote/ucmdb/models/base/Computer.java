package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Data
public class Computer implements Serializable{
    private String              id;
    private Map<String, String> properties;
    private List<Ci_Simple> cpus                        = new ArrayList();
    private List<Ci_Simple> diskDevice                  = new ArrayList();
    private List<Ci_Simple> fibreChannelHba             = new ArrayList();
    private List<Ci_Simple> fileSystem                  = new ArrayList();
    private List<Ci_Simple> hardwareBoard               = new ArrayList();
    private List<Ci_Simple> ibmLParProfiles             = new ArrayList();
    private List<Ci_Simple> ibmProcessorPool            = new ArrayList();
    private List<Ci_Simple> ipServiceEndpoint           = new ArrayList();
    private List<Ci_Simple> ipSubnet                    = new ArrayList();
    private List<Ci_Simple> logicalVolume               = new ArrayList();
    private List<Ci_Simple> networkAdapter              = new ArrayList();
    private List<Ci_Simple> oracle                      = new ArrayList();
    private List<Ci_Simple> oracleTnsListener           = new ArrayList();
    private List<Ci_Simple> osUSer                      = new ArrayList();
    private List<Ci_Simple> physicalVolume              = new ArrayList();
    private List<Ci_Simple> virtualizationLayerSoftware = new ArrayList();
    private List<Ci_Simple> volumeGroup                 = new ArrayList();
    private List<Ci_Simple> weblogicAS                  = new ArrayList();
    private List<Ci_Simple> ipAddress                   = new ArrayList();
    private List<Ci_Simple> ucmdbInterface              = new ArrayList();


    public void setAttributes(List<Ci> items){


        for (Ci item : items){
            Ci_Simple ci = new Ci_Simple();
            ci.setId(item.getId());
            ci.setProperties(item.getProperties());

            switch (item.getProperties().get("type")){
                case "cpu":
                    cpus.add(ci);
                    break;
                case "disk_device":
                    diskDevice.add(ci);
                    break;
                case "fchba":
                    fibreChannelHba.add(ci);
                    break;
                case "file_system":
                    fileSystem.add(ci);
                    break;
                case "hardware_board":
                    hardwareBoard.add(ci);
                    break;
                case "ibm_lpar_profile":
                    ibmLParProfiles.add(ci);
                    break;
                case "ibm_resource_pool":
                    ibmProcessorPool.add(ci);
                    break;
                case "interface":
                    ucmdbInterface.add(ci);
                    break;
                case "ip_address":
                    ipAddress.add(ci);
                    break;
                case "ip_service_endpoint":
                    ipServiceEndpoint.add(ci);
                    break;
                case "ip_subnet":
                    ipSubnet.add(ci);
                    break;
                case "logical_volume":
                    logicalVolume.add(ci);
                    break;
                case "network_adapter":
                    networkAdapter.add(ci);
                    break;
                case "oracle":
                    oracle.add(ci);
                    break;
                case "oracle_listener":
                    oracleTnsListener.add(ci);
                    break;
                case "osuser":
                    osUSer.add(ci);
                    break;
                case "physicalvolume":
                    physicalVolume.add(ci);
                    break;
                case "virtualization_layer":
                    virtualizationLayerSoftware.add(ci);
                    break;
                case "volumegroup":
                    volumeGroup.add(ci);
                    break;
                case "weblogicas":
                    weblogicAS.add(ci);
                    break;
            }
        }
    }

}
