package gr.cosmote.ucmdb.models.view;

import gr.cosmote.ucmdb.models.base.ValueObject;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TaskReportingResultsDTO extends ValueObject {
    private long groupId;
    private String groupName;
    private long tasksAssigned = 0;
    private List<TaskReportDTO> taskReportDTOS = new ArrayList<>();

    public void increaseTasksAssigned() {
        tasksAssigned = tasksAssigned + 1;
    }

    public void addTasksAssigned(TaskReportDTO taskDTO) {
        taskReportDTOS.add(taskDTO);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskReportingResultsDTO that = (TaskReportingResultsDTO) o;

        return groupId == that.groupId;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) (groupId ^ (groupId >>> 32));
        return result;
    }
}
