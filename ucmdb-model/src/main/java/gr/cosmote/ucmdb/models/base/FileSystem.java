package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class FileSystem implements Serializable{
    private String displayLabel;
    private String ciType;
    private String fileSystemSize;
    private String fileSystemType;
    private String freeSpaceAvailable;
    private String lastAccessTime;
    private String lastModifiedTime;
    private String mountDevice;
    private String mountPoint;
    private String name;
    private String OwnerTenant;
    private String storageType;
    private String ciCollectionDisplayLabel;
}
