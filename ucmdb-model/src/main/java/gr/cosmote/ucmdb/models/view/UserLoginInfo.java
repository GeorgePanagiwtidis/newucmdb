package gr.cosmote.ucmdb.models.view;

import gr.cosmote.ucmdb.models.base.ValueObject;
import lombok.Data;

@Data
public class UserLoginInfo extends ValueObject {
    private String username;
    private String password;
}
