package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
public class Ci implements Serializable{

    private String              id;
    private String              type;
    private Map<String, String> properties;
    private List<Computer>      computers;
    private List<Ci_Simple>     businessApplications;
    private List<Ci_Simple>     netdevices;
}
