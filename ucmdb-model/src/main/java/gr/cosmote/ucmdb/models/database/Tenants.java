package gr.cosmote.ucmdb.models.database;

import gr.cosmote.ucmdb.models.view.*;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name="TENANTS")
@Cacheable(true)
@Cache(
        type=CacheType.SOFT, // Cache everything until the JVM decides memory is low.
        size=1000,
        expiry=1000000,  // 16 minutes
        coordinationType= CacheCoordinationType.NONE  // if cache coordination is used, only send invalidation messages.
)
public class Tenants implements Serializable {
    private static final long serialVersionUID = -5526794542608764297L;

    @Id
    @GeneratedValue(generator="tenants_seq")
    @SequenceGenerator(name="tenants_seq",sequenceName="TENANTS_SEQ", allocationSize=1)
    @Column(name = "TENANT_ID")
    private int id;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "OLD_ENV")
    private boolean oldEnv;

    @Column(name = "NEW_ENV")
    private boolean newEnv;

    @Transient
    private List<GroupDTO> tenantGroups;

    @Transient
    private List<User> tenantUsers;

    @Transient
    private int numberOfUsers;

    /*@ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JsonIgnore
    @JoinTable(name = "SEARCH_TENANTS", joinColumns = @JoinColumn(name = "TENANT_ID"),
            inverseJoinColumns = @JoinColumn(name = "SEARCH_ID"))
    private List<Search> searches;*/

    /*@ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JsonIgnore
    @JoinTable(name = "GROUP_TENANT", joinColumns = @JoinColumn(name = "TENANT_ID"),
            inverseJoinColumns = @JoinColumn(name = "GROUP_ID"))
    private List<GroupDTO> groups;*/
}
