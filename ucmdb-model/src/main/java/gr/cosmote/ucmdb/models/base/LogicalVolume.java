package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class LogicalVolume implements Serializable{

    private String displayLabel;
    private String ciType;
    private int freeSpaceOnLogicalVolumeMb;
    private String lastAccessTime;
    private String lastModifiedTime;
    private String logicalVolumeFileSystemType;
    private int logicalVolumeSize;
    private String logicalVolumeStatus;
    private String name;
    private String ciCollectionDisplayLabel;
}
