package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Filters implements Serializable{
    List<ItemObject> itemObjects;
}
