package gr.cosmote.ucmdb.models.database;

import gr.cosmote.ucmdb.models.view.savedsearches.SearchOptionsDTO;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Data
@Cache(type = CacheType.NONE)
@ToString
@Table(name="SAVED_SEARCHES")
@Slf4j
public class SavedSearches implements Serializable {
    @Id
    @GeneratedValue(generator="search_seq")
    @SequenceGenerator(name="search_seq",sequenceName="SEARCH_SEQ", allocationSize=1)
    @Column(name = "SEARCH_ID")
    private int id;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "CREATION_DATE")
    @Temporal(TemporalType.DATE)
    private Date creationDate;

    @Column(name = "SEARCH_TERM")
    private String searchTerm;

    @Column(name = "SEARCH_TYPE")
    private String searchType;

    @Column(name = "USER_ID")
    private int userId;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "SEARCH_TENANTS", joinColumns = @JoinColumn(name="SEARCH_ID"),
    inverseJoinColumns = @JoinColumn(name="TENANT_ID") )
    private List<Tenants> tenants;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "SEARCH_USER", joinColumns = @JoinColumn(name="SEARCH_ID"),
    inverseJoinColumns = @JoinColumn(name="USER_ID"))
    private UcmdbUsers ucmdbUsers;

    @Column(name = "OPTIONS")
    @Lob
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private byte[] options;

    @Transient
    private SearchOptionsDTO searchOptions;

    public void setOptions(Object obj) throws IOException {
        try{
            if(obj == null)
                this.options = null;

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(out);
            os.writeObject(obj);
            this.options = out.toByteArray();
        } catch(IOException e) {
            log.error("SearchOptions :: setOptions() :: Exception while serializing object to byte array", e);
            this.options = null;
        }
    }

    public SearchOptionsDTO getOptions() {
        try{
            if(this.options == null)
                return null;

            ByteArrayInputStream in = new ByteArrayInputStream(this.options);
            ObjectInputStream is = new ObjectInputStream(in);
            return (SearchOptionsDTO)is.readObject();
        } catch(IOException e) {
            log.error("SearchOptions :: getOptions() :: Exception while deserializing object", e);
            return null;
        } catch(ClassNotFoundException cnfe) {
            log.error("SearchOptions :: getOptions() :: Exception while casting to object class", cnfe);
            return null;
        }
    }
}
