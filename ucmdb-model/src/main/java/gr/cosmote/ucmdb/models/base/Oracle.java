package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class Oracle implements Serializable{

    private String displayLabel;
    private String applicationCategory;
    private String applicationIp;
    private String applicationIpType;
    private String applicationUsername;
    private String applicationVersionDescription;
    private String ciType;
    private String discoveredProductName;
    private String fileType;
    private String lastAccessTime;
    private String lastModifiedTime;
    private String name;
    private String ownerTenant;
    private String productName;
    private String type;
    private String vendor;
    private String version;
    private String ciCollectionDisplayLabel;
}
