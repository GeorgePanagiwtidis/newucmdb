package gr.cosmote.ucmdb.models.view.savedsearches;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * BusinessSearchOptions class is part of SearchOptionsDTO class
 * that is Saved as BLOB in OPTIONS column of SavedSearches Table
 *
 * @author Vasileios Bouzas - Intrasoft Intl.
 */
@Data
public class BusinessSearchOptions implements Serializable {
    private static final long serialVersionUID = 3867694752496116093L;

    /**
     * Field used in order to distinguish Table Filters for CiCollections
     * that are currently expanded by user in UI.
     */
    private String ciCollectionGlobalId;
    private String itemsType;
    private List<TableFilter> tableFilters;
    private List<String> selectedColumns;
}
