package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class Infrastructure implements Serializable {

    private String globalId;
    private Map<String, String> properties;
}
