package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class NameCodeObject implements Serializable{

    private String name;
    private String id;
}
