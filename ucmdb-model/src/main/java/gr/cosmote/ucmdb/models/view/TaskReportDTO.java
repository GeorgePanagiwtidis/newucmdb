package gr.cosmote.ucmdb.models.view;

import gr.cosmote.ucmdb.models.base.ValueObject;
import lombok.Data;


@Data
public class TaskReportDTO extends ValueObject {
    public TaskReportDTO() {}

    private String nodeName;
    private String osType;

    // TODO -- Uncomment when Field is Created
    //    private String osInstalledDate;

    private String ownerTenant;
    private String taskStatus;
    private String assignedDate;
    private String assignedGroup;
    private String assignedEngineer;

    private String processId;
//    private String globalId;
//    private String ciType;
}
