package gr.cosmote.ucmdb.models.view;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
public class BusinessResultDTO implements Serializable {
    private Map<String, List<BusinessServicesDTO>> business;
}
