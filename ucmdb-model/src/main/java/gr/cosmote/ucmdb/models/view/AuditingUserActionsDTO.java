package gr.cosmote.ucmdb.models.view;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class AuditingUserActionsDTO implements Serializable {
    private String globalId;
    private String nodeDiffId;
    private String taskId;
    private String processId;
    private String displayLabel;
    private String ownerTenant;
    private String userGroup;
    private String updatedBy;
    private String action;
    private Date modifyTime;
}
