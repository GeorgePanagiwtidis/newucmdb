package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class VolumeGroup implements Serializable{

    private String displayLabel;
    private String ciType;
    private String lastAccessTime;
    private String lastModifiedTime;
    private String name;
    private String volumeGroupId;
    private String volumeGroupSize;
    private String ciCollectionDisplayLabel;

}
