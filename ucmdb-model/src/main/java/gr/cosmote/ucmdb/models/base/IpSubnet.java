package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class IpSubnet implements Serializable{

    private String displayLabel;
    private String ciType;
    private String ipAddressType;
    private String ipAddressValue;
    private int ipPrefixLength;
    private boolean isManaged;
    private String lastAccessTime;
    private String lastModifiedTime;
    private String name;
    private String networkBroadcastAddress;
    private String networkClass;
    private String networkMask;
    private String ownerTenant;
    private String routingDomain;
    private String ciCollectionDisplayLabel;
}
