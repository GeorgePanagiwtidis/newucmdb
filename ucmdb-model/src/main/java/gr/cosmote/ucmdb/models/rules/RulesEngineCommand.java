package gr.cosmote.ucmdb.models.rules;

import gr.cosmote.ucmdb.models.database.Task;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Intrasoft Intl - dasimakopoulos on 13/8/2017.
 */
@ToString
public class RulesEngineCommand<E extends Enum> implements Serializable {
    private Task task;
    private List<E> commands;

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public List<E> getCommands() {
        return commands;
    }

    public void setCommands(List<E> commands) {
        this.commands = commands;
    }

    public void addCommand(E command) {
        if (commands == null)
            commands = new ArrayList<E>();
        commands.add(command);
    }

    public void removeAllCommands() {
        this.commands.clear();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RulesEngineCommand that = (RulesEngineCommand) o;

        if (task != null ? !task.equals(that.task) : that.task != null) return false;
        return commands != null ? commands.equals(that.commands) : that.commands == null;
    }

    @Override
    public int hashCode() {
        int result = task != null ? task.hashCode() : 0;
        result = 31 * result + (commands != null ? commands.hashCode() : 0);
        return result;
    }
}
