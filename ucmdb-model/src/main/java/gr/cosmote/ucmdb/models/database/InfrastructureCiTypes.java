package gr.cosmote.ucmdb.models.database;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "INFRASTRUCTURE_CI_TYPES")
public class InfrastructureCiTypes implements Serializable {

    @Id
    @Column(name = "TYPE_ID")
    private int typeId;

    @Column (name = "CI_TYPE")
    private String ciType;

    @Column (name = "CI_TYPE_LABEL")
    private String ciTypeLabel;
}
