package gr.cosmote.ucmdb.models.view.viewquery;

import gr.cosmote.ucmdb.models.base.GenericCI;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ViewQueryResponseDTO implements Serializable {
    private static final long serialVersionUID = -423387998624001924L;

    private GenericCI ci;
    private List<ViewQueryRelatedCI> relationCIs;
}
