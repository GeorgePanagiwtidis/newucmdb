package gr.cosmote.ucmdb.models.commons;

import java.io.Serializable;

/**
 * Created by Intrasoft Intl - dasimakopoulos on 15/8/2017.
 */
public abstract class Groupable implements Serializable {
    private String groupName;
    private int groupPriority;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Groupable groupable = (Groupable) o;

        if (groupPriority != groupable.groupPriority) return false;
        return groupName != null ? groupName.equals(groupable.groupName) : groupable.groupName == null;
    }

    @Override
    public int hashCode() {
        int result = groupName != null ? groupName.hashCode() : 0;
        result = 31 * result + groupPriority;
        return result;
    }

    public Groupable() {
    }

    public Groupable(String groupName, int groupPriority) {
        this.groupName = groupName;
        this.groupPriority = groupPriority;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public int getGroupPriority() {
        return groupPriority;
    }

    public void setGroupPriority(int groupPriority) {
        this.groupPriority = groupPriority;
    }


}
