package gr.cosmote.ucmdb.models.view.savedsearches;

import lombok.Data;

import java.io.Serializable;

/**
 * TableFilter class is part of BLOB saved in OPTIONS Column
 * of SavedSearches Table and is used for Defining Table Property filters
 * with their values
 *
 * @author Vasileios Bouzas - Intrasoft Intl.
 */
@Data
public class TableFilter implements Serializable {
    private static final long serialVersionUID = 5472232436908251273L;

    private String propertyName;
    private String propertyValue;
}
