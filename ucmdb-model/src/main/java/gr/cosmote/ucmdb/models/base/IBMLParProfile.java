package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class IBMLParProfile implements Serializable{

    private String displayLabel;
    private int activeCpuInPool;
    private int activePhysicalCpu;
    private String ciType;
    private String cpuMode;
    private boolean connectionMonitoringEnabled;
    private int desiredNumberOfCpus;
    private int desiredNumberOfHugeMemoryPages;
    private int desiredPhysicalCpus;
    private boolean isVirtual;
    private String lParProfileName;
    private String lParBootMode;
    private int lParId;
    private String lParName;
    private boolean lParProfileAutoStart;
    private String lParProfileIoPoolIds;
    private String lParSerialNumber;
    private String lParDefaultProfileName;
    private String lParState;
    private String lParType;
    private String lastAccessTime;
    private String lastModifiedTime;
    private String lParMode;
    private int maximumNumberOfCpus;
    private int maximumNumberOfHugeMemoryPages;
    private int maximumNumberOfVirtualSlots;
    private int maximumPhysicalCpus;
    private int maximumMemory;
    private int memoryRequested;
    private int minimumMemoryOnThisLpar;
    private int minimumNumberOfCpus;
    private int minimumNumberOfHugeMemoryPages;
    private String name;
    private int onlineVirtualCpu;
    private String ownerTenant;
    private String powerControlLparIds;
    private boolean redudantErrorPathReporting;
    private int sharedPoolId;
    private String sharingMode;
    private int uncappedWeight;
    private String virtualServiceAdapters;
    private String workgroupId;
    private String ciCollectionDisplayLabel;

}
