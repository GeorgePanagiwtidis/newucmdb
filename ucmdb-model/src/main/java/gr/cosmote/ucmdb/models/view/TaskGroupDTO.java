package gr.cosmote.ucmdb.models.view;

import gr.cosmote.ucmdb.models.base.ValueObject;
import lombok.Data;

import java.util.List;

@Data
public class TaskGroupDTO extends ValueObject {
    public TaskGroupDTO() {}

    public TaskGroupDTO(String groupName, int groupPriority, List attributes) {
        this.groupName = groupName;
        this.groupPriority = groupPriority;
        this.attributes = attributes;
    }

    private String groupName;
    private int groupPriority;
    private List<TaskAttributeDTO> attributes;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskGroupDTO that = (TaskGroupDTO) o;

        if (groupPriority != that.groupPriority) return false;
        return groupName != null ? groupName.equals(that.groupName) : that.groupName == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (groupName != null ? groupName.hashCode() : 0);
        result = 31 * result + groupPriority;
        return result;
    }
}
