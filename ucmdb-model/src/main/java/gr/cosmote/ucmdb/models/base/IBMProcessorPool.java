package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class IBMProcessorPool implements Serializable {
    private String displayLabel;
    private String ciType;
    private String cpuPoolAvailablePhysicalCpus;
    private String cpuPoolConfigurableCpus;
    private String cpuPendingAvailablePhysicalCpus;
    private String lastAccessTime;
    private String lastModifiedTime;
    private String name;
    private String ownerTenant;
    private String poolId;
    private String ciCollectionDisplayLabel;

}
