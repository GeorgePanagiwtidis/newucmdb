package gr.cosmote.ucmdb.models.view;

import gr.cosmote.ucmdb.models.base.ValueObject;
import gr.cosmote.ucmdb.models.enumeration.NodeStatusEnum;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class NodeReportingResultDTO extends ValueObject {
    private List<ProcessReportDTO> processReportDTOList = new ArrayList<>();
    private long numberOfNodes;
    private NodeStatusEnum processStatus;

    public void addProccessReportRecord(ProcessReportDTO processReportDTO) {
        processReportDTOList.add(processReportDTO);
    }
}
