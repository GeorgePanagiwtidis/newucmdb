package gr.cosmote.ucmdb.models.view;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class TaskHeaderDTO implements Serializable{

    private String title;
    private String message;
    private List<TaskAttributeDTO> attributes;
}
