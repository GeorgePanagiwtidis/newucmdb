package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class IpServiceEndpoint implements Serializable{

    private String displayLabel;
    private String boundToIpAddress;
    private String boundToIpAddressType;
    private String ciType;
    private String ipServiceName;
    private String lastAccessTime;
    private String lastModifiedTime;
    private String networkPortNumber;
    private String portType;
    private String serviceAddress;
    private String serviceNames;
    private String ciCollectionDisplayLabel;
}
