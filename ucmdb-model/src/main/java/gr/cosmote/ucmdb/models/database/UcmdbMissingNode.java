package gr.cosmote.ucmdb.models.database;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "UCMDB_MISSING_NODES")
@IdClass(UcmdbMissingNodePK.class)
@NamedQuery(name = "UcmdbMissingNode.getUcmdbMissingNodes", query = "select missingNode from UcmdbMissingNode missingNode")
public class UcmdbMissingNode implements Serializable {
    private static final long serialVersionUID = 5703613149435983142L;

    @Id
    @Column(name = "TENANT_OWNER")
    private String tenantOwner;

    @Id
    @Column(name = "DISPLAY_LABEL")
    private String displayLabel;

    @Column(name = "IP_ADDRESS")
    private String ipAddress;

    @Column(name = "ENVIRONMENT")
    private String environment;

    @Column(name = "NOTE")
    private String note;

    @Column(name = "INSERTED_DATE")
    @Temporal(TemporalType.DATE)
    private Date insertedDate;

    @Column(name = "INSERTED_BY")
    private String insertedBy;
}
