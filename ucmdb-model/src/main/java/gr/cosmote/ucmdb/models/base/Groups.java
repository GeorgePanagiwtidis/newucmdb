package gr.cosmote.ucmdb.models.base;


import gr.cosmote.ucmdb.models.database.Group;
import lombok.Data;

import java.util.List;

@Data
public class Groups extends ValueObject {
    List<Group> groups;
}
