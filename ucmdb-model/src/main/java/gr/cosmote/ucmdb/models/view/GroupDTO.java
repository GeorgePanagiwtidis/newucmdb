package gr.cosmote.ucmdb.models.view;

import java.util.Date;
import java.util.Map;

import gr.cosmote.ucmdb.models.base.ValueObject;
import lombok.Data;

@Data
public class GroupDTO extends ValueObject{
    private int    id;
    private String description;
    private Date creationDate;
    private int parentId;
    private Map<Integer, String> tenants;
    private int numberOfUsersInEachGroup;
    private int numberOfSubgroupsInEachGroup;
}
