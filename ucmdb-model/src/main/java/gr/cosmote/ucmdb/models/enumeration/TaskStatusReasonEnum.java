package gr.cosmote.ucmdb.models.enumeration;

/**
 * Created by Intrasoft Intl - dasimakopoulos on 18/8/2017.
 */
public enum TaskStatusReasonEnum {
    UNKNOWN,
    INVALID_GROUP_ASSIGNMENT,
    TO_BE_DELETED_JUNK,
    NEW_TASK_CREATED_BY_USER_ACTIONS
}
