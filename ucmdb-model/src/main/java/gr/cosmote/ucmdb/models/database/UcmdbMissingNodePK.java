package gr.cosmote.ucmdb.models.database;

import lombok.Data;

import java.io.Serializable;

@Data
public class UcmdbMissingNodePK implements Serializable {
    private String tenantOwner;
    private String displayLabel;
}
