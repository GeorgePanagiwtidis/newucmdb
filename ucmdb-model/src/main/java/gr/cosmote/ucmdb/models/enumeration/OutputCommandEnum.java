package gr.cosmote.ucmdb.models.enumeration;

/**
 * Created by Intrasoft Intl - dasimakopoulos on 11/8/2017.
 */
public enum OutputCommandEnum {
    ADD_OR_UPDATE_TASK_IN_TASK_WRAPPER_COMMAND,
    DELETE_TASK_FROM_TASK_WRAPPER_COMMAND,
    UPDATE_UCMDB_COMMAND,
    SEND_EMAIL_COMMAND
}
