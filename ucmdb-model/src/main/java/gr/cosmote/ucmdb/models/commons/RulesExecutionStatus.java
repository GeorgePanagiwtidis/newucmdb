package gr.cosmote.ucmdb.models.commons;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Intrasoft Intl - dasimakopoulos on 19/8/2017.
 */
@Data
public class RulesExecutionStatus implements Serializable {
    private String status;

    public RulesExecutionStatus() {
    }
}
