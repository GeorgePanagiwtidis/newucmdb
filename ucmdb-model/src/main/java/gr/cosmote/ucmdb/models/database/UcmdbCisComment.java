package gr.cosmote.ucmdb.models.database;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "UCMDB_CIS_COMMENTS")
@NamedQuery(name = "UcmdbCisComment.getComments", query = "select comments from UcmdbCisComment comments")
@Data
public class UcmdbCisComment implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ucmdb_cis_comments_seq")
    @SequenceGenerator(name = "ucmdb_cis_comments_seq", sequenceName = "UCMDB_CIS_COMMENTS_SEQ", allocationSize = 1)
    private long commentId;

    @Column(name = "UCMDB_ID")
    private long ucmdbId;

    @Column(name = "COMMENT_BY")
    private String commentAuthor;

    @Column(name = "COMMENT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date commentDate;

    @Column(name = "COMMENT_DESC")
    private String commentText;
}
