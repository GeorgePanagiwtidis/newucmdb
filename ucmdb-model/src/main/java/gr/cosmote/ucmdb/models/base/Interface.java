package gr.cosmote.ucmdb.models.base;


import lombok.Data;
import java.io.Serializable;

@Data
public class Interface implements Serializable{

   private String displayLabel;
   private String ciType;
   private String interfaceAlias;
   private String interfaceDescription;
   private String interfaceGateways;
   private String interfaceIndex;
   private String interfaceName;
   private String interfaceRole;
   private String interfaceSpeed;
   private String interfaceType;
   private String isPseudoInterface;
   private String isVirtual;
   private String lastAccessTime;
   private String lastModifiedTime;
   private String macAddress;
}