package gr.cosmote.ucmdb.models.base;


import lombok.Data;

import java.io.Serializable;
@Data
public class IpAddress implements Serializable{

    private String displayLabel;
    private String authoritativeDnsName;
    private String ciType;
    private String IPLeaseTime;
    private String ipNetworkAddress;
    private String ipNetworkClass;
    private String ipNetworkMask;
    private String ipAddressType;
    private String ipAddressValue;
    private String lastAccessTime;
    private String lastModifiedTime;
    private String name;
    private String ownerTenant;
    private String ucmdbRoutingDomain;
}
