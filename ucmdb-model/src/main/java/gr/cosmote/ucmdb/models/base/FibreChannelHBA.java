package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class FibreChannelHBA implements Serializable{
    private String displayLabel;
    private String ciType;
    private String description;
    private String hbaModel;
    private String hbaVendor;
    private boolean isVirtual;
    private String lastAccessTime;
    private String lastModifiedTime;
    private String name;
    private String nodeWorldName;
    private String ownerTenant;
    private String serialNumber;
    private String ciCollectionDisplayLabel;
}
