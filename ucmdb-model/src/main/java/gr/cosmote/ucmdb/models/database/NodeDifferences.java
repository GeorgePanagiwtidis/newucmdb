package gr.cosmote.ucmdb.models.database;

import gr.cosmote.ucmdb.models.view.TaskAttributeDTO;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.io.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "NODE_DIFFERENCES")
@Data
@Slf4j
public class NodeDifferences implements Serializable {

    @Id
    @GeneratedValue (generator="nodeDiff_seq")
    @SequenceGenerator(name="nodeDiff_seq", sequenceName="NODE_DIFF_SEQ", allocationSize=1)
    @Column (name = "ID")
    private long id;

    @Column (name = "TASK_ID")
    private String taskId;

    @Column (name = "NODE_ID")
    private long nodeId;

    @Column (name = "GLOBAL_ID")
    private String globalId;

    @Column (name = "DISPLAY_LABEL")
    private String displayLabel;

    @Column (name = "OWNER_TENANT")
    private String ownerTenant;

    @Column (name = "USER_GROUP")
    private String userGroup;

    @Column (name = "UPDATED_BY")
    private String updatedBy;

    @Column(name = "USER_ACTION")
    private String userAction;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_MODIFIED_TIME")
    private Date lastModifiedTime;

    @Column(name = "COMMENTS")
    private String comments;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    @Lob
    @Column(name = "BEFORE_UPDATE")
    private byte[] beforeUpdate;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    @Lob
    @Column(name = "AFTER_UPDATE")
    private byte[] afterUpdate;

    public void setBeforeUpdate(Object beforeUpdateAttributes) {
        try{
            if(beforeUpdateAttributes == null)
                this.beforeUpdate = null;

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(out);
            os.writeObject(beforeUpdateAttributes);
            this.beforeUpdate = out.toByteArray();
        } catch(IOException e) {
            log.error("NodeDifferences :: setBeforeUpdate() :: Exception while serializing object to byte array", e);
            this.beforeUpdate = null;
        }
    }

    public void setAfterUpdate(Object afterUpdateAttributes) {
        try{
            if(afterUpdateAttributes == null)
                this.afterUpdate = null;

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(out);
            os.writeObject(afterUpdateAttributes);
            this.afterUpdate = out.toByteArray();
        } catch(IOException e) {
            log.error("NodeDifferences :: setAfterUpdate() :: Exception while serializing object to byte array", e);
            this.afterUpdate = null;
        }
    }

    public List<TaskAttributeDTO> getBeforeUpdate() {
        try{
            if(this.beforeUpdate == null)
                return null;

            ByteArrayInputStream in = new ByteArrayInputStream(this.beforeUpdate);
            ObjectInputStream is = new ObjectInputStream(in);
            return (List<TaskAttributeDTO>) is.readObject();
        } catch(IOException e) {
            log.error("NodeDifferences :: getBeforeUpdate() :: Exception while deserializing object", e);
            return null;
        } catch(ClassNotFoundException cnfe) {
            log.error("NodeDifferences :: getBeforeUpdate() :: Exception while casting to object class", cnfe);
            return null;
        }
    }

    public List<TaskAttributeDTO> getAfterUpdateLob() {
        try{
            if(this.afterUpdate == null)
                return null;

            ByteArrayInputStream in = new ByteArrayInputStream(this.afterUpdate);
            ObjectInputStream is = new ObjectInputStream(in);
            return (List<TaskAttributeDTO>) is.readObject();
        } catch(IOException e) {
            log.error("NodeDifferences :: getAfterUpdateLob() :: Exception while deserializing object", e);
            return null;
        } catch(ClassNotFoundException cnfe) {
            log.error("NodeDifferences :: getAfterUpdateLob() :: Exception while casting to object class", cnfe);
            return null;
        }
    }
}
