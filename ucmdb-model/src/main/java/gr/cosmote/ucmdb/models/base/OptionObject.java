package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class OptionObject implements Serializable{
    private String optionId;
    private String optionInput;
}
