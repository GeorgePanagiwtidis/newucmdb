package gr.cosmote.ucmdb.models.base;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class BusinessSearchCriteria implements Serializable{
    private String searchTerm;
    private List<Tenant> tenants;

}
