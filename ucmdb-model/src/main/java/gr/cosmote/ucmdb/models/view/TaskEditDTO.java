package gr.cosmote.ucmdb.models.view;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class TaskEditDTO implements Serializable {
    private static final long serialVersionUID = -3602236467928528069L;

    public enum ACTION_STATUS {
        Reassign, Save, Submit
    }

    private String taskId;
    private String nodeId;
    private ReassignedTaskDTO reassignTask;
    private String nodeGlobalId;
    private TaskHeaderDTO header;
    private List<TaskGroupDTO> groups;
    private TaskGroupDTO supportGroup;
    private ACTION_STATUS selectedAction;
    private String assignedDate;
    private String  ciType;
    private String osType;
    private Date  completionDate;
    private String taskStatus;
    private String currentAssignee;
    private boolean monitored;
    private int currentStage;

}
