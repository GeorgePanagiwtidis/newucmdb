package gr.cosmote.ucmdb.models.view.viewquery;

import gr.cosmote.ucmdb.models.view.savedsearches.InfrastructureOptionDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
public class ViewQueryRequestDTO implements Serializable {
    private static final long serialVersionUID = 3167427801678563792L;

    private String queryName;
    private List<String> selectedTenants;
    private List<InfrastructureOptionDTO> queryFilters;
}
