package gr.cosmote.ucmdb.models.database;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name="UCMDB_USERS")
@Cacheable(true)
@Cache(
        type=CacheType.SOFT, // Cache everything until the JVM decides memory is low.
        size=10000,
        expiry=300000,  // 5 minutes
        coordinationType= CacheCoordinationType.NONE  // if cache coordination is used, only send invalidation messages.
)
public class UcmdbUsers implements Serializable {
    @Id
    @GeneratedValue(generator = "users_seq")
    @SequenceGenerator(name = "users_seq", sequenceName = "USERS_SEQ", allocationSize = 1)
    @Column(name = "USER_ID")
    private int id;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "ACTIVE")
    private int active;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "CREATION_DATE", insertable=false)
    @Temporal(TemporalType.DATE)
    private Date creationDate;

    @Column(name = "ADMIN")
    private String admin;

    /*@ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinTable(name = "GROUP_USER", joinColumns = @JoinColumn(name = "USER_ID"),
            inverseJoinColumns = @JoinColumn(name = "GROUP_ID"))
    private List<GroupDTO> groups;*/

    //@Transient
    /*@ManyToMany(mappedBy = "users", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JsonIgnore*/
    //private List<GroupDTO> groups = new ArrayList<GroupDTO>();

    /*@ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinTable(name = "SEARCH_USER", joinColumns = @JoinColumn(name = "USER_ID"),
            inverseJoinColumns = @JoinColumn(name = "SEARCH_ID"))
    private List<Search> searches;*/
}
