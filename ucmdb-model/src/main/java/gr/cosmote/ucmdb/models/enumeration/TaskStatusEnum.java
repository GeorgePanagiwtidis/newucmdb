package gr.cosmote.ucmdb.models.enumeration;

public enum TaskStatusEnum  {
    INITIATED,//Fetched from UCDB
    OPEN,//Task Created from the system
    IN_PROGRESS,//A user has Takeover the task. TODO: Handle user action "Assign to me" and set it in progress
    COMPLETED,//The system (may be through a user action) closed the task
    CANCELED,//System wise is the same with COMPLETED - Used for reporting
}
