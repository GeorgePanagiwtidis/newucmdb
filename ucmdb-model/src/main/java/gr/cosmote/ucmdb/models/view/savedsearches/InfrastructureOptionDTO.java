package gr.cosmote.ucmdb.models.view.savedsearches;

import gr.cosmote.ucmdb.models.base.ValueObject;
import lombok.Data;

@Data
public class InfrastructureOptionDTO extends ValueObject {
    private static final long serialVersionUID = -4533563473130868527L;

    private String attributeName;
    private String attributeLabel;
    private String attributeValue;
}
