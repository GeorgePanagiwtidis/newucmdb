package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class SimpleCIObject implements Serializable{
    private String ciTitle;
    private String ciType;
    private String ciId;
}
