package gr.cosmote.ucmdb.models.database;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

import javax.persistence.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

@Entity
@Data
@Cache(type = CacheType.NONE)
@ToString
@Slf4j
@Table(name="TASK_SUMMARY")
public class TaskSummary implements Serializable {
    @Id
    @GeneratedValue(generator="task_summary_seq")
    @SequenceGenerator(name="task_summary_seq",sequenceName="TASK_SUMMARY_SEQ", allocationSize=1)
    @Column(name = "ID")
    private long id;

    @Column(name = "ASSIGNED_TO")
    private String assignedTo;

    @ManyToOne(optional = false, targetEntity=Node.class)
    @JoinColumn(name = "NODE_ID", referencedColumnName = "ID", nullable=false, updatable=false)
    private Node node;

    @ManyToOne(optional = false, targetEntity=Group.class)
    @JoinColumn(name = "GROUP_ID", referencedColumnName = "GROUP_ID", nullable=false, updatable=false)
    private Group group;

    @Column(name = "TASK_ID", length = 255)
    long taskId;

    @Column(name = "DISPLAY_LABEL")
    String displayLabel;

    @Column(name = "OWNER_TENANT")
    String ownerTenant;

    @Column(name = "CI_TYPE")
    String ciType;

    @Column(name = "OS_TYPE")
    String osType;

    @Lob
    @Column(name="TASK_DATA")
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    protected byte[] task;

    public void setTask(Object obj) throws IOException {
        try{
            if(obj == null)
                this.task = null;

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(out);
            os.writeObject(obj);
            this.task = out.toByteArray();
        } catch(IOException e) {
            log.error("TaskSummary :: setTask() :: Exception while serializing object to byte array", e);
            this.task = null;
        }
    }

    public Task getTask() {
        try{
            if(this.task == null)
                return null;

            ByteArrayInputStream in = new ByteArrayInputStream(this.task);
            ObjectInputStream is = new ObjectInputStream(in);
            return (Task)is.readObject();
        } catch(IOException e) {
            log.error("TaskSummary :: getTask() :: Exception while deserializing object", e);
            return null;
        } catch(ClassNotFoundException cnfe) {
            log.error("TaskSummary :: getTask() :: Exception while casting to object class", cnfe);
            return null;
        }
    }

}
