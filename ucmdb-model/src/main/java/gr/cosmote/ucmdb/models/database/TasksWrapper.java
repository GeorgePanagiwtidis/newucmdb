package gr.cosmote.ucmdb.models.database;

import gr.cosmote.ucmdb.models.rules.CommandObject;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
@ToString
public class TasksWrapper extends CommandObject implements Serializable {
    private static final long serialVersionUID = 6111548900062832625L;

    private String id;
    private String name;
    private Date date;

    @Getter(AccessLevel.NONE)
    private List<Task> tasks;

    public TasksWrapper() {
        if (this.tasks == null)
            this.tasks = new ArrayList<>();
    }

    public void addTask(Task task) {
        if (this.tasks == null)
            this.tasks = new ArrayList<>();
        this.tasks.add(task);
    }

    public List<Task> getTasks(long processId) {
        tasks.forEach(t -> t.setProcessId(String.valueOf(processId)));
        return tasks;
    }

    /*public List<Task> getTasks() {
        return tasks;
    }*/

    public void removeTask(Task task) {
        if (this.tasks == null)
            this.tasks = new ArrayList<Task>();
        this.tasks.remove(task);
    }

    @Override
    public String toString() {
        return "TasksWrapper{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", date=" + date +
                ", tasks=" + tasks +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        TasksWrapper that = (TasksWrapper) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return date != null ? date.equals(that.date) : that.date == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }
}
