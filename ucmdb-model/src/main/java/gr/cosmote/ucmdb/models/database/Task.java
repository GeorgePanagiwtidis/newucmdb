package gr.cosmote.ucmdb.models.database;

import gr.cosmote.ucmdb.models.base.CIAttribute;
import gr.cosmote.ucmdb.models.enumeration.TaskStatusEnum;
import gr.cosmote.ucmdb.models.enumeration.TaskStatusReasonEnum;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Task implements Serializable {
    private static final long serialVersionUID = -713511376234256452L;

    private long id;
    private String name;
    private TaskStatusEnum status;
    private String droolsHash;
    private String globalId;
    private List<CIAttribute> attributes;
    private Group userGroup;
    private String ciType;//It's required since it does not exists in CIAttributes and its needed for evaluation
    private TaskStatusReasonEnum statusReason;
    private String taskStatusReasonComment;
    private Date assignedDate;
    private Date completionDate;
    private String currentAssignee;
    private String processId;
    private boolean monitored;
    private int currentStage;
    private String osType;
    private Date creationDate;


    public Task() {
        attributes = new ArrayList<>();
    }

    public boolean isOpen() {
        return !status.equals(TaskStatusEnum.COMPLETED) && !status.equals(TaskStatusEnum.CANCELED);
    }

    public boolean isNotSystemTask() {
        return !"SYSTEM".equals(this.getCurrentAssignee());
    }

    public String findCIAttributeValue(String attributeName) {
        CIAttribute attribute = this.findCIAttribute(attributeName);
        if (attribute == null)
            return null;

        return attribute.getFieldValue();
    }

    public CIAttribute findCIAttribute(String attributeName) {
        if (this.attributes == null || this.attributes.size() == 0 || attributeName == null)
            return null;

        CIAttribute attribute = this.attributes.stream()
                .filter(a -> attributeName.equals(a.getFieldName()))
                .findFirst()
                .orElse(null);

        return attribute;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (id != task.id) return false;
        if (name != null ? !name.equals(task.name) : task.name != null) return false;
        if (status != task.status) return false;
        if (droolsHash != null ? !droolsHash.equals(task.droolsHash) : task.droolsHash != null) return false;
        if (attributes != null ? !attributes.equals(task.attributes) : task.attributes != null) return false;
        if (userGroup != null ? !userGroup.equals(task.userGroup) : task.userGroup != null) return false;
        if (ciType != null ? !ciType.equals(task.ciType) : task.ciType != null) return false;
        if (statusReason != task.statusReason) return false;
        if (taskStatusReasonComment != null ? !taskStatusReasonComment.equals(task.taskStatusReasonComment) : task.taskStatusReasonComment != null)
            return false;
        if (assignedDate != null ? !assignedDate.equals(task.assignedDate) : task.assignedDate != null) return false;
        if (completionDate != null ? !completionDate.equals(task.completionDate) : task.completionDate != null)
            return false;
        return currentAssignee != null ? currentAssignee.equals(task.currentAssignee) : task.currentAssignee == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (droolsHash != null ? droolsHash.hashCode() : 0);
        result = 31 * result + (globalId != null ? globalId.hashCode() : 0);
        result = 31 * result + (attributes != null ? attributes.hashCode() : 0);
        result = 31 * result + (userGroup != null ? userGroup.hashCode() : 0);
        result = 31 * result + (ciType != null ? ciType.hashCode() : 0);
        result = 31 * result + (statusReason != null ? statusReason.hashCode() : 0);
        result = 31 * result + (taskStatusReasonComment != null ? taskStatusReasonComment.hashCode() : 0);
        result = 31 * result + (assignedDate != null ? assignedDate.hashCode() : 0);
        result = 31 * result + (completionDate != null ? completionDate.hashCode() : 0);
        result = 31 * result + (currentAssignee != null ? currentAssignee.hashCode() : 0);
        result = 31 * result + (processId != null ? processId.hashCode() : 0);
        result = 31 * result + (monitored ? 1 : 0);
        return result;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TaskStatusEnum getStatus() {
        return status;
    }

    public void setStatus(TaskStatusEnum status) {
        this.status = status;
    }

    public String getDroolsHash() {
        return droolsHash;
    }

    public void setDroolsHash(String droolsHash) {
        this.droolsHash = droolsHash;
    }

    public List<CIAttribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<CIAttribute> attributes) {
        this.attributes = attributes;
    }

    public Group getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(Group userGroup) {
        this.userGroup = userGroup;
    }

    public String getCiType() {
        return ciType;
    }

    public void setCiType(String ciType) {
        this.ciType = ciType;
    }

    public TaskStatusReasonEnum getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(TaskStatusReasonEnum statusReason) {
        this.statusReason = statusReason;
    }

    public String getTaskStatusReasonComment() {
        return taskStatusReasonComment;
    }

    public void setTaskStatusReasonComment(String taskStatusReasonComment) {
        this.taskStatusReasonComment = taskStatusReasonComment;
    }

    public Date getAssignedDate() {
        return assignedDate;
    }

    public void setAssignedDate(Date assignedDate) {
        this.assignedDate = assignedDate;
    }

    public Date getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(Date completionDate) {
        this.completionDate = completionDate;
    }

    public String getCurrentAssignee() {
        return currentAssignee;
    }

    public void setCurrentAssignee(String currentAssignee) {
        this.currentAssignee = currentAssignee;
    }

    public String getGlobalId() {
        return globalId;
    }

    public void setGlobalId(String globalId) {
        this.globalId = globalId;
    }

    public boolean isMonitored() {
        return monitored;
    }

    public void setMonitored(boolean monitored) {
        this.monitored = monitored;
    }

    //TODO: Don't use it! Needs to be persisted throughout the application. It is only setted in reports
    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public int getCurrentStage() {
        return currentStage;
    }

    public void setCurrentStage(int currentStage) {
        this.currentStage = currentStage;
    }

    public String getOsType() {
        return osType;
    }

    public void setOsType(String osType) {
        this.osType = osType;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", status=" + status +
                ", droolsHash='" + droolsHash + '\'' +
                ", globalId='" + globalId + '\'' +
                ", attributes=" + attributes +
                ", userGroup=" + userGroup +
                ", ciType='" + ciType + '\'' +
                ", statusReason=" + statusReason +
                ", taskStatusReasonComment='" + taskStatusReasonComment + '\'' +
                ", assignedDate=" + assignedDate +
                ", completionDate=" + completionDate +
                ", currentAssignee='" + currentAssignee + '\'' +
                ", processId='" + processId + '\'' +
                ", monitored='" + monitored + '\'' +
                '}';
    }

}
