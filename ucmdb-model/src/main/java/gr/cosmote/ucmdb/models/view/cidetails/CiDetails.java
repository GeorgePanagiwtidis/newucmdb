package gr.cosmote.ucmdb.models.view.cidetails;

import gr.cosmote.ucmdb.models.database.UcmdbInfoWindowGroups;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class CiDetails implements Serializable{
    private String globalId;
    private String type;
    private List<UcmdbInfoWindowGroups> groups;
}
