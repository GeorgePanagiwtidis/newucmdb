package gr.cosmote.ucmdb.models.view;

import lombok.Data;

import java.io.Serializable;

@Data
public class BusinessServicesDTO implements Serializable {
    private String globalId;
    private String displayLabel;
    private String tenantOwner;
    private String ciType;
}
