package gr.cosmote.ucmdb.models.enumeration;

/**
 * Created by Intrasoft Intl - dasimakopoulos on 11/8/2017.
 */
public enum InputCommandEnum {
    INITIATE_TASK_COMMAND,
    SUBMIT_TASK_COMMAND,
    REJECT_TASK_COMMAND,
    IGNORE_TASK_COMMAND,
    SAVE_TASK_COMMAND
}
