package gr.cosmote.ucmdb.models.view.savedsearches;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * InfrastructureSearchOptions class is part of SearchOptionsDTO class
 * that is Saved as BLOB in OPTIONS column of SavedSearches Table
 *
 * @author Vasileios Bouzas
 */
@Data
public class InfrastructureSearchOptions implements Serializable {
    private static final long serialVersionUID = 977619975677039082L;

    /**
     * Used for Filtering Results before returned to Frontend
     */
    List<InfrastructureOptionDTO> infraOptions;

    /**
     * Used for Filtering Results that have been already returned
     */
    private List<TableFilter> tableFilters;

    private List<String> selectedColumns;
}
