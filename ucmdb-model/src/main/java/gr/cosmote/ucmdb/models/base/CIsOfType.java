package gr.cosmote.ucmdb.models.base;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class CIsOfType implements Serializable{
    List<Ci> cis;
    String type;
}
