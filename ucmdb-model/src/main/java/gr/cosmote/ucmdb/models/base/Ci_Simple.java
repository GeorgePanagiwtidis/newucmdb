package gr.cosmote.ucmdb.models.base;

import java.io.Serializable;
import java.util.Map;

import lombok.Data;

@Data
public class Ci_Simple implements Serializable {

    private String              id;
    private Map<String, String> properties;
}
