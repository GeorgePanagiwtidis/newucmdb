package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class ExcelCriteria implements Serializable{
    private String businessServiceName;
    private String tenant;
}
