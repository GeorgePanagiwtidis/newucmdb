package gr.cosmote.ucmdb.models.view;

import gr.cosmote.ucmdb.models.base.ValueObject;
import lombok.Data;

import java.util.List;

@Data
public class TaskAttributeDTO extends ValueObject {
    public TaskAttributeDTO() {}

    public TaskAttributeDTO(String fieldLabel, String fieldValue) {
        this.fieldLabel = fieldLabel;
        this.fieldValue = fieldValue;
    }

    public TaskAttributeDTO(String fieldLabel, String fieldName, String fieldValue, FIELDTYPE fieldType,
                            boolean isMandatory, boolean isEditable) {
        this.fieldLabel = fieldLabel;
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
        this.fieldLOV = null;
        this.fieldType = fieldType;
        this.isMandatory = isMandatory;
        this.isEditable = isEditable;
    }

    public TaskAttributeDTO(String fieldLabel, String fieldName, String fieldValue, List<String> fieldLOV, FIELDTYPE fieldType,
                            boolean isMandatory, boolean isEditable) {
        this.fieldLabel = fieldLabel;
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
        this.fieldLOV = fieldLOV;
        this.fieldType = fieldType;
        this.isMandatory = isMandatory;
        this.isEditable = isEditable;
    }

    private String fieldLabel;
    private String fieldName;
    private String fieldValue;
    private List<String> fieldLOV;
    private FIELDTYPE fieldType;

    public enum FIELDTYPE  {
        text, numeric, date,  list, dropdown, dropdown_multiple, checkbox
    };

    private int attributePriority;

    private boolean isMandatory;
    private boolean isEditable;
}
