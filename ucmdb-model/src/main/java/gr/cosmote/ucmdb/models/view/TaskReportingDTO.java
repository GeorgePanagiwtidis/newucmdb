package gr.cosmote.ucmdb.models.view;

import gr.cosmote.ucmdb.models.base.ValueObject;
import lombok.Data;

import java.util.List;

@Data
public class TaskReportingDTO extends ValueObject {
    public enum TASK_STATUS {
        ALL, OPEN, IN_PROGRESS, CANCELED, COMPLETED
    }

    private List<String> tenants;
    private TASK_STATUS taskStatus;
    private String taskGroup;
    private String fromDate;
    private String toDate;
}
