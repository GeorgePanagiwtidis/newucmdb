package gr.cosmote.ucmdb.models.view;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class NodeAuditingDTO implements Serializable {
    private String displayLabel;
    private String globalId;
    private String updatedBy;
    private String comments;
    private Date lastModifiedTime;
    private List<TaskAttributeDTO> beforeUpdate;
    private List<TaskAttributeDTO> afterUpdate;
}
