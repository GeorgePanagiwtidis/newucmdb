package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;


@Data
public class WeblogicAS implements Serializable{
    private String displayLabel;
    private String applicationCategory;
    private String applicationIp;
    private String applicationIpType;
    private String applicationServerType;
    private String ciType;
    private String containerName;
    private String discoveredProductName;
    private String lastAccessTime;
    private String lastModifiedTime;
    private String Name;
    private String productName;
    private String protocol;
    private String vendor;
    private String ciCollectionDisplayLabel;
}
