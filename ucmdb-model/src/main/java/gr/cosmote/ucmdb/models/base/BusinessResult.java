package gr.cosmote.ucmdb.models.base;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.Data;

@Data
public class BusinessResult implements Serializable{

    private String              id;
    private Map<String, String> properties;
    private List<Ci>            ciCollection;
    private Filters             filters;


}