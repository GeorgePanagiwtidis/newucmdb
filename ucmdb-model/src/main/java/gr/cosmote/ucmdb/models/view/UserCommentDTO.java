package gr.cosmote.ucmdb.models.view;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class UserCommentDTO implements Serializable {
    private static final long serialVersionUID = -9049778006351971355L;

    // Ci Attributes
    private String  globalId;
    private String  displayLabel;
    private String  ciType;
    private String  tenantOwner;
    private String  category;
    private String  environment;
    private String  role;
    private String  domain;

    // Comment Attributes
    private String  author;
    private Date    date;
    private String  comment;
}
