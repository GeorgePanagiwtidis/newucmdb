package gr.cosmote.ucmdb.models.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Entity
@Data
@Table(name="GROUPS")
@Cacheable(true)
@Cache(
        type=CacheType.SOFT, // Cache everything until the JVM decides memory is low.
        size=10000,  
        expiry=300000, //5m
        coordinationType= CacheCoordinationType.NONE  // if cache coordination is used, only send invalidation messages.
)
public class Group implements Serializable {
    private static final long serialVersionUID = -1749647228799855982L;

    @Id
    @GeneratedValue(generator="groups_seq")
    @SequenceGenerator(name="groups_seq",sequenceName="GROUPS_SEQ", allocationSize=1)
    @Column(name = "GROUP_ID")
    private int id;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "CREATION_DATE")
    @Temporal(TemporalType.DATE)
    private Date creationDate;

    @Column(name = "ACTIVE")
    private int active;

    @Column (name = "IS_WORKFLOW_RELATED")
    private int isWorkflowRelated;

    /*@ManyToOne
    @JoinColumn(name="PARENT_ID")
    @JsonIgnore
    public GroupDTO parent;*/

    @Column(name="PARENT_ID")
    public int parentId;

    /*@OneToMany(mappedBy="parent", fetch = FetchType.EAGER)
    public List<GroupDTO> subGroups = new ArrayList<GroupDTO>();*/

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JsonIgnore
    @JoinTable(name = "GROUPS", joinColumns = @JoinColumn(name = "PARENT_ID"),
            inverseJoinColumns = @JoinColumn(name = "GROUP_ID"))
    private List<Group> subGroups;

    /*@ManyToMany(mappedBy = "groups", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JsonIgnore
    private List<User> users;*/

    /*@ManyToMany(mappedBy = "groups", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    //@JsonIgnore
    private List<Tenants> tenants = new ArrayList<Tenants>();*/

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    //@JsonIgnore
    @JoinTable(name = "GROUP_TENANT", joinColumns = @JoinColumn(name = "GROUP_ID"),
            inverseJoinColumns = @JoinColumn(name = "TENANT_ID"))
    private List<Tenants> tenants;

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinTable(name = "GROUP_USER", joinColumns = @JoinColumn(name = "GROUP_ID"),
            inverseJoinColumns = @JoinColumn(name = "USER_ID"))
    private List<UcmdbUsers> ucmdbUsers;

    @Transient
    private int numberOfUsersInEachGroup;
    @Transient
    private int numberOfSubgroupsInEachGroup;
}
