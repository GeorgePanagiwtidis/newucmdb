package gr.cosmote.ucmdb.models.view.infrastructureDTOs;

import gr.cosmote.ucmdb.models.base.BusinessSearchCriteria;
import gr.cosmote.ucmdb.models.view.savedsearches.InfrastructureOptionDTO;
import gr.cosmote.ucmdb.models.view.savedsearches.TableFilter;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class InfraExcelExportRequest implements Serializable {
    private BusinessSearchCriteria businessCriteria;
    private List<InfrastructureOptionDTO> advancedTerms;
    private List<String> selectedTableColumns;
    private List<TableFilter> tableFilters;
}
