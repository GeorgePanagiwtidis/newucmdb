package gr.cosmote.ucmdb.models.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Entity
@Table(name = "UCMDB_INFO_WINDOW_GROUPS")
@Cacheable
@Cache(
        type= CacheType.SOFT, // Cache everything until the JVM decides memory is low.
        size=10000,
        expiry=300000, //5m
        coordinationType= CacheCoordinationType.NONE  // if cache coordination is used, only send invalidation messages.
)
public class UcmdbInfoWindowGroups implements Serializable {
    private static final long serialVersionUID = -9222309184014997425L;

    @Id
    @Column(name = "GROUP_ID")
    private Integer groupId;

    @Column(name = "GROUP_NAME")
    private String groupName;

    @Column(name = "GROUP_PRIORITY")
    private Integer groupPriority;

    @OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true, fetch =  FetchType.EAGER)
    @JoinColumn(name = "GROUP_ID")
    @JsonIgnore
    private List<UcmdbInfoWindowGroupAttributes> groupAttributes;

    @Transient
    private Map<String, String> properties = new HashMap<>();

    // Used only for Comments Group
    @Transient
    private List<UcmdbCisComment> cisComments;
}
