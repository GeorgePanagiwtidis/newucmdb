package gr.cosmote.ucmdb.models.view.infrastructureDTOs;

import gr.cosmote.ucmdb.models.base.ValueObject;
import lombok.Data;

@Data
public class InfrastructureRequestDetailsDTO extends ValueObject {
    private String globalId;
    private String ciType;
}
