package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class VirtulizationLayerSoftware implements Serializable {

    private String displayLabel;
    private String applicationIpType;
    private String ciType;
    private String discoveredProductName;
    private boolean isInMaintenanceMode;
    private String lastAccessTime;
    private String lastModifiedTime;
    private boolean vMotionIsEnabled;
    private String vendor;
    private String ciCollectionDisplayLabel;
}
