package gr.cosmote.ucmdb.models.view;

import lombok.Data;

import java.util.List;

@Data
public class TaskHeader {

    private String title;
    private String message;
    private List<TaskAttribute> attributes;
}
