package gr.cosmote.ucmdb.models.view.viewquery;

import gr.cosmote.ucmdb.models.base.GenericCI;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ViewQueryRelatedCI implements Serializable {
    private static final long serialVersionUID = 7213251401526891428L;

    private List<String> businessServiceDisplayLabel;
    private GenericCI relatedCiProperties;
}
