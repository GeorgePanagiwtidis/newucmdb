package gr.cosmote.ucmdb.models.view;

import gr.cosmote.ucmdb.models.base.ValueObject;
import gr.cosmote.ucmdb.models.database.UcmdbUsers;
import lombok.Data;

import java.util.Date;


@Data
public class TaskDTO extends ValueObject {
    public TaskDTO() {}

    private String id;
    private long nodeId;
    private String nodeGlobalId;
    private String displayLabel;
    private String ownerTenant;
    private String ciType;
    private String osType;
    private Date creationDate;
    private Date assignedDate;
    private String assignedTo;
    private String group;
    private String status;
    private String taskId;
    private boolean monitored;
    private int currentStage;
}
