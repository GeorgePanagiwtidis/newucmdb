package gr.cosmote.ucmdb.models.view;

import lombok.Data;

import java.io.Serializable;

@Data
public class BusinessCiCollectionRequestDTO implements Serializable {
    private String id;
    private String ciType;

    // Used for Business Applications CiCollections
    private String parentBusinessId;
}
