package gr.cosmote.ucmdb.models.view;

import gr.cosmote.ucmdb.models.base.CIAttribute;
import lombok.Data;

import java.util.List;

@Data
public class UpdateCiDTO {
    private String globalId;
    private String displayLabel;
    private String tenantOwner;
    private List<CIAttribute> beforeUpdateAttrs;
    private List<CIAttribute> updatedAttributes;
}
