package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class OSUser implements Serializable{

    private String displayLabel;
    private String ciType;
    private String homeDirectory;
    private String lastAccessTime;
    private String lastModifiedTime;
    private String name;
    private String note;
    private String userGroupId;
    private String userId;
    private String ciCollectionDisplayLabel;
}
