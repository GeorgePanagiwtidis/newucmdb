package gr.cosmote.ucmdb.models.view;

import gr.cosmote.ucmdb.models.base.ValueObject;
import gr.cosmote.ucmdb.models.enumeration.NodeStatusEnum;
import lombok.Data;

import java.util.Date;

@Data
public class ProcessReportDTO extends ValueObject {
    // Needed in GUI
    private String displayLabel;
    private String processId;
    private int numberOfTasks;
    private String ciType;
    private NodeStatusEnum status;

    // Needed in Excel
    private String osType;
    private String ownerTenant;
    private Date retrievalDate;
    private String assignedGroup;
}
