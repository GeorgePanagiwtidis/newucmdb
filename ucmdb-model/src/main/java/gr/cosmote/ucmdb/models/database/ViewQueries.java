package gr.cosmote.ucmdb.models.database;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "VIEW_QUERIES")
@NamedQueries({
        @NamedQuery(name = "Queries.getAll", query = "SELECT queries FROM ViewQueries queries"),
        @NamedQuery(name = "Queries.getByName", query = "SELECT query FROM ViewQueries query WHERE query.queryName = :queryName")
})
@Data
public class ViewQueries implements Serializable {
    @Id
    @Column(name = "ID")
    private int id;

    @Column(name = "QUERY_LABEL")
    private String queryLabel;

    @Column(name = "QUERY_NAME")
    private String queryName;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "viewQuery")
    @JsonManagedReference
    private List<ViewQueriesProperties> viewQueriesProperties;
}
