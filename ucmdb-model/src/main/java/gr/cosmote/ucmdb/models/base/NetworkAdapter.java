package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class NetworkAdapter implements Serializable{

    private String displayLabel;
    private String boardIndex;
    private String ciType;
    private String lastAccessTime;
    private String lastModifiedTime;
    private String name;
    private String networkAdapterType;
    private String ciCollectionDisplayLabel;
}
