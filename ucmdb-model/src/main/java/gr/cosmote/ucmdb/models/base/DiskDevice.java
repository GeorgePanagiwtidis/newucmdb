package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class DiskDevice implements Serializable {
    private String displayLabel;
    private String ciType;
    private String diskType;
    private boolean isVirtual;
    private String lastAccessTime;
    private String lastModifiedTime;
    private String name;
    private String ownerTenant;
    private String ciCollectionDisplayLabel;

}
