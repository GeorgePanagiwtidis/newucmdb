package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class Tenant extends ValueObject implements Serializable{
    private String id;
    private String description;
}
