package gr.cosmote.ucmdb.models.database;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Cache(type = CacheType.NONE)
@Table(name="TENANT_SEARCHABLE_TYPES")
public class TenantSearchableType {
    @Id
    @Column(name = "TENANT_ID")
    private int tenantId;

    @Id
    @Column(name = "TYPE")
    private String type;
}
