package gr.cosmote.ucmdb.models.database;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "UCMDB_CIS")
@NamedQueries({
        @NamedQuery(name = "UcmdbCIs.getAllUcmdbCIs", query = "select ucmdbCi from UcmdbCI ucmdbCi"),
        @NamedQuery(name = "UcmdbCIs.getCiBasedOnTenantOwnerDispLabelAndGlobId", query = "select ci from UcmdbCI ci where ci.displayLabel=:dispLabel and ci.tenantOwner=:tenantOwner and ci.globalId=:globalId"),
        @NamedQuery(name = "UcmdbCIs.getCiBasedOnId", query = "select ci from UcmdbCI ci where ci.id=:id")
})
public class UcmdbCI implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ucmdb_cis_seq")
    @SequenceGenerator(name = "ucmdb_cis_seq", sequenceName = "UCMDB_CIS_SEQ")
    private long id;

    @Column(name = "GLOBAL_ID")
    private String globalId;

    @Column(name = "DISPLAY_LABEL")
    private String displayLabel;

    @Column(name = "CI_TYPE")
    private String ciType;

    @Column(name = "TENANT_OWNER")
    private String tenantOwner;

    @Column(name = "CATEGORY")
    private String category;

    @Column(name = "ENVIRONMENT")
    private String environment;

    @Column(name = "ROLE")
    private String role;

    @Column(name = "APPLICATION_DOMAIN")
    private String applicationDomain;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "INSERTED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertedDate;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "UCMDB_ID")
    private List<UcmdbCisComment> ciComments = new ArrayList<>();
}
