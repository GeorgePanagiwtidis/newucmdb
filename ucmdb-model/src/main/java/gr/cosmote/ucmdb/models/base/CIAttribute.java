package gr.cosmote.ucmdb.models.base;

import gr.cosmote.ucmdb.models.commons.Groupable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CIAttribute extends Groupable  implements Serializable {
    private static final long serialVersionUID = -2800205617671906520L;

    String fieldName;
    String fieldLabel;
    String fieldValue;
    String fieldType;
    List<String> fieldLovs;
    private boolean editable;
    private boolean mandatory;
    private boolean canBeNull;
    private int attributePriority;

    public boolean isListType() {
        return "TEXT_LIST".equals(fieldType);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CIAttribute that = (CIAttribute) o;

        if (editable != that.editable) return false;
        if (mandatory != that.mandatory) return false;
        if (fieldName != null ? !fieldName.equals(that.fieldName) : that.fieldName != null) return false;
        if (fieldLabel != null ? !fieldLabel.equals(that.fieldLabel) : that.fieldLabel != null) return false;
        if (fieldValue != null ? !fieldValue.equals(that.fieldValue) : that.fieldValue != null) return false;
        if (fieldType != null ? !fieldType.equals(that.fieldType) : that.fieldType != null) return false;
        return fieldLovs != null ? fieldLovs.equals(that.fieldLovs) : that.fieldLovs == null;
    }

    @Override
    public int hashCode() {
        int result = fieldName != null ? fieldName.hashCode() : 0;
        result = 31 * result + (fieldLabel != null ? fieldLabel.hashCode() : 0);
        result = 31 * result + (fieldValue != null ? fieldValue.hashCode() : 0);
        result = 31 * result + (fieldType != null ? fieldType.hashCode() : 0);
        result = 31 * result + (fieldLovs != null ? fieldLovs.hashCode() : 0);
        result = 31 * result + (editable ? 1 : 0);
        result = 31 * result + (mandatory ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CIAttribute{" +
                "fieldName='" + fieldName + '\'' +
                ", fieldLabel='" + fieldLabel + '\'' +
                ", fieldValue='" + fieldValue + '\'' +
                ", fieldType='" + fieldType + '\'' +
                ", fieldLovs=" + fieldLovs +
                ", editable=" + editable +
                ", mandatory=" + mandatory +
                '}';
    }
}
