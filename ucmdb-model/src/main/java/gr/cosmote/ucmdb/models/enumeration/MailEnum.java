package gr.cosmote.ucmdb.models.enumeration;

/**
 * Created by tskourta on 20-Sep-17.
 */
public enum MailEnum {
    FIRST_TIME_LOGIN,
    TASK_ASSIGNMENT,
    TASK_REASSIGNMENT
}
