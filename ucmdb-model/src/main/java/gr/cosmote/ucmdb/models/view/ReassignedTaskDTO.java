package gr.cosmote.ucmdb.models.view;

import lombok.Data;

import java.io.Serializable;

@Data
public class ReassignedTaskDTO implements Serializable {

    private String reassignReason;
    private String groupId;
}
