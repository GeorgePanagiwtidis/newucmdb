package gr.cosmote.ucmdb.models.view;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserInfoDTO implements Serializable {
    public UserInfoDTO() {}

    public UserInfoDTO(String username, boolean isActive, boolean isAdmin, boolean isWorkflowUser) {
        this.username = username;
        this.isActive = isActive;
        this.isAdmin = isAdmin;
        this.isWorkflowUser = isWorkflowUser;
    }

    private String username;
    private boolean isActive;
    private boolean isAdmin;
    private boolean isWorkflowUser;
}
