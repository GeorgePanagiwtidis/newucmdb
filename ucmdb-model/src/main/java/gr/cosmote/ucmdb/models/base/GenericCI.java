package gr.cosmote.ucmdb.models.base;

import java.io.Serializable;
import java.util.Map;

import lombok.Data;

@Data
public class GenericCI implements Serializable {
    private String id;
    private String type;
    private Map<String, String> properties;
}
