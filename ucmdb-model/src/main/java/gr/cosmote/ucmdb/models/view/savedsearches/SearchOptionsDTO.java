package gr.cosmote.ucmdb.models.view.savedsearches;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * SearchOptionsDTO class is the BLOB that is saved
 * in SavedSearches table in OPTIONS Column
 *
 * @author Vasileios Bouzas - Intrasoft Intl.
 */
@Data
public class SearchOptionsDTO implements Serializable {
    private static final long serialVersionUID = 5056655388411407609L;

    private InfrastructureSearchOptions infraOptions;
    private List<BusinessSearchOptions> businessOptions;
}
