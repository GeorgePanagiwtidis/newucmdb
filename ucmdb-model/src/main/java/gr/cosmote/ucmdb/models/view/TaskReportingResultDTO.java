package gr.cosmote.ucmdb.models.view;

import gr.cosmote.ucmdb.models.base.ValueObject;
import lombok.Data;

import java.util.List;

@Data
public class TaskReportingResultDTO extends ValueObject {
    private List<TaskReportingResultsDTO> taskReportingResultsDTOList;
    private long totalTasks;
}
