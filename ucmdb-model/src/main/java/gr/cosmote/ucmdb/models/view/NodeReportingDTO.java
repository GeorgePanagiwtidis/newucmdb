package gr.cosmote.ucmdb.models.view;

import gr.cosmote.ucmdb.models.base.ValueObject;
import lombok.Data;

import java.util.List;

@Data
public class NodeReportingDTO extends ValueObject {
    private List<String> tenants;
    private String nodeStatus;
    private String fromDate;
    private String toDate;
}
