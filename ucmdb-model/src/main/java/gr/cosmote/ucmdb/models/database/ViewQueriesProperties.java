package gr.cosmote.ucmdb.models.database;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "VIEW_QUERIES_PROPERTIES")
@Data
public class ViewQueriesProperties implements Serializable {
    @Id
    @Column(name = "PROPERTY_ID")
    private int propertyId;

    @Column(name = "PROPERTY_NAME")
    private String propertyName;

    @Column(name = "PROPERTY_LABEL")
    private String propertyLabel;

    @Column(name = "PROPERTY_PRIORITY")
    private int propertyPriority;

    @Column(name = "FILTER_PROPERTY")
    private boolean isSearchFilterProperty;

    @ManyToOne
    @JoinColumn(name = "VIEW_QUERY", nullable = false)
    @JsonBackReference
    private ViewQueries viewQuery;
}
