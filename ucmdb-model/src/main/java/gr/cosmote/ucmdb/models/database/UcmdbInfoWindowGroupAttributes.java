package gr.cosmote.ucmdb.models.database;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.eclipse.persistence.annotations.*;
import org.eclipse.persistence.annotations.Cache;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "UCMDB_INFO_WINDOW_GA")
@NamedQuery(name = "UcmdbSupportAttribute.getSupportAttributes", query = "select attributes from UcmdbInfoWindowGroupAttributes attributes where attributes.groupId = 6")
@Cacheable
@Cache(
        type=CacheType.SOFT, // Cache everything until the JVM decides memory is low.
        size=10000,
        expiry=300000, //5m
        coordinationType= CacheCoordinationType.NONE  // if cache coordination is used, only send invalidation messages.
)
public class UcmdbInfoWindowGroupAttributes implements Serializable {
    @Id
    @GeneratedValue(generator = "groupAttributesGenerator")
    @SequenceGenerator(name = "groupAttributesGenerator", sequenceName = "GROUP_ATTRIBUTES_SEQ")
    @Column(name = "ATTRIBUTE_ID")
    private Integer attributeId;

    @Column(name = "ATTRIBUTE_NAME")
    private String attributeName;

    @Column(name = "ATTRIBUTE_LABEL")
    private String attributeLabel;

    @Column(name = "GROUP_ID")
    private Integer groupId;
}
