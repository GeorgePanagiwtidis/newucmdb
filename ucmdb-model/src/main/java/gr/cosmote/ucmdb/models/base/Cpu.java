package gr.cosmote.ucmdb.models.base;

import lombok.Data;

import java.io.Serializable;

@Data
public class Cpu implements Serializable{

    private String ciCollectionDisplayLabel;
    private String computerDisplayLabel;
    private String displayLabel;
    private String ciType;
    private String cpuVendor;
    private String coreNumber;
    private String cpuSpecifier;
    private String cpuClockSpeed;
    private String cpuID;
    private boolean isVirtual;
    private String lastAccessTime;
    private String lastModifiedTime;
    private String logicalCpuCount;
    private String name;
    private String OwnerTenant;

}
