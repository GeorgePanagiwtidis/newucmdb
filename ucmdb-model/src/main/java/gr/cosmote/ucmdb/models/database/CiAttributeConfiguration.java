package gr.cosmote.ucmdb.models.database;


import java.io.Serializable;
import java.util.List;


import lombok.Data;
import lombok.ToString;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheCoordinationType;
import org.eclipse.persistence.annotations.CacheType;

import javax.persistence.*;

@Entity
@Data
@ToString
@Table(name = "CI_ATTRIBUTES")
@NamedQuery(name = "ci_attributes.getAttributesByCiType", query = "select ciAttribute from CiAttributeConfiguration ciAttribute where ciAttribute.ciType = :ciType")
@Cacheable(true)
@Cache(
        type = CacheType.SOFT, // Cache everything until the JVM decides memory is low.
        size = 10000,
        expiry = 84800000, //24h
        coordinationType = CacheCoordinationType.NONE  // if cache coordination is used, only send invalidation messages.
)
public class CiAttributeConfiguration implements Serializable {
    @Id
    @Column(name = "ATTRIBUTE_ID")
    private int id;

    @Column(name = "CI_TYPE")
    private String ciType;

    @Column(name = "DISPLAY_LABEL")
    private String displayLabel;

    @Column(name = "ATTRIBUTE_NAME")
    private String attributeName;

    @Column(name = "ATTRIBUTE_LABEL")
    private String attributeLabel;

    @Column(name = "IS_WORKFLOW_RELATED")
    private boolean isWorkflowRelated;

    @Column(name = "IS_UPDATABLE")
    private boolean isUpdatable;

    @Column(name = "CAN_BE_NULL")
    private boolean canBeNull;

    @Column(name = "IS_MANDATORY")
    private boolean isMandatory;

    @Column(name = "ATTRIBUTE_PRIORITY")
    private int attributePriority;

    @ManyToOne(optional = false, targetEntity = CiAttributeType.class)
    @JoinColumn(name = "TYPE_ID", referencedColumnName = "ID", nullable = false, updatable = false)
    private CiAttributeType type;

    @Transient
    private List<String> attributeLovs;

    @Transient
    private boolean isVisible;

    public boolean isListType() {
        return "TEXT_LIST".equals(type.getText()) || "LIST_VALUES".equals(type.getText());
    }
}
