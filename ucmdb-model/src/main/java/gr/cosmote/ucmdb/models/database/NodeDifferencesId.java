package gr.cosmote.ucmdb.models.database;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class NodeDifferencesId implements Serializable {

    @Column (name = "TASK_ID")
    private String taskId;

    @Column (name = "NODE_ID")
    private long nodeId;

    public NodeDifferencesId() {}

    public NodeDifferencesId(String taskId, long nodeId) {
        this.taskId = taskId;
        this.nodeId = nodeId;
    }
}