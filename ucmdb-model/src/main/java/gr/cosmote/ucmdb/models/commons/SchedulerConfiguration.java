package gr.cosmote.ucmdb.models.commons;

import lombok.Data;

import java.util.Map;

@Data
public class SchedulerConfiguration {

    public SchedulerConfiguration() {}

    // Copy Constructor from HashMap
    public SchedulerConfiguration (Map<String, String> conf) {
        this.enabled = Boolean.valueOf(conf.get("SCHEDULER_IS_ENABLED"));
        this.midnightScheduler = Boolean.valueOf(conf.get("MIDNIGHT_SCHEDULER"));
        this.hourInterval = Integer.valueOf(conf.get("SCHEDULE_INTERVAL_IN_HOURS"));
        this.minuteInterval = Integer.valueOf(conf.get("SCHEDULE_INTERVAL_IN_MINUTES"));
        this.queryName = conf.get("UNCLASSIFIED_NODES_VIEW_NAME");
        this.procedureName = conf.get("PROCEDURE_NAME_DUPLICATES");
        this.onlySingleChunk = Boolean.valueOf(conf.get("SCHEDULER_RETRIEVE_SINGLE_CHUNK_NODES"));
        this.restrictionDays = Integer.valueOf(conf.get("UNCLASSIFIED_NODES_DAYS_RESTRICTION"));

        this.infraExportEnabled = Boolean.valueOf(conf.get("INFRA_EXPORT_ENABLED"));
        this.infraExportHourInterval = conf.get("INFRA_EXPORT_HOUR_INTERVAL");
        this.infraExportMinInterval = conf.get("INFRA_EXPORT_MIN_INTERVAL");
        this.infraExportNewMinInterval = conf.get("INFRA_EXPORT_NEW_MIN_INTERVAL");
    }

    private boolean enabled;
    private boolean midnightScheduler;
    private int hourInterval;
    private int minuteInterval;

    private String queryName;
    private String procedureName;

    private boolean onlySingleChunk;
    private int restrictionDays;

    private boolean infraExportEnabled;
    private String infraExportHourInterval;
    private String infraExportMinInterval;
    private String infraExportNewMinInterval;
}
