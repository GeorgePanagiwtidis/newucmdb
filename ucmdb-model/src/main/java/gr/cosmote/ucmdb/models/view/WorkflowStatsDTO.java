package gr.cosmote.ucmdb.models.view;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.enterprise.context.ApplicationScoped;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@ApplicationScoped
public class WorkflowStatsDTO implements Serializable {
    private static final long serialVersionUID = -7695106177670687195L;

    private Date    workflowMonth;
    private String  totalMonthsMessage;

    private String  tenantOwner;
    private int     totalTasks;
    private int     completedTasks;
    private int     openTasks;
}
