-- Create table
create table uc_users
(
  username varchar2(30) not null,
  active   number not null
);

-- Create table
create table uc_groups
(
  id          number not null,
  description varchar2(255) not null
);

-- Create table
create table uc_tenants
(
  id          number not null,
  description varchar2(255) not null
);

-- Create table
create table uc_group_tenant
(
  groupid  number not null,
  tenantid number not null
);

-- Create table
create table UC_GROUP_USER
(
  groupid number not null,
  userid  number not null
);

-- Create table
create table TENANT_SEARCHABLE_TYPES
(
  tenant_id number,
  type      varchar2(255)
)
;

-- Create/Recreate primary, unique and foreign key constraints
alter table TENANT_SEARCHABLE_TYPES
  add constraint tenantID_type_UNQ unique (TENANT_ID, TYPE);
