package gr.cosmote.ucmdb.ucmdb_rules.helpers;

import gr.cosmote.ucmdb.models.base.CIAttribute;
import gr.cosmote.ucmdb.models.database.Group;
import gr.cosmote.ucmdb.models.database.Task;
import gr.cosmote.ucmdb.ucmdb_rules.dataObjects.LOVs;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Intrasoft Intl - dasimakopoulos on 19/8/2017.
 */
public class RuleHelper {


    public static Group groupByName(List<Group> groups, String name) {
        for (Group group : groups)
            if (group.getDescription().equals(name))
                return group;

        return null;
    }

    public static boolean containsUserGroup(String group, Task task) {
        if (task.getUserGroup()!=null){
            return task.getUserGroup().getDescription().contains(group);
        }
        return false;
    }

    public static List<CIAttribute> setAttributeValueIfNull(List<CIAttribute> attrList, String attrName, String attrValue) {
        for (int i = 0; i < attrList.size(); i++)
            if (attrList.get(i).getFieldName().equals(attrName) &&
                    (attrList.get(i).getFieldValue() == null || attrList.get(i).getFieldValue().isEmpty())) {
                attrList.get(i).setFieldValue(attrValue);
                return attrList;
            }
        return attrList;
    }

    public static List<CIAttribute> setAttributeValue(List<CIAttribute> attrList, String attrName, String attrValue) {
        for (int i = 0; i < attrList.size(); i++)
            if (attrList.get(i).getFieldName().equals(attrName)) {
                attrList.get(i).setFieldValue(attrValue);
                return attrList;
            }
        return attrList;
    }
    public static List<CIAttribute> assignGroupsToAttributes(List<CIAttribute> attrs) {
        return assignGroupsToAttributes(attrs, LOVs.MADATORY_ATTRIBUTES);
    }

    public static <T extends Object> T clone(T object) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            return (T) ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<CIAttribute> assignGroupsToAttributes(List<CIAttribute> attrs, String[] mandatoryValues) {
        List<String> nonEditableAttributes = Arrays.asList(LOVs.NON_EDITABLE_ATTRIBUTES);

        if (attrs != null) {
            for (int i = 0; i < attrs.size(); i++) {
                //Group Attributes
                if (Arrays.asList(LOVs.DESCRIPTION_GROUP_ATTRIBUTES).contains(attrs.get(i).getFieldName())) {
                    attrs.get(i).setGroupName(LOVs.ATTRIBUTE_GROUP_DESCRIPTION);
                    attrs.get(i).setGroupPriority(Arrays.asList(LOVs.DESCRIPTION_GROUP_ATTRIBUTES).indexOf(attrs.get(i).getFieldName()));
                } else if (Arrays.asList(LOVs.HEADER_GROUP_ATTRIBUTES).contains(attrs.get(i).getFieldName())) {
                    attrs.get(i).setGroupName(LOVs.ATTRIBUTE_GROUP_HEADER);
                    attrs.get(i).setGroupPriority(Arrays.asList(LOVs.HEADER_GROUP_ATTRIBUTES).indexOf(attrs.get(i).getFieldName()));
                } else if (Arrays.asList(LOVs.APPLICATION_GROUP_ATTRIBUTES).contains(attrs.get(i).getFieldName())) {
                    attrs.get(i).setGroupName(LOVs.ATTRIBUTE_GROUP_APPLICATION);
                    attrs.get(i).setGroupPriority(Arrays.asList(LOVs.APPLICATION_GROUP_ATTRIBUTES).indexOf(attrs.get(i).getFieldName()));
                } else if (Arrays.asList(LOVs.SERVICE_GROUP_ATTRIBUTES).contains(attrs.get(i).getFieldName())) {
                    attrs.get(i).setGroupName(LOVs.ATTRIBUTE_GROUP_SERVICE);
                    attrs.get(i).setGroupPriority(Arrays.asList(LOVs.SERVICE_GROUP_ATTRIBUTES).indexOf(attrs.get(i).getFieldName()));
                } else if (Arrays.asList(LOVs.TECHNICAL_GROUP_ATTRIBUTES).contains(attrs.get(i).getFieldName())) {
                    attrs.get(i).setGroupName(LOVs.ATTRIBUTE_GROUP_TECHNICAL);
                    attrs.get(i).setGroupPriority(Arrays.asList(LOVs.TECHNICAL_GROUP_ATTRIBUTES).indexOf(attrs.get(i).getFieldName()));
                } else if (Arrays.asList(LOVs.SUPPORT_GROUP_ATTRIBUTES).contains(attrs.get(i).getFieldName())) {
                    attrs.get(i).setGroupName(LOVs.ATTRIBUTE_GROUP_SUPPORT);
                    attrs.get(i).setGroupPriority(Arrays.asList(LOVs.SUPPORT_GROUP_ATTRIBUTES).indexOf(attrs.get(i).getFieldName()));
                } else {
                    attrs.get(i).setGroupName(LOVs.ATTRIBUTE_GROUP_TECHNICAL);
                    attrs.get(i).setGroupPriority(100);//TODO: handle 100 in UI and put in the end of list
                }

                //Set Optionality
                if (Arrays.asList(mandatoryValues).contains(attrs.get(i).getFieldName())) {
                    attrs.get(i).setMandatory(true);
                }

                //Set Optionality
                if (!nonEditableAttributes.contains(attrs.get(i).getFieldName())) {
                    attrs.get(i).setEditable(true);
                }

            }
        }
        return attrs;
    }


}
