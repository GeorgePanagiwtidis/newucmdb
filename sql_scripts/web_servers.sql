INSERT INTO INFRASTRUCTURE_CI_TYPES VALUES(8, 'web_server', 'Web Servers');

-- Infra Properties
Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'display_label', 'Display Name', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'application_category', 'Application Category', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'application_instancename', 'Application Instance Name', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'application_ip', 'Application IP', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'application_ip_domain', 'Application IP Routing Domain', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'application_ip_type', 'Application IP Type', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'application_password', 'Application Password', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'application_path', 'Application Installed Path', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'application_port', 'Application Listening Port Number', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'application_timeout', 'Application Timeout', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'application_username', 'Application Username', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'application_version', 'Application Version Description', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'business_application_id', 'Business Application ID', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'city', 'City', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'configuration_file_path', 'ConfigurationFilePath', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'country', 'Country or Province', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'create_time', 'Create Time', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'data_note', 'Note', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'description', 'Description', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'discovered_product_name', 'DiscoveredProductName', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'global_id', 'Global Id', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'last_modified_time', 'LastModifiedTime', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'layer', '', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'name', 'Name', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'product_name', 'ProductName', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'root_class', 'CI Type', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'root_container', 'Container', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'root_lastaccesstime', 'Last Access Time', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'server_root', 'Server root', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'software_edition', 'Edition', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'startup_time', 'StartupTime', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'state', 'State', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'TenantOwner', 'Owner Tenant', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'TenantsUses', 'Consumer Tenants', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'user_label', 'User Label', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'vendor', 'Vendor', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'version', 'Version', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'webserver_configfile_case_sensitive', 'Configuration File (Case Sensitive)', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'webserver_type', 'Server Type', 8 );

Insert into INFRA_CI_PROPERTIES( PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.nextval, 'webserver_version', 'Version', 8 );
-- End of Infra Properties

-- Ci Attributes
INSERT INTO CI_ATTRIBUTES(CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
VALUES ('web_server', 'WebServer', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

UPDATE CI_ATTRIBUTES SET IS_WORKFLOW_RELATED = 1 WHERE CI_TYPE='web_server' AND ATTRIBUTE_NAME = 'data_note';