-- Update LOVs
ALTER TABLE CI_ATTRIBUTE_LOV ADD CI_TYPE VARCHAR2(150);

UPDATE CI_ATTRIBUTE_LOV SET CI_TYPE = 'node';

-- Remove NOT-DEFINED
Delete from CI_ATTRIBUTE_LOV where ATTRIBUTE_VALUE = 'NOT-DEFINED' AND ATTRIBUTE_NAME = 'category';
Delete from CI_ATTRIBUTE_LOV where ATTRIBUTE_VALUE = 'NOT-DEFINED' AND ATTRIBUTE_NAME = 'role';
Delete from CI_ATTRIBUTE_LOV where ATTRIBUTE_VALUE = 'NOT-DEFINED' AND ATTRIBUTE_NAME = 'Node_Environment';

-- application_status LOVs
Insert into CI_ATTRIBUTE_LOV (ID , ATTRIBUTE_NAME, ATTRIBUTE_VALUE, CI_TYPE)
values (CI_ATTRIBUTE_LOV_SEQ.NEXTVAL, 'application_status', 'ACTIVE', 'business_application');

Insert into CI_ATTRIBUTE_LOV (ID , ATTRIBUTE_NAME, ATTRIBUTE_VALUE, CI_TYPE)
values (CI_ATTRIBUTE_LOV_SEQ.NEXTVAL, 'application_status', 'RETIRED', 'business_application');

Insert into CI_ATTRIBUTE_LOV (ID , ATTRIBUTE_NAME, ATTRIBUTE_VALUE, CI_TYPE)
values (CI_ATTRIBUTE_LOV_SEQ.NEXTVAL, 'application_status', 'TO-BE-RETIRED', 'business_application');

-- category (database)
Insert into CI_ATTRIBUTE_LOV (ID , ATTRIBUTE_NAME, ATTRIBUTE_VALUE, CI_TYPE)
values (CI_ATTRIBUTE_LOV_SEQ.NEXTVAL, 'category', 'PRODUCTION', 'database');

Insert into CI_ATTRIBUTE_LOV (ID , ATTRIBUTE_NAME, ATTRIBUTE_VALUE, CI_TYPE)
values (CI_ATTRIBUTE_LOV_SEQ.NEXTVAL, 'category', 'STAGE', 'database');

Insert into CI_ATTRIBUTE_LOV (ID , ATTRIBUTE_NAME, ATTRIBUTE_VALUE, CI_TYPE)
values (CI_ATTRIBUTE_LOV_SEQ.NEXTVAL, 'category', 'TEST/DEV', 'database');

-- role LOVs
Insert into CI_ATTRIBUTE_LOV (ID , ATTRIBUTE_NAME, ATTRIBUTE_VALUE, CI_TYPE)
values (CI_ATTRIBUTE_LOV_SEQ.NEXTVAL, 'role', 'OTHER', 'node');

Insert into CI_ATTRIBUTE_LOV (ID , ATTRIBUTE_NAME, ATTRIBUTE_VALUE, CI_TYPE)
values (CI_ATTRIBUTE_LOV_SEQ.NEXTVAL, 'role', 'INFRASTRUCTURE MANAGEMENT AND NETWORK', 'node');

Insert into CI_ATTRIBUTE_LOV (ID , ATTRIBUTE_NAME, ATTRIBUTE_VALUE, CI_TYPE)
values (CI_ATTRIBUTE_LOV_SEQ.NEXTVAL, 'role', 'CSS APPLICATION', 'node');

Insert into CI_ATTRIBUTE_LOV (ID , ATTRIBUTE_NAME, ATTRIBUTE_VALUE, CI_TYPE)
values (CI_ATTRIBUTE_LOV_SEQ.NEXTVAL, 'role', 'CSS INFRA-OPS', 'node');

Insert into CI_ATTRIBUTE_LOV (ID , ATTRIBUTE_NAME, ATTRIBUTE_VALUE, CI_TYPE)
values (CI_ATTRIBUTE_LOV_SEQ.NEXTVAL, 'role', 'DSS INFRA-OPS', 'node');

Insert into CI_ATTRIBUTE_LOV (ID , ATTRIBUTE_NAME, ATTRIBUTE_VALUE, CI_TYPE)
values (CI_ATTRIBUTE_LOV_SEQ.NEXTVAL, 'role', 'DSS APPLICATION', 'node');
--End LOVs

-- Update CI ATTRIBUTES
-- Type: business_application
Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('business_application', 'BusinessApplication', 'application_owner', 'Application Owner', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 0, 0, 1, 27, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('business_application', 'BusinessApplication', 'application_status', 'Application Status', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 2, 0, 0, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('business_application', 'BusinessApplication', 'external_system_id_1', 'External System ID 1', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 0, 0, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('business_application', 'BusinessApplication', 'external_system_id_2', 'External System ID 2', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 0, 0, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('business_application', 'BusinessApplication', 'external_system_name_1', 'External System Name 1', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 0, 0, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('business_application', 'BusinessApplication', 'external_system_name_2', 'External System Name 2', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 0, 0, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('business_application', 'BusinessApplication', 'layer_4b_owner', 'Layer 4b Owner', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 0, 0, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('business_application', 'BusinessApplication', 'layer_4bs_contact', 'Layer 4b Support Contact', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 0, 0, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('business_application', 'BusinessApplication', 'layer_4bs_intern', 'Layer 4b Supported Internally', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 3, 0, 0, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('business_application', 'BusinessApplication', 'layer_5s_contact', 'Layer 5 Support Contact', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 0, 0, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('business_application', 'BusinessApplication', 'layer_5s_intern', 'Layer 5 Supported Internally', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 3, 0, 0, 0, 100, 0);


-- Type: node
Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('node', 'Node', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('node', 'Node', 'uda_reason', 'UDA Reason', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 0, 0, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('node', 'Node', 'uda_status', 'UDA Status', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 0, 0, 0, 100, 0);

-- Type: running_software
Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('running_software', 'RunningSoftware', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

-- Type: ip_address
Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('ip_address', 'IpAddress', 'exclude', 'Exclude', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 3, 0, 0, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('ip_address', 'IpAddress', 'exclude_reason', 'Exclude Reason', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 0, 0, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('ip_address', 'IpAddress', 'ip_in_probe_range', 'IP in Probe range', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 3, 0, 0, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('ip_address', 'IpAddress', 'network_notes', 'Network Notes', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 0, 0, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('ip_address', 'IpAddress', 'uda_network_access', 'UDA Network Access', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 0, 0, 0, 100, 0);
--END CI ATTRIBUTES

-- Update INFRA_CI_PROPERTIES
Insert into INFRA_CI_PROPERTIES (PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.NEXTVAL, 'business_application_id', 'Business Application ID', 1);

Insert into INFRA_CI_PROPERTIES (PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.NEXTVAL, 'is_duplicate_candidate', 'Is Duplicate Candidate', 1);

Insert into INFRA_CI_PROPERTIES (PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.NEXTVAL, 'network_notes', 'Network Notes', 1);

Insert into INFRA_CI_PROPERTIES (PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.NEXTVAL, 'uda_reason', 'UDA Reason', 1);

Insert into INFRA_CI_PROPERTIES (PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.NEXTVAL, 'uda_status', 'UDA Status', 1);

Insert into INFRA_CI_PROPERTIES (PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.NEXTVAL, 'category', 'Category', 2);

Insert into INFRA_CI_PROPERTIES (PROPERTY_ID, PROPERTY_NAME, PROPERTY_LABEL, TYPE_ID)
values (INFRA_CI_PROPERTIES_SEQ.NEXTVAL, 'ip_in_probe_range', 'IP in Probe range', 4);
--End INFRA_CI_PROPERTIES


-- Update CI_ATTRIBUTES for Database & Subtypes CI Types
Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('database', 'Database', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('database', 'Database', 'category', 'Category', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 2, 1, 1, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('db2', 'DB2', 'category', 'Category', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 2, 1, 1, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('hana_database', 'HanaDatabase', 'category', 'Category', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 2, 1, 1, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('hana_instance', 'HanaInstance', 'category', 'Category', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 2, 1, 1, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('maxdb', 'MaxDB', 'category', 'Category', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 2, 1, 1, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('mysql', 'MySQL', 'category', 'Category', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 2, 1, 1, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('nonstop_sql_mx', 'NonStop SQL/MX', 'category', 'Category', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 2, 1, 1, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('oracle', 'Oracle', 'category', 'Category', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 2, 1, 1, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('postgresql', 'PostgreSQL', 'category', 'Category', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 2, 1, 1, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('sqlserver', 'SQL Server', 'category', 'Category', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 2, 1, 1, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('sybase', 'SAP ASE Server', 'category', 'Category', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 2, 1, 1, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('Teradata', 'Teradata', 'category', 'Category', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 2, 1, 1, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('vertica', 'Vertica', 'category', 'Category', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 2, 1, 1, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('db2_database', 'Db2Database', 'category', 'Category', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 2, 1, 1, 0, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('db2_instance', 'Db2Instance', 'category', 'Category', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 2, 1, 1, 0, 100, 0);

UPDATE CI_ATTRIBUTES SET IS_WORKFLOW_RELATED = 1
WHERE CI_TYPE='database' AND ATTRIBUTE_NAME = 'data_note';
-- End

-- Update CI_Attributes of Node & Subtypes CI Type
Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('cluster_resource_group', 'ClusterResourceGroup',  'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('host_node', 'Computer', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('netdevice', 'Net Device', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('as400_node', 'iSeries', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('hp_complex', 'HP Complex', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('hp_nonstop', 'HP NonStop', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('ibm_pseries_frame', 'IBM Frame', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('ldom_server', 'LDOM Server', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('lpar', 'Mainframe Logical Partition', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('mainframe', 'Mainframe CPC', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('nt', 'Windows', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('terminalserver', 'Terminal Server', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('unix', 'Unix', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('vax', 'OpenVMS', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('vmware_esx_server', 'VMware ESX Server', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('zos', 'zOS', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('atmswitch', 'ATM Switch', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('chassis', 'Chassis', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('concentrator', 'Concentrator', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('firewall', 'Firewall', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('ip_phone', 'IP Phone', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('lb', 'Load Balancer', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('management_processor', 'Management Processor', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('netapp_filer', 'NetApp Filer', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('netapp_node', 'NetApp Node', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('netprinter', 'Net Printer', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('rack', 'Rack', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('ras', 'Remote Access Service', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('router', 'Router', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('storagearray', 'Storage Array', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('switch', 'Switch', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('tapelibrary', 'Tape Library', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('marconiatmswitch', 'Marconi ATM Switch', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('enclosure', 'Enclosure', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('fcswitch', 'Fibre Channel Switch', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('ldom_virtual_switch', 'LDOM Virtual Switch', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('switchrouter', 'Switch Router', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('vmware_distributed_switch', 'VMware Distributed Virtual Switch', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);

Insert into CI_ATTRIBUTES (CI_TYPE, DISPLAY_LABEL, ATTRIBUTE_NAME, ATTRIBUTE_LABEL, ATTRIBUTE_ID, TYPE_ID, IS_WORKFLOW_RELATED, IS_UPDATABLE, CAN_BE_NULL, ATTRIBUTE_PRIORITY, IS_MANDATORY)
values ('vmware_virtual_switch', 'VMware Virtual Switch', 'business_application_id', 'Business Application ID', CI_ATTRIBUTE_ID_SEQ.NEXTVAL, 1, 1, 1, 1, 100, 0);